<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
identification for dev
Hieu => OtMain
Thao => Ot1
Luong => Ot2
| -------------------------------------------------------------------------
*/
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = 'home/page404';
$route['translate_uri_dashes'] = FALSE;
// $route['404'] = 'home/page404';


//dashboard
$route['dashboard'] = 'dashboard/home';
$route['(signin).html'] = 'dashboard/auths/signin';
$route['(profile).html'] = 'cpanel/auths/profile';
$route['(check-2fa).html/(:any)'] = 'dashboard/auths/check2fa';
$route['(signup).html'] = 'dashboard/auths/signup';
$route['(logout).html'] = 'dashboard/auths/logout';
$route['(notify).html'] = 'dashboard/auths/notify';
$route['(check-mail-notify).html'] = 'dashboard/auths/checkmailNotify';
$route['(check-active-notify).html'] = 'dashboard/auths/checkactiveNotify';
// $route['(forget-password).html'] = 'dashboard/auths/forgetPassword';
$route['(notify-forget-password).html'] = 'dashboard/auths/notifyforgetPassword';
$route['(reset-password).html/(:any)'] = 'dashboard/auths/resetPassword';
$route['(reset-password-success).html'] = 'dashboard/auths/resetPasswordSuccess';
$route['(recruitment)/page/(:num)'] = 'recruitment/index/$1/$2';

$route['(active-account).html/(:any)'] = 'dashboard/auths/activeAccount';

$route['(update-password).html/(:any)'] = 'dashboard/auths/updatePassword';

$route['(get-country-code).html'] = 'dashboard/auths/getcountryCode';

//dashboard - account - OTMain 
$route['(dashboard/change-password).html'] = 'dashboard/auths/changePassword';
$route['(dashboard/network-tree).html'] = 'dashboard/auths/networktree';
$route['(dashboard/referal).html'] = 'dashboard/auths/referal';

//dashboard - deposite - OTMain
$route['(dashboard/deposite).html'] = 'dashboard/deposite/index';
$route['(dashboard/deposite-history).html'] = 'dashboard/deposite/history';

//dashboard - Withdraw - OTMain
$route['(dashboard/withdraw).html'] = 'dashboard/withdraw/index';
$route['(dashboard/withdraw-history).html'] = 'dashboard/withdraw/history';
$route['(dashboard/notify-withdraw).html'] = 'dashboard/withdraw/notifyWithDraw';
$route['(dashboard/trans-to-pending).html/(:any)'] = 'dashboard/withdraw/transToPending';
$route['(dashboard/trans-to-pending-success).html'] = 'dashboard/withdraw/notifyPendingSuccess';
$route['(dashboard/trans-to-pending-error).html'] = 'dashboard/withdraw/notifyPendingError';


//dashboard - bonus/commission - OTMain
$route['(dashboard/bonus).html'] = 'dashboard/bonus/index';
$route['(dashboard/commission).html'] = 'dashboard/commission/index';

//dashboard - exchange - OTMain
$route['(dashboard/transfer).html'] = 'dashboard/exchange/index';
$route['(dashboard/transfer-history).html'] = 'dashboard/exchange/history';

//dashboard - exchange - OTMain
$route['(dashboard/support).html'] = 'dashboard/support/index';
$route['(dashboard/support-history).html'] = 'dashboard/support/history';

//dashboard - buy token - OTMain
$route['(dashboard/buy-token).html'] = 'dashboard/token/index';
$route['(dashboard/buy-token-history).html'] = 'dashboard/token/history';


//google-authenticator / 2FA - OTMain
$route['(dashboard/google-authen).html'] = 'dashboard/auths/googleAuthen';
$route['(dashboard/disabled-google-2fa).html'] = 'dashboard/auths/disabledGoogle2FA';

//admin
$route['cpanel'] = 'cpanel/home';
$route['(cpanel/login).html'] = 'cpanel/auths/login';
$route['(cpanel/forget-password).html'] = 'cpanel/auths/forgetpass';
$route['(cpanel/logout).html'] = 'cpanel/auths/logout';
$route['(:any).html/page/(:num)']="route/index/$1/$2";
//====pagination====
// recruitment
// $route['recruitment'] = 'recruitment/home';
$route['recruitment/Applylist/page/(:num)'] = 'recruitment/Applylist/recruitmentFunction/$1';
$route['recruitment/Applylist/search'] = 'recruitment/Applylist/recruitmentSeach/$2/$3';
$route['recruitment/Applylist/search/page/(:num)'] = 'recruitment/Applylist/recruitmentSeach/$2';
//search - OT1
$route['(search).html'] = 'cpanel/user/search/$1';
$route['(search).html/page/(:num)'] = 'cpanel/user/search/$2';
//platform - OTMain
$route['(platform).html']="platform/index";
//content link
$route['(category)/(:any).html'] = 'blog/index/$2';
// building
$route['(building).html'] = 'cpanel/building/index';
// building
$route['permission/notpermission'] = 'cpanel/permission/notpermission';

// tuyển dụng
$route['(tuyen-dung).html'] = 'recruitment/applyregister/index';

$route['(ung-tuyen-thanh-cong).html'] = 'recruitment/Applyregister/applysuccess';
// lấy quận / huyện 
$route['(district).html'] = 'recruitment/Applyregister/getDistrict';
// lấy phường xã 
$route['(wards).html'] = 'recruitment/Applyregister/getWards';
// validate 
$route['valiedateimage'] = 'recruitment/Applyregister/valiedateimage';
$route['valiedatefile'] = 'recruitment/Applyregister/valiedatefile';
// đăng ký 
$route['register'] = 'recruitment/Applyregister/addApply';
// bài kiểm tra
$route['(bai-kiem-tra).html/(:num)'] = 'recruitment/Maketest/index/$2';
// hoàn thành bài kiểm tra
$route['(hoan-thanh-kiem-tra).html'] = 'recruitment/Maketest/donetest';
//==================================================================================



