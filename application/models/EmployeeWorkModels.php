<?php 
class EmployeeWorkModels extends MY_Model {
	public $table = 'tbl_employees_works';
	function __construct()
	{
		parent::__construct();
	}
	
	function check_exists($where = array())
	{
		$this->db->where($where);
		$query = $this->db->get('tbl_employees_works');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}

?>
