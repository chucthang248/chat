<?php

class UserModels extends MY_Model {

	public $table = 'tbl_user';

	function __construct()
	{
		parent::__construct();
	}
	function checkExist_account()
	{
		$this->db->select('employeeID')->from('tbl_user');
		$this->db->where(array('role_id !=' => 0));
		 $where_clause = $this->db->get_compiled_select();
		 //main query
		$this->db->select('id, fullname');
		$this->db->from('tbl_employees');
		$result  = $this->db->where("id NOT IN ($where_clause)", NULL, FALSE);
		$result = $this->db->get()->result_array();
		return $result;
		
	}
	//checkValidate_Email - Ot1
	function check_exists($where = array()){
		$this->db->where($where);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	//checkEmail - Login Google - Ot1
	function check_exists_google($where = array()){
		$this->db->where($where);
		$query = $this->db->get('tbl_user');
		if($query->num_rows() > 0){
			return array(
				'type'		=> 'successful',
				'message'	=> 'Add data success!',
			);
		}else{
			return array(
				'type'		=> 'error',
				'message'	=> 'Add data error!',
			);
		}
	}
	//select_row_min -OT1
	function select_row_min($table = '', $field_min = '',$where = NULL ){
		$result = $this->db->select_min($field_min)->from($table);
		if($where != NULL){
			$result = $this->db->where($where);
		}
		$result = $this->db->get()->row_array();
		return $result;
	}
	//total (Dashboarr/Auths) -OT1
	function total($table,$where = NULL){
		$result = $this->db->from($table);
		if($where != NULL){
			$result = $this->db->where($where);
		}
		$result = $this->db->count_all_results();
		return $result;
	}
	// Search data users -OT2
	function getSearch($query){ 
		$sql = $this->db->query("SELECT * FROM `tbl_user` WHERE type = 'user' AND  (email LIKE '%$query%' or phone LIKE '%$query%')");
		$query = $sql->result_array();				
		return $query;
	}


	// Search data users -OT1
	function total_where_in($table,$where = NULL,$field = '',$ar_where = NULL){
		$result = $this->db->from($table);
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($ar_where != NULL && $field != ''){
			$result = $this->db->where_in($field,$ar_where);
		}
		$result = $this->db->count_all_results();
		return $result;
	}



	function getListMember($parent_id, &$arr_childs)
	{
		$childs = $this->query_sql->select_array('tbl_user', 'id', ['referentID' => $parent_id]);
		if (!empty($childs)) {
			foreach ($childs as $child) {
				$arr_childs[] = $child['id'];
				$this->getListMember($child['id'], $arr_childs);
			}
		}
		return $arr_childs;
	}
	function getUserTree($parentid){
		$menu = $this->query_sql->select_array('tbl_user', 'id,fullname,referentID, walletUSD',array('referentID' => $parentid));
		if($menu != NULL){
			$data .= '<ul>';
			foreach ($menu as $key => $val) {
				$data .= '<li>'.$val['fullname'].' - '.number_format($val['walletUSD'], 2);
				$data .= $this->getUserTree($val['id']);
				$data .= '</li>';
			}
			$data .= '</ul>';
		}
		return $data;
	}
	function totalF1Level1($userID){
		$result = $this->db->from($this->table)->where(array('referentID' => $userID, 'level >' => 0));
		$result = $this->db->count_all_results();
		return $result;
	}	
}
