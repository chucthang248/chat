<?php
class Upload extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}
	function upload_image($path_dir, $filename, $filename_new, $fieldname, $thumb_with = 500, $max_size = 1024){
		$path_dir_thumb = $path_dir.'thumb/';
		// upload/new
		//check folder exists
		if (!is_dir($path_dir)){mkdir($path_dir);} 
		if (!is_dir($path_dir_thumb)){mkdir($path_dir_thumb);} 

		$config['upload_path']	= $path_dir;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		//change name image 
		if($filename_new == ''){
			$str = $this->get_name_image($filename);
			$str = $this->Functions->create_alias($str);
			$filename = $str.'.'.$ext;
		}else{
			$filename = $filename_new.'.'.$ext;
		}
		

		//set type images allow upload
		$config['allowed_types'] = array('gif','png' ,'jpg', 'jpeg');
		if(in_array($ext,$config['allowed_types']) === FALSE) {
			return array(
		    	'type' 			=> 'error',
		    	'message' 		=> ' Chỉ hỗ trợ file jpg, jpeg, png, gif'
		    );
		    exit();    
		} 
		//set size images allow upload
		if(($_FILES["image"]["size"]/1024) > $max_size) {
			return array(
		    	'type' 			=> 'error',
		    	'message' 		=> 'Dung lượng file tối đa là '.($max_size). ' Kb',
		    );
		    exit(); 
		}
		$config['file_name'] = $filename;
		$this->load->library('upload', $config); 
		$this->upload->initialize($config); 
		$image = $this->upload->do_upload($fieldname);
		$image_data = $this->upload->data();
		$name_image = $image_data['file_name'];

		//Create thumb
		$this->load->library('image_lib');
		$config['image_library'] 	= 'gd2';
		$config['source_image'] 	= $path_dir.$image_data['file_name'];
		$config['new_image'] 		= $path_dir_thumb.$image_data['file_name'];
		$config['create_thumb'] 	= TRUE;
		$config['maintain_ratio'] 	= TRUE;
		$config['width'] = $thumb_with;

		$str = $this->get_name_image($image_data['file_name']);
		$name_thumb 	 = $str.'_thumb.'.$ext;
	    $this->image_lib->initialize($config);
	    $this->image_lib->resize();
	    $this->image_lib->clear();

		$result = array(
			'image'			=> $name_image,
			'thumb'			=> $name_thumb,
		);
	    return $result;
	}
	function upload_pdf($path_dir, $filename, $fieldname, $max_size = 1024){
		//check folder exists
		if (!is_dir($path_dir)){mkdir($path_dir);} 

		$config['upload_path']	= $path_dir;
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		//set type images allow upload
		$config['allowed_types'] = array('pdf');
		if(in_array($ext,$config['allowed_types']) === FALSE) {
			return array(
		    	'type' 			=> 'error',
		    	'message' 		=> ' Chỉ hỗ trợ pdf'
		    );
		    exit();    
		} 
		//set size images allow upload
		if(($_FILES["image"]["size"]/1024) > $max_size) {
			return array(
		    	'type' 			=> 'error',
		    	'message' 		=> 'Dung lượng file tối đa là '.($max_size). ' Kb',
		    );
		    exit(); 
		}
		$config['file_name'] = $filename;
		$this->load->library('upload', $config); 
		$this->upload->initialize($config); 
		$image = $this->upload->do_upload($fieldname);
		$image_data = $this->upload->data();
		$name_image = $image_data['file_name'];

		$result = array(
			'file'			=> $name_image,
		);
	    return $result;
	}
	function get_name_image($nameImage)
	{
		$ar_name_image = explode('.',$nameImage);
		$str ="";
		foreach ($ar_name_image as $key => $value) {
			$character = $key > 0 ? "_": "";
			if($key<count($ar_name_image)-1){
				$str .= $character.$value;
			}else{
				break;
			}
		}
		return $str;
	}
}