<?php 
class OgchartsModels extends MY_Model {
	public $table = 'tbl_ogcharts';
	function __construct()
	{
		parent::__construct();
	}
	// phong ban
	public function dequy($data,$count = 0)
	{
		$mainArray = [];
		foreach($data as $key_data => $val_data)
		{
			$childrenMenu = $this->OgchartsModels->findWhere(array('parentID'=> $val_data['id']), 'id,name,parentID', 'id desc');
			$val_data['count'] = $val_data['parentID'] != 0 ? $count : 0;
			$val_data['child'] =  $childrenMenu == NULL ? "" : "child";
			array_push($mainArray,$val_data); 
			if($childrenMenu != NULL)
			{
				$submenu = $this->dequy($childrenMenu,$count+1);
				$mainArray = array_merge($mainArray,$submenu);
			}	
		}
		
		return $mainArray ;
	} 
	public function getDataShowSelect($parentid,$option = '|-----',$id = 0,$key_select = '')
	{
		$data = '';
		$getData = $this->db->select('id,name,parentid')->from($this->table)->where(array('parentid' => $parentid))->get()->result_array();
		if($getData != NULL){
			if($parentid > 0){ $option = $option.'-----'; }
			foreach ($getData as $key => $val) {
				$selected = "";
				if($val['id'] ==  $key_select)
				{
					$selected = "selected";
				}
				$data .= '<option '.$selected. ' value="'.$val['id'].'"';
				if($id == $val['id']){
					$data .= 'selected';
				}
				$data .= '>'.$option.$val['name'].'</option>';
				$data .= $this->getDataShowSelect($val['id'], $option,$id);
			}
		}
		return $data;
	}
}

?>
