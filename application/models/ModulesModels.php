<?php 
class ModulesModels extends MY_Model {
	public $table = 'tbl_modules';
	function __construct()
	{
		parent::__construct();
		$this->load->model('ModulesDetailModels');
	}
	public function dequy($data,$count = 0)
	{
		$mainArray = [];
		foreach($data as $key_data => $val_data)
		{
			$childrenMenu = $this->ModulesModels->findWhere(array('parentID'=> $val_data['id']), '*', 'id asc');
			$val_data['count'] = $val_data['parentID'] != 0 ? $count : 0;
			$val_data['child'] =  $childrenMenu == NULL ? "" : "child";
			$moduleDetails = $this->ModulesDetailModels->findWhere(array('moduleID'=> $val_data['id']), '*', 'sort asc');
			if($moduleDetails != NULL)
			{
				$val_data['moduleDetails'] = $moduleDetails;
			}
			array_push($mainArray,$val_data); 
			if($childrenMenu != NULL)
			{
				$submenu = $this->dequy($childrenMenu,$count+1);
				$mainArray = array_merge($mainArray,$submenu);
			}	
		}
		return $mainArray ;
	} 
	function check_exists($where = array())
	{
		$this->db->where($where);
		$query = $this->db->get('tbl_modules');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}

?>
