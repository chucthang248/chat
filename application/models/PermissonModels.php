<?php 
class PermissonModels extends MY_Model {
	public $table = 'tbl_permission';
	function __construct()
	{
		parent::__construct();
		$this->load->model('UserModels');
	}
	public function check_permisson($controller_action)
	{
		$userID = $this->Auth->logged_id();
		// lấy thông tin người người đăng nhập từ bảng tbl_user
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];

		// lấy phân quyền từ bảng tbl_permission
		// xóa dấu / cuối cùng (tránh trường hợp người dùng nhập có dấu / ...select không có trong database)
		$controller_action = rtrim($controller_action, '/');
		// check trong trường hợp có số ở sau vd: edit/1 , edit/23
		$arr_url = explode('/',$controller_action);
		// lấy phần tử cuối cùng trong mảng vd (cpanel/employee/edit/23) => 23
		$count_uri = count($arr_url)-1;
		// nếu phần tử cuối cùng là số (các id dùng để xóa, sửa)
		if(is_numeric($arr_url[$count_uri]))
		{	
			// nếu là số thì xóa phần tử cuối cùng
			unset($arr_url[$count_uri]);
			// gộp mảng bằng dấu / sau khi xóa phần tử cuối 
			$controller_action = join("/",$arr_url);
		}
		$permission_row = $this->db->select("*")->from($this->table);
		$permission_row = $this->db->where(array('roleID' => $userRow['role_id'],'link' => $controller_action));
		$permission_row = $this->db->get()->row_array();
		// select table từ $controller_action
		// admin root (mặc định trong database)
		if($userRow['type'] == "admin")
		{
			return true;
		}
		else
		{
			if($permission_row == NULL )
			{
				return false;
			}
		}
		
		return true;
	}
}

?>
