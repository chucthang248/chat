<?php 
class ChatModels extends MY_Model {
	public $table = 'tbl_chat_private';
	function __construct()
	{
		parent::__construct();
        $this->load->model(['UserModels']);
	}

    // load danh sách chat
    function loadList()
    {
        $login_id = $this->Auth->logged_id();
        $query = $this->db->query("SELECT tbl_employees.*, tbl_user.id as userID FROM tbl_employees, tbl_user  WHERE tbl_employees.id IN 
            (
                SELECT employeeID FROM tbl_user WHERE id IN 
                ( 
                    SELECT DISTINCT if(user_send_id = $login_id ,user_receive_id,user_send_id) 
                    AS listChat FROM tbl_chat_private WHERE  $login_id IN (user_send_id, user_receive_id)
                )
            ) 
            AND tbl_employees.id = tbl_user.employeeID 
        ");
        $row = [];
        if ($query->num_rows() > 0)
		{
			$row       = $query->result();
		}
		return $row;
    }
    // load messenger của item đầu tiên
    function loadMessenger($user_send_id = NULL ,$user_receive_id = NULL)
    {
        $row = [];
        if($user_send_id == NULL)
        {
            $user_send_id = $this->Auth->logged_id();
        }
         $data_check =  $this->db->query("SELECT * FROM tbl_chat_private WHERE user_send_id = $user_send_id OR  user_receive_id	 = $user_send_id  ") ;
         $data_check->num_rows();
         if($data_check->num_rows() > 0)
         {
            $user_send_id = $this->Auth->logged_id();
            //  lấy tin nhắn chat chứa id người gửi , người nhận
            $query = $this->db->query("SELECT tbl_chat_private.*,DATE_FORMAT(tbl_chat_private.created_at, '%d/%m/%Y, %H:%i:%s') 
                as created_at, tbl_user.employeeID,tbl_user.fullname,tbl_user.id as userID,tbl_employees.code, tbl_employees.avatar,
                if(user_send_id =  $user_send_id ,user_receive_id,user_send_id) AS listChat 
                FROM tbl_chat_private , tbl_user, tbl_employees
                WHERE  
                $user_send_id IN (user_send_id, user_receive_id) AND 
                $user_receive_id IN (user_send_id, user_receive_id) AND 
                tbl_user.employeeID	= tbl_employees.id AND	
                tbl_user.id = tbl_chat_private.user_send_id");
                if ($query->num_rows() > 0) {
                    $row  = $query->result();
                }
         }
           
		return $row;



    }

}

?>
