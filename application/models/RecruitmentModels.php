<?php 
class RecruitmentModels extends MY_Model {
	public $table = 'tbl_recruitment';
	function __construct()
	{
		parent::__construct();
		
	}
	function check_exists($where = array())
	{
		$this->db->where($where);
		$query = $this->db->get('tbl_recruitment');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	function createAliasAdd($name){
		$alias = $this->Functions->create_alias($name);
		$data_alias = $this->find($alias, 'alias',  'alias');
		if($data_alias != NULL){
			$randdom = rand(9,9999);
			$alias = $alias.'-'.$randdom;
		}
		return $alias;
	}

	function createdAlias($alias='',$alias_old = ''){
		$random = rand(0,999);
		$result_alias = $alias;
		if($alias_old){
			if($alias != $alias_old){
				$data = $this->find($alias, '*',  'alias');
				if($data != NULL){
					$result_alias = $alias.'-'.$random;
				}
			}
		}else{
			$data = $this->find($alias, 'alias',  'alias');
			if($data != NULL){
				$result_alias = $alias.'-'.$random;
			}else
			{
				$result_alias = $alias;
			}
		}
		//var_dump( $result_alias);die;
		return $result_alias;
	}
}

?>
