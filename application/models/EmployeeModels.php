<?php 
class EmployeeModels extends MY_Model {
	public $table = 'tbl_employees';
	function __construct()
	{
		parent::__construct();
	}


	function count_date_from_today($startdate)
	{
		$date = date('Y-m-d', time());
		$startwork = new DateTime($startdate);
		$today = new DateTime($date);
		$difference = $startwork->diff($today);
		$str_date = "";
		if($difference->y != 0)
		{
			$str_date .=$difference->y.' năm, ' ;
		}
	    if($difference->m != 0)
		{
			$str_date .=$difference->m.' tháng, ' ;
		}
		if($difference->d != 0)
		{
			$str_date .=$difference->d.' ngày ' ;
		}
		$str_date = rtrim($str_date, ', ');
		return $str_date;
	}
	

	function check_exists($where = array())
	{
		$this->db->where($where);
		$query = $this->db->get($this->table);
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	function check_marital($marital)
	{
		if($marital == 1){
			$rs = 'Độc thân';
		}else if($marital == 2){
			$rs = 'Đã kết hôn';
		}else if($marital == 3){
			$rs = 'Khác';
		}else{
			$rs = '----';
		}
		return $rs;
	}
	function getjob_position_titleIDByUserID($userID)
	{
		$this->db->select('user.employeeID,', FALSE);
		$this->db->where(array('user.id' => $userID));
 		$this->db->select('employees.job_position_titleID', FALSE);
 		$this->db->from('tbl_user as user');
 		$this->db->join("tbl_employees as employees", ' employees.id = user.employeeID','left');
 		$result = $this->db->get()->row_array();
 		return $result['job_position_titleID'];
	}



	function getCarrerIDByUserID($userID)
	{
		$this->db->select('user.employeeID,', FALSE);
		$this->db->where(array('user.id' => $userID));
 		$this->db->select('employees.careerroadsID', FALSE);
 		$this->db->from('tbl_user as user');
 		$this->db->join("tbl_employees as employees", ' employees.id = user.employeeID','left');
 		$result = $this->db->get()->row_array();
 		return $result['careerroadsID'];
	}


	function getOgchartIDByUserID($userID)
	{
		$this->db->select('user.employeeID,', FALSE);
		$this->db->where(array('user.id' => $userID));
 		$this->db->select('employees.ogchartID', FALSE);
 		$this->db->from('tbl_user as user');
 		$this->db->join("tbl_employees as employees", ' employees.id = user.employeeID','left');
 		$result = $this->db->get()->row_array();
 		return $result['ogchartID'];
	}

	function getjobpositionIDByUserID($userID)
	{
		$this->db->select('user.employeeID,', FALSE);
		$this->db->where(array('user.id' => $userID));
 		$this->db->select('employees.job_positionID', FALSE);
 		$this->db->from('tbl_user as user');
 		$this->db->join("tbl_employees as employees", ' employees.id = user.employeeID','left');
 		$result = $this->db->get()->row_array();
 		return $result['job_positionID'];
	}
}

?>
