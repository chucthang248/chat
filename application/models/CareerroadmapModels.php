<?php 
class CareerroadmapModels extends MY_Model {
	public $table = 'tbl_careerroads';
	function __construct()
	{
		parent::__construct();
	}

	 // de quy
	 public function dequy($data,$count = 0)
	 {
		 $mainArray = [];
		 foreach($data as $key_data => $val_data)
		 {
			 $childrenMenu = $this->db->select('*')->from($this->table)->where(array('parentID' => $val_data['id']))->get()->result_array();
			 $val_data['count'] = $val_data['parentID'] != 0 ? $count : 0;
			 $val_data['child'] =  $childrenMenu == NULL ? "" : "child";
				array_push($mainArray,$val_data); 
			 if($childrenMenu != NULL)
			 {
				 $submenu = $this->dequy($childrenMenu,$count+1);
				 $mainArray = array_merge($mainArray,$submenu);
			 }	
		 }
		 
		 return $mainArray ;
	 } 


	function check_exists($where = array())
	{
		$this->db->where($where);
		$query = $this->db->get('tbl_careerroads');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}

?>
