<?php 
class EmployeeBankModels extends MY_Model {
	public $table = 'tbl_employees_banks';
	function __construct()
	{
		parent::__construct();
	}
	function check_exists($where = array())
	{
		$this->db->where($where);
		$query = $this->db->get('tbl_employees_banks');
		if($query->num_rows() > 0)
		{
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}

?>
