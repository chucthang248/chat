<?php 
class JobpositionModels extends MY_Model {
	public $table = 'tbl_job_positions';
	function __construct()
	{
		parent::__construct();
	}
	    // de quy
		public function dequy($data,$count = 0)
		{
			$mainArray = [];
			foreach($data as $key_data => $val_data)
			{
				$childrenMenu = $this->db->select('id,name,parentID')->from($this->table)->where(array('parentID' => $val_data['id']))->get()->result_array();
				$val_data['count'] = $val_data['parentID'] != 0 ? $count : 0;
				$val_data['child'] =  $childrenMenu == NULL ? "" : "child";
				array_push($mainArray,$val_data); 
				if($childrenMenu != NULL)
				{
					$submenu = $this->dequy($childrenMenu,$count+1);
					$mainArray = array_merge($mainArray,$submenu);
				}	
			}
			
			return $mainArray ;
		} 


}

?>
