<?php 
class ApplyModels extends MY_Model {
	public $table = 'tbl_apply';
	function applyid_notin_calendarinterivew()
	{
		$this->db->select('applyID')->from('tbl_calendar_interview');
		 $where_clause = $this->db->get_compiled_select();
		 //main query
		$this->db->select('*');
		$this->db->from('tbl_apply');
		$result  = $this->db->where("id NOT IN ($where_clause)", NULL, FALSE);
		$result = $this->db->get()->result_array();
		if(count($result) == 0)
		{
			$this->db->select('applyID')->from('tbl_calendar_interview');
			$result_calendar_interview = $this->db->get()->result_array();
			if($result_calendar_interview == NULL)
			{
				$this->db->select('*');
				$this->db->from('tbl_apply');
				$result = $this->db->get()->result_array();
			}
		}
		return $result;
		
	}
	function applyid_notin_calendarinterivew_id($uid)
	{
		$this->db->select('applyID')->from('tbl_calendar_interview');
		$this->db->where_not_in('uid',$uid);
		 $where_clause = $this->db->get_compiled_select();
		 //main query
		$this->db->select('*');
		$this->db->from('tbl_apply');
		$result  = $this->db->where("id NOT IN ($where_clause)", NULL, FALSE);
		$result = $this->db->get()->result_array();
		return $result;
	}

	function search($fields = '*', $where = NULL, $likeValue, $order = 'id desc',$start = '', $limit = ''){
		$result = $this->db->select($fields)->from($this->table);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($likeValue != ''){
			$result = $this->db->like('fullname', $likeValue); 
			$result = $this->db->or_like('phone', $likeValue);
			$result = $this->db->or_like('email', $likeValue);
		}
		$result = $this->db->order_by($order);
		$result = $this->db->get()->result_array();
		return $result;
	}
}

?>
