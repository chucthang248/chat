<?php 
class EmployeeSalaryModels extends MY_Model {
	public $table = 'tbl_employees_salary';
	function __construct()
	{
		parent::__construct();
	}

	function total_medium_datework($startdate)
	{
		$date = date('Y-m-d', time());
		$startwork = new DateTime($startdate);
		$today = new DateTime($date);
		$difference = $startwork->diff($today);

		return $difference->days;
	}
	// tổng lương cơ bản
	function total_slary_basic()
	{
		$result = $this->db->select("SUM(salary_basic) AS salary_basic")->from($this->table);
		$result = $this->db->get()->row_array();
		return $result;
	}
}

?>
