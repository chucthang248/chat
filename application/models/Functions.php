<?php
class Functions extends CI_Model{
	function __construct()
	{
		parent::__construct();
	}
	

	//định dạng tiền (vd: 1000000 => 1.000.000)
	function currency_format($number, $suffix = '')
	{
		if (!empty($number)) {
			return number_format($number, 0, ',', ',') . "{$suffix}";
		}
	}
	function my_pagination($number_button = 3,$total_rows,$per_page,$page,$base_url='',$alias='',$key_word = '') 
		{
			// $number_button ( số button phân trang muốn hiển thị vd : 4 ( hiển thị chỉ có 1 2 3 4 trong dãy (1 2 3 4 5 6 7 8) )
			$total_page_display = ceil($total_rows / $per_page);
			$prev = $page > 1 ? $page -1 : 1;
			$next = $page +1;
			$starSlice = 0;
			//mảng chứa tất các phân trang
			$arr_page = [];
			if($total_page_display > 1 ){
				// tổng tất cả trang , lặp tạo ra mảng 
					for($i=0; $i < $total_page_display; $i++)
					{
						$numbPage = $i+1;
						$arr_page[$i] = $numbPage;
					}
					$starSlice = $page - 1   ;  // bắt đầu lấy từ ...
					//cắt mảng
					// var_dump($starSlice);die;
					$sliceArray = array_slice($arr_page,$starSlice,$number_button); // cắt ra từ mảng
				
					//nếu trong mảng tồn tại số cuối cùng và số page tồn tại trong mảng đó 
					if (in_array($page, $sliceArray) && in_array($arr_page[count($arr_page)-1], $sliceArray)) 
					{ 
						// nếu tổng tất cả các trang hiện tại > số trang muốn hiển thị hoặc tổng số các trang = số phân trang muốn hiển thị
						// ví dụ: tổng trang hiện tại = 6 , số phân trang cần hiển thị = 6 ...=> 6 = 6
						if(count($arr_page) > $number_button || count($arr_page) <= $number_button)
						{
							// lấy mảng mặc định hiện tại
							// nếu số phân trang hiện tại < số lượng phân trang hiển thị
							// thì $starSlice = $arr_page[count($arr_page)-1] -$number_button = số âm (-1,-2 ..v..v)
							$starSlice = $arr_page[count($arr_page)-1] -$number_button ;
							// var_dump($starSlice);die;
							//$starSlice = 0 nếu $starSlice < 0
							$starSlice = $starSlice < 0 ? 0 : $starSlice;
							
							//cắt mảng
							$sliceArray = array_slice($arr_page,$starSlice,$number_button);
						}
					} 
				
					$item_li = '';
					$item_li .= '<ul class="pagination ul_pagination">';
						if($page > 1 && count($arr_page) > $number_button)
						{
							$firstPage = 1;
							$item_li .= '<li class="paginate_button page-item previous forClick "  id="datatable_previous">
											<a href="'.$base_url.$alias.'/page'.'/'.$prev.'" class="page-link" data-ci-pagination-page="'.$prev.'" rel="prev">
												<i class="fa fa-angle-left" aria-hidden="true"></i>
											</a>
										</li>';
						}
					
						foreach($sliceArray as $key => $value)
						{
							$active  = $value == $page ? 'active' : '';
							$href    = $value == $page ? 'javascript:void(0)' : $base_url.$alias.'/page'.'/'.$value.''.$key_word.'';
								$item_li .= '<li class="'.$active.' page-item forClick">
									<a href="'.$href.'" data-ci-pagination-page="'.$value.'" class="page-link">'.$value.'</a>
									</li>';
						}
					
						// nếu phần tử cuối cùng của mảng vừa cắt < phần tử cuối cùng của tổng phân trang hiện tại 
						// thi in ">" (next)
						if($sliceArray[count($sliceArray)-1] < $arr_page[count($arr_page)-1]  )
						{
							$endPage =$arr_page[count($arr_page)-1];
							
							$item_li .= '<span style="display:inline-block;transform: translateY(12px);margin-right: 4px;margin-left: 4px;font-size: 20px;">...</span>
											<li class="paginate_button page-item next forClick">
												<a href="'.$base_url.$alias.'/page'.'/'.$endPage.''.$key_word.'"   class="page-link" data-ci-pagination-page="'.$endPage.'" rel="next">'
													.$endPage.
												'</a>
											</li>';
							$item_li .= '<li class="paginate_button page-item next forClick">
										<a href="'.$base_url.$alias.'/page'.'/'.$next.''.$key_word.'"  class="page-link" data-ci-pagination-page="'.$next.'" rel="next">
											<i class="fa fa-angle-right" aria-hidden="true"></i>
										</a>
									</li>';
						}
				
					$item_li .= '</ul>';
					return $item_li;
			}else
			{
				return NULL;
			}

		}
	function changDate($date,$type){
		$ar_date = explode('/',$date);
		if($type == 1){
			$result = $ar_date[2].'-'.$ar_date[1].'-'.$ar_date[0];
		}
		return $result;
	}
	function createCode($number,$table){
		$code = $this->CI_function->RandomString($number);
		$data = $this->query_sql->select_row($table, 'id',array('code' => $code));
		if($order != NULL){
			$code = $this->CI_function->RandomString($number);
		}
		return $code;
	}
	function RandomString($length = 10) {
		$characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	function RandomNumber($length = 10) {
		$characters = '0123456789';
		$charactersLength = strlen($characters);
		$data = '';
		for ($i = 0; $i < $length; $i++) {
			$data .= $characters[rand(0, $charactersLength - 1)];
		}
		return $data;
	}
	function changFormatDate($date) {
		$ar_date = explode('/',$date);
		$data = $ar_date[2].'-'.$ar_date[1].'-'.$ar_date[0];
		return $data;
	}
	function createdAlias($alias='',$type,$alias_old = ''){
		$random = rand(0,999);
		$check = $this->query_sql->select_row('tbl_alias', '*', array('alias' => $alias_old,'type' => $type));
		if($check != NULL){
			if($alias == $alias_old){
				$alias = $alias_old;
			}else{
				$data = $this->query_sql->select_row('tbl_alias', '*', array('alias' => $alias));
				if($data != NULL){
					$alias = $alias.'-'.$random;
				}
			}
			$data_edit = array(
				'alias' 	=> 	$alias,
			);
			$result = $this->query_sql->edit('tbl_alias', $data_edit,array('id' => $check['id']));
		}else{
			$data = $this->query_sql->select_row('tbl_alias', '*', array('alias' => $alias));
			if($data != NULL){
				$alias = $alias.'-'.$random;
			}
			$data_add = array(
				'alias' 	=> 	$alias,
				'type' 		=> 	$type,
				'created_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600)
			);
			$result = $this->query_sql->add('tbl_alias', $data_add);
		}
		return $alias;
	}
	function create_alias($bien)
	{
		if($bien != '')
		{
			$marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
				"ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề"
				,"ế","ệ","ể","ễ",
				"ì","í","ị","ỉ","ĩ",
				"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
				,"ờ","ớ","ợ","ở","ỡ",
				"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
				"ỳ","ý","ỵ","ỷ","ỹ",
				"đ",
				"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
				,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
				"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
				"Ì","Í","Ị","Ỉ","Ĩ",
				"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
				,"Ờ","Ớ","Ợ","Ở","Ỡ",
				"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
				"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
				"Đ",
				"!","@","#","$","%","^","&","*","(",")");

			$marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
				,"a","a","a","a","a","a",
				"e","e","e","e","e","e","e","e","e","e","e",
				"i","i","i","i","i",
				"o","o","o","o","o","o","o","o","o","o","o","o"
				,"o","o","o","o","o",
				"u","u","u","u","u","u","u","u","u","u","u",
				"y","y","y","y","y",
				"d",
				"A","A","A","A","A","A","A","A","A","A","A","A"
				,"A","A","A","A","A",
				"E","E","E","E","E","E","E","E","E","E","E",
				"I","I","I","I","I",
				"O","O","O","O","O","O","O","O","O","O","O","O"
				,"O","O","O","O","O",
				"U","U","U","U","U","U","U","U","U","U","U",
				"Y","Y","Y","Y","Y",
				"D",
				"","","","","","","","","","");
			$bien = trim($bien);
			$bien = str_replace("/","",$bien);
			$bien = str_replace(":","",$bien);
			$bien = str_replace("!","",$bien);
			$bien = str_replace("(","",$bien);
			$bien = str_replace(")","",$bien);
			$bien = str_replace($marTViet,$marKoDau,$bien);
			$bien = str_replace("-","",$bien);
			$bien = str_replace("  "," ",$bien);
			$bien = str_replace(" ","-",$bien);
			$bien = str_replace("%","-",$bien);
			$bien = str_replace("+","-",$bien);
			$bien = str_replace("'","",$bien);
			$bien = str_replace("“","",$bien);
			$bien = str_replace("”","",$bien);
			$bien = str_replace(",","",$bien);
			$bien = str_replace("’","",$bien);
			$bien = str_replace(".","",$bien);
			$bien = str_replace('"',"",$bien);
			$bien = str_replace('\\','',$bien);
			$bien = str_replace('//','',$bien);
			$bien = str_replace('?','',$bien);
			$bien = str_replace('&','',$bien);
			$bien = strtolower($bien);
			return $bien;
		}
	}
	function delStringSpecial($bien)
	{
		if($bien != ''){
			$bien = trim($bien);
			$bien = str_replace("/","",$bien);
			$bien = str_replace(":","",$bien);
			$bien = str_replace("!","",$bien);
			$bien = str_replace("(","",$bien);
			$bien = str_replace(")","",$bien);
			$bien = str_replace("-","",$bien);
			$bien = str_replace("  "," ",$bien);
			$bien = str_replace("%","",$bien);
			$bien = str_replace("+","",$bien);
			$bien = str_replace("'","",$bien);
			$bien = str_replace("“","",$bien);
			$bien = str_replace("”","",$bien);
			$bien = str_replace(",","",$bien);
			$bien = str_replace('//','',$bien);
			$bien = str_replace('?','',$bien);
			$bien = str_replace('&','',$bien);
			$bien = str_replace('–','',$bien);
			return $bien;
		}
	}
	function stripUnicode($str){
		$marTViet=array("à","á","ạ","ả","ã","â","ầ","ấ","ậ","ẩ","ẫ","ă",
			"ằ","ắ","ặ","ẳ","ẵ","è","é","ẹ","ẻ","ẽ","ê","ề"
			,"ế","ệ","ể","ễ",
			"ì","í","ị","ỉ","ĩ",
			"ò","ó","ọ","ỏ","õ","ô","ồ","ố","ộ","ổ","ỗ","ơ"
			,"ờ","ớ","ợ","ở","ỡ",
			"ù","ú","ụ","ủ","ũ","ư","ừ","ứ","ự","ử","ữ",
			"ỳ","ý","ỵ","ỷ","ỹ",
			"đ",
			"À","Á","Ạ","Ả","Ã","Â","Ầ","Ấ","Ậ","Ẩ","Ẫ","Ă"
			,"Ằ","Ắ","Ặ","Ẳ","Ẵ",
			"È","É","Ẹ","Ẻ","Ẽ","Ê","Ề","Ế","Ệ","Ể","Ễ",
			"Ì","Í","Ị","Ỉ","Ĩ",
			"Ò","Ó","Ọ","Ỏ","Õ","Ô","Ồ","Ố","Ộ","Ổ","Ỗ","Ơ"
			,"Ờ","Ớ","Ợ","Ở","Ỡ",
			"Ù","Ú","Ụ","Ủ","Ũ","Ư","Ừ","Ứ","Ự","Ử","Ữ",
			"Ỳ","Ý","Ỵ","Ỷ","Ỹ",
			"Đ",
			"!","@","#","$","%","^","&","*","(",")");

		$marKoDau=array("a","a","a","a","a","a","a","a","a","a","a"
			,"a","a","a","a","a","a",
			"e","e","e","e","e","e","e","e","e","e","e",
			"i","i","i","i","i",
			"o","o","o","o","o","o","o","o","o","o","o","o"
			,"o","o","o","o","o",
			"u","u","u","u","u","u","u","u","u","u","u",
			"y","y","y","y","y",
			"d",
			"A","A","A","A","A","A","A","A","A","A","A","A"
			,"A","A","A","A","A",
			"E","E","E","E","E","E","E","E","E","E","E",
			"I","I","I","I","I",
			"O","O","O","O","O","O","O","O","O","O","O","O"
			,"O","O","O","O","O",
			"U","U","U","U","U","U","U","U","U","U","U",
			"Y","Y","Y","Y","Y",
			"D",
			"","","","","","","","","","");
		$str = trim($str);
		$str = str_replace("/","",$str);
		$str = str_replace(":","",$str);
		$str = str_replace("!","",$str);
		$str = str_replace("(","",$str);
		$str = str_replace(")","",$str);
		$str = str_replace($marTViet,$marKoDau,$str);
		return $str;
	}
	
}
?>
