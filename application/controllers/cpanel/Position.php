<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Position extends Admin_Controller {
	public $template = 'cpanel/position/';
	public $title = 'chức vụ';
	public $control = 'position';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('PositionModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->PositionModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function add()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($data_post != NULL){ 
				$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				$result = $this->PositionModels->add($data_post);
				if($result>0){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> ADD_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> ADD_FAIL,
					));
				}
				redirect(CPANEL.$this->control);
			}
		}
	}
	
	public function getDataRow()
	{
		$id = $_POST['id'];
		$dataRow = $this->PositionModels->find($id);
		echo json_encode(array('result' => $dataRow));
	}
	public function edit()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$id = $this->input->post('idRow');
			$data_update = $this->input->post('data_update');
			if($data_update != NULL){ 
				//push date update into data_update
				$data_update['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				//if publish unchecked
				if(!$data_update['publish']){
					$data_update['publish'] = 0;
				}

				$result = $this->PositionModels->edit($data_update,$id);
				if($result>0){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> EDIT_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> EDIT_FAIL,
					));
				}
				redirect(CPANEL.$this->control);
			}
		}
	}
	public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->PositionModels->edit($data_update,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->PositionModels->delete($id);
	}
}
