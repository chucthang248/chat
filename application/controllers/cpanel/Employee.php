<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Employee extends Admin_Controller
{
	public $template = 'cpanel/employee/';
	public $title = 'nhân sự';
	public $control = 'employee';
	public $path_dir = 'upload/employee/';
	public $per_page = 60;
	public $numb_button = 3;
	public function __construct()
	{
		parent::__construct();
		$this->get_index();
		$this->load->library('Excel');
		$this->load->model('UserModels');
		$this->load->model('EmployeeModels');
		$this->load->model('BankModels');
		$this->load->model('Upload');
		$this->load->model('EmployeeBankModels');
		// Trình độ (Nhân viên)
		$this->load->model('EmployeeStandardModels');
		// người thân  (Nhân viên)
		$this->load->model('EmployeeRelativeModels');
		// loại hình kinh doanh
		$this->load->model('BussinesstypeModels');
		// việc làm (Nhân viên)
		$this->load->model('EmployeeWorkModels');
		// lương (Nhân viên)
		$this->load->model('EmployeeSalaryModels');
		// tình trạng học vấn
		$this->load->model('EdustatusModels');
		// trường học
		$this->load->model('SchoolModels');
		// ngành học 
		$this->load->model('MajorsModels');
		// bảng trình độ học vấn chứa 
		//(EdustatusModels, StandardModels , SchoolModels, MajorsModels, EmployeeModels) 
		$this->load->model('EmployeelevelModels');
		// FILE
		$this->load->helper("file");
		// trình độ 
		$this->load->model('StandardModels');
		// lịch sử làm việc
		$this->load->model('EmployeeHistoryWorkModels');
		// chi nhánh 
		$this->load->model('BranchModels');
		// chức danh
		$this->load->model('PositionModels');
		// module
		$this->load->model('ModulesModels');
		// chi tiết module
		$this->load->model('ModulesDetailModels');
		// hợp đồng
		$this->load->model('ContractModels');
		// vị trí công việc
		$this->load->model('JobpositionModels');
		// phòng ban
		$this->load->model('OgchartsModels');
		// phân loại nhân sự
		$this->load->model('ClassifyemployeeModels');
		// phòng ban
		$this->load->model('CareerroadmapModels');
	}
	
	// index
	public function index()
	{
		
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		// check quyền xóa
		$page = 1; 
		$where = array('publish' => 1);
		$config['total_rows'] = count($this->EmployeeModels->getAll());
		$config['per_page'] = $this->per_page;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = 1;
		$number_button_page = $this->numb_button;
		$page = ($page > $total_page) ? $total_page : $page;

		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->EmployeeModels->select_array_wherein('*', NULL, '', NULL, $order = 'status asc,id desc', ($page * $config['per_page']), $config['per_page']);
		}
		foreach ($getDatas as $key_currency => $val_datas) {
			$getDatas[$key_currency]['salary_current'] = $this->currency_format($val_datas['salary_current']);
			$getDatas[$key_currency]['salary'] = $this->EmployeeSalaryModels->findRowWhere(array('employeeID' => $val_datas['id']), '*');
			$getDatas[$key_currency]['salary']['salary'] = $this->currency_format($getDatas[$key_currency]['salary']['salary']);
			$getDatas[$key_currency]['salary']['salary_basic'] = $this->currency_format($getDatas[$key_currency]['salary']['salary_basic']);
			// chi nhánh
			$rs_historywork = $this->EmployeeHistoryWorkModels->findRowWhere(array('employeeID' => $val_datas['id']));

			$rs_branch = $this->BranchModels->findRowWhere(array('id' => $rs_historywork['branchID']));
			$getDatas[$key_currency]['branch_name'] = $rs_branch['name'];
			// Vị trí 
			$rs_position = $this->PositionModels->findRowWhere(array('id' => $rs_historywork['positionID']));
			$getDatas[$key_currency]['position_name'] = $rs_position['name'];
			// hợp đồng
			$rs_contract = $this->ContractModels->findRowWhere(array('employeeID' => $val_datas['id']));

			$type_contract = $this->setTypeContract();
			$getDatas[$key_currency]['contract_name'] =  $type_contract[$rs_contract['type_contractID']];
			// thâm niên
			$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($val_datas['id'] , '*', $field = 'employeeID');
			$getDatas[$key_currency]['startwork_name'] = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
			$getDatas[$key_currency]['startwork']  = $employeeHistoryWork['startwork'];
		}

		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/employee');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'datas'		=>  $getDatas,
			'count'		=> $config['total_rows'],
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	
	//add
	public function add()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		//set code
		$code = $this->setCode();
		$code_hide_max = $this->EmployeeModels->find_row_max('code_hide');
		$code_hide = (int)$code_hide_max['code_hide'] + 1;
		if ($this->input->post()) {
			$data_post = $this->input->post('data_post'); // data_post[salary_current]
			$data_post['salary_current'] = preg_replace('/[,.]/', '', $data_post['salary_current']);
			$data_post['salary_desire'] = preg_replace('/[,.]/', '', $data_post['salary_desire']);
			if ($data_post != NULL) {

				$data_post['code_hide'] = $code_hide;
				//upload avatar
				if ($_FILES["avatar"]["name"]) {
					$filename = $_FILES['avatar']['name'];
					$path_dir = $this->path_dir . $data_post['code'] . '/';
					$data_upload = $this->Upload->upload_image($path_dir, $filename, 'avatar', 'avatar', 300);
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['avatar'] = $data_upload['image'];
					}
				}
				//upload ID Front
				if ($_FILES["id-front"]["name"]) {
					$filenameIdFront = $_FILES['id-front']['name'];
					$path_dir = $this->path_dir . $data_post['code'] . '/';
					$data_upload = $this->Upload->upload_image($path_dir, $filenameIdFront, 'id-front', 'id-front');
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['id-front'] = $data_upload['image'];
					}
				}
				//upload ID Back
				if ($_FILES["id-back"]["name"]) {
					$filenameIdBack = $_FILES['id-back']['name'];
					$path_dir = $this->path_dir . $data_post['code'] . '/';
					$data_upload = $this->Upload->upload_image($path_dir, $filenameIdBack, 'id-back', 'id-back');
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['id-back'] = $data_upload['image'];
					}
				}

				//format birthday
				$data_post['birthday'] = $this->Functions->changFormatDate($data_post['birthday']);
				//field use check cmnd/passport
				$data_post['identify_type'] = $this->input->post('identify_type');
				//insert data
				$data_post['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$result = $this->EmployeeModels->add($data_post);
				$id_insert = $result['id_insert'];

				if ($result > 0) {
					// EMPLOYEE LEVEL
					$data_post_employeelevel = $this->input->post('data_post_employeelevel');
					if ($data_post_employeelevel != NULL) {
						$data_post_employeelevel['employeeID'] = $id_insert;
						$data_post_employeelevel['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$data_post_employeelevel['updated_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$result = $this->EmployeelevelModels->add($data_post_employeelevel);
					}
					//add bank info
					$data_post_bank = $this->input->post('data_post_bank');
					if ($data_post_bank != NULL) {
						$data_post_bank['employeeID'] = $id_insert;
						$data_post_bank['is_default'] = 1;
						$data_post_bank['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$this->EmployeeBankModels->add($data_post_bank);
					}

					//add standard
					$data_post_standard = $this->input->post('data_post_standard');
					$data_post_standard_insert = array();
					if ($data_post_standard != NULL) {
						for ($i = 0; $i < count($data_post_standard['date_from']); $i++) {
							if ($data_post_standard['date_from'][$i] != '' && $data_post_standard['date_to'][$i] != '') {
								//format date
								$date_from = $this->Functions->changFormatDate($data_post_standard['date_from'][$i]);
								$date_to = $this->Functions->changFormatDate($data_post_standard['date_to'][$i]);
								$data_post_standard_insert[] = array(
									'employeeID'	=>	$id_insert,
									'date_from' 	=> $date_from,
									'date_to' 		=> $date_to,
									'content' 		=> $data_post_standard['content'][$i],
									'created_at' 	=> gmdate('Y-m-d H:i:s', time() + 7 * 3600)
								);
							}
						}
						if ($data_post_standard_insert != NULL) {
							$this->EmployeeStandardModels->adds($data_post_standard_insert);
						}
					}
					// lương & phúc lợi
					$data_post_salary = $this->input->post('data_post_salary');
					if ($data_post_salary !=  NULL) {
						$data_post_salary['salary'] = preg_replace('/[,.]/', '', $data_post_salary['salary']);
						$data_post_salary['salary_basic'] = preg_replace('/[,.]/', '', $data_post_salary['salary_basic']);
						$data_post_salary['employeeID']  	= 	$id_insert;
						$data_post_salary['created_at'] 	= gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$data_post_salary['updated_at'] 	= gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$this->EmployeeSalaryModels->add($data_post_salary);
					}
					//add relative info (người thân)
					$data_post_relative = $this->input->post('data_post_relative');
					$data_post_relative_insert = array();
					if ($data_post_relative != NULL) {
						for ($i = 0; $i < count($data_post_relative['fullname']); $i++) {
							if ($data_post_relative['fullname'][$i] != '') {
								//format date
								$data_post_relative_insert[] = array(
									'employeeID'	=>	$id_insert,
									'fullname' 		=> $data_post_relative['fullname'][$i],
									'relationship' 	=> $data_post_relative['relationship'][$i],
									'phone' 		=> $data_post_relative['phone'][$i],
									'address' 		=> $data_post_relative['address'][$i],
									'created_at' 	=> gmdate('Y-m-d H:i:s', time() + 7 * 3600)
								);
							}
						}
						if ($data_post_relative_insert != NULL) {
							$this->EmployeeRelativeModels->adds($data_post_relative_insert);
						}
					}

					//add history work (lịch sử công việc)
					$data_post_work = $this->input->post('data_post_work');
					$data_post_work_insert = array();
					if ($data_post_work != NULL) {
						for ($i = 0; $i < count($data_post_work['date_from']); $i++) {
							if ($data_post_work['date_from'][$i] != '' && $data_post_work['date_to'][$i] != '') {
								//format date
								$date_from = $this->Functions->changFormatDate($data_post_work['date_from'][$i]);
								$date_to = $this->Functions->changFormatDate($data_post_work['date_to'][$i]);
								$data_post_work_insert[] = array(
									'employeeID'		=> $id_insert,
									'date_from' 		=> $date_from,
									'date_to' 			=> $date_to,
									'work_unit' 		=> $data_post_work['work_unit'][$i],
									'bussinesstypeID' 	=> $data_post_work['bussinesstypeID'][$i],
									'position' 			=> $data_post_work['position'][$i],
									'concurrently' 		=> $data_post_work['concurrently'][$i],
									'description' 		=> $data_post_work['description'][$i],
									'created_at' 		=> gmdate('Y-m-d H:i:s', time() + 7 * 3600),
									'updated_at' 		=> gmdate('Y-m-d H:i:s', time() + 7 * 3600)
								);
							}
						}
						if ($data_post_work_insert != NULL) {
							$this->EmployeeWorkModels->adds($data_post_work_insert);
						}
					}

					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> ADD_SUCCESS,
					));
				} else {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> ADD_FAIL,
					));
				}
				redirect(CPANEL . $this->control);
			}
			// employeelevel 

		}
		$getBanks 			= $this->BankModels->findWhere(array('publish' => 1), 'id, name');
		$getStandards 		= $this->StandardModels->findWhere(array('publish' => 1), 'id, name');
		$getBussinesstypes  = $this->BussinesstypeModels->findWhere(array('publish' => 1), 'id, name');
		// tình trạng học vấn
		$edustatus = $this->EdustatusModels->findWhere(array('publish' => 1));
		// trường học 
		$schools = $this->SchoolModels->findWhere(array('publish' => 1));
		// ngành học 
		$majors = $this->MajorsModels->findWhere(array('publish' => 1));
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Thêm mới',
			'template' 			=> 	$this->template . 'add',
			'control'			=>  $this->control,
			// 'code'				=>  $code,
			'banks'				=>  $getBanks,
			'standards'			=>  $getStandards,
			'bussinesstypes'	=>  $getBussinesstypes,
			'edustatus'			=> $edustatus,
			'schools'			=> $schools,
			'majors'			=> $majors
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	//edit
	public function edit($id)
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$dataEdit = $this->EmployeeModels->find($id);
		$data_employeelevel = $this->EmployeelevelModels->findRowWhere(array('employeeID' => $id), '*', 'id asc');
		$dataEdit['salary_current'] = $this->currency_format($dataEdit['salary_current']);
		$dataEdit['salary_desire'] = $this->currency_format($dataEdit['salary_desire']);

		if ($this->input->post()) {

			$data_post = $this->input->post('data_post');
			if ($data_post != NULL) {

				$data_post['salary_current'] = preg_replace('/[,.]/', '', $data_post['salary_current']);
				$data_post['salary_desire'] = preg_replace('/[,.]/', '', $data_post['salary_desire']);
				//upload avatar
				if ($_FILES["avatar"]["name"]) {
					//delete old image
					$file_avatar = $this->path_dir . $dataEdit['code'] . '/' . $dataEdit['avatar'];
					if (file_exists($file_avatar)) {
						unlink($file_avatar);
					}
					//upload new image
					$filename = $_FILES['avatar']['name'];
					$path_dir = $this->path_dir . $data_post['code'] . '/';
					$data_upload = $this->Upload->upload_image($path_dir, $filename, 'avatar', 'avatar', 300);
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['avatar'] = $data_upload['image'];
					}
				} else {
					$data_post['avatar'] = $dataEdit['avatar'];
				}
				//upload ID Front
				if ($_FILES["id-front"]["name"]) {
					//delete old image
					$file_id_front = $this->path_dir . $dataEdit['code'] . '/' . $dataEdit['id-front'];
					if (file_exists($file_id_front)) {
						unlink($file_id_front);
					}
					//upload new image
					$filenameIdFront = $_FILES['id-front']['name'];
					$path_dir = $this->path_dir . $data_post['code'] . '/';
					$data_upload = $this->Upload->upload_image($path_dir, $filenameIdFront, 'id-front', 'id-front');
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['id-front'] = $data_upload['image'];
					}
				} else {
					$data_post['id-front'] = $dataEdit['id-front'];
				}
				//upload ID Back
				if ($_FILES["id-back"]["name"]) {
					//delete old image
					$file_id_back = $this->path_dir . $dataEdit['code'] . '/' . $dataEdit['id-back'];
					if (file_exists($file_id_back)) {
						unlink($file_id_back);
					}
					//upload new image
					$filenameIdBack = $_FILES['id-back']['name'];
					$path_dir = $this->path_dir . $data_post['code'] . '/';
					$data_upload = $this->Upload->upload_image($path_dir, $filenameIdBack, 'id-back', 'id-back');
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['id-back'] = $data_upload['image'];
					}
				} else {
					$data_post['id-back'] = $dataEdit['id-back'];
				}

				//format birthday
				$data_post['birthday'] = $this->Functions->changFormatDate($data_post['birthday']);
				//field use check cmnd/passport
				$data_post['identify_type'] = $this->input->post('identify_type');
				//remove code
				// unset($data_post['code']);
				//update data
				$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$result = $this->EmployeeModels->edit($data_post, $id);


				if ($result > 0) {
					// EMPLOYEE LEVEL
					$data_post_employeelevel = $this->input->post('data_post_employeelevel');
					if ($data_post_employeelevel != NULL) {
						$data_post_employeelevel['updated_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$result = $this->EmployeelevelModels->edit($data_post_employeelevel, $id, 'employeeID');
					}
					//edit standard
					$this->EmployeeStandardModels->deleteWhere('employeeID', $id);
					$data_post_standard = $this->input->post('data_post_standard');
					$data_post_standard_edit = array();
					if ($data_post_standard != NULL) {
						for ($i = 0; $i < count($data_post_standard['date_from']); $i++) {
							if ($data_post_standard['date_from'][$i] != '' && $data_post_standard['date_to'][$i] != '') {
								//format date
								$date_from = $this->Functions->changFormatDate($data_post_standard['date_from'][$i]);
								$date_to = $this->Functions->changFormatDate($data_post_standard['date_to'][$i]);
								$data_post_standard_edit[] = array(
									'employeeID'	=>	$id,
									'date_from' 	=> $date_from,
									'date_to' 		=> $date_to,
									'content' 		=> $data_post_standard['content'][$i],
									'created_at' 	=> gmdate('Y-m-d H:i:s', time() + 7 * 3600)
								);
							}
						}

						if ($data_post_standard_edit != NULL) {
							foreach ($data_post_standard_edit as $key_postStandard => $val_standard) {
								$this->EmployeeStandardModels->add($val_standard);
							}
						}
					}

					// lương & phúc lợi
					$data_post_salary = $this->input->post('data_post_salary');
					if ($data_post_salary !=  NULL) {
						$data_post_salary['salary'] = preg_replace('/[,.]/', '', $data_post_salary['salary']);
						$data_post_salary['salary_basic'] = preg_replace('/[,.]/', '', $data_post_salary['salary_basic']);
						$data_post_salary['updated_at'] 	= gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						$this->EmployeeSalaryModels->edit($data_post_salary, $id, $key = 'employeeID');
					}

					//edit history work
					$this->EmployeeWorkModels->deleteWhere('employeeID', $id);
					$data_post_work = $this->input->post('data_post_work');
					$data_post_work_edit = array();
					if ($data_post_work != NULL) {
						for ($i = 0; $i < count($data_post_work['date_from']); $i++) {
							if ($data_post_work['date_from'][$i] != '' && $data_post_work['date_to'][$i] != '') {
								//format date
								$date_from = $this->Functions->changFormatDate($data_post_work['date_from'][$i]);
								$date_to = $this->Functions->changFormatDate($data_post_work['date_to'][$i]);
								$data_post_work_edit[] = array(
									'employeeID'		=>	$id,
									'date_from' 		=> $date_from,
									'date_to' 			=> $date_to,
									'work_unit' 		=> $data_post_work['work_unit'][$i],
									'bussinesstypeID' 	=> $data_post_work['bussinesstypeID'][$i],
									'position' 			=> $data_post_work['position'][$i],
									'concurrently' 		=> $data_post_work['concurrently'][$i],
									'description' 		=> $data_post_work['description'][$i],
									'created_at' 		=> gmdate('Y-m-d H:i:s', time() + 7 * 3600),
									'updated_at' 		=> gmdate('Y-m-d H:i:s', time() + 7 * 3600)
								);
							}
						}

						if ($data_post_work_edit != NULL) {
							$this->EmployeeWorkModels->adds($data_post_work_edit);
						}
					}

					// bank
					$data_post_bank = $this->input->post('data_post_bank');
					$this->EmployeeBankModels->deleteWhere('employeeID', $id);
					if ($data_post_bank != NULL) {
						$data_post_bank['employeeID'] = $id;
						$data_post_bank['is_default'] = 1;
						$data_post_bank['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
						//var_dump($data_post_bank);die;
						$this->EmployeeBankModels->add($data_post_bank);
					}
					//delete relative info
					$this->EmployeeRelativeModels->deleteWhere('employeeID', $id);
					//add relative info
					$data_post_relative = $this->input->post('data_post_relative');
					$data_post_relative_insert = array();
					if ($data_post_relative != NULL) {
						for ($i = 0; $i < count($data_post_relative['fullname']); $i++) {
							if ($data_post_relative['fullname'][$i] != '') {
								//format date
								$data_post_relative_insert[] = array(
									'employeeID'	=>	$id,
									'fullname' 		=> $data_post_relative['fullname'][$i],
									'relationship' 	=> $data_post_relative['relationship'][$i],
									'phone' 		=> $data_post_relative['phone'][$i],
									'address' 		=> $data_post_relative['address'][$i],
									'created_at' 	=> gmdate('Y-m-d H:i:s', time() + 7 * 3600)
								);
							}
						}
						if ($data_post_relative_insert != NULL) {
							$this->EmployeeRelativeModels->adds($data_post_relative_insert);
						}
					}


					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> ADD_SUCCESS,
					));
				} else {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> ADD_FAIL,
					));
				}
				redirect(CPANEL . $this->control, $data = "");
			}
		}
		// tbl_employee_salary
		$EmployeeSalary = $this->EmployeeSalaryModels->findRowWhere(array('employeeID' => $id), '*');
		$EmployeeSalary['salary'] = $this->currency_format($EmployeeSalary['salary']);
		$EmployeeSalary['salary_basic'] = $this->currency_format($EmployeeSalary['salary_basic']);

		$employees_standards = $this->EmployeeStandardModels->findWhere(array('employeeID' => $id), '*');
		foreach ($employees_standards as $key_change => $val_change) {
			$employees_standards[$key_change]['date_from'] = implode("/", array_reverse(explode("-", $val_change['date_from'])));
			$employees_standards[$key_change]['date_to'] =  implode("/", array_reverse(explode("-", $val_change['date_to'])));
			// $employees_standards[$key_change]['']
		}

		//employees_works
		$employees_works = $this->EmployeeWorkModels->findWhere(array('employeeID' => $id), '*', 'id asc');
		foreach ($employees_works as $key_change => $val_change) {
			$employees_works[$key_change]['date_from'] = implode("/", array_reverse(explode("-", $val_change['date_from'])));
			$employees_works[$key_change]['date_to'] =  implode("/", array_reverse(explode("-", $val_change['date_to'])));
		}

		// ngành học 
		$majors = $this->MajorsModels->findWhere(array('publish' => 1));
		// trình độ
		$getStandards = $this->StandardModels->findWhere(array('publish' => 1), 'id, name');
		// Employee Bank
		$employee_bank = $this->EmployeeBankModels->find($id, '*', 'employeeID');
		//var_dump($employee_bank);die;
		$getBanks = $this->BankModels->findWhere(array('publish' => 1), 'id, name');
		//get data edit
		$getRelatives = $this->EmployeeRelativeModels->findWhere(array('employeeID' => $id), '*');
		//loại hình công ty
		$getBussinesstypes  = $this->BussinesstypeModels->findWhere(array('publish' => 1), 'id, name');
		// tình trạng học vấn
		$edustatus = $this->EdustatusModels->findWhere(array('publish' => 1));
		// trường học 
		$schools = $this->SchoolModels->findWhere(array('publish' => 1));
		$data = array(
			'data_index'	      => $this->get_index(),
			'title'			      => 'Cập nhật',
			'template' 		      => $this->template . 'edit',
			'control'		      => $this->control,
			'dataEdit'		      => $dataEdit,
			'employeesalary' 	  => $EmployeeSalary,
			'employeeBank'        => $employee_bank,
			'banks'				  => $getBanks,
			'relatives'			  => $getRelatives,
			'standards'			  => $getStandards,
			'bussinesstypes'	  =>  $getBussinesstypes,
			'employees_standards' => $employees_standards,
			'employees_works' 	  => $employees_works,
			'edustatus'			=> $edustatus,
			'schools'			  => $schools,
			'majors' 			=> $majors,
			'level' => $data_employeelevel
		);
		// var_dump($employees_works); die;
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	//định dạng tiền (vd: 1000000 => 1.000.000)
	function currency_format($number, $suffix = '')
	{
		if (!empty($number)) {
			return number_format($number, 0, ',', ',') . "{$suffix}";
		}
	}
	// export PDF
	public function export($id)
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$arrID = explode(",", $id);
		$assign_id = [];

		foreach ($arrID as $key_id => $val_id) {
			$dataExport = $this->EmployeeModels->find($val_id);
			$relatives =  $this->EmployeeRelativeModels->findOne(array("employeeID"=>$dataExport['id']));
			
			//xử lý phần hiển thị tên nhân sự, nếu tên nhân sự dài 4 chữ thì xử lý và giữ lại tên và chữ lót kế bên.
			$arrayFullname = explode(' ',$dataExport['fullname']);
			if(count($arrayFullname) > 3){
				$dataExport['fullname'] = $arrayFullname[2].' '.$arrayFullname['3'];
 			}

			$dataExport['code_split'] =  str_split($dataExport['code']);
			$dataExport['code_split'] =  array_reverse($dataExport['code_split']);
			$dataExport['relatives']  = $relatives;
		
			$staff_code = $dataExport['code'];
			$url = base_url() . "cpanel/auths/profile/" . $staff_code;
			$dataExport['qrcode'] =  "http://chart.apis.google.com/chart?cht=qr&chs=300x300&chl=" . $url;
			$assign_id[$key_id] = $dataExport;
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Xuất thẻ',
			'template' 	=> 	$this->template . 'export',
			'control'	=>  $this->control,
			'data'		=> 	$assign_id,
			'path_dir' => $this->path_dir
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	//getDataRow
	public function getDataRow()
	{
		$id = $_POST['id'];
		$dataRow = $this->EmployeeModels->find($id);
		echo json_encode(array('result' => $dataRow));
	}
	// check hiển thị
	public function publish()
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->EmployeeModels->edit($data_update, $id);
		if ($result) {
			echo json_encode(array('result' => 1));
		}
	}
	//delete OT2
	public function delete()
	{
		
		//Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		// processed delete.
		$id = $_POST['id'];
		//delete bank info
		$this->EmployeeBankModels->deleteWhere('employeeID', $id);
		//delete relative info
		$this->EmployeeRelativeModels->deleteWhere('employeeID', $id);
		//delete historywork
		$this->EmployeeWorkModels->deleteWhere('employeeID', $id);
		// employee standard
		$this->EmployeeStandardModels->deleteWhere('employeeID', $id);
		// history work
		$this->EmployeeHistoryWorkModels->deleteWhere('employeeID', $id);
		// employee level
		$this->EmployeelevelModels->deleteWhere('employeeID', $id);
		// employee salary
		$this->EmployeeSalaryModels->deleteWhere('employeeID', $id);
		// employee
		$this->EmployeeModels->delete($id);
	}
	// tạo mã nhân viên
	public function setCode()
	{
		$code = '';
		$code_current_max_array = $this->EmployeeModels->find_row_max('code_hide');
		$code_current_max = (int)$code_current_max_array['code_hide'] + 1;
		switch (strlen($code_current_max)) {
			case 1:
				$code .= '0000' . $code_current_max;
				break;
			case 2:
				$code .= '000' . $code_current_max;
				break;
			case 3:
				$code .= '00' . $code_current_max;
				break;
			case 4:
				$code .= '0' . $code_current_max;
				break;
			default:
				$code .= $code_current_max;
				break;
		}
		return $code;
	}
	
	public function viewEmloyeeInfoDetail()
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$employeeID = $_POST['id'];
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		//kiểm tra giá trị và gán dữ liệu Trình Trạng hôn nhân
		$dataEmployee['marital'] = $this->EmployeeModels->check_marital($dataEmployee['marital']);
		// Vị trí công việc:
		$position_jb = $this->JobpositionModels->findOne(array('id'=>$dataEmployee['job_positionID']));
		// Phòng ban:
		$Ogchart  = $this->OgchartsModels->findOne(array('id'=>$dataEmployee['ogchartID']));
		// phòng ban mới 
		$careerroadmap  = $this->CareerroadmapModels->findOne(array('id'=>$dataEmployee['careerroadsID']));
		// Lịch sử làm việc
		$EmployeeHistoryWork = $this->EmployeeHistoryWorkModels->findOne(array('employeeID'=>$dataEmployee['id']));
		// Văn phòng:
		$Branch  = $this->BranchModels->findOne(array('id'=>$EmployeeHistoryWork['branchID']));
		// phân loại vị trí
		$Classifyemployee = $this->ClassifyemployeeModels->findOne(array('id'=>$EmployeeHistoryWork['classify_employeeID']));
		$data = array(
			'data_index'		=> $this->get_index(),
			'data_employee'		=> 	$dataEmployee,
			'data_position_jb' =>	$position_jb,
			'data_Ogchart' 		=>$Ogchart,
			'careerroadmap'		=> $careerroadmap,
			'data_Branch'		=> $Branch,
			'Classifyemployee' =>$Classifyemployee,
			'path_dir' 			=> $this->path_dir,
		);
	//	echo json_encode($data);die;
		$this->load->view('cpanel/employee/viewEmloyeeInfoDetail', isset($data) ? $data : NULL);
	}


	public function deletetab()
	{
		$this->db->delete($_POST['table'], array('id' => $_POST['id']));
	}
	// xóa file excel sau khi export
	public function delete_export_excel()
	{
		$linkFile  = "public/" . $_POST['filename'];
		unlink($linkFile);
	}
	// export 
	public function export_excel()
	{
		$fileName = "Danh_sach_nhan_su.xlsx";
		$where = NULL;
		$data = $this->EmployeeModels->getAllWhere('*','id desc', $where);

		foreach ($data as $key_temp => $val_temp) {
			// ngân hàng
			$str_bank = "";
			$banks_item = $this->EmployeeBankModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($banks_item as $key_bank_item => $val_bank_item) {
				$banks_name = $this->BankModels->find($val_bank_item['bankID'], 'name', 'id');
				$str_bank .= "-TK: {$banks_name['name']}, Số TK: {$val_bank_item['bank_number']}, Tên: {$val_bank_item['bank_name']}, Chi nhánh: {$val_bank_item['bank_branch']} \n";
			}
			$data[$key_temp]['banks'] = $str_bank;
			// lịch sử làm việc tại công ty
			$str_historywork = "";
			$historywork_item = $this->EmployeeHistoryWorkModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($historywork_item as $key_historywork_item => $val_historywork_item) {
				$position_name = $this->PositionModels->find($val_historywork_item['positionID'], 'name', 'id');
				$branch_name = $this->BranchModels->find($val_historywork_item['branchID'], 'name', 'id');
				$str_historywork .= "-Chức danh: {$position_name['name']}, Chi nhánh: {$branch_name['name']}, Ngày bắt đầu: {$val_historywork_item['startwork']}} \n";
			}
			$data[$key_temp]['historywork'] = $str_historywork;
			// người thân
			$str_relative = "";
			$relative_item = $this->EmployeeRelativeModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($relative_item as $key_relative_item => $val_relative_item) {
				$str_relative .= "-Tên: {$val_relative_item['fullname']}, Sdt: {$val_relative_item['phone']}, Địa chỉ: {$val_relative_item['address']}, Mối quan hệ: {$val_relative_item['relationship']} \n";
			}
			$data[$key_temp]['relative'] = $str_relative;
			// lương
			$str_salary = "";
			$salary_item = $this->EmployeeSalaryModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($salary_item as $key_salary_item => $val_salary_item) {
				$str_salary .= "-Lương: {$val_salary_item['salary']}, Lương cơ bản: {$val_salary_item['salary_basic']} \n";
			}
			$data[$key_temp]['salary_employee'] = $str_salary;
			// tóm tắt quá trình học tập
			$str_standard = "";
			$standard_item = $this->EmployeeStandardModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($standard_item as $key_standard_item => $val_standard_item) {
				$datefrom = implode("/", array_reverse(explode("-", $val_standard_item['date_from'])));
				$dateto = implode("/", array_reverse(explode("-", $val_standard_item['date_to'])));
				$str_standard .= "-Từ ngày: {$datefrom}, Đến ngày: {$dateto}, Nội dung: {$val_standard_item['content']} \n";
			}
			$data[$key_temp]['standard_employee'] = $str_standard;
			// lịch sử làm việc tại các công ty
			$str_work = "";
			$work_item = $this->EmployeeWorkModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($work_item as $key_work_item => $val_work_item) {
				$position_name = $this->PositionModels->find($val_work_item['positionID'], 'name', 'id');
				$branch_name = $this->BranchModels->find($val_work_item['branchID'], 'name', 'id');
				$business_name = $this->BussinesstypeModels->find($val_work_item['bussinesstypeID'], 'name', 'id');
				$datefrom = implode("/", array_reverse(explode("-", $val_work_item['date_from'])));
				$dateto = implode("/", array_reverse(explode("-", $val_work_item['date_to'])));
				$str_work .= "-Từ ngày: {$datefrom}, Đến ngày: {$dateto}, Đơn vị làm việc: {$val_work_item['work_unit']}, Chi nhánh: {$branch_name['name']}, Loại doanh nghiệp: {$business_name['name']}, Chức danh: {$val_work_item['position']}, Kiêm nhiệm: {$val_work_item['concurrently']}, Mô tả: {$val_work_item['description']} \n";
			}
			$data[$key_temp]['work'] = $str_work;
			// trình độ
			$str_level = "";
			$level_item = $this->EmployeelevelModels->findWhere(array("employeeID" => $val_temp['id']));
			foreach ($level_item as $key_level_item => $val_level_item) {
				$edu_status_name = $this->EdustatusModels->find($val_level_item['edu_statusID'], 'name', 'id');
				$standard_name = $this->StandardModels->find($val_level_item['standardID'], 'name', 'id');
				$school_name = $this->SchoolModels->find($val_level_item['schoolsID'], 'name', 'id');
				$majors_name = $this->MajorsModels->find($val_level_item['majorsID'], 'name', 'id');
				$str_level .= "-Tình trạng học vấn: {$edu_status_name['name']}, Trình độ học vấn: {$standard_name['name']}, Trường: {$school_name['name']}, Chuyên ngành: {$majors_name['name']}\n";
			}
			$data[$key_temp]['level'] = $str_level;
			// giới tính
			if ($data[$key_temp]['sex'] == 1) {
				$data[$key_temp]['sex'] = "Nam";
			}
			if ($data[$key_temp]['sex'] == 2) {
				$data[$key_temp]['sex'] = "Nữ";
			}
			if ($data[$key_temp]['sex'] == 3) {
				$data[$key_temp]['sex'] = "Khác";
			}
			// hôn nhân
			if ($data[$key_temp]['marital'] == 1) {
				$data[$key_temp]['marital'] = "Độc thân";
			}
			if ($data[$key_temp]['marital'] == 2) {
				$data[$key_temp]['marital'] = "Đã kết hôn";
			}
			if ($data[$key_temp]['marital'] == 3) {
				$data[$key_temp]['marital'] = "Khác";
			}
			$employeeStandard = $this->StandardModels->findRowWhere(array("id" => $val_temp['standardID']), $fields = 'name');
			$data[$key_temp]['standardID'] = $employeeStandard['name'];
		}

		//Khởi tạo đối tượng
		$excel = new PHPExcel();
		//Chọn trang cần ghi (là số từ 0->n)
		$excel->setActiveSheetIndex(0);
		//Tạo tiêu đề cho trang. (có thể không cần)
		$excel->getActiveSheet()->setTitle('Danh sách nhân sự');

		//Xét chiều rộng cho từng, nếu muốn set height thì dùng setRowHeight()
		$excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true); //1
		$excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true); // 2
		$excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true); // 3

		$excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true); // 4
		$excel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true); // 5
		$excel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true); // 6


		$excel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true); // 7
		$excel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true); // 8
		$excel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true); // 9

		$excel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true); // 10
		$excel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true); // 11
		$excel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true); //12
		$excel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true); //13

		$excel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true); // 14
		$excel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true); // 15
		$excel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true); //16
		$excel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true); //17
		$excel->getActiveSheet()->getColumnDimension('R')->setAutoSize(true); //18
		$excel->getActiveSheet()->getColumnDimension('S')->setAutoSize(true); //19
		//Xét in đậm cho khoảng cột
		$excel->getActiveSheet()->getStyle('A1:S1')->getFont()->setBold(true);
		//Tạo tiêu đề cho từng cột
		//Vị trí có dạng như sau:
		/**
		 * |A1|B1|C1|..|n1|
		 * |A2|B2|C2|..|n1|
		 * |..|..|..|..|..|
		 * |An|Bn|Cn|..|nn|
		 */
		$excel->getActiveSheet()->setCellValue('A1', 'Mã NV');
		$excel->getActiveSheet()->setCellValue('B1', 'Họ tên');
		$excel->getActiveSheet()->setCellValue('C1', 'Giới Tính');
		$excel->getActiveSheet()->setCellValue('D1', 'Hôn nhân');
		$excel->getActiveSheet()->setCellValue('E1', 'Ngày sinh');
		$excel->getActiveSheet()->setCellValue('F1', 'Địa chỉ thường trú');
		$excel->getActiveSheet()->setCellValue('G1', 'Địa chỉ tạm trú');
		$excel->getActiveSheet()->setCellValue('H1', 'Giấy tờ tùy thân CMND/Passport');
		$excel->getActiveSheet()->setCellValue('I1', 'Số điện thoại cá nhân');
		$excel->getActiveSheet()->setCellValue('J1', 'Email');
		$excel->getActiveSheet()->setCellValue('K1', 'Mức lương hiện tại');
		$excel->getActiveSheet()->setCellValue('L1', 'Mức lương mong muốn');

		$excel->getActiveSheet()->setCellValue('M1', 'Trình độ học vấn');

		$excel->getActiveSheet()->setCellValue('N1', 'Ngân hàng');
		$excel->getActiveSheet()->setCellValue('O1', 'Lịch sử làm việc');
		$excel->getActiveSheet()->setCellValue('P1', 'Người thân');
		$excel->getActiveSheet()->setCellValue('Q1', 'Lương');
		$excel->getActiveSheet()->setCellValue('R1', 'Quá trình học tập');
		$excel->getActiveSheet()->setCellValue('S1', 'Quá trình làm việc');
		// thực hiện thêm dữ liệu vào từng ô bằng vòng lặp
		// dòng bắt đầu = 2
		//A	 B	C	D	E	F	G	H	I	J	K	L	M	------- N	O	P	Q	R	S	T	U	V	W	X	Y	Z
		$numRow = 2;
		foreach ($data as $key_data => $val_row) {
			
			
			$excel->getActiveSheet()->setCellValue('A' . $numRow, $val_row['code']);
    
			$excel->getActiveSheet()->setCellValue('B' . $numRow,  $val_row['fullname']);
			$excel->getActiveSheet()->getStyle('B' . $numRow)->getAlignment()->setWrapText(true);
			
			$excel->getActiveSheet()->setCellValue('C' . $numRow,  $val_row['sex']);
			$excel->getActiveSheet()->setCellValue('D' . $numRow,  $val_row['marital']);
			$excel->getActiveSheet()->setCellValue('E' . $numRow, $val_row['birthday']);
			$excel->getActiveSheet()->setCellValue('F' . $numRow,  $val_row['permanent_address']);
			$excel->getActiveSheet()->setCellValue('G' . $numRow,  $val_row['temporary_address']);
			$excel->getActiveSheet()->setCellValue('H' . $numRow,  $val_row['identify_number']);
			$excel->getActiveSheet()->setCellValue('I' . $numRow,  $val_row['phone']);
			$excel->getActiveSheet()->setCellValue('J' . $numRow,  $val_row['email']);
			$excel->getActiveSheet()->setCellValue('K' . $numRow,  $val_row['salary_current']);
			$excel->getActiveSheet()->setCellValue('L' . $numRow,  $val_row['salary_desire']);

			$excel->getActiveSheet()->setCellValue('M' . $numRow, 	$val_row['level']);
			$excel->getActiveSheet()->getStyle('M' . $numRow)->getAlignment()->setWrapText(true);

			$excel->getActiveSheet()->setCellValue('N' . $numRow,  $val_row['banks']);
			$excel->getActiveSheet()->getStyle('N' . $numRow)->getAlignment()->setWrapText(true);

			$excel->getActiveSheet()->setCellValue('O' . $numRow,  $val_row['historywork']);
			$excel->getActiveSheet()->getStyle('O' . $numRow)->getAlignment()->setWrapText(true);

			$excel->getActiveSheet()->setCellValue('P' . $numRow,  $val_row['relative']);
			$excel->getActiveSheet()->getStyle('P' . $numRow)->getAlignment()->setWrapText(true);

			$excel->getActiveSheet()->setCellValue('Q' . $numRow, 	$val_row['salary_employee']);
			$excel->getActiveSheet()->getStyle('Q' . $numRow)->getAlignment()->setWrapText(true);

			$excel->getActiveSheet()->setCellValue('R' . $numRow,  $val_row['standard_employee']);
			$excel->getActiveSheet()->getStyle('R' . $numRow)->getAlignment()->setWrapText(true);

			$excel->getActiveSheet()->setCellValue('S' . $numRow, 	$val_row['work']);
			$excel->getActiveSheet()->getStyle('S' . $numRow)->getAlignment()->setWrapText(true);

			$numRow++;
		}
		// Khởi tạo đối tượng PHPExcel_IOFactory để thực hiện ghi file
		// ở đây mình lưu file dưới dạng excel2007
		PHPExcel_IOFactory::createWriter($excel, 'Excel2007')->save('public/' . $fileName);
		header('Content-type: application/vnd.ms-excel');
		header('Content-Disposition: attachment; filename="' . $fileName . '"');
		echo json_encode(array("filename" => $fileName));
	}
	// search 
	public function search($key_word)
	{
		$type_pageload = $_POST['type_pageload'];
		$page = 1;
		//  urldecode($key_word);
		if (!empty($_GET['key_word']) &&  $_GET['key_word'] != NULL) {
			$key_word = $_GET['key_word'];
		}

		if (!empty($_POST['key_word']) &&  $_POST['key_word'] != "") {
			$key_word = $_POST['key_word'];
		}
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		if ($this->uri->segment(5) != NULL && $this->uri->segment(5) != "") {
			$page = $this->uri->segment(5);
		}
		$my_page = $page;
		$number_button_page = $this->numb_button;
		$config['total_rows'] = $this->EmployeeModels->total_like(NULL, $key_word,'fullname','code','identify_number','phone','email');
		$per_page_default = $this->per_page;
		// ajax chọn số hàng hiển thị
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 5;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		if ($config['total_rows'] > 0) {
			$getDatas =  $this->EmployeeModels->select_array_like('*', NULL, 'status asc,id desc', ($page * $config['per_page']), $config['per_page'], $key_word,
			'fullname','code','identify_number','phone','email');
		}
		foreach ($getDatas as $key_currency => $val_datas) {
			$getDatas[$key_currency]['salary_current'] = $this->currency_format($val_datas['salary_current']);
			$getDatas[$key_currency]['salary'] = $this->EmployeeSalaryModels->findRowWhere(array('employeeID' => $val_datas['id']), '*');
			$getDatas[$key_currency]['salary']['salary'] = $this->currency_format($getDatas[$key_currency]['salary']['salary']);
			$getDatas[$key_currency]['salary']['salary_basic'] = $this->currency_format($getDatas[$key_currency]['salary']['salary_basic']);
			// chi nhánh
			$rs_historywork = $this->EmployeeHistoryWorkModels->findRowWhere(array('employeeID' => $val_datas['id']));

			$rs_branch = $this->BranchModels->findRowWhere(array('id' => $rs_historywork['branchID']));
			$getDatas[$key_currency]['branch_name'] = $rs_branch['name'];
			// Vị trí 
			$rs_position = $this->PositionModels->findRowWhere(array('id' => $rs_historywork['positionID']));
			$getDatas[$key_currency]['position_name'] = $rs_position['name'];
			// hợp đồng
			$rs_contract = $this->ContractModels->findRowWhere(array('employeeID' => $val_datas['id']));

			$type_contract = $this->setTypeContract();
			$getDatas[$key_currency]['contract_name'] =  $type_contract[$rs_contract['type_contractID']];
			// thâm niên
			$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($val_datas['id'] , '*', $field = 'employeeID');
			$getDatas[$key_currency]['startwork_name'] = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
			$getDatas[$key_currency]['startwork']  = $employeeHistoryWork['startwork'];
		}
		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/employee/search', "?key_word=" . $key_word);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'key_word'	=> $key_word,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('cpanel/ajax/employee', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}
	// pagination
	public function pagination($page = 1)
	{
	
		$type_pageload = $_POST['type_pageload'];
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$my_page = $page;
		$number_button_page = $this->numb_button;
		$config['total_rows'] = count($this->EmployeeModels->getAll());
		$per_page_default = $this->per_page;
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		// echo $page ;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->EmployeeModels->select_array_wherein('*', NULL, '', NULL, $order = 'status asc,id desc', ($page * $config['per_page']), $config['per_page']);
		}
		foreach ($getDatas as $key_currency => $val_datas) {
			$getDatas[$key_currency]['salary_current'] = $this->currency_format($val_datas['salary_current']);
			$getDatas[$key_currency]['salary'] = $this->EmployeeSalaryModels->findRowWhere(array('employeeID' => $val_datas['id']), '*');
			$getDatas[$key_currency]['salary']['salary'] = $this->currency_format($getDatas[$key_currency]['salary']['salary']);
			$getDatas[$key_currency]['salary']['salary_basic'] = $this->currency_format($getDatas[$key_currency]['salary']['salary_basic']);
			// chi nhánh
			$rs_historywork = $this->EmployeeHistoryWorkModels->findRowWhere(array('employeeID' => $val_datas['id']));

			$rs_branch = $this->BranchModels->findRowWhere(array('id' => $rs_historywork['branchID']));
			$getDatas[$key_currency]['branch_name'] = $rs_branch['name'];
			// Vị trí 
			$rs_position = $this->PositionModels->findRowWhere(array('id' => $rs_historywork['positionID']));
			$getDatas[$key_currency]['position_name'] = $rs_position['name'];
			// hợp đồng
			$rs_contract = $this->ContractModels->findRowWhere(array('employeeID' => $val_datas['id']));

			$type_contract = $this->setTypeContract();
			$getDatas[$key_currency]['contract_name'] =  $type_contract[$rs_contract['type_contractID']];
			// thâm niên
			$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($val_datas['id'] , '*', $field = 'employeeID');
			$getDatas[$key_currency]['startwork_name'] = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
			$getDatas[$key_currency]['startwork']  = $employeeHistoryWork['startwork'];
		}
		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/employee');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('cpanel/ajax/employee', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}
	// hợp đồng
	public function setTypeContract()
	{
		$arrayData = array(
	        '1'	=>	'HĐ thử việc',
	        '2'	=>	'HĐ không xác định thời hạn',
	        '3'	=>	'HĐ lao động 01 năm',
	        '4'	=>	'HĐ lao động 06 tháng',
	        '4'	=>	'HĐ dịch vụ',
	        '6'	=>	'HĐ lao động thời vụ (trên 3 tháng)',
	        '7'	=>	'HĐ 06 tháng',
		);
		return $arrayData;
	}
}
