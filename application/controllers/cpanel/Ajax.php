<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('BussinesstypeModels');
		$this->load->model('BranchModels');
		$this->load->model('PositionModels');
		$this->load->model('BankModels');
		$this->load->model('EmployeeBankModels');
		$this->load->model('EmployeeStandardModels');
		$this->load->model('EmployeeRelativeModels');
		$this->load->model('EmployeeWorkModels');
		// bảng trình độ học vấn chứa 
		//(EdustatusModels, StandardModels , SchoolModels, MajorsModels, EmployeeModels) 
		$this->load->model('EmployeelevelModels');

		// tình trạng học vấn
		$this->load->model('EdustatusModels');
		// trình độ 
		$this->load->model('StandardModels');
		// trường học
		$this->load->model('SchoolModels');
		// ngành học
		$this->load->model('MajorsModels');
		// nhân sự
		$this->load->model('EmployeeModels');
		// lịch sử làm việc
		$this->load->model('EmployeeHistoryWorkModels');
		// module
		$this->load->model('ModulesModels');
		// chi tiết module
		$this->load->model('ModulesDetailModels');
		// cơ cấu tổ chức (phòng ban)
		$this->load->model('OgchartsModels');
		// vị trí công việc
		$this->load->model('JobpositionModels');
		// phân loại nhân sự
		$this->load->model('ClassifyemployeeModels');
		// lịch làm việc
		$this->load->model('TimeworksModels');
		// lộ trình sự nghiệp
		$this->load->model('CareerroadmapModels');
		 // danh sách ứng tuyển
		 $this->load->model('ApplyModels');
	}
	public function __destruct(){
	}
	// xem thông báo ứng tuyển 
	public function viewApply()
	{
		$id = $_POST['id'];
		$data_update = array("view"=> 1);
		$result = $this->ApplyModels->edit($data_update,$id);
		if($result['type'] == "successful"){
				echo "update success";
		}
	}
	// Duyệt lịch làm việc 
	public function approval_timework()
	{
		$this->load->view('cpanel/ajax/approvaltimework/timework', isset($data)?$data:NULL);	
	}
	//  lịch làm việc nhân viên (ADD)
	public function employee_timework_add()
	{
		$this->load->view('cpanel/ajax/timework/add', isset($data)?$data:NULL);	
	}
	//  lịch làm việc nhân viên (EDIT)
	public function employee_timework_edit()
	{
		$data['uid'] = $_POST['uid'];
		$data['startwork'] = $_POST['startwork'];
		$data['endwork'] = $_POST['endwork'];
	//	$data['timework'] = $this->TimeworksModels->find($uid, '*', 'uid');
		$this->load->view('cpanel/ajax/timework/edit', isset($data)?$data:NULL);	
	}
	// thêm vị trí công việc
	public function add_jobposition()
	{
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$data['datas'] = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		$this->load->view('cpanel/ajax/jobposition/add', isset($data)?$data:NULL);	
	}
	// cập nhật vị trí công việc
	public function edit_jobposition()
	{
		$id = $_POST['id'];
		$data['datas'] = $this->JobpositionModels->find($id, '*', 'id');
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$data['ogchart'] = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		$data['id'] = $id;
		$this->load->view('cpanel/ajax/jobposition/edit', isset($data)?$data:NULL);	
	}
	// thêm phòng ban
	public function addDepartment()
	{
		$id = $_POST['id'];
		$data['datas'] = $this->OgchartsModels->find($id, '*', 'id');
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$data['ogchart'] = $this->OgchartsModels->dequy($orgchart, $count =  0);
		$data['id'] = $id;
		$this->load->view('cpanel/ajax/orgchart/adddepartment', isset($data)?$data:NULL);
	}
	// sửa phòng ban
	public function editDepartment()
	{	
		$id = $_POST['id'];
		$data['datas'] = $this->OgchartsModels->find($id, '*', 'id');
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$data['ogchart'] = $this->OgchartsModels->dequy($orgchart, $count =  0);
		$data['id'] = $id;
		$this->load->view('cpanel/ajax/orgchart/editdepartment', isset($data)?$data:NULL);
	}
	// load form  chỉnh sửa nhóm người dùng
	public function editFormRole()
	{
		$tbl_module = $this->ModulesModels->findWhere(array('publish' => 1));
		foreach($tbl_module as $key_module => $val_module)
		{
			$tbl_module[$key_module]['module_detail'] =	$this->ModulesDetailModels->findWhere(array('parentid'=> $val_module['id']),'*','id asc');
		}
		$data['role']  = $this->RoleModels->findRowWhere(array('id'=> $_POST['id'] ));
		$data['tbl_module'] = $tbl_module;
		$data['id']  =  $_POST['id'];
		$this->load->view('cpanel/role/edit', isset($data)?$data:NULL); // html
	}
	// load form  thêm nhóm người dùng
	public function addFormRole()
	{
		$role = $this->RoleModels->findWhere(array('publish' => 1));
		$tbl_module = $this->ModulesModels->findWhere(array('publish' => 1));
		foreach($tbl_module as $key_module => $val_module)
		{
			$tbl_module[$key_module]['module_detail'] =	$this->ModulesDetailModels->findWhere(array('parentid'=> $val_module['id']),'*','id asc');
		}
		$data['tbl_module'] = $tbl_module;
		$this->load->view('cpanel/role/add', isset($data)?$data:NULL);
	}
	// tài liệu tham chiếu
	public function addFormReferenceMaterial()
	{
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$userID = $this->Auth->logged_id();
		$userRow  = $this->UserModels->find($userID);
		$employeeID = $userRow['employeeID'];
		if(!empty($_POST['employeeID']))
		{
			$employeeID =$_POST['employeeID'];
		}
		$data['employee'] = $this->EmployeeModels->findRowWhere(array('id' => $employeeID), '*', 'id asc');
		$data['employeeID'] = "/".$_POST['employeeID'];
		//echo json_encode(array("test" => $data['Employee']));
		$this->load->view('cpanel/ajax/addFormReferenceMaterial', isset($data)?$data:NULL);
	}
	public function addFormStandard()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$data['employeeID'] = "/".$_POST['employeeID'];
		$this->load->view('cpanel/ajax/addFormStandard', isset($data)?$data:NULL);
	}
	public function addFormHistoryWork()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$getBussinesstypes  = $this->BussinesstypeModels->findWhere(array('publish' => 1), 'id, name');
		$data['employeeID'] = "/".$_POST['employeeID'];
		$data['bussinesstypes'] = $getBussinesstypes;

		$this->load->view('cpanel/ajax/addFormHistoryWork', isset($data)?$data:NULL);
	}
	// cập nhật lịch sử công tác
	public function addFormHistoryWorkUpdate()
	{
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$data['employeeID'] = "/".$_POST['employeeID'];
		$data['bussinesstypes']  = $this->BussinesstypeModels->findWhere(array('publish' => 1));
		$data['EmployeeWork'] = $this->EmployeeWorkModels->findRowWhere(array('id' => $id), '*', 'id asc');
		$data['EmployeeWork']['date_from'] =  date("d/m/Y", strtotime($data['EmployeeWork']['date_from']) );
		$data['EmployeeWork']['date_to'] =  date("d/m/Y", strtotime($data['EmployeeWork']['date_to']));
		$this->load->view('cpanel/ajax/addFormHistoryWorkUpdate', isset($data)?$data:NULL);
	}
	public function addFormProgressLearn()
	{
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$data['employeeID'] = "/".$_POST['employeeID'];
		$this->load->view('cpanel/ajax/addFormProgressLearn', isset($data)?$data:NULL);
	}
	public function addFormProgressLearnUpdate()
	{
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$data['EmployeeStandard'] = $this->EmployeeStandardModels->findRowWhere(array('id' => $id), '*', 'id asc');
		$data['EmployeeStandard']['date_from'] =  date("d/m/Y", strtotime($data['EmployeeStandard']['date_from']) );
		$data['EmployeeStandard']['date_to'] =  date("d/m/Y", strtotime($data['EmployeeStandard']['date_to']));
		$data['employeeID'] = "/".$_POST['employeeID'];
		//echo json_encode(array("test"=>$data));
		$this->load->view('cpanel/ajax/addFormProgressLearnUpdate', isset($data)?$data:NULL);
	}
	public function addFormEmployeelevel()
	{
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$userID = $this->Auth->logged_id();
		$userRow  = $this->UserModels->find($userID);
		$employeeID = $userRow['employeeID'];
		if(!empty($_POST['employeeID']))
		{
			$employeeID =$_POST['employeeID'];
		}
		$data['educstatus'] = $this->EdustatusModels->findWhere(array('publish' => 1));
		$data['standards']  = $this->StandardModels->findWhere(array('publish' => 1));
		$data['school']  = $this->SchoolModels->findWhere(array('publish' => 1));
		$data['majors']  = $this->MajorsModels->findWhere(array('publish' => 1));
		$data['employeeID'] = "/".$_POST['employeeID'];
		$data['employeelevel_ref'] = $this->EmployeelevelModels->findRowWhere(array('employeeID' => $employeeID), '*', 'id asc');
		// echo json_encode(array("test"=>$data ));
		$this->load->view('cpanel/ajax/addFormEmployeelevel', isset($data)?$data:NULL);
	}
	public function addFormInfoRelative()
	{
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$data['employeeID'] = "/".$_POST['employeeID'];
		$this->load->view('cpanel/ajax/addFormInfoRelative', isset($data)?$data:NULL);
	}
	// thêm người thân 
	public function addRelativeInfo()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$data['employeeID'] = "/".$_POST['employeeID'];
		$this->load->view('cpanel/ajax/addRelativeInfo', isset($data)?$data:NULL);
	}
	// cập nhật người thân
	public function addRelativeInfoUpdate()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$data['employeeID'] = "/".$_POST['employeeID'];
		$data['EmployeeRelative'] = $this->EmployeeRelativeModels->findRowWhere(array('id' => $id), '*', 'id asc');
		$this->load->view('cpanel/ajax/addRelativeInfoUpdate', isset($data)?$data:NULL);
	}
	public function loadPosition()
	{
		$id = $_POST['id'];
		$employeeID =  $_POST['employeeID'];
		$data['job_position_titleID'] =  $this->EmployeeModels->findOne(array("id" => $employeeID),'*', 'id desc');
	
		$data['position'] = NULL;
		if($id != "")
		{
			$data['position'] =  $this->JobpositionModels->findWhere(array("parentID" => $id),'*', 'id desc');
		}
	
		//echo json_encode($data['job_position_titleID']);die;
		$this->load->view('cpanel/ajax/position_formwork_info_update', isset($data)?$data:NULL);
	}
	public function addFormWorkInfoUpdate()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$userID = $this->Auth->logged_id();
		$userRow  = $this->UserModels->find($userID);
		$employeeID = $userRow['employeeID'];
		if(!empty($_POST['employeeID']))
		{
			$employeeID =$_POST['employeeID'];
		}
		$data['branch'] = $this->BranchModels->findWhere(array('publish' => 1));
		$data['position'] = $this->PositionModels->findWhere(array('publish' => 1));
		$data['employeeID'] = "/".$_POST['employeeID'];
		$data['main_employeeID'] = $_POST['employeeID'];
		$data['historyWork'] =  $this->EmployeeHistoryWorkModels->find($employeeID , '*', $field = 'employeeID');
		$data['historyWork']['startwork']	= implode("/", array_reverse(explode("-", $data['historyWork']['startwork'])));
		$data['historyWork']['endwork']	= implode("/", array_reverse(explode("-", $data['historyWork']['endwork'])));
		//lấy danh sách phòng ban đổ ra select
		// vị trí công việc
		$data['job_position'] = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		// phòng ban 
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$data['orgchart'] = $this->OgchartsModels->dequy($orgchart, $count =  0);
		// 29/4 : thay đổi phòng ban
		$data_dequy = $this->CareerroadmapModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$data['careerroad'] = $data_dequy != NULL ? $this->CareerroadmapModels->dequy($data_dequy, $count =  0) : "";

		$data['classify_employee'] = $this->ClassifyemployeeModels->findWhere(array("publish" => 1));
		$this->load->view('cpanel/ajax/addFormWorkInfoUpdate', isset($data)?$data:NULL);
	}
	public function addFormProfileUpdate()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$userID = $this->Auth->logged_id();
		$userRow  = $this->UserModels->find($userID);
		$employeeID = $userRow['employeeID'];
		if(!empty($_POST['employeeID']))
		{
			$employeeID =$_POST['employeeID'];
		}
		$data['employeeID'] = "/".$_POST['employeeID'];
		$data['employee'] = $this->EmployeeModels->findRowWhere(array('id' => $employeeID), '*', 'id asc');
		$data['employee']['birthday'] = implode("/",array_reverse(explode("-",$data['employee']['birthday'])));
		//  echo $id;
		$this->load->view('cpanel/ajax/addFormProfileUpdate', isset($data)?$data:NULL);
	}
	// Thêm tài khoản ngân hàng  người
	public function addAccountBankInfo()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$data['banks'] = $this->BankModels->findWhere(array('publish' => 1));
		$data['employeeID'] = "/".$_POST['employeeID'];
		$this->load->view('cpanel/ajax/addAccountBankInfo', isset($data)?$data:NULL);
	}
	// Cập nhật tài khoản ngân hàng người dùng 
	public function updateAccountBankInfo()
	{
		//check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		$data['employeeID'] = "/".$_POST['employeeID'];
		$id = $_POST['id'];
		$data['banks'] = $this->BankModels->findWhere(array('publish' => 1));
		$data['EmployeeBank'] = $this->EmployeeBankModels->findRowWhere(array('id' => $id), '*', 'id asc');
		//echo json_encode(array("bank"=> $data['banks'], "EmployeeBank"=> $data['EmployeeBank'],"id"=>$id ));
		$this->load->view('cpanel/ajax/updateAccountBankInfo', isset($data)?$data:NULL);
	}
}
