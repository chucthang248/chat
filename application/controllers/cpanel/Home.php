<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('EmployeeModels');
		$this->load->model('EmployeeSalaryModels');
		$this->load->model('EmployeeHistoryWorkModels');
	}	
	//Main action
	public function index()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		//push data to view
		$data['total_employee'] = count($this->EmployeeModels->getAll());
		$data['total_salary_basic'] =  $this->EmployeeSalaryModels->total_slary_basic();
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->getAll();
		// /rray(3) { [0]=> int(1102) [1]=> int(33) [2]=> int(5) }
		$totalMedium = 0;
		foreach($employeeHistoryWork as $kay_work => $val_work)
		{
			$totalMedium += 	$this->EmployeeSalaryModels->total_medium_datework($val_work['startwork']);
		}
		$data['total_timework'] = $totalMedium / count($employeeHistoryWork);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Dashboard',
			'template' 	=> 	'cpanel/home/index',
			'data'		=> 	 $data
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
}
