<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Permission extends Admin_Controller
{
    public $template = 'cpanel/permission/';
    public $title = 'Bạn không có quyền truy cập';
    public $control = 'permission';


    public function __construct()
    {
        parent::__construct();
        $this->get_index();
        $this->load->model('OgchartsModels');
        $this->load->model('ModulesModels');
        $this->load->model('JobpositionModels');
        $this->load->model('CareerroadmapModels');
    }

    public function index()
    {
       
        // Check login
        if ($this->Auth->check_logged() === false) {
            redirect(base_url() . 'cpanel/login.html');
        }
        $getOgcharts = $this->recursive_2(0);
        $data = array(
            'data_index'    => $this->get_index(),
            'title'        =>    'Quản lý ' . $this->title,
            'template'     =>     $this->template . 'index',
            'control'    =>  $this->control,
            'ogchartsDiv'  =>  $getOgcharts
        );
        $this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
    }
    public function permission($job_position_titleID)
    {
        // Check login
        if ($this->Auth->check_logged() === false) {
            redirect(base_url() . 'cpanel/login.html');
        }
        if (!$job_position_titleID) {
            redirect(CPANEL . 'permission/');
        }
        $getPermissionDetail = $this->PermissonsModels->getResultArray('controller, action, status', array('job_position_titleID' => $job_position_titleID));
        $modulParent = $this->ModulesModels->findWhere(array("parentID" => 0),'*', 'id asc');
		$module = $this->ModulesModels->dequy($modulParent, $count =  0);
      
        $data = array(
            'data_index'    => $this->get_index(),
            'title'         =>  'Phân quyền ',
            'template'      =>  $this->template . 'permission',
            'control'       =>  $this->control,
            'datas'         =>  $getPermissionDetail,
            'job_position_titleID'     =>  $job_position_titleID,
            'module'        => $module,
        );
        $this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
    }
    public function updatePermission()
    {
        $controller = $_POST['controller'];
        $action     = $_POST['action'];
        $status     = $_POST['status'];
        $job_position_titleID  = $_POST['job_position_titleID'];
        $result = 1;
        if ($status == 1) {
            $dataInsert['controller'] = $controller;
            $dataInsert['action'] = $action;
            $dataInsert['status'] = $status;
            $dataInsert['job_position_titleID'] = $job_position_titleID;
            $resultInsert = $this->PermissonsModels->add($dataInsert);
            if ($resultInsert['type'] != 'successful') {
                $result = 0;
            }
        } else {
            $this->PermissonsModels->deleteWheres(array('job_position_titleID' => $job_position_titleID, 'controller' => $controller, 'action' => $action));
        }
        echo json_encode(array('result' => $result));
    }
    public function recursive_2($parentID, $arrowIcon = '')
    {
        $data = '';
        $getData = $this->CareerroadmapModels->findWhere(array('parentID' => $parentID), '*', 'id desc');
        
        if ($getData != NULL) {
            $arrowIcon .= $parentID > 0 ? '----' : '';
            $data .= '<div class="item">';
            foreach ($getData as $key => $val) {
                $data .= '<div class="title"><div class="__border">' . $arrowIcon . ' ' . $val['name'];
                if ($parentID > 0) {
                    $data .= '<div class="box__button"><a href="' . $this->template . 'permission/' . $val['job_position_titleID'] . '">Phân quyền</a></div>';
                }
                $data .= '</div>';
                $data .= $this->recursive_2($val['id'], $arrowIcon);
                $data .= '</div>';
            }
            $data .= '</div>';
        }
        return $data;
    }


    public function recursive($parentID, $arrowIcon = '')
    {
        $data = '';
        $getData = $this->JobpositionModels->findWhere(array('parentID' => $parentID), 'id,name,parentID', 'id desc');
        if ($getData != NULL) {
            $arrowIcon .= $parentID > 0 ? '----' : '';
            $data .= '<div class="item">';
            foreach ($getData as $key => $val) {
                $data .= '<div class="title"><div class="__border">' . $arrowIcon . ' ' . $val['name'];
                if ($parentID > 0) {
                    $data .= '<div class="box__button"><a href="' . $this->template . 'permission/' . $val['id'] . '">Phân quyền</a></div>';
                }
                $data .= '</div>';
                $data .= $this->recursive($val['id'], $arrowIcon);
                $data .= '</div>';
            }
            $data .= '</div>';
        }
        return $data;
    }

    public function notpermission()
    {
        // Check login
        if ($this->Auth->check_logged() === false) {
            redirect(base_url() . 'cpanel/login.html');
        }
        $data = array(
            'data_index'        => $this->get_index(),
            'title'             =>    'Quản lý ' . $this->title,
            'template'           =>     $this->template . 'notpermission',
            'control'           =>  $this->control,
        );
        $this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
    }
}
