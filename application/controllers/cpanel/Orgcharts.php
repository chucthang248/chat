<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orgcharts extends Admin_Controller
{
	public $template = 'cpanel/orgcharts/';
	public $title = 'Cơ cấu tổ chức';
	public $control = 'orgcharts';
	public $path_url = 'cpanel/orgcharts/';

	public function __construct()
	{
		parent::__construct();
		$this->get_index();
		$this->load->model('OgchartsModels');
		$this->load->model('JobpositionModels');
	}

	public function index()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0), '*', 'id desc');
		$result = $this->OgchartsModels->dequy($orgchart, $count =  0);
		$chart = $this->dequyChart($orgchart, $count = 0);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>   $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas' => $result,
			'charts' => $chart,
			'path_url'  =>   $this->path_url,
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}

	public function dequyChart($data, $count = 0)
	{
		$html  = '<table cellspacing="0" cellpadding="0" class="child_table ">';
		$html  .= '<tbody>';
		if (count($data) > 1) {
			$html  .=	'<tr class="tr_line_top">';
			foreach ($data as $key_data1 => $val_data) {
				$bd__left = "";
					$html  .= '<td>
									<table>
										<tbody>
											<tr>
												<td class=""></td>
												<td class=""></td>
											</tr>
										</tbody>
									</table>
								</td>';
			}
			$html  .=	'</tr>';
		}
		$html  .=	'<tr class="tr_line">';
		foreach ($data as $key_data => $val_data) {
			$bd__left = "";
			if (count($data) >= 2 ||  $count == 1) {
				$bd__left = "bd__left";
			}
			$html  .= '<td>
							<table>
								<tbody>
									<tr >
										<td class="bd__top "></td>
										<td class="' . $bd__left . ' bd__top"> </td>
									</tr>
								</tbody>
							</table>
						</td>';
		}
		$html  .=	'</tr>';
		$html  .= '<tr>';
		foreach ($data as $key_data => $val_data) {
			$childrenMenu = $this->OgchartsModels->findWhere(array('parentID' => $val_data['id']), 'id,name,parentID', 'id desc');
			$line_after = $childrenMenu != NULL ? "line_after" : "";
			$html  .=	'<td>
							<div class="box__td ">
								<div class="box__content ' . $line_after . '">
									' . $val_data["name"] . '
								</div>
							</div>';
			if ($childrenMenu != NULL) {
				$html  .=  $this->dequyChart($childrenMenu, 1);
			}
			$html  .=	'</td>';
		}
		$html  .= '</tr>
					</tbody>
				</table>';
		return $html;
	}
	// thêm
	public function add()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
	
		if ($this->input->post()) {
			$data_post = $this->input->post('data_post');
			$data_post['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
			$result = $this->OgchartsModels->add($data_post);
			if ($result['type'] == 'successful') {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'sucess',
					'message'	=> 'Add module successful!!',
				));
			} else {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> 'Add module error!!',
				));
			}
			redirect($this->template . 'index');
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$data_post = $this->input->post('data_post');
			$duplicate_id = $this->OgchartsModels->find($data_post['parentID'], '*',  'id');
			$id =  $this->input->post('id');
			//var_dump($duplicate_id);die;
			if ($id == $data_post['parentID'] || $duplicate_id['parentID'] == $id) {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> EDIT_FAIL,
				));
				$data_post['parentID'] = $duplicate_id['parentID'];
			}
			$result = $this->OgchartsModels->edit($data_post, $id, 'id');
			if ($result['type'] == 'successful') {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> EDIT_SUCCESS,
				));
			} else {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> EDIT_FAIL,
				));
			}
			redirect($this->template . 'index');
		}
	}
	// check before delete
	public function checkChilditem()
	{
		$id = $_POST['id'];
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => $id), '*', 'id desc');
		if($orgchart == NULL)
		{
			echo json_encode(array("type"=> "no_data"));
		}else
		{
			echo json_encode(array("type"=> "has_data"));
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		// processed delete.
		$id = $_POST['id'];
		$this->dequyDelete($id);
		$this->OgchartsModels->delete($id);
	}

	public function dequyDelete($id)
	{
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => $id), '*', 'id desc');
		$this->OgchartsModels->deleteWhere('parentID',$id);
		if($orgchart != NULL)
		{
			foreach($orgchart as $key => $val)
			{
				$this->dequyDelete($val['id']);
			}
		}
		
	}
	// deleteAjax
	public function deleteAjax()
	{
		$this->OgchartsModels->delete($_POST['id']);
	}

	// creatchart
	public function createchart()
	{
		
		$myArray = array( array('id' => 0,
		'name' => "Cơ cấu tổ chức",
		'parentID' => -1 ));
		
		
	return $myArray;
	}
}
