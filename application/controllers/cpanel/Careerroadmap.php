<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Careerroadmap extends Admin_Controller {
	public $template = 'cpanel/careerroadmap/';
	public $title = 'Lộ trình sự nghiệp';
	public $control = 'careerroadmap';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('CareerroadmapModels');
		$this->load->model('JobpositionModels');
		$this->load->model('OgchartsModels');
	}	
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->CareerroadmapModels->getAll('*','id asc');
		$data_dequy = $this->CareerroadmapModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result = $data_dequy != NULL ? $this->CareerroadmapModels->dequy($data_dequy, $count =  0) : "";
	
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $result,
			
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	//List action - OT2
	public function add()
	{
		// phòng ban
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result_orgchart = $orgchart != NULL ? $this->OgchartsModels->dequy($orgchart, $count =  0) : "";
		// vị trí công việc
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}

		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			$data_post['created_at'] 	 = gmdate('Y-m-d H:i:s', time()+7*3600);
			$data_post['updated_at']	 = gmdate('Y-m-d H:i:s', time()+7*3600);
			$Ogchartsname	= $this->OgchartsModels->find($data_post['orgchartID'],'*', 'id');
			//$data_post['name'] = $Ogchartsname['name'];
			$result = $this->CareerroadmapModels->add($data_post);
			$id = $result['id_insert'];
			//checked
			$data_checked = $this->input->post('data_checked');
			if($data_checked != NULL)
			{
				foreach($data_checked as $key => $val )
				{
					$jobname					= $this->JobpositionModels->find($val['job_positionID'],'*', 'id');
					$val['name'] 				= $jobname['name'];
					$val['parentID'] 			= $id;
					$val['job_position_titleID']= $val['job_positionID'];
					// $val['parent_orgchartID']	= $data_post['orgchartID'];
					$val['created_at'] 			= gmdate('Y-m-d H:i:s', time()+7*3600);
					$val['updated_at']	 		= gmdate('Y-m-d H:i:s', time()+7*3600);
					$this->CareerroadmapModels->add($val);
				}
			}
			//var_dump($data_checked);die;
			if($result['type'] == 'successful'){
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> ADD_SUCCESS,
				));
				
			}else{
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> ADD_FAIL,
				));
			}
				redirect(CPANEL.$this->control);
	
		}
		$data['job_positon'] = $result ;
		$data['orgchart'] = $result_orgchart ;
		$this->load->view($this->template.'/add', isset($data)?$data:NULL);
	}
	public function edit($id)
	{
		// carreer
		$carreer = $this->CareerroadmapModels->find($id,'*', 'id');
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}

		if($this->input->post()){
			if($carreer != NULL)
			{
				$data_post = $this->input->post('data_post');
				$data_post['updated_at']	 = gmdate('Y-m-d H:i:s', time()+7*3600);
				$result = $this->CareerroadmapModels->edit($data_post, $id, 'id');
				// xóa 
				$this->CareerroadmapModels->deleteWhere("parentID",$id);
				//checked
				$data_checked = $this->input->post('data_checked');
				if($data_checked != NULL)
				{
					foreach($data_checked as $key => $val )
					{
						$jobname					= $this->JobpositionModels->find($val['job_positionID'],'*', 'id');
						$val['name'] 				= $jobname['name'];
						$val['parentID'] 			= $id;
						$val['job_position_titleID']= $val['job_positionID'];
						// $val['parent_orgchartID']	= $data_post['orgchartID'];
						$val['created_at'] 			= gmdate('Y-m-d H:i:s', time()+7*3600);
						$val['updated_at']	 		= gmdate('Y-m-d H:i:s', time()+7*3600);
						$this->CareerroadmapModels->add($val);
					}
				}
			}
		
			if($result['type'] == 'successful'){
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> ADD_SUCCESS,
				));
				
			}else{
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> ADD_FAIL,
				));
			}
				redirect(CPANEL.$this->control);
	
		}
		$this->load->view($this->template.'/edit', isset($data)?$data:NULL);
	}
	public function formAdd()
	{
		// phòng ban
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result_orgchart = $orgchart != NULL ? $this->OgchartsModels->dequy($orgchart, $count =  0) : "";
		// vị trí công việc
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data['job_positon'] = $result ;
		$data['orgchart'] = $result_orgchart ;
		$this->load->view($this->template.'/add', isset($data)?$data:NULL);
	}
	public function formEdit()
	{
		$career_job = $this->CareerroadmapModels->findWhere(array("parentID" => $_POST['id']),'*', 'id desc');
		$carreer_name = $this->CareerroadmapModels->find($_POST['id'],'*', 'id');
		// phòng ban
		$orgchart = $this->OgchartsModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result_orgchart = $orgchart != NULL ? $this->OgchartsModels->dequy($orgchart, $count =  0) : "";
		// vị trí công việc
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data['job_positon'] = $result ;
		$data['orgchart'] = $result_orgchart ;
		$data['id']	= $_POST['id'];
		$data['career_job'] = $career_job;
		$data['carreer_name'] = $carreer_name;
		
		$this->load->view($this->template.'/edit', isset($data)?$data:NULL);
	}
	//List action - OT2
	public function form()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			//var_dump($data_post);die;
			if($data_post != NULL){ 
				if($data_post['rowID'] != ''){
					$rowID = $data_post['rowID'];
					$dataRow = $this->CareerroadmapModels->find($rowID);
					$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				}else{
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				}

				if($data_post['rowID'] != ''){
					unset($data_post['rowID']);
					$result = $this->CareerroadmapModels->edit($data_post,$rowID);
				}else{
					unset($data_post['rowID']);
					$result = $this->CareerroadmapModels->add($data_post);
				}
				if($result>0){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> $data_post['rowID']!=''?EDIT_SUCCESS:ADD_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=>  $data_post['rowID']!=''?EDIT_FAIL:ADD_FAIL,
					));
				}
				redirect(CPANEL.$this->control);
			}
		}
		$this->load->view($this->template.'/form', isset($data)?$data:NULL);
	}
	public function getDataRow()
	{
		
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$dataRow = $this->CareerroadmapModels->find($id);
		
		echo json_encode(array('dataRow' => $dataRow));
	}
	
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->CareerroadmapModels->delete($id);
	}
	public function setEducationType($key)
	{
		$arrayData = array(
	        '1' => 'Trung cấp',
            '2' => 'Trung cấp/Cao đẳng',
            '3' => 'Cao đẳng',
            '4' => 'Cao đẳng/Đại học',
            '4' => 'Đại học',
            '6' => 'Đại học/Thạc sĩ',
		);
		return $arrayData[$key];
	}
}
