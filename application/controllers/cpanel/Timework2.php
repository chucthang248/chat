<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Timework extends Admin_Controller {
	public $template = 'cpanel/timework/';
	public $title = 'Lịch làm việc';
	public $control = 'timework';
    public $path_url = 'cpanel/timework/';
	public $type = "employee";
	public function __construct(){
		parent::__construct();
        $this->load->model('ModulesModels');
        $this->load->model('ModulesDetailModels');
		$this->load->model('TimeworksModels');
		$this->load->model('TimeworksHistoryModels');
	}	
	// Tổng quát:
	// - tbl_timework : 
	//          chưa duyệt =>  vàng , 
	//          hủy       => đỏ ,
	//          duyệt     => xanh lá cây
	// 	Thao tác:
	// 	- khi user chọn khung giờ thì dữ liệu sẽ được lưu vào database và column type = "employee", và column className = "" (rỗng)
	//	  nếu className = "" thì bên admin duyệt sẽ không thấy
	//  - khi user nhân "Lưu" thì các khung giờ đã chọn sẽ được cập nhật lại trong column className = "wait_approval"  ,
	//	  chỉ các className != "" thì bên admin mới hiển thị 
	//=====================================================================
	//			 + lưu giá trị hiện tại (vừa tạo hoặc sau khi update )
	//           + cột approval : check trạng thái phê duyệt 
	// 			 + cột shift :  ca làm việc (chưa dùng tới) , approval(trạng thái phê duyệt của admin)
	//			 + cột startwork, endwork :  để selected html
	//			 + cột title : hiển thị khung giờ làm việc trên lịch
	//           + cột start : xác định ngày tháng năm để load dữ liệu lên fullcalendar
	//           + cột className : background thay đổi theo trạng thái khi admin duyệt 
	//			   approval == 2 => Đang chờ duyệt
	// 			   approval == 1 => duyệt
	//			   approval == 0 => Chưa duyệt   )
	// - tbl_timework_history:
	// 			 + cột timeworkID : id của tbl_timework
	//           + lưu cả giá trị tạo và update

	// =========================================
	// 1 : chọn không lưu => type = "employee" , className = "", approval = -1	(không có gì)
	// 2 : chọn xong lưu =>  type = "employee" , className = "wait_approval", approval = 2 (đang chờ)
	// =========================================
	// 1 : lưu ajax 			=> 'approval'		=> -1, 
	// 2 : nhấn lưu 			=> 'approval'		=> 	0, đang chờ (vàng)
	// 3 : admin duyệt 			=> 'approval'		=> 	1, đã duyệt (xanh)
	// 3 : admin không duyệt 	=> 'approval'		=> 	2, hủy (đỏ)
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
            'control'	=>  $this->control,
            'path_url'  =>   $this->path_url
        );
        $data['datas'] = $this->ModulesModels->getAll();
        // Lấy danh sách ngày trong tuần hiện tại
        $arrayDayInWeek = $this->week_from_monday('04-03-2021');
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function addTimeWork()
	{
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);

		$uid 	= $_POST['uid'];
		$startwork 	= $_POST['startwork'];
		$endwork 	= $_POST['endwork'];
		$title = $_POST['title'];
		$start = $_POST['start'];
		$data = array(
			"uid"=> $uid ,
			"employeeID" 		=> $userRow['employeeID'],
			"startwork" 		=> $startwork ,
			"endwork"			=> $endwork ,
			"title"				=> $title,
			"start"				=> $start ,
			"type"				=> $this->type,
			'approval'		=> -1,  
			'created_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600),
		);
		$result = $this->TimeworksModels->add($data);
		$id_insert = $result['id_insert'];
		// bảng lịch sử tạo , cập nhật lịch làm việc
		$data_history = array(
			"uid"=> $uid ,
			"timeworkID" 		=> $id_insert,
			"startwork"			=> $startwork ,
			"endwork"			=> $endwork ,
			"title"				=> $title,
			"start"				=> $start ,
			"type"				=>  $this->type,
			'approval'		=> -1,  
			'created_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600),
		);
		 $this->TimeworksHistoryModels->add($data_history);

		// thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
		$data['listTimeWork'] = $this->TimeworksModels->findWhere(array('employeeID'=> $userRow['employeeID']), 
		'tbl_timework.uid as id , 
		tbl_timework.title , 
		tbl_timework.start, tbl_timework.className','');
		echo json_encode($data);

	}	
	public function editTimeWork()
	{
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);

		$uid 	= $_POST['uid'];
		$startwork 	= $_POST['startwork'];
		$endwork 	= $_POST['endwork'];
		$title = $_POST['title'];
		$data = array(
			"uid"=> $uid ,
			"employeeID"   		=> $userRow['employeeID'],
			"startwork"			=> $startwork ,
			"endwork"			=> $endwork ,
			"title"				=> $title,
			"type"				=>  $this->type,
			'approval'		=> -1,  
			'className'		=> 'defaultColor',
			'updated_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600),
		);
		$this->TimeworksModels->edit($data,$uid,'uid');
		$row_timework = $this->TimeworksModels->find($uid, 'id,start', 'uid');
		// bảng lịch sử tạo , cập nhật lịch làm việc
		$data_history = array(
			"uid"=> $uid ,
			"timeworkID" 	=> $row_timework['id'],
			"startwork"		=> $startwork ,
			"endwork"		=> $endwork ,
			"title"			=> $title,
			"start"			=> $row_timework['start'] ,
			"type"				=>  $this->type,
			'approval'		=> -1,  
			'className'		=> 'defaultColor',
			'created_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600),
		);
		 $this->TimeworksHistoryModels->add($data_history);
		// thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
		$data['listTimeWork'] = $this->TimeworksModels->find($uid, 'tbl_timework.uid as id , 
		tbl_timework.title , 
		tbl_timework.start, tbl_timework.className', 'uid');
		echo json_encode($data);
	}
	// lưu lại trạng thái cho admin duyệt
	public function saveStatus()
	{
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$data = array(
			'approval'		=> 2,
		    "className" 	=> "wait_approval",
		);
		$this->TimeworksModels->edit_WhereMultiple($data,array("employeeID"=> $userRow['employeeID'], "type"=>"employee"));
		$data = $this->TimeworksModels->findWhere(array("employeeID"=> $userRow['employeeID']),'tbl_timework.uid as id , 
		tbl_timework.title , 
		tbl_timework.start, tbl_timework.className');
		echo json_encode($data);
	}
	public function deleteItemTimework()
	{
		$uid 	= $_POST['uid'];
	   $this->TimeworksModels->deleteWhere('uid',$uid);
	 	$this->TimeworksHistoryModels->deleteWhere('uid',$uid);
	}
	public function loadEvents()
	{
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		// thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
		$data['listTimeWork'] = $this->TimeworksModels->findWhere(array('employeeID'=> $userRow['employeeID']), 
		'tbl_timework.uid as id , 
		tbl_timework.title ,tbl_timework.employeeID ,tbl_timework.startwork,tbl_timework.endwork,
		tbl_timework.start, tbl_timework.className','');
		echo json_encode($data);
	}
	//List action - OT2
	public function listForHR()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'listForHR',
            'control'	=>  $this->control,
            'path_url'  =>   $this->path_url
        );
        $data['datas'] = $this->ModulesModels->getAll();
        // Lấy danh sách ngày trong tuần hiện tại
        $arrayDayInWeek = $this->week_from_monday('04-03-2021');
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	// Function lấy danh sách ngày trong tuần hiện tại
	function week_from_monday($date) {
	    // Assuming $date is in format DD-MM-YYYY
	    list($day, $month, $year) = explode("-", $date);

	    // Get the weekday of the given date
	    $wkday = date('l',mktime('0','0','0', $month, $day, $year));

	    switch($wkday) {
	        case 'Monday': $numDaysToMon = 0; break;
	        case 'Tuesday': $numDaysToMon = 1; break;
	        case 'Wednesday': $numDaysToMon = 2; break;
	        case 'Thursday': $numDaysToMon = 3; break;
	        case 'Friday': $numDaysToMon = 4; break;
	        case 'Saturday': $numDaysToMon = 5; break;
	        case 'Sunday': $numDaysToMon = 6; break;   
	    }

	    // Timestamp of the monday for that week
	    $monday = mktime('0','0','0', $month, $day-$numDaysToMon, $year);

	    $seconds_in_a_day = 86400;

	    // Get date for 7 days from Monday (inclusive)
	    for($i=0; $i<7; $i++)
	    {
	        $dates[$i] = date('Y-m-d',$monday+($seconds_in_a_day*$i));
	    }
	    return $dates;
	}


}
