<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Document extends Admin_Controller
{
	public $template = 'cpanel/document/';
	public $title = 'tài liệu';
	public $control = 'document';
	public $path_dir = 'upload/document/';
	public $perpage = 20;
	public $numb_button = 3;
	public function __construct()
	{
		parent::__construct();
		$this->get_index();
		$this->load->library('Excel');
		$this->load->model('UserModels');
		$this->load->model('EmployeeModels');
		// FILE
		$this->load->helper("file");
	}

	// index
	public function index()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$page = 1;
		$where = array('publish' => 1);
		$config['total_rows'] = count($this->EmployeeModels->getAll());
		$config['per_page'] = $this->perpage;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = 1;
		$number_button_page =$this->numb_button;
		$page = ($page > $total_page) ? $total_page : $page;

		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->EmployeeModels->select_array_wherein('*', NULL, '', NULL, $order = 'status asc,id desc', ($page * $config['per_page']), $config['per_page']);
		}
		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/employee');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'datas'		=>  $getDatas,
			'count'		=> $config['total_rows'],
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}

	//getDataRow
	public function getDataRow()
	{
		$id = $_POST['id'];
		$dataRow = $this->EmployeeModels->find($id);
		echo json_encode(array('result' => $dataRow));
	}

	// pagination
	public function pagination($page = 1)
	{
		$type_pageload = $_POST['type_pageload'];
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$my_page = $page;
		$number_button_page = $this->numb_button;
		$config['total_rows'] = count($this->EmployeeModels->getAll());
		$per_page_default =  $this->perpage;
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		// echo $page ;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->EmployeeModels->select_array_wherein('*', NULL, '', NULL, $order = 'id desc', ($page * $config['per_page']), $config['per_page']);
		}

		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/employee');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('cpanel/ajax/document/index', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}

	// search 
	public function search($key_word)
	{
		$type_pageload = $_POST['type_pageload'];
		$page = 1;
		if (!empty($_GET['key_word']) &&  $_GET['key_word'] != NULL) {
			$key_word = $_GET['key_word'];
		}

		if (!empty($_POST['key_word']) &&  $_POST['key_word'] != "") {
			$key_word = $_POST['key_word'];
		}
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		if ($this->uri->segment(5) != NULL && $this->uri->segment(5) != "") {
			$page = $this->uri->segment(5);
		}
		$my_page = $page;
		$number_button_page = $this->numb_button;
		$config['total_rows'] = $this->EmployeeModels->total_like(NULL, $key_word);
		$per_page_default =  $this->perpage;
		// ajax chọn số hàng hiển thị
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 5;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		if ($config['total_rows'] > 0) {
			$getDatas =  $this->EmployeeModels->select_array_like('*', NULL, 'id desc', ($page * $config['per_page']), $config['per_page'], $key_word);
		}

		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '', "?key_word=" . $key_word);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'key_word'	=> $key_word,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('cpanel/ajax/document/index', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}
}
