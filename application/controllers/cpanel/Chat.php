<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Chat extends Admin_Controller {
	public $template = 'cpanel/chat/';
	public $title = 'Tin nhắn';
	public $control = 'chat';
    public $per_page = 10;
	public $numb_button = 3;
	public function __construct(){
		parent::__construct();
		$this->get_index();
        $this->load->model(['EmployeeModels','ChatModels','UserModels']);
        // 1 : search 
        //      - search xổ ra nhân viên ...
        //      - click vào nhân viên A: 2 trường hợp xảy ra
        //                   - 1 : Nếu người dùng A đăng nhập
        //                             + gửi tin nhắn cho nhân viên B => dữ liệu thêm database , dữ liệu gửi tới SERVER 
        //                             + SERVER emit tới client (nhân viên B)
        //                             + Nhân viên B emit tới SERVER
        //                             + SERVER emit tới client (người dùng A)
        //                   - 2 : Nếu người dùng không đăng nhập
        //                  
     
		// khi chọn nhân viên => nạp vào danh sách (nạp trên cùng)
		// khi tin nhắn tới   => nếu có trong danh sách thì sẽ được active , ngược lại thì nạp vào danh sách (nạp trên cùng)
		// khi tin nhắn mới (đã được xem) => hủy active tin nhắn mới nằm dưới cùng
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Quản lý '. $this->title,
			'template' 			=> 	$this->template.'index',
			'control'			=>  $this->control,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

    // search 
	public function search()
	{
		$page = 1;
		//  urldecode($key_word);
		if (!empty($_GET['key_word']) &&  $_GET['key_word'] != NULL) {
			$key_word = $_GET['key_word'];
		}

		if (!empty($_POST['key_word']) &&  $_POST['key_word'] != "") {
			$key_word = $_POST['key_word'];
		}
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$my_page                      = $page;
		$number_button_page           = $this->numb_button;
		$config['total_rows']         = $this->EmployeeModels->total_like(NULL, $key_word,'fullname','email');
		$per_page_default             = $this->per_page;
		
		$config['per_page']           = $per_page_default;
		$config['uri_segment']        = 5;
		$config['reuse_query_string'] = true;
		$total_page                   = ceil($config['total_rows'] / $config['per_page']);
		$page                         = ($page > $total_page) ? $total_page : $page;
		$page                         = ($page < 1) ? 1 : $page;
		$page                         = $page - 1;

		if ($config['total_rows'] > 0) {
            $getDatas                 =  $this->EmployeeModels->search_like_join('tbl_employees.*, tbl_user.id as userID, tbl_user.employeeID', NULL
										,$key_word , 'tbl_employees.fullname ', 'tbl_employees.email', NULL,  // key + column search
										'tbl_user','tbl_user.employeeID = tbl_employees.id','right',	// join 
										($page * $config['per_page']), $config['per_page']
										);
		}
		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '', "");
		$data = array(
			'data_index'	          => $this->get_index(),
			'title'		              =>	'Quản lý ' . $this->title,
			'template' 	              => 	$this->template . 'index',
			'control'	              =>  $this->control,
			'datas'		              =>  $getDatas,
			'key_word'	              => $key_word,
			'per_page'                => $config['per_page'],
			'my_pagination'           => $my_pagination,
			'count'		              => $config['total_rows']
		);
		$this->load->view('cpanel/chat/findChat', isset($data) ? $data : NULL);
		
	}

	// chọn messenger chat 
	public function getChatList()
	{
		$employeeID              = $_POST['employeeID'];
		$user_receive_id         = $_POST['user_receive_id'];
		$user_send_id            = $this->Auth->logged_id();
		$employee_info           = $this->EmployeeModels->findRowWhere(array("id" => $employeeID));
		$userdata                = $this->UserModels->findRowWhere(array("employeeID" => $employee_info['id']));
		$employee_info['userID'] = $userdata['id'];
		$Messenger 		         = $this->ChatModels->loadMessenger($user_send_id,$user_receive_id);
		// $data['Messenger']       = $Messenger;
		// $data['data_listchat']   = $employee_info;
		// $this->load->view('cpanel/chat/ajaxMesenger', isset($data)?$data:NULL);

		$data['Messenger']  	 =  $Messenger;
		$data['User'] 			 = $employee_info;
		$this->load->view('cpanel/chat/loadMessenger', isset($data)?$data:NULL);
	}

	// click lấy thông tin 
	public function clickGetInfo()
	{
		$employeeID = $_POST['employeeID'];
		$user_receive_id = $_POST['user_receive_id'];
		$user_send_id = $this->Auth->logged_id();
		
		$employee =  $this->EmployeeModels->findRowWhere(array('id'=>$employeeID));
		$userdata =  $this->UserModels->findRowWhere(array("employeeID" => $employee['id']));
		$employee['user_receive_id'] = $user_receive_id;
		$employee['user_send_id'] = $user_send_id;
		$employee['userID'] = $userdata['id'];
		$data['employee'] = $employee;
		//echo json_encode($data);
		$this->load->view('cpanel/chat/chatlist', isset($data)?$data:NULL);
	}
	// load danh sách chat
	public function loadChatlist()
	{
		$data['datas'] = $_POST['lischat'];
		$this->load->view('cpanel/chat/loadChatlist', isset($data)?$data:NULL);
	}
	// looad tin nhắn 
	public function loadMessenger()
	{
		$data['Messenger']  = $_POST['mess'];
		$data['User'] 		= $_POST['user'];
		$this->load->view('cpanel/chat/loadMessenger', isset($data)?$data:NULL);
	}
}
