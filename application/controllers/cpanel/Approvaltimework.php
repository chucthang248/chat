<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approvaltimework extends Admin_Controller {
	public $template = 'cpanel/approvaltimework/';
	public $title = 'danh sách lịch làm việc';
	public $control = 'approvaltimework';
    public $perpage = 10;
	public $numb_button = 3;
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model(['EmployeeModels','TimeworksModels','TimeworksHistoryModels']);
	}
	// index
	public function index()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		// check quyền xóa
		$page = 1; 
        $data_employee = $this->EmployeeModels->select_array_wherein_join('DISTINCT(tbl_employees.id),tbl_employees.fullname', NULL, '', NULL,'status asc,id desc', 
        NULL,NULL,
        'tbl_timework','tbl_employees.id = tbl_timework.employeeID','inner');
		$config['total_rows'] = count($data_employee);
		$config['per_page'] = $this->perpage;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = 1;
		$number_page = $this->numb_button;
		$page = ($page > $total_page) ? $total_page : $page;

		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
   
		if ($config['total_rows'] > 0) {
            $getDatas = $this->EmployeeModels->select_array_wherein_join('DISTINCT(tbl_employees.id),tbl_employees.code,tbl_employees.fullname', NULL, '', NULL,'status asc,id desc', 
            ($page * $config['per_page']), $config['per_page'],
             'tbl_timework','tbl_employees.id = tbl_timework.employeeID','inner');
        }
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/approvaltimework');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'datas'		=>  $getDatas,
			'count'		=> $config['total_rows'],
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
    // search 
	public function search($key_word)
	{
		$type_pageload = $_POST['type_pageload'];
		$page = 1;
		//  urldecode($key_word);
		if (!empty($_POST['key_word']) &&  $_POST['key_word'] != "") {
			$key_word = $_POST['key_word'];
		}
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		if ($this->uri->segment(5) != NULL && $this->uri->segment(5) != "") {
			$page = $this->uri->segment(5);
		}
		$my_page = $page;
		$number_page = $this->numb_button;
		$data_search = $this->EmployeeModels->search_like_join('DISTINCT(tbl_timework.employeeID),tbl_employees.code,tbl_employees.fullname',$where = NULL
		,$key_word, $column_1 =  'code', $column_2 =  'fullname', $column_3 = NULL,  // key + column search
		'tbl_timework','tbl_employees.id = tbl_timework.employeeID','inner');
		//total_rows
		$config['total_rows'] = count($data_search);
		$per_page_default = $this->perpage;
		// ajax chọn số hàng hiển thị
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 5;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		if ($config['total_rows'] > 0) {
				$getDatas = $this->EmployeeModels->select_array_wherein('id,fullname', NULL, '', NULL, $order = 'id desc', ($page * $config['per_page']), $config['per_page']);
			if (!empty($_POST['key_word']) &&  $_POST['key_word'] != "") {
				$getDatas = 	$this->EmployeeModels->search_like_join('DISTINCT(tbl_timework.employeeID),tbl_employees.id,tbl_employees.code,tbl_employees.fullname',
					$where = NULL
					,$key_word, $column_1 =  'code', $column_2 =  'fullname', $column_3 = NULL,  // key + column search
					'tbl_timework','tbl_employees.id = tbl_timework.employeeID','inner',
					 ($page * $config['per_page']), $config['per_page'] 
				);
			}
			
		}
	
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/approvaltimework/search', "?key_word=" . $key_word);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'key_word'	=> $key_word,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('cpanel/ajax/approvaltimework/timework', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}
	// pagination
	public function pagination($page = 1)
	{
		$type_pageload = $_POST['type_pageload'];
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$my_page = $page;
		$number_page = $this->numb_button;
		$data_employee = $this->EmployeeModels->select_array_wherein_join('DISTINCT(tbl_employees.id),tbl_employees.code,tbl_employees.fullname', NULL, '', NULL,'status asc,id desc', 
        NULL,NULL,
        'tbl_timework','tbl_employees.id = tbl_timework.employeeID','inner');
		$config['total_rows'] = count($data_employee);
		$per_page_default =$this->perpage;
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		if ($config['total_rows'] > 0) {
			$getDatas = 	$this->EmployeeModels->select_array_wherein_join('DISTINCT(tbl_employees.id),tbl_employees.code,tbl_employees.fullname', NULL, '', NULL,'status asc,id desc', 
			($page * $config['per_page']), $config['per_page'],
        	'tbl_timework','tbl_employees.id = tbl_timework.employeeID','inner');
		}
		
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/approvaltimework');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('cpanel/ajax/approvaltimework/timework', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}
    // approval timework
    public function approvalTimework()
    {
        $uid        = $_POST['uid'];
        $employeeID = $_POST['employeeID'];
        $className = $_POST['className'];
        $type       = $_POST['type'];
        $approval = $_POST['approval'];

        $data = array(
            "approval"  => $approval,
			"className" =>  $className,
            "type"      => $type
		);
		$this->TimeworksModels->edit($data,$uid,'uid');
        $this->TimeworksHistoryModels->edit($data,$uid,'uid');

		// thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
		$datas['listTimeWork'] = $this->TimeworksModels->findWhere(array('employeeID'=> $employeeID), 
		'tbl_timework.uid as id , 
		tbl_timework.title ,tbl_timework.employeeID ,tbl_timework.startwork,tbl_timework.endwork,
		tbl_timework.start, tbl_timework.className','');
		echo json_encode($datas);
    }
    // load timework
    public function loadTimework()
    {
        $id = $_POST['id'];
		// thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
		$data['listTimeWork'] = $this->TimeworksModels->findWhere(array('employeeID'=> $id,'approval !=' => -1), 
		'tbl_timework.uid as id , 
		tbl_timework.title ,tbl_timework.employeeID ,tbl_timework.startwork,tbl_timework.endwork,
		tbl_timework.start, tbl_timework.className','');
		echo json_encode($data);
    }

    public function getDataRow()
	{
		$id = $_POST['id'];
		$dataRow = $this->TimeworksModels->find($id);
		echo json_encode(array('result' => $dataRow));
	}
    public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->TimeworksModels->edit($data_update,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->TimeworksModels->delete($id);
	}
}
