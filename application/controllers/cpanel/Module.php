<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends Admin_Controller {
	public $template = 'cpanel/module/';
	public $title = 'Module';
	public $control = 'module';
    public $path_url = 'cpanel/module/';
	
	public function __construct(){
		parent::__construct();
        $this->get_index();
        $this->load->model('ModulesModels');
        $this->load->model('ModulesDetailModels');
	}
	//List action - OT2
	public function index()
	{
        $modulParent = $this->ModulesModels->findWhere(array("parentID" => 0),'*', 'id desc, length(name)');
		$module = $this->ModulesModels->dequy($modulParent, $count =  0);
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
            'control'	=>  $this->control,
            'path_url'  =>   $this->path_url,
        );
        $data['datas'] = $module;
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function add()
	{

        // Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
       
        if($this->input->post()){
            $data_post = $this->input->post('data_post');
            //var_dump($data_post);die;
            $data_insert = array(
                'parentID'      =>  isset($data_post['parentID']) ? $data_post['parentID'] : 0 ,
                'publish'       => 	$data_post['publish'],
                'name' 			=> 	$data_post['name'],
                'link' 			=> 	$data_post['link'],
                'controller' 		    =>  $data_post['controller'],
                'created_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600),
                'updated_at'    =>	gmdate('Y-m-d H:i:s', time()+7*3600)
            );
            $result = $this->ModulesModels->add($data_insert);
            if($result['type'] == 'successful')
            {
                 foreach($data_post['add_action'] as $key => $val)
                 {
                    $insert_ModulesDetail = array(
                        'name' 			=> 	$val['name_action'],
                        'sort' 			=> 	$val['sort'],
                        'action' 		=>  $val['action'],
                        'moduleID' 		=>   $result['id_insert'],
                        'created_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600),
                        'updated_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600)
                    );
                    $result_ModulesDetail = $this->ModulesDetailModels->add($insert_ModulesDetail);
                 }   

                 if($result_ModulesDetail['type'] == 'successful'){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'sucess',
						'message'	=> 'Add module successful!!',
					));
					redirect('cpanel/module/index');
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> 'Add module error!!',
					));
					redirect('cpanel/module/index');
				}
            }
		}
        $modulParent = $this->ModulesModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$module = $this->ModulesModels->dequy($modulParent, $count =  0);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thêm mới',
			'template' 	=> 	$this->template.'add',
			'control'	=>  $this->control,
            'module'    => $module
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	

	public function edit($id = 0)
	{
        // Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
        if($this->input->post()){
            $data_post = $this->input->post('data_post');
            $duplicate_id = $this->ModulesModels->find($data_post['parentID'], '*',  'id');
            $this->ModulesDetailModels->deleteWhere('moduleID',$id);// delete parentid ModulesDetail
            // Check nếu chọn lại folder chính nó
            if ($id == $data_post['parentID'] || $duplicate_id['parentID'] == $id) {
				$data_post['parentID'] = $duplicate_id['parentID'];
			}
            $data_update = array(
                'name' 			=> 	$data_post['name'],
                'link' 			=> 	$data_post['link'],
                'parentID'      =>  $data_post['parentID'] ,
                'controller' 		    =>  $data_post['controller'],
                'created_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600),
                'updated_at'    =>	gmdate('Y-m-d H:i:s', time()+7*3600)
            );
            $result = $this->ModulesModels->edit($data_update,$id,'id');
            if($result['type'] == 'successful')
            {
                foreach($data_post['add_action'] as $key => $val)
                {
                   $insert_ModulesDetail = array(
                       'name' 			=> 	$val['name_action'],
                       'sort' 			=> 	$val['sort'],
                       'action' 		=>  $val['action'],
                       'moduleID' 		=>  $id,
                       'created_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600),
                       'updated_at'	=>	gmdate('Y-m-d H:i:s', time()+7*3600)
                   );
                   $result_ModulesDetail = $this->ModulesDetailModels->add($insert_ModulesDetail);
                }   
                if($result['type'] == 'successful'){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> ADD_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> ADD_FAIL,
					));
				}
                redirect(CPANEL.$this->control);
            }
		}
        $modulParent = $this->ModulesModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$module = $this->ModulesModels->dequy($modulParent, $count =  0);
        $data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Cập nhật',
			'template' 	=> 	$this->template.'edit',
			'control'	=>  $this->control,
            'module'    => $module
        );
        $data['Modules'] =  $this->ModulesModels->find($id,'*', $field = 'id');
        $data['ModulesDetail'] =  $this->ModulesDetailModels->findWhere($where = array('moduleID'=>$id), '*','sort asc');
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
        $this->ModulesModels->delete($id);
        $this->ModulesDetailModels->delete($id);
	}
    
    // deleteAjax
    public function deleteAjax()
    {
        $this->ModulesDetailModels->delete($_POST['id']);
    }
}
