<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auths extends Admin_Controller
{
	public $template = 'cpanel/auth/';
	public $path_dir = 'upload/employee/';

	public function __construct()
	{
		parent::__construct();
		$this->get_index();
		$this->load->model('Upload');
		$this->load->model('Auth');
		$this->load->model('UserModels');
		$this->load->model('EmployeeModels');
		$this->load->model('EmployeeBankModels');
		// người thân (nhân viên)
		$this->load->model('EmployeeRelativeModels');
		// việc làm (nhân viên)
		$this->load->model('EmployeeWorkModels');
		// trình độ (nhân viên)
		$this->load->model('EmployeeStandardModels');
		// ngân hàng
		$this->load->model('BankModels');
		// Loại hình kinh doanh
		$this->load->model('BussinesstypeModels');
		// cơ cấu tổ chức (phòng ban)
		$this->load->model('OgchartsModels');
		// vị trí công việc
		$this->load->model('JobpositionModels');
		// phân loại nhân sự
		$this->load->model('ClassifyemployeeModels');
		// lương
		$this->load->model('EmployeeSalaryModels');
		// tình trạng học vấn
		$this->load->model('EdustatusModels');
		// trình độ 
		$this->load->model('StandardModels');
		// trường học
		$this->load->model('SchoolModels');
		// ngành học
		$this->load->model('MajorsModels');
		// bảng trình độ học vấn chứa 
		//(EdustatusModels, StandardModels , SchoolModels, MajorsModels, EmployeeModels) 
		$this->load->model('EmployeelevelModels');
		// lịch sử làm việc
		$this->load->model('EmployeeHistoryWorkModels');
		// chi nhánh 
		$this->load->model('BranchModels');
		// chức danh
		$this->load->model('PositionModels');
		// module
		$this->load->model('ModulesModels');
		// chi tiết module
		$this->load->model('ModulesDetailModels');
		$this->load->model('RoleModels');
		// hợp đồng 
		$this->load->model('ContractModels');
		// lộ trình sự nghiệp
		$this->load->model('CareerroadmapModels');
	}

	// =================START TEMPLATE =================== //
	// thông tin cá nhân
	public function profile($uri_employeeID)
	{
		// var_dump($this->roleID);die;
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];

		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		//kiểm tra giá trị và gán dữ liệu Trình Trạng hôn nhân
		$dataEmployee['marital'] = $this->EmployeeModels->check_marital($dataEmployee['marital']);


		//Nếu tài khoản chưa kết nối với nhân sự
		if ($dataEmployee == NULL) {
			redirect('cpanel/auths/profile_empty');
		}
		//get standard name from tbl_standard of current user
		$getStandard = $this->StandardModels->findRowWhere(array('id' => $dataEmployee['standardID'], 'publish' => 1), 'name');
		$dataEmployee['standard_name'] = $getStandard != NULL ? $getStandard['name'] : '';

		//employees_standards
		$employees_standards = $this->EmployeeStandardModels->findWhere(array('employeeID' =>  $dataEmployee['id']), '*');

		foreach ($employees_standards as $key_change => $val_change) {
			$employees_standards[$key_change]['date_from'] = implode("/", array_reverse(explode("-", $val_change['date_from'])));
			$employees_standards[$key_change]['date_to'] =  implode("/", array_reverse(explode("-", $val_change['date_to'])));
		}

		//employees_works
		$employees_works = $this->EmployeeWorkModels->findWhere(array('employeeID' =>  $dataEmployee['id']), '*', 'id asc');
		foreach ($employees_works as $key_change => $val_change) {
			$employees_works[$key_change]['date_from'] = implode("/", array_reverse(explode("-", $val_change['date_from'])));
			$employees_works[$key_change]['date_to'] =  implode("/", array_reverse(explode("-", $val_change['date_to'])));
		}
		//trình độ
		$getStandards = $this->StandardModels->findWhere(array('publish' => 1), 'id, name');
		// người thân
		$dataEmployee['relative'] =  $this->EmployeeRelativeModels->findWhere(array('employeeID' => $dataEmployee['id']), '*');
		// ngân hàng 
		$dataEmployee['allBank'] =	$this->BankModels->findWhere(array('publish' => 1));
		// ngân hàng (nhân viên)
		$dataEmployee['Employeebank'] =  $this->EmployeeBankModels->findWhere(array('employeeID' => $dataEmployee['id'], 'is_default' => 1), '*');
		foreach ($dataEmployee['Employeebank'] as $key_acbank => $val_acbank) {
			$dataBank = $this->BankModels->findRowWhere(array('id' => $val_acbank['bankID']), 'name');
			$dataEmployee['Employeebank'][$key_acbank]['bank'] = $dataBank['name'];
		}
		$dataEmployee['birthday'] = implode("/", array_reverse(explode("-", $dataEmployee['birthday'])));
		// loại hình kinh doanh
		$getBussinesstypes  = $this->BussinesstypeModels->findWhere(array('publish' => 1), 'id, name');
		// ngân hàng
		$getBanks = $this->BankModels->findWhere(array('publish' => 1), 'id, name');
		$staff_code =  $dataEmployee['code']; //$dataExport['code'];
		$url = base_url() . "cpanel/auths/profile/" . $staff_code;
		// lấy mã QRCODE
		$dataEmployee['qrcode'] =  "http://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=" . $url;
		$dataEmployee['contract'] =  $this->ContractModels->findRowWhere(array('employeeID' => $employeeID), '*', 'id asc');
		//	var_dump($dataEmployee['contract']);die;
		$data_employeelevel = $this->EmployeelevelModels->findRowWhere(array('employeeID' => $employeeID), '*', 'id asc');
		// tình trạng học vấn
		$data_same['edu_status'] = $this->EdustatusModels->findRowWhere(array('id' => $data_employeelevel['edu_statusID'], 'publish' => 1), '*', 'id asc');

		// trình độ
		$data_same['standard'] = $this->StandardModels->findRowWhere(array('id' => $data_employeelevel['standardID'], 'publish' => 1), '*', 'id asc');
		// trường học
		$data_same['schools'] = $this->SchoolModels->findRowWhere(array('id' => $data_employeelevel['schoolsID'], 'publish' => 1), '*', 'id asc');
		// chuyên ngành
		$data_same['majors'] = $this->MajorsModels->findRowWhere(array('id' => $data_employeelevel['majorsID'], 'publish' => 1), '*', 'id asc');

		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$employeeHistoryWork['startwork'] = implode("/", array_reverse(explode("-", $employeeHistoryWork['startwork'])));
		// chi nhánh (văn phòng)
		$employeeHistoryWork['branchName'] = $this->BranchModels->findRowWhere(array('id' => $employeeHistoryWork['branchID'], 'publish' => 1), '*', 'id asc');
		// chức danh
		$employeeHistoryWork['job_position_titleName'] = $this->JobpositionModels->findRowWhere(array('id' => $employeeHistoryWork['job_position_titleID']), '*', 'id asc');
		$employeeHistoryWork['classifyemployeeName'] = $this->ClassifyemployeeModels->findRowWhere(array('id' => $employeeHistoryWork['classify_employeeID'], 'publish' => 1), '*', 'id asc');
		//var_dump($employeeHistoryWork);die;
		$url_employeeID = "";
		if ($this->uri->segment(4) != NULL) {
			$url_employeeID = '/' . $this->uri->segment(4);
			$main_employeeID = $this->uri->segment(4);
		}
		$data = array(
			'data_index'			=>  $this->get_index(),
			'title'					=>	'Thông tin tài khoản',
			'template' 				=> 	$this->template . 'profile',
			'tab'       			=>  $this->template . 'profile/profile_info',
			'active'				=>  "profile_info",
			'control'				=>  $this->control,
			'userRow'				=>  $this->userRow,
			'data_employeelevel' 	=>  $data_same,
			'data_employee'			=> 	$dataEmployee,
			'startwork'				=>  $startwork,
			'employeeID'			=>  $url_employeeID,
			'main_employeeID'		=>  $main_employeeID,
			'employeeHistoryWork'	=>  $employeeHistoryWork,
			'data_account'			=>  $userRow,
			'banks'				  	=>  $getBanks,
			'bussinesstypes'	  	=>  $getBussinesstypes,
			'standards'			  	=>  $getStandards,
			'employees_standards' 	=>  $employees_standards,
			'employees_works' 	  	=>  $employees_works,
			
			'path_dir' => $this->path_dir,
		);
		$data_employee =  $this->EmployeeModels->find($employeeID , '*', 'id');
		$data['jobposition'] = $this->JobpositionModels->find($data_employee['job_positionID'],'*', 'id');
		// phòng ban
		$data['orgchart'] = $this->OgchartsModels->find($data_employee['ogchartID'],'*', 'id');
		// 29/4 : thay đoi
		$data['careerroad'] = $this->CareerroadmapModels->find($data_employee['careerroadsID'],'*', 'id');
	
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	// thông tin công việc
	public function mywork($uri_employeeID)
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];

		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		//employees_works
		$employees_works = $this->EmployeeWorkModels->findWhere(array('employeeID' => $employeeID), '*', 'id asc');
		foreach ($employees_works as $key_change => $val_change) {
			$employees_works[$key_change]['date_from'] = implode("/", array_reverse(explode("-", $val_change['date_from'])));
			$employees_works[$key_change]['date_to'] =  implode("/", array_reverse(explode("-", $val_change['date_to'])));
		}
		$data_employee =  $this->EmployeeModels->find($employeeID , '*', 'id');
		
		// loại hình kinh doanh
		$getBussinesstypes  = $this->BussinesstypeModels->findWhere(array('publish' => 1), 'id, name');
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'	 	=> $this->get_index(),
			'title'				=>	'Thông tin tài khoản',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> $this->template . 'profile/mywork',
			'active'			=> "mywork",
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_employee'		=> 	$dataEmployee,
			'data_account'		=> $userRow,
			'bussinesstypes'	=>  $getBussinesstypes,
			'employees_works' 	=> $employees_works,
			'startwork'			=> $startwork,
			'path_dir' => $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($data_employee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	// Lương thưởng & phúc lợi
	public function benefits($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Lương và phúc lợi',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> 'cpanel/building/index',
			'active'			=> "benefits",
			'data_employee'		=> 	$dataEmployee,
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_account'		=> $userRow,
			'startwork'			=> $startwork,
			'path_dir' 			=> $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($dataEmployee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	// Danh sách nhiệm vụ
	public function tasklist($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
	
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Danh sách nhiệm vụ',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> 'cpanel/building/index',
			'active'			=> "tasklist",
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_employee'		=> 	$dataEmployee,
			'data_account'		=> $userRow,
			'startwork'			=> $startwork,
			'path_dir' 			=> $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($dataEmployee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	// thành tựu & giải thưởng
	public function achievement($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
	
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Thành tựu & giải thưởng',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> 'cpanel/building/index',
			'active'			=> "achievement",
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_employee'		=> 	$dataEmployee,
			'data_account'		=> $userRow,
			'startwork'			=> $startwork,
			'path_dir' 			=> $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($dataEmployee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	//Hồ sơ và hợp đồng
	public function contracts($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
	
		$contract =  $this->ContractModels->findRowWhere(array('employeeID' => $employeeID), '*',  'id desc');
		$type_contract = $this->setTypeContract();
		$contract['type_contract_str'] = "";
		$contract['branch'] = $this->BranchModels->findRowWhere(array('id' => $contract['branchID']), '*',  'id desc');
		foreach ($type_contract as $key_type => $val_type) {
			if ($key_type == $contract['type_contractID']) {
				$contract['type_contract_str'] =  $type_contract[$key_type];
			}
		}
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Hồ sơ và hợp đồng',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> $this->template . 'profile/contracts',
			'active'			=> "contracts",
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_employee'		=> 	$dataEmployee,
			'contracts' 		=> $contract,
			'data_account'		=> $userRow,
			'startwork'			=> $startwork,
			'path_dir' 			=> $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($dataEmployee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	public function setTypeContract()
	{
		$arrayData = array(
			'1'	=>	'HĐ thử việc',
			'2'	=>	'HĐ không xác định thời hạn',
			'3'	=>	'HĐ lao động 01 năm',
			'4'	=>	'HĐ lao động 06 tháng',
			'4'	=>	'HĐ dịch vụ',
			'6'	=>	'HĐ lao động thời vụ (trên 3 tháng)',
			'7'	=>	'HĐ 06 tháng',
		);
		return $arrayData;
	}
	// Lịch làm việc
	public function timesheets($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Lịch làm việc',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> $this->template . 'profile/timesheets',
			'active'			=> "timesheets",
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_employee'		=> 	$dataEmployee,
			'data_account'		=> $userRow,
			'startwork'			=> $startwork,
			'path_dir' 			=> $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($dataEmployee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	// Sở hữu trí tuệ
	public function intellectual($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID); // $userRow['employees_id'];
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		
		$employeeHistoryWork = $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
		$startwork = $this->EmployeeModels->count_date_from_today($employeeHistoryWork['startwork']);
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Sở hữu trí tuệ',
			'template' 			=> 	$this->template . 'profile',
			'tab'       		=> 'cpanel/building/index',
			'active'			=> "intellectual",
			'control'			=>  $this->control,
			'userRow'			=>  $this->userRow,
			'data_account'		=> $userRow,
			'data_employee'		=> 	$dataEmployee,
			'startwork'			=> $startwork,
			'path_dir' => $this->path_dir,
		);
		$data['jobposition'] = $this->JobpositionModels->find($dataEmployee['job_positionID'],'*', 'id');
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	// =================END TEMPLATE =================== //
	//Login action
	public function login()
	{
		
		if ($this->Auth->check_logged() === true) {
			redirect(base_url() . 'cpanel/admin');
		} else {
			$data['title'] = 'Login';
			if ($this->input->post()) {
				$email = $this->input->post('email');
				$password = $this->input->post('password');
				$type = 'admin';
				$login_array = array($email, $password, $type);
				// var_dump($this->Auth->process_login($login_array));die;
				if ($this->Auth->process_login($login_array)) {
					//get data info user
					$user = $this->Auth->logged_info();

					redirect(base_url() . 'cpanel');
				}
			}
			$data = array(
				'title'			=>	'Login System',
			);
			$this->load->view('cpanel/auth/login', isset($data) ? $data : NULL);
		}
	}
	//Forget Password - OT1
	public function forgetpass()
	{
		if($this->input->post()){
			  $email = $this->input->post("email");
			  $random_pass = $random_password = rand(100000, 999999);
			  // gửi email
			  $subject = 'Confirm new password';
			  $message = 'Message';
			  $email = $this->input->post('email');
			  $result = $this->UserModels->find($email, '*', 'email');
			  $id = $result['id'];
			  $fullname = $result['fullname'];
			  // ghi lại thao tác
			   $this->Auth->addLog($result['employeeID'],$result['email'],'Quên mật khẩu');
			  //load primary mail
			  $this->load->library('email');
			  $subject = 'Confirm to reset Password';
			  $nickname_email = 'Hungthinh';
			  // content mail
			  $body = "<p style='margin-bottom:30px;'>Xin chào <strong>".$fullname."</strong>,<br/>";
			  $body .= "Mật khẩu mới của bạn là: ". $random_pass;
			  $message = $body;
			  $this->send($subject, $message ,$email, $nickname_email);
			  
			  // cập nhật lại 
				$rand_salt = $this->Encrypts->genRndSalt();
				$encrypt_pass = $this->Encrypts->encryptUserPwd($random_pass, $rand_salt);
				$data_post['password']  = $encrypt_pass; // pass ẩn (encrypt)
				$data_post['salt'] 	    = $rand_salt;
				$data_post['text_pass'] = $random_pass; // pass hiện 
				// cập nhật lại pass
				$this->UserModels->edit($data_post, $id);
				redirect(base_url().'cpanel/login.html');
		}
		$this->load->view('cpanel/auth/forgetpass', isset($data) ? $data : NULL);
	}
	public function send($subject = '', $message = '',$to, $nickname_email){
	    $this->config->load('email', TRUE);
	    $smtp_user = $this->config->item('smtp_user', 'email');
	    //send mail
	    $this->email->from($smtp_user, $nickname_email);
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if ($this->email->send()) {
			return true;
		} 
		return false;
	}

	// forget pass check email
	public function check_EmailForget()
	{
		$email = $this->input->post('email');
		$result = $this->UserModels->find($email, 'email,type', 'email');
		if($result == NULL)
		{
			echo json_encode(array("type"=> "error", "mess"=> "Không tìm thấy email"));
		}else
		{
			echo json_encode(array("type"=> "success", "mess"=> "Success"));
		}
	}
	public function check_Password()
	{
		$id = $this->session->userdata('logged_user');
		$result = $this->UserModels->find($id, 'salt,password');
		$rand_salt_old = $result['salt'];
		$oldpassword   = $result['password'];
		$oldpassword_post = $this->Encrypts->encryptUserPwd($this->input->post('old_password'), $rand_salt_old);
		if ($oldpassword == $oldpassword_post) {
			return true;
		} else {
			$this->form_validation->set_message(__FUNCTION__, 'Mật khẩu cũ không chính xác, xin vui lòng nhập lại!');
			return false;
		}
	}

	//ChangePassword - Ot1
	public function changepass()
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$id = $this->session->userdata('logged_user');
		if ($this->input->post()) {
			if ($this->input->post() != NULL) {
				$this->form_validation->set_rules('old_password', 'Mật khẩu cũ', 'required|callback_check_Password');
				$this->form_validation->set_rules('new_password', 'Mật khẩu mới', 'required');
				$this->form_validation->set_rules('re_password', 'Nhập lại mật khẩu mới', 'required|matches[new_password]');
				if ($this->form_validation->run()) {
					$new_password = $this->input->post('new_password');
					$rand_salt = $this->Encrypts->genRndSalt();
					$encrypt_pass = $this->Encrypts->encryptUserPwd($new_password, $rand_salt);
					$data_update = array(
						'password'			=>	$encrypt_pass,
						'salt'				=>	$rand_salt,
						'text_pass'			=>	$new_password,
						'updated_at'		=>	gmdate('Y-m-d H:i:s', time() + 7 * 3600)
					);

					$result = $this->UserModels->edit($data_update, $id);
				
					if ($result > 0) {
						// lấy thông tin user 
						$userData = $this->UserModels->find($id,'*', 'id');
						 // ghi lại thao tác
						$this->Auth->addLog($userData['employeeID'],$userData['email'],'Đổi mật khẩu');
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> EDIT_SUCCESS,
						));
					} else {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> EDIT_FAIL,
						));
					}
					redirect(CPANEL . 'auths/changepass');
				}
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Đổi mật khẩu',
			'template' 		=> 	CPANEL . 'auth/changepass',
			'control'		=>  $this->control
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}

	//Logout action
	function logout()
	{
		$this->session->unset_userdata('logged_user');
		redirect(base_url() . 'cpanel/login.html');
	}
	//create pass
	public function createPass()
	{
		$id = $_POST['id'];
		$userRow = $this->UserModels->find($id, 'email, password', 'employeeID');
		if ($userRow == NULL) {
			$data = array('rs' => false);
		} else {
			$data = array('rs' => true, 'dataRow' => $userRow);
		}

		$this->load->view($this->template . 'createPass', isset($data) ? $data : NULL);
	}

	// profile -  cập nhật  profile 
	public function profile_update_info($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/profile";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/profile/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		$data_update_info = $this->input->post('data_update_info');
		$data_update_work_info = $this->input->post('data_update_work_info');
		// Cập nhật thông tin profile
		if ($data_update_info != NULL) {
			// lưu thao tác (addLog)
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật thông tin nhân viên',json_encode($dataEmployee),json_encode($data_update_info));
			$data_update_info['birthday'] = $this->Functions->changFormatDate($data_update_info['birthday']);
			$this->EmployeeModels->edit($data_update_info, $employeeID);
		}
		// Cập nhật thông tin công việc
		if ($data_update_work_info != NULL) {
			// cập nhật orgchartID cho phân quyền
			$this->EmployeeModels->edit(array("job_positionID" =>$data_update_work_info['job_positionID'],
			// 'careerroadsID' => $data_update_work_info['careerroadsID'],
			'ogchartID' => $data_update_work_info['ogchartID'],
			'job_position_titleID' => $data_update_work_info['job_position_titleID']), $employeeID, 'id');
			// lịch sử làm làm việc 
			$findHistoryWork =  $this->EmployeeHistoryWorkModels->find($employeeID, '*', $field = 'employeeID');
			$data_update_work_info['startwork'] = $this->Functions->changFormatDate($data_update_work_info['startwork']);
			$data_update_work_info['employeeID']  = $employeeID;
			// lưu thao tác (addLog)
			$historyWorkEmployee = $findHistoryWork ;
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật thông tin công việc nhân viên',json_encode($historyWorkEmployee),json_encode($data_update_work_info));
			if ($findHistoryWork == NULL) {
				$data_update_work_info['created_at']  = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$data_update_work_info['updated_at']  = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$this->EmployeeHistoryWorkModels->add($data_update_work_info);
			} else {
				$data_update_work_info['updated_at']  = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$this->EmployeeHistoryWorkModels->edit($data_update_work_info, $employeeID, 'employeeID');
			}
		}
		redirect(CPANEL . $link_redirect);
	}
	// profile - tài khoản ngân hàng
	public function profile_account_bank($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/profile";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/profile/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployeeBank =  $this->EmployeeBankModels->find($employeeID, '*', 'employeeID');
		$data_post_bank = $this->input->post('data_post_bank');
		$data_update_bank = $this->input->post('data_update_bank');
		$result = "";
		//thêm tài khoản ngân hàng
		if ($data_post_bank != NULL) {
			$data_post_bank['employeeID'] = $employeeID;
			$data_post_bank['is_default'] = 1;
			$result = $this->EmployeeBankModels->add($data_post_bank);
			// lưu thao tác (addLog)
			$this->Auth->addLog($employeeID, $userRow['email'],'Thêm tài khoản ngân hàng',json_encode($dataEmployeeBank),json_encode($data_post_bank));
		}
		// Cập nhật tài khoản ngân hàng
		if ($data_update_bank != NULL) {
			$id_account_bank = $this->input->post('id_account_bank');
			$result = $this->EmployeeBankModels->edit($data_update_bank, $id_account_bank, $key = 'id');
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật tài khoản ngân hàng',json_encode($dataEmployeeBank),json_encode($data_update_bank));
		}
		if ($result['type'] == "successful") {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'success',
				'message'	=> ADD_SUCCESS,
			));
		} else {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'error',
				'message'	=> ADD_FAIL,
			));
		}
		redirect(CPANEL . $link_redirect);
	}
	// profile - quá trình học tập
	public function profile_progress_learn($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/profile";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/profile/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployeeStandard =  $this->EmployeeStandardModels->find($employeeID, '*', 'employeeID');
		$data_post_learn = $this->input->post('data_post_learn');
		$data_update_learn = $this->input->post('data_update_learn');
		$result = "";
		// thêm quá trình học tập
		if ($data_post_learn != NULL) {
			$data_post_learn['date_from'] = $this->Functions->changFormatDate($data_post_learn['date_from']);
			$data_post_learn['date_to'] = $this->Functions->changFormatDate($data_post_learn['date_to']);
			$data_post_learn['employeeID'] = $employeeID;
			$result = $this->EmployeeStandardModels->add($data_post_learn);
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Thêm quá trình học tập',json_encode($dataEmployeeStandard),json_encode($data_post_learn));
		}
		// cập nhật quá trình học tập
		if ($data_update_learn != NULL) {
			$id_progress_learn = $this->input->post('id_progress_learn');
			$data_update_learn['date_from'] = $this->Functions->changFormatDate($data_update_learn['date_from']);
			$data_update_learn['date_to'] = $this->Functions->changFormatDate($data_update_learn['date_to']);
			$result = $this->EmployeeStandardModels->edit($data_update_learn, $id_progress_learn, $key = 'id');
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật quá trình học tập',json_encode($dataEmployeeStandard),json_encode($data_update_learn));
		}
		if ($result['type'] == "successful") {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'success',
				'message'	=> ADD_SUCCESS,
			));
		} else {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'error',
				'message'	=> ADD_FAIL,
			));
		}
		redirect(CPANEL . $link_redirect);
	}
	// profile - cập nhật trình độ học vấn
	public function profile_update_employeelevel($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/profile";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/profile/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		$data_post = $this->input->post('data_post');
		if ($data_post != NULL) {
			$dataEmployeelevel = $this->EmployeelevelModels->findRowWhere(array('employeeID' => $employeeID));
		
			if ($dataEmployeelevel == NULL) {
				$data_post['employeeID'] = $employeeID;
				$data_post['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$result = $this->EmployeelevelModels->add($data_post);
				// lưu thao tác
				$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật trình độ học vấn','Không có dữ liệu',json_encode($data_post));
			} else {
				$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$result = $this->EmployeelevelModels->edit($data_post, $employeeID, 'employeeID');
				// lưu thao tác
				$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật trình độ học vấn',json_encode($dataEmployeelevel),json_encode($data_post));
			}

			if ($result['type'] == "successful") {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> ADD_SUCCESS,
				));
			} else {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> ADD_FAIL,
				));
			}
		}
		redirect(CPANEL . $link_redirect);
	}
	// profile - thêm người thân
	public function profile_relative($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
	
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/profile";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/profile/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployeeRelative = $this->EmployeelevelModels->findWhere(array('employeeID' => $employeeID));
		$data_post_relative = $this->input->post('data_post_relative');
		$data_update_relative = $this->input->post('data_update_relative');
		$result = "";
		// thêm
		if ($data_post_relative != NULL) {
			$data_post_relative['employeeID'] = $employeeID;
			$result = $this->EmployeeRelativeModels->add($data_post_relative);
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Thêm người liên lạc',json_encode($dataEmployeeRelative),json_encode($data_post_relative));
		}
		// cập nhật
		if ($data_update_relative != NULL) {
			$id_EmployeeRelative = $this->input->post('id_EmployeeRelative');
			$result = $this->EmployeeRelativeModels->edit($data_update_relative, $id_EmployeeRelative, $key = 'id');
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật người liên lạc',json_encode($dataEmployeeRelative),json_encode($data_update_relative));
		}
		if ($result['type'] == "successful") {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'success',
				'message'	=> ADD_SUCCESS,
			));
		} else {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'error',
				'message'	=> ADD_FAIL,
			));
		}
		redirect(CPANEL . $link_redirect);
	}
	// profile - tài liệu tham chiếu
	public function profile_refer_material($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/profile";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/profile/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployee = $this->EmployeeModels->find($employeeID);
		$data_post = $this->input->post('data_post');
		//upload ID Front
		if ($_FILES["id-front"]["name"]) {
			$filenameIdFront = $_FILES['id-front']['name'];
			$path_dir = $this->path_dir . $dataEmployee['code'] . '/';

			$data_upload_font = $this->Upload->upload_image($path_dir, $filenameIdFront, 'id-front', 'id-front');
			if ($data_upload_font['type'] == 'error') {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> $data_upload_font['message'],
				));
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$data_post['id-front'] = $data_upload_font['image'];
			}
		}
		//upload ID Back
		if ($_FILES["id-back"]["name"]) {
			$filenameIdBack = $_FILES['id-back']['name'];
			$path_dir = $this->path_dir . $dataEmployee['code'] . '/';
			$data_upload_back = $this->Upload->upload_image($path_dir, $filenameIdBack, 'id-back', 'id-back');
			if ($data_upload_back['type'] == 'error') {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> $data_upload_back['message'],
				));
				redirect($_SERVER['HTTP_REFERER']);
			} else {
				$data_post['id-back'] = $data_upload_back['image'];
			}
		}

		if ($data_post !=  NULL) {
			$data_post['identify_type'] = $this->input->post('identify_type');
			$result =  $this->EmployeeModels->edit($data_post, $employeeID, $key = 'id');
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật tài liệu tham chiếu',json_encode($dataEmployee),json_encode($data_post));
			if ($result['type'] == "successful") {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> ADD_SUCCESS,
				));
			} else {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> ADD_FAIL,
				));
			}
		}
		redirect(CPANEL . $link_redirect);
	}

	// mywork - Thêm lịch sử công tác
	public function mywork_history_work($uri_employeeID)
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();
		$userRow = $this->UserModels->find($userID);
		$link_redirect = "auths/mywork";
		if ($uri_employeeID != NULL) {
			$employeeID =  $uri_employeeID;
			$link_redirect = "auths/mywork/" . $uri_employeeID;
		} else {
			$employeeID = $userRow['employeeID'];
		}
		$dataEmployeeWork = $this->EmployeeWorkModels->findWhere(array('employeeID' => $employeeID)); // lấy dữ liệu lịch sử công tác
		$data_post_historywork = $this->input->post('data_post_historywork');
		$data_update_historywork = $this->input->post('data_update_historywork');
		$result = "";
		// thêm 
		if ($data_post_historywork != NULL) {
			$data_post_historywork['date_from'] = $this->Functions->changFormatDate($data_post_historywork['date_from']);
			$data_post_historywork['date_to'] = $this->Functions->changFormatDate($data_post_historywork['date_to']);
			$data_post_historywork['employeeID'] = $employeeID;
			$result = $this->EmployeeWorkModels->add($data_post_historywork);
			if($dataEmployeeWork == NULL)
			{
				$dataCheck = "Không có dữ liệu";
			}	
			else
			{
				$dataCheck = json_encode($dataEmployeeWork);
			}
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Thêm lịch sử công tác',$dataCheck,json_encode($data_post_historywork));
		}
		// cập nhật
		if ($data_update_historywork != NULL) {
			$id_work = $this->input->post('id_work');
			$data_update_historywork['date_from'] = $this->Functions->changFormatDate($data_update_historywork['date_from']);
			$data_update_historywork['date_to'] = $this->Functions->changFormatDate($data_update_historywork['date_to']);
			$data_update_historywork['employeeID'] = $employeeID;
			$result = $this->EmployeeWorkModels->edit($data_update_historywork, $id_work, 'id');
			// lưu thao tác
			$this->Auth->addLog($employeeID, $userRow['email'],'Cập nhật lịch sử công tác',json_encode($dataEmployeeWork),json_encode($data_update_historywork));
		}
		if ($result['type'] == "successful") {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'success',
				'message'	=> ADD_SUCCESS,
			));
		} else {
			$this->session->set_flashdata('message_flashdata', array(
				'type'		=> 'error',
				'message'	=> ADD_FAIL,
			));
		}
		redirect(CPANEL . $link_redirect);
	}
	// thông tin cá nhân
	public function profile_empty()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$userID = $this->Auth->logged_id();

		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thông tin tài khoản',
			'template' 	=> 	$this->template . 'profile_empty',
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}

	// xem thông báo ứng thuyển
	public function apply_alert()
	{
		echo "success";
	}
}
