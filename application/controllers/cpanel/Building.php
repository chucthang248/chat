<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Building extends Admin_Controller {
	public $template = 'cpanel/building/';
	public $title = 'Đang xây dựng';
	
	public function __construct(){
		parent::__construct();
	}
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}

		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}


}
