<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Files extends Admin_Controller {
	public $template = 'cpanel/files/';
	public $title = 'biên bản, giấy tờ';
	public $control = 'files';
	public $path_dir = 'upload/files/';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('FilesModels');
		$this->load->model('Upload');
		$this->load->library('TCPDF');
		$this->load->library("session");
		$this->load->model('TypefilesModels'); // loại hồ sơ
	}
	
	public function replaceVariable($string)
	{
		$start = strpos($string, '(=');
		$end = strpos($string, '=)');
		$search = substr($string, $start, ($end-$start) + 2);
		$result = str_replace($search, '....', $string);
		return $result;
	}

	public function testString()
	{
		// $string = "ban oi (=123213=) bann (=123213=) ban oi (=123213=) bann (=123213=) ban oi (=123213=) bann (=123213=) ban oi (=123213=) bann (=123213=)";
		$string = "ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ban oi ";
		$result = $this->replaceVariable($string);
		// $start = strpos($string, '(=');
		// $end = strpos($string, '=)');
		// if($start != '' AND $end != ''){
		// 	$search = substr($string, $start, ($end-$start) + 2);
		// 	echo "ok";
		// }
		// // $result = $this->replaceString($str);
		// echo $start."---".$end;
		// echo "<br>";
		echo $result;
	}

	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->FilesModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

	public function check_Code()
	{
		$data_post = $this->input->post('data_post');
		$code = $data_post['code'];
		$getFiles = $this->FilesModels->find($code, 'code', 'code');
		if($getFiles != NULL){
			$this->form_validation->set_message(__FUNCTION__,'Mã hồ sơ <b>'.$code.'</b> đã bị trùng!');
			return false;
		}else{
			return true;
		}
	}

	public function add()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$getDatas['typeFile'] = $this->TypefilesModels->findOrWhere($where = array('publish'=> 1));
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($data_post != NULL){ 
				$this->form_validation->set_rules('data_post[code]','Mã hồ sơ', 'required|callback_check_Code');
				if($this->form_validation->run()){
					//insert data
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					$result = $this->FilesModels->add($data_post);
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect(CPANEL.$this->control);
				}
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thêm mới dữ liệu',
			'template' 	=> 	$this->template.'add',
			'control'	=>  $this->control,
			'datas'		=> $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

	public function edit($id)
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$getDatas = $this->FilesModels->find($id);
		$getDatas['typeFile'] = $this->TypefilesModels->findOrWhere($where = array('publish'=> 1));
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($data_post != NULL){
				$getFiles = $this->FilesModels->findRowWhere(array('code' => $data_post['code'], 'id' => $id));
				if($getFiles == NULL){
					$this->form_validation->set_rules('data_post[code]','Mã hồ sơ', 'required|callback_check_Code');
				}else{
					$this->form_validation->set_rules('data_post[code]','Mã hồ sơ', 'required');
				} 
				if($this->form_validation->run()){
					//push date update into data_post
					$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					//if publish unchecked
					if(!$data_post['publish']){
						$data_post['publish'] = 0;
					}

					$result = $this->FilesModels->edit($data_post,$id);
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> EDIT_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> EDIT_FAIL,
						));
					}
					$result = $this->FilesModels->find($id, '*');
					$pdf=new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
					// set document information

					$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
					$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
					$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
					$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
					$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
					$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

					$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
					$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
					$pdf->SetFont('dejavusans', '', 10);
					$pdf->AddPage();


					$html = $result['content'];
					$pdf->writeHTML($html, true, 0, true, 0);

					$pdf->lastPage();
					$fileName = $getDatas['code'].".pdf";
					$folderPdf =__DIR__."\/"."PDF";
					$urlFile = $folderPdf."\/".$getDatas['code'].".pdf";
				if(is_dir($folderPdf) === false )
				{
					mkdir($folderPdf);
					$pdf->Output($urlFile, 'F');
					closedir($folderPdf); // đóng folder sau khi tạo (để xóa folder)
				}else
				{
					$pdf->Output($urlFile, 'F');
				}
					// content
					if (file_exists($urlFile)) {
						header('Content-Description: File Transfer');
						header('Content-Type: application/pdf; charset=utf-8');
						header('Content-Disposition: attachment; filename='.$fileName);
						header('Content-Length: '.filesize($urlFile));
						readfile($urlFile);
						if(is_dir($folderPdf) === TRUE )
						{		
							if(unlink($urlFile)) 
							{ 
								rmdir($folderPdf);
							} 
						}
						
					}
					redirect(CPANEL.$this->control);
				}
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Cập nhật dữ liệu',
			'template' 	=> 	$this->template.'edit',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

	//ajax showcontent (index)
	public function showcontent(){
		$id = $_POST['id'];
		$result = $this->FilesModels->find($id, 'content');
		$content = $result['content'];
		echo json_encode(array('content' => $content));
	}

	public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_post[$properties] = $field;
		$result = $this->FilesModels->edit($data_post,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->FilesModels->delete($id);
	}
}
