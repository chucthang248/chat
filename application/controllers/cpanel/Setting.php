<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends Admin_Controller
{
	public $template = 'cpanel/setting/';
	public $title = 'cấu hình thông tin chung';
	public $control = 'setting';


	public function __construct()
	{
		parent::__construct();
		$this->get_index();
		$this->load->model('SettingModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		//get fullname - OT1
		$getDataRow = $this->SettingModels->find('general', 'content', 'key');
		$dataRow = json_decode($getDataRow['content'], true);
			if ($this->input->post()) {
				$data_post = $this->input->post('data_post');
				if ($data_post != NULL) {

					//insert DB
					$data_insert['content'] = json_encode($data_post);
					$data_insert['key'] = 'general';
					$data_insert['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
					$result = $this->SettingModels->edit($data_insert, 'general', 'key');
					if ($result > 0) {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
					} else {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect(CPANEL . $this->control);
				}
			}

		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'dataRow'		=>  $dataRow,
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	//List action - OT2
	public function dashboard()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'dashboard',
			'control'	=>  $this->control,
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}

	// Thành tựu và giải thưởng
	public function achievement()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'achievement',
			'control'	=>  $this->control,
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
}
