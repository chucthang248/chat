<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Role extends Admin_Controller {
	public $template = 'cpanel/role/';
	public $title = 'nhóm người dùng';
	public $control = 'role';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('RoleModels');
		// module
		$this->load->model('ModulesModels');
        $this->load->model('ModulesDetailModels');
		$this->load->model('PermissonsModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->RoleModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	//permission
	public function permission()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý Phân quyền',
			'template' 	=> 	$this->template.'permission',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas = ""
		);
		$data['datas'] = $this->ModulesModels->getAll();
		foreach($data['datas'] as $key => $val)
		{
			$data['datas'][$key]['child'] =	$this->ModulesDetailModels->findWhere($where = array('parentid'=>$val['id']), $fields = '*');
		}
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	
	public function add()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		
		if($this->input->post()){
			$data_post_role = $this->input->post('data_post_role');
			$data_post_permisson = $this->input->post('data_post_permisson');
		
			if($data_post_role != NULL){ 
				$data_post_role['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				$result = $this->RoleModels->add($data_post_role);
				$id_insert = $result['id_insert'];
				if($data_post_permisson != NULL)
				{
					foreach($data_post_permisson as $key_permisson => $val_permisson)
					{
						if($val_permisson != NULL)
						{
							foreach($val_permisson as $key_child1_permisson => $val_child1_permisson)
							{
								if($val_child1_permisson != NULL)
								{
									
									if($val_child1_permisson['status'] != NULL && $val_child1_permisson['status'] != "" )
									{
										$val_child1_permisson['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
										$val_child1_permisson['roleID'] = $id_insert;
										if($val_child1_permisson['action'] != "index")
										{
											$val_child1_permisson['link']  = $val_child1_permisson['link']."/".$val_child1_permisson['action'];
										}
										
										$this->PermissonsModels->add($val_child1_permisson);
									}
								}
								
							}
						}
						
					}
				}
				if($result>0){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> ADD_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> ADD_FAIL,
					));
				}
				redirect(CPANEL.$this->control);
			}
			
		}
	}
	
	public function getDataRow()
	{
		$id = $_POST['id'];
		$dataRow = $this->RoleModels->find($id);
		echo json_encode(array('result' => $dataRow));
	}
	public function edit()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$id = $this->input->post('idRow');
			$data_update = $this->input->post('data_update');
			if($data_update != NULL){ 

				//push date update into data_update
				$data_update['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				//if publish unchecked
				if(!$data_update['publish']){
					$data_update['publish'] = 0;
				}
				
				$result = $this->RoleModels->edit($data_update,$id);
				$data_post_permisson = $this->input->post('data_post_permisson');
				$this->PermissonsModels->deleteWhere('roleID',$id);
				if($data_post_permisson != NULL)
				{
					foreach($data_post_permisson as $key_permisson => $val_permisson)
					{
						if($val_permisson != NULL)
						{
							foreach($val_permisson as $key_child1_permisson => $val_child1_permisson)
							{
								if($val_child1_permisson != NULL)
								{
									
									if($val_child1_permisson['status'] != NULL && $val_child1_permisson['status'] != "" )
									{
										$val_child1_permisson['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
										$val_child1_permisson['roleID'] = $id;
										if($val_child1_permisson['action'] != "index")
										{
											$val_child1_permisson['link']  = $val_child1_permisson['link']."/".$val_child1_permisson['action'];
										}
										$this->PermissonsModels->add($val_child1_permisson);
									}
								}
							}
						}
						
					}
				}
				
				if($result>0){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> EDIT_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> EDIT_FAIL,
					));
				}
				redirect(CPANEL.$this->control);
			}
		}
	}
	public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->RoleModels->edit($data_update,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->RoleModels->delete($id);
	}
}