<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contracts extends Admin_Controller {
	public $template = 'cpanel/contracts/';
	public $title = 'Hợp đồng';
	public $control = 'contracts';
	public $path_dir = 'upload/contract/';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		// hợp đồng
		$this->load->model('ContractModels');
		$this->load->model('EmployeeModels');
		$this->load->model('Upload');
		// chi nhánh 
		$this->load->model('BranchModels');
		// vị trì công việc
		$this->load->model('JobpositionModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->ContractModels->getAll();
		$getEmployees = $this->EmployeeModels->getAll('id,fullname');
		//lấy thời gian hiện tại
		$dateNow = date('Y-m-d');
		//sẽ viết lại
		if($getDatas != NULL){
			foreach ($getDatas as $key => $val) {
				$dataEmployee = $this->EmployeeModels->find($val['employeeID'],'fullname');
				$getDatas[$key]['employee_name'] = $dataEmployee['fullname'];
				$getDatas[$key]['type_contract'] = $this->setTypeContract($val['type_contractID']);
				//kiểm tra trạng thái hợp đồng
				$getDatas[$key]['status'] = 0;
				if($val['time_end'] != '0000-00-00' && ($dateNow > $val['time_end'])){
					$getDatas[$key]['status'] = 1;
				}
			}
		}
		$arrData['branch'] = $this->BranchModels->findWhere(array('publish'=>1), '*','id desc');
	
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$arrData['job_position'] = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'employees'		=>  $getEmployees,
			'arrdata'	=> $arrData
			
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	//List action - OT2
	public function form()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($data_post != NULL){ 
				if($data_post['rowID'] != ''){
					$rowID = $data_post['rowID'];
					$dataRow = $this->ContractModels->find($rowID);
					$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				}else{
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
				}
				//lấy tên nhân sự
				$dataEmployee = $this->EmployeeModels->find($data_post['employeeID'],'fullname,code');
				//lấy tên loại hợp đồng
				$nameTypeContract = $this->setTypeContract($data_post['type_contractID']);

				//upload file
				
				if ($_FILES["contract_file"]["name"]) {
					$path_dir = $this->path_dir . $dataEmployee['code'] . '/';
					//delete old image
					$path_contract_file = $path_dir . $dataRow['contract_file'];
					if (file_exists($path_contract_file)) {
						unlink($path_contract_file);
					}
					$filename = $_FILES['contract_file']['name'];
					
					$data_upload = $this->Upload->upload_pdf($path_dir, $filename, 'contract_file');
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['contract_file'] = $data_upload['file'];
					}
				}else{
					$data_post['contract_file'] = $dataRow['contract_file'];
				}

				$data_post['name'] = $dataEmployee['fullname'].' - '.$nameTypeContract;
				$data_post['time_start'] = $this->Functions->changFormatDate($data_post['time_start']);
				$data_post['time_end'] = $this->Functions->changFormatDate($data_post['time_end']);
				if($data_post['rowID'] != ''){
					unset($data_post['rowID']);
					$result = $this->ContractModels->edit($data_post,$rowID);
				}else{
					unset($data_post['rowID']);
					$result = $this->ContractModels->add($data_post);
				}
				if($result>0){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> $data_post['rowID']!=''?EDIT_SUCCESS:ADD_SUCCESS,
					));
					
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=>  $data_post['rowID']!=''?EDIT_FAIL:ADD_FAIL,
					));
				}
				redirect(CPANEL.$this->control);
			}
		}
		$this->load->view($this->template.'/form', isset($data)?$data:NULL);
	}
	public function getDataRow()
	{
		
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$dataRow = $this->ContractModels->find($id);
		$dataRow['time_start'] = date('d/m/Y',strtotime($dataRow['time_start']));
		$dataRow['time_end'] = $dataRow['time_end'] == '0000-00-00'?'00/00/0000':date('d/m/Y',strtotime($dataRow['time_end']));
		$branch = $this->BranchModels->findWhere(array('publish'=>1), '*','id desc');
		$dataRow['branchID'] = $dataRow['branchID'];
		$dataRow['branch'] = "<option value=''>Chọn chi nhánh</option>";
		if($branch  != NULL)
		{
			foreach($branch as $key_branch => $val_branch)
			{
				$selected_branch = $val_branch['id'] == $dataRow['branchID'] ? "selected" : "";

				$dataRow['branch'] .= "<option {$selected_branch} value='{$val_branch['id']}'>{$val_branch['name']} </opion>";
			}
		}
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$arrData['job_position'] = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";

		$dataRow['job_position_html'] = "<option value=''>Chọn chi nhánh</option>";
		foreach($arrData['job_position'] as $key_job => $val_job)
		{	
			$selected_jobposition = $val_job['id'] == $dataRow['job_positionID'] ? "selected" : "";
				$dataRow['job_position_html'] .= "<option {$selected_jobposition} value='{$val_job['id']}'>".str_repeat('---',$val_job['count']).$val_job['name']."</opion>";
		}
		//lấy thông tin nhân sự
		$dataEmployee = $this->EmployeeModels->find($dataRow['employeeID'],'code');
		//kiểm tra file có tồn tại hay không?
		$path_contract_file = $this->path_dir . $dataEmployee['code'] . '/' . $dataRow['contract_file'];
		if (file_exists($path_contract_file)) {
			$dataRow['contract_file'] = '<a href="'.$path_contract_file.'" target="_blank">Xem và tải hợp đồng</a>';
		}else{
			$dataRow['contract_file'] = '<span class="text-danger font-11"><i>Chưa có file cho hợp đồng này!!!</i></span>';
		}
		echo json_encode(array('dataRow' => $dataRow));
	}
	
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->ContractModels->delete($id);
	}
	public function setTypeContract($key)
	{
		$arrayData = array(
	        '1'	=>	'HĐ thử việc',
	        '2'	=>	'HĐ không xác định thời hạn',
	        '3'	=>	'HĐ lao động 01 năm',
	        '4'	=>	'HĐ lao động 06 tháng',
	        '4'	=>	'HĐ dịch vụ',
	        '6'	=>	'HĐ lao động thời vụ (trên 3 tháng)',
	        '7'	=>	'HĐ 06 tháng',
		);
		return $arrayData[$key];
	}
}
