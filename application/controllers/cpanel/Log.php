<?php 
defined('BASEPATH') or exit('No direct script access allowed');
class Log extends Admin_Controller
{
	public $template = 'cpanel/log/';
	public $title = 'Nhật ký hoạt động';
	public $control = 'log';
	public $path_dir = 'upload/log/';
	public $per_page = 20;
	public $numb_button = 3;
	public function __construct()
	{
		parent::__construct();
		$this->get_index();
		$this->load->model("LogModels");
	}
		// index
	public function index()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$date = date('Y-m-d');
		$currentDay = $date." 00:00:00";
		$page = 1; 
		$where = array('created_at >=' => $currentDay);
		$config['total_rows'] = count($this->LogModels->findWhere($where));
		$config['per_page'] = $this->per_page;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = 1;
		$number_button_page = $this->numb_button;
		$page = ($page > $total_page) ? $total_page : $page;

		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->LogModels->select_array_wherein('*, 	DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s" ) as created_at', $where, '', NULL, $order = 'id desc', ($page * $config['per_page']), $config['per_page']);
		}
		$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Quản lý ' . $this->title,
			'showDay'		=>  'ngày '.date('d/m/Y'),
			'template' 		=> 	$this->template . 'index',
			'control'		=>  $this->control,
			'per_page'  	=> $config['per_page'],
			'my_pagination' => $my_pagination,
			'datas'			=>  $getDatas,
			'count'			=> $config['total_rows'],
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
		// search 
		public function search($key_word)
		{
			$type_pageload = $_POST['type_pageload'];
			$page = 1;
			$date = date('Y-m-d');
			$currentDay = $date." 00:00:00";
			$condition = array('created_at >=' => $currentDay);
			$showFrom_To_Day =  array();
			if ($type_pageload == "ajax")
			{
				$condition = array();
			}
			if(empty($_POST['email']) &&  empty($_POST['from_day']) && empty($_POST['to_day']))
			{
				$condition = array('created_at >=' => $currentDay);

			}
			if (!empty($_POST['email']) && isset($_POST['email']))
			{
				$condition 	= array_merge($condition,array("email" => $_POST['email']));
				$showDay 	= 'ngày: '.date('d/m/Y');
			}
			if (!empty($_POST['from_day']) && isset($_POST['from_day'])) {
				//$format_from_day  =  implode("-",array_reverse(explode("/",$_POST['from_day']))) ;
				$format_from_day = DateTime::createFromFormat('d/m/Y', $_POST['from_day']);
				$format_from_day  = $format_from_day->format('Y-m-d');
				$condition 	= array_merge($condition,array("created_at >=" => $format_from_day ));

				$showFrom_To_Day = array_merge($showFrom_To_Day,array("từ ngày: ".$_POST['from_day']." "));
			}
			if (!empty($_POST['to_day']) && isset($_POST['to_day'])) {
				$format_to_day = DateTime::createFromFormat('d/m/Y', $_POST['to_day']);
				$format_to_day  = $format_to_day->format('Y-m-d');
				$condition 	= array_merge($condition,array("created_at <=" => $format_to_day." 23:59:00"  ));

				$showFrom_To_Day = array_merge($showFrom_To_Day,array("đến ngày: ".$_POST['to_day'] ));
			}
			if (!empty($_POST['page'])) {
				$page = $_POST['page'];
			}
			$showDay =  $showFrom_To_Day == NULL ? $showDay : implode("",$showFrom_To_Day);
			$my_page = $page;
			$number_button_page = $this->numb_button;
			$config['total_rows'] = $this->LogModels->total_like($condition);
			$per_page_default = $this->per_page;
			// ajax chọn số hàng hiển thị
			if (!empty($_POST['row']) &&  $_POST['row'] != "") {
				$per_page_default =  $_POST['row'];
			}
			$config['per_page'] = $per_page_default;
			$config['uri_segment'] = 5;
			$config['reuse_query_string'] = true;
			$total_page = ceil($config['total_rows'] / $config['per_page']);
			$page = ($page > $total_page) ? $total_page : $page;
			$page = ($page < 1) ? 1 : $page;
			$page = $page - 1;
	
			if ($config['total_rows'] > 0) {
				$getDatas =  $this->LogModels->select_array_like('*,DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s") as created_at', $condition, 'created_at desc, id desc', ($page * $config['per_page']), $config['per_page']);
			}
			$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '', "?key_word=" . $key_word);
			$data = array(
				'data_index'	=> $this->get_index(),
				'title'			=>	'Quản lý ' . $this->title,
				'template' 		=> 	$this->template . 'index',
				'showDay'		=> $showDay,
				'control'		=>  $this->control,
				'datas'			=>  $getDatas,
				'key_word'		=> $key_word,
				'per_page'  	=> $config['per_page'],
				'my_pagination' => $my_pagination,
				'count'			=> $config['total_rows']
			);
			if ($type_pageload == "ajax") {
				$this->load->view('cpanel/ajax/log', isset($data) ? $data : NULL);
			} else {
				$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
			}
		}
		// pagination
		public function pagination()
		{
			$page = 1;
			$type_pageload = $_POST['type_pageload'];
			if (!empty($_POST['page'])) {
				$page = $_POST['page'];
			}
			$date = date('Y-m-d');
			$currentDay = $date." 00:00:00";
			$condition = array('created_at >=' => $currentDay);
			$showDay 	= 'ngày '.date('d/m/Y');
			$my_page = $page;
			$number_button_page = $this->numb_button;
			$config['total_rows'] = count($this->LogModels->findWhere($condition));
			$per_page_default = $this->per_page;
			if (!empty($_POST['row']) &&  $_POST['row'] != "") {
				$per_page_default =  $_POST['row'];
			}
			$config['per_page'] = $per_page_default;
			$config['uri_segment'] = 3;
			$config['reuse_query_string'] = true;
			$total_page = ceil($config['total_rows'] / $config['per_page']);
			$page = ($page > $total_page) ? $total_page : $page;
			$page = ($page < 1) ? 1 : $page;
			$page = $page - 1;
			if ($config['total_rows'] > 0) {
				$getDatas = $this->LogModels->select_array_wherein('*,DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s") as created_at', $condition, '', NULL, $order = 'created_at desc, id desc', ($page * $config['per_page']), $config['per_page']);
			}
			$my_pagination = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '');
			$data = array(
				'data_index'	=> $this->get_index(),
				'title'			=>	'Quản lý ' . $this->title,
				'template' 		=> 	$this->template . 'index',
				'control'		=>  $this->control,
				'showDay'		=> $showDay,
				'datas'			=>  $getDatas,
				'per_page'  	=> $config['per_page'],
				'my_pagination' => $my_pagination,
				'count'			=> $config['total_rows']
			);
			if ($type_pageload == "ajax") {
				$this->load->view('cpanel/ajax/log', isset($data) ? $data : NULL);
			} else {
				$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
			}
		}
}