<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Typefiles extends Admin_Controller {
	public $template = 'cpanel/typefiles/';
	public $title = 'loại hồ sơ';
	public $control = 'typefiles';
	public $path_dir = 'upload/typefiles/';
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('TypefilesModels');
	}
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->TypefilesModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
    public function checkValue()
    {
        $data_post = $this->input->post('data_post');
        $name = $data_post['name'];
		if($name == ""){
			$this->form_validation->set_message(__FUNCTION__,'Vui lòng nhập tên loại hồ sơ');
			return false;
		}else{
			return true;
		}
    }
    //thêm
    public function add()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
            //var_dump($data_post);die;
			if($data_post != NULL){ 
				$this->form_validation->set_rules('data_post[name]','Tên loại hồ sơ', 'required|callback_checkValue');
				if($this->form_validation->run()){
					//insert data
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                    $data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					 $result = $this->TypefilesModels->add($data_post);
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect(CPANEL.$this->control);
				}
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thêm mới dữ liệu',
			'template' 	=> 	$this->template.'add',
			'control'	=>  $this->control,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

    // chỉnh sửa
    public function edit($id)
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$getDatas = $this->TypefilesModels->find($id);
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
            $data_post['publish'] =  $data_post['publish']==NULL?0:1;
			if($data_post != NULL){  
                $this->form_validation->set_rules('data_post[name]','Tên loại hồ sơ', 'required|callback_checkValue'); // kiểm tra field rỗng
				if($this->form_validation->run()){
					//insert data
                    $data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                   
					 $result = $this->TypefilesModels->edit($data_post,$id,'id');
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect(CPANEL.$this->control);
				}	
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Cập nhật dữ liệu',
			'template' 	=> 	$this->template.'edit',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	// publish
	public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->TypefilesModels->edit($data_update,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->TypefilesModels->delete($id);
	}
}
