<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends Admin_Controller {
	public $template = 'cpanel/user/';
	public $title = 'nhóm người dùng';
	public $control = 'user';
	public $path_url = 'cpanel/user/';
	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('UserModels');
		$this->load->model('EmployeeModels');
		$this->load->model('RoleModels');
	}
	public function index()
	{
		
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->UserModels->getResultArray('*', array('role_id !=' => 0));
		//add role
		foreach ($getDatas as $key => $value) {
			$getRoles = $this->RoleModels->find($value['role_id']);
			if($getRoles != NULL)
			{
				$getDatas[$key]['role_name'] = $getRoles['name'];
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

	public function check_Email()
	{
		$data_post = $this->input->post('data_post');
		$email = $data_post['email'];
		$getFiles = $this->UserModels->find($email, 'email', 'email');
		if($getFiles != NULL){
			$this->form_validation->set_message(__FUNCTION__,'Email <b>'.$email.'</b> đã bị trùng!');
			return false;
		}else{
			return true;
		}
	}
	public function check_username()
	{
		$data_post = $this->input->post('data_post');
		$username = $data_post['username'];
		$getFiles = $this->UserModels->find($username, 'username', 'username');
		if($getFiles != NULL){
			$this->form_validation->set_message(__FUNCTION__,'username <b>'.$username.'</b> đã bị trùng!');
			return false;
		}else{
			return true;
		}
	}
	public function add()
	{
		
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			$text_pass = $data_post['password'];
			if($data_post != NULL){ 
				$this->form_validation->set_rules('data_post[email]','Email', 'required|callback_check_Email');
				$this->form_validation->set_rules('data_post[username]','Username', 'required|callback_check_username');
				if($this->form_validation->run()){
					//lấy thông tin nhân sự
					$getEmployees = $this->EmployeeModels->find($data_post['employeeID']);

					$rand_salt = $this->Encrypts->genRndSalt();
					$password = rand(9,99999999);
					$encrypt_pass = $this->Encrypts->encryptUserPwd($password,$rand_salt);
					$data_post['password']  = $encrypt_pass;
					$data_post['salt'] 	    = $rand_salt;
					$data_post['text_pass'] = $password;
					$data_post['role_id'] 	= 1;
					//insert data
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					$result = $this->UserModels->add($data_post);
					if($result>0){
						$email = $data_post['email'];
						//load primary mail
						$this->load->library('email');
						$subject = 'Thông báo - Thông tin tài khoản đăng nhập hệ thống Hưng Thịnh';
						$title = 'Hưng thịnh HRM';
						$from = 'sentemail.optech@gmail.com';
						// content mail
						$body = "<p>Xin chào ".$getEmployees['fullname'].",</p>";
						$body .= "<p>Thông tin tài khoản của bạn trên hệ thống Hưng Thịnh HR đã được tạo. Vui lòng đăng nhập bằng thông tin bên dưới và cập nhật lại mật khẩu của bạn.</p>";
						$body .= "<ul>";
						$body .= "<li style='list-style: square'><strong>Tài khoản:</strong> ".$email."</li>";
						$body .= "<li style='list-style: square'><strong>Mật khẩu:</strong> ".$password."</li>";
						$body .= "</ul>";
						$body .= "<p>Truy cập: <a href='http://work.hungthinh1989.com/'><strong>Tại đây</strong></a></p>";
						$message = $body;
						$resultSendMail = $this->email
						    ->from($from,$title)
						    ->reply_to('')    
						    ->to(trim($email)) //
						    ->subject($subject)
						    ->message($message)
						    ->send();
						//message sucess
						if($resultSendMail == true){
							$this->session->set_flashdata('message_flashdata', array(
								'type'		=> 'success',
								'message'	=> ADD_SUCCESS,
							));
						}
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect(CPANEL.$this->control);
				}
			}
		}
		//get employees
		$getEmployees =  $this->UserModels->checkExist_account();
		$getRoles = $this->RoleModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thêm mới dữ liệu',
			'template' 	=> 	$this->template.'add',
			'control'	=>  $this->control,
			'path_url'	=>  $this->path_url,
			'datas'	=>  $getEmployees,
			'roles'	=>  $getRoles
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	
	public function check_Password()
	{
		$id = $this->input->post('userID');
		$result = $this->UserModels->find($id, 'salt,password');
		$rand_salt_old = $result['salt'];
		$oldpassword   = $result['password'];
		$oldpassword_post = $this->Encrypts->encryptUserPwd( $this->input->post('old_password'),$rand_salt_old);
		if($oldpassword == $oldpassword_post)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message(__FUNCTION__,'Mật khẩu cũ không chính xác, xin vui lòng nhập lại!');
			return false;
		}
	}

	public function changepass($id)
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$getDatas = $this->UserModels->find($id);
		if($this->input->post()){
			if($this->input->post() != NULL){
				$this->form_validation->set_rules('old_password','Mật khẩu cũ', 'required|callback_check_Password');
				$this->form_validation->set_rules('new_password','Mật khẩu mới', 'required');
				$this->form_validation->set_rules('re_password','Nhập lại mật khẩu mới', 'required|matches[new_password]');
				if($this->form_validation->run()){
					$new_password = $this->input->post('new_password');
					$rand_salt = $this->Encrypts->genRndSalt();
					$encrypt_pass = $this->Encrypts->encryptUserPwd($new_password,$rand_salt);
					$data_update = array(
						'password'			=>	$encrypt_pass,
						'salt'				=>	$rand_salt,
						'text_pass'			=>	$new_password,
						'active'			=>	$this->input->post('active'),
						'updated_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600)
					);

					$result = $this->UserModels->edit($data_update,$id);
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> EDIT_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> EDIT_FAIL,
						));
					}
					redirect(CPANEL.$this->control);
				}
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Đổi mật khẩu',
			'template' 	=> 	$this->template.'changepass',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}

	public function random_pass()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$getEmployees = $this->EmployeeModels->find($id);
		$random_password = rand(10000000,99999999);
		if($getEmployees != NULL){
			echo json_encode(array('employeeID' => $getEmployees['id'], 'fullname' => $getEmployees['fullname'], 'email' => $getEmployees['email'], 'password' => $random_password));
		}else{
			echo json_encode(array('error' => 1 ));
		}
	}

	public function active()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->UserModels->edit($data_update,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	//delete action
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->UserModels->delete($id);
	}
}
