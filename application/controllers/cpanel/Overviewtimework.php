<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Overviewtimework extends Admin_Controller {
	public $template = 'cpanel/overviewtimework/';
	public $title = 'Tổng quan lịch làm việc';
	public $control = 'overviewtimework';
    public $path_url = 'cpanel/overviewtimework/';
	public $type = "employee";
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('TimeworksModels');
		$this->load->model('TimeworksHistoryModels');
		$this->load->model('EmployeeModels');
		$this->load->model('EmployeeHistoryWorkModels');
		// chi nhánh
		$this->load->model('BranchModels');
	}	
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$getbranchs = $this->BranchModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	$this->title,
			'template' 	=> 	$this->template.'index',
            'control'	=>  $this->control,
            'path_url'  =>   $this->path_url,
			'branchs'		=>  $getbranchs,
        );
        // Lấy danh sách ngày trong tuần hiện tại
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function loadEvents()
	{
		// thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
        $data['listTimeWork'] =  $this->TimeworksModels->select_array_wherein_join('tbl_timework.uid as id , 
        tbl_timework.employeeID ,tbl_timework.startwork,tbl_timework.endwork,
        tbl_timework.start, tbl_timework.className, CONCAT(tbl_employees.fullname,": " ,tbl_timework.title) AS  title  ', array("approval"=>1), '', NULL,'', 
        NULL,NULL,
        'tbl_employees','tbl_employees.id = tbl_timework.employeeID','inner');
		echo json_encode($data);
	}

	// lọc theo chi nhánh
	public function filter_branch()
	{
		$branch_id = $_POST['branch_id'];
		$EmployeeHistoryWork = $this->EmployeeHistoryWorkModels->findWhere(array("branchID" => $branch_id ));
		$employeeID_arr = [];
		foreach($EmployeeHistoryWork as $key => $val)
		{
			array_push($employeeID_arr,$val['employeeID']);
		}
		$data['listTimeWork'] =  $this->TimeworksModels->select_array_wherein_join('tbl_timework.uid as id , 
        tbl_timework.employeeID ,tbl_timework.startwork,tbl_timework.endwork,
        tbl_timework.start, tbl_timework.className, CONCAT(tbl_employees.fullname,": " ,tbl_timework.title) AS  title  ', array("approval"=>1), 'employeeID', $employeeID_arr,'', 
        NULL,NULL,
        'tbl_employees','tbl_employees.id = tbl_timework.employeeID','inner');
		
		echo json_encode($data);
	}

}
