<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jobposition extends Admin_Controller
{
	public $template = 'cpanel/jobposition/';
	public $title = 'Vị trí công việc';
	public $control = 'jobposition';
	public $path_url = 'cpanel/jobposition/';

	public function __construct()
	{
		parent::__construct();
		$this->get_index();
        $this->load->model('JobpositionModels');
	}

	public function index()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$job_positon = $this->JobpositionModels->findWhere(array("parentID" => 0),'*', 'id desc');
		$result = $job_positon != NULL ? $this->JobpositionModels->dequy($job_positon, $count =  0) : "";
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>   $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'     => $result,
			'path_url'  =>   $this->path_url,
		);
		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}

    
	// thêm
	public function add()
	{
		// Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		if ($this->input->post()) {
			$data_post = $this->input->post('data_post');
			$data_post['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
			$result = $this->JobpositionModels->add($data_post);
			if ($result['type'] == 'successful') {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'sucess',
					'message'	=> 'Add module successful!!',
				));
			} else {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> 'Add module error!!',
				));
			}
			redirect($this->template . 'index');
		}
	}

	public function edit()
	{
		if ($this->input->post()) {
			$data_post = $this->input->post('data_post');
           // var_dump($data_post);die;
			$duplicate_id = $this->JobpositionModels->find($data_post['parentID'], '*',  'id');
			$id =  $this->input->post('id');
			//var_dump($duplicate_id);die;
			if ($id == $data_post['parentID'] || $duplicate_id['parentID'] == $id) {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> EDIT_FAIL,
				));
                $data_post['parentID'] = $duplicate_id['parentID'];
			}
			$result = $this->JobpositionModels->edit($data_post, $id, 'id');
			if ($result['type'] == 'successful') {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> EDIT_SUCCESS,
				));
			} else {
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> EDIT_FAIL,
				));
			}
			redirect($this->template . 'index');
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		// processed delete.
		$id = $_POST['id'];
		$this->JobpositionModels->deleteWhere('parentID',$id);
		$this->JobpositionModels->delete($id);
	}

	// deleteAjax
	public function deleteAjax()
	{
		$this->JobpositionModels->delete($_POST['id']);
	}

	// creatchart
	public function createchart()
	{
		$orgchart = $this->JobpositionModels->findWhere(array("parentID" => 0), '*', 'id desc');
		$result = $this->OgchartsModels->dequy($orgchart, $count =  0);
		$myArray = [];
		array_push($myArray, (object)[
			'id' => -1,
			'name' => "Cơ cấu",
			'parent' => 0,
		]);
		foreach ($result as $key => $val) {
			$parentID = $val['parentID'];
			if ($val['parentID'] == 0) {
				$parentID = -1;
			}
			array_push($myArray, (object)[
				'id' => $val['id'],
				'name' => $val['name'],
				'parent' => $parentID,
			]);
		}
		echo json_encode($myArray);
	}
}
