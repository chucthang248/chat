<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Humanresource extends Admin_Controller {
	public $template = 'cpanel/humanresource/';
	public $title = 'Nhân sự';
	public $control = 'humanresource';
    public $path_url = 'cpanel/timework/';
	public function __construct(){
		parent::__construct();
		$this->get_index();
        $this->load->model('ModulesModels');
        $this->load->model('ModulesDetailModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
            'control'	=>  $this->control,
            'path_url'  =>   $this->path_url
        );
        $data['datas'] = $this->ModulesModels->getAll();
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}


}
