<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Edustatus extends Admin_Controller {
	public $template = 'cpanel/edustatus/';
	public $title = 'tình trạng học vấn';
	public $control = 'edustatus';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('EdustatusModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
	
		//get fullname - OT1
		$getDatas = $this->EdustatusModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function add()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($data_post != NULL){ 
                $data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                $result = $this->EdustatusModels->add($data_post);
                if($result['type'] == "successful"){
                    $this->session->set_flashdata('message_flashdata', array(
                        'type'		=> 'success',
                        'message'	=> ADD_SUCCESS,
                    ));
                }else{
                    $this->session->set_flashdata('message_flashdata', array(
                        'type'		=> 'error',
                        'message'	=> ADD_FAIL,
                    ));
                }
                redirect(CPANEL.$this->control);
			}
		}
	}
	

	public function edit()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		 
        $data_post = $this->input->post('data_post');
			if($data_post != NULL){ 
                $id = $this->input->post('idRow');
                $data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                $result = $this->EdustatusModels->edit($data_post,$id);
                if($result['type'] == "successful"){
                    $this->session->set_flashdata('message_flashdata', array(
                        'type'		=> 'success',
                        'message'	=> ADD_SUCCESS,
                    ));
                }else{
                    $this->session->set_flashdata('message_flashdata', array(
                        'type'		=> 'error',
                        'message'	=> ADD_FAIL,
                    ));
                }
            
                redirect(CPANEL.$this->control);
			}
	
	}
    public function getDataRow()
	{
		$id = $_POST['id'];
		$dataRow = $this->EdustatusModels->find($id);
		echo json_encode(array('result' => $dataRow));
	}
    public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_update[$properties] = $field;
		$result = $this->EdustatusModels->edit($data_update,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->EdustatusModels->delete($id);
	}
}
