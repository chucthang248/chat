<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Awardproposal extends Admin_Controller {
	public $template = 'cpanel/awardproposal/';
	public $title = 'Giải thưởng';
	public $control = 'awardproposal';
	public $per_page = 10;
	public $numb_button = 3;
	// id , employeeID (người được đề xuất) , prizesID (giải thưởng), user_proposal (người đề xuất)
	//tbl_awardproposal
	// 1: đã duyệt , 2: đang chờ duyệt, 3: không được duyệt
	public function __construct(){
		parent::__construct();
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$this->get_index();
		$this->load->model(['AwardproposalModels','EmployeeModels','PrizesModels','UserModels']);
	}
	//List action - OT2
	public function index()
	{
		$dataAward = $this->AwardproposalModels->getAll();
		$page = 1; 
		$config['total_rows'] = count($dataAward);
		$config['per_page'] = $this->per_page;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = 1;
		$number_button_page = $this->numb_button;
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		
		if ($config['total_rows'] > 0) {
			$data['datas'] = $this->AwardproposalModels->select_array_wherein('*, DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s" ) as created_at', NULL, '', NULL, 'id desc', ($page * $config['per_page']), $config['per_page']);
			// lấy thông tin chi tiết
			foreach($data['datas'] as $key => $val)
			{
				// người được đề xuất
				$rcm_person_name = $this->EmployeeModels->find($val['employeeID'], '*','id');
				$data['datas'][$key]['recommended_person_name'] = $rcm_person_name['fullname']; 
				// người đề xuất
				$pps_person_name = $this->EmployeeModels->find($val['user_proposal'], '*','id');
				$data['datas'][$key]['proposal_person_name'] = $pps_person_name['fullname'];
				// Giaỉ thưởng
				$prizes_name = $this->PrizesModels->find($val['prizesID'], '*','id');
				$data['datas'][$key]['prizes_name'] = $prizes_name['name'];
			}
		}
		$data['my_pagination'] = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '');
		$data['data_index'] = $this->get_index();
		$data['title'] 		= 'Giải thưởng';
		$data['template']	= $this->template.'/index';
		$data['control']    = $this->control;

		$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
	}
	public function formAdd()
	{
		$data['employees'] 	= $this->EmployeeModels->findWhere(array("status !=" => 4),'*');
		$data['prizes'] 	= $this->PrizesModels->getAll();
		$this->load->view('cpanel/awardproposal/formAdd', isset($data) ? $data : NULL);

	}
	public function formEdit()
	{
		$id = $_POST['id'];
		$data['id']			= $id;
		$data['datas'] 		= $this->AwardproposalModels->find($id, '*', 'id');
		$data['employees'] 	= $this->EmployeeModels->findWhere(array("status !=" => 4),'*');
		$data['prizes'] 	= $this->PrizesModels->getAll();
		$this->load->view('cpanel/awardproposal/formEdit', isset($data) ? $data : NULL);
	}
	// admin duyệt 
	public function approval()
	{
		$status = $_POST['status'];
		$idRow = $_POST['idRow'];
		$id = $_POST['id'];
		$data['id']			= $id;
		$data['datas'] 		= $this->AwardproposalModels->find($id, '*', 'id');
		$data['employees'] 	= $this->EmployeeModels->findWhere(array("status !=" => 4),'*');
		$data['prizes'] 	= $this->PrizesModels->getAll();

		// khi có request ajax duyệt
		if(isset($_POST['status']) && !empty($_POST['status']))
		{
			$data_post['status'] = $status;
			$data['datas'] 		= $this->AwardproposalModels->edit($data_post, $idRow);
		}
		$this->load->view('cpanel/awardproposal/approval', isset($data) ? $data : NULL);
	}
	public function add()
	{	
		if ($this->input->post()) {
			$data_post = $this->input->post('data_post');
			// lấy id user đăng nhập
			$id = $this->session->userdata('logged_user');
			//$user_proposal = $this->UserModels->find($id, '*', 'id');
			$data_post['status'] = 2;
			$data_post['user_proposal'] = $id ;
			if ($data_post != NULL) {
				$data_post['created_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$result = $this->AwardproposalModels->add($data_post);
				if ($result > 0) {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> ADD_SUCCESS,
					));
				} else {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> ADD_FAIL,
					));
				}
				redirect(CPANEL . $this->control);
			}
		}
	}
	// cập nhật 
	public function edit()
	{
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		if ($this->input->post()) {
			$id = $this->input->post('idRow');
			$data_post = $this->input->post('data_post');
			if ($data_post != NULL) {
				//push date update into data_post
				$data_post['status'] = 2;
				$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time() + 7 * 3600);
				$result = $this->AwardproposalModels->edit($data_post, $id);
				if ($result > 0) {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'success',
						'message'	=> EDIT_SUCCESS,
					));
				} else {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> EDIT_FAIL,
					));
				}
				redirect(CPANEL . $this->control);
			}
		}
	}

	// PHÂN TRANG 
	public function pagination()
	{
		$page = 1;
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$dataAward = $this->AwardproposalModels->getAll();
		$config['total_rows'] = count($dataAward);
		$config['per_page'] = $this->per_page;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = $page;
		$number_button_page = $this->numb_button;
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		
		if ($config['total_rows'] > 0) {
			$data['datas'] = $this->AwardproposalModels->select_array_wherein('*, DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s" ) as created_at', NULL, '', NULL, 'id desc', ($page * $config['per_page']), $config['per_page']);
			// lấy thông tin chi tiết
			foreach($data['datas'] as $key => $val)
			{
				// người được đề xuất
				$rcm_person_name = $this->EmployeeModels->find($val['employeeID'], '*','id');
				$data['datas'][$key]['recommended_person_name'] = $rcm_person_name['fullname']; 
				// người đề xuất
				$pps_person_name = $this->EmployeeModels->find($val['user_proposal'], '*','id');
				$data['datas'][$key]['proposal_person_name'] = $pps_person_name['fullname'];
				// Giaỉ thưởng
				$prizes_name = $this->PrizesModels->find($val['prizesID'], '*','id');
				$data['datas'][$key]['prizes_name'] = $prizes_name['name'];
			}
		}
		$data['my_pagination'] = $this->Functions->my_pagination($number_button_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), '');
		$data['data_index'] = $this->get_index();
		$data['title'] 		= 'Giải thưởng';
		$data['template']	= $this->template.'/index';
		$data['control']    = $this->control;
		$this->load->view('cpanel/awardproposal/tableAjax', isset($data) ? $data : NULL);
		
	}

	// xóa 
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->AwardproposalModels->delete($id);
	}
}
