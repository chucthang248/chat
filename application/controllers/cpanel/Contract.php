<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contract extends Admin_Controller {
	public $template = 'cpanel/contract/';
	public $title = 'contract';
	public $control = 'contract';
    public $path_url = 'cpanel/contract/';
	
	public function __construct(){
		parent::__construct();
        $this->load->model('ContractModels');
        $this->load->model('EmployeeModels');
        $this->load->model('PositionModels');
	}
	//List action - OT2
    public function check_Code()
    {
        $data_post = $this->input->post('data_post');
		$code = $data_post['code'];
		$getContract = $this->ContractModels->find($code, 'code', 'code');
		if($getContract != NULL){
			$this->form_validation->set_message(__FUNCTION__,'Mã hồ sơ <b>'.$code.'</b> đã bị trùng!');
			return false;
		}else{
			return true;
		}
    }
    function currency_format($number, $suffix = '') {
        if (!empty($number)) {
            return number_format($number, 0, ',', '.') . "{$suffix}";
        }
    }
    public function formatDate($date)
    {
        $changeDate = join("-",array_reverse(explode("/",$date)));    
        return $changeDate;
    }
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
        $getContracts = $this->ContractModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
            'control'	=>  $this->control,
            'path_url'  =>   $this->path_url,
            'datas'     =>  $getContracts,
        );
       
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function add()
	{ 
        $employees = $this->EmployeeModels->getAll();
        $position = $this->PositionModels->getAll();
        if($this->input->post()){
            $data_post = $this->input->post('data_post');
            $this->form_validation->set_rules('data_post[code]','Mã hồ sơ', 'required|callback_check_Code');
            if($data_post != NULL)
            {
                if($this->form_validation->run()){
                    //insert data
                    $data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                    $data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                    $data_post['salary_net'] = join("",preg_split("/[.,]/",$data_post['salary_net']));
                    $data_post['salary_gross'] = join("",preg_split("/[.,]/",$data_post['salary_gross']));
                    $data_post['signing_date'] = $this->formatDate($data_post['signing_date']);
                    $data_post['effective_date'] = $this->formatDate($data_post['effective_date']);
                    $data_post['expiration_date'] = $this->formatDate($data_post['expiration_date']);
                    $data_post['start_date'] = $this->formatDate($data_post['start_date']);
                    $result = $this->ContractModels->add($data_post);
                    if($result>0){
                        $this->session->set_flashdata('message_flashdata', array(
                            'type'		=> 'success',
                            'message'	=> ADD_SUCCESS,
                        ));
                        
                    }else{
                        $this->session->set_flashdata('message_flashdata', array(
                            'type'		=> 'error',
                            'message'	=> ADD_FAIL,
                        ));
                    }
                   
                }
                 redirect(CPANEL.$this->control);
            }
           
		}
     
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thêm hồ sơ',
			'template' 	=> 	$this->template.'add',
			'control'	=>  $this->control,
            'employees'  => $employees ,
            'position'  =>  $position,
		);
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	

	public function edit($id = 0)
	{
        $contracts =  $this->ContractModels->find($id);
       
			$contracts['salary_net'] = $this->currency_format($contracts['salary_net']);
            $contracts['salary_gross'] = $this->currency_format($contracts['salary_gross']);
            $contracts['signing_date'] =  date("d/m/Y", strtotime($contracts['signing_date']) );
            $contracts['effective_date'] =  date("d/m/Y", strtotime($contracts['effective_date']));
            $contracts['expiration_date'] =  date("d/m/Y", strtotime($contracts['expiration_date']) );
            $contracts['start_date'] =  date("d/m/Y", strtotime($contracts['start_date']) );
     
            $employees = $this->EmployeeModels->getAll();
            // vị trí chức danh
            $position = $this->PositionModels->getAll();
            if($this->input->post()){
                $data_post = $this->input->post('data_post');
                if($data_post != NULL){
                    //insert data
                    $data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                    $data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
                    $data_post['salary_net'] = join("",preg_split("/[.,]/",$data_post['salary_net']));
                    $data_post['salary_gross'] = join("",preg_split("/[.,]/",$data_post['salary_gross']));
                    $data_post['signing_date'] = $this->formatDate($data_post['signing_date']);
                    $data_post['effective_date'] = $this->formatDate($data_post['effective_date']);
                    $data_post['expiration_date'] = $this->formatDate($data_post['expiration_date']);
                    $data_post['start_date'] = $this->formatDate($data_post['start_date']);
                     $result = $this->ContractModels->edit($data_post,$id,'id');
                    if($result>0){
                        $this->session->set_flashdata('message_flashdata', array(
                            'type'		=> 'success',
                            'message'	=> ADD_SUCCESS,
                        ));
                        
                    }else{
                        $this->session->set_flashdata('message_flashdata', array(
                            'type'		=> 'error',
                            'message'	=> ADD_FAIL,
                        ));
                    }
                    redirect(CPANEL.$this->control);
                }
            
            }
            
            $data = array(
                'data_index'	=> $this->get_index(),
                'title'		=>	'Thêm hồ sơ',
                'template' 	=> 	$this->template.'edit',
                'control'	=>  $this->control,
                'position'  =>  $position,
                'contracts' =>  $contracts,
                'employees'  => $employees 
            );
		$this->load->view('cpanel/default/index', isset($data)?$data:NULL);
	}
	public function publish()
	{
		
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
        $this->ContractModels->delete($id);
        $this->ModulesDetailModels->delete($id);
	}
    
    // deleteAjax
    public function deleteAjax()
    {
        $this->ModulesDetailModels->delete($_POST['id']);
    }
}