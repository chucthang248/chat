<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auths extends Dashboard_Controller {

	public $path_dir = 'upload/passport/';
	public $path_dir_thumb = 'upload/passport/thumb/';

	public function __construct(){
		parent::__construct();
		$this->load->model('TelegramModels');
		$this->load->model('UserModels');
		$this->load->model('WalletModels');
		$this->load->model('HistoryCPModels');
		$this->load->model('Commission');
		$this->load->model('CountryModels');
		$this->load->model('NumberPhoneModel');
		$this->config->load('email', TRUE);
	}

	//check_Signin - OT1
	public function check_Signin()
	{
		$email = trim($this->input->post('email'));
		$password = $this->input->post('password');
		$type = 'user';
		$login_array = array($email, $password, $type);
		//check capcha google
		$secretGoogle = SECRET_CAPCHA_GOOGLE;
        $captcha = $this->input->post('g-recaptcha-response');
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". $secretGoogle ."&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $result = json_decode($response,true);
        if ($result['success'] == 1) 
        {
        	if($this->Auth->signUser($login_array) === 1){
				$token = "pdq*42*grer*45*dfih*fhs*oa1*".$email."*sdf*481*156*hsd*f";
				$token = rtrim( strtr( base64_encode( $token ), '+/', '-_'), '=');
				redirect(base_url().'check-2fa.html/'.$token);
			}
			if($this->Auth->checkInfoUser($login_array) === true){
				$result = $this->UserModels->find($email, 'active', 'email');
				if($result['active'] == 0)
				{
					$this->form_validation->set_message(__FUNCTION__,'Account not activated, please check your email!');
					return false;
				}
			}
			if($this->Auth->signUser($login_array) === false)
			{
				$this->form_validation->set_message(__FUNCTION__,'Email or password is incorrect');
				return false;
			}else{
				return true;
			}
        }
	}

	//signin - OT1
	public function signin()
	{
		if($this->Auth->checkSignin() === true){redirect(base_url().'dashboard');}
		if($this->input->post()){
			//validation
			$this->form_validation->set_rules('email','Email', 'required|valid_email|callback_check_Signin');
			$this->form_validation->set_rules('password','password', 'required');
			$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'required');
			if($this->form_validation->run()){
				redirect(base_url().'dashboard');
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Sign in',
		);
		$this->load->view('dashboard/auth/signin', isset($data)?$data:NULL);
		
	}

	//check2fa - OT1
	public function check2fa()
	{
		if($this->input->post()){
			
			$this->form_validation->set_rules('google_2fa','2FA Code', 'required');
			//validate run
			if($this->form_validation->run()){
				$token = $this->uri->segment(2);
				$encode = base64_decode($token);
				$string = explode("*", $encode);
				$email = $string[7];

				$google_auth_code = $this->input->post('google_2fa');

				//load library Google 2FA
				$this->load->library('GoogleAuthenticator');

				//get info current user
				$getUser = $this->UserModels->find($email, 'id,google_auth_code', 'email');

				//get info 2fa 
				$googleauthcodeArray = json_decode($getUser['google_auth_code'],true);

				$isValid = $this->googleauthenticator->verifyCode($googleauthcodeArray['secret'], $google_auth_code, 2);

				if(!$isValid){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error', 
						'message'	=> 'Invalid authenticator code',
					));
					redirect(base_url().'check-2fa.html/'.$token);
				}else{
					$this->session->set_userdata('userID', $getUser['id']);
					redirect(base_url().'dashboard');
				}
			}
								
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Check 2FA Code',
		);
		$this->load->view('dashboard/auth/check2fa', isset($data)?$data:NULL);
		
	}

	//check_validate_Google_2fa - OT1
	public function check_Google_2fa(){
		$google_auth_code = $this->input->post('google_2fa');

		//load library Google 2FA
		$this->load->library('GoogleAuthenticator');

		//get info current user
		$user = $this->Auth->getInfoUser();

		//get info 2fa 
		$googleauthcodeArray = json_decode($user['google_auth_code'],true);

		$isValid = $this->googleauthenticator->verifyCode($googleauthcodeArray['secret'], $google_auth_code, 2);

		if(!$isValid){
			$this->form_validation->set_message(__FUNCTION__,'Invalid authenticator code');
			return false;
		}
		return true;
	}

	//check_validate_Email - SignUp - OT1
	public function check_Email(){
		$Email = $this->input->post('email');
		$where = array('email' => $Email);
		if($this->UserModels->check_exists($where)){
			//return message error
			$this->form_validation->set_message(__FUNCTION__,'Email already exists !');
			return false;
		}
		return true;
	}

	// check_validate_ReferentCode - OT1 
	public function check_Referral()
	{
		$code = trim($this->input->post('referral'));
		$result = $this->UserModels->find($code, 'code', 'code');
		if($result == NULL)
		{
			$this->form_validation->set_message(__FUNCTION__,'This code does not exist');
			return false;
		}
		else
		{
			return true;
		}
	}

	//check_capcha google -OT1
	function validate_captcha() {
		$secretGoogle = SECRET_CAPCHA_GOOGLE;
        $captcha = $this->input->post('g-recaptcha-response');
        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=". $secretGoogle ."&response=" . $captcha . "&remoteip=" . $_SERVER['REMOTE_ADDR']);
        $result = json_decode($response,true);
        if ($result['success'] == 1) 
        {
        	return TRUE;
        } else {
        	$this->form_validation->set_message(__FUNCTION__, 'Please check the the captcha form');
            return FALSE;
        }
    }
  
    //check_numberPhone with country prefix - OT1
    public function check_numberPhone(){
    	$countryCode = $this->input->post('countryCode');
    	$numberPhone = $countryCode.$this->input->post('phone');

    	$result = $this->NumberPhoneModel->validNumberPhone($numberPhone);
    	if($result['valid'] == 1){
    		return true;
    	}else {
        	$this->form_validation->set_message(__FUNCTION__, 'Please select the country code and enter the correct phone number');
            return FALSE;
        }
    	
    }

	//signup -+- OT1 
	public function signup()
	{
		//destroy session userID
		$this->session->unset_userdata('userID');
		if($this->input->post()){
			//validation
			$this->form_validation->set_rules('email','Email', 'required|valid_email|min_length[6]|callback_check_Email');
			$this->form_validation->set_rules('fullname','Fullname', 'required|min_length[6]');
			$this->form_validation->set_rules('phone','Phone', 'required|numeric|min_length[8]');
			$this->form_validation->set_rules('password','Password','required|min_length[8]');
			$this->form_validation->set_rules('re_password','Re-Password','required|min_length[8]|matches[password]');
			$this->form_validation->set_rules('g-recaptcha-response', 'recaptcha validation', 'callback_validate_captcha');
			if($this->input->post('referral') != ''){
				$this->form_validation->set_rules('referral', 'Referral', 'callback_check_Referral');
			}
			if($this->form_validation->run()){
				//Random password
				$rand_salt = $this->Encrypts->genRndSalt();
				$encrypt_pass = $this->Encrypts->encryptUserPwd($this->input->post('password'),$rand_salt);
				//get userReferent
				$code = trim($this->input->post('referral'));
				$referentID = $this->UserModels->find($code, 'id', 'code');
				$fullname = trim($this->input->post('fullname'));
				$email = trim($this->input->post('email'));
				$data_insert = array(
					'email' 			=> 	$email,
					'password' 			=> 	$encrypt_pass,
					'text_pass' 		=> 	$this->input->post('password'),
					'fullname' 			=> 	$fullname,
					'phone' 			=> 	$this->input->post('phone'),
					'referentID' 		=> 	($referentID['id'] != '')?$referentID['id']:0,
					'type'				=>	'user',
					'salt' 				=>  $rand_salt,
					'countryID' 		=>  $this->input->post('countryID'),
					'active'			=>	0,
					'created_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600),
					'updated_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600)
				);
				$result = $this->UserModels->add($data_insert);
				$this->random_referralCode($result['id_insert']);
				if($result['type'] == 'successful'){
					//config link active
					$id =$result['id_insert'];
					$token = "pdq*42*grer*45*dfih*fhs*oa1*".$id."*sdf*481*156*hsd*f";
					$token = rtrim( strtr( base64_encode( $token ), '+/', '-_'), '=');
					//load primary mail
					$this->load->library('email');
					$subject = 'Active your account';
					$title = 'BetDefi Team';
					$from = 'no-reply@betdefi.net';
					// content mail
					$body = "<p style='margin-bottom:30px;'>Dear <strong>".$fullname."</strong>,<br/>";
					$body .= "<p>Thank you for your registration.</p>";
					$body .= "<p style='margin-bottom:30px;'>Confirm your email by clicking the button below.</p>";
					$body .= "<p style='text-align:center;'><a style='background: #fedd3c;color: #000000;padding: 10px 25px;font-size: 15px;font-weight: 600;border-radius: 8px;cursor: pointer;' href='".base_url()."active-account.html/".$token."'>Active Now</a></p>";
					$body .= "<p style='margin-top:60px;'>If this was not your own activity, please ignore this email.</p>";
					$body .= "<p>Best regards,</p>";
					$body .= "<p><b>BetDefi Team</b></p>";
					$body .= "<p style='text-align:center'>Copyright © 2020 BetDefi Team</p>";
					$message = $body;
					$resultSendMail = $this->email
					    ->from($from,$title)
					    ->reply_to('')    
					    ->to(trim($email)) //
					    ->subject($subject)
					    ->message($message)
					    ->send();
					//message sucess
					if($resultSendMail == true){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'sucess',
							'message'	=> 'SignUp successful!!',
						));
						redirect(base_url().'check-mail-notify.html');
					}
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> 'SignUp error!!',
					));
					redirect(base_url().'signup.html');
				}
			}
		}

		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Sign up',
		);
		$data['country'] 		= $this->CountryModels->findWhere(array('publish' => 1));
		$data['CountryPrefix']  = $this->NumberPhoneModel->getCountryPrefix();

		$this->load->view('dashboard/auth/signup', isset($data)?$data:NULL);
		
	}
	//Notify Sign up successful - OT1
	public function checkmailNotify()
	{
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Sign up successful ',
			'content'			=>	'Please check your email to complete the registration.',
			'continue'			=>	'signin.html',
			'title_continue'	=>	'Sign In'
		);
		$this->load->view('dashboard/auth/notify', isset($data)?$data:NULL);
	}

	//active account action - OT1
	public function activeAccount(){
		$token = $this->uri->segment(2);
		$encode = base64_decode($token);
		$string = explode("*", $encode);
		$id = $string[7];
		$getUser = $this->UserModels->find($id,'active,referentID,fullname,email');
		$fullname = $getUser['fullname'];
		$email = $getUser['email'];
		if($getUser['active'] == 0){
			//active Account
			$result = $this->UserModels->edit(array('active' => 1), $id);
			// add adress wallet
			if($result['type'] == 'successful'){
				//add wallet for user
				$minID = $this->WalletModels->select_row_min('tbl_wallet', 'id', array('userID' => 0));
				if($minID != NULL){
					$data_update = array(
						'userID' 			=> 	$id,
						'status' 			=> 	1,
					);
					$getUserIDWallet = $this->WalletModels->find($id, 'userID', 'userID');
					if($getUserIDWallet == NULL){
						$updateWallet = $this->WalletModels->edit($data_update, $minID['id']);
						
					}
				}

				//load primary mail
				$this->load->library('email');
				$subject = 'Welcome to BetDefi.net';
				$title = 'BetDefi Team';
				$from = 'no-reply@betdefi.net';
				// content mail
				$body = "<p style='margin-bottom:30px;'>Dear <strong>".$fullname."</strong>,<br/>";
				$body .= "<p>Your account at BetDefi.net has been activated.</p>";
				$body .= "<p style='text-align:center;'><a style='background: #fedd3c;color: #000000;padding: 10px 25px;font-size: 15px;font-weight: 600;border-radius: 8px;cursor: pointer;' target='_blank' href='".base_url()."signin.html'>Login Now</a></p>";
				$body .= "<p>Best regards,</p>";
				$body .= "<p><b>BetDefi Team</b></p>";
				$body .= "<p style='text-align:center'>Copyright © 2020 BetDefi Team</p>";
				$message = $body;
				$resultSendMail = $this->email
				    ->from($from,$title)
				    ->reply_to('')    
				    ->to(trim($email)) //
				    ->subject($subject)
				    ->message($message)
				    ->send();
				//message sucess
				if($resultSendMail == true){
					redirect(base_url().'check-active-notify.html');
				}else{
					redirect(base_url());
				}
			}
		}
	}

	//Notify active account action - OT1
	public function checkactiveNotify()
	{
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Account activation is successful',
			'content'			=>	'Your account is ready to log in',
			'continue'			=>	'signin.html',
			'title_continue'	=>	'Sign In'
		);
		$this->load->view('dashboard/auth/notify', isset($data)?$data:NULL);
	}

	//random referral code - OT1
	public function random_referralCode($id)
	{
		$min = 10000000;
		$max = 99999999;
		$rand_code = mt_rand($min,$max);
		$result = $this->UserModels->find($rand_code, 'code', 'code');
		if($result == NULL)
		{
			$data_update = array(
				'code' 		=> 	$rand_code
			);
			$result = $this->UserModels->edit($data_update, $id);
		}
		else
		{
			do{
				$new_rand_code = mt_rand($min,$max);
				$result = $this->UserModels->find($new_rand_code, 'code', 'code');
				$max = $max + 1;
			}
			while($result['code'] == $new_rand_code);
			$data_update = array(
				'code' 		=> 	$new_rand_code
			);
			$result = $this->UserModels->edit($data_update, $id);
		}
	}

	//check Email Forget - OT1 
	public function check_EmailForget()
	{
		$email = $this->input->post('email');
		if($email != ''){
			$result = $this->UserModels->find($email, 'email,type', 'email');
		}
		if($result == NULL)
		{
			$this->form_validation->set_message(__FUNCTION__,'This email is not in the system!');
			return false;
		}
		else
		{
			if($result['type'] == 'user')
			{
				return true;
			}
			else
			{
				$this->form_validation->set_message(__FUNCTION__,'This email is not in the system!');
				return false;
			}
		}
	}

	//Forget Password - OT1
	public function forgetPassword()
	{
		if($this->input->post()){
			$this->form_validation->set_rules('email','Email','required|valid_email|min_length[8]|callback_check_EmailForget');
			//validate run
			if($this->form_validation->run()){

				$subject = 'Confirm new password';
				$message = 'Message';
				$email = $this->input->post('email');
				$result = $this->UserModels->find($email, 'id, fullname', 'email');
				$id = $result['id'];
				$fullname = $result['fullname'];
				$token = "pdq*42*grer*45*dfih*fhs*oa1*".$id."*sdf*481*156*hsd*f";
				$token = rtrim( strtr( base64_encode( $token ), '+/', '-_'), '=');


				//load primary mail
				$this->load->library('email');
				$subject = 'Confirm to reset Password';
				$title = 'BetDefi Team';
				$from = 'no-reply@betdefi.net';
				// content mail
				$body = "<p style='margin-bottom:30px;'>Dear <strong>".$fullname."</strong>,<br/>";
				$body .= "To confirm your reser password request, please click to below button:";
				$body .= "<p style='text-align:center;'><a style='background: #fedd3c;color: #000000;padding: 10px 25px;font-size: 15px;font-weight: 600;border-radius: 8px;cursor: pointer;' target='_blank' href='".base_url()."reset-password.html/".$token."'>Reset Password</a></p>";
				$body .= "<p>Best regards,</p>";
				$body .= "<p><b>BetDefi Team</b></p>";
				$body .= "<p style='text-align:center'>Copyright © 2020 BetDefi Team</p>";
				$message = $body;
				$resultSendMail = $this->email
				    ->from($from,$title)
				    ->reply_to('')    
				    ->to(trim($email)) //
				    ->subject($subject)
				    ->message($message)
				    ->send();
				//message sucess
				if($resultSendMail == true){
					redirect(base_url().'notify-forget-password.html');
				}
			}
								
		}

		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Forget Password',
		);
		$this->load->view('dashboard/auth/forgetPassword', isset($data)?$data:NULL);
	}
	public function resetPassword(){
		$token = $this->uri->segment(2);
		$encode = base64_decode($token);
		$string = explode("*", $encode);
		$id = $string[7];
		$getUser = $this->UserModels->find($id,'active,referentID,fullname,email');
		if($getUser != ''){
			$password = rand(0,99999999);
			$fullname = $getUser['fullname'];
			$email = $getUser['email'];
			$rand_salt = $this->Encrypts->genRndSalt();
			$encrypt_pass = $this->Encrypts->encryptUserPwd( $password,$rand_salt);
			$data_update = array(
				'password' 			=> 	$encrypt_pass,
				'text_pass' 		=> 	$password,
				'salt' 				=>  $rand_salt,
				'updated_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600)
			);
			$result = $this->UserModels->edit($data_update, $id);
			if($result['type'] == 'successful'){
				//load primary mail
				$this->load->library('email');
				$subject = 'Your Password has changed';
				$title = 'BetDefi Team';
				$from = 'no-reply@betdefi.net';
				// content mail
				$body = "<p style='margin-bottom:30px;'>Dear <strong>".$fullname."</strong>,<br/>";
				$body .= "Your new password is: ".$password;
				$body .= "<p>Best regards,</p>";
				$body .= "<p><b>BetDefi Team</b></p>";
				$body .= "<p style='text-align:center'>Copyright © 2020 BetDefi Team</p>";
				$message = $body;
				$resultSendMail = $this->email
				    ->from($from,$title)
				    ->reply_to('')    
				    ->to(trim($email)) //
				    ->subject($subject)
				    ->message($message)
				    ->send();
				//message sucess
				if($resultSendMail == true){
					redirect(base_url().'reset-password-success.html');
				}
			}else{
				redirect(base_url().'forget-password.html');
			}
		}
	}
	public function resetPasswordSuccess()
	{
		$data = array(
			'data_index'		=>  $this->get_index(),
			'title'				=>	'Your Password has changed',
			'continue'			=>	'signin.html',
			'title_continue'	=>	'Sign In'
		);
		$this->load->view('dashboard/auth/notify', isset($data)?$data:NULL);
	}
	public function notifyforgetPassword()
	{
		$data = array(
			'data_index'		=>  $this->get_index(),
			'title'				=>	'Send notification successful ',
			'content'			=>	'Please check your email to complete the account update',
			'continue'			=>	'signin.html',
			'title_continue'	=>	'Sign In'
		);
		$this->load->view('dashboard/auth/notify', isset($data)?$data:NULL);
	}

	//update (forget) Password - OT1
	public function updatePassword()
	{
		$token = $this->uri->segment(2);
		$encode = base64_decode($token);
		$string = explode("*", $encode);
		$id = $string[7];
		if($this->input->post()){
			$this->form_validation->set_rules('password','Password','required|min_length[8]');
			$this->form_validation->set_rules('re_password','Re-Password','required|min_length[8]|matches[password]');
			//validate run
			if($this->form_validation->run()){
				$rand_salt = $this->Encrypts->genRndSalt();
				$encrypt_pass = $this->Encrypts->encryptUserPwd( $this->input->post('password'),$rand_salt);
				$data_update = array(
					'password' 			=> 	$encrypt_pass,
					'text_pass' 		=> 	$this->input->post('password'),
					'salt' 				=>  $rand_salt,
					'updated_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600)
				);
				$result = $this->UserModels->edit($data_update, $id);
				if($result['type'] == 'successful'){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'sucess',
						'message'	=> 'Update password successful!!',
					));
					redirect('signin.html');
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> 'Update password error!!',
					));
					redirect('signin.html');
				}
			}
								
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Update Password',
		);
		$this->load->view('dashboard/auth/updatePassword', isset($data)?$data:NULL);
	}

	//profile - OT1
	public function profile()
	{
		if($this->Auth->checkSignin() === false){redirect(base_url().'signin.html');}
		$userID = $this->session->userdata('userID');
		$getUser = $this->UserModels->find($userID);
		if($this->input->post()){
			//validation
			$this->form_validation->set_rules('fullname','Fullname', 'required|min_length[6]');
			$this->form_validation->set_rules('phone','Phone', 'required|numeric|min_length[8]');
			$this->form_validation->set_rules('google_2fa','google_2fa', 'required|callback_check_Google_2fa');
			if($this->form_validation->run()){
				//upload avatar image
				if($_FILES["avatar"]["name"]){
					
					if (!is_dir($this->path_dir)){mkdir($this->path_dir);}
					if (!is_dir($this->path_dir_thumb)){mkdir($this->path_dir_thumb);}
					$config['upload_path']	= $this->path_dir;
					$config['allowed_types'] = 'jpg|png|jpeg|gif';
					$config['max_size'] = 5120;
					$this->load->library('upload', $config);
					$this->upload->initialize($config);
					$image_avatar = $this->upload->do_upload("avatar");
					$image_data = $this->upload->data();
					$name_avatar = $image_data['file_name'];

					//Create thumb
					$this->load->library('image_lib');
					$config['image_library'] 	= 'gd2';
					$config['source_image'] 	= $this->path_dir.$image_data['file_name'];
					$config['new_image'] 		= $this->path_dir_thumb.$image_data['file_name'];
					$config['create_thumb'] 	= TRUE;
					$config['maintain_ratio'] 	= TRUE;
					$config['width'] = 400;

					$ar_name_image = explode('.',$image_data['file_name']);
					$name_avatar_thumb = $ar_name_image[0].'_thumb.'.$ar_name_image[1];
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
				}else{
					$name_avatar = '';
					$name_avatar_thumb = $getUser['avatar'];
				}

				$data_update = array(
					'fullname' 			=> 	$this->input->post('fullname'),
					'phone' 			=> 	$this->input->post('phone'),
					'countryID' 		=> 	$this->input->post('countryID'),
					'avatar' 			=> 	$name_avatar_thumb,
					'updated_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600)
				);
				$result = $this->UserModels->edit($data_update,$userID);
				if($result['type'] == 'successful'){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'sucess',
						'message'	=> 'Update profile successful!!',
					));
					redirect(base_url().'dashboard/profile.html');
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> 'Update profile error!!',
					));
					redirect(base_url().'dashboard/profile.html');
				}
			}
		}

		$data = array(
			'data_index'		=>  $this->get_index(),
			'title'				=>	'Profile',
			'path_dir_thumb'	=>	$this->path_dir_thumb,
			'template' 			=> 	'dashboard/auth/profile'
		);
		$data['datas'] = $getUser;
		$data['country'] = $this->CountryModels->findWhere(array('publish' => 1));
		$this->load->view('dashboard/default/index', isset($data)?$data:NULL);
	}

	//check_OldPassword - OT1
	public function check_OldPassword(){
		$id = $this->session->userdata('userID');
		$result = $this->UserModels->find($id, 'salt,password');
		$rand_salt_old = $result['salt'];
		$oldpassword   = $result['password'];
		$oldpassword_post = $this->Encrypts->encryptUserPwd( $this->input->post('old_password'),$rand_salt_old);
		if($oldpassword == $oldpassword_post)
		{
			return true;
		}
		else
		{
			$this->form_validation->set_message(__FUNCTION__,'The old password incorrect!');
			return false;
		}

	}

	//Change Password - OT1
	public function changePassword()
	{
		if($this->Auth->checkSignin() === false){redirect(base_url().'signin.html');}
		if($this->input->post()){
			$this->form_validation->set_rules('old_password','Old Password','required|min_length[8]|callback_check_OldPassword');
			$this->form_validation->set_rules('password','New Password','required|min_length[8]');
			$this->form_validation->set_rules('re_password','Confirm New Password','matches[password]|required|min_length[8]');
			$this->form_validation->set_rules('google_2fa','google_2fa', 'required|callback_check_Google_2fa');
			//validate run
			if($this->form_validation->run()){
				$rand_salt = $this->Encrypts->genRndSalt();
				$encrypt_pass = $this->Encrypts->encryptUserPwd( $this->input->post('password'),$rand_salt);
				$data_update = array(
					'password' 			=> 	$encrypt_pass,
					'text_pass' 		=>  $this->input->post('password'),
					'salt' 				=>  $rand_salt,
					'updated_at'		=>	gmdate('Y-m-d H:i:s', time()+7*3600)
				);
				$userID = $this->session->userdata('userID');
				$result = $this->UserModels->edit($data_update, $userID);
				if($result['type'] == 'successful'){
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'sucess',
						'message'	=> 'Change password successful!!',
					));
					redirect(base_url().'dashboard/change-password.html');
				}else{
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> 'Change password error!!',
					));
					redirect(base_url().'dashboard/change-password.html');
				}
			}
								
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Change Password',
			'template' 		=> 	'dashboard/auth/changePassword'
		);
		$this->load->view('dashboard/default/index', isset($data)?$data:NULL);
	}
	
	
	//list referal - OT1
	public function referal()
	{
		if($this->Auth->checkSignin() === false){redirect(base_url().'signin.html');}
		$userID = $this->session->userdata('userID');
		$getReferalUser = $this->UserModels->findWhere(array('referentID' => $userID, 'active' =>1 ), 'fullname,created_at');
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Referal List',
			'template' 		=> 	'dashboard/auth/referal'
		);
		$data['datas'] = $getReferalUser;
		$this->load->view('dashboard/default/index', isset($data)?$data:NULL);
	}
	//network tree - Main
	public function networktree()
	{
		if($this->Auth->checkSignin() === false){redirect(base_url().'signin.html');}
		$userID = $this->session->userdata('userID');
		$getListMember = $this->UserModels->getUserTree($userID);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Network Tree',
			'template' 		=> 	'dashboard/auth/networktree'
		);
		$data['listTree'] = $getListMember;
		$this->load->view('dashboard/default/index', isset($data)?$data:NULL);
	}
	//notify - OTMain
	public function notify()
	{
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Notify',
			'content'		=>	'Efficiently provide access to installed base core competencies and end end data Interactively target equity.',
		);
		$this->load->view('dashboard/auth/notify', isset($data)?$data:NULL);
	}

	//Logout account - OT1
	function logout(){
		$this->session->unset_userdata('userID');
		redirect(base_url());
	}


	//Google Authenticator - OTMain
	public function googleAuthen(){
		//check checkSignin account AND checkStatus  -OT1
		if($this->Auth->checkSignin() === false){redirect(base_url().'dashboard');}

		//load library Google 2FA
		$this->load->library('GoogleAuthenticator');
		//get info current user
		$user = $this->Auth->getInfoUser();
		//get info 2fa 
		$secret = $this->googleauthenticator->createSecret();
		$qrCodeUrl = $this->googleauthenticator->getQRCodeGoogleUrl(APP_NAME, $user['email'], $secret);
		//push data into array
		$user['2fa']['secret'] = $secret;
		$user['2fa']['qrCodeUrl'] = $qrCodeUrl;

		//submit form
		if ($this->input->post()) {
			$google_auth_code = $this->input->post('google_auth_code');
			$secret = $this->input->post('secret');
			$isValid = $this->googleauthenticator->verifyCode($secret, $google_auth_code, 2);
			if (!$isValid) {
				$this->session->set_flashdata('warning_already', 'Invalid authenticator code');
				redirect(base_url().'dashboard/google-authen.html');
			}else{
				$qrCodeUrl = $this->googleauthenticator->getQRCodeGoogleUrl(APP_NAME, $user['email'], $secret);
				$arrayData2FA = array(
					'secret' 	=> 	$secret,
					'qrCodeUrl'	=>	$qrCodeUrl
				);
				$jsonData2FA = json_encode($arrayData2FA);
				//data update
				$data_update = array(
					'google_auth_code' 		=> 	$jsonData2FA,
					'is_enabled_2fa' 		=> 	1,
				);
				$rsResult = $this->UserModels->edit($data_update, $user['id']);
				if ($rsResult > 0) {
					$this->session->set_flashdata('security_success', 'Enabled Google Authenticator Successful!');
					redirect('dashboard/google-authen.html');
				} else {
					$this->session->set_flashdata('warning_already_security', 'Enabled Google Authenticator error!');
					redirect('dashboard/google-authen.html', $data);
				}
			}
		} 
		//data push into view
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Google Authenticator',
			'user'			=>	$user,
			'template' 		=> 	'dashboard/auth/googleAuthen'
		);
		$this->load->view('dashboard/default/index', isset($data)?$data:NULL);
	}

	//Enabled Google 2FA
	public function disabledGoogle2FA()
	{
		//check checkSignin account AND checkStatus  -OT1
		if($this->Auth->checkSignin() === false){redirect(base_url().'dashboard');}

		//load library Google 2FA
		$this->load->library('GoogleAuthenticator');
		//get info current user
		$user = $this->Auth->getInfoUser();

		//submit form
		if ($this->input->post()) {
			//check Google 2FA
			$google_auth_code = $this->input->post('google_auth_code');
			//get info 2fa 
			$googleauthcodeArray = json_decode($user['google_auth_code'],true);
			$isValid = $this->googleauthenticator->verifyCode($googleauthcodeArray['secret'], $google_auth_code, 2);
			if (!$isValid) {
				$this->session->set_flashdata('warning_already', 'Invalid authenticator code');
				redirect(base_url().'dashboard/google-authen.html');
			}

			//data update
			$data_update = array(
				'is_enabled_2fa' 		=> 	0,
			);
			$rsResult = $this->UserModels->edit($data_update, $user['id']);
			if ($rsResult > 0) {
				$this->session->set_flashdata('security_success', 'Disabled Google Authenticator Successful!');
				redirect('dashboard/google-authen.html');
			} else {
				$this->session->set_flashdata('warning_already_security', 'Disabled Google Authenticator error!');
				redirect('dashboard/google-authen.html', $data);
			}
		}
	}

}