<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Dashboard_Controller {

	public $path_dir = 'upload/passport/';
	public $path_dir_thumb = 'upload/passport/thumb/';

	public function __construct(){
		parent::__construct();
		$this->load->model('UserModels');
		$this->load->model('GameModels');
		$this->load->model('SettingModels');
		$this->load->model('BonusModels');
		$this->load->model('CommissionModels');
		$this->load->model('BetModels');
	}
	//Main action
	public function index()
	{	
		if($this->Auth->checkSignin() === false){redirect(base_url().'signin.html');}
		$userID = $this->session->userdata('userID');
		
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Dashboard',
			'path_dir_thumb'	=>	$this->path_dir_thumb,
			'template' 	=> 	'dashboard/home/index'
		);
		
		//get amount bonus
		$totalBonus = $this->BonusModels->sum(array('userID' => $userID),'amount');
		$data['bonus'] = $totalBonus['amount'];

		//get amount bet
		$totalBets = $this->BetModels->sum(array('user_id' => $userID),'bet');
		$data['totalBet'] = $totalBets['bet'];

		$this->load->view('dashboard/default/index', isset($data)?$data:NULL);
	}
}


