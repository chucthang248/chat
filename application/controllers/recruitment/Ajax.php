<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends Admin_Controller {

	public function __construct(){
		parent::__construct();
		// vị trí công việc
		$this->load->model('OgchartsModels');
		// lịch làm việc
		$this->load->model('TimeworksModels');
		// 
		$this->load->model('recruitment_rankModels');
		// Hình thức tuyển dụng
		$this->load->model('recruitment_formModels');
		// tin tuyển dụng
		$this->load->model('recruitmentModels');
		 // danh sách ứng tuyển
		 $this->load->model('ApplyModels');
		 // lịch phỏng vấn
		 $this->load->model('CalendarinterviewModels');
		//  $this->get_index();
	}
	public function __destruct(){
	}
	
	public function addInterview()
	{	
		$data['recruitment'] = $this->recruitmentModels->getAll();
		$data['apply'] = $this->ApplyModels->applyid_notin_calendarinterivew();
		//var_dump($data['recruitment']);die;
		$this->load->view('recruitment/ajax/calendar_interview', isset($data)?$data:NULL);	
	}
	public function editInterview()
	{	
		$data['recruitment'] = $this->recruitmentModels->getAll();
		$uid = $_POST['uid'];
		$data['apply'] = $this->ApplyModels->applyid_notin_calendarinterivew_id($uid);
		$data['calendar_interview'] = $this->CalendarinterviewModels->find($uid,  '*', 'uid');
		//echo json_encode($datas);
		//var_dump($data['recruitment']);die;
		$this->load->view('recruitment/ajax/calendar_interview', isset($data)?$data:NULL);	
	}
}
