<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendarinterview extends Admin_Controller {
	public $template = 'recruitment/calendarinterview/';
	public $title = 'danh sách lịch phỏng vấn';
	public $control = 'calendarinterview';
    public $main_template = "recruitment/default/index";
	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('CalendarinterviewModels');
		$this->load->model('recruitment_rankModels');
		$this->load->model('recruitment_formModels');
		$this->load->model('recruitmentModels');
		 // danh sách ứng tuyển
		 $this->load->model('ApplyModels');
	}
	//List action - OT2
	public function index()
	{
		// $data['apply'] = $this->ApplyModels->applyid_notin_calendarinterivew();
		// var_dump($data['apply']);die;
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}

		//get fullname - OT1
		//$getDatas = $this->SchoolModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
		);
		$this->load->view($this->main_template, isset($data)?$data:NULL);
	}
   public function add()
   {
	  $data = array( 	
		  	'uid'				=> $_POST['uid'],
			'applyID' 			=>  $_POST['applyID'],
			'recruitmentID' 	=>  $_POST['recruitmentID'],
			'time_interview' 	=>  $_POST['time_interview'],
			'note' 				=>  $_POST['note'],
			'start' 			=> $_POST['start'],
			'created_at' 		=> gmdate('Y-m-d H:i:s', time()+7*3600)
		);
		$this->CalendarinterviewModels->add($data);
		$datas['apply'] =  $this->CalendarinterviewModels->select_array_wherein_join('tbl_calendar_interview.id as main_id, tbl_calendar_interview.uid as id
		, CONCAT(tbl_apply.fullname ,": " ,tbl_calendar_interview.time_interview) AS  title, start, className, tbl_apply.fullname ', NULL, '', NULL,'', 
		NULL,NULL,
		'tbl_apply','tbl_apply.id = tbl_calendar_interview.applyID','inner');
	   echo json_encode($datas);
   }
   public function edit()
   {
	    $uid				= $_POST['uid'];
		$data = array( 	
		'applyID' 			=>  $_POST['applyID'],
		'recruitmentID' 	=>  $_POST['recruitmentID'],
		'time_interview' 	=>  $_POST['time_interview'],
		'note' 				=>  $_POST['note'],
		'start' 			=> $_POST['start'],
		'created_at' 		=> gmdate('Y-m-d H:i:s', time()+7*3600)
		);
		$this->CalendarinterviewModels->edit($data , $uid, $key = 'uid');
		$datas['apply'] =  $this->CalendarinterviewModels->select_array_wherein_join('tbl_calendar_interview.id as main_id, tbl_calendar_interview.uid as id
		, CONCAT(tbl_apply.fullname ,": " ,tbl_calendar_interview.time_interview) AS  title, start, className, tbl_apply.fullname ', NULL, '', NULL,'', 
		NULL,NULL,
		'tbl_apply','tbl_apply.id = tbl_calendar_interview.applyID','inner');
		echo json_encode($datas);
   }

   // xóa
   public function delete()
   {
		$uid	= $_POST['uid'];	
		$this->CalendarinterviewModels->deleteWhere("uid",$uid);
   }
   public function loadEvents()
   {
	  // thay đổi uid thành id  vì fullcalendar nhận field id, mà id thì không được trùng
	  $data['apply'] =  $this->CalendarinterviewModels->select_array_wherein_join('tbl_calendar_interview.id as main_id, tbl_calendar_interview.uid as id
	  , CONCAT(tbl_apply.fullname ,": " ,tbl_calendar_interview.time_interview) AS  title, 	start, className, tbl_apply.fullname ', NULL, '', NULL,'', 
	  NULL,NULL,
	  'tbl_apply','tbl_apply.id = tbl_calendar_interview.applyID','inner');
	  
	   echo json_encode($data);
   }
}
