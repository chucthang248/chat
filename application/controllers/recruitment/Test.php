<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends Admin_Controller {
	public $template = 'recruitment/test/';
	public $title = 'Quản lý bài kiểm tra';
	public $control = 'test';
	public $path_dir = 'upload/test/';
	public $number_page = 3;
	public $per_page 	= 4;
	public $link_test = "bai-kiem-tra.html";
	PUBLIC $link_approvaltest = "approvaltest/index";
	
	public function __construct(){
		parent::__construct();
        $this->load->model(['QuestionModels','TypeQuestionModels','CreatequizModels','QuestionRandomModels','MaketestModels']);
		$this->get_index();
	}
	
	public function index()
	{
        if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
        $question = $this->QuestionModels->getAll('*, DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s") as created_at');
        foreach($question as $key => $val)
        {
           $typeQuestion =  $this->TypeQuestionModels->findOne(array('id' => $val['type_questionID'] ), '*, DATE_FORMAT(created_at, "%d/%m/%Y") as created_at');
           $question[$key]['type_question'] =  $typeQuestion['name'];
        }
		$my_page = 1;
		$number_page = $this->number_page;
		// Tab tạo bài bài kiểm tra
		$allQuiz = $this->CreatequizModels->getAll();
		$totalData = count($allQuiz);
        $page = 1;
		$where = array('publish' => 1);
		$config['total_rows'] = $totalData;
		$config['per_page'] = $this->per_page;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		if ($config['total_rows'] > 0) {
			$data_allQuiz = $this->CreatequizModels->getAllWhere('*,DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s") as created_at', 'id desc', NULL,
			 ($page * $config['per_page']),  $config['per_page']);
		}
        // phân trang
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page);

		//=========== DUYỆT BÀI KIỂM TRA===============
		$approval_exe =  $this->MaketestModels->getAll();
		$totalData_approvalexe = count($approval_exe);
        $page_approval = 1;
		$where = array('publish' => 1);
		$total_rows = $totalData_approvalexe;
		$perpage = $this->per_page;
		$total_page = ceil($total_rows / $perpage);
		$page_approval = ($page_approval > $total_page) ? $total_page : $page_approval;
		$page_approval = ($page_approval < 1) ? 1 : $page_approval;
		$page_approval = $page_approval - 1;
		if ($total_rows > 0) {
			// $data_maketest = $this->MaketestModels->getAllWhere('*,DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s") as created_at', 'id desc', NULL,
			//  ($page_approval * $perpage),  $perpage);
			$data_maketest = $this->MaketestModels->select_array_wherein_join('tbl_maketest.*,DATE_FORMAT(tbl_maketest.created_at, "%d/%m/%Y, %H:%i:%s") as created_at,tbl_create_quiz.name ',
			NULL,'',NULL ,'tbl_maketest.id desc',  ($page_approval * $perpage),  $perpage
			,'tbl_create_quiz','tbl_create_quiz.id = tbl_maketest.create_quizID','inner');

			foreach($data_maketest as $key_maketest => $val_maketest){
				// các câu hỏi , số câu trả đúng - sai 
				$val_maketest["answers_of_createquiz"] =   json_decode($val_maketest["answers_of_createquiz"],true);
				// lấy các ID câu hỏi trong bài kiểm tra
				$questionID = [];
				foreach($val_maketest["answers_of_createquiz"]  as $key  => $val)
				{
					array_push($questionID,$val['questionID']);
				}
				// Where in lấy các câu hỏi từ array questionID câu hỏi 
				$getQuestion  = 	$this->QuestionModels->select_array_wherein('*',  NULL,'id',$questionID);
				// Lấy số câu đúng - sai, tổng số câu hỏi trong bài test
				$answerkey="";  $countFail = 0;
				foreach ($getQuestion as $key => $val) {
					foreach($val_maketest["answers_of_createquiz"] as $key_creatquiz => $val_creatquiz){ 
						if($val_creatquiz['questionID'] == $val['id']   )
						{
							$answerkey= $val_creatquiz['answerkey'];
							break;
						}
					}
					//=========Sắp xếp lại các câu hỏi==========*/
					if ($val['answer'] != NULL) {
						$val['answer'] = json_decode($val['answer']);
						usort($val['answer'], function ($a, $b) {
							return $b->sort > $a->sort;
						});
					}
					//===============số câu sai================*/
					foreach ($val['answer'] as $key_answer => $val_answer) { 
						if($answerkey == $val_answer->alph && $answerkey != $val["correct_answer"] ){ 
							$countFail++;
						}
					}
				}
				// kết quả cuối cùng 
				$data_maketest[$key_maketest]['total_question_item'] = count($getQuestion);
				$data_maketest[$key_maketest]['amoun_correct_question_item'] = count($getQuestion) -  $countFail;
				$data_maketest[$key_maketest]['amoun_false_question_item'] =  $countFail;
			}
			
		}

        // phân trang
		$my_pagination_approval = $this->Functions->my_pagination($number_page, $total_rows, $perpage, $my_page);
	
		$data = array(
			'data_index'				=> $this->get_index(),
            'title'						=>	'Bài kiểm tra',
            'datas'            		 	=>  $question,
			'template' 					=> 	$this->template.'index',
			'control'					=>  $this->control,
			'allQuiz'					=> $data_allQuiz,
			'my_pagination'				=> $my_pagination,
			'link_test'					=> $this->link_test,
			'my_pagination_approval'  	=> $my_pagination_approval,
			'data_maketest'				=> $data_maketest,
			'link_approvaltest'			=> $this->link_approvaltest
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	public function add()
	{
        $type_question = $this->TypeQuestionModels->getAll();
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
        if($this->input->post()){
			$data_post = $this->input->post('data_post');
			$data_post_answer = $this->input->post('data_post_answer');
			$data_post_answer_json = json_encode($data_post_answer);
		
            if($data_post != NULL)
            {
                    $data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					$data_post['answer'] = $data_post_answer_json ;
					$result = $this->QuestionModels->add($data_post);
					if($result['type'] == "successful"){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect('recruitment/'.$this->control);
            }
        }
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Thêm mới dữ liệu',
			'template' 			=> 	$this->template.'add',
			'control'			=>  $this->control,
            'type_question'     => $type_question
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	public function edit($id)
	{
        if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
        $type_question = $this->TypeQuestionModels->getAll();
        $questions =  $this->QuestionModels->findOne(array('id' => $id ));
		$questions['answer'] =  json_decode($questions['answer']);
		usort($questions['answer'], function($a, $b) { return $b->sort < $a->sort; });
        if($this->input->post()){
			$data_post = $this->input->post('data_post');
			$data_post_answer = $this->input->post('data_post_answer');
			$data_post_answer_json = json_encode($data_post_answer);
            if($data_post != NULL)
            {
                    $data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					$data_post['answer'] = $data_post_answer_json ;
					$result = $this->QuestionModels->edit($data_post,$id,'id');
					if($result['type'] == "successful"){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect('recruitment/'.$this->control);
            }
        }
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Cập nhật '.$this->title,
			'template' 	=> 	$this->template.'edit',
            'control'			=>  $this->control,
            'type_question'     => $type_question,
            'question'          => $questions
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}
	// tạo bài kiểm tra
	public function create_quiz()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$amount = $_POST['amount'];
		$name 	= $_POST['name'];
		$questionID = $this->QuestionModels->getAll('id', 'RAND()', '',  $amount);
		$data['created_at']	 = gmdate('Y-m-d H:i:s', time()+7*3600);
		$data['name']	 = $name ;
		$data['amount_questions'] = $amount;
	    $rs =	$this->CreatequizModels->add($data);
		if($rs['type'] == "successful")
		{  
			$id_insert  = $rs['id_insert'];
			foreach($questionID as $key => $vals)
			{
				$randomData['create_quizID'] =  $id_insert;
				$randomData['questionID'] =  $vals['id'];
				$this->QuestionRandomModels->add($randomData);
			}
		}
		$data['link_test'] = $this->link_test;
		$data['allQuiz']   = $this->CreatequizModels->getAll();
		$this->load->view('recruitment/test/loadCreateQuiz', isset($data)?$data:NULL);
		//echo json_encode($questionID);
	}
	// pagination createquiz
	public function pagination_create_quiz()
	{
		$page = 1;
		$type_pageload = $_POST['type_pageload'];
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$allQuiz = $this->CreatequizModels->getAll();
		$my_page = $page;
		$number_page =  $this->number_page;
		$config['total_rows'] = count($allQuiz);
		$per_page_default = $this->per_page;
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		// echo $page ;
		if ($config['total_rows'] > 0) {
			$data_allQuiz = $this->CreatequizModels->getAllWhere('*', 'id desc', NULL,
			($page * $config['per_page']),  $config['per_page']);
		}
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page);
		$data['link_test'] = $this->link_test;
		$data['allQuiz'] = $data_allQuiz;
		$data['my_pagination'] = $my_pagination;
		$this->load->view('recruitment/test/loadCreateQuiz', isset($data) ? $data : NULL);
         // vị trí ứng tuyển
	}
	// pagination createquiz
	public function pagination_approval_quiz()
	{
		$page_approval = 1;
		if (!empty($_POST['page'])) {
			$page_approval = $_POST['page'];
		}
		$my_page = $page_approval;
		$number_page = $this->number_page;
		$approval_exe =  $this->MaketestModels->getAll();
		$totalData_approvalexe = count($approval_exe);
      
		$total_rows = $totalData_approvalexe;
		$perpage = $this->per_page;
		$total_page = ceil($total_rows / $perpage);
		$page_approval = ($page_approval > $total_page) ? $total_page : $page_approval;
		$page_approval = ($page_approval < 1) ? 1 : $page_approval;
		$page_approval = $page_approval - 1;
		if ($total_rows > 0) {
			$data_maketest = $this->MaketestModels->select_array_wherein_join('tbl_maketest.*,DATE_FORMAT(tbl_maketest.created_at, "%d/%m/%Y, %H:%i:%s") as created_at,tbl_create_quiz.name ',
			NULL,'',NULL ,'tbl_maketest.id desc',  ($page_approval * $perpage),  $perpage
			,'tbl_create_quiz','tbl_create_quiz.id = tbl_maketest.create_quizID','inner');

			
			foreach($data_maketest as $key_maketest => $val_maketest){
				// các câu hỏi , số câu trả đúng - sai 
				$val_maketest["answers_of_createquiz"] =   json_decode($val_maketest["answers_of_createquiz"],true);
				// lấy các ID câu hỏi trong bài kiểm tra
				$questionID = [];
				foreach($val_maketest["answers_of_createquiz"]  as $key  => $val)
				{
					array_push($questionID,$val['questionID']);
				}
				// Where in lấy các câu hỏi từ array questionID câu hỏi 
				$getQuestion  = 	$this->QuestionModels->select_array_wherein('*',  NULL,'id',$questionID);
				// Lấy số câu đúng - sai, tổng số câu hỏi trong bài test
				$answerkey="";  $countFail = 0;
				foreach ($getQuestion as $key => $val) {
					foreach($val_maketest["answers_of_createquiz"] as $key_creatquiz => $val_creatquiz){ 
						if($val_creatquiz['questionID'] == $val['id']   )
						{
							$answerkey= $val_creatquiz['answerkey'];
							break;
						}
					}
					//=========Sắp xếp lại các câu hỏi==========*/
					if ($val['answer'] != NULL) {
						$val['answer'] = json_decode($val['answer']);
						usort($val['answer'], function ($a, $b) {
							return $b->sort > $a->sort;
						});
					}
					//===============số câu sai================*/
					foreach ($val['answer'] as $key_answer => $val_answer) { 
						if($answerkey == $val_answer->alph && $answerkey != $val["correct_answer"] ){ 
							$countFail++;
						}
					}
				}
				// kết quả cuối cùng 
				$data_maketest[$key_maketest]['total_question_item'] = count($getQuestion);
				$data_maketest[$key_maketest]['amoun_correct_question_item'] = count($getQuestion) -  $countFail;
				$data_maketest[$key_maketest]['amoun_false_question_item'] =  $countFail;
			}
		}
        // phân trang
		$my_pagination_approval = $this->Functions->my_pagination($number_page, $total_rows, $perpage, $my_page);
		$data['data_maketest'] = $data_maketest;
		$data['my_pagination_approval'] = $my_pagination_approval;
		//echo json_encode($data);
		$this->load->view('recruitment/test/loadApprovalQuiz', isset($data) ? $data : NULL);
		//echo $page_approval;
	}
	// xóa tạo bài kiểm tra
	public function del_createquiz()
	{
		$id = $_POST['id'];
		$this->CreatequizModels->delete($id);
		$this->QuestionRandomModels->deleteWhere('create_quizID',$id);
	}

	// xóa duyệt bài kiểm tra
	public function del_approval_quiz()
	{
		$id = $_POST['id'];
		$this->MaketestModels->delete($id);
	}	
	public function del_question()
	{
		$id = $_POST['id'];
		$this->QuestionModels->delete($id);
	}
}
