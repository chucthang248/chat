<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maketest extends My_Controller {
	public $template = 'recruitment/maketest/';
	public function __construct(){
		parent::__construct();
		$this->load->model(['QuestionModels','TypeQuestionModels',
		'CreatequizModels','QuestionRandomModels','MaketestModels']);
	}
	//Main action
	public function index($id)
	{	
	   $getQuiz = 	$this->CreatequizModels->find($id, '*', 'id');
	  // $getQuestion_Of_Quiz = 	$this->QuestionRandomModels->findWhere(array("create_quizID" => $getQuiz['id'] ), '*');
	 	$getQuestion_Of_Quiz = 	$this->QuestionModels->select_array_wherein_join('tbl_question.*, 
		 tbl_question_random.create_quizID, tbl_question_random.id as question_randomID , tbl_question_random.questionID ',
	   array("tbl_question_random.create_quizID" => $id),'',NULL ,'tbl_question.id desc',  NULL,  NULL 
		,'tbl_question_random','tbl_question_random.questionID = tbl_question.id','inner');
       
		$data['getQuiz'] = $getQuiz;
		$data['getQuestion_Of_Quiz'] = $getQuestion_Of_Quiz;
		$data['template'] = $this->template.'test';
		$this->load->view('recruitment/maketest/index', $data);
	}

	public function donetest()
	{
		$data_post = $this->input->post('data_post');
		$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
		$result = $this->MaketestModels->add($data_post);
		$data['alert'] = "";
		if($result['type'] == 'successful'){
			$data['alert'] = "Bạn đã hoàn thành bài kiểm tra!";
		}
		$data['template'] = $this->template.'donetest';
		$this->load->view('recruitment/maketest/index', $data);
	}

}


