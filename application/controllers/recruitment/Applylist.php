<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Applylist extends Admin_Controller {
	public $template = 'recruitment/applylist/';
	public $title = 'Danh sách ứng tuyển';
	public $path_dir = 'upload/apply/';
	public $control = 'applylist';
	public $main_template = "recruitment/default/index";
    public $per_page = 2;
    public $number_page = 3;
	
	public function __construct(){
		parent::__construct();
		$this->get_index();
        // danh sách ứng tuyển
        $this->load->model('ApplyModels');
		// kinh nghiệm làm việc
		$this->load->model('ApplyWorkexperienceModels');
        // Vị trí ứng tuyển
		$this->load->model('PositionModels');
		//tin tuyển dụng
		$this->load->model('RecruitmentModels');
	}
	//List action - OT2
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//Search event
		$options = array();
		$likeValue = '';
		if($_GET){
			if($_GET['keys'] != ''){
				$likeValue = $_GET['keys'];
			}
			if($_GET['positionID'] != ''){
				$options = array_merge(array('positionID' => $_GET['positionID']), $options);
			}

		}
		$totalData = count($this->ApplyModels->search('*', $options, $likeValue));
        $page = 1;
		$where = array('publish' => 1);
		$config['total_rows'] = $totalData;
		$config['per_page'] = $this->per_page;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$my_page = 1;
		$number_page = $this->number_page;
		$page = ($page > $total_page) ? $total_page : $page;

		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->ApplyModels->search('*', $options, $likeValue, $order = 'id desc', ($page * $config['per_page']), $config['per_page']);
			if($getDatas != NULL){
				foreach ($getDatas as $key => $val) {
					$getDatas[$key]['recruitmentID_apply'] = $this->RecruitmentModels->findOne(array('id' => $val['recruitmentID']));
				}
			}
		}
        // phân trang
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'recruitment/applylist');
		$recruitments = $this->RecruitmentModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'			=>	'Quản lý ' . $this->title,
			'template' 		=> 	$this->template . 'index',
			'control'		=>  $this->control,
			'per_page'  	=> $config['per_page'],
			'my_pagination' => $my_pagination,
			'recruitment'	=> $recruitments,
			'datas'			=>  $getDatas,
			'count'			=> $config['total_rows'],
		);
		$this->load->view($this->main_template, isset($data)?$data:NULL);
	}
  
    // chức năng phân trang ..
	public function pagination($page = 1)
	{
        
		$type_pageload = $_POST['type_pageload'];
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$my_page = $page;
		$number_page =  $this->number_page;
		$config['total_rows'] = count($this->ApplyModels->getAll());
		$per_page_default = $this->per_page;
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 3;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		// echo $page ;
		if ($config['total_rows'] > 0) {
			$getDatas = $this->ApplyModels->select_array_wherein('*', NULL, '', NULL, $order = 'id desc', ($page * $config['per_page']), $config['per_page']);
			if($getDatas != NULL){
				foreach ($getDatas as $key => $val) {
					$getDatas[$key]['recruitmentID_apply'] = $this->RecruitmentModels->findOne(array('id' => $val['recruitmentID']));
				}
			}
		}
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'recruitment/Applylist');
         // vị trí ứng tuyển
         $position = $this->PositionModels->getResultArray('*', array('publish'=> 1));
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows'],
            'position'  => $position
		);
		if ($type_pageload == "ajax") {
			$this->load->view('recruitment/ajax/apply', isset($data) ? $data : NULL);
		} else {
			$this->load->view($this->main_template, isset($data) ? $data : NULL);
		}
	}
	public function search()
	{
		$type_pageload = $_POST['type_pageload'];
		$page = 1;
	
		if (!empty($_POST['key_word']) &&  $_POST['key_word'] != "") {
			$key_word = $_POST['key_word'];
		}
		if (!empty($_POST['page'])) {
			$page = $_POST['page'];
		}
		$where = NULL;
		if($_POST['recruitmentID'] != "")
		{	
			$where = array("recruitmentID" => $_POST['recruitmentID']);
		}
		$my_page = $page;
		$number_page = 3;
		$config['total_rows'] = $this->ApplyModels->total_like($where, $key_word,'fullname');
		$per_page_default =$this->per_page;
		// ajax chọn số hàng hiển thị
		if (!empty($_POST['row']) &&  $_POST['row'] != "") {
			$per_page_default =  $_POST['row'];
		}
		$config['per_page'] = $per_page_default;
		$config['uri_segment'] = 5;
		$config['reuse_query_string'] = true;
		$total_page = ceil($config['total_rows'] / $config['per_page']);
		$page = ($page > $total_page) ? $total_page : $page;
		$page = ($page < 1) ? 1 : $page;
		$page = $page - 1;

		if ($config['total_rows'] > 0) {
			$getDatas =  $this->ApplyModels->select_array_like('*', $where, 'id desc', ($page * $config['per_page']), $config['per_page'], $key_word,'fullname');
			if($getDatas != NULL){
				foreach ($getDatas as $key => $val) {
					$getDatas[$key]['recruitmentID_apply'] = $this->RecruitmentModels->findOne(array('id' => $val['recruitmentID']));
				}
			}
		}
		$my_pagination = $this->Functions->my_pagination($number_page, $config['total_rows'], $config['per_page'], $my_page, base_url(), 'cpanel/applylist/search', "?key_word=" . $key_word);
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý ' . $this->title,
			'template' 	=> 	$this->template . 'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
			'key_word'	=> $key_word,
			'per_page'  => $config['per_page'],
			'my_pagination' => $my_pagination,
			'count'		=> $config['total_rows']
		);
		if ($type_pageload == "ajax") {
			$this->load->view('recruitment/ajax/apply', isset($data) ? $data : NULL);
		} else {
			$this->load->view('cpanel/default/index', isset($data) ? $data : NULL);
		}
	}
	// xem nhanh chi tiết 
	public function viewDetail()
	{
		//check login
		if ($this->Auth->check_logged() === false) {
			redirect(base_url() . 'cpanel/login.html');
		}
		$apply = $this->ApplyModels->find($_POST['id']);
	
		$detailApply = $this->ApplyWorkexperienceModels->findWhere(array('applyID' => $_POST['id']), '*');
		// vị trì ứng tuyển
		//$apply['recruitmentID_apply'] = $this->myPositions($apply['positionID']);
		// ngày sinh
		$apply['birthday'] =  implode("/", array_reverse(explode("-", $apply['birthday'])));
		// vị trì ứng tuyển
		$apply['position_apply'] = $this->RecruitmentModels->find($apply['recruitmentID'], '*', 'id');
		//echo json_encode($apply['position_apply']); die;
		//kiểm tra giá trị và gán dữ liệu Trình Trạng hôn nhân
		$data = array(
			'path_dir'  		=> $this->path_dir,
			'data_index'		=> $this->get_index(),
			'path_dir' 			=> $this->path_dir,
			'apply'				=> $apply,
			'detailApply'		=> $detailApply
		);
		$this->load->view($this->template.'viewInfoDetail', isset($data) ? $data : NULL);
	}
	//delete OT2
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->ApplyWorkexperienceModels->deleteWhere('applyID',$id);
		$this->ApplyModels->delete($id);

	}
	public function myPositions($key)
	{
		$arrayData = array(
	        '1'		=>	'Nhân Viên Bảo Vệ (Security)',
	        '2'		=>	'Giám sát hệ thống camera an ninh (CCTV Supervisor)',
	        '3'		=>	'Nhân viên bán hàng (Sale Associates)',
	        '4'		=>	'Nhân Viên kinh doanh trực tuyến (Online Sale Associates)',
	        '5'		=>	'Nhân viên kho (Storage Keeper)',
	        '6'		=>	'Nhân viên văn thư (File Clerk)',
	        '7'		=>	'Nhân viên chăm sóc khách hàng & bảo dưỡng (customer services & care center workers)',
	        '8'		=>	'Nhân viên nhập liệu (Data Processor)',
	        '9'		=>	'Giám sát kinh doanh trực tuyến (Online Supervisor)',
	        '10'	=>	'Giám sát kinh doanh (Supervisor)',
	        '11'	=>	'Cửa hàng trưởng (Store Manager)',
	        '12'	=>	'Quản lý khu vực (Territorial Manager)',
	        '13'	=>	'Nhân Viên Kế Toán (Accountant)',
	        '14'	=>	'Nhân Viên Kiểm Toán (Auditor)',
	        '15'	=>	'Nhân Viên Kiểm Soát Chất Lượng (QC Worker)',
	        '16'	=>	'Nhân Viên Thẩm Tra Chất Lượng (QA Worker)',
	        '17'	=>	'Nhân Viên Kiểm Soát Hành Chính (Admin Worker)',
	        '18'	=>	'Nhân Viên Marketing (Marketer)',
	        '19'	=>	'Nhân Viên Nhân Sự (HR worker)',
	        '20'	=>	'Nhân Viên Thu Mua (Purchasing Agent)',
	        '21'	=>	'Sáng Tạo Nội Dung (Content Writer)',
	        '22'	=>	'Sáng Tạo Hình Ảnh (Image Producer/Photographer/Graphic Designer)',
	        '23'	=>	'Phân Tích Dữ Liệu (Data Analysist)',
	        '24'	=>	'Nghiên Cứu Thị Trường (Market Researcher)',
	        '25'	=>	'Nghiên Cứu Sản Phẩm (Product Developer)',
	        '26'	=>	'Quản Trị Nhân Sự (HR Manager)',
	        '27'	=>	'Quản Trị Hành Chính (Admin Manager)',
	        '28'	=>	'Quản Trị Cung Ứng (Procurement Executive)',
	        '29'	=>	'Quản Trị Kinh Doanh (Sales Manager)',
	        '30'	=>	'Quản Trị Chăm Sóc Khách Hàng (Care Center Manager)',
	        '31'	=>	'Quản Trị Thông Tin (Information Manager)',
		);
		if($key == NULL)
		{
			return $arrayData;
		}else
		{
			return $arrayData[$key];
		}
	
	}
}
