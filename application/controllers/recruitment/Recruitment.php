<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment extends Admin_Controller {
	public $template = 'recruitment/recruitment/';
	public $title = 'Quản lý tin tuyển dụng';
	public $control = 'recruitment';
	public $path_dir = 'upload/recruitment/';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('FilesModels');
		$this->load->model('recruitment_rankModels');
		$this->load->model('recruitment_formModels');
		$this->load->model('recruitmentModels');
		$this->load->model('Upload');
		// $this->load->library('TCPDF');
		$this->load->library("session");
		$this->load->model('TypefilesModels'); // loại hồ sơ
	}
	
	public function index()
	{
		$city_api = "https://thongtindoanhnghiep.co/api/city";
		$city_resultJson = file_get_contents($city_api);
		
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
        if (isset($_GET['title']) || isset($_GET['form'])) {
           $title = $_GET['title'];
           $form = $_GET['form'];
        }
        else{
            $title = '';
            $form = '';
        }
         $getDatas = $this->recruitmentModels->select_array_wherein_join_3_table('
        tbl_recruitment_rank.name as name_rank,tbl_recruitment_form.name as name_form, tbl_recruitment.*',NULL,'id asc','tbl_recruitment_rank','tbl_recruitment_rank.id = tbl_recruitment.id_recruitment_rank','left','tbl_recruitment_form',
        'tbl_recruitment_form.id = tbl_recruitment.id_recruitment_form','left',array('title'=>$title,'id_recruitment_form'=>$form));
       
        $re_form = $this->recruitment_formModels->getAll();
		$re_rank = $this->recruitment_rankModels->getAll();
		$title_recruitment = $this->recruitmentModels->getResultArray('*',NULL,NULL,NULL,'id desc');
     
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
            'form'      =>$re_form,
            'rank'      =>$re_rank,
			'title_recruitment'		=>$title_recruitment
            // 'list_pagination' =>$this->pagination->create_links()
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	public function check_Code()
	{
		$data_post = $this->input->post('data_post');
		$code = $data_post['code'];
		$getFiles = $this->FilesModels->find($code, 'code', 'code');
		if($getFiles != NULL){
			$this->form_validation->set_message(__FUNCTION__,'Mã hồ sơ <b>'.$code.'</b> đã bị trùng!');
			return false;
		}else{
			return true;
		}
	}

	public function add()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$city_api = "https://thongtindoanhnghiep.co/api/city";
		$city_result = file_get_contents($city_api);
		//var_dump(json_decode($city_result, true));
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($this->input->post()){
				//upload avatar
				$alias = $data_post['alias'];
				//var_dump($data_post['alias']);die;
				$data_post['alias'] = $this->recruitmentModels->createdAlias($alias);
				//var_dump($data_post['alias']);die;
				if ($_FILES["avatar"]["name"]) {
					if (!is_dir($this->path_dir)){mkdir($this->path_dir);}
					$filename = $_FILES['avatar']['name'];
					$path_dir = $this->path_dir;
					$data_upload = $this->Upload->upload_image($path_dir, $filename, 'avatar', 'avatar', 300);
					if ($data_upload['type'] == 'error') {
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> $data_upload['message'],
						));
						redirect($_SERVER['HTTP_REFERER']);
					} else {
						$data_post['avatar'] =  base_url().$this->path_dir.$data_upload['image'];
					}
				}
				$data_post['date_submit'] = $this->Functions->changFormatDate($data_post['date_submit']);
				if($data_post != NULL){ 
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					$result = $this->recruitmentModels->add($data_post);
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect('recruitment/'.$this->control);
				}
			}
		}
        $re_form = $this->recruitment_formModels->getAll();
		$re_rank = $this->recruitment_rankModels->getAll();
		$data = array(
			'data_index'		=> $this->get_index(),
			'title'				=>	'Thêm mới dữ liệu',
			'template' 			=> 	$this->template.'add',
			'control'			=>  $this->control,
            'form'      		=>$re_form,
			'city' 				=>json_decode($city_result, true),
            'rank'      		=>$re_rank,
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}
	function get_district(){
		$district_api = "https://thongtindoanhnghiep.co/api/city/{$_POST['city_id']}/district";
		$district_result = file_get_contents($district_api);
		$district = json_decode($district_result, true);
		echo json_encode(array('data' => $district));
	}
	public function edit($id)
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//lấy dữ liệu load vào form
		$getData = $this->recruitmentModels->find($id,null,'id');
		$avatar = $getData['avatar'];
		$city_api = "https://thongtindoanhnghiep.co/api/city";
		$city_result = file_get_contents($city_api);
		$city_result = json_decode($city_result,true);
		$name_district = "https://thongtindoanhnghiep.co/api/city/{$getData['city']}/district";
		$district_result = file_get_contents($name_district);
		$district_result = json_decode($district_result,true);
		// lấy thông tin chi tiết 1 quận huyện
		$name_detail_district = "https://thongtindoanhnghiep.co/api/district/{$getData['district']}";
		$district_detail_result = file_get_contents($name_detail_district);
		$district_detail_result = json_decode($district_detail_result,true);
		if($this->input->post()){ //nếu submit thì tiến hành cập nhật
			$data_post = $this->input->post('data_post');
			//lấy dữ liệu từ form gán vô mảng dataUpdate
			$alias = $data_post['alias'];
			$data_post['alias'] = $this->recruitmentModels->createdAlias($alias,$getData['alias']);
			// echo $data_post['alias'];die;
			if ($_FILES["avatar"]["name"]) {
				//delete old image
				$file_avatar = $this->path_dir . '/' . $getData['avatar'];
				if (file_exists($file_avatar)) {
					unlink($file_avatar);
				}
				//upload new image
				$filename = $_FILES['avatar']['name'];
				$path_dir = $this->path_dir ;
				$data_upload = $this->Upload->upload_image($path_dir, $filename, 'avatar', 'avatar', 300);
				if ($data_upload['type'] == 'error') {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> $data_upload['message'],
					));
					redirect($_SERVER['HTTP_REFERER']);
				} else {
					$data_post['avatar'] = base_url().$this->path_dir.$data_upload['image'];
				}
			} else {
				$data_post['avatar'] = $getData['avatar'];
			}
		
			//nếu publish không được checked thì set publish = 0
			if (!isset($data_post['publish'])) {
				$data_post['publish'] = 0;
			}
			
			//gán thời gian update dữ liệu
			$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
            $data_post['date_submit'] = $this->Functions->changFormatDate($data_post['date_submit']);
            // format date
			//cập nhật dữ liệu
			$result = $this->recruitmentModels->edit($data_post,$id);
			
			if($result>0){
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> EDIT_SUCCESS,
				));
			}else{
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> EDIT_FAIL,
				));
			}
			redirect('recruitment/'.$this->control);
		}
        $re_form = $this->recruitment_formModels->getAll();
		$re_rank = $this->recruitment_rankModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Cập nhật '.$this->title,
			'template' 	=> 	$this->template.'edit',
			'control'	=>  $this->control,
			'dataRow'	=>  $getData,
            'form'      =>$re_form,
            'rank'      =>$re_rank,
			'city' 		=> $city_result,
			'path_dir'	=> $this->path_dir,
			'district' => $district_detail_result
		);
	
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	//ajax showcontent (index)


	public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_post[$properties] = $field;
		$result = $this->recruitmentModels->edit($data_post,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$getData = $this->recruitmentModels->find($id,null,'id');
		$file_avatar = $this->path_dir . '/' . $getData['avatar'];
		$file_avatar_thumb = $this->path_dir .'/' .'thumb/*';
		$files = glob($file_avatar_thumb); // get all file names
		foreach($files as $file){ // iterate files
			if(is_file($file)) {
				unlink($file); // delete file
			}
		}
		if (file_exists($file_avatar)) {
			unlink($file_avatar);
			
		}
		$this->recruitmentModels->delete($id);
	}
    function searchData(){
        $title = $_POST['title'];
        $form = $_POST['form'];
        $getDatas = $this->recruitmentModels->select_array_wherein_join_3_table('
        tbl_recruitment_rank.name as name_rank,tbl_recruitment_form.name as name_form, tbl_recruitment.*', null, '', NULL,'id asc', NULL,NULL,
        'tbl_recruitment_rank','tbl_recruitment_rank.id = tbl_recruitment.id_recruitment_rank','inner','tbl_recruitment_form',
        'tbl_recruitment_form.id = tbl_recruitment.id_recruitment_form','inner',array("id_recruitment_form"=>$form,'title'=>$title));
        $data = array(
            'datas'=>$getDatas, 
        );

        $this->load->view('cpanel/ajax/recruitment/viewsearch', isset($data)?$data:NULL);

    }
    public function report()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thống kê - báo cáo',
			'template' 	=> 	$this->template.'report',
			'control'	=>  $this->control,
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}
	// tạo alias 
	public function createSlug()
    {
		
    	$slug = $this->recruitmentModels->createAliasAdd($_POST['slug']);
		
		echo json_encode(array('slug' => $slug));
    }
}
