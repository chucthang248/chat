<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Approvaltest extends Admin_Controller {
	public $template = 'recruitment/approvaltest/';
    public $control  = "approvaltest";
	public function __construct(){
		parent::__construct();
		$this->load->model(['QuestionModels','TypeQuestionModels',
		'CreatequizModels','QuestionRandomModels','MaketestModels']);
	}
	//Main action
	public function index($id)
	{	
        $getMaketest = 	$this->MaketestModels->find($id, '*', 'id');
        $getMaketest['quiz_name'] = $this->CreatequizModels->find($getMaketest['create_quizID'], '*', 'id');
        $getMaketest['quiz_name'] = $getMaketest['quiz_name']['name'];
        $getMaketest["answers_of_createquiz"] =   json_decode($getMaketest["answers_of_createquiz"],true);
            // lấy tên bài kiểm tra
         $getCreatQuiz = $this->CreatequizModels->find($getMaketest['create_quizID'], '*', 'id');
        // lấy các câu hỏi trong bài kiểm tra
        $questionID = [];
        foreach($getMaketest["answers_of_createquiz"]  as $key  => $val)
        {
            array_push($questionID,$val['questionID']);
        }
         $getQuestion  = 	$this->QuestionModels->select_array_wherein('*',  NULL,'id',$questionID);
         //===========================
         $answerkey="";  $countFail = 0;
         foreach ($getQuestion as $key => $val) {
            foreach($getMaketest["answers_of_createquiz"] as $key_creatquiz => $val_creatquiz){ 
                if($val_creatquiz['questionID'] == $val['id']   )
                {
                     $answerkey= $val_creatquiz['answerkey'];
                     break;
                }
             }
             //=========
             if ($val['answer'] != NULL) {
                $val['answer'] = json_decode($val['answer']);
                usort($val['answer'], function ($a, $b) {
                    return $b->sort > $a->sort;
                });
             }
            //========= số câu sai ==========*/
            foreach ($val['answer'] as $key_answer => $val_answer) { 
                if($answerkey == $val_answer->alph && $answerkey != $val["correct_answer"] ){ 
                    $countFail++;
                 }
            }
         }
       
        $data['data_index']				= $this->get_index();
		$data['template']               = $this->template.'index';
        $data['getQuestion_Of_Quiz']    = $getQuestion;
        $data['countFail']              = $countFail;
        $data['countCorrect']           = count($getQuestion) - $countFail;
        $data['getMaketest']            = $getMaketest;
        $data['control']                = $this->control;
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}


}


