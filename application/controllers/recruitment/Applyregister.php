<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Applyregister extends My_Controller
{
	public $template = 'cpanel/apply/';
	public $title = 'nhân sự';
	public $control = 'apply';
	public $path_dir = 'upload/apply/';
	public $path_dir_file = 'upload/apply/file/';
	public function __construct()
	{
		parent::__construct();
		$this->load->model('EmployeeModels');
		$this->load->model('StandardModels');
		// vị trí ứng tuyển
		$this->load->model('PositionModels');
		// Upload
		$this->load->model('Upload');
		// kinh nghiệm làm việc
		$this->load->model('ApplyWorkexperienceModels');
		// ứng tuyển
		$this->load->model('ApplyModels');
		//tin tuyển dụng
		$this->load->model('recruitmentModels');
		
	}
	public function index()
	{
		
		$city_api = "https://thongtindoanhnghiep.co/api/city";
		$city_result = file_get_contents($city_api);
		$data['position'] = $this->PositionModels->getResultArray('*',array('publish' => 1));
		$data['city'] = json_decode($city_result, true);
		$data['city_temporary'] = json_decode($city_result, true);
		$data['standard'] =  $this->StandardModels->getAll();
		$data['register'] = ''; 
		$data['recruitments'] = $this->recruitmentModels->findWhere(array('publish' => 1),'id,title');
		$this->load->view('recruitment/registerappply/index', isset($data) ? $data : NULL);
	}

	public function addApply()
	{
		$data_post = $this->input->post('data_post_apply');
		$data_post_apply_exp = $this->input->post('data_post_apply_exp');
		// kiểm tra thể loại cv (file pdf, doc, docx hay là link)
		$data_cv_link = $this->input->post('data_cv_link');
		if ($data_post != NULL) {
			// format ngày sinh 
			$data_post['birthday'] = $this->Functions->changFormatDate($data_post['birthday']);
			// thành phố
			$province_city_name   =  file_get_contents("https://thongtindoanhnghiep.co/api/city/{$data_post['province_city']}");
			$province_city_name = json_decode($province_city_name, true);
			$data_post['province_city'] = $province_city_name['Title'];

			$district_name =   file_get_contents("https://thongtindoanhnghiep.co/api/district/{$data_post['district']}");
			$district_name = json_decode($district_name, true);
			$data_post['district'] = $district_name['Title'];

			// thành phố (tạm trú)
			$province_city_name_temporary   =  file_get_contents("https://thongtindoanhnghiep.co/api/city/{$data_post['province_city_temporary']}");
			$province_city_name_temporary = json_decode($province_city_name_temporary, true);
			$data_post['province_city_temporary'] = $province_city_name_temporary['Title'];
			// quận huyện (tạm trú)
			$district_name_temporary =   file_get_contents("https://thongtindoanhnghiep.co/api/district/{$data_post['district_temporary']}");
			$district_name_temporary = json_decode($district_name_temporary, true);
			$data_post['district_temporary'] = $district_name_temporary['Title'];
			// phường xã 
			$wards_name_temporary =   file_get_contents("https://thongtindoanhnghiep.co/api/ward/{$data_post['wards_temporary']}");
			$wards_name_temporary = json_decode($wards_name_temporary, true);
			$data_post['wards_temporary'] = $wards_name_temporary['Title'];
			//upload avatar
			if ($_FILES["avatar"]["name"]) {
				$filename = $_FILES['avatar']['name'];
				$data_upload = $this->Upload->upload_image($this->path_dir, $filename, 'avatar', 'avatar', 300);
				if ($data_upload['type'] == 'error') {
					$this->session->set_flashdata('message_flashdata', array(
						'type'		=> 'error',
						'message'	=> $data_upload['message'],
					));
					redirect($_SERVER['HTTP_REFERER']);
				} else {
					$data_post['avatar'] = $data_upload['image'];
				}
			}
			if ($_FILES['data_cv_file'] != NULL) {
				$data_post['type_cv'] = 'file';
				$path = $this->path_dir_file;
				if (!is_dir($path)) {
					mkdir($path);
				}
				$config['upload_path']	= $path;
				$config['file_name'] = $_FILES["data_cv_file"]['name'];
				$config['allowed_types'] = array('pdf', 'doc', 'docx');
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				$this->upload->do_upload("data_cv_file");
				$file_data = $this->upload->data();
				$data_post['cv'] = $file_data['file_name'];
			}
			if ($data_cv_link != "") {
				$data_post['type_cv'] = 'link';
				$data_post['cv'] = $data_cv_link;
			}
			$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
			$result = $this->ApplyModels->add($data_post);
			$id_insert = $result['id_insert'];
			$this->session->set_flashdata('mess_register', array(
				'name'		=> $data_post['fullname'],
				'position'	=> $this->myPositions($data_post['positionID']),
			));
			// thêm kinh nghiệm làm việc
			if($data_post_apply_exp != NULL)
			{
				foreach($data_post_apply_exp as $key_exp => $val_exp)
				{
					$val_exp['applyID'] = $id_insert;
					$this->ApplyWorkexperienceModels->add($val_exp);
				}
			}
			redirect('ung-tuyen-thanh-cong.html');
		}
	}
	
	public function applysuccess()
	{
		$data['register'] = 'success'; 
		$this->load->view('recruitment/registerappply/index', isset($data) ? $data : NULL);
	}
	public function getDistrict()
	{

		$district_api = "https://thongtindoanhnghiep.co/api/city/{$_POST['city_id']}/district";
		$district_result = file_get_contents($district_api);
		$district = json_decode($district_result, true);
		echo json_encode(array('data' => $district));
	}
	public function getWards()
	{
		$wards_api = "https://thongtindoanhnghiep.co/api/district/{$_POST['district_id']}/ward";
		$wards_result = file_get_contents($wards_api);
		$wards = json_decode($wards_result, true);
		echo json_encode(array('data' => $wards));

	}
	public function valiedateimage()
	{
		$alert = "";
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		if (false === $ext = array_search($finfo->file($_FILES['image']['tmp_name']), array('jpg' => 'image/jpeg', 'png' => 'image/png'), true)) {
			$alert = "Chỉ hỗ trợ jpg, jpge, png";
			echo $alert;
			die;
		}
		if ($_FILES['image']['size'] > 2000000) { //10 MB (size is also in bytes)
			$alert = "Dung lượng hình không được quá 2MB";
			echo $alert;
			die;
		}
	}
	public function valiedatefile()
	{
		$alert = "";
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		if (false === $ext = array_search($finfo->file($_FILES['cv']['tmp_name']), array('pdf' => 'application/pdf'), true)) {
			$alert = "Chỉ hỗ trợ pdf, doc, docx";
			echo $alert;
			die;
		}
		if ($_FILES['image']['size'] > 2000000) { //10 MB (size is also in bytes)
			$alert = "Dung lượng hình không được quá 10MB";
			echo $alert;
			die;
		}
	}

	public function myPositions($key)
	{
		$arrayData = array(
	        '1'		=>	'Nhân Viên Bảo Vệ (Security)',
	        '2'		=>	'Giám sát hệ thống camera an ninh (CCTV Supervisor)',
	        '3'		=>	'Nhân viên bán hàng (Sale Associates)',
	        '4'		=>	'Nhân Viên kinh doanh trực tuyến (Online Sale Associates)',
	        '5'		=>	'Nhân viên kho (Storage Keeper)',
	        '6'		=>	'Nhân viên văn thư (File Clerk)',
	        '7'		=>	'Nhân viên chăm sóc khách hàng & bảo dưỡng (customer services & care center workers)',
	        '8'		=>	'Nhân viên nhập liệu (Data Processor)',
	        '9'		=>	'Giám sát kinh doanh trực tuyến (Online Supervisor)',
	        '10'	=>	'Giám sát kinh doanh (Supervisor)',
	        '11'	=>	'Cửa hàng trưởng (Store Manager)',
	        '12'	=>	'Quản lý khu vực (Territorial Manager)',
	        '13'	=>	'Nhân Viên Kế Toán (Accountant)',
	        '14'	=>	'Nhân Viên Kiểm Toán (Auditor)',
	        '15'	=>	'Nhân Viên Kiểm Soát Chất Lượng (QC Worker)',
	        '16'	=>	'Nhân Viên Thẩm Tra Chất Lượng (QA Worker)',
	        '17'	=>	'Nhân Viên Kiểm Soát Hành Chính (Admin Worker)',
	        '18'	=>	'Nhân Viên Marketing (Marketer)',
	        '19'	=>	'Nhân Viên Nhân Sự (HR worker)',
	        '20'	=>	'Nhân Viên Thu Mua (Purchasing Agent)',
	        '21'	=>	'Sáng Tạo Nội Dung (Content Writer)',
	        '22'	=>	'Sáng Tạo Hình Ảnh (Image Producer/Photographer/Graphic Designer)',
	        '23'	=>	'Phân Tích Dữ Liệu (Data Analysist)',
	        '24'	=>	'Nghiên Cứu Thị Trường (Market Researcher)',
	        '25'	=>	'Nghiên Cứu Sản Phẩm (Product Developer)',
	        '26'	=>	'Quản Trị Nhân Sự (HR Manager)',
	        '27'	=>	'Quản Trị Hành Chính (Admin Manager)',
	        '28'	=>	'Quản Trị Cung Ứng (Procurement Executive)',
	        '29'	=>	'Quản Trị Kinh Doanh (Sales Manager)',
	        '30'	=>	'Quản Trị Chăm Sóc Khách Hàng (Care Center Manager)',
	        '31'	=>	'Quản Trị Thông Tin (Information Manager)',
		);
		return $arrayData[$key];
	}
}
