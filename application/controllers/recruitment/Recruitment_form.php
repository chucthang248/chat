<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment_form extends Admin_Controller {
	public $template = 'recruitment/recruitment_form/';
	public $title = 'Quản lý tuyển dụng';
	public $control = 'recruitment_form';
	public $path_dir = 'upload/recruitment_form/';

	
	public function __construct(){
		parent::__construct();
		$this->get_index();
		$this->load->model('FilesModels');
		$this->load->model('recruitment_formModels');
		$this->load->model('Upload');
		// $this->load->library('TCPDF');
		$this->load->library("session");
		$this->load->model('TypefilesModels'); // loại hồ sơ
	}
	public function index()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//get fullname - OT1
		$getDatas = $this->recruitment_formModels->getAll();
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Quản lý '. $this->title,
			'template' 	=> 	$this->template.'index',
			'control'	=>  $this->control,
			'datas'		=>  $getDatas,
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	public function check_Code()
	{
		$data_post = $this->input->post('data_post');
		$code = $data_post['code'];
		$getFiles = $this->FilesModels->find($code, 'code', 'code');
		if($getFiles != NULL){
			$this->form_validation->set_message(__FUNCTION__,'Mã hồ sơ <b>'.$code.'</b> đã bị trùng!');
			return false;
		}else{
			return true;
		}
	}

	public function add()
	{
		// Check login
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		if($this->input->post()){
			$data_post = $this->input->post('data_post');
			if($this->input->post()){
				$data_post = $this->input->post('data_post');
				if($data_post != NULL){ 
					$data_post['created_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
					$result = $this->recruitment_formModels->add($data_post);
					if($result>0){
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'success',
							'message'	=> ADD_SUCCESS,
						));
						
					}else{
						$this->session->set_flashdata('message_flashdata', array(
							'type'		=> 'error',
							'message'	=> ADD_FAIL,
						));
					}
					redirect('recruitment'.'/'.$this->control);
				}
			}
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Thêm mới dữ liệu',
			'template' 	=> 	$this->template.'add',
			'control'	=>  $this->control,
			//'datas'		=> $getDatas,
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	public function edit($id)
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		//lấy dữ liệu load vào form
		$getData = $this->recruitment_formModels->find($id,null,'id');
		if($this->input->post()){ //nếu submit thì tiến hành cập nhật
			//lấy dữ liệu từ form gán vô mảng dataUpdate
			$data_post = $this->input->post('data_post');
			//nếu publish không được checked thì set publish = 0
		
			//gán thời gian update dữ liệu
			$data_post['updated_at'] = gmdate('Y-m-d H:i:s', time()+7*3600);
			//cập nhật dữ liệu
			$result = $this->recruitment_formModels->edit($data_post,$id);
			if($result>0){
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'success',
					'message'	=> EDIT_SUCCESS,
				));
			}else{
				$this->session->set_flashdata('message_flashdata', array(
					'type'		=> 'error',
					'message'	=> EDIT_FAIL,
				));
			}
			redirect('recruitment'.'/'.$this->control);
		}
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Cập nhật '.$this->title,
			'template' 	=> 	$this->template.'edit',
			'control'	=>  $this->control,
			'dataRow'	=>  $getData,
		);
	
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}

	//ajax showcontent (index)


	public function publish()
	{
		if($this->Auth->check_logged()===false){redirect(base_url().'cpanel/login.html');}
		$id = $_POST['id'];
		$field = $_POST['field'];
		$properties = $_POST['properties'];
		$data_post[$properties] = $field;
		$result = $this->recruitment_formModels->edit($data_post,$id);
		if($result){
			echo json_encode(array('result' => 1));
		}
	}
	public function delete()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		// processed delete.
		$id = $_POST['id'];
		$this->recruitment_formModels->delete($id);
	}

}
