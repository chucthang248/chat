<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->get_index();
	}	
	//Main action
	public function index()
	{
		//Check login
		if($this->Auth->check_logged() === false){redirect(base_url().'cpanel/login.html');}
		
		$data = array(
			'data_index'	=> $this->get_index(),
			'title'		=>	'Dashboard',
			'template' 	=> 	'recruitment/home/index',
		//	'data'		=> 	 $data
		);
		$this->load->view('recruitment/default/index', isset($data)?$data:NULL);
	}
}
