<div class="container-fluid">
	<div class="base_wrapper">
        <div class="card_head">
            <h2 class="base_title"><strong> Profile Setting</strong></h2>
        </div>
    </div>
	<div class="row justify-content-center">
		<div class="col-xl-3 col-md-5 col-12">
			<div class="card settings_menu">
				<div class="card-header"><h4 class="card-title">Settings</h4></div>
				<div class="card-body">
					<ul>
						<li class="nav-item border-b">
							<a class="nav-link" href="dashboard/profile.html"><i class="fas fa-user"></i><span>Update Profile</span></a>
						</li>
						<li class="nav-item border-b">
							<a class="nav-link active" href="dashboard/change-password.html"><i class="fas fa-key"></i><span>Change password</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="dashboard/google-authen.html"><i class="fas fa-lock"></i><span>Google Authenticator</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xl-5 col-md-7 col-12">
			<?php $message_flashdata = $this->session->flashdata('message_flashdata');
			if(isset($message_flashdata) && count($message_flashdata)){ ?>
			    <div id="alerttopfix" class="myadmin-alert alert-success myadmin-alert-top-right" style="display: block;">
			        <?php if($message_flashdata['type'] == 'sucess'){?> 
			          	<i class="la la-check"></i> <?php echo $message_flashdata['message']; ?>
			        <?php }else if($message_flashdata['type'] == 'error'){?>
			          	<i class="la la-close"></i> <?php echo $message_flashdata['message']; ?>
			        <?php } ?>
			  	</div>
			<?php } ?> 
			<form action="" method="POST">
				<div class="card">
					<div class="card-body">
						<div class="form-row">
							<div class="form-group col-xl-12">
								<label class="mr-sm-2">Current password</label>
								<input type="password" class="form-control" name="old_password" >
								<div class="has-error text-danger"><?php echo form_error('old_password') ?></div>
							</div>
							<div class="form-group col-xl-12">
								<label class="mr-sm-2">New password</label>
								<input type="password" class="form-control" name="password" >
								<div class="has-error text-danger"><?php echo form_error('password') ?></div>
							</div>
							<div class="form-group col-xl-12">
								<label class="mr-sm-2">Confirm new password</label>
								<input type="password" class="form-control" name="re_password">
								<div class="has-error text-danger"><?php echo form_error('re_password') ?></div>
							</div>
							<div class="form-group col-xl-12">
								<label class="mr-sm-2">2FA Code</label>
								<input type="text" class="form-control" name="google_2fa">
								<div class="has-error text-danger"><?php echo form_error('google_2fa') ?></div>
							</div>
							<div class="text-center m-auto">
		                        <button type="submit" class="big_button"><div class="icon"><i class="fas fa-check"></i></div> Change Password</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</div>
