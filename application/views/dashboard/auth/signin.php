<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- loginGoogle - Bitono Team -->
    <meta name="google-signin-client_id" content="<?php echo CLIENT_ID ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.png">
    <title><?php echo $title;?></title>
    <!-- Css -->
    <link rel="stylesheet" href="public/dashboard/css/style.css">
    <link href="public/dashboard/css/signin.css" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <!-- Js -->
    <script src="public/dashboard/js/jquery-3.5.1.min.js"></script>
    <!-- End Js -->
     <!-- capcha google Bitono Team -->
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <!-- End Js -->
</head>
<body>
    <div id="main-wrapper">
        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        
                        <!-- Info Submit -->
                        <?php $message_flashdata = $this->session->flashdata('message_flashdata');
                        if(isset($message_flashdata) && count($message_flashdata)){ ?>
                            <div id="alerttopfix" class="myadmin-alert alert-success myadmin-alert-top-right" style="display: block;">
                                <?php if($message_flashdata['type'] == 'sucess'){?> 
                                    <i class="la la-check"></i> <?php echo $message_flashdata['message']; ?>
                                <?php }else if($message_flashdata['type'] == 'error'){?>
                                    <i class="la la-close"></i> <?php echo $message_flashdata['message']; ?>
                                <?php } ?>
                            </div>
                        <?php } ?> 
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <div class="mini-logo text-center">
                                    <img src="public/dashboard/images/logo.png" alt="BetDefi.net">
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" name="myform" class="signin_validate" action="">
                                    <div class="has-error text-danger"><?php echo form_error('error') ?></div>
                                    <div class="form-group">
                                        <label>Email <span>*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter your Email" name="email">
                                        <div class="has-error text-danger"><?php echo form_error('email') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Password <span>*</span></label>
                                        <input type="password" class="form-control" placeholder="Password" name="password">
                                        <div class="has-error text-danger"><?php echo form_error('password') ?></div>
                                    </div>
                                    <div class="box__recaptcha">
                                        <div class="g-recaptcha" data-sitekey="<?php echo SITE_CAPCHA_GOOGLE; ?>"></div>
                                        <div class="has-error text-danger"><?php echo form_error('g-recaptcha-response') ?></div>
                                    </div>
                                    <div class="form-row d-flex justify-content-between mt-4 mb-2">
                                        <div class="form-group mb-0">
                                            <label class="toggle">
                                                <input class="toggle-checkbox" type="checkbox">
                                                <span class="toggle-switch"></span>
                                                <span class="toggle-label">Remember me</span>
                                            </label>
                                        </div>
                                        <div class="form-group mb-0">
                                            <a href="forget-password.html">Forgot Password?</a>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-warning btn-block">Sign in</button>
                                    </div>
                                </form>
                                <div class="new-account mt-3">
                                    <p>Don't have an account? <a class="text-primary" href="signup.html">Sign up</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
