<div class="container">
	<div class="row justify-content-center h-100 align-items-center">
		<div class="col-xl-12 col-md-12">
			<!-- Info Submit -->
			<?php $message_flashdata = $this->session->flashdata('message_flashdata');
			if(isset($message_flashdata) && count($message_flashdata)){ ?>
			    <div id="alerttopfix" class="myadmin-alert alert-success myadmin-alert-top-right" style="display: block;">
			        <?php if($message_flashdata['type'] == 'sucess'){?> 
			          	<i class="la la-check"></i> <?php echo $message_flashdata['message']; ?>
			        <?php }else if($message_flashdata['type'] == 'error'){?>
			          	<i class="la la-close"></i> <?php echo $message_flashdata['message']; ?>
			        <?php } ?>
			  	</div>
			<?php } ?> 
			<div class="auth-form card">
				<div class="card-body">
					<form action="" class="identity-upload" method="POST" enctype="multipart/form-data">
						<div class="identity-content">
							<h4>Upload your ID card</h4>
							<span>(Driving License or Government ID card)</span>
							<p>Uploading your ID helps as ensure the safety and security of your founds</p>
						</div>
						<div class="row">
							<div class="col-6">
								<div class="form-group col-xl-12">
									<label class="mr-sm-2">Passport/ID</label>
									<input type="text" class="form-control" name="passportID" value="<?=$datas['passportID']?>">
									<div class="has-error text-danger"><?php echo form_error('passportID') ?></div>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									<label class="mr-sm-2">Gender</label>
									<select class="form-control" name="gender" required>
                                        <option value="0" <?php echo ($datas['gender']==0)?'selected':''; ?>>Male</option>    
                                        <option value="1" <?php echo ($datas['gender']==1)?'selected':''; ?>>Female</option>    
                                    </select>	
								</div>
							</div>
						</div>
						<div class="kyc_wrapper _wrapper_upload">
				            <div class="upload_form">
				                <div class="upload_card">
				                    <div id="front">
				                        <?php if($datas['card_front']){?>
				                        	<img class="icon" src="<?php echo $path_dir_thumb.$datas['card_front'];?>">
				                        <?php }else{?> 
				                        	<img class="icon" src="public/dashboard/images/id-front.png">
				                        <?php }?>
				                    </div>
				                    <div class="upload_card_footer">
				                        Front Side
				                    </div>
				                </div>
				                <button type="button" class="upload_btn">
				                    <input onchange="preview_image(event, 'front')" class="__input_upload" type="file" accept="image/*" name="card_front" <?php if(!$datas['card_front']){?> required <?php }?>>Upload
				                </button>
				            </div>
				            <div class="upload_form">
				                <div class="upload_card">
				                    <div id="back">
				                    	<?php if($datas['card_back']){?>
				                        	<img class="icon" src="<?php echo $path_dir_thumb.$datas['card_back'];?>">
				                        <?php }else{?> 
				                        	<img class="icon" src="public/dashboard/images/id-back.png">
				                        <?php }?>
				                    </div>
				                    <div class="upload_card_footer">
				                        Back Side
				                    </div>
				                </div>
				                <button type="button" class="upload_btn"> 
				                    <input onchange="preview_image(event, 'back')" class="__input_upload" type="file" accept="image/*" name="card_back" <?php if(!$datas['card_back']){?> required <?php }?>>Upload
				                </button>
				            </div>
				            <div class="upload_form">
				                <div class="upload_card">
				                    <div id="selfie">
				                    	<?php if($datas['avatar']){?>
				                        	<img class="icon" src="<?php echo $path_dir_thumb.$datas['avatar'];?>">
				                        <?php }else{?> 
				                        	<img class="icon" src="public/dashboard/images/with-id.png">
				                        <?php }?>
				                    </div>
				                    <div class="upload_card_footer">
				                        Selfie with Photo
				                    </div>
				                </div>
				                <button type="button" class="upload_btn"> 
				                    <input onchange="preview_image(event, 'selfie')" class="__input_upload" type="file" accept="image/*" name="avatar"  <?php if(!$datas['avatar']){?> required <?php }?>>Upload
				                </button>
				            </div>
				        </div>
				        <div class="clear" style="height: 70px"></div>
				        <div class="text-center m-auto">
							<button type="submit" class="big_button"><div class="icon"><i class="fas fa-cloud-upload-alt"></i></div> Submit</button>
						</div>
			        </form>
				</div>
			</div>
		</div>
	</div>
</div>
