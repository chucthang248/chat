<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- loginGoogle - OT1 -->
    <meta name="google-signin-client_id" content="<?php echo CLIENT_ID ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.png">
    <title><?php echo $title;?></title>
    <!-- Css -->
    <link href="public/dashboard/js/select2/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="public/dashboard/css/flag-icon.min.css">
    <link rel="stylesheet" href="public/dashboard/css/style.css">
    <link href="public/dashboard/css/signin.css" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <!-- Js -->
    <script src="public/dashboard/js/jquery-3.5.1.min.js"></script>
    <script src="public/dashboard/js/select2/select2.full.min.js"></script>
    <!-- capcha google OT1 -->
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <!-- End Js -->
</head>
<body>
    <div id="main-wrapper">
        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <div class="mini-logo text-center">
                                    <img src="public/dashboard/images/logo.png" alt="BetDefi.net">
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" name="myform" class="signin_validate" action="">
                                    <div class="form-group">
                                        <label>Full Name <span>*</span></label>
                                        <input onkeyup="checkUnikey(this);" type="text" class="form-control" placeholder="Enter your fullname" name="fullname" id="fullname" value="<?php echo set_value('fullname');?>">
                                        <div class="has-error text-danger"><?php echo form_error('fullname') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Email <span>*</span></label>
                                        <input type="email" class="form-control" placeholder="hello@example.com" name="email" value="<?php echo set_value('email');?>">
                                        <div class="has-error text-danger"><?php echo form_error('email') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Password <span>*</span></label>
                                        <input type="password" class="form-control" placeholder="Password" name="password" value="<?php echo set_value('password');?>">
                                        <div class="has-error text-danger"><?php echo form_error('password') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password <span>*</span></label>
                                        <input type="password" class="form-control" placeholder="Confirm Password" name="re_password" value="<?php echo set_value('re_password');?>">
                                        <div class="has-error text-danger"><?php echo form_error('re_password') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Phone <span>*</span></label>
                                        <input type="text" class="form-control" placeholder="Enter your phone number" name="phone" value="<?php echo set_value('phone');?>">
                                        <div class="has-error text-danger"><?php echo form_error('phone') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Referral ID (optional)</label>
                                        <?php $codeReferral = $_GET['sponsor'];  ?>
                                        <input type="text" class="form-control" name="referral" value="<?php echo (isset($codeReferral))?$codeReferral:'' ?>">
                                        <div class="has-error text-danger"><?php echo form_error('referral') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label class="mr-sm-2">Country <span>*</span></label>
                                        <select class="form-control select2" name="countryID" required id="listCountry">
                                            <?php foreach ($country as $key_country => $val_country) {?>
                                                <option value="<?=$val_country['id']?>"
                                                    <?php if($val_country['flagicon'] == 'us'){echo "selected";}?>
                                                    data-flagicon="<?=$val_country['flagicon']?>"><?=$val_country['name']?>
                                                </option>    
                                            <?php } ?>
                                        </select>
                                    </div>
                                    
                                    <div class="box__recaptcha">
                                        <div class="g-recaptcha" data-sitekey="<?php echo SITE_CAPCHA_GOOGLE; ?>"></div>
                                        <div class="has-error text-danger"><?php echo form_error('g-recaptcha-response') ?></div>
                                    </div>
                                    <div class="text-center mt-4">
                                        <button type="submit" class="btn btn-warning btn-block">Sign up</button>
                                    </div>
                                </form>
                                <div class="new-account mt-3">
                                    <p>You have an account? <a class="text-primary" href="signin.html">Sign in</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Js -->
    <script src="public/dashboard/js/signup.js"></script>
    <!-- END Js -->
</body>
</html>

