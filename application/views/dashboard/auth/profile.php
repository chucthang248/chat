<!-- css iconflag -->
<link href="public/dashboard/js/select2/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="public/dashboard/css/flag-icon.min.css">
<!-- ENDcss iconflag -->
<!-- Js iconflag -->
<script src="public/dashboard/js/select2/select2.full.min.js"></script>
<script src="public/dashboard/js/signup.js"></script>
<div class="content-body">
	<div class="container-fluid">
		<div class="base_wrapper">
	        <div class="card_head">
	            <h2 class="base_title"><strong> Profile Setting</strong></h2>
	        </div>
	    </div>
		<div class="row">
			<div class="col-xl-3 col-md-5 col-12">
				<div class="card settings_menu">
					<div class="card-header"><h4 class="card-title">Settings</h4></div>
					<div class="card-body">
						<ul>
							<li class="nav-item border-b">
								<a class="nav-link active" href="dashboard/profile.html"><i class="fas fa-user"></i><span>Update Profile</span></a>
							</li>
							<li class="nav-item border-b">
								<a class="nav-link" href="dashboard/change-password.html"><i class="fas fa-key"></i><span>Change password</span></a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="dashboard/google-authen.html"><i class="fas fa-lock"></i><span>Google Authenticator</span></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-xl-9 col-md-7 col-12">
				<?php $message_flashdata = $this->session->flashdata('message_flashdata');
				if(isset($message_flashdata) && count($message_flashdata)){ ?>
				    <div id="alerttopfix" class="myadmin-alert alert-success myadmin-alert-top-right" style="display: block;">
				        <?php if($message_flashdata['type'] == 'sucess'){?> 
				          	<i class="la la-check"></i> <?php echo $message_flashdata['message']; ?>
				        <?php }else if($message_flashdata['type'] == 'error'){?>
				          	<i class="la la-close"></i> <?php echo $message_flashdata['message']; ?>
				        <?php } ?>
				  	</div>
				<?php } ?> 
				<form action="" method="POST" class="identity-upload" enctype="multipart/form-data">
					<div class="card">
						<div class="card-body">
							<div class="row">
								<div class="col-xl-6">
									<div class="form-row">
										<div class="form-group col-xl-12">
											<label class="mr-sm-2">Full Name</label>
											<input type="text" class="form-control" name="fullname" value="<?php echo $datas['fullname'] ?>">
											<div class="has-error text-danger"><?php echo form_error('fullname') ?></div>
										</div>
										<div class="kyc_wrapper _wrapper_upload justify-content-center">
											<div class="upload_form" style="max-width: 70%;">
								                <div class="upload_card">
								                    <div id="avatar">
								                    	<img class="icon"
									                    	<?php if($datas['avatar'] != ''){ ?>
																src="<?php echo base_url().$path_dir_thumb.$datas['avatar'] ?>"
															<?php }else{ ?>
																src="public/dashboard/images/avatar-default.jpg"
															<?php } ?>
														>
								                    </div>
								                    <div class="upload_card_footer">
								                        My Avatar
								                    </div>
								                </div>
								                <button type="button" class="upload_btn"> 
								                    <input onchange="preview_image(event, 'avatar')" class="__input_upload" type="file" accept="image/*" name="avatar">Upload
								                </button>
								            </div>
								            <div style="height: 50px;"></div>
										</div>
									</div>
								</div>
								<div class="col-xl-6">
									<div class="form-row">
										<div class="form-group col-xl-12">
											<label class="mr-sm-2">Phone</label>
											<input type="text" class="form-control" name="phone" value="<?php echo $datas['phone'] ?>">
											<div class="has-error text-danger"><?php echo form_error('phone') ?></div>
										</div>
										<div class="form-group col-xl-12">
											<label class="mr-sm-2">Country</label>
											<select class="form-control select2" name="countryID" required>
	                                            <?php foreach ($country as $key_country => $val_country) {?>
	                                                <option value="<?=$val_country['id']?>"
	                                                    <?php if($datas['countryID'] == $val_country['id']){echo "selected";}?>
	                                                    data-flagicon="<?=$val_country['flagicon']?>"><?=$val_country['name']?>
	                                                </option>    
	                                            <?php } ?>
	                                        </select>	
										</div>
										<div class="form-group col-xl-12">
											<label class="mr-sm-2">2FA Code</label>
											<input type="text" class="form-control" name="google_2fa">
											<div class="has-error text-danger"><?php echo form_error('google_2fa') ?></div>
										</div>
									</div>
									<div class="text-center m-auto">
										<button type="submit" class="big_button"><div class="icon"><i class="fas fa-save"></i></div>Update</button>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

    

