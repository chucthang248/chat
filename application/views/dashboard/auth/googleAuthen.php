<div class="container-fluid">
	<div class="base_wrapper">
        <div class="card_head">
            <h2 class="base_title"><strong> Profile Setting</strong></h2>
        </div>
    </div>
	<div class="row justify-content-center">
		<div class="col-xl-3 col-md-5 col-12">
			<div class="card settings_menu">
				<div class="card-header"><h4 class="card-title">Settings</h4></div>
				<div class="card-body">
					<ul>
						<li class="nav-item border-b">
							<a class="nav-link" href="dashboard/profile.html"><i class="fas fa-user"></i><span>Update Profile</span></a>
						</li>
						<li class="nav-item border-b">
							<a class="nav-link" href="dashboard/change-password.html"><i class="fas fa-key"></i><span>Change password</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link active" href="dashboard/google-authen.html"><i class="fas fa-lock"></i><span>Google Authenticator</span></a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xl-5 col-md-6 col-12">
			<div class="auth-form card">
				<div class="card-body">
					<form action="<?php echo $user['is_enabled_2fa'] == 0?'':'dashboard/disabled-google-2fa.html'; ?>" method="POST" class="login__form">
						<?php if($user['is_enabled_2fa'] == 0){ ?>
							<div class="form-item">
								<div class="box__qrcode">
									<img src="<?php echo $user['2fa']['qrCodeUrl'];?>" border=0>
									<p><?php echo $user['2fa']['secret'];?></p>
								</div>
					        </div>
					    <?php } ?>
				        <div class="form-row">
					        <div class="form-group col-xl-12">
								<label class="mr-sm-2">Google Authenticator Code <span>(*)</span></label>
								<input type="text" class="form-control" required name="google_auth_code" autocomplete="off">
								<input type="hidden" class="form-control" name="secret" value="<?php echo $user['2fa']['secret'];?>">
								<?php $message_flashdata = $this->session->flashdata('warning_already');
								if(isset($message_flashdata)){ ?>
								    <div class="text-danger" style="display: block;">
								        <?php echo $message_flashdata; ?>
								  	</div>
								<?php } ?> 
							</div>
						</div>
				        <div class="col-12 text-center">
				        	<input type="hidden" name="name" autocomplete="off">

			              	<button type="submit" class="btn btn-success waves-effect"><?php echo $user['is_enabled_2fa'] == 0?'Enabled':'Disabled';?></button>
			            </div>
				    </form>
				</div>
			</div>
		</div>
	</div>
</div>
