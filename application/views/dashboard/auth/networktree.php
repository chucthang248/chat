<div class="container-fluid">
    <div class="row">
        <div class="col-12 col-md-4 col-lg-3">
            <div class="card settings_menu">
                <div class="card-body">
                    <ul>
                        <li class="nav-item border-b">
                            <a class="nav-link" href="dashboard/referal.html"><span>Referal List</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard/network-tree.html"><span>Network Tree</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-8 col-lg-9">
            <div class="card">
                <div class="card-header border-0">
                    <h4 class="card-title"><?php echo $title;?></h4>
                </div>
                <div class="card-body pt-0">
                    <div id="jstree">
                        <?php echo $listTree;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<link rel="stylesheet" href="public/dashboard/js/tree/themes/default/style.min.css" />
<!-- 5 include the minified jstree source -->
<script src="public/dashboard/js/tree/jstree.min.js"></script>
<script>
    $(function() {
        // 6 create an instance when the DOM is ready
        $('#jstree').jstree();
        // 7 bind to events triggered on the tree
        $('#jstree').on("changed.jstree", function(e, data) {
            console.log(data.selected);
        });
        // 8 interact with the tree - either way is OK
        $('button').on('click', function() {
            $('#jstree').jstree(true).select_node('child_node_1');
            $('#jstree').jstree('select_node', 'child_node_1');
            $.jstree.reference('#jstree').select_node('child_node_1');
        });
    });
</script>
<style type="text/css">
    #jstree {
        background: #FFF;
        padding: 20px 0px;
        border-radius: 10px;
    }
    .jstree-anchor, .jstree-anchor:link, .jstree-anchor:visited, .jstree-anchor:hover, .jstree-anchor:active {
        text-decoration: none;
        color: #333; 
    }
</style>