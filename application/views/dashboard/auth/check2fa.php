<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- loginGoogle - OT1 -->
    <meta name="google-signin-client_id" content="<?php echo CLIENT_ID ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.png">
    <title><?php echo $title;?></title>
    <!-- Css -->
    <link rel="stylesheet" href="public/dashboard/css/style.css">
    <link href="public/dashboard/css/signin.css" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <!-- Js -->
    <script src="public/dashboard/js/jquery-3.5.1.min.js"></script>
    <!-- End Js -->
     <!-- capcha google OT1 -->
    <script src="https://www.google.com/recaptcha/api.js"></script>
    <!-- End Js -->
</head>
<body>
    <div id="main-wrapper">
        <?php $message_flashdata = $this->session->flashdata('message_flashdata');
        if(isset($message_flashdata) && count($message_flashdata)){ ?>
            <div id="alerttopfix" class="myadmin-alert alert-success myadmin-alert-top-right" style="display: block;">
                <?php if($message_flashdata['type'] == 'sucess'){?> 
                    <i class="la la-check"></i> <?php echo $message_flashdata['message']; ?>
                <?php }else if($message_flashdata['type'] == 'error'){?>
                    <i class="la la-close"></i> <?php echo $message_flashdata['message']; ?>
                <?php } ?>
            </div>
        <?php } ?> 
        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <div class="mini-logo text-center my-1">
                                    <img src="public/dashboard/images/logo.png" alt="Anu Capital">
                                </div>
                            </div>
                            <div class="card-body">
                                <form method="post" name="myform" class="signin_validate" action="">
                                    <div class="has-error text-danger"><?php echo form_error('error') ?></div>
                                    <div class="form-group">
                                        <label>2FA Code <span>*</span></label>
                                        <input type="text" class="form-control" name="google_2fa">
                                        <div class="has-error text-danger"><?php echo form_error('google_2fa') ?></div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-warning btn-block">Sign in</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
