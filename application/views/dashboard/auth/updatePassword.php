<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- loginGoogle - OT1 -->
    <meta name="google-signin-client_id" content="<?php echo CLIENT_ID ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.png">
    <title><?php echo $title;?></title>
    <!-- Css -->
    <link href="public/dashboard/js/select2/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="public/dashboard/css/flag-icon.min.css">
    <link rel="stylesheet" href="public/dashboard/css/style.css">
    <!-- Latest compiled and minified CSS -->
    <!-- Js -->
    <script src="public/dashboard/js/jquery-3.5.1.min.js"></script>
    <script src="public/dashboard/js/select2/select2.full.min.js"></script>
    <!-- End Js -->
</head>
<body>
    <div id="main-wrapper">
        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="mini-logo text-center my-5">
                            <a href="/demo/elaenia_react/">
                                <img src="public/dashboard/images/logo.svg" alt="">
                            </a>
                        </div>
                        <div class="auth-form card">
                            <div class="card-header justify-content-center">
                                <h4 class="card-title"><?php echo $title;?></h4>
                            </div>
                            <div class="card-body">
                                <form method="post" name="myform" class="signin_validate" action="">
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input type="password" class="form-control" placeholder="Password" name="password">
                                        <div class="has-error text-danger"><?php echo form_error('password') ?></div>
                                    </div>
                                    <div class="form-group">
                                        <label>Confirm Password</label>
                                        <input type="password" class="form-control" placeholder="Confirm Password" name="re_password">
                                        <div class="has-error text-danger"><?php echo form_error('re_password') ?></div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-warning btn-block">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Js -->
    <script src="public/dashboard/js/signup.js"></script>
    <!-- END Js -->
</body>
</html>
