<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- loginGoogle - OT1 -->
    <meta name="google-signin-client_id" content="<?php echo CLIENT_ID ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="public/images/favicon.png">
    <title><?php echo $title;?></title>
    <!-- Css -->
    <link rel="stylesheet" href="public/dashboard/css/style.css">
    <link href="public/dashboard/css/signin.css" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <!-- Js -->
    <script src="public/dashboard/js/jquery-3.5.1.min.js"></script>
    <!-- End Js -->
</head>
<body>
    <div id="main-wrapper" class="box-flex">
        <div class="authincation section-padding">
            <div class="container h-100">
                <div class="row justify-content-center h-100 align-items-center">
                    <div class="col-xl-5 col-md-6">
                        <div class="auth-form card">
                            <div class="card-body">
                                <div class="identity-content">
                                    <span class="icon"><i class="fa fa-check"></i></span>
                                    <h4><?php echo $title;?></h4>
                                    <p>Efficiently provide access to installed base core competencies and end end data Interactively target equity.</p>
                                </div>
                                <div class="text-center">
                                    <a type="submit" class="btn btn-success pl-5 pr-5" href="/demo/elaenia_react/settings-account">Continue</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
