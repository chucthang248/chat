<div class="container-fluid">
	<div class="row">
		<div class="col-12 col-md-4 col-lg-3">
            <div class="card settings_menu">
                <div class="card-body">
                    <ul>
                        <li class="nav-item border-b">
                            <a class="nav-link" href="dashboard/referal.html"><span>Referal List</span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="dashboard/network-tree.html"><span>Network Tree</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
		<div class="col-12 col-md-8 col-lg-9">
			<div class="card">
				<div class="card-header border-0">
					<h4 class="card-title"><?php echo $title;?></h4>
				</div>
				<div class="card-body pt-0">
					<div class="transaction-table">
						<div class="table-responsive">
							<table class="table mb-0 table-responsive-sm">
								<thead>
									<tr>
										<th>Name</th>
										<th>Date</th>
									</tr>
								</thead>
								<tbody>
									<?php if(isset($datas) && $datas != NULL){ ?>
										<?php foreach ($datas as $key_datas => $val_datas) {?>
											<tr>
												<td><?php echo $val_datas['fullname']; ?></td>
												<td><?php echo $val_datas['created_at']; ?></td>
											</tr>
										<?php } ?>
									<?php } ?>			
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
