<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?php echo site_url(); ?>" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="icon" type="image/x-icon" href="public/images/favicon.png" />
        <title><?php echo $title;?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
        <link href="public/dashboard/css/main.min.css" rel="stylesheet" type="text/css">
        <link href="public/dashboard/css/custom.css" rel="stylesheet" type="text/css">
        <!-- Js -->
        <script src="public/dashboard/js/main.min.js"></script>
        <script src="public/dashboard/js/jquery-3.5.1.min.js"></script>
    </head>
    <body>
        <div class="push_wrapper"></div>
        <header id="header">
            <div class="header_wrapper">
                <?php $this->load->view('dashboard/layout/sidebar'); ?>
                <?php $this->load->view('dashboard/layout/profile'); ?>
            </div>
        </header>

        <div id="main">
            <!-- Content Main -->
            <?php
                if(isset($template) && !empty($template)){
                    $this->load->view($template, isset($data)?$data:NULL);
                }
            ?>
            <!-- End Content Main -->
        </div>
        <footer>© 2019 - 2021 BetDefi.net. All rights reserved.</footer>


        <?php /*<section class="page_loader">
            <img src="public/dashboard/images/logo.png" width="120" alt="<?php echo $title;?>">
            <div class="jump_block_wrapper">
                <span class="block"></span>
                <span class="block"></span>
                <span class="block"></span>
                <span class="block"></span>
                <span class="block"></span>
            </div>
        </section> */?>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        
    </body>
</html>

