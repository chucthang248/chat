<header id="header" class="clearfix">
    <div class="header-pc clearfix">
        <div class="logo">
            <a href="" class="logo-icon router-link-active">
                <img src="public/dashboard/images/logo.png" alt="" class="main-logo">
            </a>
        </div>
        <div class="navmt7">
            <ul>
                <li><a href="dashboard">Dashboard</a></li>
            </ul>
        </div>
        <div class="right">
            <?php if($data_index['info_user']['fullname']){ ?>
                <div class="dropdown myAccount">
                    <a class="dropdown-toggle fullname" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-user-shield"></i>
                            <?php echo $data_index['info_user']['fullname'];?>
                        <i class="fas fa-caret-down"></i>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        <a class="dropdown-item account" href="dashboard/profile.html"><i class="fas fa-user-cog"></i> Profile</a>
                        <a class="dropdown-item" href="logout.html"><i class="fas fa-sign-out-alt"></i> Sign Out</a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="clear"></div>
    <div class="sub-header hidden-xs">
        <div class="sub-header-wrap">
            <div id="broadcast">
                <div class="msg">
                    <?php if($this->Auth->checkSignin() === true){?>
                        <div class="text">WELCOME <i><?php echo $data_index['info_user']['fullname'];?></i> JOIN THE GAME</div>
                    <?php } ?>    
                </div>
            </div>
        </div>
    </div>
</header>