<nav class="navigation">
    <button class="mobile_menu_control"> <span><i class="dot"></i></span></button>
    <div class="logo">
        <img src="public/dashboard/images/logo.png" alt="QuanTex" class="icon">
    </div>
    <button class="mobile_account_control">
        <svg height="30px" viewBox="0 0 512 512" width="30px" xmlns="http://www.w3.org/2000/svg">
            <path d="m512 256c0-141.488281-114.496094-256-256-256-141.488281 0-256 114.496094-256 256 0 140.234375 113.539062 256 256 256 141.875 0 256-115.121094 256-256zm-256-226c124.617188 0 226 101.382812 226 226 0 45.585938-13.558594 89.402344-38.703125 126.515625-100.96875-108.609375-273.441406-108.804687-374.59375 0-25.144531-37.113281-38.703125-80.929687-38.703125-126.515625 0-124.617188 101.382812-226 226-226zm-168.585938 376.5c89.773438-100.695312 247.421876-100.671875 337.167969 0-90.074219 100.773438-247.054687 100.804688-337.167969 0zm0 0"></path>
            <path d="m256 271c49.625 0 90-40.375 90-90v-30c0-49.625-40.375-90-90-90s-90 40.375-90 90v30c0 49.625 40.375 90 90 90zm-60-120c0-33.085938 26.914062-60 60-60s60 26.914062 60 60v30c0 33.085938-26.914062 60-60 60s-60-26.914062-60-60zm0 0"></path>
        </svg>
    </button>
    <button class="mobile_control"> <i class="icon"><img src="public/dashboard/images/white_arrows.png" alt="QuanTex"></i>Menu</button>
    <ul class="link_list box_sidebar">
        <li class="link_wrapper">
            <a href="dashboard/"> 
                <i class="fas fa-tachometer-alt"></i>Home
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/deposite.html"> 
                <i class="fas fa-arrow-down"></i>Deposite
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/withdraw.html"> 
                <i class="fas fa-arrow-up"></i>Withdraw
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/transfer.html"> 
                <i class="fas fa-exchange-alt"></i>Transfer
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/profile.html"> 
                <i class="fas fa-user-cog"></i>Profile
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/referal.html"> 
                <i class="fas fa-users"></i>Affiliate
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/bonus.html"> 
                <i class="fas fa-award"></i>Bonus
            </a>
        </li>
        <li class="link_wrapper">
            <a href="dashboard/support.html"> 
                <i class="fas fa-envelope"></i>Contact Us
            </a>
        </li>
    </ul>
</nav>