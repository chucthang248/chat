<div class="user_data">
    <div class="user_preview">
        <div class="avatar" style="background-image: url(<?=$data_index['info_user']['avatar'];?>);"></div>
        <h5 class="name"><?=$data_index['info_user']['fullname'];?></h5>
        <div class="box__code">Code: 
            <div class="code" id="myCode" style="word-wrap: break-word"><?php echo $data_index['info_user']['code'];?></div> 
            <button id="btnCopyCode" data-clipboard-action="copy" data-clipboard-target="#myCode"><i class="fa fa-copy"></i></button>
        </div>
    </div>
    <div class="user_navigation"> 
        <a href="dashboard/profile.html" class="link ">
            <div class="icon">
                <img src="public/dashboard/images/icSetting.png" alt="" width="18">
            </div>
            Account Setting
        </a>
    </div>
    <div class="user_navigation"> 
        <a href="logout.html" class="link">
            <div class="icon">
                <img src="public/dashboard/images/icLogout.png" alt="" width="18">
            </div>
            Sign Out
        </a>
    </div>
</div>
<script src="public/dashboard/js/clipboard.js/clipboard.min.js"></script>
<script>
    //copy referel
    var clipboard = new ClipboardJS('#btnCopy');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });

    //copy referel
    var clipboardCode = new ClipboardJS('#btnCopyCode');
    clipboardCode.on('success', function(e) {
        console.log(e);
    });
    clipboardCode.on('error', function(e) {
        console.log(e);
    });
</script>