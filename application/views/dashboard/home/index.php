<div class="base_wrapper">
  <h2 class="base_title"><strong>BETDEFI - DECENTRALIZE BETTING PLATFORM</strong></h2>
  <div class="card_dash commission">
      <div class="card_dash-box">
          <div class="card_dash-container">
              <div class="card_dash-left" id="bitcoin">
                  <img src="public/dashboard/images/icBalance.png" alt="">
              </div>
              <div class="card_dash-right">
                  <div class="info">
                      <strong class="title"><?php echo number_format($data_index['info_user']['walletUSD'],2);?></strong>
                      <small class="sub_title">Balance</small>
                  </div>
              </div>
          </div>
      </div>
      <div class="card_dash-box">
          <div class="card_dash-container">
              <div class="card_dash-left">
                  <img src="public/dashboard/images/icCommission.png" alt="">
              </div>
              <div class="card_dash-right">
                  <div class="info">
                      <strong class="title"><?php echo number_format($data_index['info_user']['walletCommission'],2);?></strong>
                      <small class="sub_title">Commission</small>
                      <?php if($data_index['info_user']['walletCommission'] > 0){ ?>
                        <a href="dashboard/collect.html" class="collect">COLLECT</a>
                      <?php } ?>
                  </div>
              </div>
          </div>
      </div>
      <div class="card_dash-box">
          <div class="card_dash-container">
              <div class="card_dash-left">
                  <img src="public/dashboard/images/icCommission.png" alt="">
              </div>
              <div class="card_dash-right">
                  <div class="info">
                      <strong class="title"><?php echo number_format($bonus,2);?></strong>
                      <small class="sub_title">Bonus</small>
                  </div>
              </div>
          </div>
      </div>
      <div class="card_dash-box">
          <div class="card_dash-container">
              <div class="card_dash-left">
                  <img src="public/dashboard/images/icCommission.png" alt="">
              </div>
              <div class="card_dash-right">
                  <div class="info">
                      <strong class="title"><?php echo number_format($totalBet,2);?></strong>
                      <small class="sub_title">Total Bet</small>
                      <a href="platform.html" class="play_now">Play Now</a>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <div class="card_dash commission">
    <div class="box__btn">
      <a href="dashboard/deposite.html" class="block bg_success"><div class="icon"><i class="fas fa-arrow-down"></i></div> Deposite</a>
    </div>
    <div class="box__btn">
      <a href="dashboard/withdraw.html" class="block bg_danger"><div class="icon"><i class="fas fa-arrow-up"></i></div> Withdraw</a>
    </div>
    <div class="box__btn">
      <a href="dashboard/commission.html" class="block"><div class="icon"><i class="fas fa-clock"></i></div> Commission History</a>
    </div>
    <div class="box__btn">
      <a href="dashboard/transfer.html" class="block"><div class="icon"><i class="fas fa-exchange-alt"></i></div> Transfer</a>
    </div>
  </div>
</div>
<div class="clear"></div>
<div class="container-fluid" style="padding: 0px;">
  <div class="row">
    <div class="col-12 col-lg-6">
      <div class="card profile_card">
          <div class="card-body">
            <h4 class="card-title">Referral <strong>Link</strong></h4>
            <div class="box__address__wallet">
                <div class="item_wallet" id="myTextReferral" style="word-wrap: break-word"><?php echo base_url().'signup.html?sponsor='.$data_index['info_user']['code']; ?></div>
                <button id="btnCopyReferral" data-clipboard-action="copy" data-clipboard-target="#myTextReferral">Copy</button>
            </div>
          </div>
        </div>
    </div>
  </div>
</div>
<script src="public/dashboard/js/clipboard.js/clipboard.min.js"></script>
<script>
    // var clipboard = new ClipboardJS('#btnCopy');
    // clipboard.on('success', function(e) {
    //     console.log(e);
    // });
    // clipboard.on('error', function(e) {
    //     console.log(e);
    // });
    //copy referal
    var clipboardReferal = new ClipboardJS('#btnCopyReferral');
    clipboardReferal.on('success', function(e) {
        console.log(e);
    });
    clipboardReferal.on('error', function(e) {
        console.log(e);
    });
</script>