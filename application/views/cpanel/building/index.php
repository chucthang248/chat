<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <!-- <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?//ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?//$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?//$title;?></h4>
            </div>
        </div>
    </div>      -->
    <!-- end page title --> 

    <!-- notify --> 
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End --> 

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive text-center  d-flex justify-content-center align-items-center">
                <div class="box__build mt-5 mb-5">
                    <img width="550" height="auto" src="public/images/building.jpg" alt="">
                    <h3 class="mt-4">Tính năng đang được xây dựng. Xin vui lòng quay lại sau.</h3>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
    
</div> <!-- end container-fluid -->
