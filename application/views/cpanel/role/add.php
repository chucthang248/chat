<div class="overplay_role x_close"></div>
<div class="role_add">
    <div class="role_scroll">
        <i class="fas fa-times x_close"></i>
        <form class="form-horizontal" action="<?=CPANEL?>role/add" method="POST" data-toggle="validator"
            novalidate="true">
            <div class="box__role mt-3">
                <div class="form-group  ">
                    <label for="roleName ">Tên nhóm: </label>
                    <input class=" form-control" type="text" name="data_post_role[name]">
                </div>
                <label class="title_database ">Cơ sở dữ liệu</label>
                <div class="box__check__role">
                    <?php foreach($tbl_module as $key_module => $val_module) {  ?>
                    <div class="module_item">
                        <label for="moduleName"><?=$val_module['name']; ?></label>
                        <div class="moduleMethod d-flex">
                            <?php foreach($val_module['module_detail'] as $key_detail => $val_detail){ ?>
                            <div class="col-2 checkbox checkbox-secondary">
                                <input type="hidden" name="data_post_permisson[<?=$key_module?>][<?=$key_detail?>][moduleDetail_ID]" value="<?=$val_detail['id'] ?>">
                                <input type="hidden" name="data_post_permisson[<?=$key_module?>][<?=$key_detail?>][link]" value="<?=$val_module['link'] ?>">
                                <input type="hidden" name="data_post_permisson[<?=$key_module?>][<?=$key_detail?>][ctr]" value="<?=$val_module['ctr'] ?>">
                                <input type="hidden" name="data_post_permisson[<?=$key_module?>][<?=$key_detail?>][action]" value="<?=$val_detail['action'] ?>">
                                <input id="publishh-<?=$key_module?>-<?=$key_detail?>" type="checkbox" name="data_post_permisson[<?=$key_module?>][<?=$key_detail?>][status]"   value="1">
                                <label for="publishh-<?=$key_module?>-<?=$key_detail?>">  <?= $val_detail['name']; ?></label>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="checkbox checkbox-secondary mt-4">
                <input id="publish" type="checkbox" name="data_post_role[publish]" checked value="1">
                <label for="publish">
                    Hiển thị
                </label>
            </div>
            <button type="submit" class="btn btn-blue btn_search_employee waves-effect waves-light mr-2 mt-4"> Lưu</button>
        </form>
    </div>
</div>