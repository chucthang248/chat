<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title">Cơ sở dữ liệu</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
    <div id="box-notify" class="jq-toast-wrap bottom-right">
        <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
            <span class="jq-toast-loader jq-toast-loaded"
                style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
            <h2 class="jq-toast-heading">Thông báo!</h2>
            <?php echo $message_flashdata['message']; ?>
        </div>
    </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row" id="listemployee">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-12 d-flex t_justify_sb align-items-center">
                        <div class="left_box">
                            <h4>Danh sách nhân sự: <?=$count?></h4>
                        </div>
                        <div class="right_box">
                            <form action="">
                                <div class="d-flex">
                                    <div class="box__search d-flex">
                                        <input type="text" parsley-type="search" placeholder="Tìm kiếm nhân sự"
                                            name="data_filter[search]" class="form-control " id="search">
                                        <i class="fas fa-search ic_search_inside"></i>
                                    </div>
                                    <button type="button" onclick="btnSearch()"
                                        class="btn btn-blue btn_search_employee waves-effect waves-light ml-2"><i
                                            class="fas fa-search"></i> Tìm kiếm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            
                <div id="loadTable" class="mt-3">
                    <div class="table-scroll mb-2" id="table-scroll" style="border-right:none">
                        <div class="table-wrap" style="overflow: inherit;">
                            <table class=" main-table table-striped table-hover dt-responsive"
                                style="border-collapse: collapse; border-spacing: 0;">
                                <thead>
                                    <tr>
                                        <th class="text-center fixed-side">
                                           Họ và tên
                                        </th>
                                        <th>Mã</th>
                                        <th class="text-center">CMND/Passport</th>
                                        <th class="text-center">File khác</th>
                                    </tr>
                                </thead>
                                <?php if(isset($datas) && $datas != NULL){ ?>
                                    <tbody>
                                        <?php foreach ($datas as $key => $val) {
                                            $avatar = 'upload/employee/'.$val['code'].'/'.$val['avatar']; 
                                            $idFront = 'upload/employee/'.$val['code'].'/'.$val['id-front']; 
                                            $idBack = 'upload/employee/'.$val['code'].'/'.$val['id-back']; 
                                            ?>
                                            <tr class="tr<?=$val['id'];?>">
                                                <td  width="400" class="t_color_black fw-bold fixed-side">
                                                    <?php if(file_exists($avatar) && $val['avatar'] != ''){ ?>
                                                    <img src="<?=$avatar;?>" class="img-thumbnail rounded-circle mr-1" alt="avatar"
                                                        width="40" height="40">
                                                    <?php }else{ ?>
                                                    <img src="public/images/avatar-default.jpg"
                                                        class="img-thumbnail rounded-circle mr-1" alt="avatar" width="30">
                                                    <?php } ?>
                                                    <a href="cpanel/auths/profile/<?=$val['id']?>"
                                                        class="text-blue"><?php echo $val['fullname'];?></a>
                                                </td>
                                                <td width="200" class="t_color_green fw-bold">
                                                    <?php echo $val['code'];?>
                                                </td>
                                                <td width="300" class="t_color_green fw-bold">
                                                    <div class="d-flex">
                                                        <?php if(file_exists ($idFront) && $val['id-front'] != ''){ ?>
                                                            <div class="id_front  td_img">
                                                                <a class="fancybox" id="id_front<?=$val['id']?>" href="<?=$idFront?>">
                                                                    Xem mặt trước
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                        <?php if(file_exists ($idBack) && $val['id-back'] != ''){ ?>
                                                            <div class="id_back td_img">
                                                                <a class="fancybox"  id="id_back<?=$val['id']?>" href="<?=$idBack?>">
                                                                    Xem mặt sau
                                                                </a>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </td>
                                                <td class="t_color_green fw-bold">
                                                    <a href="" class="afile" download=""></a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                <?php } ?>
                            </table> 
                        </div>
                    </div>
                    <!-- pagination -->
                    <div class="wrap___flex d-flex t_justify_end ">
                        <div class="ChangeRow">
                       
                        </div>
                        <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                            <?php echo $my_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->

</div> <!-- end container-fluid -->

<!-- Xem nhanh thông tin nhân sự -->
<a href="" id="excel_dowload" download="" >  </a>
<div id="loadEmloyeeInfoDetail"></div>
<!-- Xem nhanh thông tin nhân sự END -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js" ></script>
<script type="text/javascript">
$(document).ready(function() {
    $(".fancybox").fancybox();
});
// Search
function btnSearch() {
    let url = "<?=base_url().'cpanel/document/search' ?>";
    // history.replaceState(null, null, gethref);
    let data = {
        "key_word": $("#search").val(),
        "type_pageload": "ajax",
    }
    CallAjax(data, url);
}
// PAGINATION
$(document).on('click', '.pagination .forClick a', function() {
    let checkKeyword = $("#search").val(); 
    let url = "cpanel/document/pagination";
    if(checkKeyword != "")
    {
        url = "cpanel/document/search";
    }
    let gethref = $(this).attr('href');
    let page = $(this).attr('data-ci-pagination-page');
    let data = {
        "key_word": checkKeyword,
        "page": page,
        "type_pageload": "ajax"
    }
    CallAjax(data, url);
    event.preventDefault();
});
function CallAjax(data, url) {
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function(res) {
            $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');  
            $("#loadTable").html(res);
        }
    });
}
// END PAGINATION
$(document).ready(function() {
    $("#action_change").val(0);
});

</script>