
<span class="time_overplay btn_close"></span>
<div class="popup_selectime popup_add">
         <div class="wrap__form">
            <div class="">
                <input type="hidden" id="timeclick" value="">
                <input type="hidden" name="data_post[title]" class="popup_title" id="popup_title" placeholder="Nhập nội dung">
            </div>
            <div class="alert_time "></div>
            <div class="mt-1 d-flex res-row">
                <div class="col-md-6 item_time d-flex">
                    <div class="d-label">
                         <label for="" class="">Bắt đầu</label>
                    </div>
                    <select name="data_post[starttime]" class="timesheet_day " id="starttime">
                        <?php $str_default = ""; 
                                $str_value = "";
                         for($i=7 ; $i <= 23 ; $i++){   
                            $str_default = $i < 10 ? "0" : ""; 
                            $str_value = $str_default.$i.":00:00"; ?>
                            <option  value="<?=$str_value?>"><?=$i?>:00</option>
                         <?php } ?>
                    </select>
                </div>
                <div class="col-md-6   item_time d-lex">
                    <label for="" class="d-label">Đến</label>
                    <select   class="timesheet_day "  name="data_post[endtime]" id="endtime">
                        <?php $str_default = ""; 
                                $str_value = "";
                         for($i=7 ; $i <= 23 ; $i++){   
                            $str_default = $i < 10 ? "0" : ""; 
                            $str_value = $str_default.$i.":00:00"; ?>
                            <option  value="<?=$str_value?>"><?=$i?>:00</option>
                         <?php } ?>
                    </select>
                </div>
            </div>
         </div>
         <div class="box__btn mt-3 ml-2">
            <input type="hidden" id="deleteid" value="">
            <button type="button" id="main_btn" class=" btn btn-blue btn_search_employee waves-effect waves-light mr-2 btn_save">Lưu</button>
            <button type="button" class="btn_close btn btn-blue btn_search_employee waves-effect waves-light mr-2">Hủy</button>
         </div>
    </div>

