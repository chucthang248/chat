<div class="table-scroll mb-2" id="table-scroll">
    <div class="table-wrap">
        <table class=" main-table table-striped table-hover dt-responsive" style="border-collapse: collapse; border-spacing: 0; width:2000px;">
            <thead>
                <tr>
                    <th class="text-center fixed-side">
                        <div class="checkbox checkbox-success checkbox-single">
                            <input type="checkbox" id="checkAll" name="publish" value="1" data-control="<?php echo $control; ?>">
                            <label class="mr-bottom_mn4"></label>
                        </div>
                    </th>
                    <th class="justify-content-center align-item-center fixed-side">Nhân sự</th>
                    <th>Mã</th>
                    <th class="text-center" width="100">Trạng thái</th>
                    <th class="text-center">Thâm niên</th>
                    <th class="text-center" width="120">Lương</th>
                    <th class="text-center" width="150">Lương cơ bản</th>
                    <th class="text-center" width="150">Chức danh</th>
                    <th class="text-center" width="150">Văn phòng</th>
                    <th class="text-center" width="150">Hợp đồng</th>
                    <th class="text-center" width="150">Điện thoại</th>
                    <th>Email</th>
                    <th>Ngày sinh</th>
                    <th>Giới tính</th>
                    <th class="text-center" width="150">Ngày bắt đầu</th>
                </tr>
            </thead>
            <?php if (isset($datas) && $datas != NULL) { ?>
                <tbody>
                    <?php foreach ($datas as $key => $val) {
                        $avatar = 'upload/employee/' . $val['code'] . '/' . $val['avatar'];  ?>
                        <tr class="tr<?= $val['id']; ?>">
                            <td class="text-center fixed-side">
                                <div class="checkbox checkbox-success checkbox-single mt-2">
                                    <input type="checkbox" class="input_checked_single" value="<?= $val['id']; ?>" name="publish" data-control="<?php echo $control; ?>">
                                    <label></label>
                                </div>
                            </td>
                            <td class="t_color_black width_fixed_320 fw-bold fixed-side">
                                <?php if (file_exists($avatar) && $val['avatar'] != '') { ?>
                                    <img src="<?= $avatar; ?>" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="40" height="40">
                                <?php } else { ?>
                                    <img src="public/images/avatar-default.jpg" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="30">
                                <?php } ?>
                                <a href="cpanel/auths/profile/<?= $val['id'] ?>" class="text-blue"><?php echo $val['fullname']; ?></a>
                                <a href="javascript:void(0)" onclick="viewEmloyeeInfoDetail(<?= $val['id']; ?>)" class="text-blue float-right mt-1"><i class="icon-eye font-20"></i></a>
                            </td>
                            <td class="t_color_green fw-bold">
                                <?php echo $val['code']; ?>
                            </td>
                            <td class="text-center">
                                <div class="round_status bg_status_1 round_status_color_<?= $val['status']; ?> "></div>
                            </td>
                            <td width="200" class="text-center">
                                <?= $val['startwork_name'] ?>
                            </td>
                            <td class="t_color_black fw-bold">
                                <?php echo $val['salary']['salary']; ?>
                            </td>
                            <td class="t_color_black fw-bold">
                                <?php echo $val['salary']['salary_basic']; ?>
                            </td>
                            <td>
                                <?php echo $val['position_name']; ?>
                            </td>
                            <td>
                                <?php echo $val['branch_name']; ?>
                            </td>
                            <td>
                                <?php echo $val['contract_name']; ?>
                            </td>
                            <td><?= $val['phone'] ?></td>
                            <td><?= $val['email'] ?></td>
                            <td><?= $val['birthday'] != '0000-00-00' && $val['birthday'] != '1970-01-01' ? date('d/m/Y', strtotime($val['birthday'])) : '' ?></td>
                            <td>
                                <?= $val['sex'] == 1 ? "Nam" : "Nữ" ?>
                            </td>
                            <td><?= implode("/", array_reverse(explode("-", $val['startwork'])));  ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } ?>
        </table>
        <!-- clone -->
        <table class=" main-table table-striped table-hover dt-responsive clone" style="border-collapse: collapse; border-spacing: 0; width:2000px;">
            <thead>
                <tr>
                    <th class="text-center fixed-side">
                        <div class="checkbox checkbox-success checkbox-single">
                            <input type="checkbox" id="checkAll" name="publish" value="1" data-control="<?php echo $control; ?>">
                            <label class="mr-bottom_mn4"></label>
                        </div>
                    </th>
                    <th class="justify-content-center align-item-center fixed-side">Nhân sự</th>
                    <th>Mã</th>
                    <th class="text-center" width="100">Trạng thái</th>
                    <th class="text-center">Thâm niên</th>
                    <th class="text-center" width="120">Lương</th>
                    <th class="text-center" width="150">Lương cơ bản</th>
                    <th class="text-center" width="150">Chức danh</th>
                    <th class="text-center" width="150">Văn phòng</th>
                    <th class="text-center" width="150">Hợp đồng</th>
                    <th class="text-center" width="150">Điện thoại</th>
                    <th>Email</th>
                    <th>Ngày sinh</th>
                    <th>Giới tính</th>
                    <th class="text-center" width="150">Ngày bắt đầu</th>
                </tr>
            </thead>
            <?php if (isset($datas) && $datas != NULL) { ?>
                <tbody>
                    <?php foreach ($datas as $key => $val) {
                        $avatar = 'upload/employee/' . $val['code'] . '/' . $val['avatar'];  ?>
                        <tr class="tr<?= $val['id']; ?>">
                            <td class="text-center fixed-side">
                                <div class="checkbox checkbox-success checkbox-single mt-2">
                                    <input type="checkbox" class="input_checked_single" value="<?= $val['id']; ?>" name="publish" data-control="<?php echo $control; ?>">
                                    <label></label>
                                </div>
                            </td>
                            <td class="t_color_black width_fixed_320 fw-bold fixed-side">
                                <?php if (file_exists($avatar) && $val['avatar'] != '') { ?>
                                    <img src="<?= $avatar; ?>" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="40" height="40">
                                <?php } else { ?>
                                    <img src="public/images/avatar-default.jpg" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="30">
                                <?php } ?>
                                <a href="cpanel/auths/profile/<?= $val['id'] ?>" class="text-blue"><?php echo $val['fullname']; ?></a>
                                <a href="javascript:void(0)" onclick="viewEmloyeeInfoDetail(<?= $val['id']; ?>)" class="text-blue float-right mt-1"><i class="icon-eye font-20"></i></a>
                            </td>
                            <td class="t_color_green fw-bold">
                                <?php echo $val['code']; ?>
                            </td>
                            <td class="text-center">
                                <div class="round_status bg_status_1 round_status_color_<?= $val['status']; ?> "></div>
                            </td>
                            <td width="200"  class="text-center">
                                <?= $val['startwork_name'] ?>
                            </td>
                            <td class="t_color_black fw-bold">
                                <?php echo $val['salary']['salary']; ?>
                            </td>
                            <td class="t_color_black fw-bold">
                                <?php echo $val['salary']['salary_basic']; ?>
                            </td>
                            <td>
                                <?php echo $val['position_name']; ?>
                            </td>
                            <td>
                                <?php echo $val['branch_name']; ?>
                            </td>
                            <td>
                                <?php echo $val['contract_name']; ?>
                            </td>
                            <td><?= $val['phone'] ?></td>
                            <td><?= $val['email'] ?></td>
                            <td><?= $val['birthday'] != '0000-00-00' && $val['birthday'] != '1970-01-01' ? date('d/m/Y', strtotime($val['birthday'])) : '' ?></td>
                            <td>
                                <?= $val['sex'] == 1 ? "Nam" : "Nữ" ?>
                            </td>
                            <td><?= implode("/", array_reverse(explode("-", $val['startwork'])));  ?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } ?>
        </table>
    </div>
</div>

<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end ">
    <div class="ChangeRow">
        <span>Hiển thị: </span>
        <select class="select_row ml-2" id="select_row">
            <option <?= $per_page == 5 ? "selected" : "" ?> value="5">5</option>
            <option <?= $per_page == 10 ? "selected" : "" ?> value="10">10</option>
            <option <?= $per_page == 20 ? "selected" : "" ?> value="20">20</option>
            <option <?= $per_page == 30 ? "selected" : "" ?> value="30">30</option>
            <option <?= $per_page == 40 ? "selected" : "" ?> value="30">40</option>
            <option <?= $per_page == 50 ? "selected" : "" ?> value="30">50</option>
            <option <?= $per_page == 60 ? "selected" : "" ?> value="30">60</option>
            <option <?= $per_page == 100 ? "selected" : "" ?> value="30">100</option>
        </select>
    </div>
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
        <?php echo $my_pagination; ?>
    </div>
</div>