<div class="table-scroll mb-2" id="table-scroll" style="border-right:none">
    <div class="table-wrap" style="overflow: inherit;">
        <table class=" main-table table-striped table-hover dt-responsive" style="border-collapse: collapse; border-spacing: 0;">
            <thead>
                <tr>
                    <th class="text-center fixed-side">
                        Họ và tên
                    </th>
                    <th>Mã</th>
                    <th>CMND</th>
                    <th>File</th>
                </tr>
            </thead>
            <?php if (isset($datas) && $datas != NULL) { ?>
                <tbody>
                    <?php foreach ($datas as $key => $val) {
                        $avatar = 'upload/employee/' . $val['code'] . '/' . $val['avatar'];
                        $idFront = 'upload/employee/' . $val['code'] . '/' . $val['id-front'];
                        $idBack = 'upload/employee/' . $val['code'] . '/' . $val['id-back'];
                    ?>
                        <tr class="tr<?= $val['id']; ?>">
                            <td width="400" class="t_color_black fw-bold fixed-side">
                                <?php if (file_exists($avatar) && $val['avatar'] != '') { ?>
                                    <img src="<?= $avatar; ?>" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="40" height="40">
                                <?php } else { ?>
                                    <img src="public/images/avatar-default.jpg" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="30">
                                <?php } ?>
                                <a href="cpanel/auths/profile/<?= $val['id'] ?>" class="text-blue"><?php echo $val['fullname']; ?></a>
                                <a href="javascript:void(0)" onclick="viewEmloyeeInfoDetail(<?= $val['id']; ?>)" class="text-blue float-right mt-1"></a>
                            </td>
                            <td width="200" class="t_color_green fw-bold">
                                <?php echo $val['code']; ?>
                            </td>
                            <td width="300" class="t_color_green fw-bold">
                                <div class="d-flex">
                                    <div class="id_front  td_img">
                                        <a class="fancybox" id="id_front<?= $val['id'] ?>" href="<?= $idFront ?>">
                                            Xem mặt trước
                                        </a>
                                    </div>
                                    <div class="id_back td_img">
                                        <a class="fancybox" id="id_back<?= $val['id'] ?>" href="<?= $idBack ?>">
                                            Xem mặt sau
                                        </a>
                                    </div>
                                </div>
                            </td>
                            <td class="t_color_green fw-bold">
                                <div class="row">
                                    <div class="col-md-9">

                                    </div>
                                    <div class="col-md-3">
                                        <a href="" class="afile" download=""></a>
                                    </div>
                                </div>
                            </td>

                        </tr>
                    <?php } ?>
                </tbody>
            <?php } ?>
        </table>
    </div>
</div>
<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end ">
    <div class="ChangeRow">
    </div>
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
        <?php echo $my_pagination; ?>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>