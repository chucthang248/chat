<link href="public/assets/customs/profile_refer_material.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form action="cpanel/auths/profile_refer_material<?=$employeeID?>" method="POST"  novalidate="" id="myForm" enctype="multipart/form-data">
        <div class="box-body">
            <div class="title">
                Thêm tài liệu tham chiếu
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs" />
            <div class="box-form">
                <div class="box_center">
                    <div class="radio form-check-inline">
                        <input type="radio" id="cmnd" value="cmnd" name="identify_type" <?=$employee['identify_type'] =="cmnd" ? "checked" : ""?>>
                        <label for="cmnd"> CMND </label>
                    </div>
                    <div class="radio form-check-inline">
                        <input type="radio" id="passport" value="passport" name="identify_type" <?=$employee['identify_type'] =="passport" ? "checked" : ""?>>
                        <label for="passport"> Passport </label>
                    </div>
                    <div class="clear mt-2"></div>
                    <div class="form-group row">
                        <label for="identify_number" class="col-3 col-form-label">Số <span
                                id="identify-type-text">CMND</span><span class="text-danger">*</span></label>
                        <div class="col-9">
                            <input type="text" required parsley-type="identify_number" name="data_post[identify_number]"
                              value="<?=$employee['identify_number'] ?>"  class="form-control" id="identify_number">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="identify_time"  class="col-3 col-form-label">Ngày cấp<span
                                class="text-danger">*</span></label>
                        <div class="col-9">
                            <input type="text" required parsley-type="identify_time" name="data_post[identify_time]"
                                class="form-control" id="identify_time" data-toggle="input-mask" value="<?=$employee['identify_time'] ?>"
                                data-mask-format="00/00/0000" placeholder="21/04/1991">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="identify_place" class="col-3 col-form-label">Nơi cấp<span
                                class="text-danger">*</span></label>
                        <div class="col-9">
                            <input type="text" required parsley-type="identify_place" value="<?=$employee['identify_place'] ?>" name="data_post[identify_place]"
                                class="form-control" id="identify_place">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <div class="form-group row">
                            <div class="upload_card id-front">
                                <img id="preview-id-front" src="<?php if($employee['id-front'] != "") { echo "upload/employee/".$employee['code']."/".$employee['id-front'];}else{ echo "public/images/id-front.png";} ?>" alt="your image" title='' />
                                <button type="button" class="upload_btn">
                                    <input type="file" id="id-front" name="id-front" class="imgInp custom-file-input" >
                                        Chọn ảnh mặt trước
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <div class="upload_card id-front">
                                <img id="preview-id-back" src="<?php if($employee['id-back'] !="") { echo "upload/employee/".$employee['code']."/".$employee['id-back'];}else{ echo "public/images/id-back.png";} ?>" alt="your image" title='' />
                                <button type="button" class="upload_btn">
                                    <input type="file" id="id-back" name="id-back" class="imgInp custom-file-input" >
                                    Chọn ảnh mặt sau
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            <div class="box-btn text-center">
                <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                <button class="btn btn-blue">Lưu lại</button>
            </div>
        </div>
    </form>
</div>
<script>
function closeForm() {
    $('#loadFormReferenceMaterial').html('');
}
   //input id-front
   $("#id-front").change(function(event) {  
        readURLIDFRONT(this);    
    });
    function readURLIDFRONT(input) {    
        if (input.files && input.files[0]) {   
            var reader = new FileReader();
            var filename = $("#id-front").val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                $('#preview-id-front').attr('src', e.target.result);
                $('#preview-id-front').hide();
                $('#preview-id-front').fadeIn(500);      
                // $('.custom-file-label').text(filename);             
            }
            reader.readAsDataURL(input.files[0]);    
        } 
    }
    //input id-back
    $("#id-back").change(function(event) {  
        readURLIDBACK(this);    
    });
    function readURLIDBACK(input) {    
        if (input.files && input.files[0]) {   
            var reader = new FileReader();
            var filename = $("#id-back").val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                $('#preview-id-back').attr('src', e.target.result);
                $('#preview-id-back').hide();
                $('#preview-id-back').fadeIn(500);      
                // $('.custom-file-label').text(filename);             
            }
            reader.readAsDataURL(input.files[0]);    
        } 
    }
</script>