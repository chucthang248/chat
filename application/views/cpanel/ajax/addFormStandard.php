
<div class="remove_parent">
    <div class="box_des_standard">
        <hr/>
        <i class="fas fa-times x__item_close"></i>
        <div class="row">
            <div class="col-6">
                <div class="form-group row">
                    <label class="col-3 col-form-label">Từ ngày</label>
                    <div class="col-9">
                        <input type="text" name="data_post_standard[date_from][]" class="form-control" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="01/01/2018">
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="form-group row">
                    <label class="col-3 col-form-label">Đến ngày</label>
                    <div class="col-9">
                        <input type="text" name="data_post_standard[date_to][]" class="form-control" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="01/01/2020">
                    </div>
                </div>
            </div>
            <div class="col-12">
                <div class="form-group row">
                    <label class="col-12 col-form-label">Tóm tắt nội dung</label>
                    <div class="col-12">
                        <textarea rows="5" name="data_post_standard[content][]" class="form-control"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vendor js -->
    <script src="public/assets/js/vendor.min.js"></script>
    <!-- form mask js -->
    <script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('[data-toggle="input-mask"]').each(function (idx, obj) {
                var maskFormat = $(obj).data("maskFormat");
                var reverse = $(obj).data("reverse");
                if (reverse != null)
                    $(obj).mask(maskFormat, {'reverse': reverse});
                else
                    $(obj).mask(maskFormat);
            });
        });
        
    </script>   
</div>
