<link href="public/assets/customs/profile_add_account_bank_info.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form  action="cpanel/auths/profile_account_bank<?=$employeeID?>" method="POST">
        <input type="hidden" name="id_account_bank" value="<?=$EmployeeBank['id']?>">
        <div class="box-body">
            <div class="title">
               Cập nhật
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs"/>
            <div class="box-form">
                <div class="form-group">
                    <label for="bankID" class="col-form-label">Ngân hàng</label>
                    <select id="bankID" parsley-type="sex" name="data_update_bank[bankID]" class="form-control">
                        <option value="">Chọn loại ngân hàng</option>
                        <?php if(isset($banks) && $banks != NULL){ ?>
                            <?php foreach ($banks as $key_bank => $val_bank) { ?>
                                <option value="<?=$val_bank['id'];?>" <?=$val_bank['id']==$EmployeeBank['bankID']? "selected" : "";  ?>><?=$val_bank['name'];?></option>
                            <?php } ?>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group">
                    <label for="bank_number" class="col-form-label">Số TK</label>
                    <input type="text" parsley-type="phone" name="data_update_bank[bank_number]" value="<?=$EmployeeBank['bank_number'] != ''?$EmployeeBank['bank_number']:TEXT_UPDATE; ?>" class="form-control" id="bank_number">
                </div>
                <div class="form-group">
                    <label for="bank_name" class="col-form-label">Tên TK</label>
                    <input type="text" parsley-type="phone" name="data_update_bank[bank_name]"  value="<?=$EmployeeBank['bank_name'] != ''?$EmployeeBank['bank_name']:TEXT_UPDATE; ?>" class="form-control" id="bank_name">
                </div>
                <div class="form-group">
                    <label for="bank_branch" class="col-form-label">Chi nhánh</label>
                    <input type="text" parsley-type="phone" name="data_update_bank[bank_branch]"  value="<?=$EmployeeBank['bank_branch'] != ''?$EmployeeBank['bank_branch']:TEXT_UPDATE; ?>"  class="form-control" id="bank_branch">
                </div>
            </div>
            <hr/>
            <div class="box-btn text-center">
                <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                <button class="btn btn-blue">Lưu lại</button>
            </div>
        </div>
    </div>
</div>
<script>
    function closeForm(){
        $('#loadFormAddAccountBankInfo').html('');
    }
</script>