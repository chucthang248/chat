<link href="public/assets/customs/profile_update_employeelevel.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form action="cpanel/auths/profile_update_employeelevel<?=$employeeID?>" method="POST">
        <div class="box-body">
            <div class="title">
                Cập nhật trình độ học vấn
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs" />
            <div class="box_des_standard">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="edu_statusID" class="col-3 col-form-label">Tình trạng học vấn</label>
                            <div class="col-9">
                                <select id="edu_statusID" parsley-type="sex"
                                    name="data_post[edu_statusID]" class="form-control">
                                    <option value="">Loại học vấn </option>
                                    <?php if(isset($educstatus) && $educstatus != NULL){ ?>
                                    <?php foreach ($educstatus as $key_educstatus => $val_educstatus) { ?>
                                      <option value="<?=$val_educstatus['id'];?>" <?=$val_educstatus['id'] == $employeelevel_ref['edu_statusID'] ? "selected" : "";?>><?=$val_educstatus['name'];?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="standardID" class="col-3 col-form-label">Trình độ</label>
                            <div class="col-9">
                                <select id="standardID" parsley-type="sex"
                                    name="data_post[standardID]" class="form-control">
                                    <option value="">Loại trình độ</option>
                                    <?php if(isset($standards) && $standards != NULL){ ?>
                                    <?php foreach ($standards as $key_standards => $val_standards) { ?>
                                        <option value="<?=$val_standards['id'];?>" <?=$val_standards['id'] == $employeelevel_ref['standardID'] ? "selected" : "";?>><?=$val_standards['name'];?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>   
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="schoolsID" class="col-3 col-form-label">Trường học</label>
                            <div class="col-9">
                                <select id="schoolsID" parsley-type="sex"
                                    name="data_post[schoolsID]" class="form-control">
                                    <option value="">Loại trình độ</option>
                                    <?php if(isset($school) && $school != NULL){ ?>
                                    <?php foreach ($school as $key_school => $val_school) { ?>
                                    <option value="<?=$val_school['id'];?>"  <?=$val_school['id'] == $employeelevel_ref['schoolsID'] ? "selected" : "";?> ><?=$val_school['name'];?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>            
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="majorsID" class="col-3 col-form-label">Ngành học</label>
                            <div class="col-9">
                                <select id="majorsID" parsley-type="sex"
                                    name="data_post[majorsID]" class="form-control">
                                    <option value="">Chọn ngành học</option>
                                    <?php if(isset($majors) && $majors != NULL){ ?>
                                    <?php foreach ($majors as $key_majors => $val_majors) { ?>
                                    <option value="<?=$val_majors['id'];?>"  <?=$val_majors['id'] == $employeelevel_ref['majorsID'] ? "selected" : "";?> ><?=$val_majors['name'];?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>                          
                </div>
                <div class="box-btn text-center">
                    <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                    <button class="btn btn-blue">Lưu lại</button>
                </div>
            </div>
        </div>
    </form>

</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
// đóng form
function closeForm() {
    $('#loadForm_employeelevel').html('');
}

$(document).ready(function() {
    $('[data-toggle="input-mask"]').each(function(idx, obj) {
        var maskFormat = $(obj).data("maskFormat");
        var reverse = $(obj).data("reverse");
        if (reverse != null)
            $(obj).mask(maskFormat, {
                'reverse': reverse
            });
        else
            $(obj).mask(maskFormat);
    });
});
</script>