<div class="table-scroll mb-2">
    <div class="table-wrap_other">
        <table class=" table-striped table-hover ">
            <thead>
                <tr>
                    <th class="text-center">Nhật ký hoạt động <?=$showDay ?></th>
                </tr>
            </thead>
            <?php if (isset($datas) && $datas != NULL) { ?>
                <tbody>
                    <?php foreach ($datas as $key => $val) { ?>
                        <tr class="tr<?= $val['id']; ?>">
                            <td>
                                <div class="row">
                                    <div class="col-md-2">
                                        <strong><?= $val['email'] ?></strong>
                                    </div>
                                    <div class="col-md-auto">
                                        <?= $val['des'] ?> ngày: <?= $val['created_at'] ?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } ?>
        </table>
    </div>
</div>

<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end ">
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
        <?php echo $my_pagination; ?>
    </div>
</div>