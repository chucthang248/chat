<link href="public/assets/customs/profile_add_relative_info.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form  action="cpanel/auths/profile_relative<?=$employeeID?>" method="POST">
        <div class="box-body">
            <div class="title">
                Thêm người liên lạc
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs"/>
            <div class="box-form">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="fullname" class="col-form-label">Họ & tên</label>
                            <input type="text" parsley-type="phone" name="data_post_relative[fullname]" class="form-control" id="fullname">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="relationship" class="col-form-label">Mối quan hệ</label>
                            <input type="text" parsley-type="relationship" name="data_post_relative[relationship]" class="form-control" id="relationship">
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <label for="phone" class="col-form-label">Số điện thoại</label>
                            <input type="text" parsley-type="phone" name="data_post_relative[phone]" class="form-control" id="phone">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-form-label">Địa chỉ</label>
                    <input type="text" parsley-type="address" name="data_post_relative[address]" class="form-control" id="address">
                </div>
            </div>
            <hr/>
            <div class="box-btn text-center">
                <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                <button class="btn btn-blue">Lưu lại</button>
            </div>
        </div>
    </form>
</div>
<script>
    function closeForm(){
        $('#loadFormAddRelativeInfo').html('');
    }
</script>