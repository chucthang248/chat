<div class="table-scroll mb-2" id="table-scroll" style="border-right:none !important;">
    <div class="table-wrap">
        <table class="table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;border-right:none !important;">
            <thead>
                <tr>
                    <th>Mã</th>
                    <th>Tên nhân viên</th>
                    <th>Tác vụ</th>
                </tr>
            </thead>
            <?php if (isset($datas) && $datas != NULL) { ?>
                <tbody>
                    <?php foreach ($datas as $key => $val) { ?>
                        <tr>
                            <td><?php echo  $val['code']; ?></td>
                            <td><?php echo  $val['fullname']; ?></td>
                            <td width="200" class="text-center">
                                <a href="javascript:void(0)" class="btn btn-blue btn-xs waves-effect waves-light view_timework" data-id="<?php echo $val['id']; ?>">
                                    <i class="icon-eye "></i>
                                </a>
                            </td>
                        </tr>
                    <?php } ?>
                </tbody>
            <?php } ?>
        </table>
    </div>
</div>

<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end ">
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto  mr-2" id="datatable_paginate">
        <?php echo $my_pagination; ?>
    </div>
</div>