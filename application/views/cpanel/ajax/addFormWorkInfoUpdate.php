<link href="public/assets/customs/profile_work_info.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form action="cpanel/auths/profile_update_info<?=$employeeID?>" method="POST">
        <div class="box-body">
            <div class="title">
                Cập nhật thông tin công việc
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs"/>
            <div class="box-form">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                            <label for="startwork" class="col-form-label">Ngày bắt đầu</label>
                            <input type="text" required parsley-type="startwork" class="form-control"  name="data_update_work_info[startwork]"id="startwork" value="<?=$historyWork['startwork']?>" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="21/04/1991">
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="branch" class="col-form-label">Văn phòng</label>
                            <select  name="data_update_work_info[branchID]"  class="form-control">
                                <option value="">Chọn văn phòng</option>
                                <?php if(isset($branch) && $branch != NULL){ ?>
                                    <?php foreach ($branch as $key_branch => $val_branch) { ?>
                                        <option value="<?=$val_branch['id'];?>" 
                                         <?php if($val_branch['id'] == $historyWork['branchID'] ){ echo "selected";} ?> ><?=$val_branch['name'];?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="ogchartID" class="col-form-label">Phòng ban</label>
                            <select id="ogchartID" name="data_update_work_info[ogchartID]" class="form-control">
                                <option value="">Chọn phòng ban</option>
                                <?php foreach($orgchart as $key => $val){ ?>
                                    <option <?=$historyWork['ogchartID'] == $val['id'] ? "selected" : "" ?>   value="<?=$val['id'] ?>"><?=str_repeat("---",$val['count']).$val['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="job_positionID" class="col-form-label">Vị trí công việc</label>
                            <select id="job_positionID" onchange="jobpoisition(this)"  name="data_update_work_info[job_positionID]" class="form-control">
                                <option value="">Chọn vị trí công việc</option>
                                <?php foreach($job_position as $key => $val){ ?>
                                    <option <?=$historyWork['job_positionID'] == $val['id'] ? "selected" : "" ?>   value="<?=$val['id'] ?>"><?=str_repeat("---",$val['count']).$val['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="branch" class="col-form-label">Chức danh</label>
                            <select  name="data_update_work_info[job_position_titleID]" id="job_position_titleID"   class="form-control">
                                <option value="">--Chọn chức danh--</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="branch" class="col-form-label">Lịch làm việc</label>
                            <select   class="form-control">
                                <option value="">Chọn</option>
                                <?php if(isset($banks) && $banks != NULL){ ?>
                                    <?php foreach ($banks as $key_bank => $val_bank) { ?>
                                        <option value="<?=$val_bank['id'];?>"><?=$val_bank['name'];?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="classify_employeeID" class="col-form-label">Phân loại nhân sự</label>
                            <select  name="data_update_work_info[classify_employeeID]" class="form-control">
                                <option value="">Chọn</option>
                                <?php if(isset($classify_employee) && $classify_employee != NULL){ ?>
                                    <?php foreach ($classify_employee as $key_classify_employee => $val_classify_employee) { ?>
                                        <option <?=$historyWork['classify_employeeID'] == $val_classify_employee['id'] ? "selected" : "" ?> value="<?=$val_classify_employee['id'];?>"><?=$val_classify_employee['name'];?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <hr/>
            <div class="box-btn text-center">
                <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                <button class="btn btn-blue">Lưu lại</button>
            </div>
        </div>
    </form>
</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
    function closeForm(){
        $('#loadFormWorkInfo').html('');
    }
    // vị trí công việc
    function jobpoisition(_this)
    {
        let employeeID = "<?=$main_employeeID?>";
        let id = $(_this).val();
        debugger;
        $.ajax({
            type: "POST",
            url: "cpanel/ajax/loadPosition",
            data: {id:id, employeeID : employeeID},
            success: function (response) {
                 $("#job_position_titleID").html(response);
            }
        });
    }
    $(document).ready(function(){
        $("#job_positionID").trigger("change");
    });
    $('[data-toggle="input-mask"]').each(function (idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {'reverse': reverse});
            else
                $(obj).mask(maskFormat);
        });
</script>