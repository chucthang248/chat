<link href="public/assets/customs/profile_add_progress_learn.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form action="cpanel/auths/profile_progress_learn<?=$employeeID?>" method="POST">
    <input type="hidden" name="id_progress_learn" value="<?=$EmployeeStandard['id']; ?>">
        <div class="box-body">
            <div class="title">
                Cập nhật quá trình học tập
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs" />
            <div class="box_des_standard">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="date_from" class="col-3 col-form-label">Từ ngày</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_learn[date_from]"
                                    class="form-control" id="date_from" data-toggle="input-mask" value=<?=$EmployeeStandard['date_from']; ?>
                                    data-mask-format="00/00/0000" placeholder="01/01/2018"> 
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="date_to" class="col-3 col-form-label" >Đến ngày</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_learn[date_to]"
                                    class="form-control" id="date_to" data-toggle="input-mask"  value=<?=$EmployeeStandard['date_to']; ?>
                                    data-mask-format="00/00/0000" placeholder="01/01/2020">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="content" class="col-12 col-form-label">Mô tả công việc</label>
                            <div class="col-12">
                                <textarea parsley-type="phone" rows="5" name="data_update_learn[content]"
                                    class="form-control" id="content"><?=$EmployeeStandard['content']; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-btn text-center">
                    <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                    <button class="btn btn-blue">Lưu lại</button>
                </div>
            </div>
        </div>
    </form>

</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
// đóng form
function closeForm() {
    $('#loadForm_ProgressLearn').html('');
}
$(document).ready(function() {
    $('[data-toggle="input-mask"]').each(function(idx, obj) {
        var maskFormat = $(obj).data("maskFormat");
        var reverse = $(obj).data("reverse");
        if (reverse != null)
            $(obj).mask(maskFormat, {
                'reverse': reverse
            });
        else
            $(obj).mask(maskFormat);
    });
});
</script>