<link href="public/assets/css/orgcharts.css" rel="stylesheet" />
<div class="show-modal">
    <form action="cpanel/jobposition/add" method="POST">
        <div class="overplay_modal" onclick="close_form()"></div>
        <div class="wrap_modal">    
            <p class="title_modal font-weight-medium text-truncate">Thêm vị trí công việc<a  href="javascript:void(0)" onclick="close_form()"><i class="fas fa-times"></i></a></p>
            <div class="box__item">
                <div class="form-group each_item">
                    <label for="">Tên vị trí *</label>
                    <input type="text" name="data_post[name]" class="form-control">
                </div>
                <?php if($datas != NULL){ ?>
                    <div class="form-group each_item">
                        <label for="">Vị trí công việc *</label>
                        <div class="box-selected">
                            <select class="parentID" name="data_post[parentID]" >
                                <option value="">Chọn vị trí</option>
                                <?php foreach($datas as $key => $val){ ?>
                                    <option value="<?=$val['id'] ?>"> <?=str_repeat("---",$val['count']).$val['name'] ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="box__btn">
                 <button type="submit" class="btn btn-blue ml-auto mb-1">Lưu</button>
            </div>
        </div>
     
    </form>
</div>
<script>
    function close_form()
    {
        $("#loadForm").html('');
    }
  
</script>