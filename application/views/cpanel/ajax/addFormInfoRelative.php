<div class="remove_parent bl_relative">
     <hr>
    <i class="fas fa-times x__item_close"></i> 
    <div class="row">
        <div class="col-3">
            <div class="form-group row">
                <label for="fullname_relative" class="col-3 col-form-label">Họ & tên</label>
                <div class="col-9">
                    <input type="text" parsley-type="fullname_relative" name="data_post_relative[fullname][]" class="form-control" id="fullname_relative">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group row">
                <label for="relationship_relative" class="col-6 col-form-label">Mối quan hệ</label>
                <div class="col-6">
                    <input type="text" parsley-type="relationship_relative" name="data_post_relative[relationship][]" class="form-control" id="relationship_relative">
                </div>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group row">
                <label for="phone_relative" class="col-3 col-form-label">SĐT</label>
                <div class="col-9">
                    <input type="text" parsley-type="phone_relative" name="data_post_relative[phone][]" class="form-control" id="phone_relative">
                </div>
            </div>
        </div>
        <div class="col-5">
            <div class="form-group row">
                <label for="address_relative" class="col-2 col-form-label">Địa chỉ</label>
                <div class="col-10">
                    <input type="text" parsley-type="address_relative" name="data_post_relative[address][]" class="form-control" id="address_relative">
                </div>
            </div>
        </div>
    </div>
</div>
