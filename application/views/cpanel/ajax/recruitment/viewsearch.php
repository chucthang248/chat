<table id="datatable" class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Công việc ứng tuyển</th>
                            <th>Cấp bậc</th>
                            <th>Hình thức làm việc</th>
                            <th>Mức lương</th>
                            <th>Hạn nộp</th>
                            <th>Ngày tạo</th>
                            <th>Hiển thị</th>
                            <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if(isset($datas) && $datas != NULL){ ?>
                    <tbody>
                        <?php foreach ($datas as $key => $val) { ?>
                        <tr>
                            <td><?php echo $val['title'];?></td>
                            <td><?php echo $val['name_rank'];?></td>
                            <td><?php echo $val['name_form'];?></td>
                            <td><?php echo $val['income'];?></td>
                            <td class="text-center"><?php echo date('d/m/Y',strtotime($val['date_submit']));?></td>
                            <td class="text-center"><?php echo date('d/m/Y',strtotime($val['created_at']));?></td>
                            <td class="text-center">
                                <div class="checkbox checkbox-blue checkbox-single mt-2">
                                    <input <?php if($val['publish'] == 1){ ?> checked="" <?php } ?> onclick="checkStatus(<?php echo $val['id'];?>,'publish');" type="checkbox" id="publish<?php echo $val['id'];?>" name="publish" value="1" data-control="<?php echo $control;?>">
                                    <label></label>
                                </div>
                            </td>
                            <td class="text-center">
                                <a href="<?=CPANEL.$control.'/edit/'.$val['id']?>" class="btn btn-blue btn-xs waves-effect waves-light">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a href="javascript:void(0)" onclick="del(<?php echo $val['id'];?>);" class="btn btn-danger btn-xs waves-effect waves-light delete<?php echo $val['id'];?>" data-control="<?php echo $control;?>" >
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                             
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <?php } ?>
                </table>