<link href="public/assets/customs/profile_workinfo_add_history_work.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form action="cpanel/auths/mywork_history_work<?=$employeeID?>" method="POST">
        <input type="hidden" name="id_work" value="<?=$EmployeeWork['id']?>">
        <div class="box-body">
            <div class="title">
                Cập nhật lịch sử công tác
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs" />
            <div class="box_des_standard">
                <div class="row">
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="date_from" class="col-3 col-form-label">Từ ngày</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_historywork[date_from]"
                                    class="form-control" id="date_from" data-toggle="input-mask" value="<?=$EmployeeWork['date_from']?>"
                                    data-mask-format="00/00/0000" placeholder="01/01/2018"> 
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="date_to" class="col-3 col-form-label">Đến ngày</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_historywork[date_to]"
                                    class="form-control" id="date_to" data-toggle="input-mask"  value="<?=$EmployeeWork['date_to']?>"
                                    data-mask-format="00/00/0000" placeholder="01/01/2020">
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="work_unit" class="col-3 col-form-label">ĐV công tác</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_historywork[work_unit]"  value="<?=$EmployeeWork['work_unit']?>"
                                    class="form-control" id="work_unit"> 
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="bussinesstypeID" class="col-3 col-form-label">Loại hình Cty</label>
                            <div class="col-9">
                                <select id="bussinesstypeID" parsley-type="sex"
                                    name="data_update_historywork[bussinesstypeID]" class="form-control">
                                    <option value="">Chọn loại hình công ty</option>
                                    <?php if(isset($bussinesstypes) && $bussinesstypes != NULL){ ?>
                                    <?php foreach ($bussinesstypes as $key_bussinesstypes => $val_bussinesstypes) { ?>
                                         <option value="<?=$val_bussinesstypes['id'];?>" <?=$val_bussinesstypes['id']==$EmployeeWork['bussinesstypeID']? "selected" :"" ;?>><?=$val_bussinesstypes['name'];?>
                                    </option>
                                    <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="position" class="col-3 col-form-label">Chức vụ đảm nhiệm</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_historywork[position]"  value="<?=$EmployeeWork['position']?>"
                                    class="form-control" id="position"> 
                            </div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group row">
                            <label for="concurrently" class="col-3 col-form-label">Chức vụ kiêm nhiệm</label>
                            <div class="col-9">
                                <input type="text" parsley-type="phone" name="data_update_historywork[concurrently]"  value="<?=$EmployeeWork['concurrently']?>"
                                    class="form-control" id="concurrently">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group row">
                            <label for="description" class="col-12 col-form-label">Mô tả công việc</label>
                            <div class="col-12">
                                <textarea parsley-type="phone" rows="5" name="data_update_historywork[description]"
                                    class="form-control" id="description"><?=$EmployeeWork['description']?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-btn text-center">
                    <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                    <button class="btn btn-blue">Lưu lại</button>
                </div>
            </div>
        </div>
    </form>

</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
// đóng form
function closeForm() {
    $('#loadFormWorkinfo_History').html('');
}
$(document).ready(function() {
    $('[data-toggle="input-mask"]').each(function(idx, obj) {
        var maskFormat = $(obj).data("maskFormat");
        var reverse = $(obj).data("reverse");
        if (reverse != null)
            $(obj).mask(maskFormat, {
                'reverse': reverse
            });
        else
            $(obj).mask(maskFormat);
    });
});
</script>