<link href="public/assets/css/orgcharts.css" rel="stylesheet" />
<div class="show-modal">
    <form action="cpanel/orgcharts/add" method="POST">
        <div class="overplay_modal" onclick="close_form()"></div>
        <div class="wrap_modal">    
            <p class="title_modal font-weight-medium text-truncate">Tạo mới phòng ban <a  href="javascript:void(0)" onclick="close_form()"><i class="fas fa-times"></i></a></p>
            <div class="box__item">
                <div class="form-group each_item">
                    <label for="">Tên phòng ban *</label>
                    <input type="text" name="data_post[name]" class="form-control">
                </div>
                <div class="form-group each_item">
                    <label for="">Phòng ban cha *</label>
                    <div class="box-selected">
                        <select class="parentID" name="data_post[parentID]" >
                            <option value="">Chọn phòng ban</option>
                            <?php foreach($ogchart as $key => $val){ ?>
                                <option  value="<?=$val['id'] ?>"><?=str_repeat("---",$val['count']).$val['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group each_item">
                    <label for="">Chức năng nhiệm vụ</label>
                    <textarea class="ckeditor " name="data_post[mission]" id=""></textarea>
                </div>
            </div>
            <div class="box__btn">
                 <button type="submit" class="btn btn-blue ml-auto mb-1">Lưu</button>
            </div>
        </div>
     
    </form>
</div>
<script>
    function close_form()
    {
        $("#loadForm").html('');
    }
    CKEDITOR.replace( 'data_post[mission]', {
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ],
            ['Image']
        ]
    });
</script>