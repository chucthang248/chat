<link href="public/assets/customs/profile_personal_info.css" rel="stylesheet" type="text/css" />
<div class="box">
    <form action="cpanel/auths/profile_update_info<?=$employeeID?>" method="POST">
        <div class="box-body">
            <div class="title">
                Cập nhật thông tin
                <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
            </div>
            <hr class="hr-xs" />
            <div class="box-form">

                <div class="row">
                    <div class="col-8">
                        <div class="form-group">
                            <label for="fullname" class="col-form-label">Họ và tên<span
                                    class="text-danger">*</span></label>
                            <input type="text" required parsley-type="fullname" value="<?=$employee['fullname']?>" name="data_update_info[fullname]"
                                class="form-control" id="fullname">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                            <label for="sex" class="col-form-label">Giới tính<span class="text-danger">*</span></label>
                            <select id="sex" parsley-type="sex" name="data_update_info[sex]" class="form-control">
                                <option <?=$employee['sex']==1 ? "selected" : ""?> value="1">Nam</option>
                                <option <?=$employee['sex']==2 ? "selected" : ""?> value="2">Nữ</option>
                                <option <?=$employee['sex']==3 ? "selected" : ""?>value="3">Khác</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="marital" class="col-form-label">Hôn nhân<span
                                    class="text-danger">*</span></label>
                            <select id="marital" parsley-type="marital" name="data_update_info[marital]" class="form-control">
                                <option <?=$employee['marital']==1 ? "selected" : ""?> value="1">Độc thân</option>
                                <option <?=$employee['marital']==2 ? "selected" : ""?> value="2">Đã kết hôn</option>
                                <option <?=$employee['marital']==3 ? "selected" : ""?> value="3">Khác</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="birthday" class="col-form-label">Ngày sinh<span
                                    class="text-danger">*</span></label>
                            <input type="text" required parsley-type="birthday" class="form-control" id="birthday" data-toggle="input-mask" data-mask-format="00/00/0000"
                            value="<?=$employee['birthday']?>"    placeholder="21/04/1991" name="data_update_info[birthday]">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="permanent_address" class="col-form-label">Đ/C thường trú<span
                            class="text-danger">*</span></label>
                    <textarea required parsley-type="code" class="form-control" name="data_update_info[permanent_address]"
                        rows="4" id="permanent_address"><?=$employee['permanent_address']?></textarea>
                </div>
                <div class="form-group">
                    <label for="temporary_address" class="col-form-label">Đ/C tạm trú<span
                            class="text-danger">*</span></label>
                    <textarea required parsley-type="code" class="form-control" name="data_update_info[temporary_address]"
                        rows="4" id="temporary_address"><?=$employee['temporary_address']?></textarea>
                </div>
            </div>
            <hr />
            <div class="box-btn text-center">
                <a href="javascript:void(0)" onclick="closeForm()" class="btn btn-secondary">Bỏ qua</a>
                <button class="btn btn-blue">Lưu lại</button>
            </div>
        </div>
    </form>
</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
function closeForm() {
    $('#loadFormPersonalInfo').html('');
}
$(document).ready(function() {
    $('[data-toggle="input-mask"]').each(function(idx, obj) {
        var maskFormat = $(obj).data("maskFormat");
        var reverse = $(obj).data("reverse");
        if (reverse != null)
            $(obj).mask(maskFormat, {
                'reverse': reverse
            });
        else
            $(obj).mask(maskFormat);
    });
});
</script>