
<?php if($datas != NULL){  ?>
        <?php foreach($datas as $key => $val){
            $avatar = 'upload/employee/' . $val['code'] . '/' . $val['avatar']; 
            ?>
            <div class="chat_list chat_list_find <?=$val['active_chat'] == 1 ? "active_chat" : "" ?>" data-employeeid="<?=$val['employeeID']?>" data-userid="<?=$val['userID']?>">
                <div class="chat_people">
                    <div class="chat_img">
                    <?php if (file_exists($avatar) && $val['avatar'] != '') { ?>
                        <img src="<?= $avatar; ?>" class="rounded-circle mr-1" alt="avatar" width="40" height="40">
                    <?php } else { ?>
                        <img src="public/images/avatar-default.jpg" class="rounded-circle mr-1" alt="avatar" width="30">
                    <?php } ?>
                    </div>
                    <div class="chat_ib">
                        <h5><?=$val['fullname']?></h5>
                        <p><?=$val['email']?></p>
                        <div class="status_user bg__status_user_<?=$val['online']?>"></div>
                    </div>
                </div>
            </div>       
        <?php } ?>
 <?php } ?>
