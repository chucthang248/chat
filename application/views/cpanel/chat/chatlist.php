<div class="chat_list active_chat <?=$employee['view'] == 1 ? "not_view" : "" ?>" data-employeeid="<?=$employee['id'] ?>" data-userid="<?=$employee['userID']  ?>">
    <div class="chat_people">
        <div class="chat_img">
            <?php
              $avatar = 'upload/employee/' . $employee['code'] . '/' . $employee['avatar']; 
            if (file_exists($avatar) &&  $employee['avatar'] != '') { ?>
                <img src="<?= $avatar; ?>" class="rounded-circle mr-1" alt="avatar" width="40" height="40">
            <?php } else { ?>
                <img src="public/images/avatar-default.jpg" class="rounded-circle mr-1" alt="avatar" width="30">
            <?php } ?>
        </div>
        <div class="chat_ib">
            <h5><?= $employee['fullname'] ?></h5>
            <p><?=  $employee['email'] ?></p>
        </div>
    </div>
</div>