<?php if ($datas != NULL) {
    $user_login_id = $this->Auth->logged_id();   ?>
    <?php foreach ($datas as $key => $val) {
        $avatar = 'upload/employee/' . $val['code'] . '/' . $val['avatar'];
    ?>
        <div id="item<?= $val['userID'] ?>" class="chat_list chat_list_main <?= $val['view'] == 1 && $val['user_send_id'] != $user_login_id ? "not_view" : "" ?>" data-employeeid="<?= $val['employeeID'] ?>" data-userid="<?= $val['userID'] ?>">
            <div class="chat_people">
                <div class="chat_img">
                    <?php if (file_exists($avatar) && $val['avatar'] != '' && $val['fullname'] != "admin" ) { ?>
                        <img src="<?= $avatar; ?>" class="rounded-circle mr-1" alt="avatar" width="40" height="40">
                    <?php } else { ?>
                        <img src="public/images/avatar-default.jpg" class="rounded-circle mr-1" alt="avatar" width="30">
                    <?php } ?>
                </div>
                <div class="chat_ib test">
                    <h5><?= $val['fullname'] ?></h5>
                    <p><?= $val['email'] ?></p>
                    <div class="status_user bg__status_user_<?= $val['online'] ?>"></div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php } ?>