<?php $Files = ["application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"];  ?>
<div class="info_user d-flex" id="sendto" data-user_receive_id="<?= $User['id'] ?>">
    <?php if ($User != NULL) {
        $avatar_data_listchat = 'upload/employee/' . $User['code'] . '/' . $User['avatar'];
        $avatar_check_User = file_exists($avatar_data_listchat) && $User['avatar'] != '' ?  $avatar_data_listchat : "public/images/avatar-default.jpg";
    ?>
        <div class="box__info">
            <img class="rounded-circle" src="<?= $avatar_check_User ?>" alt="sunil">
        </div>
        <h4 class="name_user"><?= $User['fullname'] ?></h4>
    <?php } ?>
</div>
<!------------- tin nhắn ------------->
<div class="mesgs" id="onlyMess">
    <div id="msg_history" class="msg_history">
        <?php if ($Messenger != NULL) {
            $login = $this->Auth->logged_id(); ?>
            <?php foreach ($Messenger as $key_mess => $val_mess) {
                $avatar = 'upload/employee/' . $val_mess['code'] . '/' . $val_mess['avatar'];
                $avatar_check = file_exists($avatar) && $val_mess['avatar'] != '' ?  $avatar : "public/images/avatar-default.jpg";
            ?>
                <?php if ($val_mess['user_send_id'] == $login) { // tin nhắn bên phải của người A ( người hiện tại )
                ?>
                    <?php if ($val_mess['files_images'] == 0) {   // nếu nội dung không file/hình ?>
                        <div class="outgoing_msg mess_login">
                            <div class="sent_msg">
                                <p><?= $val_mess['content'] ?></p>
                                <span class="time_date"><?= $val_mess['created_at'] ?></span>
                            </div>
                        </div>
                    <?php  } else {
                        $data_imges = json_decode($val_mess['content']);
                        $data_imges = json_decode(json_encode($data_imges), true); ?>
                        <div class="outgoing_msg mess_login">
                            <div class="sent_msg">
                                <div class="box__img_mess_userlogin box__mess_same_css">
                                    <div class="row">
                                        <?php foreach ($data_imges as $key => $val) { ?>
                                            <div class="col-md-4 same_col">
                                                <?php if (in_array($val['type'], $Files)) {
                                                    $img_file = "public/assets/images/";
                                                    if ($val['type'] == "application/pdf") {
                                                        $img_file .= "pdf.png";
                                                    }
                                                    if ($val['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                                                        $img_file .= "docx.png";
                                                    }
                                                    if ($val['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                                                        $img_file .= "excel.png";
                                                    }
                                                ?>
                                                    <div class="itemFiles">
                                                        <a href="<?= $val['fullpath'] ?>" download="<?= $val['fullpath'] ?>"> <img src="<?= $img_file ?>" alt=""></a>
                                                    </div>
                                                <?php  } else { ?>
                                                    <img src="<?= $val['fullpath'] ?>" alt="">
                                                <?php  }  ?>
                                            </div>
                                        <?php  }  ?>
                                    </div>
                                </div>

                                <span class="time_date"><?= $val_mess['created_at'] ?></span>
                            </div>
                        </div>

                    <?php  }  ?>
                <?php  } else { // tinn nhắn bên trái của người B  
                ?>
                    <?php if ($val_mess['files_images'] == 0) {  // nếu nội dung là hình 
                    ?>
                        <div class="incoming_msg">
                            <div class="incoming_msg_img"> <img class="rounded-circle" src="<?= $avatar_check ?>" alt="sunil"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg mess_sendto">
                                    <p><?= $val_mess['content'] ?></p>
                                    <span class="time_date"><?= $val_mess['created_at'] ?></span>
                                </div>
                            </div>
                        </div>
                    <?php  } else {
                        $data_imges = json_decode($val_mess['content']);
                        $data_imges = json_decode(json_encode($data_imges), true); ?>

                        <div div class="incoming_msg">
                            <div class="incoming_msg_img"> <img class="rounded-circle" src="<?= $avatar_check ?>" alt="sunil"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg mess_sendto">
                                    <div class="box__img_mess_send_to_me box__mess_same_css">
                                        <div class="row">
                                            <?php foreach ($data_imges as $key => $val) { ?>
                                                <div class="col-md-4 same_col">
                                                    <?php if (in_array($val['type'], $Files)) {
                                                        $img_file = "public/assets/images/";
                                                        if ($val['type'] == "application/pdf") {
                                                            $img_file .= "docx.png";
                                                        }
                                                        if ($val['type'] == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                                                            $img_file .= "pdf.png";
                                                        }
                                                        if ($val['type'] == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                                                            $img_file .= "excel.png";
                                                        }
                                                    ?>
                                                        <div class="itemFiles">
                                                            <a href="<?= $val['fullpath'] ?>" download="<?= $val['fullpath'] ?>"> <img src="<?= $img_file ?>" alt=""></a>
                                                        </div>
                                                    <?php  } else { ?>
                                                        <img src="<?= $val['fullpath'] ?>" alt="">
                                                    <?php  }  ?>
                                                </div>
                                            <?php  }  ?>
                                        </div>
                                    </div>
                                    <span class="time_date"><?= $val_mess['created_at'] ?></span>
                                </div>
                            </div>
                        </div>
                    <?php  }  ?>
                <?php } ?>
            <?php } ?>
        <?php  } ?>
    </div>
    <div class="type_msg">
        <div class="input_msg_write">
            <div class="box__messenger" id="editable" placeholder="Nhập nội dung..." contenteditable="true"></div>
            <div class="box__imgs">
                <ul>
                </ul>
            </div>
            <div class="fullicon">
                <?php $this->load->view('cpanel/chat/icons'); ?>
            </div>
            <input type="file" multiple id="inputFile">
            <div class="fileUpload"> <i class="fas fa-file-upload"></i></div>
            <div class="icons_html">&#128512;</div>
            <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
        </div>
    </div>
</div>

