<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet">
<link rel="stylesheet" href="public/assets/css/chat.css">
<link rel="stylesheet" href="public/assets/css/emoji.css">
<div class="container-cs">
    <div class="messaging">
        <div class="inbox_msg">
            <div class="inbox_people">
                <div class="headind_srch">
                   <div class="action_box d-flex align-items-center">
                    <i class="fas fa-users"></i>
                    <i class="fas fa-plus ml-2" onclick="searchEmployee('open')" ></i>
                   </div>
                   <div class="find_add_employee">
                        <input type="text" class="search-bar" onkeyup="searchAutocomplete(this)" placeholder="Tìm kiếm nhân viên">
                        <i class="fa fa-window-close" onclick="searchEmployee('close')"></i>
                    </div>
                    <div class="srch_bar">
                        <div class="stylish-input-group">
                            <input type="text" class="search-bar" onkeyup="searchAutocomplete(this)" placeholder="Tìm kiếm trò chuyện">
                            <span class="input-group-addon">
                                <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="inbox_chat same_scroll">
                  
                </div>
                <div class="find_user same_scroll"></div>
            </div>
            <div class="mesgs_t" id="loadMess" >
                     <!-- content chat  -->
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>




<script>
    let arrFiles = [];
    let FilesMime = ["application/pdf", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/vnd.openxmlformats-officedocument.wordprocessingml.document"]; 
    // xoá con trỏ nhấp nháy css
    $(document).on("click", "#editable", function() {
        $(".blinking-cursor").html("");
    });
    //=================================
    $(document).on("click", "#ulIcons li", function() {
        $(".blinking-cursor").html(""); // xoá con trỏ nhấp nháy css
        let hexcode_icon = $(this).attr("data-hexcode");
        let txtHex = `&#x${hexcode_icon};`;
        let span_icon = `<span  class='sp_icon' >${txtHex}</span>`;
        $("#editable").append(span_icon);
    });
    $(document).on("click", ".fileUpload", function() {
        $("#inputFile").trigger("click");
        $("#editable").focus();
    });
    $(document).on("change","#inputFile",function (e) { 
        debugger;
        readURLIDFRONT(this);
     });
    // nhận lại file vừa gửi 
    socket.on("server_send_fileupload",function(res){
        debugger;
        let files = JSON.parse(res.data[0].content);
        let html = `<div class="outgoing_msg mess_login">
                        <div class="sent_msg">
                            <div class="box__img_mess_userlogin box__mess_same_css">
                                <div class="row">`;
                files.map((obj,key)=>{
                    let check =  FilesMime.includes(obj.type);
                    debugger;
                    
                            html += `<div class="col-md-4 same_col">`;
                    if (FilesMime.includes(obj.type) == true) 
                    { //nếu là file
                        debugger;
                        let img_file = "public/assets/images/";
                        if (obj.type == "application/pdf") {
                            img_file += "pdf.png";
                            debugger;
                        }
                        if (obj.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                            img_file += "docx.png";
                        }
                        if (obj.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                           img_file += "excel.png";

                        }
                        html += ` 
                                    <div class="itemFiles">
                                        <a href="${obj.fullpath}" download="${obj.fullpath}"> <img src="${img_file}" alt=""></a>
                                    </div>
                                `;
                                debugger;
                    }else // nếu là hình
                    {
                                 html +=` <img src="${obj.fullpath}" alt="">`;
                    }
                             html += `</div>`;
                    debugger;
                });
            html += `           </div>
                            </div>
                            <span class="time_date">${res.data[0].created_at}</span>
                        </div>
                    </div>`;
        // thêm vào đoạn chat
        $("#msg_history").append(html);
        objDiv = document.getElementById("msg_history");
        objDiv.scrollTop = objDiv.scrollHeight;
    });
    // người khác gửi file/hình tới 
    socket.on("server_send_fileupload_to_me",function (res) { 
        let srcImgs = "public/images/avatar-default.jpg";
        if(res.messenger.avatar != null)
        {
            srcImgs = `upload/employee/${res.messenger.code}/${res.messenger.avatar}`;
        }
        let files = JSON.parse(res.messenger.content);
        let html = `<div class="incoming_msg">
                        <div class="incoming_msg_img"> <img class="rounded-circle" src="${srcImgs}" alt="sunil"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg mess_sendto">
                                    <div class="box__img_mess_send_to_me box__mess_same_css">
                                        <div class="row">`;
                                        files.map((obj,key)=>{
                                    html += `<div class="col-md-4 same_col">`;
                                            if (FilesMime.includes(obj.type) == true) 
                                            { //nếu là file
                                                debugger;
                                                let img_file = "public/assets/images/";
                                                if (obj.type == "application/pdf") {
                                                    img_file += "pdf.png";
                                                    debugger;
                                                }
                                                if (obj.type == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet") {
                                                    img_file += "docx.png";
                                                }
                                                if (obj.type == "application/vnd.openxmlformats-officedocument.wordprocessingml.document") {
                                                img_file += "excel.png";

                                                }
                                                html += ` 
                                                            <div class="itemFiles">
                                                                <a href="${obj.fullpath}" download="${obj.fullpath}"> <img src="${img_file}" alt=""></a>
                                                            </div>
                                                        `;
                                                        debugger;
                                            }else // nếu là hình
                                            {
                                        html +=` <img src="${obj.fullpath}" alt="">`;
                                            }
                                     html += `</div>`;
                                            debugger;
                                        });
            html += `                   </div>
                                    </div>
                                <span class="time_date">${res.messenger.created_at}</span>
                            </div>
                        </div>
                    </div>`;
        // thêm vào đoạn chat
        active__chat_global  = $("#sendto").attr("data-user_receive_id");
        active__chat_global  = parseInt(active__chat_global);
        debugger;
        if(res.messenger.user_send_id == active__chat_global)
        {
            $("#msg_history").append(html);
            objDiv = document.getElementById("msg_history");
            objDiv.scrollTop = objDiv.scrollHeight;
        }
       
    });
    // upload + insert vào database
    function uploadInsert(data_insert) {
        let postData = new FormData();
        arrFiles.forEach(function(f) {
            postData.append("multiple", f);
        });
        postData.append("user_send_id", data_insert.userid_login);
        postData.append("user_receive_id", data_insert.user_receive_id);
        postData.append("content", "");
        postData.append("view", data_insert.view);
        postData.append("created_at", data_insert.created_at);
        $.ajax({
            type: "POST",
            url: "http://localhost:3005/uploadfile",
            data: postData,
            processData: false,
            contentType: false,
            success: function(res) {
            }
        });
    }

    function readURLIDFRONT(input) {
        debugger;
        let countFile = input.files.length;
        if (countFile > 0) {
            $(input.files).each((key, obj) => {
                let item = obj;
                let Images = ["jpg", "jpeg", "png"];
                let Files = ["pdf", "docx", "xlxs"];
                let formData = new FormData();
                formData.append("files", item);
                $.ajax({
                    type: "POST",
                    url: "http://localhost:3005/validatefile",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(res) {
                        console.log(res);
                        let reader = new FileReader();
                        reader.onload = function(e) {
                            // files
                            let icons_file = "public/assets/images/";
                            if (res.mime == "" && res.size == "") {
                                // nạp dữ liệu upload 
                                arrFiles.push(item);
                                debugger;
                                //== file docs, pdf .v..v.===
                                if (Files.includes(res.mime_type)) {
                                    if (res.mime_type == "docx") {
                                        icons_file = icons_file + "docx.png";
                                    }
                                    if (res.mime_type == "pdf") {
                                        icons_file = icons_file + "pdf.png";
                                    }
                                    if (res.mime_type == "xlxs") {
                                        icons_file = icons_file + "excel.png";
                                    }
                                    $(".box__imgs ul").append(` <li>
                                                                    <div class="frame">
                                                                        <img src="${icons_file}" alt="">
                                                                        <i class="fas fa-times-circle remove_img"></i>
                                                                    </div>
                                                                </li>`);
                                }
                                //========== hình============
                                if (Images.includes(res.mime_type)) {
                                    $(".box__imgs ul").append(` <li>
                                                                        <div class="frame">
                                                                            <img src="${e.target.result}" alt="">
                                                                            <i class="fas fa-times-circle remove_img"></i>
                                                                        </div>
                                                                    </li>`);
                                }

                            }

                            // lỗi
                            if (res.mime != "" || res.size != "") {
                                $(".box__imgs ul").append(` <li class="append_error" >
                                                                    <div class="frame">
                                                                       <p> ${res.mime} </p>
                                                                       <p> ${res.size} </p>
                                                                       <i class="fas fa-times-circle remove_img"></i>
                                                                    </div>
                                                                </li>`);
                            }

                        }
                        reader.readAsDataURL(item);

                    }
                });

            });
        }

    }
</script>

<script type="text/javascript">

    // userid_login : được khởi tạo 1 lần ở cpanel> view> default > index.php
    const urlSearch         = "cpanel/chat/search" ;
    let objDiv              = "";                                   
    let detectEmployee      = "<?=$data_listchat[0]->userID?>";
    let active__chat_global = "";
    function searchAutocomplete(_this) {
        let url             = urlSearch;
        let searchValue     = $(_this).val();
        if(searchValue == "")
        {
            $(".inbox_chat").show();
            $(".find_user").hide();
        }else
        {
            let data = {
                "key_word": searchValue,
                "type_pageload": "ajax",
            }           
            CallAjax(data, url); 
        }
     
    }
    // PAGINATION
    $(document).on('click', '.pagination .forClick a', function() {
        let checkKeyword     = $("#search").val();
        $("#search").val(checkKeyword);
        let url              = urlSearch;
        let gethref          = $(this).attr('href');
        let page             = $(this).attr('data-ci-pagination-page');
        let data             = {
                        "key_word": checkKeyword,
                        "page": page,
                        "row": row,
                        "type_pageload": "ajax"
                    }
        CallAjax(data, url);
        event.preventDefault();
    });

    function CallAjax(data, url) {
        $.ajax({
            url : url,
            type: 'POST',
            data: data,
            success: function(res) {
                $(".find_user").html(res);
                $(".find_user").show();
                // ẩn danh sách chat
                $(".inbox_chat").hide();
            }
        });
    }
    //================================CHAT=============================================
    // nhấn enter
    function enterKey(e,_this)
    {
        if (e.keyCode == 13)
        {
            sendMessenger();
        }
    }
    function searchEmployee(type)
    {
        if(type == "open")
        {
            $(".find_add_employee").fadeIn();
        }else
        {
            $(".find_add_employee").hide();
            $(".inbox_chat").show();
            $(".find_user").hide();
        }
    }
    // onload 
    $(document).ready(function(){
        // lấy danh sách chat                    : XONG
        socket.emit("get_listchat",{userid_login:userid_login,employeeID:employeeID});
        // nhận danh sách đã chat                : XONG
        socket.on("server_send_listchat",function(data){
            if(data.type == "success")
            {
                //debugger;
                    $(".inbox_chat").html('');
                    $.ajax({
                        type: "POST",
                        url: "cpanel/chat/loadChatlist",
                        data: {lischat: data.list_chat},
                        success: function (res) {
                            $(".inbox_chat").html(res);
                              // active chat
                            active__chat_global  = $("#sendto").attr("data-user_receive_id");
                            let currentItem      = $("#item"+active__chat_global).hasClass("not_view");
                            if(active__chat_global != "")
                            {
                                $(".chat_list").removeClass("active_chat");
                                $("#item"+active__chat_global).addClass("active_chat");
                                $("#item"+active__chat_global).removeClass("not_view");
                            }
                                
                        }
                    });                
            }
 
        });
        // tin đầu tiên trong danh sách tin nhắn : XONG
        socket.on("server_send_first_messenger",function(data){
            $("#loadMess").html("");
            $.ajax({
                type: "POST",
                url: "cpanel/chat/loadMessenger",
                data: {
                        mess: data.mess.list_messenger,
                        user: data.user
                    },
                success: function (res) {
                    $("#loadMess").html("");
                    $("#loadMess").html(res);
                    objDiv               = document.getElementById("msg_history");
                    objDiv.scrollTop     = objDiv.scrollHeight;

                    // active chat
                    active__chat_global  = $("#sendto").attr("data-user_receive_id");
                    let currentItem      = $("#item"+active__chat_global).hasClass("not_view");
                    if(active__chat_global != "")
                    {
                        $(".chat_list").removeClass("active_chat");
                        $("#item"+active__chat_global).addClass("active_chat");
                        $("#item"+active__chat_global).removeClass("not_view");
                        // update view 
                        let update_view  = {
                            userid_login   :userid_login,
                            employeeID     :data.user.employeeID,
                            user_receive_id:data.user.id ,
                            view           :  0 // đã xem
                        };
                        socket.emit("update_view",update_view);
                    }
                }
            });
              // set thanh cuộn nằm dưới cùng
     
        });
      
    });
   
    // Chọn tin nhắn muốn chat
    $(document).on("click",".chat_list_main",function(){
        $(".chat_list").removeClass("active_chat");
        $(this).removeClass("not_view");
        $(this).addClass("active_chat");
        let employeeID       = $(this).attr("data-employeeid");
        let user_receive_id  = $(this).attr("data-userid");
        active__chat_global  = user_receive_id;
        let active_chat      = "active_chat";
        detectEmployee       = user_receive_id;
        debugger;
         // lấy tin nhắn của người vừa click chọn
         socket.emit("choice_user_in_list_chat",{
                                              userid_login   :userid_login,
                                              user_receive_id: user_receive_id  
                                            });
     
        // update trạng thái đã xem trong database
        let update_view       = {
                                    userid_login     :userid_login,
                                    employeeID       :employeeID,
                                    user_receive_id  :user_receive_id ,
                                    view             :  0 // đã xem
                                };
        socket.emit("update_view",update_view);
    });
    // Tìm và chọn nhân viên để chat
    $(document).on("click",".chat_list_find",function(){
        //alert("ok");
        $(".chat_list").removeClass("active_chat");
        $(this).addClass("active_chat");
        let employeeID       = $(this).attr("data-employeeid");
        let user_receive_id  = $(this).attr("data-userid");
        let active_chat      = "active_chat";
        detectEmployee       = user_receive_id;
        // lấy tin nhắn của người vừa click chọn
        $.ajax({
            type: "POST",
            url: "cpanel/chat/getChatList",
            data: {employeeID:employeeID,user_receive_id:user_receive_id },
            success: function (res) {
                //console.log(JSON.parse(res));
                    $("#loadMess").html(res);
                    $(".inbox_chat").show();
                    $(".find_user").hide();
                    // set scrollbar xuống cuối 
                    objDiv           = document.getElementById("msg_history");
                    objDiv.scrollTop = objDiv.scrollHeight;
            }
        });
        socket.emit("choice_user_chat",{
                            userid_login    :userid_login,  
                            employeeID      :employeeID,
                            user_receive_id :user_receive_id,
                            active_chat     :active_chat  
                    });

    });
    //============= Gửi tin nhắn
    $(document).on("click",".msg_send_btn",function(){
        sendMessenger();
    });
    function sendMessenger() // bắt đầu gửi tin 
    {
     
        $(".fullicon").removeClass("showIcons"); // ẩn khung icon
        $(".box__imgs ul").html("") // ẩn khung chọn hình 
        $(".blinking-cursor").html("") // xoá con trỏ nhấp nháy
        let txtMessenger =  $(".box__messenger").text();
        debugger;
        // set rỗng sau khi gửi tin
        $(".box__messenger").text("");
         let user_receive_id   = $("#sendto").attr("data-user_receive_id");
         let created_at        = moment( new Date()).format('YYYY-MM-DD H:m:s');
         let created_at_append = moment(new Date()).format('DD/MM/YY, H:m:s');
             txtMessenger      = txtMessenger.replace(/(<([^>]+)>)/, "");
         let data_insert       = {
                                    userid_login   :userid_login,
                                    view           : 1,
                                    employeeID     :employeeID,
                                    user_receive_id:user_receive_id, 
                                    txtMessenger   : txtMessenger,
                                    created_at     : created_at
                                };
                               
        if(txtMessenger != "")
        {
                socket.emit("send_messenger",data_insert);
                // thêm tin nhắn vào giao diện
                $("#msg_history").append(`
                        <div class="outgoing_msg">
                            <div class="sent_msg">
                                <p>${txtMessenger}</p>
                                <span class="time_date">${created_at_append}</span>
                            </div>
                        </div>
                    `);
                // set scrollbar xuống cuối 
                objDiv = document.getElementById("msg_history");
                objDiv.scrollTop = objDiv.scrollHeight;
        }
        let checkarr = arrFiles;
        debugger;
        // nếu có hình đã chọn
        if(arrFiles.length > 0)
        {
             debugger;
             //upload hình + insert hình 
            uploadInsert(data_insert);
          
            // set lại [] 
            arrFiles = [];
        }
    }
    //===============nhận tin nhắn chat từ người khác ngời tới
    socket.on("server_send_messenger", function (data) {
        // console.log(data.messenger.user_send_id);
        let avatar               = 'upload/employee/'+data.messenger.code + '/'+data.messenger.avatar; 
        let avatar_check         =  data.messenger.avatar != '' ? avatar : "public/images/avatar-default.jpg";
            active__chat_global  = $("#sendto").attr("data-user_receive_id");
            active__chat_global  = parseInt(active__chat_global);
        if(data.messenger.user_send_id == active__chat_global)
        {
            if(data.messenger.user_send_id == 1 || data.messenger.avatar == null)
            {
                avatar_check     =  "public/images/avatar-default.jpg";
            }
            $("#msg_history").append(`
                <div class="incoming_msg">
                    <div class="incoming_msg_img"> <img class="rounded-circle" src="${avatar_check}" alt="sunil"> </div>
                    <div class="received_msg">
                        <div class="received_withd_msg">
                            <p>${data.messenger.content}</p>
                            <span class="time_date">${data.messenger.created_at}</span>
                        </div>
                    </div>
                </div>    
            `);
              // set scrollbar xuống cuối 
        objDiv = document.getElementById("msg_history");
        objDiv.scrollTop = objDiv.scrollHeight;
        }
    });

    //=============== online - offline ===================
    socket.on("server_send_status", function (data) {
        $("#item"+data.id).find(".status_user").removeClass("bg__status_user_1");
        $("#item"+data.id).find(".status_user").removeClass("bg__status_user_0");
        let statusClass    = "bg__status_user_"+data.online;
        $("#item"+data.id).find(".status_user").addClass(statusClass);
      
    });
    //=============== update list chat khi có người gửi tin nhắn đến ============
    socket.on("server_send_listchat_update", function (data) {
        if(data.list_chat.length > 0)
        {
            data.list_chat.forEach((value,key)=> {
                let ItemLi        = $("#item"+value.id);
                let existItem     = $(ItemLi).attr("data-userid");
                let defaultAvatar = `upload/employee/${value.code}/${value.avatar}`;
                    defaultAvatar = value.id != 1 ? defaultAvatar : "public/images/avatar-default.jpg";
                if(existItem != undefined) // user đang tồn tại trong danh sách
                {
                    // nếu mess đang xem => 
                              // view = 0 
                                    //=> active_chat
                                        //=> xoá not_view 
                                                // => update view
                    // các item còn lại :
                             //  check view : == 0 ? không có not view : addClass not_view
                    //đang active ,đang xem
                    //nếu là tin nhắn mới  (view = 1 : chưa xem) thì xoá item cũ, append item mới

                    active__chat_global  = $("#sendto").attr("data-user_receive_id");
                    active__chat_global  = parseInt(active__chat_global);
                    if(value.id == active__chat_global )
                    {
                        // add class active
                        $(".chat_list").removeClass("active_chat");
                        $("#item"+value.id).addClass("active_chat");
                        $("#item"+value.id).removeClass("not_view");
                        // update view 
                        let update_view = {
                            userid_login   : userid_login,
                            employeeID     : value.employeeID,
                            user_receive_id: value.id ,
                            view :  0 // đã xem
                        };
                        socket.emit("update_view",update_view);
                    }else
                    {
                        if(value.view == 1)  // nếu là tin nhắn mới (chưa được xem)
                        {
                            $("#item"+value.id).remove();
                            // $("#item"+value.id).addClass("not_view");
                            $(".inbox_chat ").prepend(`
                                <div id="item${value.id}" class="chat_list chat_list_main not_view" data-employeeid="${value.employeeID}" data-userid="${value.id}">
                                    <div class="chat_people">
                                        <div class="chat_img">
                                                <img src="${defaultAvatar}" class="rounded-circle mr-1" alt="avatar" width="40" height="40">
                                        </div>
                                        <div class="chat_ib">
                                            <h5>${value.fullname}</h5>
                                            <p>${value.email}</p>
                                            <div class="status_user bg__status_user_${value.view}"></div>
                                        </div>
                                    </div>
                                </div>
                            `);
                        }
                    }
                }else // user kh tồn tại trong danh sách (người mới gửi tin nhắn đầu tiên )
                {
                    $(".inbox_chat ").prepend(`
                        <div id="item${value.id}" class="chat_list chat_list_main not_view" data-employeeid="${value.employeeID}" data-userid="${value.id}">
                            <div class="chat_people">
                                <div class="chat_img">
                                        <img src="${defaultAvatar}" class="rounded-circle mr-1" alt="avatar" width="40" height="40">
                                </div>
                                <div class="chat_ib">
                                    <h5>${value.fullname}</h5>
                                    <p>${value.email}</p>
                                    <div class="status_user bg__status_user_${value.view}"></div>
                                </div>
                            </div>
                        </div>
                    `);
                }
            })

        }
      
    });
    //================== Nhận lại hình vừa gửi =============
    socket.on("server_send_mess_files",function (data) {  
        debugger;
        console.log("server_send_mess_files");
        console.log(data);
    });
   //================= CLICK SHOW ICON =====================
   $(document).on("click",".icons_html",function(){
        $(".fullicon").toggleClass("showIcons");
   });

</script>