<div class="info_user d-flex" id="sendto" data-user_receive_id="<?= $data_listchat['userID'] ?>">
    <?php if ($data_listchat != NULL) {
        $avatar_data_listchat = 'upload/employee/' . $data_listchat['code'] . '/' . $data_listchat['avatar'];
        $avatar_check_data_listchat = file_exists($avatar_data_listchat) && $data_listchat['avatar'] != '' ?  $avatar_data_listchat : "public/images/avatar-default.jpg";
    ?>
        <div class="box__info">
            <img class="rounded-circle" src="<?= $avatar_check_data_listchat ?>" alt="sunil">
        </div>
        <h4 class="name_user"><?= $data_listchat['fullname'] ?></h4>
    <?php } ?>
</div>
<div class="mesgs">
    <div id="msg_history" class="msg_history">
        <?php if ($Messenger != NULL) {
            $login = $this->Auth->logged_id(); ?>
            <?php foreach ($Messenger as $key_mess => $val_mess) {
                $avatar = 'upload/employee/' . $val_mess->code . '/' . $val_mess->avatar;
                $avatar_check = file_exists($avatar) && $val_mess->avatar != '' ?  $avatar : "public/images/avatar-default.jpg";
            ?>
                <?php if ($val_mess->user_send_id == $login) { ?>
                    <div class="outgoing_msg">
                        <div class="sent_msg">
                            <p><?= $val_mess->content ?></p>
                            <span class="time_date"><?= $val_mess->created_at ?></span>
                        </div>
                    </div>
                <?php  } else { ?>
                    <div class="incoming_msg">
                        <div class="incoming_msg_img"> <img class="rounded-circle" src="<?= $avatar_check ?>" alt="sunil"> </div>
                        <div class="received_msg">
                            <div class="received_withd_msg">
                                <p><?= $val_mess->content ?></p>
                                <span class="time_date"><?= $val_mess->created_at ?></span>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>
        <?php  } ?>
    </div>
    <div class="type_msg">
        <div class="input_msg_write">
            <div class="box__messenger" id="editable" contenteditable="true"></div>
            <div class="fullicon">
                <?php $this->load->view('cpanel/chat/icons');?>    
            </div>
            <div class="icons_html" >&#128512;</div>
            <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
        </div>
    </div>
</div>
<script>
     $('#ulIcons li').click(function (e){
        let hexcode_icon =   $(this).attr("data-hexcode");
        $("#editable").append("<span  class='sp_icon' >&#"+hexcode_icon+";</span>");
    });
</script>