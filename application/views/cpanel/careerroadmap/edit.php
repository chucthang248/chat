<link rel="stylesheet" type="text/css" href="public/assets/customs/carrerroadmap.css">
<div class="container__form">
	<div class="body__form">
		<a href="javascript:void(0)" class="btnClose" onclick="closeForm()"><i class="icon-close"></i></a>
		<div class="box__form form-sm">
			<div class="box clearfix">
				<div class="title">Cập nhật dữ liệu</div>
				<hr class="hr-xs" />
				<form id="myForm" action="<?= CPANEL?>careerroadmap/edit/<?=$id ?>" method="POST" enctype="multipart/form-data">
					<div class="form">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="orgchartID" class="col-form-label">Phòng ban<span class="text-danger">*</span></label>
                                    <div class="box_input">
                                        <input type="text" require="" class="form-control" placeholder="Nhập tên phòng ban" name="data_post[name]" value="<?=$carreer_name['name']?>">
                                    </div>
									
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label for="inputName" class="col-form-label">Vị trí công việc<span class="text-danger">*</span></label>
									<div class="parent_col sidebar__filter">
										<?php //if ($val['child'] != NULL) :  ?>
											<div class="form-group position-relative">
												<label for="inputName" class="form-control drop_down_category">-- Vui lòng chọn --<i class="fas fa-chevron-down"></i></label>
												<div class="wrap_category">
                                                <?php foreach ($job_positon as $key => $val) { 
														$arrow = $val['child'] != "" ? "fas fa-chevron-down" : '';
														$margin = $val['count'] != 0 ? 27 * $val['count'] : 0;
														// $margin = $val['child'] != "" ? $margin : $margin + 14;
														$id_col = $val['parentID'] != 0 ?  $val['parentID'] :  $val['id'];
													?>
                                                    	<div class="item_job">
															<div class="wrap_parent same_parent">
																<div class="parent_border same_class"  >
																		<?= $val['name'] ?> 
																</div>
																<?php if($val['count'] != 0) { ?>
 																	<div class="wrap_checkbox checkbox checkbox-primary">
                                                                        <div class="checkbox checkbox-blue checkbox-single ">
                                                                         <?php $checked= ""; foreach($career_job as $key_job => $val_job){
                                                                             if($val_job['job_positionID'] == $val['id'])
                                                                             {
                                                                                $checked= "checked";
                                                                             }
                                                                           } 
                                                                           ?>
                                                                                <input <?=$checked ?>   value="<?=$val['id']?>" type="checkbox" id="checked<?php echo $val['id'];?>" name="data_checked[<?=$key?>][job_positionID]"  >
                                                                                <label></label>
                                                                            </div>
                                                                    </div>
																<?php  } ?>
															</div>
														</div>
                                                <?php } ?>
												</div>
											</div>
										<?php //endif; ?>
									</div>
								</div>
							</div>
						</div>
						<div class="box__btn">
							<button type="reset" id="btnReset" onclick="closeForm()">Bỏ qua</button>
							<button type="submit" class="btn__blue">Lưu lại</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="input-mask"]').each(function(idx, obj) {
			var maskFormat = $(obj).data("maskFormat");
			var reverse = $(obj).data("reverse");
			if (reverse != null)
				$(obj).mask(maskFormat, {
					'reverse': reverse
				});
			else
				$(obj).mask(maskFormat);
		});
	});
</script>