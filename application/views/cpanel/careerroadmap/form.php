<link rel="stylesheet" type="text/css" href="public/assets/customs/carrerroadmap.css">
<div class="container__form">
	<div class="body__form">
		<a href="javascript:void(0)" class="btnClose" onclick="closeForm()"><i class="icon-close"></i></a>
		<div class="box__form form-sm">
			<div class="box clearfix">
				<div class="title"><?= $title; ?></div>
				<hr class="hr-xs" />
				<form id="myForm" action="<?= CPANEL . $control ?>/form" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="rowID" name="data_post[rowID]" value="" />
					<div class="form">
						<div class="row">
							<div class="col-12">
								<div class="form-group">
									<label for="educationID" class="col-form-label">Phòng ban<span class="text-danger">*</span></label>
									<select id="educationID" name="data_post[educationID]" class="form-control" required="">
										<option value="">-- Vui lòng chọn --</option>
										<option value="1">Trung cấp</option>
										<option value="2">Trung cấp/Cao đẳng</option>
										<option value="3">Cao đẳng</option>
										<option value="4">Cao đẳng/Đại học</option>
										<option value="4">Đại học</option>
										<option value="6">Đại học/Thạc sĩ</option>
									</select>
								</div>
							</div>
							<div class="col-12">
								<div class="form-group">
									<label for="inputName" class="col-form-label">Vị trí công việc<span class="text-danger">*</span></label>
									<div class="parent_col sidebar__filter">
										<?php //if ($val['child'] != NULL) :  ?>
											<div class="form-group position-relative">
												<label for="inputName" class="form-control drop_down_category">-- Vui lòng chọn --<i class="fas fa-chevron-down"></i></label>
												<div class="wrap_category">
													<?php foreach ($val['child'] as $key_child => $val_child) : ?>
														<div class="">
															<div class="wrap_parent same_parent">
																<div class="parent_border same_class">
																	<a href="<?= $val_child['alias'] ?>">
																		<?= $val_child['name'] ?>
																	</a>
																</div>
																<div class="wrap_checkbox checkbox checkbox-primary">
																	<label for="" class="checkbox_container">
																		<input class="checked_category" id="checkbox<?= $val_child['id'] ?>" type="checkbox" name="cateID[]" value="<?= $val_child['id'] ?>">
																		<span class="checkmark"></span>
																	</label>
																</div>
															</div>
														</div>
													<?php endforeach; ?>
												</div>
											</div>
										<?php //endif; ?>
									</div>
								</div>
							</div>
						</div>
						<div class="box__btn">
							<button type="reset" id="btnReset" onclick="closeForm()">Bỏ qua</button>
							<button type="submit" class="btn__blue">Lưu lại</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('[data-toggle="input-mask"]').each(function(idx, obj) {
			var maskFormat = $(obj).data("maskFormat");
			var reverse = $(obj).data("reverse");
			if (reverse != null)
				$(obj).mask(maskFormat, {
					'reverse': reverse
				});
			else
				$(obj).mask(maskFormat);
		});
	});
</script>