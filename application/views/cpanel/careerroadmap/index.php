<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="javascript:void(0)" onclick="openForm()" class="btn btn-blue waves-effect waves-light">
                                <i class="icon-plus mr-1"></i> Thêm mới
                            </a>
                            <a href="<?= CPANEL . $control ?>" type="button" class="btn btn-linkedin waves-effect waves-light">
                                <i class="icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr />
                <table id="datatable" class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Tiêu đề</th>
                            <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if (isset($datas) && $datas != NULL) { ?>
                        <tbody>
                            <?php foreach ($datas as $key => $val) {
                                $stt = $key + 1;
                                $arrow = $val['child'] != "" ? "fas fa-chevron-down" : '';
                                $margin = $val['count'] != 0 ? 27 * $val['count'] : 0;
                               // $margin = $val['child'] != "" ? $margin : $margin + 14;
                                $id_col = $val['parentID'] != 0 ?  $val['parentID'] :  $val['id'];
                            ?>
                                <tr>
                                    <td class="text-center" width="80"><?= $stt ?></td>
                                    <td>
                                        <div class="subitem" style="padding-left:<?= $margin; ?>px">
                                                <?=$val['name']; ?>
                                        </div>    
                                    </td>
                                    <td class="text-center" width="200">
                                        <?php if($val['count'] == 0) { ?>
                                            <a href="javascript:void(0)" onclick="openFormEdit('1',<?= $val['id'] ?>)" class="btn btn-blue btn-xs waves-effect waves-light" data-id="<?php echo $val['id']; ?>">
                                                <i class="far fa-edit"></i>
                                            </a>
                                        <?php  } ?>
                                        <a href="javascript:void(0)" onclick="del(<?= $val['id'] ?>)" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val['id'] ?>" data-id="<?php echo $val['id']; ?>">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->

<div id="myForm">
    <?php // $this->load->view(CPANEL . $control . '/form', $arrdata); ?>
</div>
<script>
    $(document).on("click",".drop_down_category",function(){
        
        $(this).toggleClass("close_tr");
        let booleanClass = $(this).hasClass("close_tr");
        if (booleanClass == true) {
            $(".wrap_category").slideToggle();
        } else {
            $(".wrap_category").slideToggle();
        }
    });
    function openForm(typeRequest = 0, id = 0) {
        $('#btnReset').trigger("reset");
            $.ajax({
                url: "cpanel/careerroadmap/formAdd",
                type: "POST",
                data: {
                    id: id
                },
                success: function(data) {
                    $("#myForm").html(data)
                }
            });
    }
    function openFormEdit(typeRequest = 0, id = 0) {
        $('#btnReset').trigger("reset");
            $.ajax({
                url: "cpanel/careerroadmap/formEdit",
                type: "POST",
                data: {
                    id: id
                },
                success: function(data) {
                    $("#myForm").html(data)
                }
            });
    }

    function closeForm() {
      
        $("#myForm").html('');
        $('.container__form').fadeOut();
    }
</script>