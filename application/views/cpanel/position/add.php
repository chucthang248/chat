<div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Thêm mới dữ liệu</h4>
            </div>
            <div class="modal-body p-3">
                <form class="form-horizontal" action="<?=CPANEL.$control?>/add" method="POST" data-toggle="validator" novalidate="true">
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="username">Name</label>
                            <input class="form-control" type="text" name="data_post[name]" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="checkbox checkbox-secondary">
                                <input id="publish" type="checkbox" name="data_post[publish]" checked value="1">
                                <label for="publish">
                                    Hiển thị
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row account-btn text-center mb-0">
                        <div class="col-12">
                            <button class="btn btn-rounded btn-blue waves-effect waves-light" type="submit">Thêm mới</button>
                            <button class="btn btn-rounded btn-dark waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Hủy</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->