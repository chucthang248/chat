<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title">Duyệt lịch làm việc</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row" id="listemployee">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-12 d-flex t_justify_sb align-items-center">
                        <div class="left_box">
                        </div>
                        <div class="right_box">
                            <form action="">
                                <div class="d-flex">
                                    <div class="box__search d-flex">
                                        <input type="text" parsley-type="search" placeholder="Tìm kiếm nhân sự" name="data_filter[search]" class="form-control " id="search">
                                        <i class="fas fa-search ic_search_inside"></i>
                                    </div>
                                    <button type="button" onclick="btnSearch()" class="btn btn-blue btn_search_employee waves-effect waves-light ml-2"><i class="fas fa-search"></i> Tìm kiếm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="loadTable" class="mt-3">
                    <div class="table-scroll mb-2" id="table-scroll" style="border-right:none !important;">
                        <div class="table-wrap table-responsive">
                            <table class="table-hover " style="border-collapse: collapse; border-spacing: 0;border-right:none !important;">
                                <thead>
                                    <tr>
                                        <th>Mã</th>
                                        <th>Tên nhân viên</th>
                                        <th>Tác vụ</th>
                                    </tr>
                                </thead>
                                <?php if (isset($datas) && $datas != NULL) { ?>
                                    <tbody>
                                        <?php foreach ($datas as $key => $val) { ?>
                                            <tr>
                                                <td><?php echo  $val['code']; ?></td>
                                                <td><?php echo  $val['fullname']; ?></td>
                                                <td width="200" class="text-center">
                                                    <a href="javascript:void(0)" class="btn btn-blue btn-xs waves-effect waves-light view_timework" data-id="<?php echo $val['id']; ?>">
                                                        <i class="icon-eye "></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

                    <!-- pagination -->
                    <div class="wrap___flex d-flex t_justify_end ">
                        <div class="dataTables_paginate paging_simple_numbers d-block ml-auto mr-2" id="datatable_paginate">
                            <?php echo $my_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->

</div> <!-- end container-fluid -->

<!-- Xem nhanh thông tin nhân sự -->
<div class="wrap_info_timework" id="approvaltimework">
    <span class="time_overplay_2"></span>
    <div class="box__calendar">
        <input type="hidden" id="employeeID" value="">
        <div id="calendar"></div>
    </div>
</div>
<div id="loadEmloyeeInfoDetail"></div>
<!-- Xem nhanh thông tin nhân sự END -->
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script type="text/javascript">
  window.onresize = function(event) {
        location.reload();
    };
  // ===============full calendar===================
    $(document).on("click", ".time_overplay_2", function() {
        $(".wrap_info_timework").toggleClass("dedault_top");
    });
    let start = "";
    let getDMY = "";
    let getYear = "";
    let getMonth = "";
    let getDay = "";
    let uid = "";
    let employeeID = "";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },
            contentHeight: 'auto',
            eventResourceEditable: true,
            buttonText: {
                today: 'Hôm nay'
            },
            locale: 'vi',
            eventClick: function(info) {
                employeeID = $("#employeeID").val();
                let _def_data = info.event._def;
                uid = _def_data.publicId;
                Swal.fire({
                    title: '',
                    showConfirmButton: false,
                    html: `
                    <div class='box___btn'>
                            <button class='btn btn-blue btn_approval mr-2'>Duyệt</button>
                            <button class='btn btn-blue btn_destroy ml-2'>Hủy</button>
                    </div>
                `,
                });
            }
        });
        // button duyệt
        $(document).on("click",".btn_approval",function(){
                let className = "done_approval";
                let type = "admin";
                let approval = 1; // được duyệt
                $.ajax({
                    type: "POST",
                    url: "cpanel/Approvaltimework/approvalTimework",
                    data: { 
                        uid:uid,
                        employeeID:employeeID,
                        className: className,
                        type: type,
                        approval: approval
                    },
                    dataType: "JSON",
                    success: function (res) {
                        if (res.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                        }
                        else
                        {
                            let data_timework = res.listTimeWork;
                            calendar.removeAllEvents();
                            calendar.addEventSource(data_timework);
                            // đóng thông báo
                            swal.close();
                         
                        }
                        
                    }
                });
               
        });
         // button không duyệt
         $(document).on("click",".btn_destroy",function(){
                let className = "destroy_approval";
                let type = "admin";
                let approval = 0; // không được duyệt
                $.ajax({
                    type: "POST",
                    url: "cpanel/Approvaltimework/approvalTimework",
                    data: { 
                        uid:uid,
                        employeeID:employeeID,
                        className: className,
                        type: type,
                        approval: approval
                    },
                    dataType: "JSON",
                    success: function (res) {
                        if (res.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                        }
                        else
                        {
                            let data_timework = res.listTimeWork;
                            calendar.removeAllEvents();
                            calendar.addEventSource(data_timework);
                             // đóng thông báo
                           swal.close();
                        }
                       
                       
                    }
                });
               
        });
        // xem lịch làm việc
        $(document).on("click", ".view_timework", function() {
            let getID = $(this).attr("data-id");
            $.ajax({
                type: "POST",
                url: "cpanel/Approvaltimework/loadTimework",
                data: {
                    id: getID,
                },
                dataType: "JSON",
                success: function(res) {
                    $("#employeeID").val(getID);
                    let data_timework = res.listTimeWork;
                    calendar.removeAllEvents();
                    calendar.addEventSource(data_timework);
                    $(".wrap_info_timework").toggleClass("dedault_top");
                }
            });
        });
        calendar.render();
    });
 // =============end full calendar===================
    $(document).ready(function() {
        $("#search").val("<?= $key_word ?>");
    });
    let globalFileName = "";
    // seach 
    function btnSearch() {
        let url = "<?=base_url().'cpanel/approvaltimework/search' ?>";
        let data = {
            "key_word": $("#search").val(),
            "type_pageload": "ajax",
        }
        CallAjax(data, url);
    }
    // PAGINATION
    $(document).on('click', '.pagination .forClick a', function() {
        let checkKeyword = $("#search").val();
        $("#search").val(checkKeyword);
        let url = "<?= base_url() . 'cpanel/approvaltimework/pagination' ?>";
        // nếu input tìm kiếm có nội dung hoặc url có giá trị vừa tìm kiếm thì đổi URL
        if (checkKeyword != "") {
            url = "<?= base_url() . 'cpanel/approvaltimework/search' ?>";
        }
        let gethref = $(this).attr('href');
        let page = $(this).attr('data-ci-pagination-page');
        let data = {
            "key_word": checkKeyword,
            "page": page,
            "type_pageload": "ajax"
        }
        CallAjax(data, url);
        event.preventDefault();
    });

    function CallAjax(data, url) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res) {
                $("#loadTable").html(res);
            }
        });
    }
    // END PAGINATION
    $(document).ready(function() {
        $("#action_change").val(0);
    });
</script>