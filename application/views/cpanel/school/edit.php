<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Thêm mới dữ liệu</h4>
            </div>
            <div class="modal-body p-3">
                <form class="form-horizontal" action="<?=CPANEL.$control?>/edit" method="POST" data-toggle="validator" novalidate="true">
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="username">Mã</label>
                            <input class="form-control" type="text" id="code" name="data_post[code]" required="">
                        </div>
                        <div class="has-error"><?php echo form_error('code') ?></div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="username">Tên</label>
                            <input class="form-control" type="text" id="name"  name="data_post[name]" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="checkbox checkbox-secondary">
                                <input id="publish" type="checkbox" name="data_post[publish]" checked value="1">
                                <label for="publish">
                                    Hiển thị
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row account-btn text-center mb-0">
                        <div class="col-12">
                            <input type="hidden" id="idRow" name="idRow" value="">
                            <button class="btn btn-rounded btn-blue waves-effect waves-light" type="submit">Cập nhật</button>
                            <button class="btn btn-rounded btn-dark waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Hủy</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#edit-modal').on('show.bs.modal', function (event) {
            var id = $(event.relatedTarget).attr('data-id');
            if(id != '')
            {
                $.ajax
                ({
                    method: "POST",
                    url: "<?=CPANEL.$control?>/getDataRow",
                    data: { id:id},
                    dataType: "json",
                    success: function(data){
                        if(data.result){
                            $("#code").val(data.result.code);
                            $('#idRow').val(data.result.id);
                            $('#name').val(data.result.name);
                            if(data.result.publish == 1){
                                $('#publish-update').attr("checked", "checked");
                            }else{
                                $('#publish-update').removeAttr("checked", "checked");
                            }
                        }
                    }
                });
            }
            
        });
    });
</script>