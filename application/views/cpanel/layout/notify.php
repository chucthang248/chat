<?php if($data_index['permission_alert'] != 0) { ?><a class="nav-link dropdown-toggle  waves-effect waves-light " data-toggle="dropdown" href="javascript:void(0)" onclick="permissionAlert()" role="button" aria-haspopup="false" aria-expanded="false">
    <i class="dripicons-bell noti-icon "></i>
    <div class="box__count_appy count_apply"></div>
</a>
<div class="empty_apply" style="position: relative;z-index:1200;">
    <div class="dropdown-menu dropdown-menu-right dropdown-lg ">
        <div class="dropdown-header noti-title">
            <h5 class="text-overflow m-0"><span class="float-right">
                <span class="badge badge-danger float-right count_apply_box"></span>
                </span>Thông báo mới</h5>
        </div>
        <div class="slimscroll noti-scroll apply_item_alert" style="height: 150px !important;" >
        </div>
        <!-- All-->
        <?php
            $url = "http://work.hungthinh1989.com/recruitment/applylist";
            if($_SERVER["HTTP_HOST"] == 'localhost')
            {
                $url = base_url()."recruitment/applylist";
            }
        ?>
        <a href="<?=$url ?>" class="dropdown-item text-center text-primary notify-item notify-all">
            Xem tất cả
        </a>
    </div>
</div>

<script>
    function view(id)
    {
        $.ajax({
            type: "POST",
            url: "cpanel/ajax/viewApply",
            data: {id: id},
            success: function (response) {
                if(response != "")
                {
                    socket.emit("send_pageLoad_apply");
                }
            }
        });
    }

</script>
<?php } ?>