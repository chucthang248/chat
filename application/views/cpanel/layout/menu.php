<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="cpanel">
                        <i class="fas fa-chart-pie"></i>
                        <span> Tổng quan </span>
                    </a>
                </li>
                <li class="menu-title">Cơ sở dữ liệu</li>
                <li>
                    <a href="cpanel/employee">
                        <i class="icon-grid"></i>
                        <span> Danh sách nhân sự </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/contracts">
                        <i class="fe-check-circle"></i>
                        <span> Cam kết hợp đồng </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/careerroadmap">
                        <i class="icon-flag"></i>
                        <span> Lộ trình sự nghiệp </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-wallet"></i>
                        <span> Tiền lương </span>
                    </a>
                </li>
                <li class="menu-title">Cơ cấu tổ chức</li>
                <li>
                    <a href="cpanel/orgcharts">
                        <i class=" fas fa-network-wired"></i>
                        <span> Sơ đồ tổ chức </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/jobposition">
                        <i class="icon-briefcase"></i>
                        <span> Vị trí công việc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-list"></i>
                        <span> Nhiệm vụ công việc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/document">
                        <i class="icon-drawar"></i>
                        <span> Tài liệu </span>
                    </a>
                </li>
                <li class="menu-title">Nguyên tắc & chính sách</li>
                <li>
                    <a href="cpanel/workrule">
                        <i class="icon-book-open"></i>
                        <span> Quy định làm việc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-pin"></i>
                        <span> Chính sách nhân sự </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class=" icon-folder-alt"></i>
                        <span> Quy định cấp bậc </span>
                    </a>
                </li>
                <li class="menu-title">Lộ trình sự nghiệp</li>
                <li>
                    <a href="recruitment" target="_blank">
                        <i class="icon-people"></i>
                        <span> Tuyển dụng </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-rocket"></i>
                        <span> Kế hoạch thăng tiến </span>
                    </a>
                </li>
                <li>
                    <a href="javascript: void(0);">
                        <i class="icon-note"></i>
                        <span>Chính sách thăng tiến</span>
                        <span class="menu-arrow"></span>
                    </a>
                    <ul class="nav-second-level" aria-expanded="false">
                        <li><a href="cpanel/employee"><i class="fe-shuffle"></i> Thăng/hạ cấp</a></li>
                        <li><a href="cpanel/employee"><i class="icon-equalizer"></i> Tăng/giảm lương</a></li>
                        <li><a href="cpanel/employee"><i class="dripicons-swap"></i> Luân chuyển</a></li>
                    </ul>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-user-following"></i>
                        <span> Đánh giá thăng tiến </span>
                    </a>
                </li>
                <li class="menu-title">Thành tựu & cống hiến</li>
                <li>
                    <a href="cpanel/awardproposal">
                        <i class="icon-heart"></i>
                        <span>Đề xuất giải thưởng </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/maincertificate">
                        <i class="icon-heart"></i>
                        <span> Chứng chỉ </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/mainmilestones">
                        <i class="icon-heart"></i>
                        <span> Cột mốc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-chart"></i>
                        <span> Bảng xếp hạng </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/setting/achievement">
                        <i class="icon-settings"></i>
                        <span> Tùy chỉnh </span>
                    </a>
                </li>
                <li class="menu-title">Chính sách C&B</li>
                <li>
                    <a href="cpanel/timework">
                        <i class="icon-calender"></i>
                        <span> Lịch làm việc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/approvaltimework">
                        <i class="icon-calender"></i>
                        <span>Duyệt lịch làm việc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/overviewtimework">
                        <i class="icon-calender"></i>
                        <span>Tổng quan lịch làm việc </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-diamond"></i>
                        <span> Phúc lợi </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-clock"></i>
                        <span> Nghỉ phép </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-umbrella"></i>
                        <span> Bảo hiểm </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="icon-calculator"></i>
                        <span> Thuế </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/building">
                        <i class="far fa-calendar-check"></i>
                        <span> Ngày nghỉ lễ </span>
                    </a>
                </li>
                <li class="menu-title">Cấu hình hệ thống</li>
                <li>
                    <a href="cpanel/chat">
                        <i class="fab fa-facebook-messenger"></i>
                        <span> Tin nhắn </span>
                    </a>
                </li>
                <li>
                    <a href="cpanel/setting/dashboard">
                        <i class="icon-settings"></i>
                        <span> Cấu hình </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<script>
    let urlBuilding = "<?=$this->uri->segment(2)?>";
    $(document).ready(function(){
         $("#sidebar-menu ul li a").removeClass("active");
    });
</script>