<a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
    <img src="<?=$data_index['info_admin']['data']['avatar']?>" alt="user-image" class="rounded-circle">
    <span class="pro-user-name ml-1">
        <?=$data_index['info_admin']['data']['fullname']?>  <i class="mdi mdi-chevron-down"></i> 
    </span>
</a>
<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
    <!-- item-->
    <div class="dropdown-header noti-title">
        <h6 class="text-overflow m-0">Xin chào!</h6>
    </div>

    <!-- item-->
    <a href="cpanel/auths/profile" class="dropdown-item notify-item">
        <i class="fe-user"></i>
        <span>Tài khoản</span>
    </a>

    <!-- item-->
    <a href="javascript:void(0);" class="dropdown-item notify-item">
        <i class="fe-settings"></i>
        <span>Cài đặt</span>
    </a>

    <!-- item-->
    <a href="<?=CPANEL?>auths/changepass" class="dropdown-item notify-item">
        <i class="fe-lock"></i>
        <span>Đổi mật khẩu</span>
    </a>

    <div class="dropdown-divider"></div>

    <!-- item-->
    <a href="<?=CPANEL?>/auths/logout" class="dropdown-item notify-item">
        <i class="fe-log-out"></i>
        <span>Đăng xuất</span>
    </a>
</div>