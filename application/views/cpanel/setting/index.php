<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <!-- notify --> 
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End --> 

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a href="#company" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                            <span class="d-none d-sm-block">Thông tin công ty</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#profile" data-toggle="tab" aria-expanded="true" class="nav-link">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Profile</span>
                        </a>
                    </li>
                </ul>
                <div class="tab-content mt-3">
                    <div class="tab-pane show active" id="company">
                        <form class="form-horizontal" action="" method="POST" data-toggle="validator" novalidate="true">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="name">Tên công ty</label>
                                        <input class="form-control" type="text" name="data_post[company]" required="" value="<?php echo $dataRow['company'];?>">
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="phone">Hotline</label>
                                                <input class="form-control" type="text" name="data_post[hotline]" required="" value="<?php echo $dataRow['hotline'];?>">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input class="form-control" type="text" name="data_post[email]" required="" value="<?php echo $dataRow['email'];?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group row account-btn text-center mb-0">
                                <div class="col-12">
                                    <button class="btn btn-rounded btn-blue waves-effect waves-light" type="submit">Cập nhật</button>
                                    <button class="btn btn-rounded btn-dark waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Hủy</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="profile">
                        <h2 class="text-center mt-2">Đang xây dựng</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
