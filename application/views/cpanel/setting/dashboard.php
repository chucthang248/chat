<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=TITLE_MAIN?></a></li>
                        <li class="breadcrumb-item active">Cấu hình</li>
                    </ol>
                </div>
                <h4 class="page-title">Cấu hình</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <h4 class="header-title">Người dùng - phân quyền</h4>
                <hr />
                <div class="row">
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-grid avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/module"
                                        class="text-dark">Quản lý module</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-people avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/permission"
                                        class="text-dark">Phân quyền</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-user avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/user"
                                        class="text-dark">Quản lý tài khoản</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-user avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/classifyemployee"
                                        class="text-dark">Phân loại nhân sự</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="hr-border-weight" />
                <h4 class="header-title">Cấu hình chung</h4>
                <hr />
                <div class="row">
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-home avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/setting"
                                        class="text-dark">Thông tin chung</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-location-pin avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/branch"
                                        class="text-dark">Quản lý Chi nhánh</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-folder avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/position"
                                        class="text-dark">Quản lý Chức vụ</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-folder avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/formofwork"
                                        class="text-dark">Hình thức làm việc</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>

                </div>
                <hr />
                <div class="row">
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-folder avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/bussinesstype"
                                        class="text-dark">Loại hình kinh doanh</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-credit-card avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/bank"
                                        class="text-dark">Loại ngân hàng</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-graduation avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/standard"
                                        class="text-dark">Loại trình độ</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-graduation avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/school"
                                        class="text-dark">Trường học</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-graduation avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/majors"
                                        class="text-dark">Ngành học</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-graduation avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/edustatus"
                                        class="text-dark">Tình trạng học vấn</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon text-center">
                                <i class="fas font-20 fa-sign-in-alt"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/log"
                                        class="text-dark">Nhật ký hoạt động</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="hr-border-weight" />
                <h4 class="header-title">Hồ sơ</h4>
                <hr />
                <div class="row">
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-folder avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/files"
                                        class="text-dark">Quản lý hồ sơ</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-folder avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/typefiles"
                                        class="text-dark">Loại hồ sơ</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
</div>