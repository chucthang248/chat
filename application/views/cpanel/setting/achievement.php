<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=TITLE_MAIN?></a></li>
                        <li class="breadcrumb-item active">Thành tựu và giải thưởng</li>
                    </ol>
                </div>
                <h4 class="page-title">Thành tựu và giải thưởng</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <div class="row">
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-grid avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/prizes"
                                        class="text-dark">Giải thưởng</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-people avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/certificate"
                                        class="text-dark">Chứng chỉ</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="media">
                            <div class="avatar-sm rounded-circle widget-two-icon align-self-center">
                                <i class="icon-user avatar-title font-30 text-dark"></i>
                            </div>
                            <div class="wigdet-two-content media-body ml-1">
                                <h3 class="font-weight-medium font-15 mt-0 mb-0"><a href="cpanel/milestones"
                                        class="text-dark">Cột mốc</a></h3>
                                <p class="mb-0 font-11">Đây là đoạn mô tả về module</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>