<link rel="stylesheet" type="text/css" href="public/assets/customs/form.css">
<div class="body__form">
	<a href="javascript:void(0)" class="btnClose" onclick="closeForm()"><i class="icon-close"></i></a>
	<div class="box__form form-sm">
		<div class="box clearfix">
			<div class="title"><?=$title;?></div>
			<hr class="hr-xs" />
			<form action="<?=CPANEL.$control?>/<?=$action?>" method="POST">
				<div class="form">
					<div class="form-group">
	                    <label for="name" class="col-form-label">Tên phòng ban <span>*</span></label>
	                    <input type="text" name="data_post[name]" class="form-control" id="name">
	                </div>
	                <div class="form-group">
	                    <label for="parentID" class="col-form-label">Thuộc phòng ban</label>
	                    <select id="parentID" parsley-type="sex" name="data_post[parentID]" class="form-control">
	                        <option value="">-- Vui lòng chọn --</option>
	                        
	                    </select>
	                </div>
	                <div class="form-group">
	                    <label for="name" class="col-form-label">Mô tả chức năng nhiệm vụ</label>
	                    <textarea name="des" id="des" class="form-control" rows="5"></textarea>
	                </div>
	                <div class="box__btn">
	                	<button onclick="closeForm()">Bỏ qua</button>
	                	<button class="btn__blue">Lưu lại</button>
	                </div>
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function closeForm(){
		$('#myForm').html('');
	}
</script>
