<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/orgcharts.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<!-- chart -->
<link href="public/assets/libs/jquery-orgchart/jquery.orgchart.css" rel="stylesheet" type="text/css" />
<script src='https://rawgithub.com/debiki/utterscroll/master/jquery-scrollable.js'></script>
<script type="text/javascript">
    jQuery(function($) {
      // Here, could test if this is a touch device with not mouse, and, if so, don't enable.
      debiki.Utterscroll.enable();
    });
</script>
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box table-responsive">
                <button class="btn btn-blue ml-2" onclick="addForm(this)"> Thêm mới</button>
                <ul class="nav nav-tabs mt-3 mb-3" id="myTab">
                    <li class="nav-item">
                        <a href="#organizational_structure" data-toggle="tab" aria-expanded="true" class="nav-link ">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Cơ cấu tổ chức</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#chart" data-toggle="tab" aria-expanded="true" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Biểu đồ</span>
                        </a>
                    </li>

                </ul>
                <!-- bảng -->
                <div class="tab-content">
                    <div class="tab-pane show " id="organizational_structure">
                        <table class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">Phòng ban</th>
                                    <th class="text-center">Chức năng</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($datas as $key => $val) { ?>
                                    <?php
                                    // <i class="fas fa-chevron-right"></i>
                                    $arrow = $val['child'] != "" ? "fas fa-chevron-down arrow" : '';
                                    $margin = $val['count'] != 0 ? 27 * $val['count'] : 0;
                                    $margin = $val['child'] != "" ? $margin : $margin + 14;
                                    $id_col = $val['parentID'] != 0 ?  $val['parentID'] :  $val['id'];
                                    ?>
                                    <tr class="cursor_tr itemSpecific<?= $id_col; ?>" onclick="effectCloseRow(this,<?= $val['id']; ?>)" data-id="<?= $val['id']; ?>">
                                        <td class="text-left">
                                            <div class="subitem" style="padding-left:<?= $margin; ?>px">
                                                <a href="javascript:void(0)" class=""><i class="<?= $arrow ?>"></i></a>
                                                <span class="title_department"><?= $val['name'] ?></span>
                                            </div>
                                        </td>
                                        <td class="text-center" width="200">
                                            <a href="javascript:void(0)" onclick="editForm(this, <?= $val['id'] ?>)" class="btn btn-blue waves-effect waves-light btn-xs">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="del2(<?php echo $val['id']; ?>);" class="btn btn-danger btn-xs waves-effect waves-light delete<?php echo $val['id']; ?>" data-control="<?php echo $control; ?>">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane active chart_container" id="chart">
                        <div class="item__chart">
                            <!-- <div id="orgChart"></div>        -->
                            <table class="chart-table">
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php echo $charts?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->

<div id="loadForm"></div>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src='https://rawgithub.com/debiki/utterscroll/master/debiki-utterscroll.js'></script>
<script>
   
    // hiệu ứng đóng mở danh sách phòng ban
    function effectCloseRow(__this, id) {
        let getAlllist = $(".itemSpecific" + id).not(__this);
        $(__this).toggleClass("close_tr");
        let booleanClass = $(__this).hasClass("close_tr");
        if (booleanClass == true) {
            $(__this).find(".arrow").toggleClass("fa-chevron-right");
            $(__this).find(".arrow").toggleClass("fa-chevron-down");
            dequyHide(getAlllist);
        } else {
            $(__this).find(".arrow").toggleClass("fa-chevron-down");
            $(__this).find(".arrow").toggleClass("fa-chevron-right");
            dequyShow(getAlllist);
        }
    }
    // ẩn
    function dequyHide(getAlllist) {
        $(getAlllist).hide();
        if (getAlllist.length != 0) {
            $(getAlllist).each((key, obj, i) => {
                let getID = $(obj).attr("data-id");
                getAlllist = $(".itemSpecific" + getID);
                $(getAlllist).hide();
                dequyHide(getAlllist);
            });
        }
    }
    // hiện
    function dequyShow(getAlllist) {
        $(getAlllist).show();
        if (getAlllist.length != 0) {
            $(getAlllist).each((key, obj, i) => {
                let getID = $(obj).attr("data-id");
                getAlllist = $(".itemSpecific" + getID);
                $(getAlllist).show();
                dequyShow(getAlllist);
            });
        }
    }
    // thêm phòng ban
    function addForm(_this) {
        $.ajax({
            type: "POST",
            url: "<?= base_url("cpanel/ajax/addDepartment") ?>",
            success: function(response) {
                $("#loadForm").html(response);
                $('.parentID').select2();
               
            }
        });
    }
    // sửa 
    function editForm(__this, id) {
        $.ajax({
            type: "POST",
            url: "<?= base_url("cpanel/ajax/editDepartment") ?>",
            data: {
                id: id
            },
            success: function(response) {
                $("#loadForm").html(response);
                $('.parentID').select2();
            }
        });
    }

    function del2(id) {
        Swal.fire({
            title: "Bạn có chắc chắn muốn xóa không?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3085d6",
            cancelButtonColor: "#d33",
            confirmButtonText: "Đồng ý"
        }).then(function(result) {
           
            if (result.value) {

                var control = $('.delete' + id).attr('data-control');
                if (id != '') {
                    $.ajax({
                        method: "POST",
                        url: "cpanel/" + control + "/checkChilditem",
                        data: {
                            id: id
                        },
                        dataType: "JSON",
                        success: function(res) {
                            startDel(id,res,control);
                           
                        },
                    });

                }
               
            }
        });
    }

    function startDel(id,res,control) {
        if (res.type == "has_data") {
            Swal.fire({
                title: "Có dữ liệu mục con! bạn có muốn xóa không?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Đồng ý"
            }).then(function(result) {
                if (result.value == true) {
                    $.ajax({
                        method: "POST",
                        url: "cpanel/" + control + "/delete",
                        data: {
                            id: id
                        },
                        success: function(data) {
                           
                            if (data != "") {
                                    let parseData = JSON.parse(data);
                                if (parseData.type == "error") {
                                    Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                                }
                               
                            } else {
                                $('.delete' + id).parent().parent().fadeOut();
                                Swal.fire("Xóa thành công!", "", "success");
                            }
                           
                        }
                    });
                }
               
            });
        } 
        if (res.type == "error") {
            $.ajax({
                method: "POST",
                url: "cpanel/" + control + "/delete",
                data: {
                    id: id
                },
                success: function(data) {
                    if (data != "") {
                            let parseData = JSON.parse(data);
                        if (parseData.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                        }
                    } else {
                        $('.delete' + id).parent().parent().fadeOut();
                        Swal.fire("Xóa thành công!", "", "success");
                    }
                    
                }
            });
        } 
        if (res.type == "no_data")  {
            $.ajax({
                method: "POST",
                url: "cpanel/" + control + "/delete",
                data: {
                    id: id
                },
                success: function(data) {
                    if (data != "") {
                            let parseData = JSON.parse(data);
                        if (parseData.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                        }
                    } else {
                        $('.delete' + id).parent().parent().fadeOut();
                        Swal.fire("Xóa thành công!", "", "success");
                    }
                }
            });
        }
    }
</script>