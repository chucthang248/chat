<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row justify-content-center">
        <div class="col-lg-5">
            <div class="card-box">
                <h4 class="header-title mb-4"><?=$title;?></h4>
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <div class="clear mt-3"></div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <!-- input hidden -->
                                <input type="hidden" required parsley-type="code" name="data_post[employeeID]" class="form-control" id="employeeID">
                                <!-- end input hidden -->
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Chọn nhân sự<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <select id="employeeID" onchange="randomPass(this)" parsley-type="employeeID" class="form-control">
                                            <option selected="">--Chọn nhân sự--</option>
                                            <?php foreach ($datas as $key => $value) { ?>
                                                <option value="<?=$value['id']?>"><?=$value['fullname']?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Họ và tên<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="text" required parsley-type="code" name="data_post[fullname]" class="form-control" id="fullname" value="<?php echo set_value('data_post[fullname]') ?>">
                                        <div class="has-error text-danger"><?php echo form_error('data_post[fullname]') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Username<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="username"  required parsley-type="username" name="data_post[username]" class="form-control" id="username">
                                        <div class="has-error text-danger"><?php echo form_error('data_post[username]') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Tài khoản<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="email" required parsley-type="code" name="data_post[email]" class="form-control" id="email">
                                        <div class="has-error text-danger"><?php echo form_error('data_post[email]') ?></div>
                                    </div>
                                </div>
                                <?php /*<div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Mật khẩu<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="password" required parsley-type="code" name="data_post[password]" class="form-control" id="password">
                                    </div>
                                </div> */?>
                                <div class="checkbox checkbox-blue">
                                    <input id="active" type="checkbox" name="data_post[active]" checked value="1">
                                    <label for="active">
                                        kích hoạt
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box__tools text-center">
                        <button class="btn btn-blue waves-effect waves-light mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
    function randomPass(el){
        let id = $(el).val();
        $.ajax({
            url: '<?=$path_url?>random_pass',
            type: 'POST',
            dataType: 'json',
            data: {id: id},
            success: function(data){
                $("#employeeID").val(data.employeeID); 
                $("#fullname").val(data.fullname); 
                $("#email").val(data.email); 
                $("#password").val(data.password);
                if(data.error == 1){
                    $("#fullname").val('');
                    $("#email").val(''); 
                    $("#password").val('');
                }
            }
        })
    }
</script>

