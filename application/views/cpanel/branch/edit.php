<div id="edit-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Cập nhật dữ liệu</h4>
            </div>
            <div class="modal-body p-3">
                <form class="form-horizontal" id="form-data" action="<?=CPANEL.$control?>/edit" method="POST" data-toggle="validator" novalidate="true">
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="name">Tên chi nhánh</label>
                            <input class="form-control" type="text" id="name" name="data_update[name]" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="phone">Số điện thoại</label>
                            <input class="form-control" type="text" id="phone" name="data_update[phone]" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="email">Email</label>
                            <input class="form-control" type="text" id="email" name="data_update[email]" required="">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label for="address">Địa chỉ</label>
                            <textarea class="form-control" id="address" name="data_update[address]" required=""></textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <div class="checkbox checkbox-secondary">
                                <input id="publish-update" type="checkbox" name="data_update[publish]" value="1">
                                <label for="publish-update">
                                    Hiển thị
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row account-btn text-center mb-0">
                        <div class="col-12">
                            <input type="hidden" id="idRow" name="idRow" value="">
                            <button class="btn btn-rounded btn-blue waves-effect waves-light" type="submit">Cập nhật</button>
                            <button class="btn btn-rounded btn-dark waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Hủy</button>
                        </div>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#edit-modal').on('show.bs.modal', function (event) {
            var id = $(event.relatedTarget).attr('data-id');
            if(id != '')
            {
                $.ajax
                ({
                    method: "POST",
                    url: "<?=CPANEL.$control?>/getDataRow",
                    data: { id:id},
                    dataType: "json",
                    success: function(data){
                        if(data.result){
                            $('#form-data #idRow').val(data.result.id);
                            $('#form-data #name').val(data.result.name);
                            $('#form-data #phone').val(data.result.phone);
                            $('#form-data #email').val(data.result.email);
                            $('#form-data #address').val(data.result.address);
                            if(data.result.publish == 1){
                                $('#form-data #publish-update').attr("checked", "checked");
                            }else{
                                $('#form-data #publish-update').removeAttr("checked", "checked");
                            }
                        }
                    }
                });
            }
            
        });
    });
</script>