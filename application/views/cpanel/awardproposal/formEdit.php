<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Cập nhật đề xuất</h4>
    </div>
    <div class="modal-body p-3">
        <form id="myForm" class="form-horizontal" action="<?= CPANEL?>/awardproposal/edit" method="POST" data-toggle="validator" novalidate="true">
        <input type="hidden" name="idRow" value="<?=$id?>">
            <div class="form-group row">
                <div class="col-12">
                    <div class="lb">
                        <label for="username">Nhân sự </label>
                    </div>
                    <select required="" class="form-control" name="data_post[employeeID]" id="employeeID">
                        <option value="">-- Chọn nhân sự --</option>
                        <?php if($employees != NULL ) { ?>
                            <?php foreach($employees as $key_el => $val_el){ ?>
                                <option <?=$datas['employeeID'] == $val_el['id'] ? "selected" : "" ?> value="<?=$val_el['id'] ?>"><?=$val_el['fullname'] ?></option>
                            <?php } ?>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="lb">
                        <label for="username">Giải thưởng</label>
                    </div>
                    <select required=""  class="form-control" name="data_post[prizesID]" id="prizesID">
                        <option value="">-- Chọn giải thưởng --</option>
                        <?php if($prizes != NULL ) { ?>
                            <?php foreach($prizes as $key_prizes => $val_prizes){ ?>
                                <option <?=$datas['prizesID'] == $val_prizes['id'] ? "selected" : "" ?> value="<?=$val_prizes['id'] ?>"><?=$val_prizes['name'] ?></option>
                            <?php } ?>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row account-btn text-center mb-0">
                <div class="col-12">
                    <button class="btn btn-rounded btn-blue waves-effect waves-light" type="submit">Cập nhật</button>
                    <button class="btn btn-rounded btn-dark waves-effect waves-light" data-dismiss="modal" aria-hidden="true">Hủy</button>
                </div>
            </div>
        </form>
    </div>
</div><!-- /.modal-content -->
<script>
    $("#myForm").submit(function() {
            let employee = $("#employeeID").val();
            let prizes = $("#prizesID").val();
           
            if(employee != "" && prizes != "" )
            {
                $("#myForm").unbind('submit');
                $("#myForm").submit();
            }
            event.preventDefault();
        });
</script>