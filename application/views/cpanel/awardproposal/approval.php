<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Cập nhật đề xuất</h4>
    </div>
    <div class="modal-body p-3">
        <form id="myForm" class="form-horizontal" action="<?= CPANEL?>/awardproposal/approval" method="POST" data-toggle="validator" novalidate="true">
            <input type="hidden" id="idRow" name="idRow" value="<?=$id?>">
            <div class="form-group row">
                <div class="col-12">
                    <div class="lb">
                        <label for="username">Nhân sự </label>
                    </div>
                    <select required="" class="form-control" name="data_post[employeeID]" id="employeeID">
                        <option value="">-- Chọn nhân sự --</option>
                        <?php if($employees != NULL ) { ?>
                            <?php foreach($employees as $key_el => $val_el){ ?>
                                <option <?=$datas['employeeID'] == $val_el['id'] ? "selected" : "" ?> value="<?=$val_el['id'] ?>"><?=$val_el['fullname'] ?></option>
                            <?php } ?>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="lb">
                        <label for="username">Giải thưởng</label>
                    </div>
                    <select required=""  class="form-control" name="data_post[prizesID]" id="prizesID">
                        <option value="">-- Chọn giải thưởng --</option>
                        <?php if($prizes != NULL ) { ?>
                            <?php foreach($prizes as $key_prizes => $val_prizes){ ?>
                                <option <?=$datas['prizesID'] == $val_prizes['id'] ? "selected" : "" ?> value="<?=$val_prizes['id'] ?>"><?=$val_prizes['name'] ?></option>
                            <?php } ?>
                        <?php  } ?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="lb">
                        <label for="username">Trạng thái</label>
                    </div>
                    <select required="" onchange="approvalSelect(this)" class="form-control" name="data_post[status]" id="status">
                        <option value="">-- Chọn trạng thái --</option>
                        <option value="1">Đã duyệt</option>
                        <option value="2">Đang chờ duyệt</option>
                        <option value="3">Không được duyệt</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
</div><!-- /.modal-content -->
<script>
    $("#myForm").submit(function() {
            let employee = $("#employeeID").val();
            let prizes = $("#prizesID").val();
           
            if(employee != "" && prizes != "" )
            {
                $("#myForm").unbind('submit');
                $("#myForm").submit();
            }
            event.preventDefault();
    });

    // duyệt 
    function approvalSelect(_this)
    {
        let status = $(_this).val();
        let idRow  = $("#idRow").val();
        $.ajax({
            type: "POST",
            url: "cpanel/awardproposal/approval",
            data: {status: status,idRow:idRow},
            success: function (response) {
                $.toast({
                    heading: 'Thông báo!',
                    text: 'Cập nhật hoàn tất.',
                    position: 'top-right',
                    loaderBg: '#5ba035',
                    icon: 'success',
                    hideAfter: 2000,
                });

                let url = 'cpanel/Awardproposal/pagination';
                let page = 1;
                let data = {
                    "page": 1,
                    "type_pageload": "ajax"
                }
                CallAjax(data, url);
            }
        });
    }
</script>