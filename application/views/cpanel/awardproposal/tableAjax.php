<table class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">Người đề xuất</th>
                                    <th class="text-center">Giải thưởng</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($datas as $key => $val) { 
                                    $status = '';
                                    if($val['status'] ==  1)
                                    {
                                        $status = 'Đã duyệt';
                                    }
                                    if($val['status'] == 2)
                                    {
                                        $status = 'Đang chờ duyệt';
                                    }
                                    if($val['status'] ==  3)
                                    {
                                        $status = 'Không duyệt';
                                    }
                                    
                                    ?>
                                    <tr >
                                        <td class="text-left">
                                            <?= $val['proposal_person_name'] ?>
                                        </td>
                                        <td class="text-left">
                                            <?= $val['prizes_name'] ?>
                                        </td>
                                        <td class="text-left">
                                            <div class="text_status_<?=$val['status']?>">
                                                <?= $status?>     
                                            </div>
                                        </td>
                                        <td class="text-center" width="200">
                                            <a href="javascript:void(0)" onclick="approval('<?= $val['id'] ?>')" class="btn btn-blue waves-effect waves-light btn-xs">
                                                <i class="icon-eye "></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="editForm(this, '<?= $val['id'] ?>')" class="btn btn-blue waves-effect waves-light btn-xs">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="del('<?php echo $val['id']; ?>');" class="btn btn-danger btn-xs waves-effect waves-light delete<?php echo $val['id']; ?>" data-control="<?php echo $control; ?>">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- pagination -->
                        <div class="wrap___flex d-flex t_justify_end ">
                            <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                                <?php echo $my_pagination; ?>
                            </div>
                        </div>    