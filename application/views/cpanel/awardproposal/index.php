<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/orgcharts.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<!-- chart -->
<script src='https://rawgithub.com/debiki/utterscroll/master/jquery-scrollable.js'></script>
<script type="text/javascript">
    jQuery(function($) {
      // Here, could test if this is a touch device with not mouse, and, if so, don't enable.
      debiki.Utterscroll.enable();
    });
</script>
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->
    <div class="row">
        <div class="col-md-12">
            <div class="card-box table-responsive">
                <button class="btn btn-blue mb-2" onclick="addForm(this)">Thêm đề xuất</button>
                <!-- bảng -->
                <div class="tab-pane show " id="organizational_structure mt-4">
                    <div id="loadTable">
                        <table class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead>
                                <tr>
                                    <th class="text-center">Người đề xuất</th>
                                    <th class="text-center">Giải thưởng</th>
                                    <th class="text-center">Trạng thái</th>
                                    <th class="text-center">Tác vụ</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($datas as $key => $val) { 
                                    $status = '';
                                    if($val['status'] ==  1)
                                    {
                                        $status = 'Đã duyệt';
                                    }
                                    if($val['status'] == 2)
                                    {
                                        $status = 'Đang chờ duyệt';
                                    }
                                    if($val['status'] ==  3)
                                    {
                                        $status = 'Không duyệt';
                                    }
                                    
                                    ?>
                                    <tr >
                                        <td class="text-left">
                                            <?= $val['proposal_person_name'] ?>
                                        </td>
                                        <td class="text-left">
                                            <?= $val['prizes_name'] ?>
                                        </td>
                                        <td class="text-left">
                                            <div class="text_status_<?=$val['status']?>">
                                                <?= $status?>     
                                            </div>
                                        </td>
                                        <td class="text-center" width="200">
                                            <a href="javascript:void(0)" onclick="approval('<?= $val['id'] ?>')" class="btn btn-blue waves-effect waves-light btn-xs">
                                                <i class="icon-eye "></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="editForm(this, '<?= $val['id'] ?>')" class="btn btn-blue waves-effect waves-light btn-xs">
                                                <i class="far fa-edit"></i>
                                            </a>
                                            <a href="javascript:void(0)" onclick="del('<?php echo $val['id']; ?>');" class="btn btn-danger btn-xs waves-effect waves-light delete<?php echo $val['id']; ?>" data-control="<?php echo $control; ?>">
                                                <i class="far fa-trash-alt"></i>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <!-- pagination -->
                        <div class="wrap___flex d-flex t_justify_end ">
                            <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                                <?php echo $my_pagination; ?>
                            </div>
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->

<div id="add-modal" class="modal proposal_form fade" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
       
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
     .text_status_1
    {
        color: green;
    }
    .text_status_2
    {
        color: orange;
    }
    .text_status_3
    {
        color: red;
    }
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src='https://rawgithub.com/debiki/utterscroll/master/debiki-utterscroll.js'></script>
<script>
    // thêm đề xuất
   function addForm()
   {
       $.ajax({
           type: "POST",
           url: "cpanel/awardproposal/formAdd",
           success: function (res) {

                $(".modal-dialog").html(res);
                $(".modal").modal("show");
           }
       });
   }
   // cập nhật đề xuất
   function editForm(_this,id)
   {
        $.ajax({
           type: "POST",
           url: "cpanel/awardproposal/formEdit",
           data: {id:id},
           success: function (res) {
                $(".modal-dialog").html(res);
                $(".modal").modal("show");
           }
       });
   }
   // duyệt
   function approval(id)
   {
        $.ajax({
            type: "POST",
            url: "cpanel/awardproposal/approval",
            data: {id:id},
            success: function (res) {
                 checkJson = IsJsonString(res);
                 if(checkJson == true)
                 {
                    res = JSON.parse(res);
                    if (res.type == "error") {
                        Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                    }
                 }
                else
                {
                    $(".modal-dialog").html(res);
                    $(".modal").modal("show");
                }
                   
            }
        });
   }
   function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
</script>
<script type="text/javascript">
    let url_pagination = 'cpanel/Awardproposal/pagination';
    // PAGINATION
    $(document).on('click', '.pagination .forClick a', function() {
        let url = url_pagination;
        let gethref = $(this).attr('href');
        let page = $(this).attr('data-ci-pagination-page');
        let data = {
            "page": page,
            "type_pageload": "ajax"
        }
        CallAjax(data, url);
        event.preventDefault();
    });

    function CallAjax(data, url) {
        
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res) {
                debugger;
                $("#loadTable").html(res);
            }
        });
    }

  
</script>