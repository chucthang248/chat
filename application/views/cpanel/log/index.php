<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/clockpicker/bootstrap-clockpicker.min.css" rel="stylesheet">
<link href="public/assets/libs/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title">Nhật ký hoạt động</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row" id="listemployee">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-12 d-flex ">
                        <div class="right_box col-6">
                            <form action="">
                                <div class="d-flex">
                                    <input  type="text" id="email" class="form-control mr-2 " placeholder="Nhập email">
                                    <div class="input-daterange input-group" id="date-range">
                                        <input type="text" id="from_day" class="form-control mr-2" name="start"  placeholder="Bắt đầu: 02/05/2019" value="">
                                        <input type="text" id="to_day" class="form-control" name="end"  placeholder="Kết thúc: 05/05/2020" value="">
                                    </div>
                                   
                                    <button type="button" onclick="btnSearch()" class="col-md-auto btn btn-blue btn_search_employee waves-effect waves-light ml-2"><i class="fas fa-search"></i> Tìm kiếm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="loadTable" class="mt-3">
                    <div class="table-scroll mb-2" >
                        <div class="table-wrap_other">
                            <table class=" table-striped table-hover ">
                                <thead>
                                    <tr>
                                        <th class="text-center" >Nhật ký hoạt động <?=$showDay ?></th>
                                    </tr>
                                </thead>
                                <?php if (isset($datas) && $datas != NULL) { ?>
                                    <tbody>
                                        <?php foreach ($datas as $key => $val) { ?>
                                            <tr class="tr<?= $val['id']; ?>">
                                                <td>
                                                    <div class="row">
                                                        <div class="col-md-2">
                                                            <strong><?=$val['email'] ?></strong>
                                                        </div>
                                                        <div class="col-md-auto">
                                                            <?=$val['des'] ?> ngày: <?=$val['created_at'] ?>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

                    <!-- pagination -->
                    <div class="wrap___flex d-flex t_justify_end ">
                        <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                            <?php echo $my_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->

</div> <!-- end container-fluid -->
<!-- Xem nhanh thông tin nhân sự -->
<a href="" id="excel_dowload" download=""> </a>
<div id="loadEmloyeeInfoDetail"></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
 <!-- Vendor js -->
 <script src="public/assets/js/vendor.min.js"></script>
<script src="public/assets/libs/moment/moment.min.js"></script>
<script src="public/assets/libs/bootstrap-colorpicker/bootstrap-colorpicker.min.js"></script>
<script src="public/assets/libs/clockpicker/bootstrap-clockpicker.min.js"></script>
<script src="public/assets/libs/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="public/assets/libs/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- Init js-->
<script src="public/assets/js/pages/form-pickers.init.js"></script>
<!-- App js -->
<script src="public/assets/js/app.min.js"></script>
<script type="text/javascript">
    let url_pagination = 'cpanel/log/pagination';
    let url_search     =  'cpanel/log/search';
        $('#from_day').datepicker({ format: "dd/mm/yyyy"}); // format to show
        $('#to_day').datepicker({ format: "dd/mm/yyyy"}); // format to show
    // phân quyền xem thông báo ứng tuyển
    function permissionAlert()
    {
        $("#type_display").toggleClass("show");
    }
    $(document).ready(function() {
    
        // ngày
        $('[data-toggle="input-mask"]').each(function (idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {'reverse': reverse});
            else
                $(obj).mask(maskFormat);
        });
        // 
        $("#search").val("<?= $key_word ?>");
    });
    let globalFileName = "";

    // seach 
    function btnSearch() {
        let url = url_search;
       let from_day =  new Date($("#from_day").val());
       let to_day    =  new Date($("#to_day").val());
       if(from_day > to_day)
       {
            Swal.fire({
                title: "Ngày bắt đầu phải nhỏ hơn ngày kết thúc",
                type: "warning",
                showCancelButton: true,
            });
       }else
       {
            $.toast({
                heading: 'Thông báo!',
                text: 'Tìm kiếm hoàn tất.',
                position: 'top-right',
                loaderBg: '#5ba035',
                icon: 'success',
                hideAfter: 2000,
            });
            let data = {
                "from_day": $("#from_day").val(),
                "to_day": $("#to_day").val(),
                "email" : $("#email").val(),
                "type_pageload": "ajax",
            }
            CallAjax(data, url);
       }
      
    }
    // PAGINATION
    $(document).on('click', '.pagination .forClick a', function() {
        let from_day =  $("#from_day").val();
        let  to_day = $("#to_day").val();
        let  email = $("#email").val();
        // debugger;
        let url = url_pagination;
        // nếu input tìm kiếm có nội dung hoặc url có giá trị vừa tìm kiếm thì đổi URL
        if (from_day != "" || to_day != "" || email != "" ) {
            url = url_search;
        }
        let gethref = $(this).attr('href');
        let page = $(this).attr('data-ci-pagination-page');
       console.log(url);
        //history.replaceState(null, null, gethref);
        let data = {
            "from_day": from_day,
            "to_day": to_day,
            "email": email,
            "page": page,
            "type_pageload": "ajax"
        }
        console.log(data);
        CallAjax(data, url);
        event.preventDefault();
    });

    function CallAjax(data, url) {
        
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res) {
            //      res = JSON.parse(res);
               // console.log(res);
                $("#loadTable").html(res);
            }
        });
    }

  
</script>