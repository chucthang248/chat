<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="mb-0">Lịch làm việc từ ngày 02/03/2021 - 09/03/2021</h4>
                <hr />
                <div class="d-flex flex-end res-flex">
                    <div class="box__branch">
                        <select class="form-control" id="filterBranch">
                                <option value="">-- Chọn chi nhánh --</option>
                            <?php foreach($branchs as $key => $val){ ?>
                                <option value="<?=$val['id']?>"><?=$val['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <form action="<?= CPANEL . 'employee'; ?>">
                    <div id='calendar'></div>
                </form>
            </div>
        </div>
    </div> <!-- end row -->
</div> <!-- end container-fluid -->
<div id="loadForm"></div>
<style>
    .select2-container {
        z-index: 2000;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    window.onresize = function(event) {
        location.reload();
    };
    // full calendar
    let start = "";
    let getDMY = "";
    let getYear = "";
    let getMonth = "";
    let getDay = "";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },
            eventResourceEditable: true,
            buttonText: {
                today: 'Hôm nay'
            },
            locale: 'vi',
        });
        // load events
        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "cpanel/overviewtimework/loadEvents",
                dataType: "JSON",
                success: function(res) {
                    let data_timework = res.listTimeWork;
                    calendar.addEventSource(data_timework);
                }
            });
        });
        // lọc chi nhánh
        $(document).on("change","#filterBranch",function(){
            let branch_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "cpanel/overviewtimework/filter_branch",
                data: {branch_id: branch_id},
                dataType: "JSON",
                success: function(res) {
                    calendar.removeAllEvents();
                    let data_timework = res.listTimeWork;
                    calendar.addEventSource(data_timework);
                }
            });
            console.log(branch_id);
        });
        // xong tất các tác vụ thì mới render
        calendar.render();
    });
    // uuid
    function uuid() {
        var temp_url = URL.createObjectURL(new Blob());
        var uuid = temp_url.toString();
        URL.revokeObjectURL(temp_url);
        return uuid.substr(uuid.lastIndexOf('/') + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
    }
</script>