<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-md-12">
            <div class="card-box">
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <?php if($module != NULL){  ?>
                    <div class="form-group">
                        <select name="data_post[parentID]" id="" class="form-control col-2">
                            <option value="">-- Chọn module cha --</option>
                            <?php foreach($module as $key => $val){ ?>
                                <option  <?=$val['id']==$Modules['id']? "selected" : ""; ?> value="<?=$val['id'] ?>"><?=str_repeat("---",$val['count']).$val['name'] ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="inputName" class="control-label">Tên module</label>
                        <input type="text" class="form-control" id="name" name="data_post[name]" value="<?=$Modules['name']?>" required="">
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="control-label">Link</label>
                        <input type="text" class="form-control" id="link" name="data_post[link]" value="<?=$Modules['link']?>"  >
                    </div>
                    <div class="form-group">
                        <label for="inputName" class="control-label">Controller</label>
                        <input type="text" class="form-control" id="controller" name="data_post[controller]" value="<?=$Modules['controller']?>"  >
                    </div>
                   
                    <div class="checkbox checkbox-danger">
                        <input id="checkbox6" type="checkbox" checked="" <?php if($Modules['publish'] == 1) { echo "checked";} ?> name="data_post[publish]" value="1">
                        <label for="checkbox6"> Hiển thị </label>
                     </div>
                     <div class="form-group row">
                            <label for="checkbox6"class=" col-form-label ml-3"> Thêm action </label>  
                            <button type="button"  onclick="ActionFn('add',this,null)"  class="btn btn-dark waves-effect waves-light ml-2" type="checkbox" ><i class="fas fa-folder-plus"></i></button> 
                     </div>
                     <div class="add_actione">
                        <div class="action_item">
                            <?php if($ModulesDetail != NULL){ ?>
                                <?php foreach($ModulesDetail as $key => $val){ ?>
                                    <div class="form-group item__group d-flex">
                                        <div class="item__col col-md-5">
                                             <input type="text" class="form-control  w-full" id="action" name="data_post[add_action][<?=$key?>][name_action]" value="<?=$val['name'];?>" placeHolder="Tên action" value="Danh sách" required="">
                                        </div>
                                        <div class="item__col col-md-5">
                                            <input type="text" class="form-control  w-full" id="action" name="data_post[add_action][<?=$key?>][action]" value="<?=$val['action'];?>"  placeHolder="Action" value="index"  required="">
                                        </div>
                                        <div class="item__col col-md-2 d-flex">
                                            <button  type="button"  onclick="ActionFn('delete',this,<?=$val['id'];?>)"   class="del_btn btn btn-dark waves-effect waves-light "> <i class="fas fa-trash-alt "></i></button>
                                            <input type="text" placeholder="sắp xếp" class="form-control ml-2 col-3" name="data_post[add_action][<?=$key?>][sort]" value="<?=$val['sort'];?>">  
                                        </div>
                                    </div>
                                <?php } ?>
                          <?php } ?>
                        </div>
                     </div>
                     <div class="box__tools ">
                        <button class="btn btn-primary waves-effect waves-light ml-2 mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
    function ActionFn(type,__this,id)
    {  
        if(type == 'add')
        {
            let Count_item_addaction = document.querySelectorAll(".item__group").length - 1;
            let numbCount = ++Count_item_addaction;
            let sort = ++numbCount;
            $(".action_item").append(` <div class="form-group item__group d-flex">
                                            <div class="item__col col-md-5">
                                                <input type="text" class="form-control w-full" id="action"
                                                    name="data_post[add_action][${numbCount}][name_action]" placeHolder="tên" value="" required="">
                                            </div>
                                            <div class="item__col col-md-5">
                                                <input type="text" class="form-control  w-full" id="action"
                                                    name="data_post[add_action][${numbCount}][action]"  placeHolder="tên action" value="" required="">
                                            </div>
                                            <div class="item__col col-md-2 d-flex">
                                                <button type="button" onclick="ActionFn('delete',this)"  class="del_btn btn btn-dark waves-effect waves-light"> <i  class="fas fa-trash-alt "></i></button>
                                                <input type="text" placeholder="sắp xếp"  class="form-control ml-2  col-3 "  name="data_post[add_action][${numbCount}][sort]" value="${sort}">
                                            </div>
                                        </div>`);
        }
       if(type == 'delete')
        {
            $.ajax({
                url: 'cpanel/<?=$control?>/deleteAjax',
                method: "POST",
                data: {id : id},
                success: function(res)
                {
                        $(__this).closest(".item__group").remove();
                }
            });
        
        }
        
    }
</script>