<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="<?= CPANEL . $control ?>/add" type="button" class="btn btn-blue waves-effect waves-light">
                                <i class="icon-plus mr-1"></i> Thêm mới
                            </a>
                            <a href="<?= CPANEL . $control ?>" type="button" class="btn btn-linkedin waves-effect waves-light">
                                <i class="icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr />
                <table class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th class="text-center">Tên</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if (isset($datas) && $datas != NULL) { ?>
                        <tbody>
                            <?php foreach ($datas as $key => $val) { ?>
                                <?php
                                    $arrow = $val['child'] != "" ? "fas fa-chevron-down arrow" : '';
                                    $margin = $val['count'] != 0 ? 27 * $val['count'] : 0;
                                    $margin = $val['child'] != "" ? $margin : $margin + 14;
                                    $id_col = $val['parentID'] != 0 ?  $val['parentID'] :  $val['id'];
                                ?>
                                <tr  class="cursor_tr itemSpecific<?= $id_col; ?>" onclick="effectCloseRow(this,<?= $val['id']; ?>)" data-id="<?= $val['id']; ?>" >
                                    <td class="text-left">
                                        <div class="subitem" style="padding-left:<?= $margin; ?>px">
                                            <a href="javascript:void(0)" class=""><i class="<?= $arrow ?>"></i></a>
                                            <span class="title_department"><?= $val['name'] ?></span>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <?= $val['updated_at'] ?>
                                    </td>
                                    <td class="text-center">
                                        <a href="<?= $path_url . 'edit/' . $val['id'] ?>" class="btn btn-blue waves-effect waves-light btn-xs">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" onclick="del(<?php echo $val['id']; ?>);" class="btn btn-danger btn-xs waves-effect waves-light delete<?php echo $val['id']; ?>" data-control="<?php echo $control; ?>">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
<script>
    // hiệu ứng đóng mở danh sách phòng ban
    function effectCloseRow(__this, id) {
        let getAlllist = $(".itemSpecific" + id).not(__this);
        $(__this).toggleClass("close_tr");
        let booleanClass = $(__this).hasClass("close_tr");
        if (booleanClass == true) {
            dequyHide(getAlllist);
        } else {
            dequyShow(getAlllist);
        }
    }
    // ẩn
    function dequyHide(getAlllist) {
        $(getAlllist).hide();
        if (getAlllist.length != 0) {
            $(getAlllist).each((key, obj, i) => {
                let getID = $(obj).attr("data-id");
                getAlllist = $(".itemSpecific" + getID);
                $(getAlllist).hide();
                dequyHide(getAlllist);
            });
        }
    }
    // hiện
    function dequyShow(getAlllist) {
        $(getAlllist).show();
        if (getAlllist.length != 0) {
            $(getAlllist).each((key, obj, i) => {
                let getID = $(obj).attr("data-id");
                getAlllist = $(".itemSpecific" + getID);
                $(getAlllist).show();
                dequyShow(getAlllist);
            });
        }
    }
</script>