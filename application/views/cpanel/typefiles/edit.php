<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title mb-4"><?=$title;?></h4>
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <div class="clear mt-3"></div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Tên hồ sơ<span class="text-danger">
                                            *</span></label>
                                    <div class="col-12">
                                        <input type="text" required parsley-type="code" name="data_post[name]"
                                            class="form-control" id="code"
                                            value="<?=$datas['name']?>">
                                        <div class="has-error text-danger"><?php echo form_error('data_post[name]') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="checkbox checkbox-blue checkbox-single ml-2 mb-2">
                        <input  type="checkbox" id="publish" <?=$datas['publish'] == 1?"checked":"";?>  name="data_post[publish]" value="1" >
                        <label class="t_padding_left_30 width_180" >  Hiển thị</label>
                    </div>
                    <div class="box__tools text-left ml-2">
                        <button class="btn btn-blue waves-effect waves-light mr-1" type="submit"><i
                                class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i
                                class="fas fa-backspace"></i> Hủy</a>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>