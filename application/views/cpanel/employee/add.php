<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title mb-4">Thêm mới nhân sự</h4>

                <ul class="nav nav-tabs" id="myTab">
                    <li class="nav-item">
                        <a href="#general" data-toggle="tab" aria-expanded="false" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="mdi mdi-home-variant"></i></span>
                            <span class="d-none d-sm-block">Thông tin cá nhân</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#mySalary" data-toggle="tab" aria-expanded="true" class="nav-link">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Lương & phúc lợi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#myJob" data-toggle="tab" aria-expanded="true" class="nav-link">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Trình độ</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#workHistory" data-toggle="tab" aria-expanded="true" class="nav-link">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Lịch sử công tác</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#infoRelative" data-toggle="tab" aria-expanded="true" class="nav-link">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Thông tin người thân</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#documentFile" data-toggle="tab" aria-expanded="true" class="nav-link">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Tài liệu đối chiếu</span>
                        </a>
                    </li>
                </ul>
                <form method="POST" action="" class="parsley-examples" novalidate="" onsubmit="return SubmitForm();" id="myForm" enctype="multipart/form-data">
                    <div class="tab-content">
                        <div class="tab-pane show active" id="general">
                            <div class="clear mt-3"></div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <label for="code" class="col-3 col-form-label">Mã NV<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input type="text" required parsley-type="code" name="data_post[code]" class="form-control" id="code" value="<?php //echo $code?>">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="fullname" class="col-3 col-form-label">Họ và tên<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input type="text" required parsley-type="fullname" name="data_post[fullname]" value="<?php echo set_value('data_post[fullname]'); ?>" class="form-control" id="fullname">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="sex" class="col-3 col-form-label">Giới tính<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <select id="sex" parsley-type="sex" name="data_post[sex]" class="form-control">
                                                    <option value="1">Nam</option>
                                                    <option value="2">Nữ</option>
                                                    <option value="3">Khác</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="birthday" class="col-3 col-form-label">Ngày sinh<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input type="text" required parsley-type="birthday" class="form-control" id="birthday"  value="<?php echo set_value('data_post[birthday]'); ?>" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="21/04/1991" name="data_post[birthday]">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="marital" class="col-3 col-form-label">Hôn nhân<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <select id="marital" parsley-type="marital" name="data_post[marital]" class="form-control">
                                                    <option value="1">Độc thân</option>
                                                    <option value="2">Đã kết hôn</option>
                                                    <option value="3">Khác</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="permanent_address" class="col-3 col-form-label">Đ/C thường trú<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <textarea required parsley-type="code" class="form-control" name="data_post[permanent_address]" rows="4" id="permanent_address"><?php echo set_value('data_post[permanent_address]'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="temporary_address" class="col-3 col-form-label">Đ/C tạm trú<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <textarea required parsley-type="code" class="form-control" name="data_post[temporary_address]" rows="4" id="temporary_address"><?php echo set_value('data_post[temporary_address]'); ?></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="phone" class="col-3 col-form-label">Số ĐT<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input type="text"  parsley-type="phone" name="data_post[phone]"  value="<?php echo set_value('data_post[phone]'); ?>" class="form-control" id="phone">
                                                <div class="has-error t_color_red error-phone"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email" class="col-3 col-form-label">Email nội bộ<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input type="text"  parsley-type="email" name="data_post[email]"  value="<?php echo set_value('data_post[email]'); ?>"  class="form-control" id="email">
                                                <div class="has-error t_color_red error-mail"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="email_personal" class="col-3 col-form-label">Email cá nhân<span class="text-danger">*</span></label>
                                            <div class="col-9">
                                                <input type="text"  parsley-type="email_personal" name="data_post[email_personal]"  value="<?php echo set_value('data_post[email_personal]'); ?>"  class="form-control" id="email_personal">
                                                <div class="has-error t_color_red error-mail"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="box_center">
                                            <div class="radio form-check-inline">
                                                <input type="radio" id="cmnd" value="cmnd"  name="identify_type" checked>
                                                <label for="cmnd"> CMND </label>
                                            </div>
                                            <div class="radio form-check-inline">
                                                <input type="radio" id="passport" value="passport" value="<?php echo set_value('passport'); ?>" name="identify_type">
                                                <label for="passport"> Passport </label>
                                            </div>
                                            <div class="clear mt-2"></div>
                                            <div class="form-group row">
                                                <label for="identify_number" class="col-3 col-form-label">Số  <span id="identify-type-text">CMND</span><span class="text-danger">*</span></label>
                                                <div class="col-9">
                                                    <input type="number" required parsley-type="identify_number"  value="<?php echo set_value('data_post[identify_number]'); ?>"  name="data_post[identify_number]" class="form-control" id="identify_number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="identify_time" class="col-3 col-form-label">Ngày cấp<span class="text-danger">*</span></label>
                                                <div class="col-9">
                                                    <input type="text" required parsley-type="identify_time"  value="<?php echo set_value('data_post[identify_time]'); ?>"  name="data_post[identify_time]" class="form-control" id="identify_time" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="21/04/1991">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="identify_place" class="col-3 col-form-label">Nơi cấp<span class="text-danger">*</span></label>
                                                <div class="col-9">
                                                    <input type="text" required parsley-type="identify_place"  value="<?php echo set_value('data_post[identify_place]'); ?>" name="data_post[identify_place]" class="form-control" id="identify_place">
                                                </div>
                                            </div>
                                            <hr/>
                                            <h4 class="header-title mb-3">Thông tin ngân hàng</h4>
                                            <div class="form-group row">
                                                <label for="bankID" class="col-3 col-form-label">Ngân hàng</label>
                                                <div class="col-9">
                                                    <select id="bankID" parsley-type="sex" name="data_post_bank[bankID]" class="form-control">
                                                        <option value="">Chọn loại ngân hàng</option>
                                                        <?php if(isset($banks) && $banks != NULL){ ?>
                                                            <?php foreach ($banks as $key_bank => $val_bank) { ?>
                                                                <option value="<?=$val_bank['id'];?>"><?=$val_bank['name'];?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bank_number" class="col-3 col-form-label">Số TK</label>
                                                <div class="col-9">
                                                    <input type="number" parsley-type="phone" name="data_post_bank[bank_number]"   value="<?php echo set_value('data_post_bank[bank_number]'); ?>" class="form-control" id="bank_number">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bank_name" class="col-3 col-form-label">Tên TK</label>
                                                <div class="col-9">
                                                    <input type="text" parsley-type="phone" name="data_post_bank[bank_name]"  value="<?php echo set_value('data_post_bank[bank_name]'); ?>" class="form-control" id="bank_name">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="bank_branch" class="col-3 col-form-label">Chi nhánh</label>
                                                <div class="col-9">
                                                    <input type="text" parsley-type="phone" name="data_post_bank[bank_branch]"  value="<?php echo set_value('data_post_bank[bank_branch]'); ?>" class="form-control" id="bank_branch">
                                                </div>
                                            </div>
                                            <hr/>
                                            <div class="form-group row">
                                                <label for="status" class="col-3 col-form-label">Trạng thái</label>
                                                <div class="col-9">
                                                    <select id="status" parsley-type="status" name="data_post[status]" class="form-control">
                                                        <option value="1">Đang làm việc</option>
                                                        <option value="2">Đang nghỉ phép</option>
                                                        <option value="3">Tạm đình chỉ</option>
                                                        <option value="4">Đã nghỉ việc</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <div class="upload_card">
                                                <img id="preview" src="public/images/avatar-default.jpg" alt="Ảnh đại diện" title=''/>
                                                <button type="button" class="upload_btn">
                                                    <input type="file" id="avatar" name="avatar" class="imgInp custom-file-input" aria-describedby="avatar" name="avatar">
                                                    Chọn ảnh đại diện
                                                </button>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <div class="upload_card id-front">
                                                        <img id="preview-id-front" src="public/images/id-front.png" alt="your image" title=''/>
                                                        <button type="button" class="upload_btn">
                                                            <input type="file" id="id-front" name="id-front" class="imgInp custom-file-input" aria-describedby="avatar" name="id-front">
                                                            Chọn ảnh mặt trước
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-6">
                                                <div class="form-group row">
                                                    <div class="upload_card id-front">
                                                        <img id="preview-id-back" src="public/images/id-back.png" alt="your image" title=''/>
                                                        <button type="button" class="upload_btn">
                                                            <input type="file" id="id-back" name="id-back" class="imgInp custom-file-input" aria-describedby="avatar" name="id-back">
                                                            Chọn ảnh mặt sau
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- lương & phúc lợi -->
                        <div class="tab-pane" id="mySalary">
                            <div class="clear mt-3"></div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-9 row">
                                        <div class="col-6 form-group row">
                                            <label for="salary_current" class="col-2 col-form-label">Lương</label>
                                            <div class="col-8">
                                                <input type="text" name="data_post_salary[salary]" class="form-control format_salary"  id="salary_current">
                                            </div>
                                        </div>
                                        <div class="col-6 form-group row">
                                            <label for="salary_desire" class="col-3 col-form-label">Lương cơ bản</label>
                                            <div class="col-8">
                                                <input type="text"  name="data_post_salary[salary_basic]" class="form-control format_salary"   id="salary_basic">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                           
                        <!--end lương & phúc lợi -->
                        <div class="tab-pane" id="myJob">
                            <div class="clear mt-3"></div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <label for="standardID" class="col-4 col-form-label">Tình trạng học vấn</label>
                                            <div class="col-8">
                                                <select id="standardID" parsley-type="sex" name="data_post_employeelevel[edu_statusID]" class="form-control">
                                                    <option value="">Chọn tình trạng học vấn</option>
                                                    <?php if(isset($edustatus) && $edustatus != NULL){ ?>
                                                        <?php foreach ($edustatus as $key_edustatus => $val_edustatus) { ?>
                                                            <option  value="<?=$val_edustatus['id'];?>"><?=$val_edustatus['name'];?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="standardID" class="col-4 col-form-label">Trình độ học vấn</label>
                                            <div class="col-8">
                                                <select id="standardID" parsley-type="sex" name="data_post_employeelevel[standardID]" class="form-control">
                                                    <option value="">Chọn trình độ học vấn</option>
                                                    <?php if(isset($standards) && $standards != NULL){ ?>
                                                        <?php foreach ($standards as $key_standard => $val_standard) { ?>
                                                            <option value="<?=$val_standard['id'];?>"><?=$val_standard['name'];?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="schoolsID" class="col-4 col-form-label">Trường học</label>
                                            <div class="col-8">
                                                <select id="schoolsID" parsley-type="sex" name="data_post_employeelevel[schoolsID]" class="form-control">
                                                    <option value="">Chọn trường</option>
                                                    <?php if(isset($schools) && $schools != NULL){ ?>
                                                        <?php foreach ($schools as $key_schools => $val_schools) { ?>
                                                            <option  value="<?=$val_schools['id'];?>"><?=$val_schools['name'];?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="majors" class="col-4 col-form-label">Ngành học</label>
                                            <div class="col-8">
                                                 <select id="majors" parsley-type="sex" name="data_post_employeelevel[majorsID]" class="form-control">
                                                    <option value="">Chọn ngành học</option>
                                                    <?php if(isset($majors) && $majors != NULL){ ?>
                                                        <?php foreach ($majors as $key_majors => $val_majors) { ?>
                                                            <option  value="<?=$val_majors['id'];?>"><?=$val_majors['name'];?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <h4 class="header-title float-left">Tóm tắt quá trình học tập của bạn</h4>
                                        <div class="float-right">
                                            <a href="javascript:void(0)" onclick="addFormStandard()" class="btn btn-blue btn-sm"><i class="icon-plus"></i> Thêm nội dung tóm tắt</a>
                                        </div>
                                        <div class="clear"></div>
                                       
                                        <div class="box_des_standard mt-3 remove_parent">
                                            <hr/>
                                            <a href="javascript:void(0)" class="x__item_close">
                                                <i class="fas fa-times"></i>   
                                            </a>
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Từ ngày</label>
                                                        <div class="col-9">
                                                            <input type="text" name="data_post_standard[date_from][]" class="form-control" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="01/01/2018">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label class="col-3 col-form-label">Đến ngày</label>
                                                        <div class="col-9">
                                                            <input type="text" name="data_post_standard[date_to][]" class="form-control" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="01/01/2020">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <label class="col-12 col-form-label">Tóm tắt nội dung</label>
                                                        <div class="col-12">
                                                            <textarea rows="5" name="data_post_standard[content][]" class="form-control"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- add standard -->
                                        <div id="loadStandard"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="workHistory">
                            <div class="clear mt-3"></div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group row">
                                            <label for="salary_current" class="col-5 col-form-label">Mức lương hiện tại</label>
                                            <div class="col-7">
                                                <input type="text" parsley-type="salary_current" name="data_post[salary_current]" class="form-control format_salary" id="salary_current_datapost">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="salary_desire" class="col-5 col-form-label">Mức lương mong muốn</label>
                                            <div class="col-7">
                                                <input type="text" parsley-type="salary_desire" name="data_post[salary_desire]" class="form-control format_salary" id="salary_desire">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-8">
                                        <h4 class="header-title float-left">Tóm tắt quá trình làm việc của bạn</h4>
                                        <div class="float-right">
                                            <a href="javascript:void(0)" onclick="addFormHistoryWork()" class="btn btn-blue btn-sm"><i class="icon-plus"></i> Thêm nội dung tóm tắt</a>
                                        </div>
                                        <div class="clear"></div>
                                       
                                        <div class="box_des_standard mt-4  remove_parent histoeywork_org">
                                             <hr/>
                                             <a href="javascript:void(0)" class="x__item_close">
                                                <i class="fas fa-times"></i>   
                                            </a>
                                            <div class="row">
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label for="date_from" class="col-3 col-form-label">Từ ngày</label>
                                                        <div class="col-9">
                                                            <input type="text" parsley-type="phone" name="data_post_work[date_from][]" class="form-control" id="date_from" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="01/01/2018">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label for="date_to" class="col-3 col-form-label">Đến ngày</label>
                                                        <div class="col-9">
                                                            <input type="text" parsley-type="phone" name="data_post_work[date_to][]" class="form-control" id="date_to" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="01/01/2020">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label for="work_unit" class="col-3 col-form-label">ĐV công tác</label>
                                                        <div class="col-9">
                                                            <input type="text" parsley-type="phone" name="data_post_work[work_unit][]" class="form-control" id="work_unit">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label for="bussinesstypeID" class="col-3 col-form-label">Loại hình Cty</label>
                                                        <div class="col-9">
                                                            <select id="bussinesstypeID" parsley-type="sex" name="data_post_work[bussinesstypeID][]" class="form-control">
                                                                <option value="">Chọn loại hình công ty</option>
                                                                <?php if(isset($bussinesstypes) && $bussinesstypes != NULL){ ?>
                                                                    <?php foreach ($bussinesstypes as $key_bussinesstypes => $val_bussinesstypes) { ?>
                                                                        <option value="<?=$val_bussinesstypes['id'];?>"><?=$val_bussinesstypes['name'];?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label for="position" class="col-3 col-form-label">Chức vụ đảm nhiệm</label>
                                                        <div class="col-9">
                                                            <input type="text" parsley-type="phone" name="data_post_work[position][]" class="form-control" id="position">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-6">
                                                    <div class="form-group row">
                                                        <label for="concurrently" class="col-3 col-form-label">Chức vụ kiêm nhiệm</label>
                                                        <div class="col-9">
                                                            <input type="text" parsley-type="phone" name="data_post_work[concurrently][]" class="form-control" id="concurrently">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <label for="description" class="col-12 col-form-label">Mô tả công việc</label>
                                                        <div class="col-12">
                                                            <textarea parsley-type="phone" rows="5" name="data_post_work[description][]" class="form-control" id="description"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- add work history -->
                                        <div id="loadHistoryWork"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Thông tin người thân -->
                        <div class="tab-pane" id="infoRelative">
                            <div class="clear mt-3"></div>
                            <div class="col-12">
                                <div class="box_btn mb-0 float-right">
                                    <a href="javascript:void(0)" onclick="addFormInfoRelative()" class="btn btn-blue btn-sm"><i class="icon-plus"></i> Thêm thông tin người thân </a>
                                </div>
                                <div class="clear"></div>
                                <div class="remove_parent bl_relative">
                                    <a href="javascript:void(0)" class="x__item_close">
                                        <i class="fas fa-times"></i>   
                                    </a>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="form-group row">
                                                <label for="fullname_relative" class="col-3 col-form-label">Họ & tên</label>
                                                <div class="col-9">
                                                    <input type="text" parsley-type="fullname_relative" name="data_post_relative[fullname][]" class="form-control" id="fullname_relative">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group row">
                                                <label for="relationship_relative" class="col-6 col-form-label">Mối quan hệ</label>
                                                <div class="col-6">
                                                    <input type="text" parsley-type="relationship_relative" name="data_post_relative[relationship][]" class="form-control" id="relationship_relative">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group row">
                                                <label for="phone_relative" class="col-3 col-form-label">SĐT</label>
                                                <div class="col-9">
                                                    <input type="text" parsley-type="phone_relative" name="data_post_relative[phone][]"  pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==11) return false;"  class="form-control" id="phone_relative">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-5">
                                            <div class="form-group row">
                                                <label for="address_relative" class="col-2 col-form-label">Địa chỉ</label>
                                                <div class="col-10">
                                                    <input type="text" parsley-type="address_relative" name="data_post_relative[address][]" class="form-control" id="address_relative">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                        
                                <div id="loadInfoRelative"></div>
                            </div>
                        </div> 
                        <div class="tab-pane" id="infoRelative">
                        </div>
                        <div class="tab-pane" id="documentFile">
                            <div class="box__build mt-5 mb-5 text-center">
                                <img width="550" height="auto" src="public/images/building.jpg" alt="">
                                <h3 class="mt-4">Tính năng đang được xây dựng. Xin vui lòng quay lại sau.</h3>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="box__tools text-center">
                        <button class="btn btn-blue waves-effect waves-light mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
    // remove add item
    $(document).on("click", ".x__item_close", function() {
        $(this).closest(".remove_parent").remove();
    });

    $(document).on("keyup",".format_salary",function(){
        let elmentID = $(this).attr('id');
        let elementValue = $(this).val();
        $("#"+elmentID).val(My_Numberformat(elementValue));
    });
    // foramt currency
    function My_Numberformat(calc) {
        let regex = /[,~!@#$%^&*(){}.<>]/;
        let checkChracter = regex.test(calc);
        if(regex.test(calc) == true)
        {
         calc = calc.replaceAll(/[.,]/ig, '');
        }
     
        calc = parseInt(calc);
        calc = calc.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
        calc = calc.replace(/[.]/g, ',');
        calc = calc.replace(/[.]/g, ',');
        calc = calc.replace(/ VND/g, '');
        calc = calc.replace(/VND/g, '');
        calc = calc.replace(/ ₫/g, '');
        calc = calc.replace(/₫/g, '');
        calc = calc.replace(/\s+/g, '');
        return calc;
    }
    // validation 
    function checkPhone()
    {
        let flag =  true;
        let phone = $("#phone").val();
        let lengthPhone = phone.length;
        
        let phoneno = /[,~!@#$%^&*(){}.<>]/;
        let rgxPhone = phoneno.test(phone);
        
        if(phone.length < 9 || phone.length > 10 || phone == ""  || rgxPhone == true  )
        {
            
            flag = false;
        }
        return flag
    }
    function checkEmail()
    {
        let flag =  true;
        let email = $("#email").val();
        
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email) == false || email == "" )
        {
            
            flag =  false;
        }
           return flag; 
    }
    function SubmitForm()
    {
        let validatePhone = checkPhone();
        let validateMail = checkEmail();
        if(validatePhone == false && validateMail == false )
        {
            $(".error-phone").text("Số điện thoại không đúng định dạng");
            $(".error-mail").text("Email không đúng định dạng");
            return false;
        }
        else  if(validatePhone == false )
        {
            $(".error-phone").text("Số điện thoại không đúng định dạng");
            return false;
        }
        else if(validateMail == false )
        {
            
            $(".error-mail").text("Email không đúng định dạng");
            return false;
        }
        return true;
    }

    $(document).ready(function(){
        $('[data-toggle="input-mask"]').each(function (idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {'reverse': reverse});
            else
                $(obj).mask(maskFormat);
        });
        //bắt sự kiện chọn loại CMND/Passport
        $('#myForm input').on('change', function() {
            var identify_type = $('input[name=identify_type]:checked', '#myForm').val(); 
            if(identify_type == 'cmnd'){
                $('#identify-type-text').html('CMND');
            }else{
                $('#identify-type-text').html('Passport');
            }
        });
        // block tabs reload
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if(activeTab){
            $('#myTab a[href="' + activeTab + '"]').tab('show');
        }
    });
    //input avatar
    $("#avatar").change(function(event) {  
        readURL(this);    
    });
    function readURL(input) {    
        if (input.files && input.files[0]) {   
            var reader = new FileReader();
            var filename = $("#avatar").val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
                $('#preview').hide();
                $('#preview').fadeIn(500);      
                // $('.custom-file-label').text(filename);             
            }
            reader.readAsDataURL(input.files[0]);    
        } 
    }
    //input id-front
    $("#id-front").change(function(event) {  
        readURLIDFRONT(this);    
    });
    function readURLIDFRONT(input) {    
        if (input.files && input.files[0]) {   
            var reader = new FileReader();
            var filename = $("#id-front").val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                $('#preview-id-front').attr('src', e.target.result);
                $('#preview-id-front').hide();
                $('#preview-id-front').fadeIn(500);      
                // $('.custom-file-label').text(filename);             
            }
            reader.readAsDataURL(input.files[0]);    
        } 
    }
    //input id-back
    $("#id-back").change(function(event) {  
        readURLIDBACK(this);    
    });
    function readURLIDBACK(input) {    
        if (input.files && input.files[0]) {   
            var reader = new FileReader();
            var filename = $("#id-back").val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                $('#preview-id-back').attr('src', e.target.result);
                $('#preview-id-back').hide();
                $('#preview-id-back').fadeIn(500);      
                // $('.custom-file-label').text(filename);             
            }
            reader.readAsDataURL(input.files[0]);    
        } 
    }
    //thêm nội dung tóm tắt quá trình học tập
    function addFormStandard() {
        $.ajax
        ({
            url: "cpanel/ajax/addFormStandard",
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadStandard').append(data);
                }
            }
        });
    }
    //thêm nội dung tóm tắt quá trình làm việc
    function addFormHistoryWork() {
        $.ajax
        ({
            url: "cpanel/ajax/addFormHistoryWork",
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadHistoryWork').append(data);
                }
            }
        });
    }
    //thêm thông tin người thân
    function addFormInfoRelative() {
        $.ajax
        ({
            url: "cpanel/ajax/addFormInfoRelative",
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadInfoRelative').append(data);
                }
            }
        });
    }
</script>