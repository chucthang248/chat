<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title">Cơ sở dữ liệu</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row" id="listemployee">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-12 d-flex t_justify_sb align-items-center">
                        <div class="left_box">
                            <h4>Danh sách nhân sự: <?= $count ?></h4>
                        </div>
                        <div class="right_box">
                            <form action="">
                                <div class="d-flex">
                                    <button type="button" onclick="import_excel()" class="btn btn-blue btn_search_employee waves-effect waves-light mr-2"><i class="fas fa-file-import"></i> Nhập file</button>
                                    <button type="button" onclick="export_excel()" class="btn btn-blue btn_search_employee waves-effect waves-light mr-2"><i class="fas fa-file-export"></i> Xuất file</button>
                                    <div class="box__search d-flex">
                                        <input type="text" parsley-type="search" placeholder="Tìm kiếm nhân sự" name="data_filter[search]" class="form-control " id="search">
                                        <i class="fas fa-search ic_search_inside"></i>
                                    </div>
                                    <button type="button" onclick="btnSearch()" class="btn btn-blue btn_search_employee waves-effect waves-light ml-2"><i class="fas fa-search"></i> Tìm kiếm</button>
                                    <?php /*<select name="data_filter[select]" class="ml-2 width_150 form-control" id="">
                                        <option value="">Đang làm việc</option>
                                        <option value="">Đang nghỉ phép</option>
                                    </select> */?>
                                    <select onchange="chosseAction(this)" name="data_filter[select]" id="action_change" class="ml-2 width_150 form-control" id="">
                                        <option value="0">Chọn tác vụ</option>
                                        <option value="update">Sửa</option>
                                        <option value="delete">Xóa</option>
                                        <option value="detail">Xem chi tiết</option>
                                        <option value="export">Xuất thẻ</option>
                                    </select>
                                    <a href="<?= CPANEL . $control ?>/add" class="btn btn-blue waves-effect waves-light ml-2"><i class="icon-plus"></i>
                                        Thêm mới</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div id="loadTable" class="mt-3">
                    <div class="table-scroll mb-2" id="table-scroll">
                        <div class="table-wrap">
                            <table class=" main-table table-striped table-hover dt-responsive" style="border-collapse: collapse; border-spacing: 0; width:2000px;">
                                <thead>
                                    <tr>
                                        <th class="text-center fixed-side">
                                            <div class="checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" id="checkAll" name="publish" value="1" data-control="<?php echo $control; ?>">
                                                <label class="mr-bottom_mn4"></label>
                                            </div>
                                        </th>
                                        <th class="justify-content-center align-item-center fixed-side">Nhân sự</th>
                                        <th>Mã</th>
                                        <th class="text-center" width="100">Trạng thái</th>
                                        <th class="text-center">Thâm niên</th>
                                        <th class="text-center" width="120">Lương</th>
                                        <th class="text-center" width="150">Lương cơ bản</th>
                                        <th class="text-center" width="150">Chức danh</th>
                                        <th class="text-center" width="150">Văn phòng</th>
                                        <th class="text-center" width="150">Hợp đồng</th>
                                        <th class="text-center" width="150">Điện thoại</th>
                                        <th>Email</th>
                                        <th>Ngày sinh</th>
                                        <th>Giới tính</th>
                                        <th class="text-center" width="150">Ngày bắt đầu</th>
                                    </tr>
                                </thead>
                                <?php if (isset($datas) && $datas != NULL) { ?>
                                    <tbody>
                                        <?php foreach ($datas as $key => $val) {
                                            $avatar = 'upload/employee/' . $val['code'] . '/' . $val['avatar'];  ?>
                                            <tr class="tr<?= $val['id']; ?>">
                                                <td class="text-center fixed-side">
                                                    <div class="checkbox checkbox-success checkbox-single mt-2">
                                                        <input type="checkbox" class="input_checked_single" value="<?= $val['id']; ?>" name="publish" data-control="<?php echo $control; ?>">
                                                        <label></label>
                                                    </div>
                                                </td>
                                                <td class="t_color_black width_fixed_320 fw-bold fixed-side">
                                                    <?php if (file_exists($avatar) && $val['avatar'] != '') { ?>
                                                        <img src="<?= $avatar; ?>" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="40" height="40">
                                                    <?php } else { ?>
                                                        <img src="public/images/avatar-default.jpg" class="img-thumbnail rounded-circle mr-1" alt="avatar" width="30">
                                                    <?php } ?>
                                                    <a href="cpanel/auths/profile/<?= $val['id'] ?>" class="text-blue"><?php echo $val['fullname']; ?></a>
                                                    <a href="javascript:void(0)" onclick="viewEmloyeeInfoDetail(<?= $val['id']; ?>)" class="text-blue float-right mt-1"><i class="icon-eye font-20"></i></a>
                                                </td>
                                                <td class="t_color_green fw-bold">
                                                    <?php echo $val['code']; ?>
                                                </td>
                                                <td class="text-center">
                                                    <div class="round_status bg_status_1 round_status_color_<?= $val['status']; ?> "></div>
                                                </td>
                                                <td width="200" class="text-center">
                                                    <?= $val['startwork_name'] ?>
                                                </td>
                                                <td class="t_color_black fw-bold">
                                                    <?php echo $val['salary']['salary']; ?>
                                                </td>
                                                <td class="t_color_black fw-bold">
                                                    <?php echo $val['salary']['salary_basic']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $val['position_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $val['branch_name']; ?>
                                                </td>
                                                <td>
                                                    <?php echo $val['contract_name']; ?>
                                                </td>
                                                <td><?= $val['phone'] ?></td>
                                                <td><?= $val['email'] ?></td>
                                                <td><?= $val['birthday'] != '0000-00-00' && $val['birthday'] != '1970-01-01' ? date('d/m/Y', strtotime($val['birthday'])) : '' ?></td>
                                                <td>
                                                    <?= $val['sex'] == 1 ? "Nam" : "Nữ" ?>
                                                </td>
                                                <td><?= implode("/", array_reverse(explode("-", $val['startwork'])));  ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                <?php } ?>
                            </table>
                        </div>
                    </div>

                    <!-- pagination -->
                    <div class="wrap___flex d-flex t_justify_end ">
                        <div class="ChangeRow">
                            <span>Hiển thị: </span>
                            <select class="select_row ml-2" id="select_row">
                                <option <?= $per_page == 5 ? "selected" : "" ?> value="5">5</option>
                                <option <?= $per_page == 10 ? "selected" : "" ?> value="10">10</option>
                                <option <?= $per_page == 20 ? "selected" : "" ?> value="20">20</option>
                                <option <?= $per_page == 30 ? "selected" : "" ?> value="30">30</option>
                                <option <?= $per_page == 40 ? "selected" : "" ?> value="30">40</option>
                                <option <?= $per_page == 50 ? "selected" : "" ?> value="30">50</option>
                                <option <?= $per_page == 60 ? "selected" : "" ?> value="30">60</option>
                                <option <?= $per_page == 100 ? "selected" : "" ?> value="30">100</option>
                            </select>
                        </div>
                        <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                            <?php echo $my_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->

</div> <!-- end container-fluid -->

<!-- Xem nhanh thông tin nhân sự -->
<a href="" id="excel_dowload" download=""> </a>
<div id="loadEmloyeeInfoDetail"></div>
<!-- Xem nhanh thông tin nhân sự END -->
<script type="text/javascript">
    $(document).ready(function() {
        $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
    });

    // thay đổi số lượng hàng
    let row = "";
    $(document).on("change", "#select_row", function() {
        row = this.value;
        let data = {
            "row": row,
            "type_pageload": "ajax"
        }
        let url = "<?= base_url() . 'cpanel/employee/pagination' ?>";
        CallAjax(data, url);
        event.preventDefault();

    });
    // xuất file excel
    $(document).ready(function() {
        $("#search").val("<?= $key_word ?>");
    });
    let globalFileName = "";

    function export_excel() {
        $.ajax({
            url: "cpanel/employee/export_excel",
            type: 'POST',
            data: {
                text: "test"
            },
            success: function(res) {
                let data = JSON.parse(res);
                let url = "<?= base_url() ?>" + "public/" + data.filename;
                $("#excel_dowload").attr("href", url);
                $("#excel_dowload").attr("download", data.filename);
                globalFileName = data.filename;
                let gethRef = $("#excel_dowload").attr("href");
                if (gethRef != "") {
                    window.location = gethRef;
                }
                // request remove file 
                $.ajax({
                    url: "cpanel/employee/delete_export_excel",
                    type: 'POST',
                    data: {
                        filename: data.filename
                    },
                    success: function(res) {}
                });
            }

        });
    }

    // seach 
    function btnSearch() {
        let url = "<?= base_url() . 'cpanel/employee/search' ?>";
        // history.replaceState(null, null, gethref);
        let data = {
            "key_word": $("#search").val(),
            "type_pageload": "ajax",
            "row": row
        }
        
        CallAjax(data, url);
    }
    // PAGINATION
    $(document).on('click', '.pagination .forClick a', function() {
        let checkKeyword = $("#search").val();
        $("#search").val(checkKeyword);
        let url = "<?= base_url() . 'cpanel/employee/pagination' ?>";
        // nếu input tìm kiếm có nội dung hoặc url có giá trị vừa tìm kiếm thì đổi URL
        if (checkKeyword != "") {
            url = "<?= base_url() . 'cpanel/employee/search' ?>";
        }
        let gethref = $(this).attr('href');
        let page = $(this).attr('data-ci-pagination-page');
        //history.replaceState(null, null, gethref);
        let data = {
            "key_word": checkKeyword,
            "page": page,
            "row": row,
            "type_pageload": "ajax"
        }
        CallAjax(data, url);
        event.preventDefault();
    });

    function CallAjax(data, url) {
        
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res) {
                
                $(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
                $("#loadTable").html(res);
            }
        });
    }
    // END PAGINATION
    $(document).ready(function() {
        $("#action_change").val(0);
    });

    $(document).on("change", ".input_checked_single", function() {
        // $('.input_checked_single').not(this).prop('checked', false);
        // $(this).prop('checked', this.checked);
    })
    //bắt sự kiện chọn tác vụ
    function chosseAction(action) {
        let arrChecked = [];
        let id = "";
        let checkedCheckbox = document.querySelectorAll(".input_checked_single");
        $(checkedCheckbox).each((key, obj, i) => {
            if (obj.checked == true) {
                arrChecked.push(obj.value);
                id = obj.value;
            }
        });
        action = action.value;
        if (action == 'detail') {
            window.location.href = "<?= CPANEL ?>/auths/profile/" + id;
        }
        if (id != "") {
            if (action == 'update') {
                window.location.href = "<?= CPANEL . $control ?>/edit/" + id;
            }
            if (action == 'delete') {
                Swal.fire({
                    title: "Bạn có chắc chắn muốn xóa không?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#3085d6",
                    cancelButtonColor: "#d33",
                    confirmButtonText: "Đồng ý"
                }).then(function(result) {
                    if (result.value) {
                        $('.sel_tools' + id).parent().parent().fadeOut();
                        if (id != '') {
                            $.ajax({
                                method: "POST",
                                url: "cpanel/employee/delete",
                                data: {
                                    id: id
                                },
                                success: function(data) {
                                    if (data != "") {
                                        let parseData = JSON.parse(data);
                                        
                                        if (parseData.type == "error") {
                                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                                        }
                                    } else {
                                        Swal.fire("Xóa thành công!", "", "success");
                                        $(".tr" + id).remove();
                                    }
                                    // $("#loadTable").html(data);
                                }
                            });
                        }

                    }
                });
            }
            if (action == 'create_pass') {
                $.ajax({
                    method: "POST",
                    url: "cpanel/auths/createPass",
                    data: {
                        id: id
                    },
                    dataType: "html",
                    success: function(data) {
                        if (data) {
                            $('#loadNotifyAjax').html(data);
                        }
                    }
                });
            }
            if (action == 'export') {
                if (arrChecked.length > 1) {
                    id = arrChecked.join();
                }
                window.location.href = "<?= CPANEL . $control ?>/export/" + id;
            }
        }
        if (id == "" && action != 'add') {
            Swal.fire({
                title: "Bạn chưa chọn nhân sự nào ?",
                type: "warning",
                showCancelButton: true,
                cancelButtonColor: "#d33",
            }).then(function(result) {
                if (result.value) {
                    $('.sel_tools' + id).parent().parent().fadeOut();
                }
            });
        }
    }

    function viewEmloyeeInfoDetail(id) {
        $.ajax({
            method: "POST",
            url: "cpanel/employee/viewEmloyeeInfoDetail",
            data: {
                id: id
            },
             dataType: "html",
             //dataType: "JSON",
            success: function(data) {   
                
                if (data) {
                    $('#loadEmloyeeInfoDetail').html(data);
                }
            }
        });
    }
</script>