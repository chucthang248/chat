<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Archivo+Black&display=swap" rel="stylesheet">
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
    <div id="box-notify" class="jq-toast-wrap bottom-right">
        <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
            <span class="jq-toast-loader jq-toast-loaded"
                style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
            <h2 class="jq-toast-heading">Thông báo!</h2>
            <?php echo $message_flashdata['message']; ?>
        </div>
    </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="javascript:void(0)" id="download_pdf" type="button"
                                class="btn btn-success waves-effect waves-light">
                                <i class="fas fa-download"></i> Tải về
                            </a>
                        </div>
                    </div>
                </div>
              
                <div id="export_pdf" >
                    <input type="hidden" id="one_file" value="<?=$data[0]['code']; ?>">
                    <?php foreach($data as $key_data => $val_data){ ?>
                        <div class="row mt-4 justify-content-center multiple_file">
                            <div class="fixed_w_420 first_card">
                                <div class="box__card left__box_card">
                                    <div class="card__top">
                                        <div class="box__lineheight">
                                            <h2 class="shop_name">HƯNG THỊNH</h2>
                                            <div class="shop_desc">Est.1989</div>
                                        </div>
                                    </div>
                                    <div class="wrap__middle">
                                        <div class="card__middle_left align-items-center  justify-content-center">
                                            <div class="above_avatar d-flex t_flex_derection_col justify-content-center align-items-center">
                                                <div class="box__avatar_main">
                                                    <?php  $avatar = $path_dir.$val_data['code'].'/'.$val_data['avatar'];  ?>
                                                    <img   src="<?=file_exists($avatar) && $val_data['avatar'] != ''?$avatar:"public/images/avatar-default.jpg"; ?>">
                                                </div>
                                                <?php if($val_data['fullname'] != "")
                                                        { 
                                                            $val_data_name = $val_data['fullname'];
                                                        } 
                                                        else{  $val_data_name = TEXT_UPDATE;  
                                                                $val_data_style = 'style="font-size:22px;"'; }  ?>
                                                <div class="staff_name mt-2" <?=$val_data_style?>><?=$val_data_name?></div>
                                                <div class="staff_position  mt-1">Nhân viên kinh doanh</div>
                                            </div>
                                        </div>
                                        <div class=" card_above"> </div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="fixed_w_420 after_card">
                                <div class="box__card">
                                    <div class="card__top">
                                        <div class="box__lineheight">
                                            <h2 class="staff__title">STAFF</h2>
                                            <div class="shop__desc_top">
                                                <p>© 2021 Giày Hưng Thịnh. All Rights Reserved.</p>
                                                <p>Employee’s Hotline (đường dây nóng nhân sự): +84 898 548 980</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card__middle">
                                        <div class=" t-relative">
                                            <div class="box___item">
                                                <div class="row">
                                                    <div class="col-md-2 col-2">
                                                        <div class="left__box">
                                                            <img class="ic__home" src="public/images/export_icon/1.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10 col-10">
                                                        <div class="right__box">
                                                            <?=$val_data['temporary_address'] != ""? $val_data['temporary_address'] :TEXT_UPDATE ;?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2  col-2">
                                                        <div class="left__box">
                                                            <img class="ic__phone" src="public/images/export_icon/2.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10 col-10">
                                                        <div class="right__box">
                                                            <?=$val_data['phone'] != "" ? $val_data['phone'] :TEXT_UPDATE;  ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2  col-2">
                                                        <div class="left__box">
                                                             <img class="ic__mail" src="public/images/export_icon/3.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-10 col-10">
                                                        <div class="right__box">
                                                            <?=$val_data['email'] != "" ?$val_data['email'] :TEXT_UPDATE ;?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-2  col-2">
                                                        <div class="left__box">
                                                        <img class="ic__user" src="public/images/export_icon/4.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8 col-10">
                                                        <div class="right__box">
                                                        <?php if($val_data['relatives'] != NULL ) { ?>
                                                            <?=$val_data['relatives']['relationship'].": ".$val_data['relatives']['fullname']."\n sdt: ".$val_data['relatives']['phone'] ?>
                                                        <?php  }else{ echo TEXT_UPDATE;}  ?>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="d-flex">
                                                    <div class="qrcode"> <?php if($val_data['qrcode'] != ""){ ?> <img width="50" height="50" src=" <?php echo $val_data['qrcode']; ?>" alt=""><?php } ?></div>
                                                </div>
                                            </div> 
                                        </div>
                                        <div class="fixed__height_168">
                                            <div class="d-flex content_absolute">
                                                <div class="col-md-1 col-2 d-flex align-items-end">
                                                    <div class="shop_id_card">
                                                        <div class="id_vertical">
                                                            <?php foreach($val_data['code_split'] as $key_code => $val_code){ ?>   
                                                                <?php echo "<p class='main_id_staff'>".$val_code."</p>"; ?>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="id_normal"> ID</div> 
                                                    </div>
                                                </div>
                                                <div class="col-md-11 col-10">
                                                    <div class="shop_desc_card mt-2">
                                                        <div class="en_text">
                                                            <div class="inside_en_text">
                                                                This card is the property of Hung Thinh Inc.
                                                            </div>
                                                            
                                                            <div class="inside_en_text">
                                                                    Use of this card is subject to the company policy.
                                                            </div>
                                                            <div class="inside_en_text">
                                                                    Not valid without authorized signature.
                                                            </div>
                                                        </div>
                                                        <div class="vn_text">
                                                            <div class="inside_vn_text">
                                                                    Thẻ này là tài sản của Hệ Thống Cửa Hàng Hưng Thịnh.
                                                            </div>
                                                            <div class="inside_vn_text">
                                                                Việc sử dụng thẻ này là sự đồng thuận với quy định
                                                                của công ty dưới bất cứ hình thức nào.
                                                            </div>
                                                            <div class="inside_vn_text">
                                                                Thẻ không có giá trị nếu không có chữ ký.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card__footer">
                                    <img src="public/assets/images/logo-sm.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="hr-border"></div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.4/jspdf.min.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
<script type="text/javascript">

$("#download_pdf").click(function() {
    let MultipleFile =  parseInt(document.querySelectorAll(".multiple_file").length) ;
    let fileName = "staff.pdf";
    if(MultipleFile <= 1)
    {
        fileName =  $("#one_file").val();
    }
    let HTML_Width = $("#export_pdf").width();
    let HTML_Height = $("#export_pdf").height();
    let top_left_margin = 15;
    let PDF_Width = HTML_Width + (top_left_margin * 2);
    let PDF_Height = (PDF_Width * 1.5) + (top_left_margin * 2);
    let canvas_image_width = HTML_Width;
    let canvas_image_height = HTML_Height;
    let totalPDFPages = Math.ceil(HTML_Height / PDF_Height) - 1;
    html2canvas($("#export_pdf")[0], {
        allowTaint: true,
        useCORS: true
    }).then(function(canvas) {
        canvas.getContext('2d');
        let imgData = canvas.toDataURL("image/jpeg", 1.0); // đường dẫn hình
        let pdf = new jsPDF('p', 'pt', [PDF_Width, PDF_Height]);
        pdf.addImage(imgData, 'JPG', top_left_margin, top_left_margin, canvas_image_width, canvas_image_height);
        for (let i = 1; i <= totalPDFPages; i++) {
            pdf.addPage(PDF_Width, PDF_Height);
            pdf.addImage(imgData, 'JPG', top_left_margin, -(PDF_Height * i) + (top_left_margin * 4), canvas_image_width, canvas_image_height);
        }
        pdf.save(fileName);
    });
});
//bắt sự kiện chọn tác vụ
function chosseAction(action, id) {
    action = action.value;
    if (action == 'update') {
        window.location.href = "<?=CPANEL.$control?>/edit/" + id;
    }
    if (action == 'export') {
        window.location.href = "<?=CPANEL.$control?>/export/" + id;
    }
}
</script>