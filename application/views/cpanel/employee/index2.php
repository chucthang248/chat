<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <!-- notify --> 
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End --> 

    <div id="loadNotifyAjax"></div>
    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="<?=CPANEL.$control?>/add" type="button" class="btn btn-success waves-effect waves-light">
                                <i class="icon-plus mr-1"></i> Thêm mới
                            </a>
                            <a href="<?=CPANEL.$control?>" type="button" class="btn btn-linkedin waves-effect waves-light">
                                <i class="icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr/>
                <table class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Mã NV</th>
                            <th>Nhân viên</th>
                            <th>Chức vụ</th>
                            <th class="text-center">Ngày tạo</th>
                            <th class="text-center">Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if(isset($datas) && $datas != NULL){ ?>
                    <tbody>
                        <?php foreach ($datas as $key => $val) {
                            $avatar = 'upload/employee/'.$val['code'].'/'.$val['avatar']; 
                        ?>
                        <tr id="tr<?=$val['id'];?>">
                            <td width="100"><?php echo $val['code'];?></td>
                            <td width="250">
                                <?php if(file_exists($avatar) && $val['avatar'] != ''){ ?>
                                    <img src="<?=$avatar;?>" class="img-thumbnail rounded-circle" alt="avatar" width="50">
                                <?php }else{ ?>
                                    <img src="public/images/avatar-default.jpg" class="img-thumbnail rounded-circle" alt="avatar" width="50">
                                <?php } ?>
                                <?php echo $val['fullname'];?>
                            </td>
                            <td>Đang cập nhật</td>
                            <td class="text-center"><?php echo date('d/m/Y',strtotime($val['created_at']));?></td>
                            <td class="text-center">
                                <select class="sel_tools sel_tools<?=$val['id'];?>" onchange="chosseAction(this, <?=$val['id'];?>)">        
                                    <option value="0">Chọn tác vụ</option>
                                    <option value="update">Sửa</option>
                                    <option value="delete">Xóa</option>
                                    <option value="detail">Xem chi tiết</option>
                                    <option value="create_pass">Cấp tài khoản</option>
                                    <option value="detail">Khóa tài khoản</option>
                                    <option value="export">Xuất thẻ</option>
                                </select>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
<script type="text/javascript">
    //bắt sự kiện chọn tác vụ
    function chosseAction(action, id){

        action = action.value;
        if(action == 'update'){
            window.location.href = "<?=CPANEL.$control?>/edit/"+id;
        }
        if(action == 'delete'){
            Swal.fire({
                title: "Bạn có chắc chắn muốn xóa không?",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#3085d6",
                cancelButtonColor: "#d33",
                confirmButtonText: "Đồng ý"
              }).then(function (result) {
                if (result.value) {
                    $('.sel_tools' + id).parent().parent().fadeOut();
                    if(id != '')
                    {
                        $.ajax
                        ({
                            method: "POST",
                            url: "cpanel/employee/delete",
                            data: { id:id},
                        });
                    }
                    Swal.fire("Xóa thành công!", "", "success");
                }
            });
        }
        if(action == 'create_pass'){
            $.ajax
            ({
                method: "POST",
                url: "cpanel/auths/createPass",
                data: { id:id},
                dataType: "html",
                success: function(data){
                    if(data){
                        $('#loadNotifyAjax').html(data);
                    }
                }
            });
        }
        if(action == 'export')
        {
            window.location.href = "<?=CPANEL.$control?>/export/"+id;
        }
    }
</script>
