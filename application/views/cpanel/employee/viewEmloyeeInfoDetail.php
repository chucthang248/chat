<link href="public/assets/customs/emloyee_info_detail.css" rel="stylesheet" type="text/css" />
<div class="box">
    <div class="box-body">
        <div class="title">
            Thông tin nhân sự
            <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
        </div>
        <hr class="hr-xs"/>
        <div class="box-info">
            <div class="row">
                <div class="col-4">
                    <div class="wrap__content ">
                        <div class="box__content text-center">
                            <?php  $avatar = $path_dir.$data_employee['code'].'/'.$data_employee['avatar']; ?>
                            <div class="box__avatar">
                                <img src="<?=file_exists($avatar) && $data_employee['avatar'] != ''?$avatar:"public/images/avatar-default.jpg"; ?>"
                                class="img-thumbnail rounded-circle d-block mx-auto" alt="avatar" width="180" height="180">
                                <div class="box__status"><i class="icon-check"></i></div>
                            </div>
                            <h3 class="staff_name mt-2">
                                <?=$data_employee['fullname'] != NULL?$data_employee['fullname']:TEXT_UPDATE; ?></h3>
                            <div class="staff_position  mt-2"> <?=$data_position_jb['name'] != NULL?$data_position_jb['name']:TEXT_UPDATE; ?></div>
                        </div>
                        <hr>
                        <div class="box__content ">
                            <div class="form-group">
                                <span class="t_inline_block width_40 text-danger"><i class="fas fa-heart"></i></span>
                                <span class="fs-14px fw-500"><?= date('d/m/Y',strtotime($data_employee['birthday'])); ?></span>
                            </div>
                            <div class="form-group">
                                <span class="t_inline_block width_40"><i class="mdi mdi-qrcode-scan"></i></span>
                                <span class="fs-14px fw-500">
                                    <?=$data_employee['code'] != NULL ?$data_employee['code']:TEXT_EMPTY;  ?></span>
                            </div>
                            <div class="form-group">
                                <span class="t_inline_block width_40"><i class="icon-envelope-open"></i></span>
                                <span
                                    class="fs-14px fw-500"><?=$data_employee['email'] != ""?$data_employee['email']:TEXT_EMPTY?></span>
                            </div>
                            <div class="form-group">
                                <span class="t_inline_block width_40"><i class="icon-phone"></i></span>
                                <span
                                    class="fs-14px fw-500"><?=$data_employee['phone'] != ""?$data_employee['phone']:TEXT_EMPTY?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group  ">
                                <label class="t_color_b9b9b9" for="">Họ và tên:</label>
                                <div class="info_name">
                                    <?=$data_employee['fullname'] != ""?$data_employee['fullname']:TEXT_EMPTY?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Giới tính:</label>
                                <div class="info_name"><?=$data_employee['sex'] != 0?"Nam":"Nữ"?></div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Ngày sinh:</label>
                                <div class="info_name">
                                    <?=$data_employee['birthday'] != ""?$data_employee['birthday']:TEXT_EMPTY?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Văn phòng:</label>
                                <div class="info_name"><?=$data_Branch['name'] != ""?$data_Branch['name']:TEXT_EMPTY?></div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Lịch làm việc:</label>
                                <div class="info_name">----</div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Tình trạng hôn nhân:</label>
                                <div class="info_name"><?=$data_employee['marital']?></div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Địa chỉ thường trú:</label>
                                <div class="info_name">
                                    <?=$data_employee['permanent_address'] != ""?$data_employee['permanent_address']:TEXT_EMPTY?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Địa chỉ tạm trú:</label>
                                <div class="info_name">
                                    <?=$data_employee['temporary_address'] != ""?$data_employee['temporary_address']:TEXT_EMPTY?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Phòng ban:</label>
                                <div class="info_name"><?=$careerroadmap['name'] != ""?$careerroadmap['name']:TEXT_EMPTY?></div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Phân loại nhân sự:</label>
                                <div class="info_name"><?=$Classifyemployee['name'] != ""?$Classifyemployee['name']:TEXT_EMPTY?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function closeForm(){
        $('#loadEmloyeeInfoDetail').html('');
    }
</script>