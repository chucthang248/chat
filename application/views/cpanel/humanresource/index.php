<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
    <div id="box-notify" class="jq-toast-wrap bottom-right">
        <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
            <span class="jq-toast-loader jq-toast-loaded"
                style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
            <h2 class="jq-toast-heading">Thông báo!</h2>
            <?php echo $message_flashdata['message']; ?>
        </div>
    </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <form action="">
                    <div  class="form-group col-2 d-flex">
                        <input type="text" readonly="" name="search_date" class="form-control" value="01/01/2021 - 31/01/2021">
                        <button class="btn_searchdate btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-search"></i></button>
                    </div>
                    <div class="table-responsive height__fit_content border_right_1px_eee border_left_1px_eee custom_scrollbar">
                        <table class="width_3200 table table-bordered mb-0">
                            <thead>
                                <tr>
                                    <th>Ngày</th>
                                    <th class="text-center">8h</th>
                                    <th class="text-center">9h</th>
                                    <th class="text-center">10h</th>
                                    <th class="text-center">11h</th>
                                    <th class="text-center">12h</th>
                                    <th class="text-center">13h</th>
                                    <th class="text-center">14h</th>
                                    <th class="text-center">15h</th>
                                    <th class="text-center">16h</th>
                                    <th class="text-center">17h</th>
                                    <th class="text-center">18h</th>
                                    <th class="text-center">19h</th>
                                    <th class="text-center">20h</th>
                                    <th class="text-center">21h</th>
                                    <th class="text-center">22h</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th width="200" scope="row">Thứ 2</th>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <th width="200" scope="row">Thứ 3</th>
                                    <td width="200" class="text-center">

                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 4
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 4
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 5
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <th width="200" scope="row">Thứ 4</th>
                                    <td width="200" class="text-center">

                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 4
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 5
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <th width="200" scope="row">Thứ 5</th>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <th width="200" scope="row">Thứ 6</th>
                                    <td width="200" class="text-center">
                                        
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                    <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 4
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 5
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <th width="200" scope="row">Thứ 7</th>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>

                                <tr>
                                    <th width="200" scope="row">Chủ nhật</th>
                                    <td width="200" class="text-center">
                                        <div class="staff_item d-flex  align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 1
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 2
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                        <div class="staff_item  d-flex align-items-center justify-content-center">
                                            <div class="staff_name">
                                                Nhân viên 3
                                            </div>
                                            <div class="staff_checkbox ml-3 checkbox checkbox-success checkbox-single">
                                                <input type="checkbox" name="publish" value="1"
                                                    data-control="<?php echo $control;?>">
                                                <label class="mr__bt_t4px"></label>
                                            </div>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>
                                    <td width="200" class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single">
                                            <input type="checkbox" name="publish" value="1"
                                                data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
<script>
 $(document).ready(function(){
        $('input[name="search_date"]').daterangepicker();
        let start = moment().subtract(22, 'days');
        let end = moment();
        end.locale('vn');
        $('input[name="search_date"]').daterangepicker({
        startDate: start,
        endDate: end,
        timePicker: true,
        locale: {
        format: 'DD/M/Y',
        customRangeLabel : '',
        daysOfWeek: ['CN', 'T2', 'T3', 'T4', 'T5', 'T6','T7'],
        monthNames: ['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 
       'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'],
        },
        alwaysShowCalendars:true,
        showCustomRangeLabel: false,
        ranges: {
           'Hôm nay': [moment(), moment()],
           'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           '7 ngày trước': [moment().subtract(6, 'days'), moment()],
           '30 ngày trước': [moment().subtract(29, 'days'), moment()],
           'Tháng này': [moment().startOf('month'), moment().endOf('month')],
           'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    });
});
</script>