<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <!-- notify --> 
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End --> 

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="<?=CPANEL.$control?>/add" type="button" class="btn btn-success waves-effect waves-light" >
                                <i class="icon-plus mr-1"></i> Thêm mới
                            </a>
                            <a href="<?=CPANEL.$control?>" type="button" class="btn btn-linkedin waves-effect waves-light">
                                <i class="icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr/>
                <table id="datatable" class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Mã</th>
                            <th>Ngày ký</th>
                            <th>Ngày hiệu lực</th>
                            <th>Ngày hết hạn</th>
                            <th  class="text-center">Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if($datas != NULL){ ?>
                        <tbody>
                            <?php foreach($datas as $key_item => $val_item){ ?>
                                <tr>
                                    <td><?=$val_item['code']?></td>
                                    <td><?=$val_item['signing_date']?></td>
                                    <td><?=$val_item['effective_date']?></td>
                                    <td><?=$val_item['expiration_date']?></td>
                                    <td class="text-center">
                                        <a href="<?=CPANEL.$control.'/edit/'.$val_item['id']?>" class="btn btn-info waves-effect waves-light">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" onclick="del(<?php echo $val_item['id'];?>);" class="btn btn-danger waves-effect waves-light delete<?php echo $val['id'];?>" data-control="<?php echo $control;?>" >
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->