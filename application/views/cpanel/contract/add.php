<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 
    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title mb-4"><?=$title;?></h4>
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Mã hợp đồng<span class="text-danger">*</span></label>
                                <input type="text" name="data_post[code]" class="col-8 form-control"  >
                                <div class="has-error text-danger ml-auto"><?php echo form_error('data_post[code]') ?></div>
                            </div>
                            <div class="form-group row">
                                <label  class="col-4 col-form-label" >Thời hạn hợp đồng<span class="text-danger">*</span></label>
                                <select name="data_post[period]" class="col-8  form-control" >
                                    <option value="0">Thời hạn</option>
                                    <option value="3">3 Tháng</option>
                                    <option value="6">6 Tháng</option>
                                    <option value="9">9 Tháng</option>
                                    <option value="12">12 Tháng</option>
                                    <option value="24">24 Tháng</option>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Ngày bắt đầu công tác<span class="text-danger">*</span></label>
                                <input type="text" required=""  value="<?php echo set_value('data_post[start_date]'); ?>" parsley-type="start_date" class="col-8 form-control" id="start_date" placeholder="22/04/2019" name="data_post[start_date]">
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Lương net<span class="text-danger">*</span></label>
                                <input type="text" required="" parsley-type="salary_net" class="col-8 form-control" id="salary_net"  value="<?php echo set_value('data_post[salary_net]'); ?>"  name="data_post[salary_net]">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Nhân sự<span class="text-danger">*</span></label>
                                <select name="data_post[employeeID]" class="col-8 form-control" >
                                    <option value="0">Chọn nhân sự</option>
                                    <?php if($employees != NULL){ ?>
                                            <?php foreach($employees as $key_employee => $val_employee){ ?>
                                                    <option value="<?=$val_employee['id']?>"><?=$val_employee['fullname']?> </option>
                                            <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Ngày có hiệu lực<span class="text-danger">*</span></label>
                                <input type="text" required="" parsley-type="effective_date" class="col-8 form-control" id="effective_date"  value="<?php echo set_value('data_post[effective_date]'); ?>" placeholder="24/08/2020" name="data_post[effective_date]">
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Vị trí<span class="text-danger">*</span></label>
                                <select name="data_post[positionsID]" class="col-8 form-control" >
                                    <option value="0">Chọn chức vụ</option>
                                    <?php if($position != NULL){ ?>
                                            <?php foreach($position as $key_position => $val_position){ ?>
                                                    <option  value="<?=$val_position['id']?>"><?=$val_position['name']?> </option>
                                            <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Lương gross<span class="text-danger">*</span></label>
                                <input type="text" required="" parsley-type="salary_gross" class="col-8 form-control" id="salary_gross" value="<?php echo set_value('data_post[salary_gross]'); ?>"  name="data_post[salary_gross]">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label  class="col-4 col-form-label">Ngày ký hợp đồng<span class="text-danger">*</span></label>
                                <input type="text" required="" parsley-type="signing_date" class="col-8 form-control" id="signing_date" value="<?php echo set_value('data_post[signing_date]'); ?>" placeholder="21/04/1991" name="data_post[signing_date]">
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Ngày hết hạn hợp đồng<span class="text-danger">*</span></label>
                                <input type="text" required="" parsley-type="expiration_date" class="col-8 form-control" id="expiration_date" value="<?php echo set_value('data_post[expiration_date]'); ?>" placeholder="23/08/2012" name="data_post[expiration_date]">
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Đơn vị làm việc<span class="text-danger">*</span></label>
                                <input type="text" required="" parsley-type="work_unit" class="col-8 form-control" id="work_unit" value="<?php echo set_value('data_post[work_unit]'); ?>"  name="data_post[work_unit]">
                            </div>
                            <div class="form-group row">
                                <label class="col-4 col-form-label">Trích yếu<span class="text-danger">*</span></label>
                                <textarea type="text" required="" parsley-type="quote" class="col-8 form-control" id="quote"  name="data_post[quote]"> <?php echo set_value('data_post[quote]'); ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="box__tools text-left">
                        <button class="btn btn-primary waves-effect waves-light mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
     $('#signing_date, #period, #effective_date, #expiration_date, #start_date').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'dd/mm/yyyy'
        });
</script>