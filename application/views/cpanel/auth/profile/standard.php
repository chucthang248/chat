<div class="panel__content">
    <div class="title_tab">
        <div class="title_tab">
            <div class="row">
                <div class="col-md-5">
                    <h4 class="mb-0">TRÌNH ĐỘ HỌC VẤN</h4>
                    <p class="mb-0">Education background</p>
                </div>
                <?php if($data_index['roleID']['type'] != "error") { ?>
                    <div class="col-md-7 text-right">
                    <a href="javascript:void(0)"  onclick="addFormProgressLearn()" class="text-blue font-weight-bold">Thêm trường | </a>
                        <a href="javascript:void(0)" onclick="addFormEmployeelevel()" onlick="update" class="text-blue font-weight-bold">Cập nhật</a>
                    </div>
                <?php  } ?>
            </div>
        </div>
    </div>
    <hr class="hr-xs"/>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group mb-0">
                <label class="t_color_b9b9b9" for="">Tình trạng học vấn:</label>
                <div class="info_name"><?=$data_employeelevel['edu_status']['name'] != ""? $data_employeelevel['edu_status']['name'] : TEXT_UPDATE; ?> </div>
            </div>
        </div>
        <div class="col-md-3 ">
            <div class="form-group mb-0">
                <label class="t_color_b9b9b9" for="">Trình độ:</label>
                <div class="info_name">
                     <?=$data_employeelevel['standard']['name'] != ""? $data_employeelevel['standard']['name'] : TEXT_UPDATE; ?>
                </div>
            </div>
            
        </div>
        <div class="col-md-3">
            <div class="form-group mb-0">
                <label class="t_color_b9b9b9" for="">Trường học:</label>
                <div class="info_name">
                    <?=$data_employeelevel['schools']['name'] != ""? $data_employeelevel['schools']['name'] :TEXT_UPDATE; ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group mb-0">
                <label class="t_color_b9b9b9" for="">Ngành học:</label>
                <div class="info_name">
                    <?=$data_employeelevel['majors']['name'] != ""? $data_employeelevel['majors']['name'] :TEXT_UPDATE; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="hr-xs"/>
<div class="panel__content mt-3 ml-4">
    <div class="title_tab">
        <div class="title_tab">
            <div class="row">
                <div class="col-md-5">
                    <h5 class="mb-0">QUÁ TRÌNH HỌC TẬP</h5>
                    <p class="mb-0">List of all education histories</p>
                </div>
            </div>
        </div>
    </div>
    <hr>
    <?php if($employees_standards != NULL) {?>
        <?php foreach($employees_standards  as $key_standards => $val_standards) { ?>
            <div class="item_relatives">
                <i class="fas fa-pen icon_edit" onclick="addFormProgressLearnUpdate(<?=$val_standards['id']?>)"></i>
                <div class="row">
                    <div class="col-md-3 d-flex">
                        <div class="form-group mb-0">
                            <label class="t_color_b9b9b9" for="">Từ ngày:</label>
                            <div class="info_name"><?php if($val_standards['date_from'] != ""){ echo  $val_standards['date_from']; }else{echo TEXT_UPDATE;} ?></div>
                        </div>
                    
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-0">
                            <label class="t_color_b9b9b9" for="">Đến ngày:</label>
                            <div class="info_name"><?php if($val_standards['date_to'] != "") { echo $val_standards['date_to']; }else{echo TEXT_UPDATE;} ?></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-0">
                            <label class="t_color_b9b9b9" for="">Tóm tắt nội dung:</label>
                            <div class="info_name"><?php if( $val_standards['content'] != "") { echo $val_standards['content']; }else{echo TEXT_UPDATE;} ?></div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="hr-xs"/>
        <?php } ?>
    <?php }else{ ?>
        <div class="text-left mb-3">
            <p>Đang chờ cập nhật...</p>
        </div>
    <?php } ?>
</div>
<div id="loadForm_employeelevel"></div>
<!-- ProgressLearn    -->
<div id="loadForm_ProgressLearn"></div>
<!-- ProgressLearn    -->
<script>
     function addFormEmployeelevel(){
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormEmployeelevel",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                $('#loadForm_employeelevel').html(data);
            }
        });
    }
    // thêm quá trình học tập
    function addFormProgressLearn()
    {
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormProgressLearn",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                $('#loadForm_ProgressLearn').html(data);
            }
        });
    }
    // cập nhật quá trình học tập
    function addFormProgressLearnUpdate(id)
    {
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormProgressLearnUpdate",
            type: "POST",
            data: {employeeID:employeeID,id:id},
            dataType: "html",
            success: function(data){
                // let check = JSON.parse(data);
                if(data){
                    $('#loadForm_ProgressLearn').html(data);
                }
            }
        });
    }
</script>