<div class="title_tab">
    <div class="row">
        <div class="col-md-5">
            <h4 class="mb-0">THÔNG TIN NGƯỜI THÂN</h4>
            <p class="mb-0">Family, dependants and other contacts</p>
        </div>
        <?php if($data_index['roleID']['type'] != "error") { ?>
            <div class="col-md-7 text-right">
                <a href="javascript:void(0)" onclick="addRelativeInfo()" class="text-blue font-weight-bold">Thêm người liên lạc</a>
            </div>
        <?php  } ?>
        
    </div>
</div>
<!-- <hr> -->
<?php if($data_employee['relative'] != NULL) {?>
    <?php foreach($data_employee['relative'] as $key_relative => $val_relative) { ?>
        <hr/>
        <div class="item_relatives ">
         <i class="fas fa-pen icon_edit"  onclick="addRelativeInfoUpdate(<?=$val_relative['id']?>)"></i>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group  ">
                        <label class="t_color_b9b9b9" for="">Họ và tên:</label>
                        <div class="info_name"><?php if( $val_relative['fullname'] != NULL){ echo  $val_relative['fullname']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group  ">
                        <label class="t_color_b9b9b9" for="">Mối quan hệ:</label>
                        <div class="info_name"><?php if($val_relative['relationship'] != NULL){ echo  $val_relative['relationship']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Số điện thoại:</label>
                        <div class="info_name"><?php if($val_relative['phone'] != NULL) { echo $val_relative['phone']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Địa chỉ:</label>
                        <div class="info_name"><?php if( $val_relative['address'] != NULL) { echo $val_relative['address']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
            </div>
        </div>
       
    <?php } ?>
<?php }else{ ?>
    <div class="text-left mb-3">
        <p>Đang chờ cập nhật...</p>
    </div>
<?php } ?>
<!-- Thêm thông tin người liên lạc -->
<div id="loadFormAddRelativeInfo"></div>
<!-- Thêm thông tin người liên lạc END -->
<script>
    function addRelativeInfo(){
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addRelativeInfo",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormAddRelativeInfo').html(data);
                }
            }
        });
    }
    // cập nhật
    function addRelativeInfoUpdate(id)
    {
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addRelativeInfoUpdate",
            type: "POST",
            data: {employeeID:employeeID,id:id},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormAddRelativeInfo').html(data);
                }
            }
        });   
    }
  
</script>