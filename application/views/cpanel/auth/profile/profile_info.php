<div class="col-11">
    <div class="panel__content">
        <div class="title_tab">
            <div class="row">
                <div class="col-md-6">
                    <h4 class="mb-0">THÔNG TIN CÁ NHÂN</h4>
                    <p class="mb-0">Profile information</p>
                </div>
                <?php  if($data_index['roleID']['type'] != "error"){ ?>
                    <div class="col-md-6 text-right mt-1">
                        <a href="javascript:void(0)" onclick="profileUpdate()" class="text-blue font-weight-bold">Cập nhật thông tin | </a>
                        <a href="javascript:void(0)" onclick="workInfoUpdate()" class="text-blue font-weight-bold">Cập nhật thông tin công việc</a>
                    </div>
                <?php } ?>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group  ">
                    <label class="t_color_b9b9b9" for="">Họ và tên:</label>
                    <div class="info_name">
                        <?=$data_employee['fullname'] != ""?$data_employee['fullname']:TEXT_UPDATE?>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Giới tính:</label>
                    <div class="info_name"><?php if($data_employee['sex'] ==1 ){ echo "Nam"; } elseif($data_employee['sex'] ==2) { echo "Nữ"; }else{echo "Khác"; } ?></div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Ngày sinh:</label>
                    <div class="info_name">
                        <?=$data_employee['birthday'] != ""?$data_employee['birthday']:TEXT_UPDATE?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Tình trạng hôn nhân:</label>
                    <div class="info_name"><?=$data_employee['marital']?></div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Địa chỉ thường trú:</label>
                    <div class="info_name">
                        <?=$data_employee['permanent_address'] != ""?$data_employee['permanent_address']:TEXT_UPDATE?>
                    </div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Địa chỉ tạm trú:</label>
                    <div class="info_name">
                        <?=$data_employee['temporary_address'] != ""?$data_employee['temporary_address']:TEXT_UPDATE?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Ngày bắt đầu:</label>
                    <div class="info_name"><?=$employeeHistoryWork['startwork']!= ""?$employeeHistoryWork['startwork']:TEXT_UPDATE ?></div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Hợp đồng hiện tại:</label>
                    <div class="info_name"><?=$data_employee['contract']['name'] != ""?$data_employee['contract']['name']:TEXT_UPDATE?></div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Chức danh:</label>
                    <div class="info_name"><?=$employeeHistoryWork['job_position_titleName']['name']!= ""?$employeeHistoryWork['job_position_titleName']['name']:TEXT_UPDATE ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Trạng thái làm việc:</label>
                    <?php  
                         $status_active = "";
                         $status = "";
                         if($data_employee['status'] == 1)
                         {
                            $status = "Đang làm việc";
                         }
                         else if($data_employee['status'] == 2)
                         {
                            $status = "Đang nghỉ phép";
                         }
                         else if($data_employee['status'] == 3)
                         {
                            $status = "Tạm đình chỉ";
                         }
                         else if($data_employee['status'] == 4)
                         {
                            $status = "Đã nghỉ việc";
                         }
                        ?>
                    <div class="info_name text_status_<?=$data_employee['status']?>"><?=$status != ""? $status : TEXT_UPDATE?> </div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Email cá nhân:</label>
                    <div class="info_name"><?=$data_employee['email'] != ""?$data_employee['email']:TEXT_UPDATE?></div>
                </div>
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Phân loại nhân sự:</label>
                    <div class="info_name"><?=$employeeHistoryWork['classifyemployeeName']['name']!= ""?$employeeHistoryWork['classifyemployeeName']['name']:TEXT_UPDATE ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Vị trí công việc:</label>
                    <div class="info_name"><?=$jobposition['name'] != NULL?$jobposition['name']:TEXT_UPDATE; ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Phòng ban:</label>
                    <div class="info_name"><?=$orgchart['name']!= ""?$orgchart['name']:TEXT_UPDATE ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Văn phòng:</label>
                    <div class="info_name"><?=$employeeHistoryWork['branchName']['name']!= ""?$employeeHistoryWork['branchName']['name']:TEXT_UPDATE ?></div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Lịch làm việc:</label>
                    <div class="info_name">----</div>
                </div>
            </div>
           
            <div class="col-md-3">
                <div class="form-group mb-2">
                    <label class="t_color_b9b9b9" for="">Ghi chú thêm:</label>
                    <div class="info_name">----</div>
                </div>
            </div>
        </div>
    </div>

    <hr class="hr-border-weight hr-xs"/>
    <div class="panel__content mt-3">
        <?php $this->load->view('cpanel/auth/profile/mybank');?>
    </div>
    <hr class="hr-border-weight"/>
    <?php $this->load->view('cpanel/auth/profile/standard');?>
    <hr class="hr-border-weight mt-4"/>
    <div class="panel__content mt-3">
        <?php $this->load->view('cpanel/auth/profile/myrelatives');?>
    </div>
    <hr class="hr-border-weight mt-4"/>
    <div class="panel__content mt-3">
        <?php $this->load->view('cpanel/auth/profile/tax_insurance');?>
    </div>
    <hr class="hr-border-weight mt-4"/>
    <div class="panel__content mt-3">
        <?php $this->load->view('cpanel/auth/profile/refer_material');?>
    </div>
</div>

<!-- Cập nhật thông tin -->
<div id="loadFormPersonalInfo"></div>
<!-- Cập nhật thông tin END -->

<!-- Cập nhật thông tin công việc -->
<div id="loadFormWorkInfo"></div>
<!-- Cập nhật thông tin công việc END -->
<style>
    .active_status
    {
        color: green;
        font-weight: bold;
    }
    .takeleave_status
    {
        color: #104db1;
        font-weight: bold;
    }
    .suspended_status
    {
        color: #b59d13;
        font-weight: bold;
    }
    .quitjob_status
    {
        color: red;
        font-weight: bold;
    }
</style>
<script>
    function profileUpdate()
    {
       let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormProfileUpdate",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormPersonalInfo').html(data);
                }
            }
        });
    }
    function workInfoUpdate(){
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormWorkInfoUpdate",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormWorkInfo').html(data);
                }
            }
        });
    }

</script>