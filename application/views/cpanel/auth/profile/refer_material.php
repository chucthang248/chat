<div class="title_tab">
    <div class="row">
        <div class="col-md-5">
            <h4 class="mb-0">TÀI LIỆU THAM CHIẾU</h4>
            <p class="mb-0">Reference material</p>
        </div>
    </div>
</div>
<hr>
<?php if($data_employee != NULL) {?>
        <div class="item_relatives ">
         <i class="fas fa-pen icon_edit"  onclick="addFormReferenceMaterial(<?=$data_employee['id']?>)"></i>
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group  ">
                        <label class="t_color_b9b9b9" for="">Số <?=$data_employee['identify_type']=="cmnd"? "CMND" : "Passport"?>:</label>
                        <div class="info_name"><?php if( $data_employee['identify_number'] != NULL){ echo  $data_employee['identify_number']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group  ">
                        <label class="t_color_b9b9b9" for="">Ngày cấp: </label>
                        <div class="info_name"><?php if($data_employee['identify_time'] != NULL){ echo  $data_employee['identify_time']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Nơi cấp:</label>
                        <div class="info_name"><?php if($data_employee['identify_place'] != NULL) { echo $data_employee['identify_place']; }else{echo TEXT_UPDATE;} ?></div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Ảnh mặt trước:</label>
                        <div class="info_img">
                            <img class="fullScreen_img" src="<?=$data_employee['id-front']!="" ?$path_dir.$data_employee['code']."/".$data_employee['id-front']: ""; ?>" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Ảnh mặt sau:</label>
                        <div class="info_img">
                            <img class="fullScreen_img" src="<?=$data_employee['id-back']!="" ?$path_dir.$data_employee['code']."/".$data_employee['id-back']: ""; ?>" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr/>
<?php }else{ ?>
    <div class="text-left mb-3">
        <p>Đang chờ cập nhật...</p>
    </div>
<?php } ?>
<div class="fullImage ">
    <i class="fas fa-times x__close"></i>
    <div class="contain_img"><img src="" id="main_fullscreen" alt=""></div>
    <span class="overplay x__close"></span>
    <div class="list_img"> </div>
</div>
<!-- Thêm tài liệu tham chiếu START-->
<div id="loadFormReferenceMaterial"></div>
<!-- Thêm tài liệu tham chiếu END -->
<script>
    $(document).on("click",".x__close",function(){
        $(".fullImage").fadeOut();
        $(".list_img").html("");
    });
    $(document).on("click",".fullScreen_img",function(){
        $(".list_img").html("");
       let src_img = $(this).attr("src");
       let allImg = document.querySelectorAll(".fullScreen_img");
       $(allImg).each((key,obj,i)=>{
            let item_src =  $(obj).attr("src");
            if(item_src != "" )
            {
                $(".list_img").append(`<img class="item__img fullScreen_img" src="${item_src}" >`);
            }
           
       });
       $(".fullImage").fadeIn();
       $("#main_fullscreen").attr("src", src_img);
    });
    // cập nhật
    function addFormReferenceMaterial(id)
    {
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormReferenceMaterial",
            type: "POST",
            data: {employeeID:employeeID,id:id},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormReferenceMaterial').html(data);
                }
            }
        });   
    }
  
</script>