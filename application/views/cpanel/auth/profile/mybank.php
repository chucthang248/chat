<div class="title_tab">
    <div class="row">
        <div class="col-md-5">
            <h4 class="mb-0">THÔNG TIN TÀI KHOẢN NGÂN HÀNG</h4>
            <p class="mb-0">Bank account information</p>
        </div>
        <?php if($data_index['roleID']['type'] != "error") { ?>
        <div class="col-md-7 text-right">
            <a href="javascript:void(0)" onclick="addAccountBankInfo()" class="text-blue font-weight-bold">Thêm tài khoản</a>
        </div>
        <?php  } ?> 
        
    </div>
</div>
<hr>
<?php if($data_employee['Employeebank'] != NULL) {  ?>
    <?php foreach($data_employee['Employeebank'] as $key_Employeebank=> $val_Employeebank){ ?>
        <div class="item_relatives parent_item">
             <i class="fas fa-pen icon_edit" onclick="updateAccountBankInfo(<?=$val_Employeebank['id']?>)"></i>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Ngân hàng:</label>
                        <div class="info_name">
                            <?=$val_Employeebank['bank'] != ''?$val_Employeebank['bank']:TEXT_UPDATE; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Số TK:</span></label>
                        <div class="info_name">
                            <?=$val_Employeebank['bank_number'] != ''?$val_Employeebank['bank_number']:TEXT_UPDATE; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Tên TK:</span></label>
                        <div class="info_name">
                            <?=$val_Employeebank['bank_name'] != ''?$val_Employeebank['bank_name']:TEXT_UPDATE; ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group ">
                        <label class="t_color_b9b9b9" for="">Tên chi nhánh:</span></label>
                        <div class="info_name">
                            <?=$val_Employeebank['bank_branch'] != ''?$val_Employeebank['bank_branch']:TEXT_UPDATE; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
<?php }else{ ?>
<div class="text-left mb-3">
    <p>Đang chờ cập nhật...</p>
</div>
<?php } ?>
<!-- Thêm thông tin tk ngân hàng -->
<div id="loadFormAddAccountBankInfo"></div>
<!-- Thêm thông tin tk ngân hàng END -->
<script>
    function updateAccountBankInfo(id)
    {
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/updateAccountBankInfo",
            type: "POST",
            data: {employeeID:employeeID,id:id},
            dataType: "html",
            success: function(data){
                // let check = JSON.parse(data);
                if(data){
                    $('#loadFormAddAccountBankInfo').html(data);
                }
            }
        });
    }
    function addAccountBankInfo(){
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addAccountBankInfo",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormAddAccountBankInfo').html(data);
                }
            }
        });
    }
</script>