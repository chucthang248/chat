<div class="col-12">
    <div class="panel__content">
        <div class="row">
            <div class="col-md-4 ">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Mức lương hiện tại:</label>
                    <div class="info_name font-22"><?=$data_employee['salary_current'] != "" &&  $data_employee['salary_current'] != 0 ?number_format($data_employee['salary_current']):TEXT_UPDATE?></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group ">
                    <label class="t_color_b9b9b9" for="">Mức lương mong muốn:</label>
                    <div class="info_name font-22"><?=$data_employee['salary_desire'] != ""  &&  $data_employee['salary_desire'] != 0 ?number_format($data_employee['salary_desire']):TEXT_UPDATE?></div>
                </div>
            </div>
        </div>
    </div>
    <hr class="hr-xs"/>
    <div class="panel__content mt-3">
        <div class="title_tab">
            <div class="row">
                <div class="col-md-5">
                    <h4 class="mb-0">TÓM TẮT QUÁ TRÌNH LÀM VIỆC</h4>
                    <p class="mb-0">List of work histories</p>
                </div>
                 <?php if($data_index['roleID']['type'] != "error") { ?>
                    <div class="col-md-7 text-right">
                        <a href="javascript:void(0)" onclick="addFormHistoryWork()" class="text-blue font-weight-bold">Thêm lịch sử công tác</a>
                    </div>
                 <?php  } ?>
            </div>
        </div>
        <?php if($employees_works != NULL) {?>
            <?php foreach($employees_works  as $key_works => $val_works) { ?>
                <div class="item_relatives parent_item">
                     <hr>
                    <i class="fas fa-pen icon_edit" onclick="addFormHistoryWorkUpdate(<?=$val_works['id']?>)"></i>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Thời gian:</label>
                                <div class="info_name">
                                    <?php if($val_works['date_from'] != ""){ echo  $val_works['date_from']; }else{echo TEXT_UPDATE;} ?> - 
                                    <?php  if($val_works['date_to'] != "") { echo $val_works['date_to']; }else{echo TEXT_UPDATE;} ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="t_color_b9b9b9" for="">Chức vụ đảm nhiệm:</label>
                                        <div class="info_name"><?=$val_works['work_unit'] != ""?$val_works['work_unit']:TEXT_UPDATE?></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="t_color_b9b9b9" for="">Loại hình công ty:</label>
                                        <div class="info_name"><?php if($bussinesstypes != NULL){ ?>
                                                <?php foreach($bussinesstypes  as $key_bsType => $val_bsType){ ?>   
                                                        <?php if($val_bsType['id'] == $val_works['bussinesstypeID'] ) { ?>
                                                            <?=$val_bsType['name']; ?>  
                                                        <?php } ?>
                                                <?php }?>
                                            <?php }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="t_color_b9b9b9" for="">Chức vụ đảm nhiệm:</label>
                                        <div class="info_name"><?php if( $val_works['position'] != "") { echo $val_works['position']; }else{echo TEXT_UPDATE;} ?></div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group ">
                                        <label class="t_color_b9b9b9" for="">Chức vụ kiêm nhiệm:</label>
                                        <div class="info_name"><?php if( $val_works['concurrently'] != "") { echo $val_works['concurrently']; }else{echo TEXT_UPDATE;} ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Tóm tắt nội dung:</label>
                                <p><?php if( $val_works['description'] != "") { echo $val_works['description']; }else{echo TEXT_UPDATE;} ?></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        <?php }else{ ?>
            <div class="text-left mb-3">
                <p>Đang chờ cập nhật...</p>
            </div>
        <?php } ?>
    </div>
</div>
 <!-- Thêm lịch sử công tác -->
 <div id="loadFormWorkinfo_History"></div>
    <!-- Thêm lịch sử công tác -->
<script>
   
     function addFormHistoryWork(){
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormHistoryWork",
            type: "POST",
            data: {employeeID:employeeID},
            dataType: "html",
            success: function(data){
                $('#loadFormWorkinfo_History').html(data);
            }
        });
    }
    // cập nhật
    function addFormHistoryWorkUpdate(id)
    {
        let employeeID  = "<?=$this->uri->segment(4);?>";
        $.ajax
        ({
            url: "cpanel/ajax/addFormHistoryWorkUpdate",
            type: "POST",
            data: {employeeID:employeeID,id:id},
            dataType: "html",
            success: function(data){
                if(data){
                    $('#loadFormWorkinfo_History').html(data);
                }
            }
        });   
    }
</script>