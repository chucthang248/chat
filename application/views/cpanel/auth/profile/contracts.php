<div class="panel__content">
    <div class="title_tab">
        <div class="row">
            <div class="col-md-6">
                <h4 class="mb-0">HỒ SƠ VÀ HỢP ĐỒNG</h4>
                <p class="mb-0">Records and contracts</p>
            </div>
            <div class="col-md-6 text-right mt-1">
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-3">
            <div class="form-group  ">
                <label class="t_color_b9b9b9" for="">Hợp đồng:</label>
                <div class="info_name"><?=$contracts['type_contract_str'] ?></div>
            </div>
            <div class="form-group  ">
                <label class="t_color_b9b9b9" for="">Hồ sơ (file):</label>
                <div class="info_name">
                    <a href="upload/contract/<?=$contracts['contract_file'] ?>" download="<?=$contracts['contract_file'] ?>" ><?=$contracts['contract_file'] ?></a>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group ">
                <label class="t_color_b9b9b9" for="">Trạng thái:</label>
                <div class="info_name"><span class="badge badge-<?=$contracts['status'] == 0?'success':'danger'?> badge-pill"><?=$contracts['status'] == 0?'Đã ký':'Hết hạn'?></span></div>
            </div>
            <div class="form-group ">
                <label class="t_color_b9b9b9" for="">Chi nhánh:</label>
                <div class="info_name"><?=$contracts['branch']['name'] ?></div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group ">
                <label class="t_color_b9b9b9" for="">Ngày tạo:</label>
                <div class="info_name"><?php echo date('d/m/Y',strtotime($contracts['created_at']));?></div>
            </div>
            <div class="form-group ">
                <label class="t_color_b9b9b9" for="">Ngày hết hạn:</label>
                <div class="info_name"><?php echo date('d/m/Y',strtotime($contracts['time_end']));?></div>
            </div>
        </div>
    </div>
</div>