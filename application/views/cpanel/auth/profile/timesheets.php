<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />
<div class="col-12 timesheet">
    <div class="panel__content t_padding_left_50px t_padding_right_50px">
        <div id='calendar'></div>
        <div class="line-weight mt-4"> </div>
        <div class="current_timeSheet mt-3">
            <div class="row aligns-item-center">
                <div class="col-6">
                    <div class="t-group">
                        <div class="cr_title t_color_313a46">
                            Current Timesheet
                        </div>
                        <div class="cr_des t_color_b9b9b9">Current timesheet infomation</div>
                    </div>
                </div>
                <div class="col-6 right-cr  d-flex aligns-item-center">
                    <div class="col-auto item d-flex">
                        <span class="sp-icon-ar"><i class="fas fa-long-arrow-alt-right"></i></span>
                        <a href="javascript:void(0)" class="t_color_b9b9b9">Go to base timeoff</a>
                    </div>
                    <div class="col-auto item d-flex">
                        <a href="javascript:void(0)" class="t_color_b9b9b9">Edit timesheet</a>
                    </div>
                    <div class="col-auto item d-flex">
                        <a href="javascript:void(0)" class="t_color_b9b9b9">Edit PTO</a>
                    </div>
                    <div class="col-auto item d-flex">
                        <i class="fas fa-sort-down"></i>
                    </div>
                </div>
            </div>
        </div>
        <hr class="mt-3">
        <div class="tab_below">
            <div class="timesheet__item_above row">
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        Timesheet
                    </label>
                    <div class="ch_item_des t_color_096397">
                        Part-time sáng
                    </div>
                </div>
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        PTO policy
                    </label>
                    <div class="ch_item_des t_color_096397">
                        Standar PTO
                    </div>
                </div>
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        Enrollment
                    </label>
                    <div class="ch_item_des t_color_096397">
                        24/02/2021
                    </div>
                </div>
            </div>
            <div class="timesheet__item_below row">
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        Current PTO balance
                    </label>
                    <div class="ch_item_des">
                        11.0 (days)
                    </div>
                </div>
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        Carry over
                    </label>
                    <div class="ch_item_des">
                        2.0 (days)
                    </div>
                </div>
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        Year to date
                    </label>
                    <div class="ch_item_des ">
                        9.0 (days)
                    </div>
                </div>
                <div class="form-group col-6 col-sm-3">
                    <label class="ch_item_title t_color_b9b9b9">
                        Grant by length of service
                    </label>
                    <div class="ch_item_des ">
                        0.0 (days)
                    </div>
                </div>
            </div>
            <div class="timehseet_preview mt-2">
                <div class="title_preview">Timesheet preview</div>
                <div class="box_item_preview mt-2">
                    <div class="item_preview d-flex">
                        <div class="child_date">
                            Thứ 2
                        </div>
                        <div class="child_time">
                            <span class="sp_bg"> 8:00 - 10:00</span>
                        </div>
                    </div>
                    <div class="item_preview d-flex">
                        <div class="child_date">
                            Thứ 3
                        </div>
                        <div class="child_time">
                            <span class="sp_bg"> 9:00 - 11:00 </span>
                        </div>
                    </div>
                    <div class="item_preview d-flex">
                        <div class="child_date">
                            Thứ 4
                        </div>
                        <div class="child_time">
                            <span class="sp_bg"> 13:00 - 15:00 </span>
                        </div>
                    </div>
                    <div class="item_preview d-flex">
                        <div class="child_date">
                            Thứ 5
                        </div>
                        <div class="child_time">
                            <span class="sp_bg"> 16:00 - 18:00 </span>
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:void(0)" class="pto_activity t_color_b9b9b9  "><i
                    class="fas fa-long-arrow-alt-right"></i> PTO activity logs (Base timeoff)</a>
            <div class="line-weight "> </div>
            <div class="checkin_code">
                <div class="current_timeSheet mt-3">
                    <div class="row aligns-item-center">
                        <div class="col-6">
                            <div class="t-group">
                                <div class="cr_title t_color_313a46">
                                    Checkin code
                                </div>
                                <div class="cr_des t_color_b9b9b9">List of checkin code by office</div>
                            </div>
                        </div>
                        <div class="col-6 right-cr  d-flex aligns-item-center">
                            <div class="col-auto item d-flex">
                                <span class="sp-icon-ar"><i class="fas fa-plus"></i></span>
                                <a href="javascript:void(0)" class="t_color_b9b9b9"> Add checkin code</a>
                            </div>
                            <div class="col-auto item d-flex">
                                <i class="fas fa-sort-down"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="list__tab">
                    <table class="table table-hover dt-responsive nowrap mt-3"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Office</th>
                                <th>Code</th>
                                <th></th>
                             
                            </tr>
                        </thead>
                        <tbody>
                            <tr width="150">
                                <td >
                                    01
                                </td>
                                <td width="750" >
                                    <div class="office_name">HỒ CHÍ MINH</div>
                                    <div class="office_address">Tầng 1 chung cư plaza, phường bến nghé</div>
                                </td>
                                <td class=" fw-bold"> <span class="box__number">016</span> </td>
                                   
                                <td class="text-center">
                                    <div class="box_function">
                                        <div class="item_function">
                                            Sửa
                                        </div>    
                                        <div class="item_function">
                                        <i class="fas fa-chevron-down"></i>
                                        </div>       
                                    </div>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- load form -->
<div id="loadForm"></div>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    window.onresize = function(event) {
        location.reload();
    };
  // full calendar
  let eventsArrayObject = [];
    let current_eventClick = "";
    let defaultColor= "defaultColor";
    let employeeID = $("#employeeID").val();
    let waitColor = "wait_approval";
    let evtClick_uid = "";
    let delID_Array = [];
    let start = "";
    let getDMY = "";
    let getYear = "";
    let getMonth = "";
    let getDay = "";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },
            eventResourceEditable: true,
            buttonText: {
                today: 'Hôm nay'
            },
            locale: 'vi',
           
        });
        // load events
        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "cpanel/timework/loadEvents",
                dataType: "JSON",
                success: function(res) {
                    let data_timework = res.listTimeWork;
                    calendar.addEventSource(data_timework);
                }
            });
        });
        // xong tất các tác vụ thì mới render
        calendar.render();
    });
    function getUniqueListBy(arr, key) {
     return [...new Map(arr.map(item => [item[key], item])).values()]
    }
    // uuid
    function uuid() {
        var temp_url = URL.createObjectURL(new Blob());
        var uuid = temp_url.toString();
        URL.revokeObjectURL(temp_url);
        return uuid.substr(uuid.lastIndexOf('/') + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
    }
</script>