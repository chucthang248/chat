<!-- Start Content-->
<div id="loadAjaxPage"></div>
<div class="container-fluid profile__info">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Hưng Thịnh HR</a></li>
                        <li class="breadcrumb-item active">Tài khoản</li>
                    </ol>
                </div>
                <h4 class="page-title">Thông tin Tài khoản</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <div class="row mr_lr_fix">
        <div class="col-sm-4 col-md-2 bg-white t_padding_tb_20 bd-right">
            <div class="wrap__content ">
                <div class="box__content text-center">
                    <?php  $avatar = $path_dir.$data_employee['code'].'/'.$data_employee['avatar']; ?>
                    <div class="box__avatar" id="boxAvatar" > 
                        <img src="<?=file_exists($avatar) && $data_employee['avatar'] != ''?$avatar:"public/images/avatar-default.jpg"; ?>"
                        class="img-thumbnail rounded-circle d-block mx-auto" alt="avatar" width="180" height="180">
                        <div class="box__status round_status bg_status_1 round_status_color_<?= $data_employee['status']; ?> ">
                       
                        </div>
                    </div>
                    <h3 class="staff_name mt-2 font-20">
                        <?=$data_employee['fullname'] != NULL?$data_employee['fullname']:TEXT_UPDATE; ?>
                    </h3>
                    <div class="staff_position  mt-2"><?=$jobposition['name'] != NULL?$jobposition['name']:TEXT_UPDATE; ?> </div>
                </div>
                <hr>
                <div class="box__content ">
                    <div class="form-group">
                        <span class="t_inline_block width_40 text-danger"><i class="fas fa-heart"></i></span>
                        <span class="fs-14px fw-500"><?=$startwork !="" ? $startwork : TEXT_UPDATE ?></span>
                    </div>
                    <div class="form-group">
                        <span class="t_inline_block width_40"><i class="mdi mdi-qrcode-scan"></i></span>
                        <span class="fs-14px fw-500">
                            <?=$data_employee['code'] != NULL ?$data_employee['code']:TEXT_UPDATE;  ?></span>
                    </div>
                    <div class="form-group clearfix">
                        <span class="t_inline_block width_40 float-left"><i class="icon-envelope-open"></i></span>
                        <span class="fs-14px fw-500 profile-mail float-left">
                            <?=$data_employee['email'] != ""?character_limiter($data_employee['email'],1):TEXT_UPDATE?>
                        </span>
                    </div>
                    <div class="form-group">
                        <span class="t_inline_block width_40"><i class="icon-phone"></i></span>
                        <span
                            class="fs-14px fw-500"><?=$data_employee['phone'] != ""?$data_employee['phone']:TEXT_UPDATE?></span>
                    </div>
                </div>
                <hr>
                <div class="box__content">
                    <div class="">
                        <ul class="nav profile_tab t_flex_derection_col" id="myTab">
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/profile<?="/".$this->uri->segment(4)?>" id="profile_info"  class="t_inline_block w-100 t_padding_tb_10 normal_link active">
                                    <span class="t_inline_block width_40"><i class="icon-user"></i></span>
                                    <span class="t_inline_block"><strong>Thông tin cá nhân</strong> <br/>
                                        <span class="des">Profile, Legals & Background</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/mywork<?="/".$this->uri->segment(4)?>" id="mywork"  aria-expanded="true"
                                    class="t_inline_block  normal_link   w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="icon-flag"></i></span>
                                    <span class="t_inline_block"><strong>Thông tin công việc</strong><br/>
                                        <span class="des">Career, Offers, Promotions & more</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/benefits<?="/".$this->uri->segment(4)?>" id="benefits"  aria-expanded="true"
                                    class="t_inline_block  normal_link   w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="fab fa-wpforms"></i></span>
                                    <span class="t_inline_block"><strong>Lương & phúc lợi</strong><br/>
                                        <span class="des">Payrolls, Benefits, Statements & more</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/tasklist<?="/".$this->uri->segment(4)?>"  id="tasklist"  aria-expanded="true"
                                    class="t_inline_block  normal_link   w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="icon-list"></i></span>
                                    <span class="t_inline_block"><strong>Danh sách nhiệm vụ</strong><br/>
                                        <span class="des">Job functions & feedbacks</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/achievement<?="/".$this->uri->segment(4)?>"  id="achievement"  aria-expanded="true"
                                    class="t_inline_block  normal_link  w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="icon-badge"></i></span>
                                    <span class="t_inline_block"><strong>Thành tựu & giải thưởng</strong><br/>
                                        <span class="des">Certificates, Awards, Appreciation</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/contracts<?="/".$this->uri->segment(4)?>" id="contracts"  aria-expanded="true"
                                    class="t_inline_block  normal_link  w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="icon-folder"></i></span>
                                    <span class="t_inline_block"><strong>Hồ sơ & hợp đồng</strong><br/>
                                        <span class="des">Contracts & HR Documents</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/timesheets<?="/".$this->uri->segment(4)?>" id="timesheets"   aria-expanded="true"
                                    class="t_inline_block  normal_link  w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="icon-calender"></i></span>
                                    <span class="t_inline_block"><strong>Lịch làm việc & checkins</strong><br/>
                                        <span class="des">Timesheets, Checkins & Timeoffs</span>
                                    </span>
                                </a>
                            </li>
                            <li class="profile__item__menu">
                                <a href="cpanel/auths/intellectual<?="/".$this->uri->segment(4)?>" id="intellectual"  aria-expanded="true"
                                    class="t_inline_block  normal_link  w-100 t_padding_tb_10">
                                    <span class="t_inline_block width_40"><i class="icon-drawar"></i></span>
                                    <span class="t_inline_block"><strong>Sở hữu trí tuệ</strong><br/>
                                        <span class="des">Intellectual properties and Physic...</span>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-8 col-md-10 right-col  bg-white t_padding_tb_20">
            <div class="tab-content">
                <?php
                    if(isset($tab) && !empty($tab)){
                        $this->load->view($tab, isset($data)?$data:NULL);
                    }
                ?>
            </div>
        </div>
    </div>
</div> <!-- end container-fluid -->
<script>

    $(document).ready(function() {
        let getCurrenTab= "<?=$active?>";
        $(".normal_link").removeClass("active");
        $("#"+getCurrenTab).addClass("active");
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('active_account', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('active_account');
        
        if(activeTab != null){
            $('a[data-toggle="tab"]').removeClass("active");
            $('#myTab a[data-aTab="'+ activeTab +'"]').addClass("active");
            $(".tab-pane").removeClass("active");
            $(activeTab).addClass("active");
        }
        // end save active tab
    });
</script>