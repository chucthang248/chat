<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 
    <!-- notify --> 
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End --> 

    <div class="row justify-content-center">
        <div class="col-lg-4">
            <div class="card-box">
                <h4 class="font-20 text-uppercase mb-1 mt-2 text-center"><?=$title;?></h4>
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <div class="clear mt-3"></div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="code" class="col-12 col-form-label">Mật khẩu cũ<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="password" required parsley-type="code" name="old_password" class="form-control" id="old_password">
                                        <div class="has-error text-danger"><?php echo form_error('old_password') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="code" class="col-12 col-form-label">Mật khẩu mới<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="password" required parsley-type="code" name="new_password" class="form-control" id="new_password">
                                        <div class="has-error text-danger"><?php echo form_error('new_password') ?></div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="code" class="col-12 col-form-label">Nhập lại khẩu mới<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="password" required parsley-type="code" name="re_password" class="form-control" id="re_password">
                                        <div class="has-error text-danger"><?php echo form_error('re_password') ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box__tools text-center">
                        <button class="btn btn-blue waves-effect waves-light mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
    function randomPass(el){
        let id = $(el).val();
        $.ajax({
            url: '<?=$path_url?>random_pass',
            type: 'POST',
            dataType: 'json',
            data: {id: id},
            success: function(data){
                $("#email").val(data.email); 
                $("#password").val(data.password); 
            }
        })
    }
</script>

