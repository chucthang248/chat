<!DOCTYPE html>
<html lang="en">
<head>
	<base href="<?php echo site_url(); ?>" />
    <meta charset="utf-8" />
    <title>Login | Hưng Thịnh HR</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/images/favicon.webp">

    <!-- App css -->
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
    <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css"  id="app-stylesheet" />
    <link href="public/assets/css/style.css" rel="stylesheet" type="text/css" />

</head>
<body class="authentication-bg authentication-bg-pattern d-flex align-items-center pb-0 vh-100">
    <div class="account-pages w-100 mt-5 mb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-lg-6 col-xl-4">
                    <div class="card mb-0">
                        <div class="card-body p-4">
                            <div class="account-box">
                                <div class="account-logo-box">
                                    <div class="text-center">
                                        <a href="index.html">
                                            <img src="public/images/logo.png" alt="" height="40">
                                        </a>
                                    </div>
                                    <h5 class="text-uppercase mb-1 mt-3">ĐĂNG NHẬP</h5>
                                </div>

                                <div class="account-content mt-3">
                                    <form class="form-horizontal" action="" method="POST">

                                        <div class="form-group row">
                                            <div class="col-12">
                                                <label for="emailaddress">Địa chỉ email</label>
                                                <input class="form-control" type="email" name="email" id="emailaddress" required="" placeholder="john@gmail.com">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-12">
                                                <a href="cpanel/forget-password.html" class="text-muted float-right"><small>Quên mật khẩu?</small></a>
                                                <label for="password">Mật khẩu</label>
                                                <input class="form-control" type="password" name="password" required="" id="password" placeholder="Nhập mật khẩu của bạn">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <div class="col-12">

                                                <div class="checkbox checkbox-dark">
                                                    <input id="remember" type="checkbox" checked="">
                                                    <label for="remember">
                                                        Ghi nhớ mật khẩu
                                                    </label>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="form-group row text-center mt-2">
                                            <div class="col-12">
                                                <button type="submit" class="btn btn-md btn-block btn-blue waves-effect waves-light" type="submit">Đăng nhập</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- end card-body -->
                </div>
                <!-- end card -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
    </div>
    <!-- end page -->
    <!-- Vendor js -->
    <script src="public/assets/js/vendor.min.js"></script>

    <!-- App js -->
    <script src="public/assets/js/app.min.js"></script>
</body>
</html>