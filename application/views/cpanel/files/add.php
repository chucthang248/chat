<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title mb-4"><?=$title;?></h4>
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <div class="clear mt-3"></div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-auto">
                                <div class="form-group ">
                                    <label for="code" class=" col-form-label">Loại hồ sơ<span class="text-danger"> *</span></label>
                                    <select  name="data_post[typefileID]"
                                        id="action_change" class="width_150 form-control" id="">
                                        <option value="0">Chọn loại hồ sơ</option>
                                        <?php if($datas['typeFile'] != NULL){ ?>
                                            <?php foreach($datas['typeFile'] as $ke_type => $val_type){ ?>
                                                <option value="<?=$val_type['id'];?>"><?=$val_type['name'];?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Mã hồ sơ<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="text" required parsley-type="code" name="data_post[code]" class="form-control" id="code" value="<?php echo set_value('data_post[code]'); ?>">
                                        <div class="has-error text-danger"><?php echo form_error('data_post[code]') ?></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-5">
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Tiêu đề<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <input type="text" required parsley-type="code" name="data_post[name]" class="form-control" id="name" value="<?php echo set_value('data_post[name]'); ?>">
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group row">
                                    <label for="code" class="col-3 col-form-label">Nội dung<span class="text-danger"> *</span></label>
                                    <div class="col-12">
                                        <textarea required parsley-type="code" class="form-control ckeditor" name="data_post[content]" rows="5" id="content">
                                            <?php echo set_value('data_post[content]'); ?>
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="checkbox checkbox-blue">
                                    <input id="publish" type="checkbox" name="data_post[publish]" checked value="1">
                                    <label for="publish">
                                        Hiển thị
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="box__tools text-center">
                        <button class="btn btn-blue waves-effect waves-light mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?=CPANEL.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
