<link rel="stylesheet" type="text/css" href="public/assets/customs/form.css">
<div class="container__form">
	<div class="body__form">
		<a href="javascript:void(0)" class="btnClose" onclick="closeForm()"><i class="icon-close"></i></a>
		<div class="box__form form-sm">
			<div class="box clearfix">
				<div class="title"><?=$title;?></div>
				<hr class="hr-xs" />
				<form id="myForm" action="<?=CPANEL.$control?>/form" method="POST" enctype="multipart/form-data">
					<input type="hidden" id="rowID" name="data_post[rowID]" value="" />
					<div class="form">
						<div class="row">
							<div class="col-6">
				                <div class="form-group">
				                    <label for="employeeID" class="col-form-label">Nhân sự *</label>
				                    <select id="employeeID" name="data_post[employeeID]" class="form-control" required="">
				                        <option value="">-- Vui lòng chọn --</option>
				                        <?php if(isset($employees) && $employees != NULL){ ?>
				                        	<?php foreach ($employees as $key_employee => $val_employee) { ?>
				                        		<option value="<?=$val_employee['id'];?>"><?=$val_employee['fullname'];?></option>
				                        	<?php } ?>
				                        <?php } ?>
				                    </select>
				                </div>
				            </div>
							<div class="col-6">
				                <div class="form-group">
				                    <label for="type_contractID" class="col-form-label">Loại hợp đồng *</label>
				                    <select id="type_contractID" name="data_post[type_contractID]" class="form-control" required="">
				                        <option value="">-- Vui lòng chọn --</option>
				                        <option value="1">HĐ thử việc</option>
				                        <option value="2">HĐ không xác định thời hạn</option>
				                        <option value="3">HĐ lao động 01 năm</option>
				                        <option value="4">HĐ lao động 06 tháng</option>
				                        <option value="4">HĐ dịch vụ</option>
				                        <option value="6">HĐ lao động thời vụ (trên 3 tháng)</option>
				                        <option value="7">HĐ 06 tháng</option>
				                    </select>
				                </div>
				            </div>
				            <div class="col-6">
								<div class="form-group">
									 <label for="">Chi nhánh</label>
									 <div >
										<select class="form-control" name="data_post[branchID]" id="loadBranch">
											<option value="">Chọn chi nhánh</option>
											<?php foreach($arrdata['branch'] as $key_branch => $val_branch) {?>
													<option value="<?=$val_branch['id']?>"><?=$val_branch['name']?></option>
											<?php  } ?>
										</select>
									 </div>
								</div>
							</div>
							<div class="col-6">
								<div class="form-group">
									 <label for="">Vị trí công việc</label>
										<select class="form-control" name="data_post[job_positionID]" id="load_jobposition">
											<option value="">Chọn vị trí</option>
											<?php foreach($arrdata['job_position'] as $key => $val){ ?>
												<option <?=$val['id'] == $dataRow['job_positionID'] ? "selected" : ""?> value="<?=$val['id'] ?>"> <?=str_repeat("---",$val['count']).$val['name'] ?></option>
											<?php } ?>
										</select>
								</div>
							</div>
				            <div class="col-6">
				                <div class="form-group">
	                                <label for="time_start" class="col-form-label">Thời hạn hợp đồng</label>
	                                <input type="text" parsley-type="time_start" name="data_post[time_start]" class="form-control" id="time_start" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="21/04/1991">
	                            </div>
				            </div>
				            <div class="col-6">
				                <div class="form-group">
	                                <label for="time_end" class="col-form-label">Ngày kết thúc</label>
	                                <input type="text" parsley-type="time_end" name="data_post[time_end]" class="form-control" id="time_end" data-toggle="input-mask" data-mask-format="00/00/0000" placeholder="21/04/1991">
	                            </div>
				            </div>
				        </div>
				        <div class="form-group">
	                        <label class="col-form-label" for="example-fileinput">Đính kèm</label>
	                        <input type="file" name="contract_file" id="contract_file" class="form-control">
	                        <div id="loadFile" class="mt-2"></div>
	                    </div>
		                <div class="form-group">
		                    <label for="name" class="col-form-label">Chi tiết hợp đồng và các yêu cầu khác</label>
		                    <textarea name="data_post[content]" id="content" class="form-control ckeditor" rows="5"></textarea>
		                    <script type="text/javascript">
				                CKEDITOR.replace( 'content',
			                        {
			                            toolbar: [
								           [ 'Bold', 'Italic', 'Underline', '-', 'NumberedList', 'BulletedList' ],
								           [ 'FontSize', 'TextColor', 'BGColor' ]
								       	]
			                        });
				            </script>
		                </div>
		                <div class="box__btn">
		                	<button type="reset" id="btnReset" onclick="closeForm()">Bỏ qua</button>
		                	<button type="submit" class="btn__blue">Lưu lại</button>
		                </div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
        $('[data-toggle="input-mask"]').each(function (idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {'reverse': reverse});
            else
                $(obj).mask(maskFormat);
        });
    });
</script>
