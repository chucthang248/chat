<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="javascript:void(0)" onclick="openForm()" class="btn btn-blue waves-effect waves-light">
                                <i class="icon-plus mr-1"></i> Thêm mới
                            </a>
                            <a href="<?= CPANEL . $control ?>" type="button" class="btn btn-linkedin waves-effect waves-light">
                                <i class="icon-refresh"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr />
                <table id="datatable" class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th class="text-center">#</th>
                            <th>Nhân sự</th>
                            <th>Hợp đồng</th>
                            <th class="text-center">Trạng thái</th>
                            <th class="text-center">Ngày tạo</th>
                            <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if (isset($datas) && $datas != NULL) { ?>
                        <tbody>
                            <?php foreach ($datas as $key => $val) {
                                $stt = $key + 1;
                            ?>
                                <tr>
                                    <td class="text-center"><?= $stt ?></td>
                                    <td><?php echo $val['employee_name']; ?></td>
                                    <td><?php echo $val['type_contract']; ?><br />
                                        <span class="font-11"><?= date('d/m/Y', strtotime($val['time_start'])); ?>
                                            - <?= $val['time_end'] == '0000-00-00' ? ' ... ' : date('d/m/Y', strtotime($val['time_end'])); ?></span>
                                    </td>
                                    <td class="text-center">
                                        <span class="badge badge-<?= $val['status'] == 0 ? 'success' : 'danger' ?> badge-pill"><?= $val['status'] == 0 ? 'Đã ký' : 'Hết hạn' ?></span>
                                    </td>
                                    <td class="text-center"><?php echo date('d/m/Y', strtotime($val['created_at'])); ?></td>
                                    <td class="text-center">
                                        <a href="javascript:void(0)" onclick="openForm('1',<?= $val['id'] ?>)" class="btn btn-blue btn-xs waves-effect waves-light" data-id="<?php echo $val['id']; ?>">
                                            <i class="far fa-edit"></i>
                                        </a>
                                        <a href="javascript:void(0)" onclick="del(<?= $val['id'] ?>)" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val['id'] ?>" data-id="<?php echo $val['id']; ?>">
                                            <i class="far fa-trash-alt"></i>
                                        </a>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->

<div id="myForm">
    <?php $this->load->view(CPANEL . $control . '/form', $arrdata); ?>
</div>
<script>
    function openForm(typeRequest = 0, id = 0) {
        $('#btnReset').trigger("reset");
            $.ajax({
                url: "cpanel/contracts/getDataRow",
                type: "POST",
                data: {
                    id: id
                },
                dataType: "json",
                success: function(data) {
                    if (data.type == "error") {
                        Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");
                    } else {
                        if (typeRequest == '1') {
                             $('.title').html('Cập nhật dữ liệu');
                             $("#loadBranch").html(data.dataRow.branch);
                            $("#load_jobposition").html(data.dataRow.job_position_html);
                            $("#rowID").val(data.dataRow.id);
                            $("#type_contractID").val(data.dataRow.type_contractID);
                            $("#employeeID").val(data.dataRow.employeeID);
                            $("#time_start").val(data.dataRow.time_start);
                            $("#time_end").val(data.dataRow.time_end);
                            $("#loadFile").html(data.dataRow.contract_file);
                            $("#content").val(data.dataRow.content);
                            CKEDITOR.instances['content'].setData(data.dataRow.content);
                            $('.container__form').fadeIn();
                        }else
                        {
                            $('.title').html('Tạo mới dữ liệu');
                            $('.container__form').fadeIn();
                        }
                       
                      
                    }
                    
                }
            });
    }

    function closeForm() {
        CKEDITOR.instances['content'].setData('');
        $("#loadFile").html('');
        $('.container__form').fadeOut();
    }
</script>