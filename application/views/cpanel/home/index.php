<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Hưng Thịnh HR</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-2 col-sm-6">
            <div class="card-box widget-box-two widget-two-custom ">
                <div class="media">
                    <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                        <i class="mdi mdi-account-multiple avatar-title font-30 text-white"></i>
                    </div>

                    <div class="wigdet-two-content media-body">
                        <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Nhân sự</p>
                        <h3 class="font-weight-medium my-2"> <span data-plugin="counterup"><?=$total_employee?></span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card-box widget-box-two widget-two-custom ">
                <div class="media">
                    <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                        <i class="mdi mdi-account-multiple avatar-title font-30 text-white"></i>
                    </div>

                    <div class="wigdet-two-content media-body">
                        <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Tổng lương cơ bản</p>
                        <h3 class="font-weight-medium my-2"> <span class="count"><?=$total_salary_basic['salary_basic'];?></span></h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card-box widget-box-two widget-two-custom">
                <div class="media">
                    <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                        <i class="far fa-clock avatar-title font-30 text-white"></i>
                    </div>
                    <div class="wigdet-two-content media-body">
                        <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Thời gian làm việc trung bình</p>
                        <h3 class="font-weight-medium my-2"><span class="count"><?=$total_timework?></span> ngày</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.count').each(function () {
		$(this).prop('Counter',0).animate({
			Counter: $(this).text()
		}, {
			duration: 4000,
			easing: 'swing',
			step: function (now) {
				$(this).text(My_Numberformat(Math.ceil(now)));
			}
		});
    });
    function My_Numberformat(calc) {
        let regex = /[,~!@#$%^&*(){}.<>]/;
        let checkChracter = regex.test(calc);
        if(regex.test(calc) == true)
        {
         calc = calc.replaceAll(/[.,]/ig, '');
        }
     
        calc = parseInt(calc);
        calc = calc.toLocaleString('it-IT', { style: 'currency', currency: 'VND' });
        calc = calc.replace(/[.]/g, ',');
        calc = calc.replace(/[.]/g, ',');
        calc = calc.replace(/ VND/g, '');
        calc = calc.replace(/VND/g, '');
        calc = calc.replace(/ ₫/g, '');
        calc = calc.replace(/₫/g, '');
        calc = calc.replace(/\s+/g, '');
        return calc;
    }
</script>