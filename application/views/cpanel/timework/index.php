<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="public/assets/fullcalendar/lib/main.min.css">
<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />

<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="mb-0">Lịch làm việc từ ngày 02/03/2021 - 09/03/2021</h4>
                <hr />
                <div class="row row__note">
                    <div class="col-md-auto">
                        <div class="box__note_color d-flex">
                            <div class="sharp_color wait_approval"></div>
                            <span>: Đang chờ duyệt</span>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="box__note_color d-flex">
                            <div class="sharp_color done_approval"></div>
                            <span>: Đã được duyệt</span>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="box__note_color d-flex">
                            <div class="sharp_color destroy_approval"></div>
                            <span>: Không được duyệt</span>
                        </div>
                    </div>
                </div>
                <form  action="<?= CPANEL . 'employee'; ?>">
                    <input type="hidden" id="employeeID" value="<?= $employeeID ?>">
                         <div id='calendar'></div>
                </form>
                <div class="btn__save_all mt-2 btn btn-blue">Lưu</div>
            </div>
        </div>
    </div> <!-- end row -->
</div> <!-- end container-fluid -->
<div id="loadForm"></div>

<!-- <script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script> -->
<script src="public/assets/fullcalendar/lib/main.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    window.onresize = function(event) {
        location.reload();
    };
    // đóng form
    $(document).on("click", ".btn_close", function() {
        $("#loadForm").html("");
    });

    // full calendar
    let check__delete = "";
    let check__add = "";
    let check__update = "";
    let eventsArrayObject = [];
    let current_eventClick = "";
    let defaultColor = "defaultColor";
    let employeeID = $("#employeeID").val();
    let waitColor = "wait_approval";
    let evtClick_uid = "";
    let delID_Array = [];
    let start = "";
    let getDMY = "";
    let getYear = "";
    let getMonth = "";
    let getDay = "";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },
            eventResourceEditable: true,
            buttonText: {
                today: 'Hôm nay'
            },
            locale: 'vi',
            dateClick: function(info) {
                let countEvent = $(info.dayEl).find(".fc-daygrid-event-harness");
                let currentDate = moment(new Date()).format('YYYY-MM');
                start = info.dateStr;
                getDMY = info.dateStr.split("-");
                getYear = getDMY[0];
                getMonth = getDMY[1];
                getDay = getDMY[2];
                let beforeCurrentDate = moment(getYear+"-"+getMonth).isBefore(currentDate);
                if(beforeCurrentDate == true)
                {
                    Swal.fire({
                            title: "Bạn không thể dùng tác vụ này",
                            type: "warning",
                            showCancelButton: false,
                        });
                }else
                {
                    if (countEvent.length >= 2) {
                        Swal.fire({
                            title: "Bạn không thể nhập hơn 2 mục",
                            type: "warning",
                            showCancelButton: false,
                        });
                    } else {
                        $.ajax({
                            type: "POST",
                            url: "cpanel/ajax/employee_timework_add",
                            success: function(res) {
                                $(" #loadForm").html(res);
                                $("#timeclick").val(info.dateStr);

                            }
                        });

                    }                    
                }

            },
            eventClick: function(info) {
                info.jsEvent.preventDefault(); // don't let the browser navigate
                current_eventClick = info.event;
                start = moment(info.event.start).format("YYYY-MM-DD");
                let currentDate = moment(new Date()).format('YYYY-MM');
                let getYearMonth = moment(info.event.start).format("YYYY-MM");
                let _def_data = info.event._def;
                let optionOther = _def_data.extendedProps;
                evtClick_uid = _def_data.publicId;
                let startwork = optionOther.startwork;
                let endwork = optionOther.endwork;
                let beforeCurrentDate = moment(getYearMonth).isBefore(currentDate);
                if(beforeCurrentDate == true)
                {
                    Swal.fire({
                            title: "Bạn không thể dùng tác vụ này",
                            type: "warning",
                            showCancelButton: false,
                        });
                }else
                {
                    $.ajax({
                        type: "POST",
                        data: {
                            uid: evtClick_uid,
                            startwork: startwork,
                            endwork: endwork
                        },
                        url: "cpanel/ajax/employee_timework_edit",
                        success: function(res) {
                            $(" #loadForm").html(res);
                        }
                    });
                }

            }
        });
        // load events
        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "cpanel/timework/loadEvents",
                dataType: "JSON",
                success: function(res) {
                    let data_timework = res.listTimeWork;
                    calendar.addEventSource(data_timework);
                }
            });
        });
        $(document).on("click", ".btn__save_all", function() {
            let check = eventsArrayObject;
            let joinDel = delID_Array;
            // =======thêm=====
            $.ajax({
                type: "POST",
                url: "cpanel/timework/add",
                data: {
                    events: eventsArrayObject,
                    delid: delID_Array
                },
                dataType: "JSON",
                success: function(res) {
                    if (check__add != "") {
                        if (res.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ thêm!", "", "error");
                        } else {
                            calendar.removeAllEvents();
                            calendar.addEventSource(res);
                            // thông báo 
                            Swal.fire(
                                'Cập nhật thành công !',
                            );
                        }
                    }

                }
            });
            //======= sửa ==========
            $.ajax({
                type: "POST",
                url: "cpanel/timework/edit",
                data: {
                    events: eventsArrayObject,
                },
                dataType: "JSON",
                success: function(res) {
                    if (check__update != "") {
                        if (res.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ cập nhật!", "", "error");
                        } else {
                            calendar.removeAllEvents();
                            calendar.addEventSource(res);
                            // thông báo 
                            Swal.fire(
                                'Cập nhật thành công !',
                            );
                        }
                    }

                }
            });
            // ====== xóa ===========
            $.ajax({
                type: "POST",
                url: "cpanel/timework/delete",
                data: {
                    delid: delID_Array
                },
                dataType: "JSON",
                success: function(res) {
                    if (check__delete != "") {
                        if (res.type == "error") {
                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ xóa!", "", "error");
                        } else {
                            $("#loadForm").html(res);
                            calendar.removeAllEvents();
                            calendar.addEventSource(res);
                            delID_Array = []; // reset delete id
                            // thông báo 
                            Swal.fire(
                                'Cập nhật thành công !',
                            );
                        }
                    }

                }
            });

        });
        // add event
        $(document).on("click", ".btn_save", function() {
            check__add = "add";
            let getTimeClick = $("#timeclick").val();
            let startwork = $("#starttime").val();
            let endwork = $("#endtime").val();
            let parse_startwork = parseInt(startwork);
            let parse_endwork = parseInt(endwork);
            let title = parse_startwork + ":00 - " + parse_endwork + ":00";
            let uid = uuid();
            let flag = true;
            let data_client = [{
                id: uid,
                startwork: startwork,
                endwork: endwork,
                title: title,
                start: start,
                approval: 2,
                type: "employee",
                className: defaultColor
            }];
            // thêm vào mảng mỗi khi nhấn thêm
            eventsArrayObject.push({
                employeeID: employeeID,
                uid: uid,
                startwork: startwork,
                endwork: endwork,
                title: title,
                start: start,
                approval: 2,
                type: "employee",
                className: waitColor
            });
            if (parse_startwork > parse_endwork || parse_startwork == parse_endwork) {
                $(".alert_time").text("* Giờ bắt đầu phải nhỏ hơn giờ kết thúc *");
            } else {
                $(".alert_time").text("");
                calendar.addEventSource(data_client);
                $("#loadForm").html("");
            }

        });
        // update event
        $(document).on("click", ".btn__update", function() {
            check__update = "update";
            let getTimeClick = $("#timeclick").val();
            let startwork = $("#starttime").val();
            let endwork = $("#endtime").val();
            let parse_startwork = parseInt(startwork);
            let parse_endwork = parseInt(endwork);
            let title = parse_startwork + ":00 - " + parse_endwork + ":00";
            let flag = true;
            // lưu phía client
            let data_client = [{
                id: evtClick_uid,
                startwork: startwork,
                endwork: endwork,
                title: title,
                start: start,
                approval: 2,
                type: "employee",
                className: defaultColor
            }];
            // lưu phía database
            eventsArrayObject.push({
                employeeID: employeeID,
                uid: evtClick_uid,
                startwork: startwork,
                endwork: endwork,
                title: title,
                start: start,
                approval: 2,
                type: "employee",
                className: waitColor
            });
            eventsArrayObject = getUniqueListBy(eventsArrayObject, 'uid');
            if (parse_startwork > parse_endwork || parse_startwork == parse_endwork) {
                $(".alert_time").text("* Giờ bắt đầu phải nhỏ hơn giờ kết thúc *");
            } else {
                $(".alert_time").text("");
                current_eventClick.setProp('title', title);
                current_eventClick.setProp('classNames', defaultColor);
                current_eventClick.setExtendedProp('startwork', startwork);
                current_eventClick.setExtendedProp('endwork', endwork);
                $("#loadForm").html("");
            }
        });
        // remove current select event
        $(document).on("click", ".btn_del", function() {
            check__delete = "delete";
            delID_Array.push(evtClick_uid);
            delID_Array = [...new Set(delID_Array)];
            // xóa item trong mảng khi click delete
            eventsArrayObject = eventsArrayObject.filter(function(el) {
                return el.uid != evtClick_uid;
            });
            let item = calendar.getEventById(evtClick_uid);
            item.remove();
            $("#loadForm").html("");
        });

        // xong tất các tác vụ thì mới render
        calendar.render();
    });

    function getUniqueListBy(arr, key) {
        return [...new Map(arr.map(item => [item[key], item])).values()]
    }
    // uuid
    function uuid() {
        var temp_url = URL.createObjectURL(new Blob());
        var uuid = temp_url.toString();
        URL.revokeObjectURL(temp_url);
        return uuid.substr(uuid.lastIndexOf('/') + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
    }
</script>