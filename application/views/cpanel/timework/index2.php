<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />

<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="mb-0">Lịch làm việc từ ngày 02/03/2021 - 09/03/2021</h4>
                <hr />
                <div class="row">
                    <div class="col-md-auto">
                        <div class="box__note_color d-flex">
                            <div class="sharp_color wait_approval"></div>
                            <span>: Đang chờ duyệt</span>
                        </div>
                    </div>
                    <div class="col-md-auto">
                        <div class="box__note_color d-flex">
                            <div class="sharp_color done_approval"></div>
                            <span>: Đã được duyệt</span>
                        </div>
                    </div>
                    <div class="col-md-auto">
                    <div class="box__note_color d-flex">
                            <div class="sharp_color destroy_approval"></div>
                            <span>: Không được duyệt</span>
                        </div>
                    </div>
                </div>
                <form action="<?= CPANEL . 'employee'; ?>">
                    <div id='calendar'></div>
                </form>
                <div class="btn__save_all mt-2 btn btn-blue">Lưu</div>
            </div>
        </div>
    </div> <!-- end row -->
</div> <!-- end container-fluid -->
<div id="loadForm"></div>
<style>
    .select2-container {
        z-index: 2000;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    // đóng form
    $(document).on("click", ".btn_close", function() {
        $("#loadForm").html("");
    });

    // full calendar
    let start = "";
    let getDMY = "";
    let getYear = "";
    let getMonth = "";
    let getDay = "";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },
            eventResourceEditable: true,
            buttonText: {
                today: 'Hôm nay'
            },
            locale: 'vi',
            dateClick: function(info) {
                let countEvent = $(info.dayEl).find(".fc-daygrid-event-harness");
                start = info.dateStr;
                getDMY = info.dateStr.split("-");
                getYear = getDMY[0];
                getMonth = getDMY[1];
                getDay = getDMY[2];
                
                if (countEvent.length >= 2) {
                    Swal.fire({
                        title: "Bạn không thể nhập hơn 2 mục",
                        type: "warning",
                        showCancelButton: false,
                    });
                } else {
                   $.ajax({
                        type: "POST",
                        url: "cpanel/ajax/employee_timework_add",
                        success: function(res) {
                            $(" #loadForm").html(res);
                            $("#timeclick").val(info.dateStr);

                        }
                    });

                }
            },
            eventClick: function(info) {
                info.jsEvent.preventDefault(); // don't let the browser navigate
                let _def_data = info.event._def;
                let optionOther = _def_data.extendedProps;
                let uid = _def_data.publicId;
                $.ajax({
                        type: "POST",
                        data: {uid:uid},
                        url: "cpanel/ajax/employee_timework_edit",
                        success: function(res) {
                            $(" #loadForm").html(res);
                        }
                    });
            }
        });
        // load events
        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "cpanel/timework/loadEvents",
                dataType: "JSON",
                success: function(res) {
                    let data_timework = res.listTimeWork;
                    calendar.addEventSource(data_timework);
                }
            });
        });
        $(document).on("click",".btn__save_all",function(){
        $.ajax({
                type: "GET",
                url: "cpanel/timework/saveStatus",
                dataType: "JSON",
                success: function(res) {
                    calendar.removeAllEvents();
                    calendar.addEventSource(res);
                }
            });

         });
        // add event
        $(document).on("click", ".btn_save", function() {
            let getTimeClick = $("#timeclick").val();
            let startwork = $("#starttime").val();
            let endwork = $("#endtime").val();
            let parse_startwork = parseInt(startwork);
            let parse_endwork = parseInt(endwork);
            let title = parse_startwork + ":00 - " + parse_endwork + ":00";
            let uid = uuid();
            let flag = true;
            
            if (parse_startwork > parse_endwork || parse_startwork == parse_endwork) {
                $(".alert_time").text("* Giờ bắt đầu phải nhỏ hơn giờ kết thúc *");
            } else {
                $(".alert_time").text("");
                $.ajax({
                    type: "POST",
                    url: "cpanel/timework/addTimeWork",
                    data: {
                        uid: uid,
                        startwork: startwork,
                        endwork: endwork,
                        title: title,
                        start: start
                    },
                    dataType: "JSON",
                    success: function(res) {
                        let data_timework = res.listTimeWork;
                        calendar.removeAllEvents();
                        calendar.addEventSource(data_timework);
                    }
                });
                $("#loadForm").html("");
            }
         
        });
        // update event
        $(document).on("click", ".btn__update", function() {
            let getTimeClick = $("#timeclick").val();
            let startwork = $("#starttime").val();
            let endwork = $("#endtime").val();
            let parse_startwork = parseInt(startwork);
            let parse_endwork = parseInt(endwork);
            let title = parse_startwork + ":00 - " + parse_endwork + ":00";
            let uid = $("#uid").val();
            let flag = true;
            
            if (parse_startwork > parse_endwork || parse_startwork == parse_endwork) {
                $(".alert_time").text("* Giờ bắt đầu phải nhỏ hơn giờ kết thúc *");
            } else {
                $(".alert_time").text("");
                $.ajax({
                    type: "POST",
                    url: "cpanel/timework/editTimeWork",
                    data: {
                        uid: uid,
                        startwork: startwork,
                        endwork: endwork,
                        title: title,
                    },
                    dataType: "JSON",
                    success: function(res) {
                        let item = calendar.getEventById(res.uid);
                        item.setProp('title', res.title);
                        item.setProp('classNames', res.className);
                    }
                });
                $("#loadForm").html("");
            }
        });
        // remove current select event
        $(document).on("click",".btn_del",function(){
            let uid = $("#uid").val();
            $.ajax({
                    type: "POST",
                    url: "cpanel/timework/deleteItemTimework",
                    data: {
                        uid: uid,
                    },
                    success: function(res) {
                        let item = calendar.getEventById(uid);
                        item.remove();
                    }
                });
                $("#loadForm").html("");
        });
        
        // xong tất các tác vụ thì mới render
        calendar.render();
    });
    // uuid
    function uuid() {
        var temp_url = URL.createObjectURL(new Blob());
        var uuid = temp_url.toString();
        URL.revokeObjectURL(temp_url);
        return uuid.substr(uuid.lastIndexOf('/') + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
    }
</script>