<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
    <div id="box-notify" class="jq-toast-wrap bottom-right">
        <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
            <span class="jq-toast-loader jq-toast-loaded"
                style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
            <h2 class="jq-toast-heading">Thông báo!</h2>
            <?php echo $message_flashdata['message']; ?>
        </div>
    </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="mb-0">Lịch làm việc từ ngày 02/03/2021 - 09/03/2021</h4>
                <hr/>
                <div class="timework__list">
                    <div class="employee__list">
                        <div class="item">
                            Lê Văn Tuấn
                        </div>
                        <div class="item">
                            Lê Văn Tuấn
                        </div>
                        <div class="item">
                            Lê Văn Tuấn
                        </div>
                        <div class="item">
                            Lê Văn Tuấn
                        </div>
                        <div class="item">
                            Lê Văn Tuấn
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>