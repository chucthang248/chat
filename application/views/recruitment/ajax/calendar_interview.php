<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Lịch phỏng vấn</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <select id="recruitmentID" class="form-group form-control">
                <option value="">-- Tin tuyển dụng --</option>
                <?php foreach($recruitment as $key => $val) {?>
                    <option <?=$calendar_interview['recruitmentID'] == $val['id'] ? "selected" : ""  ?> value="<?=$val['id'];?>"><?=$val['title'];?></option>
                <?php } ?>
            </select>
            <select id="applyID" class="form-group form-control">
                <option value="">-- Ứng viên --</option>
                <?php foreach($apply as $key_apply => $val_apply) {?>
                    <option <?=$calendar_interview['applyID'] == $val_apply['id'] ? "selected" : ""  ?> value="<?=$val_apply['id'];?>"><?=$val_apply['fullname'];?></option>
                <?php } ?>
            </select>
            <select   class="form-group form-control" id="time_interview">
                <option value="">-- Giờ phỏng vấn -- </option>
                <?php $str_default = ""; 
                    $str_value = "";
                for($i=7 ; $i <= 23 ; $i++){   
                $str_default = $i < 10 ? "0" : ""; 
                $str_value = $str_default.$i.":00:00"; ?>
                <option <?=$calendar_interview['time_interview'] == $str_value ? "selected" : ""  ?> value="<?=$str_value?>"><?=$i?>:00</option>
                <?php } ?>
            </select>
            <textarea id="note" class="form-control" cols="30" rows="10" placeholder="Ghi chú"><?=$calendar_interview['note']?></textarea>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary  <?=isset($calendar_interview) ? 'btn_update' : 'btn_add' ?>">Lưu</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Hủy</button>
            <?=isset($calendar_interview) ? '<button type="button" class="btn btn-secondary btn_del" data-dismiss="modal">Xóa</button>' : '' ?>
        </div>
    </div>
</div>
<script>
     $(document).ready(function(){
        $('[data-toggle="input-mask"]').each(function (idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {'reverse': reverse});
            else
                $(obj).mask(maskFormat);
        });
    });
</script>