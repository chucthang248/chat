<table class="table table-bordered table-striped table-hover dt-responsive nowrap mt-3" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    <thead>
        <tr>
            <th class="text-center">
                <div class="checkbox checkbox-success checkbox-single">
                    <input type="checkbox" id="checkAll" name="publish" value="1" data-control="<?php echo $control; ?>">
                    <label class="mr-bottom_mn4"></label>
                </div>
            </th>
            <th>Tên</th>
            <th>Giới tính</th>
            <th>Ngày sinh</th>
            <th>Số điện thoại</th>
            <th>Email</th>
            <th>Vị trí ứng tuyển</th>
            <th class="text-center">Ngày ứng tuyển</th>
        </tr>
    </thead>
    <tbody>
        <?php if ($datas != NULL) { ?>
            <?php foreach ($datas as $key => $val) { ?>
                <tr class="tr<?= $val['id']; ?>">
                    <td class="text-center">
                        <div class="checkbox checkbox-success checkbox-single mt-2">
                            <input type="checkbox" class="input_checked_single" value="<?= $val['id']; ?>" id="publish" name="publish" data-control="<?php echo $control; ?>">
                            <label></label>
                        </div>
                    </td>
                    <td>
                        <?= $val['fullname']; ?>
                        <a href="javascript:void(0)" onclick="viewEmloyeeInfoDetail(<?=$val['id'];?>)"
                                         class="text-blue float-right mt-1"><i class="icon-eye font-20"></i></a>
                    </td>
                    <td>
                        <?= $val['sex'] == 1 ? "Nam" : "Nữ"; ?>
                    </td>
                    <td>
                        <?= implode("/", array_reverse(explode("-", $val['birthday']))) ?>
                    </td>
                    <td>
                        <?= $val['phone']; ?>
                    </td>
                    <td>
                        <?= $val['email']; ?>
                    </td>
                    <td>
                        <?= $val['position_apply']; ?>
                    </td>
                    <td class="text-center">
                        <?= date('d/m/Y', strtotime($val['created_at'])); ?>
                    </td>
                </tr>
            <?php } ?>
    </tbody>
<?php } ?>
</table>
<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end ">
    <div class="ChangeRow">
        <span>Hiển thị: <strong> <?= $per_page; ?></strong> Trong tổng số: <strong><?= $count ?></strong> </span>
    </div>
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
        <?= $my_pagination; ?>
    </div>
</div>