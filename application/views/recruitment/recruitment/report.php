<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Hưng Thịnh HR</a></li>
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                </div>
                <h4 class="page-title">Dashboard</h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-xl-3 col-sm-6">
            <div class="card-box widget-box-two widget-two-custom ">
                <div class="media">
                    <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                        <i class="mdi mdi-account-multiple avatar-title font-30 text-white"></i>
                    </div>

                    <div class="wigdet-two-content media-body">
                        <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Tin tuyển dụng</p>
                        <div class="row">
                        	<div class="col-6"><h3 class="font-weight-medium my-2"> <span data-plugin="counterup">1</span></h3></div>
                        	<div class="col-6 mt-3"><a href="recruitment/recruitment" class="text-blue font-weight-medium">Xem chi tiết</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card-box widget-box-two widget-two-custom ">
                <div class="media">
                    <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                        <i class="mdi mdi-account-multiple avatar-title font-30 text-white"></i>
                    </div>

                    <div class="wigdet-two-content media-body">
                        <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Ứng viên hiện tại</p>
                        <div class="row">
                        	<div class="col-6"><h3 class="font-weight-medium my-2"> <span data-plugin="counterup">1</span></h3></div>
                        	<div class="col-6 mt-3"><a href="recruitment/applylist" class="text-blue font-weight-medium">Xem chi tiết</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-sm-6">
            <div class="card-box widget-box-two widget-two-custom ">
                <div class="media">
                    <div class="avatar-lg rounded-circle bg-primary widget-two-icon align-self-center">
                        <i class="mdi mdi-account-multiple avatar-title font-30 text-white"></i>
                    </div>

                    <div class="wigdet-two-content media-body">
                        <p class="m-0 text-uppercase font-weight-medium text-truncate" title="Statistics">Lịch phỏng vấn</p>
                        <div class="row">
                        	<div class="col-6"><h3 class="font-weight-medium my-2"> <span data-plugin="counterup">1</span></h3></div>
                        	<div class="col-6 mt-3"><a href="recruitment/calendarinterview" class="text-blue font-weight-medium">Xem chi tiết</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>