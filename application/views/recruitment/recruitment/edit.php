<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <div class="row">
        <div class="col-lg-12">
            <div class="card-box">
                <h4 class="header-title mb-4"><?=$title;?></h4>
                <form method="POST" action="" class="parsley-examples" novalidate="" id="myForm" enctype="multipart/form-data">
                    <div class="clear mt-3"></div>
                    <div class="col-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group ">
                                    <div class="upload_card">
                                        <img id="preview" src="<?=$dataRow['avatar'] != "" ? $dataRow['avatar']  : "public/images/image-default.jpg" ?>" alt="Ảnh đại diện" title='' />
                                        <button type="button" class="upload_btn"> <input type="file" id="avatar" name="avatar" class="imgInp custom-file-input" aria-describedby="avatar" name="avatar">Chọn ảnh đại diện</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class=" col-form-label">Tiêu đề<span class="text-danger"> *</span></label>
                                            <input onkeyup="createSlug(this);" id="name"  data-control="recruitment/recruitment"  type="text" class="form-control" name="data_post[title]" required value="<?php echo $dataRow['title']; ?>">
                                            <div class="has-error text-danger"><?php echo form_error('data_post[title]') ?></div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label for="inputName" class="col-form-label">Slug</label>
                                            <input type="text"  data-control="recruitment/recruitment" name="data_post[alias]"  class="form-control" id="alias" value="<?php echo $dataRow['alias']; ?>"  required="" oninvalid="this.setCustomValidity('Bạn chưa nhập tiêu đề')" oninput="setCustomValidity('')">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class=" col-form-label">Cấp bậc<span class="text-danger"> *</span></label>
                                        <select name="data_post[id_recruitment_rank]" required class="form-control" id="">
                                            <option value="">--- Chọn cấp bậc ---</option>
                                            <?php foreach($rank as $key =>$val){ $selected = '';
                                                    if ($dataRow['id_recruitment_rank'] == $val['id']) {
                                                        $selected = 'selected';
                                                    }
                                                ?>
                                                    <option <?= $selected; ?> value="<?= $val['id'] ?>"><?= $val['name']; ?></option>
                                                <?php }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Hình thức<span class="text-danger"> *</span></label>
                                        <select name="data_post[id_recruitment_form]" required class="form-control" id="">
                                            <option value="">--- Chọn hình thức ---</option>
                                            <?php foreach($form as $key =>$val){
                                                $selected = '';
                                                if ($dataRow['id_recruitment_form'] == $val['id']) {
                                                    $selected = 'selected';
                                                }
                                                ?>
                                                    <option <?= $selected; ?> value="<?= $val['id'] ?>"><?= $val['name']; ?></option>
                                                <?php }
                                            ?>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Bằng cấp<span class="text-danger"> *</span></label>
                                            <input type="text" placeholder ="Bằng cấp" name="data_post[degree]" value="<?= $dataRow['degree'] ?>" required class = "form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Thu nhập<span class="text-danger"> *</span></label>
                                            <input type="text"   placeholder ="Thu nhập"  name="data_post[income]" value="<?= $dataRow['income'] ?>" required class ="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Kinh nghiệm<span class="text-danger"> *</span></label>
                                            <input type="text" placeholder ="Kinh nghiệm" name="data_post[experience]" value="<?= $dataRow['experience'] ?>" required class ="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Số lượng cần tuyển<span class="text-danger"> *</span></label>
                                            <input type="text" placeholder ="Số lượng ứng tuyển" name="data_post[amount]" value="<?= $dataRow['amount'] ?>" required class ="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Hạn nộp hồ sơ<span class="text-danger"> *</span></label>
                                            <input type="text" placeholder ="30/12/2021" name="data_post[date_submit]" value="<?= implode('/',array_reverse(explode("-",$dataRow['date_submit']))) ?>"  data-toggle="input-mask" data-mask-format="00/00/0000"  required class ="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Chọn thành phố<span class="text-danger"> *</span></label>
                                            <select name="data_post[city]" onchange= "change_District(this)" class="address_selected form-control itemValue" id="city">
                                                <option value="">Chọn thành phố</option>
                                                <?php if ($city['LtsItem'] != NULL) { ?>
                                                    <?php foreach ($city['LtsItem'] as $key_city => $val_city) { ?>
                                                        <option <?= $val_city['ID']==$dataRow['city']?'selected':'' ?> value="<?= $val_city['ID'] ?>"><?= $val_city['Title'] ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label ">Chọn quận / huyện<span class="text-danger"> *</span></label>
                                            <select name="data_post[district]" id="district" class="form-control">
                                                <option value="<?=$district['ID'] ?>"><?=$district['Title'] ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group ">
                                            <label  class="col-form-label">Nơi làm việc<span class="text-danger"> *</span></label>
                                            <textarea name="data_post[workplace]" id="" cols="30" rows="2" required  class="form-control"><?= $dataRow['workplace'] ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="form-group">
                                            <div class="checkbox checkbox-danger">
                                                <input id="checkbox6" type="checkbox" checked="" <?php if($Modules['publish'] == 1) { echo "checked";} ?> name="data_post[publish]" value="1">
                                                <label for="checkbox6"> Hiển thị </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group row">
                                            <label for="code" class="col-3 col-form-label">Nội dung<span class="text-danger"> *</span></label>
                                            <div class="col-12">
                                                <textarea required parsley-type="code" class="form-control ckeditor" name="data_post[content]" rows="5" id="content">
                                                <?= $dataRow['content'] ?>
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                      
                        </div>
                    </div>
                    <div class="box__tools text-center">
                        <button class="btn btn-blue waves-effect waves-light mr-1" type="submit"><i class="far fa-save"></i> Lưu</button>
                        <a href="<?='recruitment/'.$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<style>
    .upload_card
    {
        width: 92% !important;
    }
</style>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
    //input avatar
    $("#avatar").change(function(event) {  
        readURL(this);    
    });
    function readURL(input) {    
        if (input.files && input.files[0]) {   
            var reader = new FileReader();
            var filename = $("#avatar").val();
            filename = filename.substring(filename.lastIndexOf('\\')+1);
            reader.onload = function(e) {
                $('#preview').attr('src', e.target.result);
                $('#preview').hide();
                $('#preview').fadeIn(500);      
                // $('.custom-file-label').text(filename);             
            }
            reader.readAsDataURL(input.files[0]);    
        } 
    }
    $(document).ready(function(){
        $('[data-toggle="input-mask"]').each(function (idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {'reverse': reverse});
            else
                $(obj).mask(maskFormat);
        });
    });
</script>
<script>
        function change_District(_this){
        let city_id = _this.value;
        $.ajax({
            url:"recruitment/recruitment/get_district",
            method:"POST",
            data:{city_id:city_id},
            dataType:"JSON",
            success:function(e){
               if (e) {
                $('#district').html("");
                $("#district").append(`<option value="">Chọn quận huyện</option>`);
                  $(e.data).each((key,obj,i)=>{
                    $('#district').append(`<option value="${obj.ID}">${obj.Title}</option>`)
                  });
               }
            }
        });
    }
</script>
