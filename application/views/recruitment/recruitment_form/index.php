<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>     
    <!-- end page title --> 

    <!-- notify --> 
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End --> 
    <div class="row">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div class="row">
                    <div class="col-6">
                        <div class="box-tools">
                            <a href="<?='recruitment/'.$control?>/add" type="button" class="btn btn-blue waves-effect waves-light">
                                <i class="icon-plus mr-1"></i> Thêm mới
                            </a>
                            <a href="<?='recruitment/'.$control?>" type="button" class="btn btn-linkedin waves-effect waves-light">
                                <i class="icon-refresh"></i>
                            </a>
                            <a href="<?='recruitment/'.'recruitment'?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Trở lại</a>
                        </div>
                    </div>
                </div>
                <hr/>
                <table id="datatable" class="table table-bordered table-striped table-hover dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                    <thead>
                        <tr>
                            <th>Hình thức làm việc </th>
                            <th>Ngày tạo</th>
                            <th>Hiển thị</th>
                            <th>Tác vụ</th>
                        </tr>
                    </thead>
                    <?php if(isset($datas) && $datas != NULL){ ?>
                    <tbody>
                        <?php foreach ($datas as $key => $val) { ?>
                        <tr>
                            <td><?php echo $val['name'];?></td>
                            <td class="text-center"><?php echo date('d/m/Y',strtotime($val['created_at']));?></td>
                            <td class="text-center">
                                <div class="checkbox checkbox-blue checkbox-single mt-2">
                                    <input <?php if($val['publish'] == 1){ ?> checked="" <?php } ?> onclick="checkStatus_recruitment(<?php echo $val['id'];?>,'publish');" type="checkbox" id="publish<?php echo $val['id'];?>" name="publish" value="1" data-control="<?php echo $control;?>">
                                    <label></label>
                                </div>
                            </td>
                            <td class="text-center">
                                <a href="<?= 'recruitment/'.$control.'/edit/'.$val['id']?>" class="btn btn-blue btn-xs waves-effect waves-light">
                                    <i class="far fa-edit"></i>
                                </a>
                                <a href="javascript:void(0)" onclick="del_recruitment(<?php echo $val['id'];?>);" class="btn btn-danger btn-xs waves-effect waves-light delete<?php echo $val['id'];?>" data-control="<?php echo $control;?>" >
                                        <i class="far fa-trash-alt"></i>
                                    </a>
                             
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                    <?php } ?>
                </table>
            </div>
        </div>
        <div class="col-8" id="show__content">
            <i id="icon__close" class="far fa-times-circle"></i>
            <div id="result_content">
                
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
<style>
    #show__content{
        padding: 10px 20px;
        width: 65%;
        height: 708px;
        position: fixed;
        bottom: 80px;
        right: 333px;
        z-index: 99;
        background-color: #fff;
        color: #000;
        border-radius: 5px;
        display: none;
        border: 1px solid #64c5b1;
        overflow: scroll;
    }
    #icon__close{
        float: right;
        font-size: 45px;
        cursor: pointer;
        color: #64c5b1;
        position: fixed;
        right: inherit;
        right: 360px;
    }
</style>
<script>
    $(document).on('click','#icon__close',function(){
        $("#show__content").hide();
    });
    function showContent(id)
    {
        $.ajax({
            url: 'cpanel/files/showcontent',
            type: 'POST',
            dataType: 'json',
            data: {id: id},
            success: function(data){
                $("#show__content").show();
                if(data.content != ''){
                    $("#result_content").html(data.content); 
                }else{
                    $("#result_content").html('Chưa có nội dung'); 
                }
            }
        })
    }
</script>
