<div class="logo-box">
    <a href="cpanel" class="logo text-center">
        <span class="logo-lg">
            <img src="public/images/logo-white.png" alt="" height="40">
            <!-- <span class="logo-lg-text-light">UBold</span> -->
        </span>
        <span class="logo-sm">
            <!-- <span class="logo-sm-text-dark">U</span> -->
            <img src="public/assets/images/logo-sm.png" alt="" height="35">
        </span>
    </a>
</div>