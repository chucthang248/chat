<div class="left-side-menu">
    <div class="slimscroll-menu">
        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <ul class="metismenu" id="side-menu">
                <li>
                    <a href="recruitment/recruitment/report">
                        <i class="fas fa-chart-pie"></i>
                        <span> Tổng quan </span>
                    </a>
                </li>
                <li>
                    <a href="recruitment/recruitment">
                        <i class="icon-grid"></i>
                        <span> Tin tuyển dụng </span>
                    </a>
                </li>
                <li>
                    <a href="recruitment/applylist">
                        <i class="icon-people"></i>
                        <span> Danh sách ứng viên </span>
                    </a>
                </li>
                <li>
                    <a href="recruitment/test">
                        <i class="icon-grid"></i>
                        <span> Bài kiểm tra </span>
                    </a>
                </li>
                <li>
                    <a href="recruitment/calendarinterview">
                        <i class="icon-calender"></i>
                        <span> Lịch phỏng vấn </span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Sidebar -->
        <div class="clearfix"></div>
    </div>
    <!-- Sidebar -left -->
</div>
<script>
    
</script>