<link href="public/assets/customs/recruitment_infodetail.css" rel="stylesheet" type="text/css" />
<div class="box">
    <div class="box-body">
        <div class="title">
            Thông tin ứng tuyển
            <a onclick="closeForm()" class="btn-close"><i class="fe-x"></i></a>
        </div>
        <hr class="hr-xs" />
        <div class="box-info">
            <div class="row">
                <div class="col-4">
                    <div class="wrap__content ">
                        <div class="box__content text-center">
                            <?php $avatar = $path_dir . $apply['avatar']; ?>
                            <div class="box__avatar">
                                <img src="<?= file_exists($avatar) && $apply['avatar'] != '' ? $avatar : "public/images/avatar-default.jpg"; ?>" class="img-thumbnail rounded-circle d-block mx-auto" alt="avatar" width="180" height="180">
                                <div class="box__status"><i class="icon-check"></i></div>
                            </div>
                            <h3 class="staff_name mt-2">
                                <?= $apply['fullname'] != NULL ? $apply['fullname'] : TEXT_UPDATE; ?></h3>
                        </div>
                        <hr>
                        <div class="box__content ">
                            <div class="form-group">
                                <span class="t_inline_block width_40 text-danger"><i class="fas fa-heart"></i></span>
                                <span class="fs-14px fw-500"> <?= $apply['birthday'] != "" ? $apply['birthday'] : TEXT_EMPTY ?></span>
                            </div>
                            <div class="form-group">
                                <span class="t_inline_block width_40"><i class="icon-envelope-open"></i></span>
                                <span class="fs-14px fw-500"><?= $apply['email'] != "" ? $apply['email'] : TEXT_EMPTY ?></span>
                            </div>
                            <div class="form-group">
                                <span class="t_inline_block width_40"><i class="icon-phone"></i></span>
                                <span class="fs-14px fw-500"><?= $apply['phone'] != "" ? $apply['phone'] : TEXT_EMPTY ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-8">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group  ">
                                <label class="t_color_b9b9b9" for="">Họ và tên:</label>
                                <div class="info_name">
                                    <?= $apply['fullname'] != "" ? $apply['fullname'] : TEXT_EMPTY ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Giới tính:</label>
                                <div class="info_name"><?= $apply['sex'] != 0 ? "Nam" : "Nữ" ?></div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Ngày sinh:</label>
                                <div class="info_name">
                                    <?= $apply['birthday'] != "" ? $apply['birthday'] : TEXT_EMPTY ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Tình trạng hôn nhân:</label>
                                <div class="info_name"><?= $apply['marital'] == 1 ? "Độc thân" : "Đã kết hôn" ?></div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Địa chỉ tạm trú:</label>
                                <div class="info_name">
                                    <?= $apply['apartment_number_temporary'] . "," . $apply['wards_temporary'] . "," . $apply['district_temporary'] . "," . $apply['province_city_temporary'] ?>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label class="t_color_b9b9b9" for="">Vị trí ứng tuyển:</label>
                                <div class="info_name"><?= $apply['position_apply'] != "" ? $apply['position_apply'] : TEXT_EMPTY ?></div>
                            </div>
                        </div>
                    </div>
                    <?php if($detailApply != NULL){ ?>
                        <div class="wrap__exp mt-3">
                            <strong class="t_color_b9b9b9" for="">Kinh nghiệm làm việc:</strong>
                            <div class="box__exp">
                                <?php foreach($detailApply as $key_detail => $val_detail){ ?>
                                    <hr>
                                <div class="item row">
                                    <div class="form-group col-md-6">
                                        <label class="t_color_b9b9b9" for="">Tên công ty:</label>
                                        <span class="fs-14px "> <?= $val_detail['company'] != "" ? $val_detail['company'] : TEXT_EMPTY ?></span>
                                    </div>
                                    <div class="form-group  col-md-6">
                                        <label class="t_color_b9b9b9" for="">Chức vụ:</label>
                                        <span class="fs-14px "><?= $val_detail['position'] != "" ? $val_detail['position'] : TEXT_EMPTY ?></span>
                                    </div>
                                    <div class=" col-md-12">
                                        <label class="t_color_b9b9b9" for="">Mô tả:</label>
                                        <span class="fs-14px "><?= $val_detail['content'] != "" ? $val_detail['content'] : TEXT_EMPTY ?></span>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    function closeForm() {
        $('#loadrecruitmentInfoDetail').html('');
    }
</script>