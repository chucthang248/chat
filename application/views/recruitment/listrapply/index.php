<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?=ROOT_DASHBOARD?></a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title">Cơ sở dữ liệu</h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if($message_flashdata && $message_flashdata['type'] == 'success'){ ?>
    <div id="box-notify" class="jq-toast-wrap bottom-right">
        <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
            <span class="jq-toast-loader jq-toast-loaded"
                style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
            <h2 class="jq-toast-heading">Thông báo!</h2>
            <?php echo $message_flashdata['message']; ?>
        </div>
    </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row" id="listemployee">
        <div class="col-12">
            <div class="card-box table-responsive">
                <div id="loadTable">
                    <table class="table table-bordered table-striped table-hover dt-responsive nowrap mt-3"
                        style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    <div class="checkbox checkbox-success checkbox-single">
                                        <input type="checkbox" id="checkAll" name="publish" value="1"
                                            data-control="<?php echo $control;?>">
                                        <label class="mr-bottom_mn4"></label>
                                    </div>
                                </th>
                                <th>Tên</th>
                                <th>Giới tính</th>
                                <th class="text-center">Ngày sinh</th>
                                <th>Số điện thoại</th>
                                <th>Email</th>
                                <th>Vị trí ứng tuyển</th>
                                <th class="text-center">Ngày ứng tuyển</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if($datas != NULL){ ?>
                            <?php  foreach($datas as $key => $val){ ?>
                                <tr class="tr<?=$val['id'];?>">
                                    <td class="text-center">
                                        <div class="checkbox checkbox-success checkbox-single mt-2">
                                            <input type="checkbox" class="input_checked_single" value="<?=$val['id'];?>"
                                                id="publish" name="publish" data-control="<?php echo $control;?>">
                                            <label></label>
                                        </div>
                                      
                                    </td>
                                    <td >
                                        <?=$val['fullname']; ?>
                                        <a href="javascript:void(0)" onclick="viewEmloyeeInfoDetail(<?=$val['id'];?>)"
                                         class="text-blue float-right mt-1"><i class="icon-eye font-20"></i></a>
                                    </td>
                                    <td >
                                        <?=$val['sex']==1?"Nam": "Nữ"; ?>
                                    </td>
                                    <td class="text-center">
                                        <?=implode("/", array_reverse(explode("-", $val['birthday']))) ?>
                                    </td>
                                    <td >
                                        <?=$val['phone']; ?>
                                    </td>
                                    <td >
                                        <?=$val['email']; ?>
                                    </td>
                                    <td>
                                        <?=$val['position_apply']; ?>
                                    </td>
                                    <td class="text-center"><?=date('d/m/Y',strtotime($val['created_at'])); ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                        <?php } ?>
                    </table>
                    <!-- pagination -->
                    <div class="wrap___flex d-flex t_justify_end ">
                        <div class="ChangeRow">
                        <span>Hiển thị: <strong> <?=$per_page; ?></strong> Trong tổng số: <strong><?=$count?></strong>  </span>
                        </div>
                        <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                            <?=$my_pagination; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->

</div> <!-- end container-fluid -->

<!-- Xem nhanh thông tin ứng tuyển -->
<div id="loadrecruitmentInfoDetail"></div>
<!-- Xem nhanh thông tin ứng tuyển -->
<script type="text/javascript">
const viewDetail = "recruitment/Applylist/viewDetail";
const urlFunction = "recruitment/Applylist/recruitmentFunction";
const urlSearch =   "recruitment/Applylist/recruitmentSeach";
// xuất file excel

let globalFileName  = "";

// PAGINATION
$(document).on('click', '.pagination .forClick a', function() {
    let checkKeyword = $("#search").val(); 
    let url = "<?=base_url()?>"+urlFunction;
    // nếu input tìm kiếm có nội dung hoặc url có giá trị vừa tìm kiếm thì đổi URL
    if(checkKeyword != "" && checkKeyword != undefined )
    {
        url = "<?=base_url()?>"+urlSearch;
    }else
    {
        checkKeyword = "";
    }
    
    let gethref = $(this).attr('href');
    let page = $(this).attr('data-ci-pagination-page');
    history.replaceState(null, null, gethref);
    
    let data = {
        "key_word": checkKeyword,
        "page": page,
        "row": "",
        "type_pageload": "ajax"
    }
    
    CallAjax(data, url);
    event.preventDefault();
});

function CallAjax(data, url) {
    $.ajax({
        url: url,
        type: 'POST',
        data: data,
        success: function(res) {
            $("#loadTable").html(res);
        }
    });
}

function viewEmloyeeInfoDetail(id) {
    $.ajax({
        method: "POST",
        url: viewDetail,
        data: {
            id: id
        },
        dataType: "html",
        success: function(data) {
            if (data) {
                $('#loadrecruitmentInfoDetail').html(data);
            }
        }
    });
}
</script>