<table class="table table-bordered table-striped table-hover dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    <thead>
        <tr>
            <th>Tên người dùng</th>
            <th>Tên bài kiểm tra</th>
            <th>Kết quả</th>
            <th class="text-center">Ngày tạo</th>
            <th>Tác vụ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data_maketest as $key_maketest => $val_maketest) { ?>
            <tr class="tr<?= $val_maketest['id']; ?>">
                <td></td>
                <td>
                    <div class="d-flex t-justifi-bw">
                        <div class="col-md-auto">
                            <?= character_limiter($val_maketest['name'], 100) ?>
                        </div>
                        <div class="col-md-auto">
                            <a target="_blank" href="<?= base_url() . "recruitment/" . $link_approvaltest . "/" . $val_maketest['id'] ?>"><i class="icon-eye font-20"></i></a>
                        </div>
                    </div>
                </td>
                <td>
                    <?=$val_maketest['amoun_correct_question_item']."/".$val_maketest['total_question_item']; ?>
                </td>
                <td class="text-center"><?= $val_maketest['created_at']; ?></td>
                <td class="text-center">
                    <a href="javascript:void(0)" onclick="del_quiz(<?= $val_maketest['id'] ?>,'true','del_approval_quiz','<?= base_url() ?>')" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val_maketest['id'] ?>" data-id="<?php echo $val_maketest['id']; ?>">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end" id="approval_exe_pagination">
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
        <?= $my_pagination_approval; ?>
    </div>
</div>
<!--end pagination -->