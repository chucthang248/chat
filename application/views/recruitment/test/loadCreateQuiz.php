<table class="table table-bordered table-striped table-hover dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
    <thead>
        <tr>
            <th>Tên bài kiểm tra</th>
            <th>Số lượng câu hỏi</th>
            <th class="text-center">Ngày tạo</th>
            <th>Tác vụ</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($allQuiz as $key => $val) { ?>
            <tr class="tr<?= $val['id']; ?>">
                <td>
                    <div class="d-flex t-justifi-bw">
                        <div class="col-md-auto">
                            <?= character_limiter($val['name'], 100) ?>
                        </div>
                        <div class="col-md-auto">
                            <a href="<?=base_url().$link_test."/".$val['id']?>"><i class="icon-eye font-20"></i></a>
                        </div>
                    </div>
                    
                </td>
                <td><?= $val['amount_questions']; ?></td>
                <td class="text-center"><?= $val['created_at']; ?></td>
                <td class="text-center">
                     <a href="javascript:void(0)" onclick="del_quiz(<?= $val['id'] ?>,'true','del_createquiz','<?= $val['base_url'] ?>')" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val['id'] ?>" data-id="<?php echo $val['id']; ?>">
                        <i class="far fa-trash-alt"></i>
                    </a>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<!-- pagination -->
<div class="wrap___flex d-flex t_justify_end " id="create_exe_pagination">
    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
        <?= $my_pagination; ?>
    </div>
</div>
<!--end pagination -->