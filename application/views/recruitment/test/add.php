<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?=$title;?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?=$title;?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->

    <div class="row">
        <div class="col-md-8">
            <div class="card-box">
                <form method="POST" class="add_edit_question" action="" onsubmit="return checkSubmit()"  id="myForm" enctype="multipart/form-data" >
                    <div class="form-group">
                        <select required="" name="data_post[type_questionID]" id=""  class="form-control" >
                            <option value="">--- Chọn loại câu hỏi ---</option>
                            <?php if($type_question != NULL ) { ?>
                                <?php foreach($type_question as $key => $val){ ?>
                                    <option value="<?=$val['id'] ?>"><?=$val['name'] ?></option>
                               <?php } ?>
                            <?php  } ?>    
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea  required=""  placeholder="Nhập câu hỏi..." name="data_post[question]"  class="form-control ckeditor"  id="" cols="20" rows="10"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="hidden"  id="correct_answer" value="" name="data_post[correct_answer]">
                        <div class="row form-group">
                            <div class="col-1"></div>
                            <div class="col-8">
                                <label class="txt_lb" for="">Nhập các đáp án</label>
                            </div>
                            <div class="col-1">
                                <label class="txt_lb"  for="">Sắp xếp</label>
                            </div>
                            <div class="col-2">
                                <label class="txt_lb"  for="">Chọn đáp án</label>
                            </div>
                        </div>
                        <?php $question_arr = [["qt"=> "A"],["qt"=> "B"],["qt"=> "C"],["qt"=> "D"]] ; 
                                foreach($question_arr as $key_qt => $val_qt){  ?>
                            <div class="form-group">
                                    <input type="hidden" value="<?=$val_qt["qt"]?>" name="data_post_answer[<?=$key_qt?>][alph]">
                                    <div class="row">
                                        <div class="col-md-1">
                                            <label for="">Câu <?=$val_qt["qt"]?></label>
                                        </div>
                                        <div class="col-md-8">
                                            <textarea required="" placeholder="Nhập câu trả lời..." name="data_post_answer[<?=$key_qt?>][answer]" class="form-control answer_txt" id=""  rows="10"></textarea>
                                        </div>
                                        <div class="col-md-1">
                                            <input type="number" placeholder="STT" class="form-control" name="data_post_answer[<?=$key_qt?>][sort]">
                                        </div>
                                        <div class="col-md-2 d-flex  align-items-center">
                                                <div class="radio form-check-inline t-radio-check ">
                                                    <input type="radio" value="<?=$val_qt["qt"]?>" name="choice" >
                                                    <label for=""></label>
                                                </div>  
                                                <span>Đáp án <?=$val_qt["qt"]?></span>
                                        </div>
                                    </div>
                            </div>
                        <?php } ?>
                    </div>
                    <div class="box__tools ">
                        <button class="btn btn-primary waves-effect waves-light ml-2 mr-1" type="submit"><i
                                class="far fa-save"></i> Lưu</button>
                        <a href="<?="recruitment/".$control?>" class="btn btn-dark waves-effect waves-light mr-1"><i class="fas fa-backspace"></i> Hủy</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link href="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
<script src="public/assets/libs/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<!-- sweetalert -->
<script src="public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
<!-- Tost-->
<script src="public/assets/libs/jquery-toast/jquery.toast.min.js"></script>
<script>
    // Kiểm tra xem người dùng đã chọn câu đúng hay chưa
    function checkSubmit()
    {
        let getCorrect = $("#correct_answer").val();
        if(getCorrect == "")
        {
            Swal.fire({
                title: "Vui lòng chọn đáp án đúng",
                type: "warning",
                showCancelButton: false,
                cancelButtonColor: "#d33",
            });
            return false;
        }
        return true;
    }
    $(document).ready(function(){
        $("input[type='radio']").click(function(){
            var radioValue = $("input[name='choice']:checked").val();
            if(radioValue){
                $("#correct_answer").val(radioValue);
            }
        });
    });
    CKEDITOR.replace('data_post[question]', {
        toolbar: [
            [ 'Bold', 'Italic', '-', 'NumberedList', 'BulletedList', '-', 'Link', 'Unlink' ],
            [ 'FontSize', 'TextColor', 'BGColor' ]
        ]
    });
</script>