<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />

<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);"><?= ROOT_DASHBOARD ?></a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->

    <div class="row" id="listemployee">
        <div class="col-12">
            <div class="card-box table-responsive">
                <ul class="nav nav-tabs mt-3 mb-3" id="myTab">
                    <li class="nav-item">
                        <a href="#list_question" data-toggle="tab" aria-expanded="true" class="nav-link active">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Danh sách câu hỏi</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#create_exe" data-toggle="tab" aria-expanded="true" class="nav-link ">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Tạo bài kiểm tra</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#approval_exe" data-toggle="tab" aria-expanded="true" class="nav-link ">
                            <span class="d-block d-sm-none"><i class="mdi mdi-account"></i></span>
                            <span class="d-none d-sm-block">Duyệt bài kiểm tra</span>
                        </a>
                    </li>
                </ul>
                <!-- tab content -->
                <div class="tab-content">
                    <div class="tab-pane active " id="list_question">
                        <div class="search-container">
                            <form action="" method="GET">
                                <div class="row">
                                    <div class="col-md-auto ">
                                        <a href="<?= "recruitment/" . $control ?>/add" type="button" class="btn btn-blue waves-effect waves-light">
                                            <i class="icon-plus mr-1"></i> Thêm mới
                                        </a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div id="loadTable" class="mt-2">
                            <table id="datatable" class="table table-bordered table-striped table-hover dt-responsive nowrap mt-3" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Loại câu hỏi</th>
                                        <th>Câu hỏi</th>
                                        <th class="text-center">Ngày tạo</th>
                                        <th class="text-center">Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if ($datas != NULL) { ?>
                                        <?php foreach ($datas as $key => $val) { ?>
                                            <tr class="tr<?= $val['id']; ?>">
                                                <td>
                                                    <?= $val['type_question']; ?>
                                                </td>
                                                <td>
                                                    <?= character_limiter($val['question'], 100) ?>
                                                </td>
                                                <td class="text-center"><?= $val['created_at']; ?></td>
                                                <td class="text-center">
                                                    <a href="<?= "recruitment/" . $control ?>/edit/<?= $val['id'] ?>" class="btn btn-blue btn-xs waves-effect waves-light btn-xs">
                                                        <i class="far fa-edit"></i>
                                                    </a>
                                                    <a href="javascript:void(0)" onclick="del_quiz(<?= $val['id'] ?>,'true','del_question','<?= $val['base_url'] ?>')" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val['id'] ?>" data-id="<?php echo $val['id']; ?>">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                </tbody>
                            <?php } ?>
                            </table>

                        </div>
                    </div>
                    <!-- tạo bài kiểm tra -->
                    <div class="tab-pane" id="create_exe">
                        <div class="row ">
                            <div class="col-md-2">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Tên bài kiểm tra...">
                            </div>
                            <div class="col-md-auto">
                                <input type="number" class="form-control" id="amount" name="amount" placeholder="Số lượng câu hỏi...">
                            </div>
                            <div class="col-md-auto">
                                <button type="button" onclick="randomExe()" class="btn btn-blue waves-effect waves-light ">Tạo bài kiểm tra</button>
                            </div>
                        </div>
                        <div class="mt-2" id="loadTableExe">
                            <table class="table table-bordered table-striped table-hover dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead>
                                    <tr>
                                        <th>Tên bài kiểm tra</th>
                                        <th>Số lượng câu hỏi</th>
                                        <th class="text-center">Ngày tạo</th>
                                        <th>Tác vụ</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php foreach ($allQuiz as $key => $val) { ?>
                                            <tr class="tr<?= $val['id']; ?>">
                                                <td>
                                                    <div class="d-flex t-justifi-bw">
                                                        <div class="col-md-auto">
                                                            <?= character_limiter($val['name'], 100) ?>
                                                        </div>
                                                        <div class="col-md-auto">
                                                            <a href="<?=base_url().$link_test."/".$val['id']?>"><i class="icon-eye font-20"></i></a>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td><?= $val['amount_questions']; ?></td>
                                                <td class="text-center"><?= $val['created_at']; ?></td>
                                                <td class="text-center">
                                                    <a href="javascript:void(0)" onclick="del_quiz(<?= $val['id'] ?>,'true','del_createquiz','<?= base_url() ?>')" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val['id'] ?>" data-id="<?php echo $val['id']; ?>">
                                                        <i class="far fa-trash-alt"></i>
                                                    </a>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                </tbody>
                            
                            </table>
                              <!-- pagination -->
                                <div class="wrap___flex d-flex t_justify_end " id="create_exe_pagination">
                                    <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                                        <?= $my_pagination; ?>
                                    </div>
                                </div>
                             <!--end pagination -->
                        </div>
                    </div>
                     <!-- Duyệt bài kiểm tra -->
                     <div class="tab-pane" id="approval_exe">   
                            <div class="mt-2" id="loadTableApprovalExe">
                                <table class="table table-bordered table-striped table-hover dt-responsive nowrap " style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>Tên người dùng</th>
                                            <th>Tên bài kiểm tra</th>
                                            <th>Kết quả</th>
                                            <th class="text-center">Ngày tạo</th>
                                            <th>Tác vụ</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                            <?php foreach ($data_maketest as $key_maketest => $val_maketest) { ?>
                                                <tr class="tr<?= $val_maketest['id']; ?>">
                                                    <td></td>
                                                    <td>
                                                        <div class="d-flex t-justifi-bw">
                                                            <div class="col-md-auto">
                                                                <?= character_limiter($val_maketest['name'], 100) ?>
                                                            </div>
                                                            <div class="col-md-auto">
                                                                <a target="_blank" href="<?=base_url()."recruitment/".$link_approvaltest."/".$val_maketest['id']?>"><i class="icon-eye font-20"></i></a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <?=$val_maketest['amoun_correct_question_item']."/".$val_maketest['total_question_item']; ?>
                                                    </td>
                                                    <td class="text-center"><?= $val_maketest['created_at']; ?></td>
                                                    <td class="text-center">
                                                        <a href="javascript:void(0)" onclick="del_quiz(<?= $val_maketest['id'] ?>,'true','del_approval_quiz','<?= base_url() ?>')" data-control="<?= $control ?>" class="btn btn-danger btn-xs waves-effect waves-light delete<?= $val_maketest['id'] ?>" data-id="<?php echo $val_maketest['id']; ?>">
                                                            <i class="far fa-trash-alt"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                    </tbody>
                                
                                </table>
                                <!-- pagination -->
                                    <div class="wrap___flex d-flex t_justify_end" id="approval_exe_pagination">
                                        <div class="dataTables_paginate paging_simple_numbers d-block ml-auto" id="datatable_paginate">
                                            <?= $my_pagination_approval; ?>
                                        </div>
                                    </div>
                                <!--end pagination -->
                            </div>                     
                     </div>
                </div>

            </div>
        </div>
    </div> <!-- end row -->
    <!-- end row -->
</div> <!-- end container-fluid -->
<div id="loadrecruitmentInfoDetail"></div>
<style>
.t-justifi-bw
{
    justify-content: space-between;
}
</style>
<script type="text/javascript">
// RANDOM CÂU HỎI & TẠO BÀI KIỂM
const create_quiz = "recruitment/test/create_quiz";
function randomExe() {
    let name = $("#name").val();
    let amount = $("#amount").val();
    if (name == "" || amount == "" ) {
        Swal.fire({
            title: "Vui lòng nhập tên bài kiểm tra, số lượng câu hỏi!",
            type: "warning",
            showCancelButton: false,
            cancelButtonColor: "#d33",
         });
    } else {
        $.ajax({
            type: "POST",
            url: create_quiz,
            data: {
                name: name,
                amount: amount
            },
            success: function(response) {
                // let check = JSON.parse(response);
                $("#loadTableExe").html(response);
                $.toast({
                    heading: 'Thông báo!',
                    text: 'Tạo bài kiểm tra thành công.',
                    position: 'top-right',
                    loaderBg: '#5ba035',
                    icon: 'success',
                    hideAfter: 2000,
                });
            }
        });
    }
    // thông báo

}
// XÓA TẠO BÀI KIỂM TRA
function del_quiz(id,type = null,action,crossDomain = null) {
    // debugger;
    Swal.fire({
        title: "Bạn có chắc chắn muốn xóa không?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Đồng ý"
        }).then(function (result) {
        if (result.value) {
            var control = $('.delete'+id).attr('data-control');
            if(id != '')
            {
                if(type == "true")
                {
                    $.ajax
                    ({
                        method: "POST",
                        crossDomain: true,
                        url: "recruitment/"+control+"/"+action,
                        data: {id:id},
                        success: function(data)
                        { 
                            $.ajax
                            ({
                                method: "POST",
                                url: "recruitment/"+control+"/delete",
                                data: { id:id},
                                success: function(data)
                                {
                                    if (data != "") {
                                        let parseData = JSON.parse(data);
                                        if (parseData.type == "error") {
                                            Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                                        }
                                    } else {
                                        $('.delete'+id).parent().parent().fadeOut();
                                        Swal.fire("Xóa thành công!", "", "success");
                                        $(".tr" + id).remove();
                                    }
                                }
                            });    
                        }
                    });
                }
                else
                {
                    $.ajax
                    ({
                        method: "POST",
                        url: "recruitment/"+control+"/delete",
                        data: { id:id},
                        success: function(data)
                        {
                            if (data != "") {
                                let parseData = JSON.parse(data);
                                if (parseData.type == "error") {
                                    Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                                }
                            } else {
                                $('.delete'+id).parent().parent().fadeOut();
                                Swal.fire("Xóa thành công!", "", "success");
                                $(".tr" + id).remove();
                            }
                        }
                    });    
                }
                
            }
        }
    });
}


// PHÂN TRANG TẠO BÀI KIỂM TRA
const urlPagination = "recruitment/test/pagination_create_quiz";
const urlSearch = "recruitment/test/search_create_quiz";
// PHÂN TRANG DUYỆT BÀI KIỂM TRA
const urlPagination_approval = "recruitment/test/pagination_approval_quiz";
    // xuất file excel
    let globalFileName = "";
    // PHÂN TRANG (TẠO BÀI KIỂM TRA)
    $(document).on('click', '#create_exe_pagination .pagination .forClick a', function() {
        let checkKeyword = $("#search").val();
        let recruitmentID =  $("#recruitmentID").val();
        let url = "<?= base_url() ?>" + urlPagination;
        // nếu input tìm kiếm có nội dung hoặc url có giá trị vừa tìm kiếm thì đổi URL
        if (checkKeyword != "" && checkKeyword != undefined || 
            recruitmentID != "" && recruitmentID != undefined ) {
            url = "<?= base_url() ?>" + urlSearch;
        } else {
            checkKeyword = "";
            recruitmentID = "";
        }
        
        let gethref = $(this).attr('href');
        let page = $(this).attr('data-ci-pagination-page');
        let data = {
            "key_word": checkKeyword,
            "recruitmentID" :recruitmentID,
            "page": page,
            "row": "",
            "type_pageload": "ajax"
        }

        CallAjax(data, url,"#loadTableExe");
        event.preventDefault();
    });
     // PHÂN TRANG (DUYỆT BÀI KIỂM TRA)
     $(document).on('click', '#approval_exe_pagination .pagination .forClick a', function() {
         console.log("ok");
        let checkKeyword = $("#search").val();
        let recruitmentID =  $("#recruitmentID").val();
        let url = "<?= base_url() ?>" + urlPagination_approval;
        // nếu input tìm kiếm có nội dung hoặc url có giá trị vừa tìm kiếm thì đổi URL
        if (checkKeyword != "" && checkKeyword != undefined || 
            recruitmentID != "" && recruitmentID != undefined ) {
            url = "<?= base_url() ?>" + urlSearch;
        } else {
            checkKeyword = "";
            recruitmentID = "";
        }
        
        let gethref = $(this).attr('href');
        let page = $(this).attr('data-ci-pagination-page');
        let data = {
            "page": page,
        }

        CallAjax(data, url,"#loadTableApprovalExe");
        event.preventDefault();
    });
    function CallAjax(data, url,htmlLoad = null) {
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function(res) {
                //console.log("ok");
                $(htmlLoad).html(res);
            }
        });
    }

</script>
