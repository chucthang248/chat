<!DOCTYPE html>
<html lang="en">
    <head>
    	<base href="<?php echo site_url(); ?>" />
        <meta charset="utf-8" />
        <title>Dashboard | Hưng Thịnh HR</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
        <meta content="Coderthemes" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- App favicon -->
        <link rel="shortcut icon" href="public/images/favicon.jpg">

        <!-- C3 Chart css -->
        <link href="public/assets/libs/c3/c3.min.css" rel="stylesheet" type="text/css" />

        <!-- App css -->
        <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
        <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css"  id="app-stylesheet" />
        <link href="public/assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="public/assets/js/calendar/main.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="public/assets/css/recruitment.css">
        <!-- Vendor js -->
        <script src="public/assets/js/vendor.min.js"></script>
        <!-- Calendar init -->
        <script src="public/assets/js/calendar/main.js"></script>
        <script src="public/assets/js/calendar/custom.js"></script>
        <!-- Ckeditor -->
        <script type="text/javascript" language="javascript" src="vendors/ckeditor/ckeditor.js" ></script>
        <!-- socket io -->
        <script src="https://cdn.socket.io/3.1.3/socket.io.min.js" ></script>
        <?php
            $url = "http://work.hungthinh1989.com:3005";
                if($_SERVER["HTTP_HOST"] == 'localhost')
                {
                    $url = "http://localhost:3005";
                }
            ?>
        <script>
             var socket = io('<?= $url?>');
                $(document).ready(function(){
                    socket.emit("send_pageLoad_apply");
                    socket.on("receive_pageLoad_apply", (data) => {
                     
                        count_apply = data.length;
                        $(".count_apply_box").html(count_apply);
                        $(".count_apply").html(`<span class="badge badge-pink rounded-circle noti-icon-badge ">${count_apply}</span>`);
                            console.log(data.length);
                              // load data
                        $(".apply_item_alert").html("");
                        if( data.length == 0)
                        {
                            $(".empty_apply").hide();
                        }else
                        {
                            $(".empty_apply").show();
                        }
                     
                        $(data).each(function(key,obj){
                          
                            $(".apply_item_alert").append(`
                            <a href="javascript:void(0);" onclick="view('${obj.id}')" class="dropdown-item notify-item">
                                <div class="notify-icon bg-success"><i class="mdi mdi-comment-account-outline"></i></div>
                                <p class="notify-details">${obj.fullname}<small class="text-muted">${obj.created_at}</small></p>
                            </a>
                            `);
                        });
                     });
                      
                       
                });
        </script>
    </head>

    <body>

        <!-- Begin page -->
        <div id="wrapper">
            <!-- Topbar Start -->
            <div class="navbar-custom">
                <ul class="list-unstyled topnav-menu float-right mb-0">
                    <!-- notify -->
                    <li class="dropdown notification-list">
                        <?php $this->load->view('cpanel/layout/notify');?>
                    </li>
                    <!-- notify End -->

                    <li class="dropdown notification-list">
                        <?php $this->load->view('cpanel/layout/account');?>
                    </li>

                    <li class="dropdown notification-list">
                        <a href="javascript:void(0);" class="nav-link right-bar-toggle waves-effect waves-light">
                            <i class="fe-settings noti-icon"></i>
                        </a>
                    </li>
                </ul>

                <!-- LOGO -->
                <?php $this->load->view('cpanel/layout/logo');?>
                <!-- LOGO END -->
                <!-- Search -->
                <?php $this->load->view('cpanel/layout/search');?>
                <!-- Search END -->
            </div>
            <!-- end Topbar -->
            
            <!-- ========== Left Sidebar Start ========== -->
            <?php $this->load->view('recruitment/layout/menu');?>
            <!-- Left Sidebar End -->

            <!-- ============================================================== -->
            <!-- Start Page Content here -->
            <!-- ============================================================== -->

            <div class="content-page">
                <div class="content">
                	<?php
		                if(isset($template) && !empty($template)){
		                    $this->load->view($template, isset($data)?$data:NULL);
		                }
		            ?>
                </div> <!-- end content -->
                <!-- Footer Start -->
                <?php $this->load->view('cpanel/layout/footer');?>
                <!-- end Footer -->

            </div>

            <!-- ============================================================== -->
            <!-- End Page content -->
            <!-- ============================================================== -->

        </div>
        
        <!-- END wrapper -->
        <!-- Right bar overlay-->
        <div class="rightbar-overlay"></div>
        
        <!-- Required datatable js -->
		<script src="public/assets/libs/datatables/jquery.dataTables.min.js"></script>
		<script src="public/assets/libs/datatables/dataTables.bootstrap4.min.js"></script>
		<!-- Responsive examples -->
        <script src="public/assets/libs/datatables/dataTables.responsive.min.js"></script>
        <script src="public/assets/libs/datatables/responsive.bootstrap4.min.js"></script>
        <!-- sweetalert -->
        <script src="public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
        <!-- Tost-->
        <script src="public/assets/libs/jquery-toast/jquery.toast.min.js"></script>
        <!-- Datatables init -->
        <script src="public/assets/js/pages/datatables.init.js"></script>
        <!-- Plugin js-->
        <script src="public/assets/libs/parsleyjs/parsley.min.js"></script>
        <!-- Validation init js-->
        <script src="public/assets/js/pages/form-validation.init.js"></script>
        
        <!-- App js -->
        <script src="public/assets/js/app.min.js"></script>
        <script src="public/assets/js/my_scripts.js"></script>
    </body>
</html>
