<!DOCTYPE html>
<html lang="en">

<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tuyển dụng</title>
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/images/favicon.webp">
    <!-- C3 Chart css -->
    <link href="public/assets/libs/c3/c3.min.css" rel="stylesheet" type="text/css" />
    <!-- App css -->
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
    <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="public/assets/css/recruitment.css">
</head>
<body>
    <div class="wrap_recruitment">
        <div class="bg">
            <div class="boxname-recruit">
                <img class="icon_recruit" src="public/images/tuyendung.webp" alt="">
            </div>
            <?php if (isset($register) && !empty($register)) {  $mess_register = $this->session->flashdata('mess_register');  ?>
                <div class="recruitmentNotify">
                    <img class="icCheck" src="public/images/icons/check.png" alt="" />
                    <p>Chúc mừng <strong><?=$mess_register['name'] ?></strong> đã gửi thông tin ứng tuyển vị trí <strong><?=$mess_register['position'] ?></strong> thành công.</p>
                    <p>Chúng tôi sẽ liên hệ với bạn sớm nhất. Xin cảm ơn!</p>
                </div>
            <?php } else {  ?>
                <div class="wrap_form">
                    <form method="POST" action="register" class="parsley-examples" onsubmit="return SubmitForm();" id="myForm" enctype="multipart/form-data">
                        <div class="enter-info">
                            <div class="check-info  item_recruit">
                                <label>1</label>
                                <h4 class="top_title mb-2">Vui lòng chọn địa điểm bên dưới</h4>
                                <div class="row">
                                    <div class="col-md-6 res_mr">
                                        <select onchange="selectedCity(this)" name="data_post_apply[province_city]" class="address_selected ml-2 width_150 form-control" id="">
                                            <?php foreach ($city['LtsItem'] as $key_city => $city) { ?>
                                                <option value="<?= $city['ID'] ?>"><?= $city['Title'] ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6 ">
                                        <select onchange="selectedDistrict(this)" name="data_post_apply[district]" class="address_selected ml-2 width_150 form-control" id="district">
                                            <option value="">Chọn quận/huyện</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="check-info  item_recruit">
                                <label>2</label>
                                <h4 class="showinfo show_line  mb-2 close__form" data-name="show_info">Thông tin cá nhân</h4>
                                <div id="show_info">
                                    <div class="frame_info form-info row form-group mt-2">
                                        <div class="col-md-4">
                                            <strong class="" for="">Giới tính</strong>
                                            <div class="group_radio d-flex radio">
                                                <div class="item_radio_sx mr-4 cursor-pointer">
                                                    <input type="radio" id="male" name="data_post_apply[sex]" checked value="1">
                                                    <label for="male">Nam</label><br>
                                                </div>
                                                <div class="item_radio_sx cursor-pointer">
                                                    <input type="radio" id="female" name="data_post_apply[sex]" value="2">
                                                    <label for="female">Nữ</label><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <strong class="" for="">Tình trạng</strong>
                                            <div class="group_radio d-flex radio">
                                                <div class="item_radio_sx mr-4 cursor-pointer ">
                                                    <input type="radio" id="alone" name="data_post_apply[marital]" checked value="1">
                                                    <label for="alone">Độc thân</label><br>
                                                </div>
                                                <div class="item_radio_sx cursor-pointer">
                                                    <input type="radio" id="married" name="data_post_apply[marital]" value="2">
                                                    <label for="married">Đã kết hôn</label><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="frame_info form-group form-info parent_find">
                                        <strong>Họ và tên đầy đủ của bạn</strong>
                                        <input type="text" value="" class="ip_text form-control  itemValue" name="data_post_apply[fullname]" placeholder="Nhập họ và tên">
                                        <div class="alert_messenger" data-alert="Vui lòng nhập họ và tên của bạn"> </div>
                                    </div>
                                    <div class="frame_info form-group form-info">
                                        <div class="row">
                                            <div class="col-md-6 parent_find">
                                                <strong>Ngày tháng năm sinh</strong>
                                                <input type="text" value="" class="ip_text form-control  itemValue" name="data_post_apply[birthday]" placeholder="VD: 24/08/1998" data-toggle="input-mask" data-mask-format="00/00/0000">
                                                <div class="alert_messenger" data-alert="Vui lòng nhập ngày sinh của bạn"> </div>
                                            </div>
                                            <div class="col-md-6 parent_find">
                                                <strong>CMND/ căn cước:</strong>
                                                <input type="text" value="" class="ip_text form-control  itemValue" name="data_post_apply[identify_number]" placeholder="CMND / Căn cước">
                                                <div class="alert_messenger" data-alert="Vui lòng nhập căn cước/cmnd của bạn"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="frame_info form-group form-info ">
                                        <strong>Địa chỉ tạm trú hiện nay</strong>
                                        <div class="row address_temporary form-group">
                                            <div class="col-md-6 parent_find">
                                                <select onchange="selectedCityTemporary(this)" name="data_post_apply[province_city_temporary]"  class="address_selected ml-2 width_150 form-control itemValue" id="">
                                                        <option value="">Chọn tỉnh/ thành phố</option> 
                                                    <?php foreach ($city_temporary['LtsItem'] as $key_city => $city) { ?>
                                                        <option data-citytemporaryid="<?= $city['ID'] ?>" value="<?= $city['ID'] ?>"><?= $city['Title'] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div class="alert_messenger"  data-alert="Vui lòng chọn tỉnh/thành"> </div>
                                            </div>
                                            <div class="col-md-6 parent_find">
                                                <select onchange="selectedDistrictTemporary(this)" id="district_temporary" name="data_post_apply[district_temporary]" class="address_selected ml-2 width_150 form-control itemValue" id="">
                                                     <option value="">Chọn quận/huyện</option>
                                                </select>  
                                                <div class="alert_messenger"  data-alert="Vui lòng chọn quận huyện"> </div>
                                            </div>                    
                                        </div>
                                        <div class="row address_temporary form-group">
                                            <div class="col-md-6 parent_find">
                                                <select name="data_post_apply[wards_temporary]" id="wards_temporary" class="address_selected ml-2 width_150 form-control itemValue" id="">
                                                     <option value="">Chọn phường xã</option>
                                                </select> 
                                                <div class="alert_messenger"  data-alert="Vui lòng chọn phường xã"> </div>
                                            </div>
                                            <div class="col-md-6 parent_find">
                                                <input type="text" value="" class="form-control itemValue" name="data_post_apply[apartment_number_temporary]" placeholder="Số nhà, tên đường">
                                                <div class="alert_messenger"  data-alert="Vui lòng nhập số nhà, tên đường"> </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="frame_info form-group form-info parent_find">
                                        <strong>Nhập số điện thoại của bạn</strong>
                                        <input type="text" value="" id="phone" class="ip_text form-control  itemValue" name="data_post_apply[phone]" placeholder="Nhập số điện thoại của bạn">
                                        <div class="alert_messenger alert_phone" data-typecheck="validate" data-alert="Vui lòng nhập số điện thoại của bạn"> </div>
                                    </div>                      
                                    <div class="frame_info form-group form-info parent_find">
                                        <strong>Nhập email của bạn</strong>
                                        <input type="text" value="" id="email" class="ip_text form-control  itemValue" name="data_post_apply[email]" placeholder="Nhập email của bạn">
                                        <div class="alert_messenger alert_email" data-typecheck="validate" data-alert="Vui lòng nhập email của bạn"> </div>
                                    </div>
                                    <?php if(isset($recruitments) && $recruitments != NULL){ ?>
                                    <div class="frame_info form-group form-info parent_find">
                                        <strong>Chọn vị trí ứng tuyển</strong>
                                        <select name="data_post_apply[positionID]" class="address_selected ml-2 width_150 form-control itemValue" id="">
                                            <option value="">Vị trí ứng tuyển</option>
                                                <?php foreach ($recruitments as $key_recruitment => $val_recruitment) { ?>
                                                    <option value="<?=$val_recruitment['id'];?>"><?=$val_recruitment['title'];?></option>
                                                <?php } ?>
                                            <?php ?>
                                            
                                        </select>
                                        <div class="alert_messenger" data-alert="Vui lòng chọn vị trí ứng tuyển"> </div>
                                    </div>
                                    <?php } ?>
                                    <div class="frame_info form-group form-info parent_find">
                                        <strong>Chọn bằng cấp</strong>
                                        <select name="data_post_apply[standardID]" class="address_selected ml-2 width_150 form-control itemValue" id="">
                                            <?php if ($standard != NULL) { ?>
                                                <?php foreach ($standard as $key_standard => $val_standard) { ?>
                                                    <option value="<?= $val_standard['id'] ?>"><?= $val_standard['name'] ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <div class="alert_messenger" data-alert="Vui lòng chọn bằng cấp"> </div>
                                    </div>
                                    <div class="frame_info form-group form-info">
                                        <strong>Giới thiệu về bản thân</strong>
                                        <textarea name="data_post_apply[introduce]" class=" form-control" id="" cols="30" rows="10"></textarea>
                                    </div>
                                    <div class=" frame_info form-group form-info radio parent_find">
                                        <strong>Ảnh chân dung rõ mặt</strong>
                                        <div class="row">
                                            <div class="col-6 col-md-6 ">
                                                <button class="btn_choice btn btn-blue waves-effect waves-light">Chọn hình
                                                    trong máy</button>
                                                <input type="file" id="file_mobile" name="avatar" class="file_mobile itemValue">
                                                <div class="hidden_img mt-2">
                                                    <div class="load__choice_img d-flex">
                                                        <img src="" id="box_img_mobile">
                                                        <div class="text_choice">
                                                            <a href="javascript:void(0)" class="del_red">Xóa ảnh</a>
                                                            <div class="line_padding"></div>
                                                            <div class="wrap__choice">
                                                                <a href="javascript:void(0)" class="choice_green choice_another_img">Chọn ảnh
                                                                    khác</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="alert_messenger image_alert" data-typecheck="validate" data-alert="Vui lòng chọn hình"> </div>
                                            </div>
                                            <div class="col-6 col-md-6 ">
                                                <button class="btn_choice btn btn-blue waves-effect waves-light">Chọn từ
                                                    camera</button>
                                            </div>
                                        </div>
                                        <a href="javascript:void(0)" class="btn btn-blue waves-effect waves-light mt-2" id="btn_continue">Tiếp tục</a>
                                    </div>
                                </div>
                            </div>
                            <!-- tiếp tục -->
                            <div class="check-info   item_recruit">
                                <label>3</label>
                                <h4 class="showexp show_line  mb-2 close__form" data-name="wrap__experience">Kinh nghiệm làm việc</h4>
                                <div id="show_experience">
                                    <div class="checked_exper mt-2">
                                        <label class="container_experience">Chưa có
                                            <input onchange="notExp(this)" type="checkbox" id="not_exp">
                                            <span class="checkmark_experience"></span>
                                        </label>
                                    </div>
                                    <div class="wrap__experience mt-2">
                                        <div class="item__child">
                                            <strong>Cách 1: nếu đã có CV thể hiện quá trình làm việc trước kia,
                                                bạn có thể
                                            </strong>
                                            <div class="choice_cv_file form-exp mt-2">
                                                <div class="sendcv">
                                                    <div class="attach check-info_input">
                                                        <span>Đính kèm CV</span>
                                                        <div class="choosefile">
                                                            <small>PDF, doc, docx. Tối đa 10MB</small>
                                                            <a href="javascript:;">Chọn file</a>
                                                            <span id="Cv_filename"></span>
                                                            <input type="file" id="CVFile" name="data_cv_file">
                                                        </div>
                                                        <div class="alert_messenger cv_alert" data-typecheck="validate" data-alert="Vui lòng chọn hình"> </div>
                                                        <div class="delfile">
                                                            <a href="javascript:void(0)" onclick="delCv()" class="del">Xóa </a>
                                                            <a href="javascript:void(0)" onclick="$('#CVFile').trigger('click');">| Chọn file khác</a>
                                                        </div>
                                                    </div>
                                                    <div class="either">
                                                        <span>Hoặc</span>
                                                    </div>
                                                    <div class="attach linksend">
                                                        <span>Link gửi CV</span>
                                                        <div class="inputbox">
                                                            <input type="text" id="CVLink" name="data_cv_link" placeholder="http://">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item__child">
                                            <strong>Nếu chưa có CV, bạn có thể chia sẻ kinh nghiệm làm việc của mình dưới đây</strong>
                                            <div class="content_exp form-exp mt-2">
                                                <div class="alotcomp" id="multiple_company">
                                                    <div class="box-company">
                                                        <a href="javascript:void(0)" onclick="close_exp(this)" class="a_close_exp"><i class="fas fa-times-circle"></i></a>
                                                        <input type="text" id="CompanyName1" class="form-control" name="data_post_apply_exp[0][company]" placeholder="Tên công ty">
                                                        <input type="text" id="Position1" class="form-control" name="data_post_apply_exp[0][position]" placeholder="Chức vụ (nhân viên, trưởng phòng)">
                                                        <textarea id="Note1" class="form-control" name="data_post_apply_exp[0][content]" placeholder="Thời gian làm công việc chính: Lý do nghỉ: Người tham khảo, tel (không bắt buộc)"></textarea>
                                                    </div>
                                                </div>
                                                <a href="javascript:;" class="addcomp" onclick="Company()">+ Thêm công ty khác</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="send-info mt-3">
                                        <button type="submit" href="javascript:;" class="send btn btn-blue waves-effect waves-light">Gửi thông tin ứng tuyển</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            <?php } ?>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- form mask js -->
    <script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
</body>
<script>
    // ngày sinh
    $(document).ready(function() {
        $('[data-toggle="input-mask"]').each(function(idx, obj) {
            var maskFormat = $(obj).data("maskFormat");
            var reverse = $(obj).data("reverse");
            if (reverse != null)
                $(obj).mask(maskFormat, {
                    'reverse': reverse
                });
            else
                $(obj).mask(maskFormat);
        });
    });
    // chọn tỉnh/ thành phố
    function selectedCity(_this) {
        let city_id = _this.value;
        $.ajax({
            url: "district.html",
            type: "POST",
            data: {
                city_id: city_id
            },
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    $("#district").html("");
                    $("#district").append(`<option value="">Chọn quận/huyện</option>`);
                    $(res.data).each((key, obj, i) => {
                        $("#district").append(`<option value="${obj.ID}">${obj.Title}</option>`);
                    });
                }
            }
        });
    }
    // chọn quận huyện
    function selectedDistrict(_this) {
        if (_this.value != "") {
            $("#show_info").fadeIn();
            $(".showinfo").addClass("active_change");
        }
    }
    // địa chỉ tạm trú (chọn thành phố)
    function selectedCityTemporary(_this)
    {
        let city_id = _this.value;
        $.ajax({
            url: "district.html",
            type: "POST",
            data: {
                city_id: city_id
            },
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    $("#district_temporary").html("");
                    $("#district_temporary").append(`<option value="">Chọn quận/huyện</option>`);
                    $(res.data).each((key, obj, i) => {
                        $("#district_temporary").append(`<option value="${obj.ID}">${obj.Title}</option>`);
                    });
                }
            }
        });
    }
    // dia95 chi3 tam5 trú (chọn quận huyện)
    function selectedDistrictTemporary(_this)
    {
        let district_id = _this.value;
        $.ajax({
            url: "wards.html",
            type: "POST",
            data: {
                district_id: district_id
            },
            dataType: "JSON",
            success: function(res) {
                if (res) {
                    $("#wards_temporary").html("");
                    $("#wards_temporary").append(`<option value="">Chọn phường xã</option>`);
                    $(res.data).each((key, obj, i) => {
                        $("#wards_temporary").append(`<option value="${obj.ID}">${obj.Title}</option>`);
                    });
                }
            }
        });
    }
    // click tiếp tục 
    $(document).on("click", "#btn_continue", function() {
        let flagForm = validateTop_form();
        if (flagForm.length == 0) {
            $("#show_experience").fadeIn();
            $(".wrap__experience").fadeIn();
            $(".showexp").addClass("active_change");
        }
    });
    // submit form 
    let checkDv = true;

    function SubmitForm() {
        let cvFile = document.getElementById("not_exp").checked;
        let cvFile_value = $("#CVFile").val();
        let validateForm = validateTop_form();
        
        if (validateForm.length != 0) {
            return false;
        }
        return true;
    }
    // check các điều kiện (validate.v..v) trước khi nhấn tiếp tục
    // check phone
    function checkPhone() {
        let flag = true;
        let phone = $("#phone").val();
        let lengthPhone = phone.length;
        let phoneno = /[,~!@#$%^&*(){}.<>]/;
        let rgxPhone = phoneno.test(phone);
        if (phone != "") {
            if (phone.length < 9 || phone.length > 10 || rgxPhone == true) {
                flag = false;
            }
        }
        return flag
    }

    function checkEmail() {
        let flag = true;
        let email = $("#email").val();
        if (email != "") {
            if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email) == false) {
                flag = false;
            }
        }

        return flag;
    }

    function validateTop_form() {
        flag = [];
        let parent_find = document.querySelectorAll(".parent_find");
        $(parent_find).each((key, obj, i) => {
            let checkValue = $(obj).find(".itemValue").val();
            let elAlert = $(obj).find(".alert_messenger");
            let messAlert = $(elAlert).attr("data-alert");
            if (checkValue == "") {
                flag.push("false");
                $(elAlert).text(`*${messAlert}*`);
            } else {
                $(elAlert).text(``);
            }
        });
        if ($("#phone").val() != "") {
            if (checkPhone() == false) {
                flag.push("false");
                $(".alert_phone").text("*Số điện thoại không đúng định dạng*");
            } else {
                $(".alert_phone").text("")
            }
        }
        if ($("#email").val() != "") {
            if (checkEmail() == false) {
                flag.push("false");
                $(".alert_email").text("*Email không đúng định dạng*");
            } else {
                $(".alert_email").text("");
            }
        }
        return flag;
    }
    // chưa có kinh nghiệm
    function notExp(_this) {
        let checked = _this.checked;
        if (_this.checked == true) {
            $(".wrap__experience").fadeOut();
        } else {
            $(".wrap__experience").fadeIn();
        }
    }
    let countComany = 0;
    // thêm form công ty
    function Company() {
        countComany++;
        $("#multiple_company").append(`<div class="box-company mt-2">
                                <a href="javascript:void(0)" class="a_close_exp" onclick="close_exp(this)" ><i class="fas fa-times-circle"></i></a>
                                <input type="text" id="CompanyName1" class="form-control" name="data_post_apply_exp[${countComany}][company]" placeholder="Tên công ty">
                                <input type="text" id="Position1"  class="form-control"  name="data_post_apply_exp[${countComany}][position]" placeholder="Chức vụ (nhân viên, trưởng phòng)">
                                <textarea id="Note1"  class="form-control"  name="data_post_apply_exp[${countComany}][content]" placeholder="Thời gian làm công việc chính: Lý do nghỉ: Người tham khảo, tel (không bắt buộc)"></textarea>
                            </div>`);

    }
    // xóa form công ty
    function close_exp(__this) {
        countComany--;
        $(__this).closest(".box-company").remove();
    }
    // đóng form
    $(document).on("click", ".close__form", function() {
        // let form_name = $(this).attr("data-name");
        // $("#"+form_name).fadeOut();
        // 
    });
    // click chọn ảnh khác
    $(".choice_another_img").click(function() {
        $("#file_mobile").click();
    })
    // click xóa ảnh
    $(".del_red").click(function() {
        $('#box_img_mobile').attr('src', "");
    });

    // select2 jquery 
    $(document).ready(function() {
        $('.address_selected').select2();
    });
    //input id-front
    $("#file_mobile").change(function(event) {
        readURLIDFRONT(this);
    });

    function readURLIDFRONT(input) {
        let allowedExtensions = /(\.jpg|\.jpeg|\.png)$/i;
        let formData = new FormData();
        formData.append("image", input.files[0]);
        $.ajax({
            url: "valiedateimage", // tên của routes trong config > routes
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(res) {
                if (res != "") {
                    $(".image_alert").text(res);
                } else {
                    $(".image_alert").text("");
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        var filename = $("#file_mobile").val();
                        filename = filename.substring(filename.lastIndexOf('\\') + 1);
                        reader.onload = function(e) {
                            $('#box_img_mobile').attr('src', e.target.result);
                            $(".hidden_img").css({
                                "display": "block"
                            });
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
            }
        });
    }
    //chọn cv file
    $("#CVFile").change(function(event) {
        readURLCV(this);
    });

    function readURLCV(input) {
        let allowedExtensions = /(\.pdf|\.PDF|\.doc|\.docx)$/i;
        let formData = new FormData();
        formData.append("cv", input.files[0]);
        $.ajax({
            url: "valiedatefile", // tên của routes trong config > routes
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function(res) {
                if (res != "") {
                    checkDv = false;
                    $(".cv_alert").text(res);
                } else {
                    checkDv = true;
                    $(".cv_alert").text("");
                    if (input.files && input.files[0]) {
                        var reader = new FileReader();
                        var filename = $("#CVFile").val();
                        filename = filename.substring(filename.lastIndexOf('\\') + 1);
                        reader.onload = function(e) {
                            $("#Cv_filename").text(filename);
                            $(".delfile").css({
                                "display": "block"
                            });
                        }
                        reader.readAsDataURL(input.files[0]);
                    }
                }
            }
        });
    }

    // xóa cv
    function delCv() {
        $("#CVFile").val("");
        $("#Cv_filename").text("");
    }
</script>

</html>