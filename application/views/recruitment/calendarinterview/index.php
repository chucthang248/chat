<!-- third party css -->
<link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
<link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
<!-- sweetalert css -->
<link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
<!-- Jquery Toast css -->
<link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
<link href="public/assets/css/customs/timework.css" rel="stylesheet" type="text/css" />
<div class="container-fluid">
    <!-- start page title -->
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <div class="page-title-right">
                    <ol class="breadcrumb m-0">
                        <li class="breadcrumb-item"><a href="javascript: void(0);">Dashboard</a></li>
                        <li class="breadcrumb-item active"><?= $title; ?></li>
                    </ol>
                </div>
                <h4 class="page-title"><?= $title; ?></h4>
            </div>
        </div>
    </div>
    <!-- end page title -->
    <!-- notify -->
    <?php $message_flashdata = $this->session->flashdata('message_flashdata'); ?>
    <?php if ($message_flashdata && $message_flashdata['type'] == 'success') { ?>
        <div id="box-notify" class="jq-toast-wrap bottom-right">
            <div class="jq-toast-single jq-has-icon jq-icon-success" style="text-align:left;">
                <span class="jq-toast-loader jq-toast-loaded" style="-webkit-transition: width 9.6s ease-in;-o-transition: width 9.6s ease-in;transition: width 9.6s ease-in;background-color: #5ba035;"></span>
                <h2 class="jq-toast-heading">Thông báo!</h2>
                <?php echo $message_flashdata['message']; ?>
            </div>
        </div>
    <?php } ?>
    <!-- notify End -->
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div id='calendar'></div>
            </div>
        </div>
    </div> <!-- end row -->
</div> <!-- end container-fluid -->
<!-- load aplly form -->
<div id="loadAplly">
    <div class="modal" id="modal" tabindex="-1" role="dialog">
    
    </div>
</div>
<style>
    .select2-container {
        z-index: 2000;
    }
    .modal {
        background: rgba(0 0 0/ 27%);
         z-index: 4050;
         padding-right: 0 ;
    }
</style>
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.5.1/main.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.10.2/locale/vi.min.js"> </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<!-- form mask js -->
<script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
<script>
    window.onresize = function(event) {
        location.reload();
    };
    // full calendar
    let start = "";
    let getDMY = "";
    let getYear = "";
    let getMonth = "";
    let getDay = "";
    document.addEventListener('DOMContentLoaded', function() {
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, 
        { 
            headerToolbar: {
                left: 'prev,next,today',
                center: 'title',
                right: ''
            },
            eventResourceEditable: true,
            buttonText: {
                today: 'Hôm nay'
            },
            locale: 'vi',
            
            dateClick: function(info) { 
                let countEvent = $(info.dayEl).find(".fc-daygrid-event-harness");
                start = info.dateStr;
                getDMY = info.dateStr.split("-");
                getYear = getDMY[0];
                getMonth = getDMY[1];
                getDay = getDMY[2];
                   $.ajax({
                        type: "POST",
                        url: "recruitment/ajax/addInterview",
                        success: function(res) {
                            $("#modal").html(res);
                            $("#modal").modal("show");
                        }
                    });
            },
            eventClick: function(info) {
                info.jsEvent.preventDefault(); // don't let the browser navigate
                current_eventClick = info.event;
                start =  moment(info.event.start).format("YYYY-MM-DD");
                let _def_data = info.event._def;
                let optionOther = _def_data.extendedProps;
                 evtClick_uid = _def_data.publicId;
                $.ajax({
                    type: "POST",
                        data: {uid:evtClick_uid,
                                },
                        url: "recruitment/ajax/editInterview",
                        success: function(res) {
                            $("#modal").html(res);
                            $("#modal").modal("show");

                        }
                    });
            }
        });
        // load events
        $(document).ready(function() {
            $.ajax({
                type: "GET",
                url: "recruitment/calendarinterview/loadEvents",
                dataType: "JSON",
                success: function(res) {
                    let data_timework = res.apply;
                    calendar.addEventSource(data_timework);
                }
            });
        });
        // thêm
        $(document).on("click",".btn_add",function(){
            // id ứng viên
            let applyID = $("#applyID").val();
            // tin tuyển  
            let recruitmentID = $("#recruitmentID").val();
             // giờ 
             let time_interview = $("#time_interview").val();
              // giờ 
            let note = $("#note").val();
            if(applyID != "" && recruitmentID != "" && time_interview != "")
            {
                    $.ajax({
                        type: "POST",
                        url: "recruitment/calendarinterview/add",
                        data: {
                                uid : uuid(),
                                applyID: applyID, 
                                recruitmentID : recruitmentID,
                                time_interview : time_interview,
                                note : note,
                                start: start
                            },
                        dataType: "JSON",
                        success: function(res) {
                            if (res.type == "error") {
                                Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                            }
                            else
                            {
                                let data_timework = res.apply;
                                calendar.removeAllEvents();
                                calendar.addEventSource(data_timework);
                            }
                          
                        }
                    });
                    $("#modal").modal("hide");
            }
            if(applyID == "" || recruitmentID == "" || time_interview == "")
            {
                Swal.fire({
                        title: "Vui lòng nhập đủ thông tin!",
                        type: "warning",
                        showCancelButton: false,
                    });
            }   
           
        });
        // sửa
        $(document).on("click",".btn_update",function(){
            // id ứng viên
            let applyID = $("#applyID").val();
            // tin tuyển  
            let recruitmentID = $("#recruitmentID").val();
             // giờ 
             let time_interview = $("#time_interview").val();
              // giờ 
            let note = $("#note").val();
            
            if(applyID != "" && recruitmentID != "" && time_interview != "")
            {
                    $.ajax({
                        type: "POST",
                        url: "recruitment/calendarinterview/edit",
                        data: {
                                uid : evtClick_uid,
                                applyID: applyID, 
                                recruitmentID : recruitmentID,
                                time_interview : time_interview,
                                note : note,
                                start: start
                            },
                        dataType: "JSON",
                        success: function(res) {
                            
                            if (res.type == "error") {
                                Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                            }
                            else
                            {
                                let data_timework = res.apply;
                                calendar.removeAllEvents();
                                calendar.addEventSource(data_timework);
                            }
                        }
                    });
                    $("#modal").modal("hide");
            }
            if(applyID == "" || recruitmentID == "" || time_interview == "")
            {
                Swal.fire({
                        title: "Vui lòng nhập đủ thông tin!",
                        type: "warning",
                        showCancelButton: false,
                    });
            }   
           
        });
        // remove current select event
        $(document).on("click",".btn_del",function(){
            
            $.ajax({
                type: "POST",
                url: "recruitment/calendarinterview/delete",
                data: {
                        uid : evtClick_uid,
                    },
                success: function(res) {
                    let item = calendar.getEventById(evtClick_uid);
                    if(res != "")
                    {
                        let data = JSON.parse(res);
                        if (data.type == "error") {
                             Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                        }
                    }
                    else
                    {
                        item.remove();
                        $("#modal").modal("hide");
                    }           

                  
                }
            });
           
        });
        // lọc chi nhánh
        $(document).on("change","#filterBranch",function(){
            let branch_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "cpanel/overviewtimework/filter_branch",
                data: {branch_id: branch_id},
                dataType: "JSON",
                success: function(res) {
                    
                   calendar.removeAllEvents();
                    let data_timework = res.apply;
                    calendar.addEventSource(data_timework);
                }
            });
            console.log(branch_id);
        });
        // xong tất các tác vụ thì mới render
        calendar.render();
    });
    // uuid
    function uuid() {
        var temp_url = URL.createObjectURL(new Blob());
        var uuid = temp_url.toString();
        URL.revokeObjectURL(temp_url);
        return uuid.substr(uuid.lastIndexOf('/') + 1); // remove prefix (e.g. blob:null/, blob:www.test.com/, ...)
    }
</script>