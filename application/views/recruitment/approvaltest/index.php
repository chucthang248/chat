<div class="card-body">
    <div class="card">
        <!-- check -->
        <form action="hoan-thanh-kiem-tra.html" method="post" onsubmit="return submitTest()">
            <div class="bg_maketest_b-e">
                <div class="d-flex align-items-center">
                    <h2 class="name_test mr-2"><?= $getMaketest['quiz_name'].","?></h2>
                    <strong class="name_test"><?="".$countCorrect."/".count($getQuestion_Of_Quiz) ?> câu đúng</strong>
                </div>
                
                <input type="hidden" id="create_quizID" value="<?= $getQuiz['id'] ?>" name="data_post[create_quizID]">
                <hr>
                <div class="box__test mt2 row">
                    <input type="hidden" id="answers_of_createquiz" value="" name="data_post[answers_of_createquiz]">
                    <?php if ($getQuestion_Of_Quiz != NULL) { ?>
                        <?php $checkID = ""; $answerkey=""; $checked = ""; foreach ($getQuestion_Of_Quiz as $key => $val) { ?>
                            <div class="item_question col-md-4">
                                <div class="d-flex">
                                    <h4 class="name_stt col-md-auto">Câu <?= $key + 1 ?>:</h4>
                                    <div class="col-md-auto">
                                        <h5 class="name_question"><?= $val['question'] ?></h5>
                                    </div>
                                </div>
                                <?php foreach($getMaketest["answers_of_createquiz"] as $key_creatquiz => $val_creatquiz){ 
                                    if($val_creatquiz['questionID'] == $val['id']   )
                                    {
                                        $checkID = $val_creatquiz['questionID'];
                                        $answerkey= $val_creatquiz['answerkey'];
                                        break;
                                    }
                                } ?>
                                <div data-check="<?=$checkID?>" data-correctkey="<?=$correct?>" class="wrap_question">
                                    <div class="box_answer">
                                        <?php if ($val['answer'] != NULL) {
                                            $val['answer'] = json_decode($val['answer']);
                                            usort($val['answer'], function ($a, $b) {
                                                return $b->sort > $a->sort;
                                            }); ?>
                                            <div class="row">
                                                <?php foreach ($val['answer'] as $key_answer => $val_answer) {  ?>
                                                    <div   class="col-md-6 row form-group">
                                                        <div class="col-md-1 ">
                                                            <div class="radio form-check-inline">
                                                                <input <?php if($answerkey == $val_answer->alph || $val_answer->alph == $val["correct_answer"] ){ echo "checked"; } ?>  type="radio" value="<?php echo $val_answer->alph ?>" onchange="choiceAnswer('<?= $val['id'] ?>')" >
                                                                <label class="<?php if($answerkey == $val_answer->alph && $answerkey != $val["correct_answer"] ){ echo "false_choice"; } ?>"  for=""></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-auto ">
                                                            <label for="">Câu <?php echo $val_answer->alph ?>: </label>
                                                        </div>
                                                        <div class="col-md-auto">
                                                            <div><?php echo $val_answer->answer ?></div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?= $key < count($getQuestion_Of_Quiz) - 1 ?  "<hr>" : "" ?>
                        <?php } ?>
                    <?php  } ?>
                </div>
            </div>
        </form>
        <!-- check -->
    </div>
</div>
