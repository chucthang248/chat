<!DOCTYPE html>
<html lang="en">
<head>
    <base href="<?php echo site_url(); ?>" />
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài kiểm tra</title>
    <!-- App favicon -->
    <link rel="shortcut icon" href="public/images/favicon.webp">
    <!-- C3 Chart css -->
    <link href="public/assets/libs/c3/c3.min.css" rel="stylesheet" type="text/css" />
    <!-- App css -->
    <link href="public/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" id="bootstrap-stylesheet" />
    <link href="public/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="public/assets/css/app.min.css" rel="stylesheet" type="text/css" id="app-stylesheet" />
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <!-- third party css -->
    <link href="public/assets/libs/datatables/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="public/assets/libs/datatables/buttons.bootstrap4.css" rel="stylesheet" type="text/css" />
    <link href="public/assets/libs/datatables/responsive.bootstrap4.css" rel="stylesheet" type="text/css" />
    <!-- sweetalert css -->
    <link href="public/assets/libs/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css" />
    <!-- Jquery Toast css -->
    <link href="public/assets/libs/jquery-toast/jquery.toast.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="public/assets/css/recruitment.css">
</head>
<body class="body_maketest">
    <div class="main_maketest">
        <div class="wrap_recruitment">
            <?php
                if(isset($template) && !empty($template)){
                    $this->load->view($template, isset($data)?$data:NULL);
                }
            ?>
        </div>    
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!-- form mask js -->
    <script src="public/assets/libs/jquery-mask-plugin/jquery.mask.min.js"></script>
    <!-- sweetalert -->
    <script src="public/assets/libs/sweetalert2/sweetalert2.min.js"></script>
    <!-- Tost-->
    <script src="public/assets/libs/jquery-toast/jquery.toast.min.js"></script>
    <style>
    </style>
    <script>
        let AnswerArr = [];
        let countQuestion = "<?=count($getQuestion_Of_Quiz) ?>";
        function choiceAnswer(questionID,answerkey)
        {
            AnswerArr.push({questionID: questionID,answerkey:answerkey });
            AnswerArr =  getUniqueListBy(AnswerArr, 'questionID');
            // AnswerArr =  [...new Set(AnswerArr)];
            let  AnswerString = JSON.stringify(AnswerArr);
            $("#answers_of_createquiz").val(AnswerString);
        }
        function getUniqueListBy(arr, key) {
            return [...new Map(arr.map(item => [item[key], item])).values()]
        }
         function submitTest()
         {
             if(AnswerArr.length == 0 || AnswerArr.length < parseInt(countQuestion))
             {
                // $(".alert-form").show();
                Swal.fire({
                    title: "Vui lòng hoàn thành tất cả câu hỏi",
                    type: "warning",
                    showCancelButton: false,
                    cancelButtonColor: "#d33",
                });
         
                return false;
             }
             return true;
         }
    </script>
</body>
</html>