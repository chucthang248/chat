
    <div class="container">
        <form action="hoan-thanh-kiem-tra.html" method="post" onsubmit="return submitTest()">
            <div class="bg_maketest">
                <h2 class="name_test"><?= $getQuiz['name'] ?></h2>
                <input type="hidden" id="create_quizID" value="<?= $getQuiz['id'] ?>" name="data_post[create_quizID]">
                <hr>
                <div class="box__test mt2">
                    <input type="hidden" id="answers_of_createquiz" value="" name="data_post[answers_of_createquiz]">
                    <?php if ($getQuestion_Of_Quiz != NULL) { ?>
                        <?php foreach ($getQuestion_Of_Quiz as $key => $val) { ?>
                            <div class="item_question">
                                <div class="d-flex">
                                    <h4 class="name_stt">Câu <?= $key + 1 ?>:</h4>
                                    <div class="col-md-auto">
                                         <h5 class="name_question"><?= $val['question'] ?></h5>
                                    </div>
                                </div>
                                <div class="wrap_question">
                                    <div class="box_answer">
                                        <?php if ($val['answer'] != NULL) {
                                            $val['answer'] = json_decode($val['answer']);
                                            usort($val['answer'], function ($a, $b) {
                                                return $b->sort < $a->sort;
                                            }); ?>
                                            <div class="row">
                                            <?php foreach ($val['answer'] as $key_answer => $val_answer) { ?>
                                                    <div class="col-md-6 row form-group">
                                                        <div class="col-md-1 ">
                                                            <div class="radio form-check-inline">
                                                                <input type="radio" value="<?php echo $val_answer->alph ?>" onchange="choiceAnswer('<?= $val['id'] ?>','<?php echo $val_answer->alph ?>')" name="choice<?= $key ?>">
                                                                <label for=""></label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-auto ">
                                                            <label for="">Câu <?php echo $val_answer->alph ?>: </label>
                                                        </div>
                                                        <div class="col-md-auto">
                                                            <div><?php echo $val_answer->answer ?></div>
                                                        </div>
                                                    </div>
                                            <?php } ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?= $key < count($getQuestion_Of_Quiz) - 1 ?  "<hr>" : "" ?>
                        <?php } ?>
                    <?php  } ?>
                </div>
                <div class="box__btn_send">
                    <button class="btn btn-primary waves-effect waves-light " type="submit"><i class="far fa-save mr-2"></i>Hoàn thành</button>
                </div>
            </div>
        </form>
    </div>
