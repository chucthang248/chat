<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//My_Controller for module frontend
class MY_Controller extends CI_Controller {
	public function __construct(){
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		parent::__construct();
		
	}
	//Main Function
	protected function get_index(){
		// if (!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != "on") {
	 //        $url = "https://". $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
	 //        redirect($url);
	 //        exit;
	 //    }
		return $data;
	}
	//example function
}
//My_Controller for module admin
class Admin_Controller extends CI_Controller {
	public function __construct(){
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		parent::__construct();
		$this->load->model('EmployeeModels');
		$this->load->model('PermissonsModels');
		$this->load->model('LogModels');
	}
	//Main Function
	protected function get_index(){
		$data['info_admin'] = $this->get_admin();
		$data['roleID']		= $this->get_roleID();
		$data['permission_alert'] = $this->checkPermissionAlert();
		//$this->checkPermission();
		return $data;
	}
	protected function get_roleID(){
		$data = array();
			$userID = $this->Auth->logged_id();
			$getUser = $this->db->select('*')->from('tbl_user')->where('id', $userID)->get()->row_array();
			//lấy controller hiện tại
			if($getUser['role_id'] > 0)
			{
				$data = array(
					'type' => 'error',
					'data' => ''
				);
			}
	
		return $data;
	}
	protected function get_admin(){
		if($this->Auth->check_logged()){
			$id_user = $this->Auth->logged_id();
			$getUser = $this->db->select('*')->from('tbl_user')->where('id', $id_user)->get()->row_array();
			$getEmployees = $this->db->select('id, code, fullname, email, phone, avatar')->from('tbl_employees')->where('id', $getUser['employeeID'])->get()->row_array();
			if($getEmployees != NULL){
				$getUser['data'] = $getEmployees;
				$getUser['data']['avatar'] = 'upload/employee/'.$getEmployees['code'].'/'.$getEmployees['avatar'];
			}else{
				$getUser['data']['fullname'] = 'Quản trị';
				$getUser['data']['avatar'] = 'public/images/avatar.png';
			}
			return $getUser;
		}
	}
	protected function checkPermission(){
		//lấy ID của user đang đăng nhập
		$userID = $this->Auth->logged_id();
		$getUser = $this->db->select('*')->from('tbl_user')->where('id', $userID)->get()->row_array();
		//lấy controller hiện tại
		if($getUser['role_id'] != 0)
		{
			$currentController = $this->router->fetch_class();
			//lấy action hiện tại
			$currentAction = $this->router->fetch_method();
			//lấy thông tin phòng ban của user
			$job_position_titleID = $this->EmployeeModels->getjob_position_titleIDByUserID($userID);
			$another_Action = $this->anotherAction($currentAction);
			$getPermissionDetail = $this->PermissonsModels->findRowWhere(array('controller' => $currentController,'action' => $currentAction, 'job_position_titleID' => $job_position_titleID));
	
			$checkProfile = FALSE;
			$idEmployee = $this->uri->segment(4);
			// kiểm tra khi người dùng vào xem thông tin cá nhân (xem của bản thân và xem của người khác)
			if($getPermissionDetail != NULL && $currentController  == "auths")
			{
				$checkProfile = TRUE;
			}else
			{
				if($idEmployee != NULL && $currentController  == "auths")
				{
					// nếu id route == employeeID trong db thì vào
					if($idEmployee == $getUser['employeeID'])
					{
						$checkProfile = TRUE;
					}
				}
				if($idEmployee == NULL && $currentController  == "auths")
				{
					$checkProfile = TRUE;
				}	
			}
			//if($getPermissionDetail == NULL && $currentController != 'permission' && $currentController != 'auths' && $another_Action == false)
			if($getPermissionDetail == NULL && $currentController != 'permission'  && $another_Action == false && $checkProfile ==  false){
				
				if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
				{
					echo json_encode(array("type"=>"error","redirect" => CPANEL.'permission/notpermission'));die;
				}else
				{
					redirect(CPANEL.'permission/notpermission');
				}
			}
		}
		
	}
	// phân quyền xem thông báo ứng tuyển
	protected function checkPermissionAlert(){
		//lấy ID của user đang đăng nhập
		$userID = $this->Auth->logged_id();
		$getUser = $this->db->select('*')->from('tbl_user')->where('id', $userID)->get()->row_array();
		//lấy controller hiện tại
		$rs = 1;
		if($getUser['role_id'] != 0)
		{
			
			$careerroads = $this->EmployeeModels->getCarrerIDByUserID($userID);
			$getPermissionDetail = $this->PermissonsModels->findRowWhere(array('controller' => "apply",'action' => "alert", 'careerroads' => $careerroads));
			if($getPermissionDetail == NULL){
				
				$rs = 0;
			}
		}
		return $rs;
	}
	protected function anotherAction($currentAction)
	{
		$arr_controller = ["search","pagination","delete_export_excel","export_excel","viewEmloyeeInfoDetail","filter_branch","getDataRow","checkChilditem",
		"export","publish","loadEvents","addInterview","editInterview","viewDetail","deleteAjax","saveStatus","editInterview","formAdd","formEdit",
		"addInterview","loadTimework"];
		if(in_array($currentAction, $arr_controller ))
		{
			return TRUE;
		}
		return FALSE;
	}
}

//My_Controller for module dashboard
class Dashboard_Controller extends CI_Controller {
	public function __construct(){
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		parent::__construct();
	}
	//Main Function
	protected function get_index(){
		$data['info_user'] = $this->get_user();
		return $data;
	}

	//get_user - OT1	
	protected function get_user(){
		$id_user = $this->Auth->userID();
		$data = $this->db->select('*')->from('tbl_user')->where(array('id' => $id_user))->get()->row_array();
		if($data != NULL){
			if($data['avatar'] == ''){
				$data['avatar'] = 'public/dashboard/images/avatar-default.jpg';
			}else{
				$data['avatar'] = 'upload/passport/thumb/'.$data['avatar'];
			}
		}
		return $data;
	}
}
