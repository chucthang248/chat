<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Model extends CI_Model {
	// name table
	public $table = '';
	function add($data = NULL){
		$this->db->insert($this->table, $data); //
		$flag = $this->db->affected_rows();
		$insert_id = $this->db->insert_id();
		if($flag > 0){
			return array(
				'id_insert'	=> $insert_id,
				'type'		=> 'successful',
				'message'	=> 'Add value successful!',
			);
		}
		else
		{
			return array(
				'type'		=> 'error',
				'message'	=> 'Add value error!',
			);
		}
	}
	function adds($data = NULL){
		$this->db->insert_batch($this->table, $data);
		$flag = $this->db->affected_rows();
		$insert_id = $this->db->insert_id();
		if($flag > 0){
			return array(
				'id_insert'	=> $insert_id,
				'type'		=> 'successful',
				'message'	=> 'Thêm dữ liệu thành công!',
			);
		}
		else
		{
			return array(
				'type'		=> 'error',
				'message'	=> 'Thêm dữ liệu không thành công!',
			);
		}
	}

	function edit($data = NULL, $id = '', $key = 'id'){
		if($data !='' AND $id !='' ){
			$this->db->where(array($key => $id))->update($this->table, $data);
			$flag = $this->db->affected_rows();
			if($flag > 0){
				return array(
					'type'		=> 'successful',
					'message'	=> 'Update value successful!',
				);
			}
			else
			{
				return array(
					'type'		=> 'error',
					'message'	=> 'Update value error!',
				);
			}
		}
		
	}

	function edit_WhereMultiple($data = NULL,$where){
			$this->db->where($where)->update($this->table, $data);
			$flag = $this->db->affected_rows();
			if($flag > 0){
				return array(
					'type'		=> 'successful',
					'message'	=> 'Update value successful!',
				);
			}
			else
			{
				return array(
					'type'		=> 'error',
					'message'	=> 'Update value error!',
				);
			}
	}

	function deleteWhere($field,$id){
		if($id != ''){
		 	$this->db->delete($this->table, array($field => $id));
		}
	}
	function deleteWheres($where){
		$this->db->delete($this->table, $where);
	}

	function delete($id){
		if($id != ''){
			$this->db->delete($this->table, array('id' => $id));
		}
	}
	function getAllWhere($fields = '*', $order = 'id desc', $where = NULL,$start = '', $limit = ''){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->order_by($order);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		if($where != NULL){
			$result = $this->db->where($where);
			$result = $this->db->get()->row_array();
		}
		$result = $this->db->get()->result_array();
		return $result;
	}
	function getResultArray($fields = '*', $where = NULL,$start = '', $limit = '', $order = 'id desc'){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->order_by($order);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		if($where != NULL){
			$result = $this->db->where($where);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}

	function getAll($fields = '*', $order = 'id desc', $start = '', $limit = ''){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->order_by($order);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}

	function find($fieldValue = '', $fields = '*', $field = 'id'){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->where(array($field => $fieldValue));
		$result = $this->db->get()->row_array();
		return $result;
	}

	function findOne($where = NULL, $fields = '*'){
		$result = $this->db->select($fields)->from($this->table);
		if($where != ''){
			$result = $this->db->where($where);
			$result = $this->db->get()->row_array();
		}
		return $result;
	}

	function findRowWhere($where = NULL, $fields = '*', $order = 'id desc', $start = '', $limit = ''){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->order_by($order);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		if($where != ''){
			$result = $this->db->where($where);
			$result = $this->db->get()->row_array();
		}
		return $result;
	}

	function findWhere($where = NULL, $fields = '*', $order = 'id desc', $start = '', $limit = ''){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->order_by($order);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		if($where != ''){
			$result = $this->db->where($where);
			$result = $this->db->get()->result_array();
		}
		return $result;
	}

	function findOrWhere($where = NULL, $orWhere = NULL, $fields = '*', $order = 'id desc', $start = '', $limit = ''){
		$result = $this->db->select($fields)->from($this->table);
		$result = $this->db->order_by($order);
		if($limit != ''){
			$result = $this->db->limit($limit, $start);
		}
		if($where != ''){
			$result = $this->db->where($where);
			if($orWhere != NULL){
				$result = $this->db->or_where($orWhere);
			}
			$result = $this->db->get()->result_array();
		}
		return $result;
	}

	function find_row_max($field_max = ''){
		$result = $this->db->select_max($field_max)->from($this->table);
		$result = $this->db->get()->row_array();
		return $result;
	}
	function _pagination()
	{
		// $config['full_tag_open'] = '<ul class="pagination">';
		// $config['full_tag_close'] = '</ul>';
		// $config['first_link'] = '<i class="fa fa-angle-double-left" aria-hidden="true"></i>';
		// $config['first_tag_open'] = '<li>';
		// $config['first_tag_close'] = '</li>';

		// $config['last_link'] = '<i class="fa fa-angle-double-right" aria-hidden="true"></i>';
		// $config['last_tag_open'] = '<li>';
		// $config['last_tag_close'] = '</li>';

		// $config['next_link'] = '<i class="fa fa-angle-right" aria-hidden="true"></i>';
		// $config['next_tag_open'] = '<li class="paginate_button next">';
		// $config['next_tag_close'] = '</li>';

		// $config['prev_link'] = '<i class="fa fa-angle-left" aria-hidden="true"></i>';
		// $config['prev_tag_open'] = '<li class="paginate_button previous">';
		// $config['prev_tag_close'] = '</li>';

		// $config['cur_tag_open'] = '<li class="paginate_button active"><a class="number current">';
		// $config['cur_tag_close'] = '</a></li>';
		// $config['num_tag_open'] = '<li>';
		// $config['num_tag_close'] = '</li>';

		$config['num_links'] = 5;
		$config['uri_segment'] = 3;

		$config['use_page_numbers'] = TRUE;
		$config['per_page'] = 5;
		return $config;
	}

	function search_like_join_row($data = NULL,$where = NULL
			,$likeValue = "", $column_1 =  NULL, $or_column_2 =  NULL, $or_column_3 = NULL,  // key + column search
			$join_table = NULL,$query_join = NULL,$type = NULL,	// join 
			$start = NULL, $limit = NULL
			){	
		$result = $this->db->select($data)->from($this->table);
		if($join_table != NULL && $query_join != NULL && $type != NULL)
		{
			$this->db->join($join_table,$query_join,$type); 
		}
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($likeValue != ""){
			if($column_1 != NULL)
		{
		$this->db->like($column_1, $likeValue);
		}
			if($or_column_2 != NULL)
		{
		$this->db->or_like($or_column_2, $likeValue);
		}
			if($or_column_3 != NULL)
		{
			$this->db->or_like($or_column_3, $likeValue);
		}
		}
		if($limit > 0){
			$result = $this->db->limit($limit,$start);
		}
		//$result = $this->db->count_all_results();
			$result = $this->db->get()->result_array();
			return $result;
	}

	function search_like_join($data = NULL,$where = NULL
						,$likeValue = "", $column_1 =  NULL, $or_column_2 =  NULL, $or_column_3 = NULL,  // key + column search
						$join_table = NULL,$query_join = NULL,$type = NULL,	// join 
						$start = NULL, $limit = NULL
						){	
		$result = $this->db->select($data)->from($this->table);
		if($join_table != NULL && $query_join != NULL && $type != NULL)
		{
			$this->db->join($join_table,$query_join,$type); 
		}
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($likeValue != ""){
			if($column_1 != NULL)
			{
				$this->db->like($column_1, $likeValue);
			}
			if($or_column_2 != NULL)
			{
				$this->db->or_like($or_column_2, $likeValue);
			}
			if($or_column_3 != NULL)
			{
				$this->db->or_like($or_column_3, $likeValue);
			}
		}
		if($limit > 0){
			$result = $this->db->limit($limit,$start);
		}
		//$result = $this->db->count_all_results();
		$result = $this->db->get()->result_array();
		return $result;
	}
	
	function total_like($where = NULL,$likeValue = NULL, $column_1 = NULL,$column_2 = NULL,$column_3 = NULL,$column_4 = NULL,$column_5 = NULL){
		$result = $this->db->from($this->table);
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($likeValue != NULL){
			if($column_1 != NULL )
			{
				$this->db->like($column_1, $likeValue);
			}
			if($column_2 != NULL )
			{
				$this->db->or_like($column_2, $likeValue);
			}
			if($column_3 != NULL )
			{
				$this->db->or_like($column_3, $likeValue);
			}
			if($column_4 != NULL )
			{
				$this->db->or_like($column_4, $likeValue);
			}
			if($column_5 != NULL )
			{
				$this->db->or_like($column_5, $likeValue);
			}
		}
		$result = $this->db->count_all_results();
		//echo $this->db->last_query(); die;
		return $result;
	}
	function select_array_like( $data = NULL, $where = NULL, $order = 'id desc', $start, $limit,$likeValue, 
	$column_1 = NULL,$column_2 = NULL,$column_3 = NULL,$column_4 = NULL,$column_5 = NULL){
		$result = $this->db->select($data)->from($this->table);
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($likeValue){
			//$this->db->like(array($column_1 => $likeValue,$column_2 => $likeValue,$column_3=> $likeValue, $column_4 =>$likeValue , $column_5 =>$likeValue  ));
			if($column_1 != NULL )
			{
				$this->db->like($column_1, $likeValue);
			}
			if($column_2 != NULL )
			{
				$this->db->or_like($column_2, $likeValue);
			}
			if($column_3 != NULL )
			{
				$this->db->or_like($column_3, $likeValue);
			}
			if($column_4 != NULL )
			{
				$this->db->or_like($column_4, $likeValue);
			}
			if($column_5 != NULL )
			{
				$this->db->or_like($column_5, $likeValue);
			}
		}
		if($order != ''){
			$result = $this->db->order_by($order);
		}
		if($limit > 0){
			$result = $this->db->limit($limit,$start);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}
	

	function select_array_wherein($data = NULL, $where = NULL,$field = '',$ar_where = NULL, $order = 'id desc', $start = NULL, $limit = NULL){

		$result = $this->db->select($data)->from($this->table);
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($ar_where != NULL && $field != ''){
			$result = $this->db->where_in($field,$ar_where);
		}
		if($order != ''){
			$result = $this->db->order_by($order);
		}
		if($limit > 0){
			$result = $this->db->limit($limit,$start);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}

	function select_array_wherein_join($data = NULL, $where = NULL,$field = '',$ar_where = NULL, $order = 'id desc', $start = NULL, $limit = NULL,$join_table = '',$query_join,$type){
		$result = $this->db->select($data)->from($this->table);
		$this->db->join($join_table,$query_join,$type); 
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($ar_where != NULL && $field != ''){
			$result = $this->db->where_in($field,$ar_where);
		}
		if($order != ''){
			$result = $this->db->order_by($order);
		}
		if($limit > 0){
			$result = $this->db->limit($limit,$start);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}
	function select_array_wherein_join_3_table($data = NULL, $where = NULL, $order = 'id desc',$join_table = '',$query_join,$type,$join_table2 = '',$query_join2,$type2,$likeValue){
		$result = $this->db->select($data)->from($this->table);
		$this->db->join($join_table,$query_join,$type); 
		$this->db->join($join_table2,$query_join2,$type2); 
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($likeValue){
			$this->db->like($likeValue);
		}
		if($order != ''){
			$result = $this->db->order_by($order);
		}
		$result = $this->db->get()->result_array();
		return $result;
	}
	function select_array_wherein_join_row($data = NULL, $where = NULL,$field = '',$ar_where = NULL, $order = 'id desc', $start = NULL, $limit = NULL,$join_table = '',$query_join,$type,$join_table2 = '',$query_join2,$type2,$likeValue){

		$result = $this->db->select($data)->from($this->table);
		$this->db->join($join_table,$query_join,$type); 
		$this->db->join($join_table2,$query_join2,$type2); 
		if($where != NULL){
			$result = $this->db->where($where);
		}
		if($ar_where != NULL && $field != ''){
			$result = $this->db->where_in($field,$ar_where);
		}
		if($likeValue){
			$this->db->like($likeValue);
		}
		if($order != ''){
			$result = $this->db->order_by($order);
		}
		if($limit > 0){
			$result = $this->db->limit($limit,$start);
		}
		$result = $this->db->count_all_results();
		return $result;
	}
	
}
?>
