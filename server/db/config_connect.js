const mysql = require("mysql"); // MYSQL
// DATABASE
let db_config = {
    host: "localhost",
    user: "root",
    password: "",
    database: "2020_hungthinh",
    charset : 'utf8mb4'
  };
  // connect
  let conn ;
  // Nếu bị mất kết nối sẽ tự động kết nối lại
  function handleDisconnect() {
     conn = mysql.createConnection(db_config);  
      conn.connect(function(err) {            
      if(err) {                                    
        console.log('error when connecting to db:', err);
        // setTimeout(handleDisconnect, 2000); 
      }                                   
    });                                    
      conn.on('error', function(err) {
      console.log('db error', err);
      if(err.code === 'PROTOCOL_CONNECTION_LOST') { 
        handleDisconnect();                        
      } else {                                     
        throw err;                                
      }
    });
  }
  handleDisconnect();
 
  module.exports = conn ;