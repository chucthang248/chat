const express      = require("express");
const bodyParser   = require("body-parser");
const cookieParser = require('cookie-parser');
const cors         = require('cors')
const app          = express();

var corsOptions    = {
  origin: '*',
  optionsSuccessStatus: 200 
}
app.use(cors(corsOptions));
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const port = process.env.PORT || 3005;
// server
const server             = require("http").Server(app);
// user
const users              = require("./model/users");
// module thông báo ứng tuyển
const apply              = require("./apply");
// chat
const chat_private       = require("./chat/chat");
const model_chat_private = require("./model/chat_private");
let io                   = require("socket.io")(server, {cors: { origin: '*', } });
//====================== ROUTER ====================
let router               = require("./router/router");
//===================== SOCKET IO ==================
io.on("connection", function (socket) {
   
    // dùng socket cho route 
    app.set('socket', socket);
    app.set('io', io);
    console.log("co nguoi ket noi: " + socket.id);
    // ý chính:
    //    - Khi người dùng đăng nhập sẽ gửi dữ liệu chat lên cho người dùng xem
    //    - Khi người dùng bị mất kết nối hoặc nguyên nhân nào đó , khi server kết nối lại sẽ gửi diệu chat cho người dùng lần nữa....
    // lấy id người vừa đăng nhập
    // kiểm tra trong mảng login_info có id người dùng chưa, nếu có thì thay đổi socketid (socktid thay đổi sau mỗi lần kết nối)
    detectLogin(socket);
    router(app,io,socket);
    // module thông báo ứng tuyển
    //apply(io,socket,conn);
    // chat private
    chat_private(io,socket);
    //disconnect
    socket.on("disconnect", function (reason) {
      detectLogout(socket);
      console.log("Ngắt kết nối: "+socket.id);
    });
});
// Lưu socketid , userid của người dùng đăng nhập
async function detectLogin(socket)
{
  
  socket.on("userid_login",async function (data) {
    let set     = `socketID = '${socket.id}' , online= 1`;
    let where   = `id = ${data.userid_login}`;
    let result  = await users.update(set,where);
    statusUser(socket,data.userid_login);
  });
  
}
// Xoá socketID sau khi logout
async function detectLogout(socket)
{
  // xoá socketid
  let set     = `online= 0`;
  let where   = `socketID = '${socket.id}' `;
  let result  =  await users.update(set,where);
  // Load danh sách chat cho những người khác
  statusUser(socket,null);
} 
// Load danh sách chat cho những người khác
async function statusUser(socket, user_id = null)
{
  let select_user  = `*`;
  let from_user    = "tbl_user";
  let where_user   = `socketID = '${socket.id}'`;
  if(user_id != null)
  {
      where_user   = `id = ${user_id}`;
  }
  let user         = await users.get(select_user,from_user,where_user );
  //============================================================
  if(user.data.length > 0)
  {
    let data           = await model_chat_private.load_list_chat_id(user.data[0].id);
    let arrListID      = data.list_chat;
    arrListID.forEach(async function(value, key) {
      if(value.socketID != '')
      {
        // let list_chat  = await model_chat_private.load_list_chat_send_receive(value.userID);
        io.to(`${value.socketID}`).emit('server_send_status', user.data[0]);
      }
      
    })
  }
}
server.listen(port, () => console.log("Server running in port " + port));

