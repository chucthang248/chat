const express = require("express");
const chat_private = require("../model/chat_private");
const users = require("../model/users");
module.exports = async (io,socket) =>{
      
        // load danh sách chat
        socket.on("get_listchat",async function (data) {
            
            // lấy danh sách tin nhắn theo id người gửi tới
            // ví dụ hiện tại máy này là người A 
            // khi người B gửi tin nhắn đến A thì nhánh sẽ là B -> A
            // khi người A trả lời lại B thì nhánh sẽ là A -> B
            // đoạn query dưới lấy những người nào gửi tin nhắn tới A
           let load_list_chat       = await  chat_private.load_list_chat_send_receive(data.userid_login);
           if(load_list_chat.list_chat.length > 0)
           {
                let userid_login    = data.userid_login;
                let user_receive_id = load_list_chat.list_chat[0].id;
                let data_obj        = await loadMessenger(userid_login,user_receive_id);
                socket.emit('server_send_listchat', load_list_chat);
                socket.emit('server_send_first_messenger', data_obj);
           }
        });

        // Nhận tin nhắn tới và thêm vào database
        socket.on("send_messenger",async function (data) {
            let data_insert = {
                user_send_id   : data.userid_login,
                user_receive_id: data.user_receive_id,
                content        : data.txtMessenger,
                view           : data.view,
                created_at     : data.created_at
            };
            // thêm vào database
            let add_messenger = await chat_private.add(data_insert);
            let get_messenger = await chat_private.get_messenger_current_user(data.userid_login,add_messenger.insertId);
            // lấy thông tin người nhận (lấy socketid để gửi dữ liệu)
            let user_receive_id   =  get_messenger.messenger.user_receive_id;
            let where             = `id = ${user_receive_id}`;
            let user              =  await users.get("*","tbl_user", where);
           
            if(user.data.length > 0)
            {
                try {
                    // Gửi tin nhắn đến người nhận theo socketid
                    io.to(`${user.data[0].socketID}`).emit('server_send_messenger', get_messenger);
                } catch (error) {
                    console.log(error);
                }
            }
            // load lại danh sách chat của người nhận vì có tin nhắn mới 
            let load_list_chat    = await  chat_private.load_list_chat_send_receive(user_receive_id);
            // gửi lại danh sách chat cho người nhận theo socketid
            if(load_list_chat.list_chat.length > 0)
            {
                try {
                     io.to(`${user.data[0].socketID}`).emit('server_send_listchat_update', load_list_chat);
                } catch (error) {
                    console.log(error);
                }
            }
        });

        // Tìm và chọn người để chat
        socket.on("choice_user_chat",async function (data) {
           let where             = `WHERE merge_tbl.id NOT IN(${data.user_receive_id})`;
           let load_list_chat    = await  chat_private.load_list_chat_send_receive(data.userid_login,where);
           // lấy thông tin người vừa được click
           let choic_user        = await  users.get_join(data.user_receive_id);
           // gộp người vừa click với danh sách (lý do: tránh trường hợp người vừa click đã có trong danh sách chat)
           let listChat =   load_list_chat.list_chat;
           choic_user.data[0].active_chat = 1;
           listChat.unshift(choic_user.data[0]);
           // load lại danh sách chat
           socket.emit("server_send_listchat",{type: "success",list_chat: listChat});
        });
        // Chọn người chat trong danh sách chat đang có
        socket.on("choice_user_in_list_chat",async function (data) {
            let userid_login    = data.userid_login;
            let user_receive_id = data.user_receive_id;
            let data_obj        = await loadMessenger(userid_login,user_receive_id);
            //console.log("=====choice_user_in_list_chat=====");
            //console.log(data_obj);
            socket.emit('server_send_first_messenger', data_obj);
        });
        // Cập nhật trạng thái đã xem
        socket.on("update_view",async function (data) {
        // vd: B nhận tin từ A 
        // thì lúc này database là user_send_id là A, user_receive_id = B (người đang xem tin nhắn này)
        // data.userid_login là người đang xem tin nhắn (người B)
        // data.user_receive_id là người gửi tin qua (người A)
        let multiple_condition = `user_receive_id = ${data.userid_login} AND  user_send_id = 
            ${data.user_receive_id} `;
            let set            = `view = ${data.view}`;
            let update_view    = await chat_private.update(set,multiple_condition);
        });


        //==========================================
        async function loadMessenger(userid_login,user_receive_id)
        {
            return new Promise(async function (resolve, reject) {
                  try {
                      // lấy tin nhắn đầu tiên trong danh sách người chat
                    let load_first_messenger_in_listchat = await  chat_private.load_first_messenger(userid_login, user_receive_id);
                    //  thông tin người đầu tiên trong danh sách chat
                    let first_users_in_list_chat         = await  users.get_join(user_receive_id);
                    // gửi tin nhắn đầu tiên trong danh sách người chat
                    let data_obj= {
                            mess: load_first_messenger_in_listchat,
                            user: first_users_in_list_chat.data[0]
                    };
                    resolve(data_obj);

                  } catch (error) {
                    reject(error);
                  }  
            });
            
        }

}