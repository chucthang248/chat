const multer          = require('multer');
const path            = require('path');
const fs              = require('fs');
const multiparty      = require('multiparty');
const FileType        = require('file-type');
const chat_private    = require("../model/chat_private");
const users           = require("../model/users");
const util            = require('util');
module.exports        = async (app ) => {
    const pathUpload  = '../upload/chat/';
    // cấu hình multer
    let storage = multer.diskStorage({
        destination: function (req, file, cb) {
            let pathFile = pathUpload;
              
            if(!fs.existsSync(pathFile)) {  // kiểm tra folder có tồn tại không
                fs.mkdirSync(pathFile);  // nếu folder không tồn tại thì tạo
                cb(null, pathFile);
            }else
            {
                cb(null, pathFile);    
            }
        },
        filename: function (req, file, cb) {
            let fileName = file.fieldname + '-' + Date.now() + path.extname(file.originalname);
            cb(null, fileName);
           
            //  req.img.push({type: file.mimetype , fileName : fileName});
            
        }
    })
    let upload = multer({ 
                          storage: storage,
                          fileFilter: (req, file, cb) => {
                           
                            if (
                               
                                 file.mimetype == "image/png"  || file.mimetype == "image/jpg" || 
                                 file.mimetype == "image/jpeg" || file.mimetype == "application/pdf" ||
                                 file.mimetype == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" || 
                                 file.mimetype == "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                                )  {
                              cb(null, true);
                            } else {
                              cb(null, false);
                              return cb(new Error('Only .png, .jpg and .jpeg format allowed!'));
                            }
                          }
                        });
    // upload

    //================================
    app.post('/uploadfile', upload.array('multiple'),async (req, res, next) => {
        const files   = req.files;
        let   socket  = req.app.get('socket');
        let   io      = req.app.get('io');
        if (!files) {
          const error = new Error('Please choose files')
          error.httpStatusCode = 400
          return next(error)
        }
        //=============================
        let arrContent = [];
        files.forEach(function(obj,key){
            // arrContent.push
            arrContent.push({type: obj.mimetype , fileName : obj.filename, fullpath: "upload/chat/"+obj.filename});
        });
        req.body.files_images = 1; // đoạn tin nhắn này là file/hình
        req.body.content      = JSON.stringify(arrContent);
        let insert_img_id     = await chat_private.add(req.body);
        let where             = ` id = '${insert_img_id.insertId}' `;
        let data_insert_img   = await chat_private.get(where);
        let where_user_send   = ` id = ${req.body.user_send_id} `;
        let user_send         =  await users.get("*","tbl_user", where_user_send);
        // không thể dùng sock.emit() trong route bởi vì nó không hiểu socket id của user request
        io.to(`${user_send.data[0].socketID}`).emit('server_send_fileupload', data_insert_img);
        //========================================================
        // Lấy tin nhắn kèm avatar
        let get_messenger     = await chat_private.get_messenger_current_user(req.body.user_send_id,insert_img_id.insertId);
        let where_user        = ` id = ${req.body.user_receive_id} `;
        let user              =  await users.get("*","tbl_user", where_user);
        // Gửi tin nhắn đến người nhận theo socketid
        io.to(`${user.data[0].socketID}`).emit('server_send_fileupload_to_me', get_messenger);
      });
      
        
    // validate 
    app.post("/validatefile",async (req, res,next) => {
            let form=new multiparty.Form();
            form.parse(req,async function(err, fields, files) {
                let filepath         = files.files[0].path;
                let result           = {mime: "", size: "" ,mime_type: "", error: ""};
                let data             = await FileType.fromFile(filepath);
                if(data != undefined)
                {
                    let filter       = ["jpg","jpeg","png","pdf","docx","xlxs"];
                    let detectMime   = filter.includes(data.ext);
                    let convertKB    = (files.files[0].size / 1024).toFixed(2);
                    let convertMB    = (files.files[0].size / 1048576).toFixed(2);
                    result.mime_type = data.ext;
                    if(detectMime == false) // không phải mime ở trên => false
                    {
                        result.mime  = "Chỉ hỗ trợ jpg, jpeg, png, pdf, word, excel";
                    }
                    if(convertMB > 1) // 1 mb
                    {
                        result.size  = "Dung lượng tối đa 1mb";
                    }
                }else
                {
                        result.error ="Lỗi underfined";
                }
              
                res.send(result);
                
            });
    });
}