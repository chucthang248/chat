let conn =  require("../db/config_connect");
module.exports =  {
    table: "tbl_user",
    // CẬP NHẬT
    update: function(set,where){
        table = this.table;
        return new Promise(function (resolve, reject) {
            let query = `UPDATE ${table} SET ${set} WHERE ${where}`;
            conn.query(query, (err, res) => {
                if (err) {
                    reject(err);
					return;
                }else
                {
                    resolve({ "type": "success"});
                }
            });
		});
      
    },
    // LẤY TẤT CẢ USER WHERE
    get: function(select = "*", from = this.table, where = null, oderby = null){
        table = this.table;
        oderby_asg = 'id desc';
        if(oderby != null)
        {
            oderby_asg = oderby;
        }
        return new Promise(function (resolve, reject) {
            let query = `SELECT ${select} FROM ${from} WHERE ${where} ORDER BY ${oderby}`;
            if(where == null)
            {
                query = `SELECT ${select} FROM ${from} ORDER BY ${oderby}`;
            }
            conn.query(query,(err, res) => {
                if (err) {
                    reject(err);
					return;
                }else
                {
                    resolve({ "type": "success","data": res});
                }
            });
		});
       
    },
    get_join: function(id,oderby = null){
        table = this.table;
        oderby_asg = 'id desc';
        if(oderby != null)
        {
            oderby_asg = oderby;
        }
        return new Promise(function (resolve, reject) {
            let query = `SELECT tbl_user.*,tbl_user.id as userID, tbl_employees.code, tbl_employees.avatar,  
            tbl_employees.fullname as employee_fullname, tbl_employees.email as employee_email
            FROM tbl_user LEFT JOIN tbl_employees ON tbl_user.employeeID = tbl_employees.id WHERE tbl_user.id = ${id} 
            ORDER BY ${oderby_asg}
            `;
            conn.query(query,(err, res) => {
                if (err) {
                    reject(err);
					return;
                }else
                {
                    resolve({ "type": "success","data": res});
                }
            });
		});
    },
 }