let conn = require("../db/config_connect");
module.exports = {
	table: "tbl_chat_private",
	// GET WHERE
	get: function (multiple_condition, oderby = null) {
		table = this.table;
		oderby_asg = "id desc";
		if (oderby != null) {
			oderby_asg = oderby;
		}
		let query = `SELECT *, DATE_FORMAT(created_at, "%d/%m/%Y, %H:%i:%s") as created_at FROM tbl_chat_private WHERE ${multiple_condition} ORDER BY ${oderby_asg}`;
		return new Promise(function (resolve, reject) {
			conn.query(query, (err, res) => {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({ type: "success", data: res });
				}
			});
		});
	},

	add: function (data_insert) {
		table = this.table;
		return new Promise(function (resolve, reject) {
			// data_insert    : data_insert  = {name: tien, age:24 }
			let query = `INSERT INTO ${table} SET ?`;
			conn.query(query, data_insert, function (err, res, fields) {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({ type: "success", insertId: res.insertId });
				}
			});
		});
	},
	// CẬP NHẬT
	update: function (set, where) {
		table = this.table;
		// set                : data_set              = {name: tien, age:24 }
		// multiple_condition : multiple_condition    = {id: 203}
		return new Promise(function (resolve, reject) {
			let query = `UPDATE ${table} SET ${set} WHERE ${where}`;
			conn.query(query, (err, res) => {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({ type: "success" });
				}
			});
		});
	},
	get_messenger_current_user: function (user_send_id,insert_messenger_id) {
        table = this.table;
		let get_tbl_chatprivate = `
		SELECT tbl_chat.*, DATE_FORMAT(tbl_chat.created_at, "%d/%m/%Y, %H:%i:%s") as created_at, code, avatar, fullname, email FROM (
			SELECT tbl_chat_private.*,tbl_user.id as userID, tbl_user.employeeID, 
			tbl_user.socketID, tbl_user.online
			FROM tbl_chat_private, tbl_user
			WHERE ${table}.id = ${insert_messenger_id}
			AND   ${user_send_id} IN (user_send_id, user_receive_id) 
			AND  tbl_user.id = user_send_id
		) as tbl_chat LEFT JOIN tbl_employees
			ON tbl_chat.employeeID = tbl_employees.id;
        `;
		
	
		return new Promise(function (resolve, reject) {
			conn.query(get_tbl_chatprivate, function (err, res, fields) {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({
						type: "success",
						messenger: res[0],
					});
				}
			});
		});
	},
	// lấy danh sách chat của người đó
    load_list_chat_send_receive: function (user_login_id,where = '',orderby = 'merge_tbl.view desc, merge_tbl.created_at desc') {
        table = this.table;
		let query = ` SELECT merge_tbl.*,tbl_employees.code, tbl_employees.avatar, tbl_employees.fullname as employee_fullname, tbl_employees.email as employee_email
		FROM (
		SELECT tbl_user.id,tbl_user.username, tbl_user.employeeID, tbl_user.socketID , tbl_user.online, tbl_user.email,
		tbl_user.fullname,tbl_user.verify,
		${table}.user_send_id, ${table}.user_receive_id,
		if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) AS userID ,
		MAX(view) as view,
		${table}.created_at   
		FROM ${table}, tbl_user
		WHERE  ${user_login_id} IN (user_send_id, user_receive_id) 
		AND tbl_user.id = if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) 
		AND tbl_user.id = user_send_id
		GROUP BY(userID)
		
		UNION
		
		SELECT tbl_user.id, tbl_user.username, tbl_user.employeeID, tbl_user.socketID , tbl_user.online, tbl_user.email,
			tbl_user.fullname,tbl_user.verify,
			${table}.user_send_id, ${table}.user_receive_id,
			if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) AS userID ,
			MAX(view) as view,
			${table}.created_at 
			FROM ${table}, tbl_user
			WHERE  ${user_login_id} IN (user_send_id, user_receive_id) 
			AND tbl_user.id = if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) 
			AND tbl_user.id NOT IN(
										SELECT 
										if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) AS userID
										FROM ${table}, tbl_user
										WHERE  ${user_login_id} IN (user_send_id, user_receive_id) 
										AND tbl_user.id = if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) 
										AND tbl_user.id = user_send_id
										GROUP BY(userID)
									)
			GROUP BY(userID)
		) AS merge_tbl LEFT JOIN  tbl_employees ON merge_tbl.employeeID = tbl_employees.id ${where} ORDER BY ${orderby}
        `;
		return new Promise(function (resolve, reject) {
            conn.query(query, function (err, res, field) {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({
						type: "success",
						list_chat: res,
					});
				}
			});
		});
	},
	//   lấy tin nhắn đầu tiên trong ds list chat để hiển thị
	load_first_messenger: function (userid_login, user_receive_id) {
        table           = this.table;

		let query       = `
		SELECT tbl_chat.*, DATE_FORMAT(tbl_chat.created_at, "%d/%m/%Y, %H:%i:%s") as created_at, code, avatar, fullname, email FROM (
						SELECT tbl_chat_private.*,tbl_user.id as userID, tbl_user.employeeID, 
						tbl_user.socketID, tbl_user.online
						FROM tbl_chat_private, tbl_user
						WHERE ${userid_login} IN (user_send_id, user_receive_id) 
						AND  ${user_receive_id} IN (user_send_id, user_receive_id)  
						AND  tbl_user.id = user_send_id
                    ) as tbl_chat LEFT JOIN tbl_employees
   						ON tbl_chat.employeeID = tbl_employees.id;
        `;
	
		return new Promise(function (resolve, reject) {
			conn.query(query, function (err, res, field) {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({
						type: "success",
						list_messenger: res,
					});
				}
			});
		});
	},
	// mới, code ngắn hơn
	load_list_chat_id: function(user_login_id,where = '')
	{
		table = this.table;
		let query = ` SELECT merge_tbl.socketID,merge_tbl.userID
		FROM (
		SELECT tbl_user.socketID,tbl_user.employeeID,
		if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) AS userID 
		FROM ${table}, tbl_user
		WHERE  ${user_login_id} IN (user_send_id, user_receive_id) 
		AND tbl_user.id = if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) 
		AND tbl_user.id = user_send_id
		GROUP BY(userID)
		
		UNION
		
		SELECT  tbl_user.socketID,tbl_user.employeeID,
			if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) AS userID 
			FROM ${table}, tbl_user
			WHERE  ${user_login_id} IN (user_send_id, user_receive_id) 
			AND tbl_user.id = if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) 
			AND tbl_user.id NOT IN(
										SELECT 
										if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) AS userID
										FROM ${table}, tbl_user
										WHERE  ${user_login_id} IN (user_send_id, user_receive_id) 
										AND tbl_user.id = if(user_send_id =  ${user_login_id} ,user_receive_id,user_send_id) 
										AND tbl_user.id = user_send_id
										GROUP BY(userID)
									)
			GROUP BY(userID)
		) AS merge_tbl LEFT JOIN  tbl_employees ON merge_tbl.employeeID = tbl_employees.id ${where}
        `;
		return new Promise(function (resolve, reject) {
            conn.query(query, function (err, res, field) {
				if (err) {
					reject(err);
					return;
				} else {
					resolve({
						type: "success",
						list_chat: res,
					});
				}
			});
		});
	},
};
