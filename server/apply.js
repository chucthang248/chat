module.exports = (io,socket,conn) =>{
    // page load
    socket.on("send_pageLoad_apply", function (data) {
        let query = `SELECT *, DATE_FORMAT(created_at,'%d/%m/%Y') as created_at  FROM tbl_apply where view = 0 ORDER BY id desc `;
        conn.query(query, function (err, result, fields) {
          if (err) console.log("send_pageLoad_apply : FAILS");
          io.emit("receive_pageLoad_apply",result);
        });
    });
     // check có người vừa ứng tuyển
     socket.on("id_apply", function (data) {
        let query = `SELECT *, DATE_FORMAT(created_at,'%d/%m/%Y')  as created_at FROM tbl_apply where view = 0 ORDER BY id desc`;
        conn.query(query, function (err, result, fields) {
          if (err) console.log("FAILS");
          io.emit("receive_pageLoad_apply",result);
        });
    });

// ====================SOCKET IO ==================
}