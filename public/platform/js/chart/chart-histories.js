function ChartHistories() {
	// let fontSizeTick = $(window).width() < 500 ? 6 : 15;
		//
		// fontSizeTick = $(window).width() > 678 ? 18 : fontSizeTick;
		// fontSizeTick = $(window).width() > 450 && $(window).width() <= 678 ? 8 : fontSizeTick;
		//
		//
		// var ctx = document.getElementById("canvas-chart-history");
		// let labels = [];
		// let data = [];
		// let bgColors = [];
		//
		// let chartHistories = new Chart(ctx,
		// {
		// 	"type": "bar",
		// 	"data":
		// 	{
		// 		"labels": labels,
		// 		"datasets": [{
		// 			"label": "",
		// 			"data": data,
		// 			"fill": false,
		// 			"backgroundColor": bgColors,
		// 			"borderWidth": 1
		// 		}]
		// 	},
		// 	"options": {
		//
		// 		"scales": {
		// 			"yAxes": [{
		// 				gridLines: {
		// 					color: "rgba(255, 255, 255, 0.5)",
		// 					borderDash: [3, 10],
		// 				},
		// 				"ticks": {
		// 					"beginAtZero": true,
		// 					"fontColor": "#bebebe",
		// 					"fontSize": fontSizeTick,
		// 				},
		// 			}],
		// 			"xAxes": [{
		// 				gridLines: {
		// 					color: "rgba(255, 255, 255, 0.5)",
		// 					borderDash: [0, 10],
		// 				},
		// 				"ticks": {
		// 					"fontColor": "#bebebe",
		// 					"fontSize": fontSizeTick,
		// 				},
		// 			}]
		// 		},
		// 		legend: {
		// 			display: false
		// 		},
		// 		tooltips: {
		// 			callbacks: {
		// 				label: function (tooltipItem) {
		// 					return tooltipItem.yLabel;
		// 				}
		// 			}
		// 		},
		// 	},
		// }
	// );

	function DrawHistoriesCircle(data) {
		let _html = "";
		for (let i = 0; i < 125; i++) {
			_html += `
			<div class="valueTrending__item">
				<div class="item__view ${data[i] ? getColorGame(data[i]) : "null"}"></div>
			</div>
			`;
		}
		$("#chart-circle").html(_html);
	}

	this.updateCharts = ({ chartBarHistories, chartCircleHistories }) => {
		// chartHistories.data.datasets.forEach((dataset) => {
		// 	dataset.backgroundColor = [];
		// 	dataset.data = [];
		// 	chartHistories.data.labels = [];
		//
		// 	chartBarHistories.forEach((ht, i) => {
		// 		const y = ht === null ? 0 : ht.crash_at / 100;
		// 		chartHistories.data.labels.push(i + 1);
		// 		dataset.data.push(y);
		// 		dataset.backgroundColor.push(COLOR[getColorGame(y)]);
		// 	})
			const dataChart = chartCircleHistories.slice(-125);
			DrawHistoriesCircle(dataChart.map(ht => ht === null ? 0 : ht.crash_at / 100));
		// });
		// chartHistories.update();
	}
}
