function innerHtml(ele, _html) {
    ele.append(_html);
}
function inverseGrowth(result) { // crashPoint -> time duration
    var c = 16666.666667;
    return c * Math.log(0.01 * result);
}
function growthFunc(ms) { // elapsed -> crashPoint
    var r = 0.00006;
    return Math.floor(100 * Math.pow(Math.E, r * ms));
}
function getColorGame(crash_at, type="bg") {
	if (crash_at < 2) {
		if (type === 'bg') return "bg__red";
		return "cl__red";
	}
	if (crash_at >= 2 && crash_at < 10) {
		if (type === 'bg') return "bg__green";
		return "cl__green";
	}
	if (type === 'bg') return "bg__yl";
	return "cl__yl";
}function getColorGameByStatus(status) {
	if (status === 'PLAYING') return "";
	else if (status === 'CASHED_OUT') return "cl__green";
	return "cl__red";
}
function exchangeMoney(type = "usg-pins", amount) {
    switch (type) {
        case "usg-pins": {
            return +amount * 10;
        }
        case "pins-usg": {
            return +amount / 10;
        }
        default: return +amount;
    }
}
function _ajax(url, method, data, cb) {
    $.ajax({
        url: "/api" + url, method,
        data,
        success: data => {
            cb(null, data);
        },
        error: er => {
            er ? cb(JSON.parse(er.responseText)) : cb(true)
        }
    })
}
const _libs = {
// isNumber:
}
const COLOR = {
    red: "#ef6862",
    green: "#1fd384",
    yellow: "#ffff69"
}
// toastr config
toastr.options = {
    "closeButton": true,
    "positionClass": "toast-bottom-right",
    "progressBar": true,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

function x2Coin() {
    const amount = parseFloat($("#input-bet").val());
    if (amount > 0) {
        $("#input-bet").val(amount * 2);
    }
}

function halfCoin() {
    const amount = parseFloat($("#input-bet").val());
    if (amount > 0) {
        result = amount / 2;
        if (result < 0.1) {
            result = 0.1
        }
        $("#input-bet").val(result);
    }
}

function coin10() {
    const amount = parseFloat($("#input-bet").val());
    if (amount > 0) {
        $("#input-bet").val(amount + 10);
    }
}

function coin100() {
    const amount = parseFloat($("#input-bet").val());
    if (amount > 0) {
        $("#input-bet").val(amount + 100);
    }
}

function coinMin() {
	$("#input-bet").val(0.1);
}

function replaceAll(str, find = ',', replace = '') {
	return str.replace(new RegExp(find, 'g'), replace);
}

function getTotalMoneyNumber() {
	var current = $('#wallet-main').html();
	current = replaceAll(current, ',', '');
	current = parseFloat(current);
	if (isNaN(current)) {
		var regex = /[\d|,|.|e|E|\+]+/g;
		current = $('#total_money').html().match(regex)['1'];
		current = replaceAll(current, ',', '');
		current = parseFloat(current);
	}
	return current;
}

function coinMax() {
	const money = getTotalMoneyNumber();
	$("#input-bet").val(money);
}

function check(input) {
    if (input.value == 0) {
        input.setCustomValidity('The number must not be zero.');
    } else {
        input.setCustomValidity('');
    }
}
