// const betChart = BetChart();
// const historiesManager = new HistoryManager();
const chartHistories = new ChartHistories();

let timeoutCountdown = new Object();


const y = x => x * x;
const getY = time => parseInt((y(time / 1000) + 100))
const formatCrashOut = co => co ? (+co / 100).toFixed(2) : "-";

const STATUS_ALL_HIDE = "000";
const STATUS_COUTDOWN = "001";
const STATUS_CRASHING = "100";
const STATUS_CRASHED = "010";

const BTN_BETTING = "BTN_BETTING";
const BTN_PLACED = "BTN_PLACED";
const BTN_READY = "BTN_READY";

const getProfit = (cashed_out, bet, bonus) => {
    if (cashed_out) {
        return '+' + (parseFloat((+bet * (+cashed_out - 100) / 100)) + parseFloat(+bonus)) * 0.95
    }
    return +bet * -1;
}

function showBigAnimal(at) {
    if (at > 300) {
        $(".animal-big-crash-out #phoenix-gif").show(100);
    } else {
        $(".animal-big-crash-out img").hide(0);
    }
    // if (at >= 1000 && at < 5000) {
    //     $(".animal-big-crash-out #phoenix-gif").hide(0);
    //     // $(".animal-big-crash-out #horse-gif").show(100);
    // } else if (at >= 5000) {
    //     $(".animal-big-crash-out #phoenix-gif").show(100);
    //     $(".animal-big-crash-out #horse-gif").hide(0);
    // } else {
    //     $(".animal-big-crash-out img").hide(0);
    // }
}

function showStatus(status, type = "not-playing") {
    +status[0] ? $(".crashing").show(0) : $(".crashing").hide(0);
    +status[1] ? $(".crashed").show(0) : $(".crashed").hide(0);
    +status[2] ? $(".count-down-time").show(0) : $(".count-down-time").hide(0);

    if (type == "playing") {
        $(".crashing").addClass("playing");
    } else $(".crashing").removeClass("playing");
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function UpdateAccountUser(wallets) {
	if (wallets && wallets.length > 0) {
		const coin = $('#input-bet').attr('data-coin');
		let child = 0;
		let html = '';
		wallets.forEach((wallet, index) => {
			if (wallet.name === coin) child = index;
			html += `
				<li class="item">
					<div class="item__boxMoney">
						<img class="item__imgMoney" src="${wallet.image}" />
						<span class="item__money">${wallet.money}</span>
					</div>
					<span class="item__coin">${wallet.name}</span>
				</li>
			`;
		});
		$('.list-wallet').html(html);
		if ($('.dropdown .dropdown-menu li')[child]) {
			const first = $('.dropdown .dropdown-menu li')[child];
			selectWallet(first);
		}
	}
}

function getHtmHistoryLine(history) {
    let crash_at = formatCrashOut(history.crash_at);
    const history_id = history.id ? history.id : '?';
    return `
    	<div class="value__item">
			<div class="item__circle ${getColorGame(crash_at)}"></div>
			<div class="item__value">
				<p class="value__key">${history_id}</p>
				<p class="value__number ${getColorGame(crash_at, 'p')}">${crash_at}X</p>
			</div>
		</div>
    `;
    // return `
    //     <span class="${getColorGame(crash_at)}">${crash_at}x</span>
    // `;
}

function getHtmlRowHistory(history) {
    let crash_at = formatCrashOut(history.crash_at);
    return `
            <tr>
                    <td width="30%" class="table-color-${getColorGame(crash_at)}">${crash_at}x</td>
                    <td width="10%">-</td>
                    <td width="10%">-</td>
                    <td class="hash-history-text" width="10%">-</td>
                    <td width="10%">-</td>
                    <td class="hash-history-text" width="30%"> <span><span class="text-hash">${history.hash.slice(0, 10)}..</span>  <img src="public/platform/images/hist-icon.png"></span></td>
            </tr>
    `;
}

function getHtmlRowBoardPlayer(player) {
    let status = {
        "PLAYING": "table-color-betting", // cược
        "CASHED_OUT": "table-color-war", // win
        "LOSS": "table-color-red", // thua
    };

    // return `
    //     <tr class="${status[player.status]}">
    //         <td width="25%" ><a href="/user/${player.user.username}">${player.user.username}</a></td>
    //         <td width="15%" >${player.status == "LOSS" ? "-" : (formatCrashOut(player.stoppedAt) || "-")}</td>
    //         <td width="20%" >${player.status !== "PLAYING" ? player.bet : "-"}</td>
    //         <td width="20%" >${player.bonus ? parseFloat(player.bonus).toFixed(2) : "-"}</td>
    //         <td width="20%" >${player.profit ? parseFloat(player.profit).toFixed(2) : "-"}</td>
    //     </tr>
    // `
	const profit = getProfit(player.stoppedAt, player.bet, player.bonus);
	let class_color = getColorGameByStatus(player.status);
	let earn = `
		<img class="infoCoin__img" src="${player.user.coin.image}" />
		<p class="infoCoin__number ${class_color}">${profit}</p>
		
	`;
    // <p class="infoCoin__unit">${player.user.coin.name}</p>
	let status_td = 'betting';
	if (player.status === 'CASHED_OUT') status_td = formatCrashOut(player.stoppedAt) + 'x' || "-";
	else if (player.status === 'LOSS') status_td = 'bang';
	else earn = '<p class="infoOut__text">betting</p>';

	return `
        <tr class="${status[player.status]}">
        	<td>
				<div class="infoUser">
					<img class="infoUser__img" src="public/platform/images/icon/avatar.png" />
					<a href="" class="infoUser__name">${player.user.fullname}</a>
				</div>
			</td>
			<td><p class="infoOut__text ${class_color}">${status_td}</p></td>
            <td>
				<div class="infoCoin">
					<img class="infoCoin__img" src="${player.user.coin.image}" />
					<p class="infoCoin__number">${player.bet}</p>
					
				</div>
			</td>
			<td>
				<div class="infoCoin">${earn}</div>
			</td>
        </tr>
    `;
    // <p class="infoCoin__unit">${player.user.coin.name}</p>
}

function countDown(time) {
    // clearTimeout(timeoutCountdown);
    // $(".count-down-time .txt-value").text((time / 1000).toFixed(1));
    // timeoutCountdown = setTimeout(() => {
    //     countDown(time - 100)
    // }, 100);
}

const getTypeCashOut = pl => pl.stoppedAt ? pl.stoppedAt : pl.auto_cash_out
const sortPlayers = (players) => {
    return Object.entries(players)
        .sort((a, b) => getTypeCashOut(a[1]) - getTypeCashOut(b[1]))
}

const arrangeBoardPlayers = players => {
    let playersCashedOut = [];
    let playersPlaying = [];
    let playersLoss = [];

    Object.entries(players).forEach(([username, player]) => {
        if (player.status == "PLAYING") {
            playersPlaying.push(player);
        }
        if (player.status == "CASHED_OUT") {
            playersCashedOut.push(player);
        }
        if (player.status == "LOSS") {
            playersLoss.push(player);
        }
    })

    return [
        ...playersLoss.sort((a, b) => a.bet - b.bet),
        ...playersPlaying.sort((a, b) => a.bet - b.bet),
        ...playersCashedOut.sort((a, b) => b.stoppedAt - a.stoppedAt),

        // ...players_notCrashed.sort((a, b) => a.bet <= b.bet ? 1 : -1),
        // ...players_crashed.sort((a, b) => a.crashed_out <= b.crashed_out ? 1 : -1),
    ];
}

function DrawBoardPlayers(players) {
	$(".player__count").html(`${Object.keys(players).length} PLAYERS`);
    $(".board-players tbody").html(
        arrangeBoardPlayers(players)
            .reduce(
                (cur, player) => cur + getHtmlRowBoardPlayer(player),
                ''
            )
    )
}

// da comment
function DrawHistoriesLine(histories) {
	const data = histories.slice(0, 6).reverse();
    $(".histories-line").html(data.reduce((cur, history) => cur + getHtmHistoryLine(history), ''))
}

function DrawTableHistories(histories) {
    $(".history-table table tbody").html(histories.reduce((cur, history) => cur + getHtmlRowHistory(history), ''))
}


function DrawGameHistories(data, type) {
    if (type == "PREPEND") {
        $(".histories-line .value__item").first().remove();

        $(".history-table table tbody").append(getHtmlRowHistory(data))
        $(".histories-line").append(getHtmHistoryLine(data));
    }
    if (type == "HTML") {
        DrawTableHistories(data);
        DrawHistoriesLine(data);
    }
}

function showPlayerWin(money) {
    $('#notify').show();
    if (money > 0) {
        $("#status-player").html('You win');
    } else {
        $("#status-player").html('You lose');
    }
    $("#money-change").html(`$${money}`);
    setTimeout(() => {
        $('#notify').hide();
    }, 1500);
}

function showPlayerBonus(money) {
    document.getElementById("audio-win_sound").play();
    $("#modal-game-result").modal("show");

    $("#modal-game-result #text-game-status").text("Your Bonus");
    $("#text-game-result").html(`<span id="money">${money}</span><b id="currency"> PINS</b>`)

    // $("#modal-game-result #text-game-result #currency").text("pins: ")
    // $("#modal-game-result #money").text(money);
    setTimeout(() => {
        $("#modal-game-result").modal("hide");
    }, 2300);
}

const showGameConnecting = (isConnecting) => {
    if (isConnecting) {
        $(".Play .area-connecting").fadeIn(0);
    } else {
        $(".Play .area-connecting").fadeOut(1000);
    }
}

const showStatusBarPlayers = (total_players, total_betting) => {
    $(".betting-bar .players-count .txt-value").text(total_players);
    $(".betting-bar .betting-profit .txt-value").text(total_betting);
}

const showStatusBtnPlaceBet = (status = BTN_READY, profit = 0) => {
    $(".btn-place-bet").removeClass("betting");
    $(".btn-place-bet").removeClass("placed");
    $(".btn-place-bet").removeClass("actived");


    switch (status) {
        case BTN_BETTING: {
            $(".btn-place-bet").addClass("betting");
            $(".btn-place-bet").addClass("actived");
            $(".btn-place-bet #txt-btn-place-bet").text("BETTING (Payout)");
            // $(".btn-place-bet #btn-bet span").text("Cash out @" + profit);
            break;
        }
        case BTN_PLACED: {
            $(".btn-place-bet").addClass("actived");
            $(".btn-place-bet").addClass("placed");
            $(".btn-place-bet #txt-btn-place-bet").text("BET (Cancel)");
            break;
        }
        case BTN_READY: {
            $(".btn-place-bet #txt-btn-place-bet").text("BET");
            break;
        }
        default: return;
    }
}

const totalBet = (players) => Object.entries(players).map(item => item[1]).reduce((cur, next) => cur + +next.bet, 0);

const getEventBtnBetClick = cb => {
    $(".frm_trade").off("click").on("click", "#btn-bet", (e) => {
        const amount = parseFloat($("#input-bet").val());
        const coin = 'BTC';
        // const coin = $("#input-bet").attr('data-coin');
        // const image = $("#input-bet").attr('data-image');
        const image = './public/platform/images/icon/ETH.png';
        const autoCashOut = parseInt(parseFloat($("#input-crash-out").val()) * 100);
        var boolean = true;
        if (amount < 0.1 || amount === '' || isNaN(amount)) {
            boolean = false;
            $("#input-bet").val(1);
            toastr.error("Amount trade cannot lower than 0.1 or empty");
        }
        if (autoCashOut <= 103 || autoCashOut === '' || isNaN(autoCashOut)) {
            boolean = false;
            $("#input-crash-out").val(2);
            toastr.error("Amount trade cannot lower than 1.03 or empty");
        }
        if (boolean) {
            if (amount && autoCashOut) {
                cb({
                    amount,
                    autoCashOut,
					coin,
					image
                });
            } else {
                toastr.error("Can not place a trade with " + amount + "  " + autoCashOut)
            }
        }
    })
}

const uppdateAutoBetConfig = (config, isWin) => {
    if (isWin) {
        if (!config.on_win.is_return_base_bet) {
            config.cur_bet += +config.on_win.increase_bet_by;
        } else {
            config.cur_bet = config.base_bet;
        }
    } else {
        if (!config.on_loss.is_return_base_bet) {
            config.cur_bet += +config.on_loss.increase_bet_by;
        } else {
            config.cur_bet = config.base_bet;
        }
    }
    if (config.cur_bet > config.max_bet_stop) {
        $(".bet-logged .auto-control #auto-bar-stop").trigger("click");
        config.is_auto = false;
    }
    return config;
}

const getDataFromAutoForm = () => {
    let base_bet = parseInt($(".type-auto-bet #base_bet").val() || 1);
    let auto_crash_out = parseFloat($(".type-auto-bet #auto_crash_out").val() || 1) * 100;
    return {
        is_auto: false,
        base_bet,
        cur_bet: base_bet,
        cur_auto_crash: auto_crash_out,
        auto_crash_out,
        max_bet_stop: parseFloat($(".type-auto-bet #max_bet_stop").val() || 1),
        on_loss: {
            is_return_base_bet: $(".type-auto-bet .radio-group input[name='on-loss']:checked").val() === "return",
            increase_bet_by: parseFloat($(".type-auto-bet #on_loss_increase_value").val() || 1),
        },
        on_win: {
            is_return_base_bet: $(".type-auto-bet .radio-group input[name='on-win']:checked").val() === "return",
            increase_bet_by: parseFloat($(".type-auto-bet #on_win_increase_value").val() || 1)
        },
    };
}

const getEventAutoBarClick = (cb) => {
    $(".bet-logged").off("click").on("click", ".auto-control .auto-control-bar button", (e) => {
        let AutoBetConfig = getDataFromAutoForm();

        if (e.target.id == "auto-bar-run") {
            cb({ ...AutoBetConfig, is_auto: true });
        }

        if (e.target.id == "auto-bar-stop") {
            cb({ ...AutoBetConfig, is_auto: false });
        }
    })
}

const makeAutoBet = (config, cb) => {
    if (config.is_auto) {
        cb({
            bet: config.cur_bet,
            auto_crash: parseFloat(config.cur_auto_crash),
        })
    }
}

const CheckOnData = (data, check, cb) => {
    data.find(check) ? cb(data.find(check)) : cb(false);
}

const checkWin = game => game.crashed_out !== 0;
const checkLose = game => game.crashed_out === 0;

const checkExistPlayer = (players, userName, take = "user") => {
    if (
        players[userName] &&
        players[userName].user &&
        players[userName].user.username === userName &&
        players[userName][take]) {
        return players[userName][take];
    }
    return null;
}

// function GameBet({ userId, userName }, jwt_token) {
function GameBet({ userName }, jwt_token) {
    let CACHED = {
        playBet: null,
    }
    let players = {};
    // let AutoBetConfig = getDataFromAutoForm();

    const enventHandler = io => {

        getEventBtnBetClick((newBet) => {
            placeBet(io, newBet);
        })

        // getEventAutoBarClick(config => {
        //     AutoBetConfig = config;
        // })
    }

    const placeBet = (io, newBet) => {
        io.emit("place_bet", newBet, (er) => {
            if (er == 'BETTING') {
                showStatusBtnPlaceBet(BTN_PLACED);
            }
            if (er == "CANCEL_BETTING") {
                delete players[userName];
                DrawBoardPlayers(players);
                showStatusBtnPlaceBet(BTN_READY);
            }

            if (er == "GAME_IN_PROGRESS") {
                switch (checkExistPlayer(players, userName, "status")) {
                    case "PLAYING": {
                        io.emit("cash_out", er => {
                            er && console.log(er);
                        });
                        break;
                    }
                    default: {
                        // da cuoc thi` set null, sau do click lan nua thi cuoc
                        if (CACHED.playBet) {
                            CACHED.playBet = null;
                            showStatusBtnPlaceBet(BTN_READY);
                        } else {
                            showStatusBtnPlaceBet(BTN_PLACED);
                            CACHED.playBet = newBet;
                        }
                    }
                }
            }

            if (er == "NOT_ENOUGH_COIN") {
                toastr.error("Your balance is not enough");
                showStatusBtnPlaceBet(BTN_READY);
            }

			if (er == "ERR_AUTH") {
				toastr.error("Cannot valid user");
			}

            if (er && er.message == "LIMIT_PLACE_BET") {
                toastr.error(er.limit);
            }
        });
    }

    const connect = (io) => {
        /**
         * Game place
         */

        io.on("close-site", function() {
            location.reload();
        });

        io.on("total-player-random", (data) => {
            $('.total-player').html(numberWithCommas(data));
        });

        io.on("random-playing", (data) => {
            $('.total-playing').html(numberWithCommas(data.people));
            $('.total-betting').html(numberWithCommas(data.money));
        });

        // get status chart to draw chart
        io.on("game_status", (data) => {
            const elapsed = Math.abs(Date.now() - new Date(data.startTime).getTime())
            players = data.players;
            showBigAnimal(false);

            if (data.game_state == "STARTING") {
                ChartCanvas().starting();
                if (checkExistPlayer(players, userName, "status") == "PLAYING") {
                    showStatusBtnPlaceBet(BTN_PLACED);
                }
            }

            if (data.game_state == "IN_PROGRESS") {
                ChartCanvas().started(elapsed);
                if (checkExistPlayer(players, userName, "status") == "PLAYING") {
                    showStatusBtnPlaceBet(BTN_BETTING);
                    ChartCanvas().setPlayer(true);
                }
            }

            if (data.game_state == "ENDED") {
                // showStatus(STATUS_CRASHED);
                ChartCanvas().stoped(data.forcePoint);
                // $(".chart-canvas .crashed .txt-value").text(formatCrashOut(data.forcePoint));
            }
            DrawGameHistories(data.game_histories, "HTML")
            DrawBoardPlayers(players);
			chartHistories.updateCharts(data.historyChart);
        })

        io.on("tick", time => {
            let yChart = growthFunc(time);
            showBigAnimal(yChart);
            if (players[userName] && players[userName].status == "PLAYING") {
                const pl = players[userName];
                const profit = parseFloat(pl.bet * (yChart / 100)).toFixed(2);
                showStatusBtnPlaceBet(BTN_BETTING, profit);
            }
        })

        // recived when user emit place and server response
        io.on("bet", (data) => {
            players[data.user.username] = data;
            DrawBoardPlayers(players);
            if (checkExistPlayer(players, userName, "status")) {
                showStatusBtnPlaceBet(BTN_PLACED);
            }
        })

        io.on("game_starting", data => {
            $('.total-playing').html(0);
            $('.total-betting').html(0);
            players = {};
            ChartCanvas().starting();

            if (CACHED.playBet) {
                placeBet(io, CACHED.playBet);
                showStatusBtnPlaceBet(BTN_PLACED);
                CACHED.playBet = null;
            } else {
                showStatusBtnPlaceBet(BTN_READY);
            }
            DrawBoardPlayers(players);
            showStatusBarPlayers("?", "?");
        })

        io.on("bets", data => {
            DrawBoardPlayers(data.bets);
        })

        io.on("game_started", data => {
            players = data.bets;
            ChartCanvas().started();

            DrawBoardPlayers(players);
            showStatusBarPlayers(Object.keys(players).length, totalBet(players));
            // check user is login
            if (checkExistPlayer(players, userName, "status")) {
                ChartCanvas().setPlayer(true);
                showStatusBtnPlaceBet(BTN_BETTING);
            } else {
                ChartCanvas().setPlayer(false);
            }
        })

        io.on("game_crash", data => {
            // historiesManager.addHistory(data.history);
            DrawGameHistories(data.history, "PREPEND");
            chartHistories.updateCharts(data.historyChart);
            showBigAnimal(false);
            ChartCanvas().stoped(data.history.crash_at);

            DrawBoardPlayers(data.players);
            if (CACHED.playBet == null) {
                showStatusBtnPlaceBet(BTN_READY);
            }

            CheckOnData(
                Object.entries(data.players).map(item => item[1]),
                player => player.user.username === userName,
                (player) => {
                    if (player) {
                        if (player.status == 'LOSS') {
                            showPlayerWin((player.profit).toFixed(2));
                        }
                        io.emit("my_account", jwt_token, data => UpdateAccountUser(data))
                        // if (player.bonus > 0) {
                        //     showPlayerBonus(
                        //         player.bonus.toFixed(2)
                        //     );
                        // }
                    }
                }
            );
        })

        io.on("cashed_out", player => {
            players[player.user.username] = player;
            DrawBoardPlayers(players);
            if (player.user.username === userName) {
                // let profit = getProfit(player.stoppedAt, player.bet, player.bonus);
                // if (+player.stoppedAt > 100) {
                    // showPlayerWin(
                    //     (exchangeMoney("pins-usg", profit)).toFixed(2)
                    // );
                // }
                showPlayerWin((player.profit).toFixed(2));
               
                // betChart.setColorLine("not-playing");
                // showStatus(STATUS_CRASHING, "not-playing");
                showStatusBtnPlaceBet(BTN_READY);
                ChartCanvas().setPlayer(false);

                CACHED.playBet = null;
                io.emit("my_account", jwt_token, data => UpdateAccountUser(data))
            }
        })

        io.on('reload_page', () => {
            window.location.reload();
        })

        io.on("err", err => {
            console.log("ERROR", err);
            toastr.error(err || "Cant not do this actions");
        })
        enventHandler(io);
    }

    return {
        connect,
    }
}
