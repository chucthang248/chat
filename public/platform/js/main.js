window.onscroll = function() {menuFix()};
var menu = document.getElementById("menuMain");
var sticky = menu.offsetTop;
function menuFix() {
	console.log(window.pageYOffset);
    if (window.pageYOffset > sticky) {
        menu.classList.add("sticky");
    } else {
        menu.classList.remove("sticky");
    }
}
$(document).ready(function(){
	$('.slider-client').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		dots:true
	});
	$('.slider-partner').slick({
		infinite: true,
		slidesToShow: 8,
		slidesToScroll: 1,
		autoPlay:true,
		speed:1000,
		dots:false,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: false,
		        slidesToShow: 5
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: false,
		        slidesToShow: 2
		      }
		    }
	  	]
	});
	$('.sliderProductDetail').slick({
		infinite: true,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoPlay:true,
		speed:1000,
		dots:false,
		responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: false,
		        slidesToShow: 4
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: false,
		        slidesToShow: 2
		      }
		    }
	  	]
	});
	$('.btn-menu-mobi').click(function(){
	    if($('.ul-menu-mobi').hasClass('move_mobi_fix')){
			$('.ul-menu-mobi').removeClass('move_mobi_fix');
			$('#bg-fix').css('display','none');
		}else{
			$('.ul-menu-mobi').addClass('move_mobi_fix');
			$('#bg-fix').css('display','block');
		} 
	});
	$('#bg-fix').click(function(){
		$('#bg-fix').css('display','none');
		$('.ul-menu-mobi').removeClass('move_mobi_fix');
	});

    $('#slider').nivoSlider();

    //Back to Top
    if ($('#back-to-top').length) {
	    var scrollTrigger = 100, // px
	        backToTop = function () {
	            var scrollTop = $(window).scrollTop();
	            if (scrollTop > scrollTrigger) {
	                $('#back-to-top').addClass('show');
	            } else {
	                $('#back-to-top').removeClass('show');
	            }
	        };
	    backToTop();
	    $(window).on('scroll', function () {
	        backToTop();
	    });
	    $('#back-to-top').on('click', function (e) {
	        e.preventDefault();
	        $('html,body').animate({
	            scrollTop: 0
	        }, 700);
	    });
	}

});
