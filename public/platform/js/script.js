$(document).ready(function () {
    // DROPDOWN SELECT
    $('.dropdown').click(function () {
        $(this).toggleClass('active');
        $(this).find('.dropdown-menu').slideToggle(300);
    });
    $('.dropdown').focusout(function () {
        $(this).removeClass('active');
        $(this).find('.dropdown-menu').slideUp(300);
    });
    if ($('.dropdown .dropdown-menu li')[0]) {
        const first = $('.dropdown .dropdown-menu li')[0];
		selectWallet(first);
    }
    $(document).on('click', '.dropdown .dropdown-menu li', function () {
		selectWallet(this);
	});

    $('.btn__checkbox').change(function() {
		if(this.checked) {
			$('.blockChart__cateGroup').addClass('hidden');
			$('.blockChart__valueTrending').removeClass('hidden');
		} else {
			$('.blockChart__cateGroup').removeClass('hidden');
			$('.blockChart__valueTrending').addClass('hidden');
		}
	});

    $('.tab-my-bets').click(function() {
		getMyBets('platform/myBets/1', '#my-bets');
	});
    $('.tab-history-game').click(function() {
		getMyBets('platform/historyGame/1', '#history-games');
	});

	$(document).on('click', '.pagination-myBets .pageNavigation__item a', function (e) {
		e.preventDefault();
		getMyBets($(this).attr('href'), '#my-bets');
	});

	$(document).on('click', '.pagination-historyGame .pageNavigation__item a', function (e) {
		e.preventDefault();
		getMyBets($(this).attr('href'), '#history-games');
	});
});

function selectWallet($this) {
	const img = $($this).find('.item__imgMoney').attr('src');
	const money = $($this).find('.item__money').text();
	const coin = $($this).find('.item__coin').text();
	$('.dropdown').find('.select .item__imgMoney').attr('src', img);
	$('.dropdown').find('.select .item__money').text(money);
	$('.dropdown').find('.select .item__coin').text(coin);

	$('.blockStakes__number img').attr('src', img);
	$('#input-bet').attr('data-coin', coin);
	$('#input-bet').attr('data-image', img);
}

function getMyBets(url, elm) {
	$.ajax({
		url: url,
	}).done(function(res) {
		$(elm).html(res);
	});
}


