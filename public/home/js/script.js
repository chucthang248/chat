$(document).ready(function() {
    //banner top
    $('.bannerSlide').slick({
        infinite: true,
        autoplay: false,
        speed: 800,
        arrows: false,
        dots: false,
    });

    // scroll to top 
    $('.go__top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({ scrollTop: 0 }, 300);
    });

});
//back to top
$(window).scroll(function() {
    if ($(this).scrollTop() > 200) {
        $('.go__top').fadeIn(200);
    } else {
        $('.go__top').fadeOut(200);
    }
});

//back to top End
function SlideForm(type) {
    if (type == "close") {
        $(".register_form").removeClass("active_register");
        $(".login_form").removeClass("active_register");
        $(".overplay_form").removeClass("active_register");
        $("html").css({ "overflow-y": "auto" });
    }
    if (type == "register") {
        $(".register_form").addClass("active_register");
        $(".overplay_form").addClass("active_register");
    }
    if (type == "login") {
        $(".login_form").addClass("active_register");
        $(".overplay_form").addClass("active_register");
    }
    if (type == "register" || type == "login") {
        $("html").css({ "overflow-y": "hidden" });
    }
}

// ul li select option
$('.select ul li.option').click(function() {
    // $(this).siblings().addBack().children().remove();
    $(this).siblings().toggle();
    $(this).parent().prepend(this);
})