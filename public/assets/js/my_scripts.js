$(document).ready(function(){
    $('.check-all').click(function() {  //on click
        if(this.checked) { // check select status
            $('.checkbox-list').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"
            });
        }else{
            $('.checkbox-list').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"
            });
        }
    });
    $('html').bind('keypress', function(e)
    {
        if(e.keyCode == 13)
        {
            return false;
        }
    });
    $('#del-all').click(function(){
        if($('.check-all').is(':checked') || $('.checkbox-list').is(':checked')) {
            var control = $(this).attr('data-control');
            swal({title: "Are you sure?",showCancelButton: true,

            }
            , function(isConfirm){
                if (isConfirm) {
                    var listid="";
                    $("input[name='chon']").each(function(){
                    if(this.checked){
                      $(this).parent().parent().parent().fadeOut();
                      listid = listid+","+this.value;
                    }
                    })
                    listid=listid.substr(1);
                    if(listid != '')
                    {
                        $.ajax
                        ({
                         method: "POST",
                         url: "otadmin/"+control+"/deleteall",
                         data: { listid:listid},
                        });
                        swal("Chúc mừng!", "Xóa thành công.", "success");
                    }
                } else {
                    swal("Dữ liệu của bạn đã không bị xóa!");
                }
            });
        }else{
          swal('Vui lòng chọn đối tượng bạn muốn thao tác!!');
        }
    });
    setTimeout(function(){ $('#box-notify').fadeOut(); }, 2000);
});
function del(id) {
    Swal.fire({
        title: "Bạn có chắc chắn muốn xóa không?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Đồng ý"
      }).then(function (result) {
        if (result.value) {
            var control = $('.delete'+id).attr('data-control');
            if(id != '')
            {
                $.ajax
                ({
                    method: "POST",
                    url: "cpanel/"+control+"/delete",
                    data: { id:id},
                    success: function(data)
                    {
                        if (data != "") {
                            let parseData = JSON.parse(data);
                            if (parseData.type == "error") {
                                Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                            }
                            
                        } else {
                            $('.delete'+id).parent().parent().fadeOut();
                            Swal.fire("Xóa thành công!", "", "success");
                            $(".tr" + id).remove();
                        }
                    }
                });
            }
        }
    });
}
function checkStatus(id,properties){
    var control = $('#'+properties+id).attr('data-control');
    
    var field = 0;
    if($('#' + properties + id).is(':checked')){ field = 1; }
    if(id != '')
    {
         
        $.ajax
        ({
            method: "POST",
            url: "cpanel/"+control+"/"+properties,
            data: { id:id, field:field, properties:properties},
            dataType: "json",
            success: function(data){
                if(data.result){
                    $.toast({
                        heading: 'Thông báo!',
                        text: 'Cập nhật trạng thái hiển thị thành công!.',
                        position: 'top-right',
                        loaderBg: '#5ba035',
                        icon: 'success',
                        hideAfter: 2000,
                        // stack: 1
                    });
                }
                 
            }
        });
        
    }
}
function changeSort(sort,id){
    var control = $('.publish'+id).attr('data-control');
    var sort = sort.value;
    if(id != '')
    {
        $.ajax
        ({
            method: "POST",
            url: "otadmin/"+control+"/sort",
            data: { id:id,sort:sort},
            dataType: "json",
            success: function(data){
                if(data.rs == 1){
                    $.toast({
                        text: 'Cập nhật thứ tứ thành công!',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'success',
                        hideAfter: 3000,
                        stack: 6
                    });
                }
            }
        });
    }
}
function changeSort2(sort,id){
    var control = $('.sort'+id).attr('data-control');
    var sort = sort.value;
    if(id != '')
    {
        $.ajax
        ({
            method: "POST",
            url: "otadmin/"+control+"/sort",
            data: { id:id,sort:sort},
            dataType: "json",
            success: function(data){
                if(data.rs == 1){
                    $.toast({
                        text: 'Cập nhật thứ tứ thành công!',
                        position: 'top-right',
                        loaderBg: '#ff6849',
                        icon: 'success',
                        hideAfter: 3000,
                        stack: 6
                    });
                }
            }
        });
    }
}
function loadCity(country_id){
    var country_id = country_id.value;
    if(country_id != ''){
        $('#city_id').html('<option>Loading.....</option>');
        $.ajax
        ({
            method: "POST",
            url: "otadmin/ajax/loadcity",
            data: { country_id:country_id},
            dataType: "html",
            success: function(data){
                if(data != ''){
                  $('#city_id').html(data);
                }else{
                  $('#city_id').html('');
                }
            }
        });
    }
}
function loadDistrict(city_id){
    var city_id = city_id.value;
    if(city_id != ''){
        $('#district_id').html('<option>Loading.....</option>');
        $.ajax
        ({
            method: "POST",
            url: "otadmin/ajax/loaddistrict",
            data: { city_id:city_id},
            dataType: "html",
            success: function(data){
                if(data != ''){
                  $('#district_id').html(data);
                }else{
                  $('#district_id').html('');
                }
            }
        });
    }
}
function FormatNumber(num)
{
    var pattern = "0123456789.";
    var len = num.value.length;
    if (len != 0)
    {
        var index = 0;

        while ((index < len) && (len != 0))
            if (pattern.indexOf(num.value.charAt(index)) == -1)
            {
                if (index == len-1)
                    num.value = num.value.substring(0, len-1);
                else if (index == 0)
                        num.value = num.value.substring(1, len);
                     else num.value = num.value.substring(0, index)+num.value.substring(index+1, len);
                index = 0;
                len = num.value.length;
            }
            else index++;
    }
    val = num.value;
    val = val.replace(/[^\d.]/g,"");
    arr = val.split('.');
    lftsde = arr[0];
    rghtsde = arr[1];
    result = "";
    lng = lftsde.length;
    j = 0;
    for (i = lng; i > 0; i--){
        j++;
        if (((j % 3) == 1) && (j != 1)){
            result = lftsde.substr(i-1,1) + "," + result;
        }else{
            result = lftsde.substr(i-1,1) + result;
        }
    }
    if(rghtsde==null){
        num.value = result;
    }else{
        num.value = result+'.'+arr[1];
    }
}
function del_recruitment(id,type = null,crossDomain = null) {
   // debugger;
    Swal.fire({
        title: "Bạn có chắc chắn muốn xóa không?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Đồng ý"
      }).then(function (result) {
        if (result.value) {
            var control = $('.delete'+id).attr('data-control');
            if(id != '')
            {
                if(type == "true")
                {
                    $.ajax
                    ({
                        method: "POST",
                        crossDomain: true,
                        url: crossDomain+"deleteApply/"+id,
                        data: {id:id},
                        success: function(data)
                        { 
                            //debugger;
                           $.ajax
                           ({
                               method: "POST",
                               url: "recruitment/"+control+"/delete",
                               data: { id:id},
                               success: function(data)
                               {
                                   if (data != "") {
                                       let parseData = JSON.parse(data);
                                       if (parseData.type == "error") {
                                           Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                                       }
                                   } else {
                                       $('.delete'+id).parent().parent().fadeOut();
                                       Swal.fire("Xóa thành công!", "", "success");
                                       $(".tr" + id).remove();
                                   }
                               }
                           });    
                        }
                    });
                }
                else
                {
                    $.ajax
                    ({
                        method: "POST",
                        url: "recruitment/"+control+"/delete",
                        data: { id:id},
                        success: function(data)
                        {
                            if (data != "") {
                                let parseData = JSON.parse(data);
                                if (parseData.type == "error") {
                                    Swal.fire("Bạn không đủ quyền thực hiện tác vụ này!", "", "error");      
                                }
                            } else {
                                $('.delete'+id).parent().parent().fadeOut();
                                Swal.fire("Xóa thành công!", "", "success");
                                $(".tr" + id).remove();
                            }
                        }
                    });    
                }
               
            }
        }
    });
}

function checkStatus_recruitment(id,properties){
    var control = $('#'+properties+id).attr('data-control');
    
    var field = 0;
    if($('#' + properties + id).is(':checked')){ field = 1; }
    if(id != '')
    {
         
        $.ajax
        ({
            method: "POST",
            url: "recruitment/"+control+"/"+properties,
            data: { id:id, field:field, properties:properties},
            dataType: "json",
            success: function(data){
                if(data.result){
                    $.toast({
                        heading: 'Thông báo!',
                        text: 'Cập nhật trạng thái hiển thị thành công!.',
                        position: 'top-right',
                        loaderBg: '#5ba035',
                        icon: 'success',
                        hideAfter: 2000,
                        // stack: 1
                    });
                }
                 
            }
        });
        
    }
}