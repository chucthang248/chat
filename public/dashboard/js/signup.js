$(document).ready(function() {
    $('.select2').select2({
        templateResult: format,
        templateSelection:format,
    });
});
function format (state) {
    if (!state.id) { return state.text; }
    if ($(state.element).data('image')) {
        var $state = $(
            '<span class="theitem"><img style="display: inline-block;height:17px;" src="'+$(state.element).data('image')+'" /> ' + state.text +'</span>'
        );
    }  else if ($(state.element).data('flagicon')) {
        var $state = $(
            '<span class="theitem"> <span class="flag-icon flag-icon-'+ $(state.element).data('flagicon') +'"></span> ' + state.text +'</span>'
        );
    } else {
        var $state = $(
            '<span class="theitem">' + state.text +'</span>'
        );
    }
    return $state;
}
function checkUnikey(str){
    str = str.value;
    str = change_alias(str);
    $('#fullname').val(str);
}
function change_alias(alias) {
    var str = alias;
    str = str.toLowerCase();
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g,"a"); 
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g,"e"); 
    str = str.replace(/ì|í|ị|ỉ|ĩ/g,"i"); 
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g,"o"); 
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g,"u"); 
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g,"y"); 
    str = str.replace(/đ/g,"d");
    str = str.replace(/!|@|%|\^|\*|\(|\)|\+|\=|\<|\>|\?|\/|,|\.|\:|\;|\'|\"|\&|\#|\[|\]|~|\$|_|`|-|{|}|\||\\/g," ");
    str = str.replace(/ + /g," ");
    // str = str.trim(); 
    return str;
}