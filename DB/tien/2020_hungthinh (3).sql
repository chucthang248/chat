-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 18, 2021 lúc 12:55 PM
-- Phiên bản máy phục vụ: 10.1.32-MariaDB
-- Phiên bản PHP: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `2020_hungthinh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_apply`
--

CREATE TABLE `tbl_apply` (
  `id` int(10) UNSIGNED NOT NULL,
  `base_url` text COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `positionID` int(11) NOT NULL,
  `recruitmentID` int(11) NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `marital` tinyint(1) NOT NULL,
  `birthday` date NOT NULL,
  `identify_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `province_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `province_city_temporary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `district_temporary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wards_temporary` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apartment_number_temporary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `temporary_address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `standardID` int(11) NOT NULL,
  `introduce` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_cv` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_apply`
--

INSERT INTO `tbl_apply` (`id`, `base_url`, `fullname`, `positionID`, `recruitmentID`, `sex`, `marital`, `birthday`, `identify_number`, `province_city`, `province_city_temporary`, `district`, `district_temporary`, `wards_temporary`, `apartment_number_temporary`, `permanent_address`, `temporary_address`, `phone`, `email`, `standardID`, `introduce`, `avatar`, `cv`, `type_cv`, `view`, `created_at`, `updated_at`) VALUES
(1, 'dfdfgdfg', 'Minh Tiến', 1, 1, 1, 1, '2021-03-24', '324234234', '42', '25', '23', '12', '12', '12', '16/10, Linh chiểu', '16/10, Linh chiểu', '0349190665', 'minhtienn2408@gmail.com', 9, 'CDCNTD', '24.jpg', 'cv', 'cv', 0, '2020-09-30', '0000-00-00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_apply_workexperience`
--

CREATE TABLE `tbl_apply_workexperience` (
  `id` int(10) UNSIGNED NOT NULL,
  `applyID` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_awardproposal`
--

CREATE TABLE `tbl_awardproposal` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_proposal` int(11) NOT NULL,
  `prizesID` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `status` int(5) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_awardproposal`
--

INSERT INTO `tbl_awardproposal` (`id`, `user_proposal`, `prizesID`, `employeeID`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(2, 1, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(3, 12, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(4, 13, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(5, 13, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(6, 14, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(7, 14, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(8, 15, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(9, 16, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(10, 17, 2, 23, 1, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(11, 17, 2, 23, 3, '2021-05-17 14:25:59', '0000-00-00 00:00:00'),
(12, 18, 2, 23, 3, '2021-05-17 14:25:59', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_banks`
--

CREATE TABLE `tbl_banks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_banks`
--

INSERT INTO `tbl_banks` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'VietinBank', 1, '2021-02-02 14:47:10', '2021-02-27 14:49:25'),
(2, 'Vietcombank', 1, '2021-02-02 14:47:20', '2021-02-27 14:49:07'),
(3, 'Agribank', 1, '2021-02-02 14:47:33', '2021-02-27 14:48:09'),
(4, 'BIDV', 1, '2021-02-27 14:49:51', '0000-00-00 00:00:00'),
(5, 'Techcombank', 1, '2021-02-27 14:50:02', '0000-00-00 00:00:00'),
(6, 'Sacombank', 1, '2021-02-27 14:50:11', '0000-00-00 00:00:00'),
(7, 'MB Bank', 1, '2021-02-27 14:50:19', '0000-00-00 00:00:00'),
(8, 'SHBank', 1, '2021-02-27 14:50:28', '0000-00-00 00:00:00'),
(9, 'VPBank', 1, '2021-02-27 14:50:38', '0000-00-00 00:00:00'),
(10, 'ACB', 1, '2021-02-27 14:50:44', '0000-00-00 00:00:00'),
(11, 'TP Bank', 1, '2021-02-27 14:50:51', '0000-00-00 00:00:00'),
(12, 'Đông Á Bank', 1, '2021-02-27 14:51:01', '0000-00-00 00:00:00'),
(13, 'SeABank', 1, '2021-02-27 14:51:10', '2021-02-27 14:51:24'),
(14, 'ABBank', 1, '2021-02-27 14:51:16', '0000-00-00 00:00:00'),
(15, 'BacABank', 1, '2021-02-27 14:51:30', '0000-00-00 00:00:00'),
(16, 'VietCapitalBank', 1, '2021-02-27 14:51:37', '0000-00-00 00:00:00'),
(17, 'MSB', 1, '2021-02-27 14:51:43', '0000-00-00 00:00:00'),
(18, 'KienLongBank', 1, '2021-02-27 14:51:49', '0000-00-00 00:00:00'),
(19, 'Nam Á Bank', 1, '2021-02-27 14:51:55', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_branchs`
--

CREATE TABLE `tbl_branchs` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_branchs`
--

INSERT INTO `tbl_branchs` (`id`, `name`, `phone`, `email`, `address`, `publish`, `created_at`, `updated_at`) VALUES
(6, 'HTG727', '0898999071', 'giayhungthinh727@gmail.com', '727 Kha Vạn Cân, P. Linh Tây, Thủ Đức, TP. HCM', 1, '2021-02-27 14:45:29', '0000-00-00 00:00:00'),
(7, 'HTG703', '0898999061', 'giayhungthinh703@gmail.com', '703 Kha Vạn Cân, P. Linh Tây, Thủ Đức, TP. HCM', 1, '2021-02-27 14:46:00', '0000-00-00 00:00:00'),
(8, 'HTG-HQ', '0898548980', 'info@hungthinh1989.com', '4 đường 8, P. Trường Thọ, Thủ Đức, TP. HCM', 1, '2021-02-27 14:46:46', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_bussinesstypes`
--

CREATE TABLE `tbl_bussinesstypes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_bussinesstypes`
--

INSERT INTO `tbl_bussinesstypes` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Công Ty TNHH', 1, '2021-01-29 20:37:31', '2021-02-27 09:56:15'),
(2, 'Hộ Kinh Doanh', 1, '2021-01-29 20:37:46', '2021-02-27 09:56:06'),
(3, 'Công Ty Cổ Phần', 1, '2021-01-29 20:37:58', '2021-02-27 09:56:22'),
(4, 'Công Ty FDI Toàn Phần', 1, '2021-02-27 17:58:41', '2021-02-27 17:59:28'),
(5, 'Công Ty Liên Doanh Nước Ngoài', 1, '2021-02-27 17:58:58', '0000-00-00 00:00:00'),
(6, 'Công Ty Quốc Doanh Toàn Phần', 1, '2021-02-27 17:59:16', '2021-02-27 17:59:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_calendar_interview`
--

CREATE TABLE `tbl_calendar_interview` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` text COLLATE utf8_unicode_ci NOT NULL,
  `applyID` int(11) NOT NULL,
  `recruitmentID` int(11) NOT NULL,
  `time_interview` time NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `note` text COLLATE utf8_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `className` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_calendar_interview`
--

INSERT INTO `tbl_calendar_interview` (`id`, `uid`, `applyID`, `recruitmentID`, `time_interview`, `title`, `note`, `start`, `className`, `created_at`, `updated_at`) VALUES
(31, '9a132641-1bed-438d-9cd1-a6f0e2efd90e', 9, 9, '10:00:00', '', 'Teets', '2021-04-08', '', '2021-04-10 13:36:11', '0000-00-00 00:00:00'),
(32, 'd0bd6db4-4626-48c8-8533-79ec18180030', 8, 3, '16:00:00', '', '', '2021-04-24', '', '2021-04-23 15:34:55', '0000-00-00 00:00:00'),
(33, '9a132641-1bed-438d-9cd1-a6f0e2efd90e', 10, 9, '10:00:00', '', 'Teets', '2021-04-23', '', '2021-04-10 13:36:11', '0000-00-00 00:00:00'),
(34, 'd0bd6db4-4626-48c8-8533-7WERWER22333', 12, 3, '16:00:00', '', '', '2021-04-25', '', '2021-04-23 15:34:55', '0000-00-00 00:00:00'),
(35, '984ec644-d9d0-4105-aa35-27bf3a6e1b3a', 13, 4, '10:00:00', '', '', '2021-04-27', '', '2021-04-26 18:25:47', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_careerroads`
--

CREATE TABLE `tbl_careerroads` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `job_position_titleID` int(11) NOT NULL,
  `job_positionID` int(11) NOT NULL,
  `parentID` int(11) NOT NULL,
  `publish` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_careerroads`
--

INSERT INTO `tbl_careerroads` (`id`, `name`, `job_position_titleID`, `job_positionID`, `parentID`, `publish`, `created_at`, `updated_at`) VALUES
(35, 'Ban Kinh Doanh Cửa Hàng', 0, 0, 0, 0, '2021-05-05 15:18:52', '2021-05-07 16:16:26'),
(53, 'Phó ban/Thừa ủy quyền Ban (Assistant Principal)', 53, 53, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(54, 'Trưởng Ban (Principal)', 54, 54, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(55, 'Chuyên viên sơ cấp (Junior)', 50, 50, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(56, 'Chuyên viên (Senior)', 51, 51, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(57, 'Nhân viên chính thức (Associate)', 46, 46, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(58, 'Thử việc (Entry)', 47, 47, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(59, 'Thực tập sinh/học việc (Intern)', 48, 48, 35, 0, '2021-05-07 16:16:26', '2021-05-07 16:16:26'),
(60, 'Phòng Kỹ thuật', 0, 0, 0, 0, '2021-05-10 15:07:05', '2021-05-10 15:14:21'),
(66, 'Trưởng Ban (Principal)', 54, 54, 60, 0, '2021-05-10 15:14:21', '2021-05-10 15:14:21'),
(67, 'Chuyên viên sơ cấp (Junior)', 50, 50, 60, 0, '2021-05-10 15:14:21', '2021-05-10 15:14:21'),
(68, 'Chuyên viên (Senior)', 51, 51, 60, 0, '2021-05-10 15:14:21', '2021-05-10 15:14:21'),
(69, 'Nhân viên chính thức (Associate)', 46, 46, 60, 0, '2021-05-10 15:14:21', '2021-05-10 15:14:21'),
(70, 'Thử việc (Entry)', 47, 47, 60, 0, '2021-05-10 15:14:21', '2021-05-10 15:14:21'),
(71, 'Thực tập sinh/học việc (Intern)', 48, 48, 60, 0, '2021-05-10 15:14:21', '2021-05-10 15:14:21'),
(72, 'Phòng Hành Chính Nhân Sự', 0, 0, 0, 0, '2021-05-11 10:56:52', '2021-05-11 10:56:52'),
(73, 'Chuyên viên sơ cấp (Junior)', 50, 50, 72, 0, '2021-05-11 10:56:52', '2021-05-11 10:56:52'),
(74, 'Chuyên viên (Senior)', 51, 51, 72, 0, '2021-05-11 10:56:52', '2021-05-11 10:56:52'),
(75, 'Nhân viên chính thức (Associate)', 46, 46, 72, 0, '2021-05-11 10:56:52', '2021-05-11 10:56:52'),
(76, 'Thử việc (Entry)', 47, 47, 72, 0, '2021-05-11 10:56:52', '2021-05-11 10:56:52'),
(77, 'Thực tập sinh/học việc (Intern)', 48, 48, 72, 0, '2021-05-11 10:56:52', '2021-05-11 10:56:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_certificate`
--

CREATE TABLE `tbl_certificate` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_certificate`
--

INSERT INTO `tbl_certificate` (`id`, `userID`, `name`, `des`, `publish`, `created_at`, `updated_at`) VALUES
(2, 1, 'Chứng chỉ 1', 'Nội dung', 1, '2021-05-13 16:55:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_chat_private`
--

CREATE TABLE `tbl_chat_private` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_send_id` int(11) NOT NULL,
  `user_receive_id` int(11) NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `type_send` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_chat_private`
--

INSERT INTO `tbl_chat_private` (`id`, `user_send_id`, `user_receive_id`, `content`, `type_send`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(2, 1, 2, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 1, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(4, 2, 1, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 1, 3, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(6, 1, 3, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 3, 1, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(8, 3, 1, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 3, 4, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(10, 3, 4, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(11, 1, 4, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(12, 1, 4, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(13, 1, 4, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53'),
(14, 1, 2, '', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(15, 2, 1, '', '', '2020-09-30 00:00:00', '2020-10-05 09:09:53');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_classify_employee`
--

CREATE TABLE `tbl_classify_employee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_classify_employee`
--

INSERT INTO `tbl_classify_employee` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Partime', 1, '2021-04-07 10:08:52', '0000-00-00 00:00:00'),
(2, 'Fulltime', 1, '2021-04-07 10:09:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_contracts`
--

CREATE TABLE `tbl_contracts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_contractID` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `branchID` int(11) NOT NULL,
  `job_positionID` int(11) NOT NULL,
  `time_start` date NOT NULL,
  `time_end` date NOT NULL,
  `contract_file` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_create_quiz`
--

CREATE TABLE `tbl_create_quiz` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount_questions` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_create_quiz`
--

INSERT INTO `tbl_create_quiz` (`id`, `name`, `amount_questions`, `created_at`) VALUES
(16, 'Kiến trúc & Nội thất biệt thự hiện đại Vllaa', 2, '2021-05-13 11:41:57'),
(17, 'Bài kiểm tra 1', 2, '2021-05-13 11:42:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_edu_status`
--

CREATE TABLE `tbl_edu_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_edu_status`
--

INSERT INTO `tbl_edu_status` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Đã tốt nghiệp', 1, '2021-02-19', '2021-02-27'),
(5, 'Chưa tốt nghiệp', 1, '2021-02-19', '2021-02-27'),
(6, 'Đã nghỉ học', 1, '2021-02-19', '2021-02-27');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employeelevel`
--

CREATE TABLE `tbl_employeelevel` (
  `id` int(11) UNSIGNED NOT NULL,
  `edu_statusID` int(11) UNSIGNED NOT NULL,
  `standardID` int(11) UNSIGNED NOT NULL,
  `schoolsID` int(11) UNSIGNED NOT NULL,
  `majorsID` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employeelevel`
--

INSERT INTO `tbl_employeelevel` (`id`, `edu_statusID`, `standardID`, `schoolsID`, `majorsID`, `employeeID`, `created_at`, `updated_at`) VALUES
(1, 5, 4, 55, 28, 13, '2021-02-28', '2021-05-04'),
(2, 1, 5, 20, 21, 15, '2021-02-28', '2021-04-22'),
(3, 5, 4, 63, 143, 16, '2021-02-28', '2021-04-28'),
(4, 5, 4, 0, 0, 17, '2021-02-28', '2021-04-27'),
(5, 5, 4, 55, 28, 4, '2021-02-28', '2021-04-27'),
(6, 5, 4, 55, 69, 12, '2021-02-28', '2021-04-27'),
(7, 5, 5, 49, 26, 10, '2021-02-28', '2021-04-27'),
(8, 5, 3, 55, 143, 11, '2021-02-28', '2021-04-27'),
(9, 5, 4, 56, 10, 9, '2021-02-28', '2021-04-27'),
(10, 6, 1, 0, 0, 8, '2021-02-28', '2021-05-15'),
(11, 1, 4, 69, 20, 14, '2021-02-28', '2021-05-15'),
(12, 5, 5, 4, 52, 7, '2021-02-28', '2021-04-27'),
(13, 5, 4, 56, 28, 6, '2021-02-28', '2021-04-27'),
(14, 5, 5, 20, 21, 5, '2021-02-28', '2021-04-24'),
(15, 5, 3, 55, 143, 3, '2021-02-28', '2021-04-27'),
(16, 5, 4, 55, 23, 1, '2021-02-28', '2021-04-29'),
(17, 5, 4, 55, 28, 2, '2021-02-28', '2021-04-29'),
(18, 0, 0, 0, 0, 18, '2021-03-02', '2021-04-24'),
(19, 0, 0, 0, 0, 19, '2021-03-02', '2021-04-24'),
(20, 0, 0, 0, 0, 20, '2021-04-05', '2021-04-27'),
(21, 5, 4, 62, 30, 21, '2021-04-05', '2021-05-15'),
(22, 5, 4, 55, 143, 22, '2021-04-05', '2021-05-15'),
(23, 5, 4, 90, 143, 23, '2021-04-05', '2021-05-15'),
(25, 5, 5, 28, 28, 26, '2021-04-27', '2021-05-13'),
(26, 0, 0, 0, 0, 28, '2021-05-14', '2021-05-14'),
(27, 0, 0, 0, 0, 29, '2021-05-15', '2021-05-16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees`
--

CREATE TABLE `tbl_employees` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `code_hide` int(11) NOT NULL,
  `sex` tinyint(1) NOT NULL,
  `birthday` date NOT NULL,
  `permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `temporary_address` text COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email_personal` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `identify_type` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `identify_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `identify_time` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `identify_place` text COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id-front` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id-back` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `marital` tinyint(1) NOT NULL,
  `majors` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `standardID` int(11) NOT NULL,
  `job_position_titleID` int(11) NOT NULL,
  `job_positionID` int(11) NOT NULL,
  `careerroadsID` int(11) NOT NULL,
  `ogchartID` int(11) NOT NULL,
  `schoolsID` int(11) NOT NULL,
  `edu_statusID` int(11) NOT NULL,
  `salary_current` double NOT NULL,
  `salary_desire` double NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees`
--

INSERT INTO `tbl_employees` (`id`, `fullname`, `code`, `code_hide`, `sex`, `birthday`, `permanent_address`, `temporary_address`, `phone`, `email`, `email_personal`, `identify_type`, `identify_number`, `identify_time`, `identify_place`, `avatar`, `id-front`, `id-back`, `marital`, `majors`, `standardID`, `job_position_titleID`, `job_positionID`, `careerroadsID`, `ogchartID`, `schoolsID`, `edu_statusID`, `salary_current`, `salary_desire`, `publish`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Hồ Thị Phương Nhi', '00064', 1, 2, '2000-03-12', 'Thạch Hà, Quảng Sơn, Ninh Sơn, Ninh Thuận 			', '195/11A  đường Đình Phong Phú phường Tăng Nhơn Phú B Thành phố Thủ Đức', '0923924030', 'nhiphuong120300@gmail.com', 'nhiphuong120300@gmail.com', 'cmnd', '264527018', '08/03/2018', 'CA Ninh Thuận', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 51, 49, 0, 70, 0, 0, 0, 0, 0, 1, '2021-02-27 13:56:59', '2021-04-29 14:25:20'),
(2, 'Trần Thị Thanh Xuân', '00053', 2, 2, '1997-01-13', '76/36/58,đường 19, phường Linh Chiểu, quận Thủ Đức, thành phố Hồ Chí Minh.			', '76/36/58,đường 19, phường Linh Chiểu, quận Thủ Đức, thành phố Hồ Chí Minh.			', '0342186986', 'tuyendung.giayhungthinh@gmail.com', 'thanhxuann2004@gmail.com', 'cmnd', '025812183', '07/10/2013', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 47, 45, 0, 40, 0, 0, 0, 0, 0, 1, '2021-02-27 14:18:32', '2021-04-29 14:35:41'),
(3, 'Nguyễn Thị Trúc Tiên', '00037', 3, 2, '2002-06-16', '40/11B Tỉnh lộ 43, KP1, P. Bình Chiểu, Q. Thủ Đức, TP. HCM			', '40/11B Tỉnh lộ 43, KP1, P. Bình Chiểu, Q. Thủ Đức, TP. HCM			', '077781670', 'nguyenthitructien167@gmail.com', 'nguyenthitructien167@gmail.com', 'cmnd', '079302030310', '07/10/2018', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 0, 34, 0, 0, 0, 0, 0, 0, 0, 1, '2021-02-27 14:23:05', '2021-04-27 08:39:22'),
(4, 'Vũ Thị Hà', '00018', 4, 2, '2000-08-17', 'Thôn 13 - Hòa Nam - Di Linh - Lâm Đồng			', '35/18 Đường 11, Phường Linh Chiểu, Quận Thủ Đức, TP. HCM			', '0396836805', 'vuthiha140900@gmail.com', 'vuthiha140900@gmail.com', 'cmnd', '251212637', '22/07/2016', 'CA. Lâm Đồng', 'avatar.jpg', '', '', 1, '', 0, 0, 34, 0, 0, 0, 0, 0, 0, 0, 1, '2021-02-27 14:26:10', '2021-04-27 08:39:00'),
(5, 'Lý Thị Liên', '00019', 5, 2, '1999-01-10', 'Xã Tân Bình ,Thị xã LaGi, tình Bình Thuận		', '97 Nguyễn Gia Trí phường 25 quận Bình Thạnh TPHCM', '0393157839', 'lienly839@gmail.com', '', 'cmnd', '261456115', '12/01/2013', 'CA Bình Thuận', 'avatar.jpg', '', '', 2, 'Quản trị kinh doanh', 5, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, '2021-02-27 14:36:48', '2021-04-24 09:00:57'),
(6, 'Nguyễn Thị Thanh Bình', '00075', 6, 2, '2002-09-15', 'Ấp 1 Thanh Sơn Định Quán Đồng nai', '21B đường 16 Linh Chiểu, Thành phố Thủ Đức', '0335283537', 'thanhbinh1592002@gmail.com', 'thanhbinh1592002@gmail.com', 'cmnd', '272867998', '19/05/2017', 'Đồng Nai', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 0, 34, 0, 0, 0, 0, 0, 0, 0, 1, '2021-02-27 14:44:19', '2021-04-27 08:38:43'),
(7, 'Lê Thị Ngọc Sáng', '00041', 7, 2, '2000-03-27', 'Thôn 16, Xã Hòa Ninh, Huyện Di Linh, Lâm Đồng			', '79/2 Đường 4, P. Linh Tây, Q. Thủ Đức, TP. HCM			', '0946348827', 'sangglee123@gmail.com', 'sangglee123@gmail.com', 'cmnd', '251212898', '29/01/2016', 'CA. Lâm Đồng', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 50, 49, 0, 70, 0, 0, 0, 0, 0, 1, '2021-02-27 14:50:28', '2021-04-27 08:38:21'),
(8, 'Trần Thị Minh Xuân', '00069', 8, 2, '1997-05-17', '25/3 đường 6 tăng nhơn phú B quận 9 , thành phố Hồ Chí Minh			', '49e đường 1 tăng nhơn phú b quận 9 thành phố Hồ Chí Minh			', '0703235732', 'menxinh@gmail.com', '', 'cmnd', '079197008305', '04/04/2017', 'CA quận 9', 'avatar.jpeg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, '2021-02-27 14:56:33', '2021-05-15 14:34:00'),
(9, 'Nguyễn Thị Thùy Trâm', '00076', 9, 2, '2002-06-02', 'Ấp 4 Xuân Tâm, Xuân Lộc Đồng  Nai', 'Đường 18 Phường Linh  Trung TP Thủ Đức', '0378918362', 'nguyentram000262@gmail.com', 'nguyentram000262@gmail.com', 'cmnd', '272882021', '29/04/2017', '19036755711018', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 52, 0, 0, 0, 0, 0, 1, '2021-02-27 15:09:10', '2021-04-27 08:37:59'),
(10, 'Nguyễn Thị Thanh Hà', '00071', 10, 2, '2002-12-20', 'An Bình -Dĩ An -Bình Dương			', '16/54 ấp Bình Đường 4 -An Bình- Dĩ An -Bình Dương			', '0369011435', 'thanhha20122002@gmail.com', 'thanhha20122002@gmail.com', 'cmnd', '281336653', '28/05/2002', 'CA Bình Dương', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-02-27 15:17:03', '2021-04-27 08:37:43'),
(11, 'Nguyễn Thị Bảo Châu', '00063', 11, 2, '2003-12-22', '183/32/11 Nguyễn Hữu Cảnh , Phường 22 , Bình Thạnh , TP.Hồ Chí Minh 			', '1092/11/4 tỉnh lộ 43, phường bình chiểu , quận thủ đức ,			', '0972921033', 'nguyenthibaochau2212@gmail.com', 'nguyenthibaochau2212@gmail.com', 'cmnd', '066303000122', '09/08/2018', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-02-27 15:21:51', '2021-04-27 08:37:25'),
(12, 'Nguyễn Hoàng Anh Tuấn', '00030', 12, 1, '2002-12-26', '86 Tổ 37 Đường 17 Khu Phố 3 Phường Linh Chiểu Quận Thủ Đức			', '86 Tổ 37 Đường 17 Khu Phố 3 Phường Linh Chiểu Quận Thủ Đức			', '0363377673', 'nhanhtuantkdh217@gmail.com', 'nhanhtuantkdh217@gmail.com', 'cmnd', '079202011824', '07/10/2018', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-02-27 15:25:08', '2021-04-27 08:37:07'),
(13, 'Nguyễn Ngọc Phương Thảo', '00025', 13, 2, '2000-09-12', '139/26 KP5 phường Linh Tây quận Thủ Đức', '139/26 KP5 phường Linh Tây quận Thủ Đức', '0396655714', 'Thaon@hungthinh1989.com', 'nguyenngocphuongthao113@gmail.com', 'cmnd', '082300000060', '08/08/2016', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 0, 0, 0, 22, 0, 0, 0, 0, 0, 1, '2021-02-27 15:32:32', '2021-05-04 10:46:13'),
(14, 'Huỳnh Minh Hùng', '00001', 14, 1, '1991-11-09', '986/12 Kha Vạn Cân, P. Linh Chiểu, Q. Thủ Đức, TP. HCM			', '986/12 Kha Vạn Cân, P. Linh Chiểu, Q. Thủ Đức, TP. HCM			', '0902404167', 'Hungh@hungthinh1989.com', 'henrimin.huynh@gmail.com', 'cmnd', '025721700', '28/03/2013', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', '', 1, '', 0, 54, 52, 5, 20, 0, 0, 0, 0, 0, 4, '2021-02-27 15:37:43', '2021-05-15 14:34:40'),
(15, 'Ma Thị Oanh', '00022', 15, 2, '1998-05-22', 'Thôn 9 Nam Dong Cư Jut Đăk Nông', '833/42A Kha Vạn Cân Phường Linh Tây TP Thủ Đức', '0336921010', 'mathioanh98@gmail.com', '', 'cmnd', '245303245', '04/01/2014', 'CA Đak Nông', 'avatar.jpg', 'id-front1.jpg', 'id-back1.jpg', 1, '', 0, 0, 0, 0, 21, 0, 0, 0, 0, 0, 1, '2021-02-27 15:43:23', '2021-04-22 14:19:45'),
(16, 'Nguyễn Thị Kim Thoa', '00074', 16, 2, '2002-04-13', 'Ông Non, Tân Trung, Thị xã Gò Công Tiền Giang', '42/11/7 đường 2 khu phố 6 Hiệp Bình Phước Thành Phố Thủ Đức', '0364712507', 'kimthonguyen130402@gmail.com', 'kimthonguyen130402@gmail.com', 'cmnd', '312484272', '30/05/2016', 'CA Tiền Giang', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-02-28 10:22:13', '2021-04-28 14:04:32'),
(17, 'Lưu Ngọc Quỳnh Như', '00073', 17, 2, '2002-02-23', '143 đường 9 kp3 phường trường thọ quận thủ đức			', '143 đường 9 kp3 phường trường thọ quận thủ đức			', '0584383288', 'nhuluu006@gmail.com', 'nhuluu006@gmail.com', 'cmnd', '048302000078', '04/06/2018', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-02-28 10:32:18', '2021-04-27 08:36:36'),
(18, 'Bùi Thị Yến Nhi', '00079', 18, 2, '1999-08-30', '112/5 đường số 9 Khu Phố 1 Linh Tây Thủ Đức TP Hồ Chí Minh', '112/5 đường số 9 Khu Phố 1 Linh Tây Thủ Đức TP Hồ Chí Minh', '0898725930', 'yennhii3108@gmail.com', '', 'cmnd', '079199007469', '26/03/2018', 'CA TP HCM', '', '', '', 1, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, '2021-03-02 17:16:01', '2021-04-24 11:44:48'),
(19, 'Phan Bảo Trân', '00048', 19, 2, '1999-06-03', 'Ấp 7, Tân Tây Gò Công, Đông Tiền Giang			', '29/3 Đường 16, P. Linh Chiểu, Q. Thủ Đức, TP. HCM			', '0327561681', 'tranbao3699@gmail.com', '', 'cmnd', '312381831', '22/08/2013', 'CA Tiền Giang', 'avatar.jpg', '', '', 1, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, '2021-03-02 17:20:22', '2021-04-24 09:01:13'),
(20, 'Tất Thị Hồng My', '00109', 20, 2, '2002-12-27', '6b đường 12 tổ 22 KP2 phường Linh Chiểu quận Thủ Đức,TP Hồ Chí Minh', '6b đường 12 tổ 22 KP2 phường Linh Chiểu quận Thủ Đức,TP Hồ Chí Minh', '0916484370', 'tatthihongmy@gmail.com', 'tatthihongmy@gmail.com', 'cmnd', '079302017341', '51/12/020', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-04-05 09:09:23', '2021-04-27 08:36:16'),
(21, 'Nguyễn Huỳnh Xuân Hương', '00202', 21, 2, '2001-12-26', 'số 18 đường ò Dưa KP4 Phường Tam Bình TP Thủ Đức', 'số 18 đường ò Dưa KP4 Phường Tam Bình TP Thủ Đức', '0974205928', 'nguyenhuynhxuanhuong2612@gmail.com', 'nguyenhuynhxuanhuong2612@gmail.com', 'cmnd', '212849122', '16/06/2017', 'Quảng Ngãi', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-04-05 09:30:16', '2021-05-15 10:14:26'),
(22, 'Nguyễn Thanh Thảo', '00203', 22, 2, '2002-08-20', 'số 1 đường số 9 KP1 phường Trường Thọ TP Thủ Đức', 'số 1 đường số 9 KP1 phường Trường Thọ TP Thủ Đức', '0382353112', 'nguyenthanhthao200802@gmail.com', 'nguyenthanhthao200802@gmail.com', 'cmnd', '07930202324', '04/08/2017', 'CA TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-04-05 09:39:11', '2021-05-15 10:13:18'),
(23, 'Hồ Ngọc Thảo Nguyên', '00108', 23, 2, '2002-09-02', '30 Bế văn Đàn An Bình Dĩ An Bình Dương', '30 Bế văn Đàn An Bình Dĩ An Bình Dương', '0326827716', 'thaonguyen302802@gmail.com', 'thaonguyen302802@gmail.com', 'cmnd', '272966442', '07/05/2018', 'Đồng Nai', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 46, 45, 0, 71, 0, 0, 0, 0, 0, 1, '2021-04-05 09:56:17', '2021-05-15 10:12:05'),
(26, 'Nguyễn Duy Đăng', '00204', 24, 1, '2002-12-01', '133 đường 9 khu Phố 5 Phường Linh Tây Tp Thủ Đức', '133 đường 9 khu Phố 5 Phường Linh Tây Tp Thủ Đức', '0932691808', 'nguyenduydang2323@gmail.com', 'nguyenduydang2323@gmail.com', 'cmnd', '079202019745', '07/07/2017', 'cảnh sát TP HCM', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 47, 45, 0, 71, 0, 0, 0, 0, 0, 4, '2021-04-27 14:30:23', '2021-05-13 14:42:38'),
(28, 'Trân', '000000', 25, 2, '1994-05-16', '4 đường 8 Phường trường thọ tp Thủ Đức', '4 đường 8 Phường trường thọ tp Thủ Đức', '0932013905', 'tranj.giayhungthinh@gmail.com', 'tranj.giayhungthinh@gmail.com', 'cmnd', '321654987', '07/10/2018', 'tp Hồ chí Minh', '', '', '', 1, '', 0, 51, 49, 0, 24, 0, 0, 0, 0, 0, 1, '2021-05-14 14:23:25', '0000-00-00 00:00:00'),
(29, 'Nguyễn Thị Mai Hân', '00110', 26, 2, '2002-09-04', 'Thôn Phú Long Xã Long Hải Huyện Phú Quý Tỉnh Bình Thuận', '531 Kha Vạn Cân', '0387328759', 'nguyenthimaihank18232@gmail.com', 'nguyenthimaihank18232@gmail.com', 'cmnd', '261637832', '13/06/2018', 'CA Bình Thuận', 'avatar.jpg', 'id-front.jpg', 'id-back.jpg', 1, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, '2021-05-15 11:54:47', '2021-05-16 11:18:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees_banks`
--

CREATE TABLE `tbl_employees_banks` (
  `id` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `bankID` int(11) NOT NULL,
  `bank_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_branch` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees_banks`
--

INSERT INTO `tbl_employees_banks` (`id`, `employeeID`, `bankID`, `bank_number`, `bank_name`, `bank_branch`, `is_default`, `created_at`, `updated_at`) VALUES
(123, 15, 5, '19034573561018', 'MA THI OANH', 'Thủ Đức', 1, '2021-04-22 14:19:45', '0000-00-00 00:00:00'),
(134, 5, 5, '19034560965012', ' LY THI LIEN', 'Thủ Đức', 1, '2021-04-24 09:00:57', '0000-00-00 00:00:00'),
(135, 19, 5, '19035321945014', 'PHAN BAO TRAN', 'Thủ Đức', 1, '2021-04-24 09:01:13', '0000-00-00 00:00:00'),
(139, 18, 1, '19036688595016', 'BUI THI YEN NHI', 'Thủ Đức', 1, '2021-04-24 11:44:48', '0000-00-00 00:00:00'),
(145, 20, 5, '19036895443013', 'Tất Thị Hồng My', 'Thủ Đức', 1, '2021-04-27 08:36:16', '0000-00-00 00:00:00'),
(146, 17, 5, '19036588416017', 'LUU NGOC QUYNH NHU', 'Thủ Đức', 1, '2021-04-27 08:36:36', '0000-00-00 00:00:00'),
(148, 12, 5, '19034674359015', 'NGUYEN HOANG ANH TUAN', 'Thủ Đức', 1, '2021-04-27 08:37:07', '0000-00-00 00:00:00'),
(149, 11, 5, '19036046719013', 'NGUYEN THI BAO CHAU', 'Thủ Đức', 1, '2021-04-27 08:37:25', '0000-00-00 00:00:00'),
(150, 10, 5, '19036360389011', 'NGUYEN THI THANH HA', 'Thủ Đức', 1, '2021-04-27 08:37:43', '0000-00-00 00:00:00'),
(151, 9, 5, '19036755711018', 'NGUYEN THI THUY TRÂM', 'Thủ Đức', 1, '2021-04-27 08:37:59', '0000-00-00 00:00:00'),
(152, 7, 5, '19035140099014', 'LE THI NGOC SANG', 'Thủ Đức', 1, '2021-04-27 08:38:21', '0000-00-00 00:00:00'),
(153, 6, 5, '19036693351011', 'NGUYEN THI THANH BINH', 'Thủ Đức', 1, '2021-04-27 08:38:43', '0000-00-00 00:00:00'),
(154, 4, 5, '19034599008015', 'Vu Thi Ha', 'Thủ Đức', 1, '2021-04-27 08:39:00', '0000-00-00 00:00:00'),
(155, 3, 5, '19034939091011', 'NGUYEN THI TRUC TIEN', 'Thủ Đức', 1, '2021-04-27 08:39:22', '0000-00-00 00:00:00'),
(162, 16, 5, '19036599474019', 'NGUYEN THI KIM THOA', 'Thủ Đức', 1, '2021-04-28 14:04:32', '0000-00-00 00:00:00'),
(166, 1, 5, '19036052010018', 'Ho Thi Phuong Nhi', 'Thủ Đức', 1, '2021-04-29 14:25:20', '0000-00-00 00:00:00'),
(167, 2, 5, '19035489769017', 'TRAN THI THANH XUAN', 'Thủ Đức', 1, '2021-04-29 14:35:41', '0000-00-00 00:00:00'),
(172, 13, 5, '19034591411015', 'NGUYEN NGOC PHUONG THAO', 'Thủ Đức', 1, '2021-05-04 10:46:13', '0000-00-00 00:00:00'),
(176, 28, 0, '', '', '', 1, '2021-05-14 14:23:25', '0000-00-00 00:00:00'),
(177, 23, 5, '19036991245011', 'HO NGOC THAO NGUYEN', 'Thủ Đức', 1, '2021-05-15 10:12:05', '0000-00-00 00:00:00'),
(178, 22, 5, '19036985322010', 'NGUYEN THANH THAO', 'Thủ Đức', 1, '2021-05-15 10:13:18', '0000-00-00 00:00:00'),
(179, 21, 5, '19036980314011', 'Nguyen Huynh Xuan Huong', 'Thủ Đức', 1, '2021-05-15 10:14:26', '0000-00-00 00:00:00'),
(181, 8, 5, '19036232202012', 'TRAN  THI MINH XUAN', 'Thủ Đức', 1, '2021-05-15 14:34:00', '0000-00-00 00:00:00'),
(182, 14, 5, '19033026268016', 'HUYNH MINH HUNG', 'Thủ Đức 1', 1, '2021-05-15 14:34:40', '0000-00-00 00:00:00'),
(183, 29, 0, '', '', '', 1, '2021-05-16 11:18:23', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees_historywork`
--

CREATE TABLE `tbl_employees_historywork` (
  `id` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `careerroadsID` int(11) NOT NULL,
  `job_position_titleID` int(11) NOT NULL,
  `job_positionID` int(11) NOT NULL,
  `branchID` int(11) NOT NULL,
  `positionID` int(11) NOT NULL,
  `ogchartID` int(11) NOT NULL,
  `classify_employeeID` int(11) NOT NULL,
  `startwork` date NOT NULL,
  `endwork` date NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees_historywork`
--

INSERT INTO `tbl_employees_historywork` (`id`, `employeeID`, `careerroadsID`, `job_position_titleID`, `job_positionID`, `branchID`, `positionID`, `ogchartID`, `classify_employeeID`, `startwork`, `endwork`, `created_at`, `updated_at`) VALUES
(1, 3, 0, 0, 34, 8, 0, 0, 0, '2019-09-02', '0000-00-00', '2021-02-27 18:00:34', '2021-04-26 16:41:08'),
(2, 15, 0, 0, 0, 8, 12, 21, 2, '2019-06-29', '0000-00-00', '2021-02-28 09:55:30', '2021-04-24 08:11:20'),
(3, 13, 0, 0, 0, 8, 12, 22, 0, '2019-07-05', '0000-00-00', '2021-02-28 09:56:29', '2021-04-24 08:59:23'),
(4, 5, 0, 0, 0, 8, 0, 0, 0, '2019-06-11', '0000-00-00', '2021-02-28 09:57:12', '2021-02-28 09:57:12'),
(5, 12, 0, 46, 45, 8, 0, 71, 1, '2019-06-13', '0000-00-00', '2021-02-28 09:57:31', '2021-05-11 10:50:17'),
(6, 4, 0, 0, 34, 8, 0, 0, 0, '2019-06-05', '0000-00-00', '2021-02-28 09:57:47', '2021-04-26 16:40:48'),
(7, 7, 0, 50, 49, 7, 0, 70, 1, '2019-10-04', '0000-00-00', '2021-02-28 09:58:02', '2021-05-11 10:52:03'),
(8, 11, 0, 46, 45, 7, 0, 71, 1, '2020-06-20', '0000-00-00', '2021-02-28 09:58:28', '2021-05-11 10:50:40'),
(9, 10, 0, 46, 45, 6, 0, 71, 1, '2020-08-28', '0000-00-00', '2021-02-28 09:58:50', '2021-05-11 10:51:03'),
(10, 9, 0, 46, 45, 6, 0, 52, 1, '2020-11-30', '0000-00-00', '2021-02-28 09:59:15', '2021-05-11 10:51:31'),
(11, 6, 0, 0, 34, 8, 0, 0, 0, '2020-11-25', '0000-00-00', '2021-02-28 09:59:30', '2021-04-26 16:40:29'),
(12, 1, 0, 51, 49, 6, 0, 70, 1, '2020-06-17', '0000-00-00', '2021-02-28 10:00:10', '2021-05-11 10:53:54'),
(13, 8, 0, 0, 0, 7, 0, 0, 0, '2020-08-14', '0000-00-00', '2021-02-28 10:00:23', '2021-02-28 10:00:23'),
(14, 2, 0, 47, 45, 7, 0, 40, 1, '2020-01-13', '0000-00-00', '2021-02-28 10:00:37', '2021-05-11 10:53:21'),
(15, 14, 5, 54, 52, 8, 12, 20, 0, '2019-04-30', '0000-00-00', '2021-03-05 17:23:21', '2021-05-14 11:26:32'),
(16, 23, 0, 46, 45, 7, 0, 71, 1, '2021-03-12', '0000-00-00', '2021-04-24 11:28:21', '2021-05-11 10:46:08'),
(17, 22, 0, 46, 45, 6, 0, 71, 1, '2021-03-18', '0000-00-00', '2021-04-24 11:28:44', '2021-05-11 10:45:01'),
(18, 21, 0, 46, 45, 6, 0, 71, 1, '2021-03-03', '0000-00-00', '2021-04-24 11:29:13', '2021-05-11 10:47:26'),
(19, 20, 0, 46, 45, 7, 0, 71, 1, '2021-01-21', '0000-00-00', '2021-04-24 11:38:59', '2021-05-11 10:47:51'),
(20, 17, 0, 46, 45, 6, 0, 71, 1, '2020-10-29', '0000-00-00', '2021-04-24 11:41:03', '2021-05-11 10:48:15'),
(21, 16, 0, 46, 45, 7, 0, 71, 1, '2020-11-07', '0000-00-00', '2021-04-24 11:42:35', '2021-05-11 10:48:34'),
(22, 18, 0, 0, 0, 7, 0, 0, 0, '2020-12-09', '0000-00-00', '2021-04-24 11:44:15', '2021-04-30 00:34:17'),
(23, 19, 0, 0, 0, 6, 0, 0, 0, '2019-11-25', '0000-00-00', '2021-04-24 11:46:04', '2021-04-30 00:34:04'),
(24, 26, 0, 47, 45, 6, 0, 71, 1, '2021-04-09', '0000-00-00', '2021-05-03 10:51:46', '2021-05-11 10:45:20'),
(25, 28, 0, 51, 49, 0, 0, 24, 0, '2021-05-14', '0000-00-00', '2021-05-14 14:26:36', '2021-05-14 14:26:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees_relatives`
--

CREATE TABLE `tbl_employees_relatives` (
  `id` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `relationship` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees_relatives`
--

INSERT INTO `tbl_employees_relatives` (`id`, `employeeID`, `fullname`, `phone`, `address`, `relationship`, `created_at`, `updated_at`) VALUES
(6, 15, 'Nông Thị Huyền', '0362343341', '', 'Mẹ', '2021-04-22 14:19:45', '0000-00-00 00:00:00'),
(8, 21, 'Lương thị mỹ hương', '0165849684', 'Thủ đức, Linh chiểu', 'mẹ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(9, 21, 'Lê Văn Toàn', '0165849684', 'Thủ đức, Linh chiểu', 'Anh', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees_salary`
--

CREATE TABLE `tbl_employees_salary` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeID` int(11) NOT NULL,
  `salary` double NOT NULL,
  `salary_basic` double NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees_salary`
--

INSERT INTO `tbl_employees_salary` (`id`, `employeeID`, `salary`, `salary_basic`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, '2021-02-27', '2021-04-29'),
(2, 2, 0, 0, '2021-02-27', '2021-04-29'),
(3, 3, 0, 0, '2021-02-27', '2021-04-27'),
(4, 4, 0, 0, '2021-02-27', '2021-04-27'),
(5, 5, 0, 0, '2021-02-27', '2021-04-24'),
(6, 6, 0, 0, '2021-02-27', '2021-04-27'),
(7, 7, 0, 0, '2021-02-27', '2021-04-27'),
(8, 8, 0, 0, '2021-02-27', '2021-05-15'),
(9, 9, 0, 0, '2021-02-27', '2021-04-27'),
(10, 10, 0, 0, '2021-02-27', '2021-04-27'),
(11, 11, 0, 0, '2021-02-27', '2021-04-27'),
(12, 12, 0, 0, '2021-02-27', '2021-04-27'),
(13, 13, 0, 0, '2021-02-27', '2021-05-04'),
(14, 14, 0, 0, '2021-02-27', '2021-05-15'),
(15, 15, 0, 0, '2021-02-27', '2021-04-22'),
(16, 16, 0, 0, '2021-02-28', '2021-04-28'),
(17, 17, 0, 0, '2021-02-28', '2021-04-27'),
(18, 18, 0, 0, '2021-03-02', '2021-04-24'),
(19, 19, 0, 0, '2021-03-02', '2021-04-24'),
(20, 20, 0, 0, '2021-04-05', '2021-04-27'),
(21, 21, 0, 0, '2021-04-05', '2021-05-15'),
(22, 22, 0, 0, '2021-04-05', '2021-05-15'),
(23, 23, 0, 0, '2021-04-05', '2021-05-15'),
(25, 26, 0, 0, '2021-04-27', '2021-05-13'),
(26, 28, 0, 0, '2021-05-14', '2021-05-14'),
(27, 29, 0, 0, '2021-05-15', '2021-05-16');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees_standards`
--

CREATE TABLE `tbl_employees_standards` (
  `id` int(11) NOT NULL,
  `employeeID` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_employees_works`
--

CREATE TABLE `tbl_employees_works` (
  `id` int(10) UNSIGNED NOT NULL,
  `employeeID` int(11) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `work_unit` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bussinesstypeID` int(11) NOT NULL,
  `branchID` int(11) NOT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `concurrently` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_employees_works`
--

INSERT INTO `tbl_employees_works` (`id`, `employeeID`, `date_from`, `date_to`, `work_unit`, `bussinesstypeID`, `branchID`, `position`, `concurrently`, `description`, `created_at`, `updated_at`) VALUES
(3, 7, '0000-00-00', '0001-11-30', '', 0, 0, '', '', '', '2021-04-27 08:38:21', '2021-04-27 08:38:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_files`
--

CREATE TABLE `tbl_files` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typefileID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(3) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_files`
--

INSERT INTO `tbl_files` (`id`, `code`, `typefileID`, `name`, `content`, `publish`, `created_at`, `updated_at`) VALUES
(6, '002', 0, 'Hợp đồng lao động nhân thời vụ', '<p><strong>CỘNG H&Ograve;A X&Atilde; HỘI CHỦ NGHĨA VIỆT NAM</strong><br />\r\nĐộc lập &ndash; Tự do &ndash; Hạnh ph&uacute;c</p>\r\n\r\n<p>{DAY} ng&agrave;y....th&aacute;ng...năm{YEAR}</p>\r\n\r\n<p><a href=\"https://luatminhkhue.vn/hop-dong-lao-dong--ban-thoi-gian.aspx\"><strong>HỢP ĐỒNG LAO ĐỘNG</strong></a></p>\r\n\r\n<p><br />\r\n<br />\r\nCh&uacute;ng t&ocirc;i, gồm hai b&ecirc;n b&ecirc;n l&agrave; : &Ocirc;ng/B&agrave; :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>\r\n\r\n<p>Chức vụ:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;........</p>\r\n\r\n<p>Đại diện cho &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;............</p>\r\n\r\n<p>Địa chỉ:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...........................................</p>\r\n\r\n<p>Điện thoại:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.................................................................................</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>V&agrave; một b&ecirc;n l&agrave; : &Ocirc;ng/B&agrave; :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;</p>\r\n\r\n<p>Sinh ng&agrave;y:...th&aacute;ng&hellip;năm&hellip;&hellip;.&hellip;&hellip;Tại:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..............................</p>\r\n\r\n<p>Nghề nghiệp :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.....</p>\r\n\r\n<p>Địa chỉ thường tr&uacute;:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;....</p>\r\n\r\n<p>Số CMTND:&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; cấp ng&agrave;y&hellip;../&hellip;./&hellip;&hellip;................................</p>\r\n\r\n<p>Số sổ lao động (nếu c&oacute;) :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;.......cấp ng&agrave;y...../...../...................</p>\r\n\r\n<p>Thỏa thuận&nbsp;<a href=\"https://luatminhkhue.vn/tu-van-ve-muc-luong-khi-ky-ket-hop-dong-lao-dong-.aspx\">k&yacute; kết hợp đồng lao động</a>&nbsp;v&agrave; cam kết l&agrave;m đ&uacute;ng những điều khoản sau đ&acirc;y :</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều 1 : Thời hạn v&agrave; c&ocirc;ng việc hợp đồng</strong></p>\r\n\r\n<p>- &Ocirc;ng, b&agrave; :...................................l&agrave;m việc theo loại hợp đồng lao động&hellip;&hellip;&hellip;&hellip;..ừ ng&agrave;y...th&aacute;ng...năm....đến ng&agrave;y...th&aacute;ng&hellip;năm&hellip;.......</p>\r\n\r\n<p>- Thử việc từ ng&agrave;y&hellip;.th&aacute;ng&hellip;năm&hellip;&hellip;..đến ng&agrave;y&hellip;.&hellip;th&aacute;ng&hellip;&hellip;năm&hellip;&hellip;.....</p>\r\n\r\n<p>- Địa điểm l&agrave;m việc :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.&hellip;.&hellip;........</p>\r\n\r\n<p>- Chức vụ :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.&hellip;.&hellip;........</p>\r\n\r\n<p>- C&ocirc;ng việc phải l&agrave;m :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..&hellip;..........</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều 2 : Chế độ l&agrave;m việc</strong></p>\r\n\r\n<p>- Thời giờ l&agrave;m việc :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;..................</p>\r\n\r\n<p>&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.................</p>\r\n\r\n<p>- Điều kiện an to&agrave;n v&agrave; vệ sinh lao động tại nơi l&agrave;m việc theo quy định hiện h&agrave;nh của nh&agrave; nước.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Đỉều 3 : Nghĩa vụ, quyền hạn v&agrave; c&aacute;c quyền lợi người lao động được hưởng như sau :</strong></p>\r\n\r\n<p><em><strong>3.1.Nghĩa vụ :</strong></em></p>\r\n\r\n<p>Trong c&ocirc;ng việc, chịu sự điều h&agrave;nh trực tiếp của &ocirc;ng, b&agrave; :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;........</p>\r\n\r\n<p>Ho&agrave;n th&agrave;nh những c&ocirc;ng việc trong hợp đồng lao động.</p>\r\n\r\n<p>Chấp h&agrave;nh nghi&ecirc;m t&uacute;c nội quy, quy chế của đơn vị, kỷ luật lao động, an to&agrave;n lao động v&agrave; c&aacute;c quy định trong thỏa ước lao động tập thể.</p>\r\n\r\n<p><em><strong>3.2.Quyền hạn :</strong></em></p>\r\n\r\n<p>C&oacute; quyền đề xuất, khiếu nại, thay đổi, tạm ho&atilde;n, chấm dứt hợp đồng lao động theo quy định của ph&aacute;p luật lao động hiện h&agrave;nh.</p>\r\n\r\n<p><em><strong>3.3. Quyền lợi :</strong></em></p>\r\n\r\n<p>- Phương tiện đi lại l&agrave;m việc :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;.............</p>\r\n\r\n<p>- Mức lương ch&iacute;nh hoặc tiền c&ocirc;ng :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. Được trả&hellip;&hellip;&hellip;.lần v&agrave;o c&aacute;c ng&agrave;y&hellip;&hellip;&hellip;..v&agrave; ng&agrave;y&hellip;&hellip;&hellip;&hellip;..h&agrave;ng th&aacute;ng.</p>\r\n\r\n<p>- Phụ cấp gồm : .......................................................................................................</p>\r\n\r\n<p>- Được trang bị bảo hộ lao động gồm :....................................................................</p>\r\n\r\n<p>- Số ng&agrave;y nghỉ h&agrave;ng năm được hưởng lương (nghỉ lễ, ph&eacute;p, việc ri&ecirc;ng) :</p>\r\n\r\n<p>.................................................................................................................................</p>\r\n\r\n<p>- Bảo hiểm x&atilde; hội:</p>\r\n\r\n<p>..................................................................................................................................</p>\r\n\r\n<p>- Được hưởng c&aacute;c ph&uacute;c lợi :</p>\r\n\r\n<p>..................................................................................................................................</p>\r\n\r\n<p>- Được hưởng c&aacute;c khoản thưởng, n&acirc;ng lương, bồi dưỡng nghiệp vụ, thực hiện nhiệm vụ hợp t&aacute;c khoa học, c&ocirc;ng nghệ với c&aacute;c đơn vị trong hoặc ngo&agrave;i nước :</p>\r\n\r\n<p>...............................................................................................................................</p>\r\n\r\n<p>- Được hưởng c&aacute;c chế độ ngừng việc trợ cấp th&ocirc;i việc, bồi thường theo quy định của ph&aacute;p luật lao động</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều 4 : Nghĩa vụ v&agrave; quyền hạn của người sử dụng lao động :</strong></p>\r\n\r\n<p><strong><em>4.1.Nghĩa vụ :</em></strong></p>\r\n\r\n<p>Thực hiện đầy đủ những điều kiện cần thiết đ&atilde; cam kết trong hợp đồng lao động để người lao động l&agrave;m việc đạt hiệu quả. Đảm bảo việc l&agrave;m cho người lao động theo hợp đồng đ&atilde; k&yacute;. Thanh to&aacute;n đầy đủ, dứt điểm c&aacute;c chế độ v&agrave; quyền lợi của người lao động đ&atilde; cam kết trong hợp đồng lao động.</p>\r\n\r\n<p><em><strong>4.2.Quyền hạn :</strong></em></p>\r\n\r\n<p>C&oacute; quyền điều chuyển tạm thời người lao động, tạm ngừng việc, thay đổi, tạm ho&atilde;n, chấm dứt hợp đồng lao động v&agrave; &aacute;p dụng c&aacute;c biện ph&aacute;p kỷ luật theo quy định của ph&aacute;p luật lao động.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều 5 : Điều khoản chung :</strong></p>\r\n\r\n<p>5.1.Những thỏa thuận kh&aacute;c :</p>\r\n\r\n<p>.....................................................................................................................................</p>\r\n\r\n<p>5.2.Hợp đồng lao động c&oacute; hiệu lực từ ng&agrave;y...th&aacute;ng...năm... đến ng&agrave;y&hellip;.th&aacute;ng&hellip;.năm&hellip;..........</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Điều 6 : Hợp đồng lao động n&agrave;y l&agrave;m th&agrave;nh 02 bản :</strong></p>\r\n\r\n<p>- 01 bản do người lao động giữ.</p>\r\n\r\n<p>- 01 bản do người sử dụng lao động giữ.</p>\r\n\r\n<p>L&agrave;m tại :&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip;...............................................................<br />\r\n<br />\r\n<strong>Người lao động</strong>&nbsp;<strong>Người sử dụng lao động</strong><br />\r\n(K&yacute; t&ecirc;n) ( K&yacute; t&ecirc;n v&agrave; đ&oacute;ng dấu)</p>\r\n', 1, '2021-02-01 13:49:56', '2021-02-06 14:23:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_formofworks`
--

CREATE TABLE `tbl_formofworks` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_formofworks`
--

INSERT INTO `tbl_formofworks` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Hành chính', 1, '2021-01-29 20:33:32', '0000-00-00 00:00:00'),
(2, 'Toàn thời gian', 1, '2021-01-29 20:33:36', '2021-04-11 10:49:43'),
(4, 'Bán thời gian', 1, '2021-01-29 20:33:52', '2021-04-11 10:49:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_job_positions`
--

CREATE TABLE `tbl_job_positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parentID` int(11) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_job_positions`
--

INSERT INTO `tbl_job_positions` (`id`, `name`, `parentID`, `publish`, `created_at`, `updated_at`) VALUES
(11, 'Kế toán viên', 8, 0, '2021-04-22 13:48:20', '0000-00-00 00:00:00'),
(12, 'Kế toán trưởng', 8, 0, '2021-04-22 13:48:31', '0000-00-00 00:00:00'),
(14, 'Trưởng ban công nghệ thông tin', 13, 0, '2021-04-23 15:13:42', '0000-00-00 00:00:00'),
(16, 'Phó ban Công Nghệ Thông Tin', 13, 0, '2021-04-23 15:17:09', '0000-00-00 00:00:00'),
(18, 'Trưởng phòng Kỹ Thuật & Pháp Chế', 17, 0, '2021-04-23 15:20:28', '0000-00-00 00:00:00'),
(19, 'Phó Phòng Kỹ Thuật & Pháp Chế', 17, 0, '2021-04-23 15:21:24', '0000-00-00 00:00:00'),
(21, 'Giám Đốc Khối Chia Sẻ Thông Tin', 20, 0, '2021-04-23 15:22:47', '0000-00-00 00:00:00'),
(23, 'Chuyên Viên Công Nghệ Thông Tin', 13, 0, '2021-04-23 15:23:42', '0000-00-00 00:00:00'),
(24, 'Chuyên viên Công Nghệ Thông Tin Sơ cấp', 13, 0, '2021-04-23 15:24:25', '0000-00-00 00:00:00'),
(25, 'Nhân viên Công Nghệ Thông Tin', 13, 0, '2021-04-23 15:24:50', '0000-00-00 00:00:00'),
(26, 'Thử việc Công Nghệ Thông Tin', 13, 0, '2021-04-23 15:25:20', '0000-00-00 00:00:00'),
(27, 'Thực tập sinh Công Nghệ Thông Tin', 13, 0, '2021-04-23 15:25:52', '0000-00-00 00:00:00'),
(31, 'Chuyên Viên Ban Hành Chính', 30, 0, '2021-04-26 16:04:44', '0000-00-00 00:00:00'),
(32, 'Chuyên Viên Ban Nhân Sự', 29, 0, '2021-04-26 16:05:08', '0000-00-00 00:00:00'),
(34, 'BP. Nhân Viên Kinh Doanh', 33, 0, '2021-04-26 16:36:13', '0000-00-00 00:00:00'),
(35, 'Thử việc (Entry)', 34, 0, '2021-05-05 14:49:25', '0000-00-00 00:00:00'),
(36, 'Nhân viên chính thức (Associate)', 34, 0, '2021-05-05 14:49:37', '0000-00-00 00:00:00'),
(37, 'Thực tập sinh/học việc (Intern)', 44, 0, '2021-05-05 14:53:59', '0000-00-00 00:00:00'),
(38, 'Thử việc (Entry)', 44, 0, '2021-05-05 14:54:10', '0000-00-00 00:00:00'),
(39, 'Nhân viên chính thức (Associate)', 44, 0, '2021-05-05 14:54:30', '0000-00-00 00:00:00'),
(45, 'Nhân Viên', 0, 0, '2021-05-05 15:16:22', '0000-00-00 00:00:00'),
(46, 'Nhân viên chính thức (Associate)', 45, 0, '2021-05-05 15:16:30', '0000-00-00 00:00:00'),
(47, 'Thử việc (Entry)', 45, 0, '2021-05-05 15:16:47', '0000-00-00 00:00:00'),
(48, 'Thực tập sinh/học việc (Intern)', 45, 0, '2021-05-05 15:16:59', '0000-00-00 00:00:00'),
(49, 'Chuyên Viên', 0, 0, '2021-05-05 15:17:12', '0000-00-00 00:00:00'),
(50, 'Chuyên viên sơ cấp (Junior)', 49, 0, '2021-05-05 15:17:19', '0000-00-00 00:00:00'),
(51, 'Chuyên viên (Senior)', 49, 0, '2021-05-05 15:17:32', '0000-00-00 00:00:00'),
(52, 'Quản Lý', 0, 0, '2021-05-05 15:17:44', '0000-00-00 00:00:00'),
(53, 'Phó ban/Thừa ủy quyền Ban (Assistant Principal)', 52, 0, '2021-05-05 15:18:06', '0000-00-00 00:00:00'),
(54, 'Trưởng Ban (Principal)', 52, 0, '2021-05-05 15:18:19', '0000-00-00 00:00:00'),
(55, 'Quản lý cấp trung', 0, 0, '2021-05-11 10:57:43', '0000-00-00 00:00:00'),
(56, 'Trưởng Phòng', 55, 0, '2021-05-11 10:58:03', '0000-00-00 00:00:00'),
(57, 'Phó Phòng', 55, 0, '2021-05-11 10:58:17', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_log`
--

CREATE TABLE `tbl_log` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` text COLLATE utf8_unicode_ci NOT NULL,
  `employeeID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `capnhat_thongtin_old` text COLLATE utf8_unicode_ci NOT NULL,
  `capnhat_thongtin_new` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_log`
--

INSERT INTO `tbl_log` (`id`, `username`, `email`, `code`, `employeeID`, `name`, `des`, `capnhat_thongtin_old`, `capnhat_thongtin_new`, `created_at`) VALUES
(52, 'nhih', 'nhiphuong120300@gmail.com', '00064', 1, 'Hồ Thị Phương Nhi', 'login', '', '', '2021-05-10 15:17:27'),
(53, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-11 07:43:08'),
(54, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-11 08:22:56'),
(55, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-11 10:18:11'),
(56, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-11 10:18:17'),
(57, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-11 10:40:46'),
(58, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-12 05:48:32'),
(59, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'login', '', '', '2021-05-12 08:47:05'),
(60, '', 'nguyenthibaochau2212@gmail.com', '', 11, 'Nguyễn Thị Bảo Châu', 'login', '', '', '2021-05-12 09:28:52'),
(61, '', 'tatthihongmy@gmail.com', '', 20, 'Tất Thị Hồng My', 'login', '', '', '2021-05-12 09:35:23'),
(62, '', 'nguyenthibaochau2212@gmail.com', '', 11, 'Nguyễn Thị Bảo Châu', 'login', '', '', '2021-05-12 09:37:55'),
(63, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'login', '', '', '2021-05-12 11:14:38'),
(64, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-12 12:03:35'),
(65, '', 'thanhha20122002@gmail.com', '', 10, 'Nguyễn Thị Thanh Hà', 'login', '', '', '2021-05-12 14:49:50'),
(66, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'login', '', '', '2021-05-12 15:01:28'),
(67, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'login', '', '', '2021-05-12 15:02:11'),
(68, '', 'tuyendung.giayhungthinh@gmail.com', '', 2, 'Trần Thị Thanh Xuân', 'login', '', '', '2021-05-12 15:08:35'),
(69, '', 'nhuluu006@gmail.com', '', 17, 'Lưu Ngọc Quỳnh Như', 'login', '', '', '2021-05-12 15:42:53'),
(70, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-12 16:22:43'),
(71, '', 'nguyenthibaochau2212@gmail.com', '', 11, 'Nguyễn Thị Bảo Châu', 'login', '', '', '2021-05-13 09:54:33'),
(72, '', 'tuyendung.giayhungthinh@gmail.com', '', 2, 'Trần Thị Thanh Xuân', 'login', '', '', '2021-05-13 09:58:35'),
(73, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-13 11:18:02'),
(74, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 11:39:34'),
(75, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 11:40:12'),
(76, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 11:40:58'),
(77, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 11:41:59'),
(78, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 11:42:32'),
(79, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 11:52:09'),
(80, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'login', '', '', '2021-05-13 11:56:04'),
(81, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 12:04:17'),
(82, '', 'nguyenthibaochau2212@gmail.com', '', 11, 'Nguyễn Thị Bảo Châu', 'login', '', '', '2021-05-13 13:12:08'),
(83, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-13 13:43:58'),
(84, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-13 13:46:09'),
(85, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 13:47:18'),
(86, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 14:39:59'),
(87, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'login', '', '', '2021-05-13 14:40:52'),
(88, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'login', '', '', '2021-05-13 14:45:18'),
(89, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'Đăng nhập', '', '', '2021-05-13 15:49:05'),
(90, '', 'Hungh@hungthinh1989.com', '', 14, 'Huỳnh Minh Hùng', 'Đăng nhập', '', '', '2021-05-13 15:50:38'),
(91, '', 'minhtienn2408@gmail.com', '', 27, 'minh tien ', 'Đăng nhập', '', '', '2021-05-13 15:53:44'),
(92, '', 'minhtienn2408@gmail.com', '', 27, 'minh tien ', 'Quên mật khẩu', '', '', '2021-05-13 15:55:48'),
(93, '', 'nhxhuong@giayhungthinh.vn', '', 21, 'Nguyễn Huỳnh Xuân Hương', 'Đăng nhập', '', '', '2021-05-13 18:06:42'),
(94, '', 'nhxhuong@giayhungthinh.vn', '', 21, 'Nguyễn Huỳnh Xuân Hương', 'Đăng nhập', '', '', '2021-05-13 18:10:13'),
(95, '', 'nhxhuong@giayhungthinh.vn', '', 21, 'Nguyễn Huỳnh Xuân Hương', 'Đăng nhập', '', '', '2021-05-13 18:12:27'),
(96, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'Đăng nhập', '', '', '2021-05-13 18:19:21'),
(97, '', 'nguyenthibaochau2212@gmail.com', '', 11, 'Nguyễn Thị Bảo Châu', 'Đăng nhập', '', '', '2021-05-13 19:12:35'),
(98, '', 'hntnguyen@giayhungthinh.vn', '', 23, 'Hồ Ngọc Thảo Nguyên', 'Quên mật khẩu', '', '', '2021-05-13 19:29:07'),
(99, '', 'hntnguyen@giayhungthinh.vn', '', 23, 'Hồ Ngọc Thảo Nguyên', 'Quên mật khẩu', '', '', '2021-05-13 19:29:11'),
(100, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'Đăng nhập', '', '', '2021-05-13 19:35:27'),
(101, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-13 21:55:23'),
(102, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-13 21:56:05'),
(103, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'Đăng nhập', '', '', '2021-05-13 23:19:06'),
(104, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'Đăng nhập', '', '', '2021-05-14 04:45:04'),
(105, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-14 07:37:26'),
(106, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-14 07:37:54'),
(107, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'Đăng nhập', '', '', '2021-05-14 08:02:53'),
(108, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'Đăng nhập', '', '', '2021-05-14 08:07:11'),
(109, '', 'tuyendung.giayhungthinh@gmail.com', '', 2, 'Trần Thị Thanh Xuân', 'Đăng nhập', '', '', '2021-05-14 08:14:00'),
(110, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-14 08:34:33'),
(111, '', 'kimthonguyen130402@gmail.com', '', 16, 'Nguyễn Thị Kim Thoa', 'Đăng nhập', '', '', '2021-05-14 08:34:41'),
(112, '', 'hungthinhllc@gmail.com', '', 0, 'admin', 'Đăng nhập', '', '', '2021-05-14 08:35:34'),
(113, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'Đăng nhập', '', '', '2021-05-14 08:42:18'),
(114, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-14 08:46:28'),
(115, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'Đăng nhập', '', '', '2021-05-14 08:47:04'),
(116, '', 'nguyenthanhthao200802@gmail.com', '', 22, 'Nguyễn Thanh Thảo', 'Đăng nhập', '', '', '2021-05-14 08:48:13'),
(117, '', 'thaonguyen302802@gmail.com', '', 23, 'Hồ Ngọc Thảo Nguyên', 'Đăng nhập', '', '', '2021-05-14 08:48:15'),
(118, '', 'thaonguyen302802@gmail.com', '', 23, 'Hồ Ngọc Thảo Nguyên', 'Đổi mật khẩu', '', '', '2021-05-14 08:48:48'),
(119, '', 'nhiphuong120300@gmail.com', '', 1, 'Hồ Thị Phương Nhi', 'Đăng nhập', '', '', '2021-05-14 08:48:57'),
(120, '', 'tuyendung.giayhungthinh@gmail.com', '', 2, 'Trần Thị Thanh Xuân', 'Đăng nhập', '', '', '2021-05-14 09:38:12'),
(121, '', 'nguyenthibaochau2212@gmail.com', '', 11, 'Nguyễn Thị Bảo Châu', 'Đăng nhập', '', '', '2021-05-14 10:45:07'),
(135, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 12:01:35'),
(136, '', 'nhxhuong@giayhungthinh.vn', '', 21, '', 'Đăng nhập', '', '', '2021-05-14 13:08:37'),
(137, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-14 13:36:24'),
(138, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-14 13:58:40'),
(139, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-14 14:25:52'),
(140, '', 'hungthinhllc@gmail.com', '', 28, '', 'Cập nhật thông tin công việc nhân viên', 'null', '{\"startwork\":\"2021-05-14\",\"branchID\":\"\",\"ogchartID\":\"24\",\"job_positionID\":\"49\",\"job_position_titleID\":\"51\",\"classify_employeeID\":\"\",\"employeeID\":\"28\"}', '2021-05-14 14:26:36'),
(141, '', 'tranj.giayhungthinh@gmail.com', '', 28, '', 'Đăng nhập', '', '', '2021-05-14 14:29:18'),
(142, '', 'thanhxuann2004@gmai.com', '', 2, '', 'Đăng nhập', '', '', '2021-05-14 14:50:14'),
(143, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-14 15:17:19'),
(144, '', 'nhxhuong@giayhungthinh.vn', '', 21, '', 'Đăng nhập', '', '', '2021-05-14 15:34:58'),
(145, '', 'nhuluu006@gmail.com', '', 17, '', 'Đăng nhập', '', '', '2021-05-14 15:49:45'),
(146, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-14 17:28:26'),
(147, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 17:43:39'),
(148, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 17:47:14'),
(149, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-14 17:49:38'),
(150, '', 'tthmy@giayhungthinh.vn', '', 20, '', 'Đăng nhập', '', '', '2021-05-14 18:00:53'),
(151, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 19:14:29'),
(152, '', 'thanhxuann2004@gmai.com', '', 2, '', 'Đăng nhập', '', '', '2021-05-14 19:33:11'),
(153, '', 'sangglee123@gmail.com', '', 7, '', 'Quên mật khẩu', '', '', '2021-05-14 19:51:09'),
(154, '', 'sangglee123@gmail.com', '', 7, '', 'Đăng nhập', '', '', '2021-05-14 19:51:37'),
(155, '', 'tranj.giayhungthinh@gmail.com', '', 28, '', 'Đăng nhập', '', '', '2021-05-14 22:01:54'),
(156, '', 'tranj.giayhungthinh@gmail.com', '', 28, '', 'Đổi mật khẩu', '', '', '2021-05-14 22:02:28'),
(157, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 22:08:23'),
(158, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-14 22:33:29'),
(159, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 23:07:18'),
(160, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đổi mật khẩu', '', '', '2021-05-14 23:07:40'),
(161, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 23:29:54'),
(162, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-14 23:39:56'),
(163, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-15 08:49:24'),
(164, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-15 09:30:56'),
(165, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-15 10:06:10'),
(166, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-15 10:08:52'),
(167, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-15 10:57:15'),
(168, '', 'sangglee123@gmail.com', '', 7, '', 'Đăng nhập', '', '', '2021-05-15 10:59:20'),
(169, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-15 13:14:49'),
(170, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-15 13:44:22'),
(171, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-15 14:03:23'),
(172, '', 'sangglee123@gmail.com', '', 7, '', 'Đăng nhập', '', '', '2021-05-15 17:15:38'),
(173, '', 'tranj.giayhungthinh@gmail.com', '', 28, '', 'Đăng nhập', '', '', '2021-05-15 17:24:05'),
(174, '', 'nhxhuong@giayhungthinh.vn', '', 21, '', 'Đăng nhập', '', '', '2021-05-15 23:42:59'),
(175, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-16 08:44:26'),
(176, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-16 10:00:00'),
(177, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-16 10:01:18'),
(178, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-16 11:01:14'),
(179, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-16 17:20:50'),
(180, '', 'thaonguyen302802@gmail.com', '', 23, '', 'Đăng nhập', '', '', '2021-05-17 05:18:47'),
(181, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-17 07:58:24'),
(182, '', 'ntthao@giayhhungthinh.vn', '', 22, '', 'Đăng nhập', '', '', '2021-05-17 08:16:22'),
(183, '', 'Hungh@hungthinh1989.com', '', 14, '', 'Đăng nhập', '', '', '2021-05-17 08:24:38'),
(184, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-17 08:30:01'),
(185, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-17 08:30:25'),
(186, '', 'Hungh@hungthinh1989.com', '', 14, '', 'Đăng nhập', '', '', '2021-05-17 09:11:06'),
(187, '', 'thanhxuann2004@gmai.com', '', 2, '', 'Đăng nhập', '', '', '2021-05-17 14:16:25'),
(188, '', 'hungthinhllc@gmail.com', '', 21, '', 'Thêm người liên lạc', '[{\"id\":\"21\",\"edu_statusID\":\"5\",\"standardID\":\"4\",\"schoolsID\":\"62\",\"majorsID\":\"30\",\"employeeID\":\"21\",\"created_at\":\"2021-04-05\",\"updated_at\":\"2021-05-15\"}]', '{\"fullname\":\"L\\u01b0\\u01a1ng th\\u1ecb m\\u1ef9 h\\u01b0\\u01a1ng\",\"relationship\":\"m\\u1eb9\",\"phone\":\"0165849684\",\"address\":\"Th\\u1ee7 \\u0111\\u1ee9c, Linh chi\\u1ec3u\",\"employeeID\":\"21\"}', '2021-05-17 16:36:11'),
(189, '', 'hungthinhllc@gmail.com', '', 21, '', 'Thêm người liên lạc', '[{\"id\":\"21\",\"edu_statusID\":\"5\",\"standardID\":\"4\",\"schoolsID\":\"62\",\"majorsID\":\"30\",\"employeeID\":\"21\",\"created_at\":\"2021-04-05\",\"updated_at\":\"2021-05-15\"}]', '{\"fullname\":\"L\\u00ea V\\u0103n To\\u00e0n\",\"relationship\":\"Anh\",\"phone\":\"0165849684\",\"address\":\"Th\\u1ee7 \\u0111\\u1ee9c, Linh chi\\u1ec3u\",\"employeeID\":\"21\"}', '2021-05-17 16:36:22'),
(190, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-18 08:01:02'),
(191, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-18 09:38:43'),
(192, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-18 10:23:49'),
(193, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-18 11:17:13'),
(194, '', 'nhi67536@gmail.com', '', 1, '', 'Đăng nhập', '', '', '2021-05-18 14:07:35'),
(195, '', 'hungthinhllc@gmail.com', '', 0, '', 'Đăng nhập', '', '', '2021-05-18 14:08:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_majors`
--

CREATE TABLE `tbl_majors` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_majors`
--

INSERT INTO `tbl_majors` (`id`, `code`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(5, 'NH0001', 'Công nghệ thực phẩm', 1, '2021-02-27', '0000-00-00'),
(6, 'NH0002', 'Công nghệ chế biến sau thu hoạch', 1, '2021-02-27', '0000-00-00'),
(7, 'NH0003', 'Công nghệ chế biến thủy sản', 1, '2021-02-27', '0000-00-00'),
(8, 'NH0004', 'Kỹ thuật dệt', 1, '2021-02-27', '0000-00-00'),
(9, 'NH0005', 'Công nghệ sợi dệt', 1, '2021-02-27', '0000-00-00'),
(10, 'NH0006', 'Công nghệ da giầy', 1, '2021-02-27', '0000-00-00'),
(11, 'NH0007', 'Công nghệ chế biến lâm sản', 1, '2021-02-27', '0000-00-00'),
(12, 'NH0008', 'Kiến trúc', 1, '2021-02-27', '0000-00-00'),
(13, 'NH0009', 'Kinh tế và quản lý đô thị', 1, '2021-02-27', '0000-00-00'),
(14, 'NH0010', 'Kỹ thuật công trình biển', 1, '2021-02-27', '2021-02-27'),
(15, 'NH0011', 'Kỹ thuật xây dựng', 1, '2021-02-27', '0000-00-00'),
(16, 'NH0012', 'Kinh tế xây dựng', 1, '2021-02-27', '0000-00-00'),
(17, 'NH0013', 'Quản lý xây dựng', 1, '2021-02-27', '0000-00-00'),
(18, 'NH0014', 'Kỹ thuật xây dựng công trình giao thông', 1, '2021-02-27', '0000-00-00'),
(19, 'NH0015', 'Công nghệ kỹ thuật công trình xây dựng', 1, '2021-02-27', '0000-00-00'),
(20, 'NH0016', 'Công nghệ kỹ thuật - cơ khí', 1, '2021-02-27', '0000-00-00'),
(21, 'NH0017', 'Quản trị kinh doanh', 1, '2021-02-27', '0000-00-00'),
(22, 'NH0018', 'Quản trị dịch vụ du lịch và lữ hành', 1, '2021-02-27', '0000-00-00'),
(23, 'NH0019', 'Quản trị khách sạn', 1, '2021-02-27', '0000-00-00'),
(24, 'NH0020', 'Marketing', 1, '2021-02-27', '0000-00-00'),
(25, 'NH0021', 'Môi giới bất động sản', 1, '2021-02-27', '0000-00-00'),
(26, 'NH0022', 'Kinh doanh quốc tế', 1, '2021-02-27', '0000-00-00'),
(27, 'NH0023', 'Tài chính - Ngân Hàng', 1, '2021-02-27', '0000-00-00'),
(28, 'NH0024', 'Kế toán', 1, '2021-02-27', '0000-00-00'),
(29, 'NH0025', 'Kiểm toán', 1, '2021-02-27', '0000-00-00'),
(30, 'NH0026', 'Quản trị nhân lực', 1, '2021-02-27', '0000-00-00'),
(31, 'NH0027', 'Hệ thống thông tin quản lý', 1, '2021-02-27', '0000-00-00'),
(32, 'NH0028', 'Quản trị văn phòng', 1, '2021-02-27', '0000-00-00'),
(33, 'NH0029', 'Khoa học - máy tính', 1, '2021-02-27', '0000-00-00'),
(34, 'NH0030', 'Truyền thông đa phương tiện', 1, '2021-02-27', '0000-00-00'),
(35, 'NH0031', 'Kỹ thuật phần mềm', 1, '2021-02-27', '0000-00-00'),
(36, 'NH0032', 'Công nghệ thông tin', 1, '2021-02-27', '0000-00-00'),
(37, 'NH0033', 'Luật kinh tế', 1, '2021-02-27', '0000-00-00'),
(38, 'NH0034', 'Luật quốc tế', 1, '2021-02-27', '0000-00-00'),
(39, 'NH0035', 'Việt nam học', 1, '2021-02-27', '0000-00-00'),
(40, 'NH0036', 'Ngôn ngữ Anh – Tiếng Anh', 1, '2021-02-27', '0000-00-00'),
(41, 'NH0037', 'Ngôn ngữ Nga – Tiếng Nga', 1, '2021-02-27', '0000-00-00'),
(42, 'NH0038', 'Ngôn ngữ Pháp – Tiếng Pháp', 1, '2021-02-27', '0000-00-00'),
(43, 'NH0039', 'Ngôn ngữ Trung – Tiếng Trung', 1, '2021-02-27', '0000-00-00'),
(44, 'NH0040', 'Ngôn ngữ Đức – Tiếng Đức', 1, '2021-02-27', '0000-00-00'),
(45, 'NH0041', 'Ngôn ngữ Tây Ban Nha – Tiếng Tây Ban Nha', 1, '2021-02-27', '0000-00-00'),
(46, 'NH0042', 'Ngôn ngữ Bồ Đào Nha – Bồ Đào Nha', 1, '2021-02-27', '0000-00-00'),
(47, 'NH0043', 'Ngôn ngữ Italia – Tiếng Italia', 1, '2021-02-27', '0000-00-00'),
(48, 'NH0044', 'Ngôn ngữ Nhật – Tiếng Nhật', 1, '2021-02-27', '0000-00-00'),
(49, 'NH0045', 'Ngôn ngữ Hàn Quốc – Tiếng Hàn Quốc', 1, '2021-02-27', '0000-00-00'),
(50, 'NH0046', 'Ngôn ngữ A rập – Tiếng Ả rập', 1, '2021-02-27', '0000-00-00'),
(51, 'NH0047', 'Ngôn ngữ Quốc Tế Học', 1, '2021-02-27', '0000-00-00'),
(52, 'NH0048', 'Đông Phương Học', 1, '2021-02-27', '0000-00-00'),
(53, 'NH0049', 'Đông Nam Á học', 1, '2021-02-27', '0000-00-00'),
(54, 'NH0050', 'Trung Quốc học', 1, '2021-02-27', '0000-00-00'),
(55, 'NH0051', 'Nhật Bản học', 1, '2021-02-27', '0000-00-00'),
(56, 'NH0052', 'Hàn Quốc học', 1, '2021-02-27', '0000-00-00'),
(57, 'NH0053', 'Khu vực Thái Bình Dương học', 1, '2021-02-27', '0000-00-00'),
(58, 'NH0054', 'Triết học', 1, '2021-02-27', '0000-00-00'),
(59, 'NH0055', 'Lịch sử học', 1, '2021-02-27', '0000-00-00'),
(60, 'NH0056', 'Văn học', 1, '2021-02-27', '0000-00-00'),
(61, 'NH0057', 'Văn hóa học', 1, '2021-02-27', '0000-00-00'),
(62, 'NH0058', 'Quản lý văn hóa', 1, '2021-02-27', '0000-00-00'),
(63, 'NH0059', 'Quản lý thể dục thể thao', 1, '2021-02-27', '0000-00-00'),
(64, 'NH0060', 'Hội họa', 1, '2021-02-27', '0000-00-00'),
(65, 'NH0061', 'Đồ họa', 1, '2021-02-27', '0000-00-00'),
(66, 'NH0062', 'Điêu khắc', 1, '2021-02-27', '0000-00-00'),
(67, 'NH0063', 'Chế Tác Gốm', 1, '2021-02-27', '0000-00-00'),
(68, 'NH0064', 'Thiết kế công nghiệp', 1, '2021-02-27', '0000-00-00'),
(69, 'NH0065', 'Thiết kế đồ họa', 1, '2021-02-27', '0000-00-00'),
(70, 'NH0066', 'Thiết kế thời trang', 1, '2021-02-27', '0000-00-00'),
(71, 'NH0067', 'Thiết kế nội thất', 1, '2021-02-27', '0000-00-00'),
(72, 'NH0068', 'Kinh tế', 1, '2021-02-27', '0000-00-00'),
(73, 'NH0069', 'Kinh tế quốc tế', 1, '2021-02-27', '0000-00-00'),
(74, 'NH0070', 'Chính trị học', 1, '2021-02-27', '0000-00-00'),
(75, 'NH0071', 'Xây dựng đảng chính quyền và nhà nước', 1, '2021-02-27', '0000-00-00'),
(76, 'NH0072', 'Quản lý nhà nước', 1, '2021-02-27', '0000-00-00'),
(77, 'NH0073', 'Quan hệ quốc tế', 1, '2021-02-27', '0000-00-00'),
(78, 'NH0074', 'Xã hội học', 1, '2021-02-27', '0000-00-00'),
(79, 'NH0075', 'Nhân văn', 1, '2021-02-27', '0000-00-00'),
(80, 'NH0076', 'Tâm lý học', 1, '2021-02-27', '0000-00-00'),
(81, 'NH0077', 'Báo chí', 1, '2021-02-27', '0000-00-00'),
(82, 'NH0078', 'Công nghệ truyền thông', 1, '2021-02-27', '0000-00-00'),
(83, 'NH0079', 'Quan hệ công chúng', 1, '2021-02-27', '0000-00-00'),
(84, 'NH0080', 'Thông tin học', 1, '2021-02-27', '0000-00-00'),
(85, 'NH0081', 'Khoa học thư viện', 1, '2021-02-27', '0000-00-00'),
(86, 'NH0082', 'Lưu trữ học', 1, '2021-02-27', '0000-00-00'),
(87, 'NH0083', 'Bảo tàng học', 1, '2021-02-27', '0000-00-00'),
(88, 'NH0084', 'Xuất bản', 1, '2021-02-27', '0000-00-00'),
(89, 'NH0085', 'Kinh doanh xuất bản phẩm', 1, '2021-02-27', '0000-00-00'),
(90, 'NH0086', 'Công nghệ sinh học', 1, '2021-02-27', '0000-00-00'),
(91, 'NH0087', 'Sinh học', 1, '2021-02-27', '0000-00-00'),
(92, 'NH0088', 'Kỹ thuật sinh học', 1, '2021-02-27', '0000-00-00'),
(93, 'NH0089', 'Sinh học ứng dụng', 1, '2021-02-27', '0000-00-00'),
(94, 'NH0090', 'Thiên văn học', 1, '2021-02-27', '0000-00-00'),
(95, 'NH0091', 'Vật lý học', 1, '2021-02-27', '2021-02-27'),
(96, 'NH0092', 'Hóa học', 1, '2021-02-27', '0000-00-00'),
(97, 'NH0093', 'Khoa học vật liệu', 1, '2021-02-27', '0000-00-00'),
(98, 'NH0094', 'Địa chất học', 1, '2021-02-27', '0000-00-00'),
(99, 'NH0095', 'Địa lý tự nhiên', 1, '2021-02-27', '0000-00-00'),
(100, 'NH0096', 'Khí tượng học', 1, '2021-02-27', '0000-00-00'),
(101, 'NH0097', 'Thủy văn học', 1, '2021-02-27', '0000-00-00'),
(102, 'NH0098', 'Hải dương học', 1, '2021-02-27', '0000-00-00'),
(103, 'NH0099', 'Khoa học môi trường', 1, '2021-02-27', '0000-00-00'),
(104, 'NH0100', 'Khoa học đất', 1, '2021-02-27', '0000-00-00'),
(105, 'NH0101', 'Toán học', 1, '2021-02-27', '0000-00-00'),
(106, 'NH0102', 'Toán ứng dụng', 1, '2021-02-27', '0000-00-00'),
(107, 'NH0103', 'Quản lý giáo dục', 1, '2021-02-27', '0000-00-00'),
(108, 'NH0104', 'Giáo dục học', 1, '2021-02-27', '0000-00-00'),
(109, 'NH0105', 'Sư phạm mầm non', 1, '2021-02-27', '0000-00-00'),
(110, 'NH0106', 'Giáo dục tiểu học', 1, '2021-02-27', '0000-00-00'),
(111, 'NH0107', 'Giáo dục đặc biệt', 1, '2021-02-27', '0000-00-00'),
(112, 'NH0108', 'Giáo dục công dân', 1, '2021-02-27', '2021-02-27'),
(113, 'NH0109', 'Giáo dục chính trị', 1, '2021-02-27', '0000-00-00'),
(114, 'NH0110', 'Giáo dục thể chất', 1, '2021-02-27', '0000-00-00'),
(115, 'NH0111', 'Huấn luyện thể thao', 1, '2021-02-27', '0000-00-00'),
(116, 'NH0112', 'Giáo dục quốc phòng – an ninh', 1, '2021-02-27', '0000-00-00'),
(117, 'NH0113', 'Sư phạm toán học', 1, '2021-02-27', '0000-00-00'),
(118, 'NH0114', 'Sư phạm tin học', 1, '2021-02-27', '0000-00-00'),
(119, 'NH0115', 'Sư phạm vật lý', 1, '2021-02-27', '0000-00-00'),
(120, 'NH0116', 'Sư phạm hóa học', 1, '2021-02-27', '0000-00-00'),
(121, 'NH0117', 'Sư phạm sinh học', 1, '2021-02-27', '0000-00-00'),
(122, 'NH0118', 'Sư phạm kỹ thuật công nghiệp', 1, '2021-02-27', '0000-00-00'),
(123, 'NH0119', 'Sư phạm kỹ thuật nông nghiệp', 1, '2021-02-27', '0000-00-00'),
(124, 'NH0120', 'Sư phạm kinh tế gia đình', 1, '2021-02-27', '0000-00-00'),
(125, 'NH0121', 'Sư phạm ngữ văn', 1, '2021-02-27', '0000-00-00'),
(126, 'NH0122', 'Sư phạm lịch sử', 1, '2021-02-27', '0000-00-00'),
(127, 'NH0123', 'Sư phạm địa lý', 1, '2021-02-27', '0000-00-00'),
(128, 'NH0124', 'Sư phạm âm nhạc', 1, '2021-02-27', '0000-00-00'),
(129, 'NH0125', 'Sư phạm mỹ thuật', 1, '2021-02-27', '0000-00-00'),
(130, 'NH0126', 'Sư phạm tiếng Anh', 1, '2021-02-27', '0000-00-00'),
(131, 'NH0127', 'Sư phạm tiếng Pháp', 1, '2021-02-27', '0000-00-00'),
(132, 'NH0128', 'Khuyến nông', 1, '2021-02-27', '0000-00-00'),
(133, 'NH0129', 'Chăn nuôi', 1, '2021-02-27', '0000-00-00'),
(134, 'NH0130', 'Nông nghiệp - thú y', 1, '2021-02-27', '0000-00-00'),
(135, 'NH0131', 'Nông học', 1, '2021-02-27', '0000-00-00'),
(136, 'NH0132', 'Khoa học cây trồng', 1, '2021-02-27', '0000-00-00'),
(137, 'NH0133', 'Công nghệ rau hoa quả - cảnh quan', 1, '2021-02-27', '0000-00-00'),
(138, 'NH0134', 'Kinh doanh nông nghiệp', 1, '2021-02-27', '0000-00-00'),
(139, 'NH0135', 'Kinh tế nông nghiệp', 1, '2021-02-27', '0000-00-00'),
(140, 'NH0136', 'Phát triển nông thôn', 1, '2021-02-27', '0000-00-00'),
(141, 'NH0137', 'Logistic', 1, '2021-02-27', '0000-00-00'),
(142, 'NH0138', 'Kinh doanh thương mại', 1, '2021-02-27', '0000-00-00'),
(143, 'NH0139', 'Khác', 1, '2021-02-27', '0000-00-00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_maketest`
--

CREATE TABLE `tbl_maketest` (
  `id` int(10) UNSIGNED NOT NULL,
  `userID` int(11) NOT NULL,
  `create_quizID` int(11) NOT NULL,
  `answers_of_createquiz` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_maketest`
--

INSERT INTO `tbl_maketest` (`id`, `userID`, `create_quizID`, `answers_of_createquiz`, `created_at`) VALUES
(35, 0, 15, '[{\"questionID\":\"42\",\"answerkey\":\"A\"},{\"questionID\":\"41\",\"answerkey\":\"D\"}]', '2021-05-13 11:04:51'),
(36, 0, 16, '[{\"questionID\":\"42\",\"answerkey\":\"C\"},{\"questionID\":\"41\",\"answerkey\":\"D\"}]', '2021-05-13 11:49:13'),
(37, 0, 18, '[{\"questionID\":\"43\",\"answerkey\":\"A\"},{\"questionID\":\"42\",\"answerkey\":\"B\"},{\"questionID\":\"41\",\"answerkey\":\"C\"}]', '2021-05-15 14:07:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_milestones`
--

CREATE TABLE `tbl_milestones` (
  `id` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_milestones`
--

INSERT INTO `tbl_milestones` (`id`, `userID`, `name`, `des`, `publish`, `created_at`, `updated_at`) VALUES
(1, 1, 'Cột mốc 1 update', 'Nội dung', 1, '2021-05-13 16:59:43', '2021-05-13 17:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_modules`
--

CREATE TABLE `tbl_modules` (
  `id` int(11) UNSIGNED NOT NULL,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) CHARACTER SET latin1 NOT NULL,
  `controller` varchar(255) CHARACTER SET latin1 NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_modules`
--

INSERT INTO `tbl_modules` (`id`, `parentID`, `name`, `link`, `controller`, `publish`, `created_at`, `updated_at`) VALUES
(29, 0, 'CƠ SỞ DỮ LIỆU', '', '', 1, '2021-04-08 10:33:34', '2021-04-08 10:33:34'),
(30, 29, 'Danh sách nhân sự', 'cpanel/employee', 'employee', 1, '2021-04-08 14:38:22', '2021-04-08 14:38:22'),
(31, 29, 'Cam kết hợp đồng', 'cpanel/contracts', 'contracts', 1, '2021-04-09 10:43:04', '2021-04-09 10:43:04'),
(32, 29, 'Lộ trình sự nghiệp', 'cpanel/careerroadmap', 'careerroadmap', 1, '2021-04-29 09:41:35', '2021-04-29 09:41:35'),
(33, 29, 'Tiền lương', 'cpanel/salary', 'salary', 1, '2021-04-08 14:38:51', '2021-04-08 14:38:51'),
(34, 0, 'CƠ CẤU TỔ CHỨC', '', '', 1, '2021-04-08 10:33:12', '2021-04-08 10:33:12'),
(35, 34, 'Sơ đồ tổ chức', 'cpanel/orgcharts', 'orgcharts', 1, '2021-04-09 10:48:54', '2021-04-09 10:48:54'),
(36, 34, 'Vị trí công việc', 'cpanel/jobposition', 'jobposition', 1, '2021-04-08 14:39:15', '2021-04-08 14:39:15'),
(37, 34, 'Nhiệm vụ công việc', 'cpanel/jobduties', 'jobduties', 1, '2021-04-08 14:37:51', '2021-04-08 14:37:51'),
(38, 34, 'Tài liệu', 'cpanel/document', 'document', 1, '2021-04-08 14:38:07', '2021-04-08 14:38:07'),
(39, 0, 'NGUYÊN TẮC & CHÍNH SÁCH', '', '', 1, '2021-04-08 10:32:22', '2021-04-08 10:32:22'),
(40, 39, 'Quy định làm việc', 'cpanel/workingrules', 'workingrules', 1, '2021-04-08 14:37:01', '2021-04-08 14:37:01'),
(41, 39, 'Chính sách nhân sự', 'cpanel/employeepolicies', 'employeepolicies', 1, '2021-04-08 14:37:15', '2021-04-08 14:37:15'),
(42, 39, 'Quy định cấp bậc', 'cpanel/rulesofrank', 'rulesofrank', 1, '2021-04-08 14:37:32', '2021-04-08 14:37:32'),
(43, 0, 'TUYỂN DỤNG', '', '', 1, '2021-04-08 10:36:15', '2021-04-08 10:36:15'),
(44, 43, 'Tổng quan', 'recruitment/report', 'recruitment', 1, '2021-04-10 13:00:33', '2021-04-10 13:00:33'),
(45, 43, 'Tin tuyển dụng', 'recruitment/recruitment', 'recruitment', 1, '2021-04-08 14:36:13', '2021-04-08 14:36:13'),
(46, 43, 'Danh sách ứng tuyển', 'recruitment/applylist', 'applylist', 1, '2021-04-08 14:36:23', '2021-04-08 14:36:23'),
(47, 43, 'Lịch phỏng vấn', 'recruitment/calendarinterview', 'calendarinterview', 1, '2021-04-08 14:36:33', '2021-04-08 14:36:33'),
(48, 0, 'CHÍNH SÁCH C&B', '', '', 1, '2021-04-08 10:41:32', '2021-04-08 10:41:32'),
(49, 48, 'Lịch làm việc', 'recruitment/timework', 'timework', 1, '2021-04-08 14:34:45', '2021-04-08 14:34:45'),
(50, 48, 'Phúc lợi', 'cpanel/welfares', 'welfares', 1, '2021-04-08 14:34:55', '2021-04-08 14:34:55'),
(51, 48, 'Nghỉ phép', 'cpanel/takeleave', 'takeleave', 1, '2021-04-08 14:35:13', '2021-04-08 14:35:13'),
(52, 48, 'Bảo hiểm', 'cpanel/insurrance', 'insurrance', 1, '2021-04-08 14:35:22', '2021-04-08 14:35:22'),
(53, 48, 'Thuế', 'cpanel/tax', 'tax', 1, '2021-04-08 14:35:33', '2021-04-08 14:35:33'),
(54, 48, 'Ngày nghỉ lễ', 'cpanel/holiday', 'holiday', 1, '2021-04-08 14:35:51', '2021-04-08 14:35:51'),
(55, 0, 'CẤU HÌNH HỆ THỐNG', '', '', 1, '2021-04-08 10:51:55', '2021-04-08 10:51:55'),
(56, 55, 'Tổng quan', 'cpanel/setting/dashboard', 'setting', 1, '2021-04-08 18:14:35', '2021-04-08 18:14:35'),
(57, 55, 'Quản lý module', 'cpanel/module', 'module', 1, '2021-04-08 14:31:33', '2021-04-08 14:31:33'),
(58, 55, 'Phân quyền', 'cpanel/permission', 'permission', 1, '2021-04-08 18:17:53', '2021-04-08 18:17:53'),
(59, 55, 'Tài khoản', 'cpanel/user', 'user', 1, '2021-04-09 07:55:05', '2021-04-09 07:55:05'),
(60, 55, 'Phân loại nhân sự', 'cpanel/classifyemployee', 'classifyemployee', 1, '2021-04-08 14:32:29', '2021-04-08 14:32:29'),
(61, 55, 'Thông tin chung', 'cpanel/setting', 'setting', 1, '2021-04-09 08:19:03', '2021-04-09 08:19:03'),
(62, 55, 'Chi nhánh', 'cpanel/branch', 'branch', 1, '2021-04-08 14:32:52', '2021-04-08 14:32:52'),
(63, 55, 'Quản lý Chức vụ', 'cpanel/position', 'position', 1, '2021-04-08 14:33:01', '2021-04-08 14:33:01'),
(64, 55, 'Hình thức làm việc', 'cpanel/formofwork', 'formofwork', 1, '2021-04-08 14:33:13', '2021-04-08 14:33:13'),
(65, 55, 'Loại hình kinh doanh', 'cpanel/bussinesstype', 'bussinesstype', 1, '2021-04-08 14:33:22', '2021-04-08 14:33:22'),
(66, 55, ' Loại ngân hàng', 'cpanel/bank', 'bank', 1, '2021-04-08 14:33:32', '2021-04-08 14:33:32'),
(67, 55, 'Loại trình độ', 'cpanel/standard', 'standard', 1, '2021-04-08 14:33:41', '2021-04-08 14:33:41'),
(68, 55, 'Trường học', 'cpanel/school', 'school', 1, '2021-04-08 14:33:51', '2021-04-08 14:33:51'),
(69, 55, 'Ngành học', 'cpanel/majors', 'majors', 1, '2021-04-08 14:34:03', '2021-04-08 14:34:03'),
(70, 55, 'Tình trạng học vấn', 'cpanel/edustatus', 'edustatus', 1, '2021-04-08 14:34:12', '2021-04-08 14:34:12'),
(71, 55, 'Hồ sơ', 'cpanel/files', 'files', 1, '2021-04-08 14:34:20', '2021-04-08 14:34:20'),
(72, 55, 'Loại hồ sơ', 'cpanel/typefiles', 'typefiles', 1, '2021-04-08 14:34:30', '2021-04-08 14:34:30'),
(73, 43, 'Cấp bậc', 'recruitment/Recruitment_rank', 'Recruitment_rank', 1, '2021-04-08 16:04:30', '2021-04-08 16:04:30'),
(74, 43, 'Hình thức', 'recruitment/Recruitment_form', 'Recruitment_form', 1, '2021-04-08 16:05:11', '2021-04-08 16:05:11'),
(75, 48, 'Duyệt lịch làm việc', 'cpanel/approvaltimework', 'approvaltimework', 1, '2021-05-17 08:41:22', '2021-05-17 08:41:22'),
(76, 48, 'Tổng quan lịch làm việc', 'cpanel/overviewtimework', 'overviewtimework', 1, '2021-04-08 18:04:58', '2021-04-08 18:04:58'),
(77, 55, 'check', '', '', 1, '2021-04-08 18:15:57', '2021-04-08 18:15:57'),
(78, 43, 'Tuyển dụng ', 'home/index', 'home', 1, '2021-04-19 08:59:01', '2021-04-19 08:59:01'),
(79, 29, 'Thông tin cá nhân', 'auths/profile', 'auths', 1, '2021-04-26 17:09:03', '2021-04-26 17:09:03'),
(80, 29, 'Thông báo ứng tuyển', 'apply/alert', 'apply', 1, '2021-05-04 09:21:20', '2021-05-04 09:21:20'),
(81, 39, 'Quy định làm việc', 'workrule/index', 'workrule', 1, '2021-05-04 10:48:57', '2021-05-04 10:48:57'),
(82, 55, 'Nhật ký hoat động', 'log/index', 'log', 1, '2021-05-14 13:39:17', '2021-05-14 13:39:17'),
(83, 0, 'THÀNH TỰU VÀ CỐNG HIẾN', '', '', 1, '2021-05-17 16:09:13', '2021-05-17 16:09:13'),
(84, 83, 'Đề xuất giải thưởng', 'awardproposal/index', 'awardproposal', 1, '2021-05-17 16:10:42', '2021-05-17 16:10:42'),
(85, 43, 'Bài kiểm tra', 'test/index', 'test', 1, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(86, 43, 'Duyệt bài kiểm tra', 'approvaltest/index', 'approvaltest', 1, '2021-05-18 16:11:23', '2021-05-18 16:11:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_modules_detail`
--

CREATE TABLE `tbl_modules_detail` (
  `id` int(11) UNSIGNED NOT NULL,
  `moduleID` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `action` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_modules_detail`
--

INSERT INTO `tbl_modules_detail` (`id`, `moduleID`, `name`, `publish`, `action`, `sort`, `created_at`, `updated_at`) VALUES
(356, 57, 'Xem', 0, 'index', 1, '2021-04-08 14:31:33', '2021-04-08 14:31:33'),
(357, 57, 'Thêm', 0, 'add', 2, '2021-04-08 14:31:33', '2021-04-08 14:31:33'),
(358, 57, 'Xóa', 0, 'delete', 4, '2021-04-08 14:31:33', '2021-04-08 14:31:33'),
(359, 57, 'Sửa', 0, 'edit', 3, '2021-04-08 14:31:33', '2021-04-08 14:31:33'),
(368, 60, 'Xem', 0, 'index', 1, '2021-04-08 14:32:29', '2021-04-08 14:32:29'),
(369, 60, 'Thêm', 0, 'add', 2, '2021-04-08 14:32:29', '2021-04-08 14:32:29'),
(370, 60, 'Xóa', 0, 'delete', 4, '2021-04-08 14:32:29', '2021-04-08 14:32:29'),
(371, 60, 'Sửa', 0, 'edit', 3, '2021-04-08 14:32:29', '2021-04-08 14:32:29'),
(376, 62, 'Xem', 0, 'index', 1, '2021-04-08 14:32:52', '2021-04-08 14:32:52'),
(377, 62, 'Thêm', 0, 'add', 2, '2021-04-08 14:32:52', '2021-04-08 14:32:52'),
(378, 62, 'Xóa', 0, 'delete', 4, '2021-04-08 14:32:52', '2021-04-08 14:32:52'),
(379, 62, 'Sửa', 0, 'edit', 3, '2021-04-08 14:32:52', '2021-04-08 14:32:52'),
(380, 63, 'Xem', 0, 'index', 1, '2021-04-08 14:33:01', '2021-04-08 14:33:01'),
(381, 63, 'Thêm', 0, 'add', 2, '2021-04-08 14:33:01', '2021-04-08 14:33:01'),
(382, 63, 'Xóa', 0, 'delete', 4, '2021-04-08 14:33:01', '2021-04-08 14:33:01'),
(383, 63, 'Sửa', 0, 'edit', 3, '2021-04-08 14:33:01', '2021-04-08 14:33:01'),
(384, 64, 'Xem', 0, 'index', 1, '2021-04-08 14:33:13', '2021-04-08 14:33:13'),
(385, 64, 'Thêm', 0, 'add', 2, '2021-04-08 14:33:13', '2021-04-08 14:33:13'),
(386, 64, 'Xóa', 0, 'delete', 4, '2021-04-08 14:33:13', '2021-04-08 14:33:13'),
(387, 64, 'Sửa', 0, 'edit', 3, '2021-04-08 14:33:13', '2021-04-08 14:33:13'),
(388, 65, 'Xem', 0, 'index', 1, '2021-04-08 14:33:22', '2021-04-08 14:33:22'),
(389, 65, 'Thêm', 0, 'add', 2, '2021-04-08 14:33:22', '2021-04-08 14:33:22'),
(390, 65, 'Xóa', 0, 'delete', 4, '2021-04-08 14:33:22', '2021-04-08 14:33:22'),
(391, 65, 'Sửa', 0, 'edit', 3, '2021-04-08 14:33:22', '2021-04-08 14:33:22'),
(392, 66, 'Xem', 0, 'index', 1, '2021-04-08 14:33:32', '2021-04-08 14:33:32'),
(393, 66, 'Thêm', 0, 'add', 2, '2021-04-08 14:33:32', '2021-04-08 14:33:32'),
(394, 66, 'Xóa', 0, 'delete', 4, '2021-04-08 14:33:32', '2021-04-08 14:33:32'),
(395, 66, 'Sửa', 0, 'edit', 3, '2021-04-08 14:33:32', '2021-04-08 14:33:32'),
(396, 67, 'Xem', 0, 'index', 1, '2021-04-08 14:33:41', '2021-04-08 14:33:41'),
(397, 67, 'Thêm', 0, 'add', 2, '2021-04-08 14:33:41', '2021-04-08 14:33:41'),
(398, 67, 'Xóa', 0, 'delete', 4, '2021-04-08 14:33:41', '2021-04-08 14:33:41'),
(399, 67, 'Sửa', 0, 'edit', 3, '2021-04-08 14:33:41', '2021-04-08 14:33:41'),
(400, 68, 'Xem', 0, 'index', 1, '2021-04-08 14:33:52', '2021-04-08 14:33:52'),
(401, 68, 'Thêm', 0, 'add', 2, '2021-04-08 14:33:52', '2021-04-08 14:33:52'),
(402, 68, 'Xóa', 0, 'delete', 4, '2021-04-08 14:33:52', '2021-04-08 14:33:52'),
(403, 68, 'Sửa', 0, 'edit', 3, '2021-04-08 14:33:52', '2021-04-08 14:33:52'),
(404, 69, 'Xem', 0, 'index', 1, '2021-04-08 14:34:03', '2021-04-08 14:34:03'),
(405, 69, 'Thêm', 0, 'add', 2, '2021-04-08 14:34:03', '2021-04-08 14:34:03'),
(406, 69, 'Xóa', 0, 'delete', 4, '2021-04-08 14:34:03', '2021-04-08 14:34:03'),
(407, 69, 'Sửa', 0, 'edit', 3, '2021-04-08 14:34:03', '2021-04-08 14:34:03'),
(408, 70, 'Xem', 0, 'index', 1, '2021-04-08 14:34:12', '2021-04-08 14:34:12'),
(409, 70, 'Thêm', 0, 'add', 2, '2021-04-08 14:34:12', '2021-04-08 14:34:12'),
(410, 70, 'Xóa', 0, 'delete', 4, '2021-04-08 14:34:12', '2021-04-08 14:34:12'),
(411, 70, 'Sửa', 0, 'edit', 3, '2021-04-08 14:34:12', '2021-04-08 14:34:12'),
(412, 71, 'Xem', 0, 'index', 1, '2021-04-08 14:34:20', '2021-04-08 14:34:20'),
(413, 71, 'Thêm', 0, 'add', 2, '2021-04-08 14:34:20', '2021-04-08 14:34:20'),
(414, 71, 'Xóa', 0, 'delete', 4, '2021-04-08 14:34:20', '2021-04-08 14:34:20'),
(415, 71, 'Sửa', 0, 'edit', 3, '2021-04-08 14:34:20', '2021-04-08 14:34:20'),
(416, 72, 'Xem', 0, 'index', 1, '2021-04-08 14:34:30', '2021-04-08 14:34:30'),
(417, 72, 'Thêm', 0, 'add', 2, '2021-04-08 14:34:30', '2021-04-08 14:34:30'),
(418, 72, 'Xóa', 0, 'delete', 4, '2021-04-08 14:34:30', '2021-04-08 14:34:30'),
(419, 72, 'Sửa', 0, 'edit', 3, '2021-04-08 14:34:30', '2021-04-08 14:34:30'),
(420, 49, 'Xem', 0, 'index', 1, '2021-04-08 14:34:45', '2021-04-08 14:34:45'),
(421, 49, 'Thêm', 0, 'add', 2, '2021-04-08 14:34:45', '2021-04-08 14:34:45'),
(422, 49, 'Xóa', 0, 'delete', 4, '2021-04-08 14:34:45', '2021-04-08 14:34:45'),
(423, 49, 'Sửa', 0, 'edit', 3, '2021-04-08 14:34:45', '2021-04-08 14:34:45'),
(424, 50, 'Xem', 0, 'index', 1, '2021-04-08 14:34:55', '2021-04-08 14:34:55'),
(425, 50, 'Thêm', 0, 'add', 2, '2021-04-08 14:34:55', '2021-04-08 14:34:55'),
(426, 50, 'Xóa', 0, 'delete', 4, '2021-04-08 14:34:55', '2021-04-08 14:34:55'),
(427, 50, 'Sửa', 0, 'edit', 3, '2021-04-08 14:34:55', '2021-04-08 14:34:55'),
(428, 51, 'Xem', 0, 'index', 1, '2021-04-08 14:35:13', '2021-04-08 14:35:13'),
(429, 51, 'Thêm', 0, 'add', 2, '2021-04-08 14:35:14', '2021-04-08 14:35:14'),
(430, 51, 'Xóa', 0, 'delete', 4, '2021-04-08 14:35:14', '2021-04-08 14:35:14'),
(431, 51, 'Sửa', 0, 'edit', 3, '2021-04-08 14:35:14', '2021-04-08 14:35:14'),
(432, 52, 'Xem', 0, 'index', 1, '2021-04-08 14:35:22', '2021-04-08 14:35:22'),
(433, 52, 'Thêm', 0, 'add', 2, '2021-04-08 14:35:22', '2021-04-08 14:35:22'),
(434, 52, 'Xóa', 0, 'delete', 4, '2021-04-08 14:35:22', '2021-04-08 14:35:22'),
(435, 52, 'Sửa', 0, 'edit', 3, '2021-04-08 14:35:22', '2021-04-08 14:35:22'),
(436, 53, 'Xem', 0, 'index', 1, '2021-04-08 14:35:33', '2021-04-08 14:35:33'),
(437, 53, 'Thêm', 0, 'add', 2, '2021-04-08 14:35:33', '2021-04-08 14:35:33'),
(438, 53, 'Xóa', 0, 'delete', 4, '2021-04-08 14:35:33', '2021-04-08 14:35:33'),
(439, 53, 'Sửa', 0, 'edit', 3, '2021-04-08 14:35:33', '2021-04-08 14:35:33'),
(440, 54, 'Xem', 0, 'index', 1, '2021-04-08 14:35:51', '2021-04-08 14:35:51'),
(441, 54, 'Thêm', 0, 'add', 2, '2021-04-08 14:35:51', '2021-04-08 14:35:51'),
(442, 54, 'Xóa', 0, 'delete', 4, '2021-04-08 14:35:51', '2021-04-08 14:35:51'),
(443, 54, 'Sửa', 0, 'edit', 3, '2021-04-08 14:35:51', '2021-04-08 14:35:51'),
(448, 45, 'Xem', 0, 'index', 1, '2021-04-08 14:36:13', '2021-04-08 14:36:13'),
(449, 45, 'Thêm', 0, 'add', 2, '2021-04-08 14:36:13', '2021-04-08 14:36:13'),
(450, 45, 'Xóa', 0, 'delete', 4, '2021-04-08 14:36:13', '2021-04-08 14:36:13'),
(451, 45, 'Sửa', 0, 'edit', 3, '2021-04-08 14:36:13', '2021-04-08 14:36:13'),
(452, 46, 'Xem', 0, 'index', 1, '2021-04-08 14:36:23', '2021-04-08 14:36:23'),
(453, 46, 'Thêm', 0, 'add', 2, '2021-04-08 14:36:24', '2021-04-08 14:36:24'),
(454, 46, 'Xóa', 0, 'delete', 4, '2021-04-08 14:36:24', '2021-04-08 14:36:24'),
(455, 46, 'Sửa', 0, 'edit', 3, '2021-04-08 14:36:24', '2021-04-08 14:36:24'),
(456, 47, 'Xem', 0, 'index', 1, '2021-04-08 14:36:33', '2021-04-08 14:36:33'),
(457, 47, 'Thêm', 0, 'add', 2, '2021-04-08 14:36:33', '2021-04-08 14:36:33'),
(458, 47, 'Xóa', 0, 'delete', 4, '2021-04-08 14:36:33', '2021-04-08 14:36:33'),
(459, 47, 'Sửa', 0, 'edit', 3, '2021-04-08 14:36:33', '2021-04-08 14:36:33'),
(464, 41, 'Xem', 0, 'index', 1, '2021-04-08 14:37:15', '2021-04-08 14:37:15'),
(465, 41, 'Thêm', 0, 'add', 2, '2021-04-08 14:37:15', '2021-04-08 14:37:15'),
(466, 41, 'Xóa', 0, 'delete', 4, '2021-04-08 14:37:15', '2021-04-08 14:37:15'),
(467, 41, 'Sửa', 0, 'edit', 3, '2021-04-08 14:37:15', '2021-04-08 14:37:15'),
(468, 42, 'Xem', 0, 'index', 1, '2021-04-08 14:37:32', '2021-04-08 14:37:32'),
(469, 42, 'Thêm', 0, 'add', 2, '2021-04-08 14:37:32', '2021-04-08 14:37:32'),
(470, 42, 'Xóa', 0, 'delete', 4, '2021-04-08 14:37:32', '2021-04-08 14:37:32'),
(471, 42, 'Sửa', 0, 'edit', 3, '2021-04-08 14:37:32', '2021-04-08 14:37:32'),
(476, 37, 'Xem', 0, 'index', 1, '2021-04-08 14:37:51', '2021-04-08 14:37:51'),
(477, 37, 'Thêm', 0, 'add', 2, '2021-04-08 14:37:51', '2021-04-08 14:37:51'),
(478, 37, 'Xóa', 0, 'delete', 4, '2021-04-08 14:37:51', '2021-04-08 14:37:51'),
(479, 37, 'Sửa', 0, 'edit', 3, '2021-04-08 14:37:51', '2021-04-08 14:37:51'),
(480, 38, 'Xem', 0, 'index', 1, '2021-04-08 14:38:07', '2021-04-08 14:38:07'),
(481, 38, 'Thêm', 0, 'add', 2, '2021-04-08 14:38:07', '2021-04-08 14:38:07'),
(482, 38, 'Xóa', 0, 'delete', 4, '2021-04-08 14:38:07', '2021-04-08 14:38:07'),
(483, 38, 'Sửa', 0, 'edit', 3, '2021-04-08 14:38:07', '2021-04-08 14:38:07'),
(484, 30, 'Xem', 0, 'index', 1, '2021-04-08 14:38:22', '2021-04-08 14:38:22'),
(485, 30, 'Thêm', 0, 'add', 2, '2021-04-08 14:38:22', '2021-04-08 14:38:22'),
(486, 30, 'Xóa', 0, 'delete', 4, '2021-04-08 14:38:22', '2021-04-08 14:38:22'),
(487, 30, 'Sửa', 0, 'edit', 3, '2021-04-08 14:38:22', '2021-04-08 14:38:22'),
(497, 33, 'Xem', 0, 'index', 1, '2021-04-08 14:38:51', '2021-04-08 14:38:51'),
(498, 33, 'Thêm', 0, 'add', 2, '2021-04-08 14:38:51', '2021-04-08 14:38:51'),
(499, 33, 'Xóa', 0, 'delete', 4, '2021-04-08 14:38:51', '2021-04-08 14:38:51'),
(500, 33, 'Sửa', 0, 'edit', 3, '2021-04-08 14:38:51', '2021-04-08 14:38:51'),
(501, 36, 'Xem', 0, 'index', 1, '2021-04-08 14:39:15', '2021-04-08 14:39:15'),
(502, 36, 'Thêm', 0, 'add', 2, '2021-04-08 14:39:15', '2021-04-08 14:39:15'),
(503, 36, 'Xóa', 0, 'delete', 4, '2021-04-08 14:39:15', '2021-04-08 14:39:15'),
(504, 36, 'Sửa', 0, 'edit', 3, '2021-04-08 14:39:15', '2021-04-08 14:39:15'),
(544, 73, 'Xem', 0, 'index', 1, '2021-04-08 16:04:30', '2021-04-08 16:04:30'),
(545, 73, 'Thêm', 0, 'add', 2, '2021-04-08 16:04:30', '2021-04-08 16:04:30'),
(546, 73, 'Xóa', 0, 'delete', 4, '2021-04-08 16:04:30', '2021-04-08 16:04:30'),
(547, 73, 'Sửa', 0, 'edit', 3, '2021-04-08 16:04:30', '2021-04-08 16:04:30'),
(548, 74, 'Xem', 0, 'index', 1, '2021-04-08 16:05:11', '2021-04-08 16:05:11'),
(549, 74, 'Thêm', 0, 'add', 2, '2021-04-08 16:05:11', '2021-04-08 16:05:11'),
(550, 74, 'Xóa', 0, 'delete', 4, '2021-04-08 16:05:11', '2021-04-08 16:05:11'),
(551, 74, 'Sửa', 0, 'edit', 3, '2021-04-08 16:05:11', '2021-04-08 16:05:11'),
(556, 76, 'Xem', 0, 'index', 1, '2021-04-08 18:04:58', '2021-04-08 18:04:58'),
(557, 76, 'Thêm', 0, 'add', 2, '2021-04-08 18:04:58', '2021-04-08 18:04:58'),
(558, 76, 'Xóa', 0, 'delete', 4, '2021-04-08 18:04:58', '2021-04-08 18:04:58'),
(559, 76, 'Sửa', 0, 'edit', 3, '2021-04-08 18:04:58', '2021-04-08 18:04:58'),
(560, 56, 'Xem', 0, 'dashboard', 1, '2021-04-08 18:14:35', '2021-04-08 18:14:35'),
(561, 56, 'Thêm', 0, 'add', 2, '2021-04-08 18:14:35', '2021-04-08 18:14:35'),
(562, 56, 'Xóa', 0, 'delete', 4, '2021-04-08 18:14:35', '2021-04-08 18:14:35'),
(563, 56, 'Sửa', 0, 'edit', 3, '2021-04-08 18:14:35', '2021-04-08 18:14:35'),
(564, 58, 'Xem', 0, 'index', 1, '2021-04-08 18:17:53', '2021-04-08 18:17:53'),
(565, 58, 'Thêm', 0, 'add', 2, '2021-04-08 18:17:53', '2021-04-08 18:17:53'),
(566, 58, 'Xóa', 0, 'delete', 4, '2021-04-08 18:17:53', '2021-04-08 18:17:53'),
(567, 58, 'Sửa', 0, 'edit', 3, '2021-04-08 18:17:53', '2021-04-08 18:17:53'),
(568, 58, 'Phân quyền', 0, 'permission', 3, '2021-04-08 18:17:53', '2021-04-08 18:17:53'),
(569, 59, 'Xem', 0, 'index', 1, '2021-04-09 07:55:05', '2021-04-09 07:55:05'),
(570, 59, 'Thêm', 0, 'add', 2, '2021-04-09 07:55:05', '2021-04-09 07:55:05'),
(571, 59, 'Xóa', 0, 'delete', 4, '2021-04-09 07:55:05', '2021-04-09 07:55:05'),
(572, 59, 'Sửa', 0, 'edit', 3, '2021-04-09 07:55:05', '2021-04-09 07:55:05'),
(573, 59, 'Đổi mật khẩu', 0, 'changepass', 3, '2021-04-09 07:55:05', '2021-04-09 07:55:05'),
(574, 61, 'Xem', 0, 'index', 1, '2021-04-09 08:19:03', '2021-04-09 08:19:03'),
(579, 31, 'Xem', 0, 'index', 1, '2021-04-09 10:43:04', '2021-04-09 10:43:04'),
(580, 31, 'Xóa', 0, 'delete', 4, '2021-04-09 10:43:04', '2021-04-09 10:43:04'),
(581, 31, 'Thêm / Sửa', 0, 'form', 3, '2021-04-09 10:43:04', '2021-04-09 10:43:04'),
(582, 35, 'Xem', 0, 'index', 1, '2021-04-09 10:48:55', '2021-04-09 10:48:55'),
(583, 35, 'Thêm', 0, 'add', 2, '2021-04-09 10:48:55', '2021-04-09 10:48:55'),
(584, 35, 'Xóa', 0, 'delete', 4, '2021-04-09 10:48:55', '2021-04-09 10:48:55'),
(585, 35, 'Sửa', 0, 'edit', 3, '2021-04-09 10:48:55', '2021-04-09 10:48:55'),
(586, 44, 'Tổng quan', 0, 'report', 1, '2021-04-10 13:00:33', '2021-04-10 13:00:33'),
(587, 78, 'Xem', 0, 'index', 1, '2021-04-19 08:59:01', '2021-04-19 08:59:01'),
(590, 79, 'Xem', 0, 'profile', 1, '2021-04-26 17:09:03', '2021-04-26 17:09:03'),
(591, 32, 'Xem', 0, 'index', 1, '2021-04-29 09:41:35', '2021-04-29 09:41:35'),
(592, 32, 'Thêm', 0, 'form', 2, '2021-04-29 09:41:35', '2021-04-29 09:41:35'),
(593, 32, 'Sửa', 0, 'form', 3, '2021-04-29 09:41:35', '2021-04-29 09:41:35'),
(594, 32, 'Xóa', 0, 'delete', 4, '2021-04-29 09:41:35', '2021-04-29 09:41:35'),
(595, 80, 'Xem', 0, 'alert', 1, '2021-05-04 09:21:20', '2021-05-04 09:21:20'),
(596, 81, 'Xem', 0, 'index', 1, '2021-05-04 10:48:57', '2021-05-04 10:48:57'),
(597, 81, 'Thêm', 0, 'add', 2, '2021-05-04 10:48:57', '2021-05-04 10:48:57'),
(598, 81, 'Sửa', 0, 'edit', 4, '2021-05-04 10:48:57', '2021-05-04 10:48:57'),
(599, 81, 'Xóa', 0, 'delete', 3, '2021-05-04 10:48:57', '2021-05-04 10:48:57'),
(608, 82, 'Xem', 0, 'index', 1, '2021-05-14 13:39:17', '2021-05-14 13:39:17'),
(609, 82, 'Thêm', 0, 'add', 2, '2021-05-14 13:39:17', '2021-05-14 13:39:17'),
(610, 82, 'Xóa', 0, 'delete', 3, '2021-05-14 13:39:17', '2021-05-14 13:39:17'),
(611, 82, 'Sửa', 0, 'edit', 4, '2021-05-14 13:39:17', '2021-05-14 13:39:17'),
(612, 75, 'Xem', 0, 'index', 1, '2021-05-17 08:41:22', '2021-05-17 08:41:22'),
(613, 75, 'Duyệt / hủy', 0, 'approvalTimework', 2, '2021-05-17 08:41:22', '2021-05-17 08:41:22'),
(614, 84, 'Xem', 0, 'index', 1, '2021-05-17 16:10:42', '2021-05-17 16:10:42'),
(615, 84, 'Thêm', 0, 'add', 2, '2021-05-17 16:10:42', '2021-05-17 16:10:42'),
(616, 84, 'Sửa', 0, 'edit', 4, '2021-05-17 16:10:42', '2021-05-17 16:10:42'),
(617, 84, 'Xóa', 0, 'delete', 3, '2021-05-17 16:10:42', '2021-05-17 16:10:42'),
(618, 84, 'Duyệt', 0, 'approval', 5, '2021-05-17 16:10:42', '2021-05-17 16:10:42'),
(619, 85, 'Xem', 0, 'index', 1, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(620, 85, 'Thêm', 0, 'add', 2, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(621, 85, 'Sửa', 0, 'edit', 4, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(622, 85, 'Xóa câu hỏi', 0, 'del_question', 3, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(623, 85, 'Xóa bài kiểm tra', 0, 'del_createquiz', 5, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(624, 85, 'Xóa duyệt bài kiểm tra', 0, 'del_approval_quiz', 6, '2021-05-18 16:00:52', '2021-05-18 16:00:52'),
(625, 86, 'Xem', 0, 'index', 1, '2021-05-18 16:11:23', '2021-05-18 16:11:23');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_ogcharts`
--

CREATE TABLE `tbl_ogcharts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parentID` int(11) NOT NULL,
  `mission` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_ogcharts`
--

INSERT INTO `tbl_ogcharts` (`id`, `name`, `parentID`, `mission`, `created_at`, `updated_at`) VALUES
(18, 'Hội Đồng Quản Trị (BoD)', 0, '<p>Board of Directors</p>\r\n', '2021-04-10 14:42:44', '0000-00-00 00:00:00'),
(19, 'Giám Đốc Điều Hành (CEO)', 18, '<p>Chief of Executive Officer</p>\r\n', '2021-04-10 14:43:10', '0000-00-00 00:00:00'),
(20, 'Khối Chia Sẻ Thông Tin (CIO)', 19, '<p>Chief of Information Officer</p>\r\n', '2021-04-10 14:44:38', '0000-00-00 00:00:00'),
(21, 'Khối Vận Hành (COO)', 19, '<p>Chief of Operating Officer</p>\r\n', '2021-04-10 14:45:02', '0000-00-00 00:00:00'),
(22, 'Khối Tài Chính (CFO)', 19, '<p>Chief of Financial Officer</p>\r\n', '2021-04-10 14:45:27', '0000-00-00 00:00:00'),
(23, 'Phòng Kỹ Thuật - Pháp Chế', 20, '<p>Technical -&nbsp;Legal Compliance&nbsp;Department</p>\r\n', '2021-04-10 14:46:37', '0000-00-00 00:00:00'),
(24, 'Phòng Hành Chính - Nhân Sự', 21, '<p>Administrative - Human Resources Department</p>\r\n', '2021-04-10 14:47:00', '0000-00-00 00:00:00'),
(25, 'Phòng Kinh Doanh', 21, '<p>Business Department</p>\r\n', '2021-04-10 14:47:32', '0000-00-00 00:00:00'),
(26, 'Phòng Tài Chính - Kế Toán', 22, '<p>Finance - Accounting Department</p>\r\n', '2021-04-10 14:48:31', '0000-00-00 00:00:00'),
(27, 'Phòng Cung Ứng', 22, '<p>Supply Chain Department</p>\r\n', '2021-04-10 14:49:20', '0000-00-00 00:00:00'),
(28, 'Ban Hậu Cần', 27, '<p>Logistic Division</p>\r\n', '2021-04-10 14:49:59', '0000-00-00 00:00:00'),
(29, 'Ban Cung Ứng', 27, '<p>Supply Chain Division</p>\r\n', '2021-04-10 14:50:23', '0000-00-00 00:00:00'),
(30, 'Ban Chất Lượng', 27, '<p>Quality Compliance Division</p>\r\n', '2021-04-10 14:50:51', '0000-00-00 00:00:00'),
(31, 'Ban Kế Toán', 26, '<p>Accounting Division</p>\r\n', '2021-04-10 14:51:08', '0000-00-00 00:00:00'),
(32, 'Ban Tài Chính', 26, '<p>Finance Division</p>\r\n', '2021-04-10 14:51:37', '0000-00-00 00:00:00'),
(33, 'Ban Kiểm Toán', 26, '<p>Auditing Division</p>\r\n', '2021-04-10 14:52:19', '0000-00-00 00:00:00'),
(34, 'Ban Nghiên Cứu & Phát Triển', 25, '<p>Research &amp; Development Division</p>\r\n', '2021-04-10 14:52:59', '0000-00-00 00:00:00'),
(35, 'Ban Chăm Sóc Khách Hàng', 25, '<p>Customer Service Division</p>\r\n', '2021-04-10 14:56:11', '0000-00-00 00:00:00'),
(36, 'Ban Tiếp Thị & Truyền Thông', 25, '<p>Marketing Division</p>\r\n', '2021-04-10 14:57:06', '0000-00-00 00:00:00'),
(37, 'Ban Kinh Doanh Trực Tuyến', 25, '<p>Digital Business Division</p>\r\n', '2021-04-10 14:58:38', '0000-00-00 00:00:00'),
(38, 'Ban Kinh Doanh Cửa Hàng', 25, '<p>Physical Store Division</p>\r\n', '2021-04-10 14:59:02', '0000-00-00 00:00:00'),
(39, 'Ban Hành Chính', 24, '<p>Administration Division</p>\r\n', '2021-04-10 15:00:00', '0000-00-00 00:00:00'),
(40, 'Ban Nhân Sự', 24, '<p>Human Resources Division</p>\r\n', '2021-04-10 15:01:36', '0000-00-00 00:00:00'),
(41, 'Ban Pháp Chế', 23, '<p>Legal Compliance Division</p>\r\n', '2021-04-10 15:02:58', '0000-00-00 00:00:00'),
(42, 'Ban Vận Hành Cửa Hàng', 23, '<p>Stores Operation &amp; Maintenance Division</p>\r\n', '2021-04-10 15:03:34', '0000-00-00 00:00:00'),
(43, 'Ban Vận Hành Văn Phòng', 23, '<p>Office Operation &amp; Maintenance Division</p>\r\n', '2021-04-10 15:04:05', '0000-00-00 00:00:00'),
(44, 'Ban Công Nghệ Thông Tin', 23, '<p>Information Technologies Division</p>\r\n', '2021-04-10 15:05:17', '0000-00-00 00:00:00'),
(45, 'BP. Xử Lý Dữ Liệu', 28, '<p>Data Processing Unit</p>\r\n', '2021-04-10 15:06:15', '0000-00-00 00:00:00'),
(46, 'BP. Công Cụ Dụng Cụ', 28, '<p>Stationery Management Unit</p>\r\n', '2021-04-10 15:07:15', '0000-00-00 00:00:00'),
(47, 'BP. Vận Tải Kho Bãi', 28, '<p>Logistic Management Unit</p>\r\n', '2021-04-10 15:08:48', '0000-00-00 00:00:00'),
(48, 'BP. Thu Mua Cung Ứng', 29, '<p>Procurement Supply Unit</p>\r\n', '2021-04-10 15:18:30', '0000-00-00 00:00:00'),
(49, 'BP. Quan Hệ Đối Tác', 29, '<p>Supplier Relation Division</p>\r\n', '2021-04-10 15:44:48', '0000-00-00 00:00:00'),
(50, 'BP. Hợp Đồng Đối Tác', 29, '<p>Supplier Contract Management Unit</p>\r\n', '2021-04-10 15:45:31', '0000-00-00 00:00:00'),
(51, 'BP. Phát Triển Nguồn Hàng', 30, '<p>Sourcing Development Unit</p>\r\n', '2021-04-10 15:46:50', '0000-00-00 00:00:00'),
(52, 'BP. Kiểm Soát Chất Lượng', 30, '<p>Quality Control Unit</p>\r\n', '2021-04-10 15:47:35', '0000-00-00 00:00:00'),
(53, 'BP. Đảm Bảo Chất Lượng', 30, '<p>Quality Assurance Unit</p>\r\n', '2021-04-10 15:48:03', '0000-00-00 00:00:00'),
(54, 'BP. Kiểm Soát Bảo Quản', 30, '<p>Maintenance Control Unit</p>\r\n', '2021-04-10 15:48:27', '0000-00-00 00:00:00'),
(55, 'BP. Kiểm Soát Vận Chuyển', 30, '<p>Logistic Quality Control Unit</p>\r\n', '2021-04-10 15:52:02', '0000-00-00 00:00:00'),
(56, 'BP. Đánh Giá Chất Lượng', 30, '<p>Certified Inspection Unit</p>\r\n', '2021-04-10 15:52:51', '0000-00-00 00:00:00'),
(57, 'BP. Tài Chính', 33, '<p>Financial Auditing Unit</p>\r\n', '2021-04-10 15:54:12', '0000-00-00 00:00:00'),
(58, 'BP. Kiểm Toán Kho - TSCĐ', 33, '<p>Inventories &amp; Assets Auditing Unit</p>\r\n', '2021-04-10 15:55:08', '0000-00-00 00:00:00'),
(59, 'BP. Thủ Quỹ', 32, '<p>Treasuries&nbsp;Unit</p>\r\n', '2021-04-10 15:55:52', '0000-00-00 00:00:00'),
(60, 'BP. Phân Tích Tài Chính', 32, '<p>Financial Analysis Unit</p>\r\n', '2021-04-10 15:56:30', '0000-00-00 00:00:00'),
(61, 'BP. Phân Tích Quản Trị Rủi Ro', 32, '<p>Risk Analysis Management Unit</p>\r\n', '2021-04-10 15:57:12', '0000-00-00 00:00:00'),
(62, 'BP. Công Nợ', 31, '<p>Payable &amp; Receivable Unit</p>\r\n', '2021-04-10 15:58:14', '0000-00-00 00:00:00'),
(63, 'BP. Biên Chế & BHXH', 31, '<p>Payroll &amp; Insurance Unit</p>\r\n', '2021-04-10 15:58:44', '0000-00-00 00:00:00'),
(64, 'BP. Kế Toán Kho & TSCĐ', 31, '<p>Inventories - Tangible &amp; Intangible Fixed Assets</p>\r\n', '2021-04-10 16:00:02', '0000-00-00 00:00:00'),
(65, 'BP. Thu Chi', 31, '<p>Bookeeping Unit</p>\r\n', '2021-04-10 16:04:02', '0000-00-00 00:00:00'),
(66, 'BP. Thuế', 31, '<p>Taxation Unit</p>\r\n', '2021-04-10 16:04:56', '0000-00-00 00:00:00'),
(67, 'BP. Bán Hàng - Tiền Mặt - Ngân Hàng', 31, '<p>Sales - Cash - Bank Account Unit</p>\r\n', '2021-04-10 16:05:42', '0000-00-00 00:00:00'),
(68, 'BP. Quản Lý Vùng', 38, '<p>Area Management Unit</p>\r\n', '2021-04-10 16:06:38', '0000-00-00 00:00:00'),
(69, 'BP. Cửa Hàng Trưởng', 68, '<p>Store Manager Unit</p>\r\n', '2021-04-10 16:07:09', '0000-00-00 00:00:00'),
(70, 'BP. Giám Sát Kinh Doanh Cửa Hàng', 69, '<p>Store Supervisor Unit</p>\r\n', '2021-04-10 16:08:09', '0000-00-00 00:00:00'),
(71, 'BP. Nhân Viên Bán Hàng', 70, '<p>Store Sales Associate</p>\r\n', '2021-04-10 16:08:47', '0000-00-00 00:00:00'),
(72, 'BP. Nhân Viên Kho', 70, '<p>Stock Keeper Unit</p>\r\n', '2021-04-10 16:10:47', '0000-00-00 00:00:00'),
(73, 'BP. Nhân Viên Bảo Vệ', 70, '<p>Security Unit</p>\r\n', '2021-04-10 16:11:26', '0000-00-00 00:00:00'),
(74, 'BP. Giám Sát Kinh Doanh Trực Tuyến', 37, '<p>Digital Stores&nbsp;Supervisor</p>\r\n', '2021-04-10 16:12:20', '0000-00-00 00:00:00'),
(75, 'BP. Nhân Viên Kinh Doanh Trực Tuyến', 74, '<p>Online Sales Associate</p>\r\n', '2021-04-10 16:13:30', '0000-00-00 00:00:00'),
(76, 'BP. Giao Nhận Trực Tuyến', 74, '<p>Online Logistical Management Unit</p>\r\n', '2021-04-10 16:18:01', '0000-00-00 00:00:00'),
(77, 'BP. Phát Triển Chiến Lược', 36, '<p>Stratetic Development Unit</p>\r\n', '2021-04-10 16:18:44', '0000-00-00 00:00:00'),
(78, 'BP. Sáng Tạo Nội Dung', 36, '<p>Content Development Unit</p>\r\n', '2021-04-10 16:19:10', '0000-00-00 00:00:00'),
(79, 'BP. Sáng Tạo Hình Ảnh', 36, '<p>Graphic Development Unit</p>\r\n', '2021-04-10 16:20:27', '0000-00-00 00:00:00'),
(80, 'BP. Sáng Tạo Trưng Bày', 36, '<p>Display Development Unit</p>\r\n', '2021-04-10 16:21:03', '0000-00-00 00:00:00'),
(81, 'BP. Phát Triển Sản Phẩm', 36, '<p>Product Development Unit</p>\r\n', '2021-04-10 16:21:29', '0000-00-00 00:00:00'),
(82, 'BP. Phát Triển Chiến Dịch', 36, '<p>Campaign Development Unit</p>\r\n', '2021-04-10 16:21:57', '0000-00-00 00:00:00'),
(83, 'BP. Quan Hệ Công Chúng', 36, '<p>Public Relation Unit</p>\r\n', '2021-04-10 16:22:34', '0000-00-00 00:00:00'),
(84, 'BP. Sữa Chữa & Bảo Hành', 35, '<p>Repair &amp; Warranty Unit</p>\r\n', '2021-04-10 16:23:20', '0000-00-00 00:00:00'),
(85, 'BP. CSKH Trực Tuyến', 35, '<p>Online Customer Service Unit</p>\r\n', '2021-04-10 16:24:17', '0000-00-00 00:00:00'),
(86, 'BP. CSKH Cửa Hàng', 35, '<p>Store Customer Service Unit</p>\r\n', '2021-04-10 16:24:55', '0000-00-00 00:00:00'),
(87, 'BP. Nghiên Cứu Thị Trường', 34, '<p>Market Analysis Unit</p>\r\n', '2021-04-10 16:25:52', '0000-00-00 00:00:00'),
(88, 'BP. Phát Triển Thị Trường', 34, '<p>Market Development Unit</p>\r\n', '2021-04-10 16:27:09', '0000-00-00 00:00:00'),
(89, 'BP. Quản Lý & Phát Triển Thương Hiệu', 34, '<p>Brand Management &amp; Development</p>\r\n', '2021-04-10 16:28:20', '0000-00-00 00:00:00'),
(90, 'BP. Quan Hệ Lao Động', 39, '<p>Employee Relation Unit</p>\r\n', '2021-04-10 16:30:32', '0000-00-00 00:00:00'),
(91, 'BP. Đào Tạo & Phát Triển', 39, '<p>Training &amp; Development Unit</p>\r\n', '2021-04-10 16:31:06', '0000-00-00 00:00:00'),
(92, 'BP. Kiểm Soát Kỷ Luật', 39, '<p>Labour Disciplinary Unit</p>\r\n', '2021-04-10 16:31:45', '0000-00-00 00:00:00'),
(93, 'BP. Tuyển Dụng', 40, '<p>Recruitment Unit</p>\r\n', '2021-04-10 16:32:54', '0000-00-00 00:00:00'),
(94, 'BP. Lương Thưởng', 40, '<p>Wages &amp; Rewards Unit</p>\r\n', '2021-04-10 16:35:13', '0000-00-00 00:00:00'),
(95, 'BP. Phụ Cấp & BHXH', 40, '<p>Allowances &amp; Insurances Unit</p>\r\n', '2021-04-10 16:36:17', '0000-00-00 00:00:00'),
(96, 'BP. Chấm Công', 40, '<p>Time Keeping Unit</p>\r\n', '2021-04-10 16:36:57', '0000-00-00 00:00:00'),
(97, 'BP. Kiểm Soát Hành Chính', 41, '<p>Administrative Management Unit</p>\r\n', '2021-04-10 16:38:11', '0000-00-00 00:00:00'),
(98, 'BP. Truyền Thông', 41, '<p>Public Relation &amp; Communication Unit</p>\r\n', '2021-04-10 16:39:02', '0000-00-00 00:00:00'),
(99, 'BP. Quản Trị Pháp Lý', 41, '<p>Legal Management Unit</p>\r\n', '2021-04-10 16:39:34', '0000-00-00 00:00:00'),
(100, 'BP. Vận Hành Văn Phòng', 43, '<p>Office Operation Unit</p>\r\n', '2021-04-10 16:47:36', '0000-00-00 00:00:00'),
(101, 'BP. Bảo Dưỡng Văn Phòng', 43, '<p>Office Maintenance Unit</p>\r\n', '2021-04-10 16:50:46', '0000-00-00 00:00:00'),
(102, 'BP. TK-PT Văn Phòng', 43, '<p>Office Design &amp; Development</p>\r\n', '2021-04-10 16:53:59', '0000-00-00 00:00:00'),
(103, 'BP. Bảo Dưỡng Cửa Hàng', 42, '<p>Store Maintenance Unit</p>\r\n', '2021-04-10 16:55:18', '0000-00-00 00:00:00'),
(104, 'BP. Vận Hành Cửa Hàng', 42, '<p>Store Operation Unit</p>\r\n', '2021-04-10 16:55:52', '0000-00-00 00:00:00'),
(105, 'BP. TK-PT Cửa Hàng', 42, '<p>Store Design &amp; Development Unit</p>\r\n', '2021-04-10 16:56:43', '0000-00-00 00:00:00'),
(106, 'BP. Quản Trị Phần Mềm', 44, '<p>Software Management Unit</p>\r\n', '2021-04-10 16:57:38', '0000-00-00 00:00:00'),
(107, 'BP. Quản Trị Hạ Tầng', 44, '<p>Hardware Management Unit</p>\r\n', '2021-04-10 16:58:41', '0000-00-00 00:00:00'),
(108, 'BP. Kiểm Soát Hệ Thống', 44, '<p>System Administration Unit</p>\r\n', '2021-04-10 16:59:14', '0000-00-00 00:00:00'),
(109, 'BP. Phát Triển Hệ Thống', 44, '<p>System Development Unit</p>\r\n', '2021-04-10 17:00:06', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_orgcharts`
--

CREATE TABLE `tbl_orgcharts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_orgcharts`
--

INSERT INTO `tbl_orgcharts` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_permissions`
--

CREATE TABLE `tbl_permissions` (
  `id` int(11) NOT NULL,
  `careerroads` int(11) NOT NULL,
  `job_position_titleID` int(11) NOT NULL,
  `job_positionID` int(11) NOT NULL,
  `ogchartID` int(11) NOT NULL,
  `controller` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_permissions`
--

INSERT INTO `tbl_permissions` (`id`, `careerroads`, `job_position_titleID`, `job_positionID`, `ogchartID`, `controller`, `action`, `status`) VALUES
(749, 9, 0, 0, 0, 'employee', 'index', 1),
(751, 0, 54, 0, 0, 'contracts', 'index', 1),
(752, 0, 54, 0, 0, 'careerroadmap', 'index', 1),
(757, 0, 46, 0, 0, 'timework', 'index', 1),
(758, 0, 46, 0, 0, 'timework', 'add', 1),
(759, 0, 46, 0, 0, 'timework', 'edit', 1),
(760, 0, 51, 0, 0, 'timework', 'index', 1),
(761, 0, 51, 0, 0, 'timework', 'add', 1),
(762, 0, 51, 0, 0, 'timework', 'edit', 1),
(763, 0, 51, 0, 0, 'timework', 'delete', 1),
(764, 0, 51, 0, 0, 'approvaltimework', 'index', 1),
(765, 0, 51, 0, 0, 'approvaltimework', 'add', 1),
(766, 0, 51, 0, 0, 'approvaltimework', 'edit', 1),
(767, 0, 51, 0, 0, 'overviewtimework', 'index', 1),
(768, 0, 51, 0, 0, 'overviewtimework', 'add', 1),
(769, 0, 51, 0, 0, 'overviewtimework', 'edit', 1),
(770, 0, 50, 0, 0, 'timework', 'index', 1),
(771, 0, 50, 0, 0, 'timework', 'add', 1),
(772, 0, 50, 0, 0, 'timework', 'edit', 1),
(773, 0, 50, 0, 0, 'approvaltimework', 'index', 1),
(774, 0, 50, 0, 0, 'approvaltimework', 'add', 1),
(775, 0, 50, 0, 0, 'approvaltimework', 'edit', 1),
(776, 0, 50, 0, 0, 'overviewtimework', 'index', 1),
(777, 0, 50, 0, 0, 'overviewtimework', 'add', 1),
(778, 0, 50, 0, 0, 'overviewtimework', 'edit', 1),
(799, 0, 47, 0, 0, 'overviewtimework', 'index', 1),
(808, 0, 51, 0, 0, 'orgcharts', 'index', 1),
(809, 0, 51, 0, 0, 'employeepolicies', 'index', 1),
(810, 0, 51, 0, 0, 'recruitment', 'report', 1),
(811, 0, 51, 0, 0, 'recruitment', 'index', 1),
(812, 0, 51, 0, 0, 'recruitment', 'add', 1),
(813, 0, 51, 0, 0, 'recruitment', 'edit', 1),
(814, 0, 51, 0, 0, 'applylist', 'index', 1),
(815, 0, 51, 0, 0, 'applylist', 'add', 1),
(816, 0, 51, 0, 0, 'applylist', 'edit', 1),
(817, 0, 51, 0, 0, 'calendarinterview', 'index', 1),
(818, 0, 51, 0, 0, 'calendarinterview', 'add', 1),
(819, 0, 51, 0, 0, 'calendarinterview', 'edit', 1),
(820, 0, 51, 0, 0, 'Recruitment_rank', 'index', 1),
(821, 0, 51, 0, 0, 'Recruitment_rank', 'add', 1),
(822, 0, 51, 0, 0, 'Recruitment_rank', 'edit', 1),
(823, 0, 51, 0, 0, 'Recruitment_form', 'index', 1),
(824, 0, 51, 0, 0, 'Recruitment_form', 'add', 1),
(825, 0, 51, 0, 0, 'Recruitment_form', 'edit', 1),
(826, 0, 51, 0, 0, 'home', 'index', 1),
(827, 0, 51, 0, 0, 'auths', 'profile', 1),
(828, 0, 46, 0, 0, 'overviewtimework', 'index', 1),
(829, 0, 47, 0, 0, 'timework', 'index', 1),
(830, 0, 47, 0, 0, 'timework', 'add', 1),
(831, 0, 47, 0, 0, 'timework', 'edit', 1),
(833, 0, 46, 0, 0, 'timework', 'delete', 1),
(838, 0, 54, 0, 0, 'timework', 'index', 1),
(839, 0, 54, 0, 0, 'timework', 'add', 1),
(840, 0, 54, 0, 0, 'timework', 'edit', 1),
(841, 0, 54, 0, 0, 'timework', 'delete', 1),
(843, 0, 51, 0, 0, 'employee', 'index', 1),
(846, 0, 51, 0, 0, 'approvaltimework', 'approvalTimework', 1),
(847, 0, 54, 0, 0, 'employee', 'index', 1),
(848, 0, 54, 0, 0, 'auths', 'profile', 1),
(851, 0, 48, 0, 0, 'employee', 'index', 1),
(852, 0, 47, 0, 0, 'employee', 'index', 1),
(853, 0, 47, 0, 0, 'awardproposal', 'index', 1),
(855, 0, 47, 0, 0, 'awardproposal', 'approval', 1),
(856, 0, 51, 0, 0, 'test', 'index', 1),
(857, 0, 51, 0, 0, 'test', 'del_question', 1),
(858, 0, 51, 0, 0, 'test', 'del_createquiz', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_positions`
--

CREATE TABLE `tbl_positions` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_positions`
--

INSERT INTO `tbl_positions` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(3, 'Thực tập sinh/học việc (Intern)', 1, '2021-04-11 09:04:41', '2021-04-11 09:05:26'),
(4, 'Thử việc (Entry)', 1, '2021-04-11 09:05:18', '0000-00-00 00:00:00'),
(5, 'Nhân viên chính thức (Associate)', 1, '2021-04-11 09:06:13', '0000-00-00 00:00:00'),
(6, 'Chuyên viên sơ cấp (Junior)', 1, '2021-04-11 09:06:27', '0000-00-00 00:00:00'),
(7, 'Chuyên viên (Senior)', 1, '2021-04-11 09:06:42', '0000-00-00 00:00:00'),
(8, 'Phó Ban/Thừa ủy quyền Ban (Assistant Principal)', 1, '2021-04-11 09:07:38', '2021-04-11 09:08:37'),
(9, 'Trưởng Ban (Principal)', 1, '2021-04-11 09:07:53', '0000-00-00 00:00:00'),
(10, 'Phó Phòng/Thừa Ủy Quyền Phòng (Assistant Manager)', 1, '2021-04-11 09:08:25', '0000-00-00 00:00:00'),
(11, 'Trưởng Phòng (Manager)', 1, '2021-04-11 09:09:07', '0000-00-00 00:00:00'),
(12, 'Giám Đốc (Officer)', 1, '2021-04-11 09:09:24', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_prizes`
--

CREATE TABLE `tbl_prizes` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `des` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_prizes`
--

INSERT INTO `tbl_prizes` (`id`, `name`, `des`, `publish`, `created_at`, `updated_at`) VALUES
(2, 'Giaỉ thưởng 1', 'Nội dung', 1, '2021-05-13 16:42:48', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_question`
--

CREATE TABLE `tbl_question` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_questionID` int(11) NOT NULL,
  `question` text COLLATE utf8_unicode_ci NOT NULL,
  `answer` text COLLATE utf8_unicode_ci NOT NULL,
  `correct_answer` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_question`
--

INSERT INTO `tbl_question` (`id`, `type_questionID`, `question`, `answer`, `correct_answer`, `created_at`, `updated_at`) VALUES
(41, 1, '<p><strong>Đ&acirc;u l&agrave; một loại h&igrave;nh chợ tạm tự ph&aacute;t thường xuất hiện trong c&aacute;c khu d&acirc;n cư?</strong></p>\r\n', '[{\"alph\":\"D\",\"answer\":\"Heo\",\"sort\":\"4\"},{\"alph\":\"C\",\"answer\":\"B\\u1ecd c\\u1ea1p\",\"sort\":\"3\"},{\"alph\":\"B\",\"answer\":\"Th\\u1eb1n l\\u1eb1n\",\"sort\":\"2\"},{\"alph\":\"A\",\"answer\":\"\\u1ebech\",\"sort\":\"1\"}]', 'C', '2021-05-13 10:59:06', '2021-05-13 11:04:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_question_random`
--

CREATE TABLE `tbl_question_random` (
  `id` int(10) UNSIGNED NOT NULL,
  `create_quizID` int(11) NOT NULL,
  `questionID` int(11) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_question_random`
--

INSERT INTO `tbl_question_random` (`id`, `create_quizID`, `questionID`, `created_at`) VALUES
(91, 16, 41, '0000-00-00 00:00:00'),
(92, 16, 42, '0000-00-00 00:00:00'),
(93, 17, 41, '0000-00-00 00:00:00'),
(94, 17, 42, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_recruitment`
--

CREATE TABLE `tbl_recruitment` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alias` text COLLATE utf8_unicode_ci NOT NULL,
  `id_recruitment_rank` int(11) NOT NULL,
  `id_recruitment_form` int(11) NOT NULL,
  `degree` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `income` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `experience` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount` int(11) NOT NULL,
  `date_submit` date NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `district` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `workplace` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_recruitment`
--

INSERT INTO `tbl_recruitment` (`id`, `title`, `alias`, `id_recruitment_rank`, `id_recruitment_form`, `degree`, `income`, `experience`, `amount`, `date_submit`, `city`, `district`, `address`, `workplace`, `publish`, `content`, `avatar`, `created_at`, `updated_at`) VALUES
(3, 'Tuyển NHÂN VIÊN BẢO VỆ', 'tuyen-nhan-vien-bao-ve', 1, 1, 'Không yêu cầu', '6 - 7.5 Triệu', 'Không cần kinh nghiệm', 3, '2021-05-31', '4', '915', '', '703-727 Kha Vạn Cân, Phường Linh Tây, TP.Thủ Đức, TP. HCM', 1, '<p><strong>M&Ocirc; TẢ C&Ocirc;NG VIỆC</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Ghi thẻ xe, tr&ocirc;ng giữ, đảm bảo an to&agrave;n phương tiện của kh&aacute;ch h&agrave;ng;</p>\r\n	</li>\r\n	<li>\r\n	<p>Giữ an ninh, trật tự trước khu vực cửa h&agrave;ng;</p>\r\n	</li>\r\n	<li>\r\n	<p>Đảm bảo vệ sinh khu mặt tiền của cửa h&agrave;ng;</p>\r\n	</li>\r\n	<li>\r\n	<p>Kiểm tra v&agrave; bảo quản c&aacute;c vật dụng được giao, ph&aacute;svt hiện hư hỏng kịp thời c&aacute;c hệ thống điện, chiếu s&aacute;ng...;</p>\r\n	</li>\r\n	<li>\r\n	<p>Phối hợp với c&aacute;c bộ phận kh&aacute;c v&agrave; thực hiện một số y&ecirc;u cầu từ cửa h&agrave;ng trường.</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><strong>Y&Ecirc;U CẦU C&Ocirc;NG VIỆC</strong></p>\r\n\r\n<ul>\r\n	<li>\r\n	<p>Sức khỏe tốt</p>\r\n	</li>\r\n	<li>\r\n	<p>Phong c&aacute;ch tự tin, th&acirc;n thiện, nhiệt &nbsp;t&igrave;nh</p>\r\n	</li>\r\n	<li>\r\n	<p>Trung thực, thẳng thắn, th&aacute;i độ l&agrave;m việc nghi&ecirc;m t&uacute;c, t&acirc;m huyết, mong muốn gắn&nbsp;b&oacute; l&acirc;u d&agrave;i với cửa h&agrave;ng</p>\r\n	</li>\r\n	<li>\r\n	<p>C&oacute; tinh thần l&agrave;m việc tập thể, c&oacute; kỹ năng l&agrave;m việc nh&oacute;m</p>\r\n	</li>\r\n	<li>\r\n	<p>Chỉ phỏng vấn&nbsp;hồ sơ&nbsp;đạt y&ecirc;u cầu.</p>\r\n	</li>\r\n	<li>\r\n	<p>Nhanh nhẹn, năng động, th&aacute;o v&aacute;t, chịu kh&oacute;</p>\r\n	</li>\r\n</ul>\r\n\r\n<p><strong>H&Igrave;NH THỨC L&Agrave;M VIỆC</strong></p>\r\n\r\n<ul>\r\n	<li>To&agrave;n thời gian</li>\r\n	<li>Thời gian thử việc: 1 th&aacute;ng&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>PH&Uacute;C LỢI</strong></p>\r\n\r\n<ul>\r\n	<li>Mức lương&nbsp;6.000.000 đồng &ndash; 7.500.000 đồng</li>\r\n	<li>\r\n	<p>Đồng phục</p>\r\n	</li>\r\n	<li>\r\n	<p>C&oacute; cơ hội tham gia c&aacute;c lớp huấn luyện, đ&agrave;o tạo kĩ năng</p>\r\n	</li>\r\n	<li>\r\n	<p>Chế độ nghỉ ph&eacute;p</p>\r\n	</li>\r\n	<li>\r\n	<p>Chăm s&oacute;c sức khỏe hằng năm</p>\r\n	</li>\r\n</ul>\r\n', 'http://work.hungthinh1989.com/upload/recruitment/avatar31.jpg', '2021-04-17 10:55:47', '2021-05-14 23:17:50'),
(4, 'Tuyển nhân viên nhập liệu', 'tuyen-nhan-vien-nhap-lieu', 1, 2, 'Không yêu cầu', '3 - 5 Triệu', 'Không cần kinh nghiệm', 1, '2021-05-01', '4', '16', '', '4 Đường số 8, Phường Trường Thọ', 1, '<p><strong>M&Ocirc; TẢ C&Ocirc;NG VIỆC</strong></p>\r\n\r\n<ul>\r\n	<li>Tiếp nhận kế hoạch nhập xuất h&agrave;ng h&oacute;a</li>\r\n	<li>Nhập dữ liệu tr&ecirc;n hệ thống phần mềm, cơ sở dữ liệu database</li>\r\n	<li>L&agrave;m lệnh nhập xuất h&agrave;ng h&oacute;a</li>\r\n	<li>Kiểm so&aacute;t qu&aacute; tr&igrave;nh nhập, xuất của h&agrave;ng h&oacute;a</li>\r\n	<li>Tạo h&oacute;a đơn lu&acirc;n chuyển h&agrave;ng h&oacute;a giữa c&aacute;c chi nh&aacute;nh</li>\r\n</ul>\r\n\r\n<p><strong>Y&Ecirc;U CẦU C&Ocirc;NG VIỆC</strong></p>\r\n\r\n<ul>\r\n	<li>Nam/Nữ</li>\r\n	<li>Giao tiếp tiếng anh cơ bản</li>\r\n	<li>Tuổi từ 20-30</li>\r\n	<li>Ưu ti&ecirc;n ứng vi&ecirc;n c&oacute; kinh nghiệm nhập liệu hoặc th&ocirc;ng thạo excel</li>\r\n	<li>C&oacute; kỹ năng lưu trữ, sắp xếp hồ sơ khoa học.</li>\r\n	<li>Kỹ năng giao tiếp v&agrave; l&agrave;m việc nh&oacute;m tốt.</li>\r\n	<li>C&aacute;c tố chất cần thiết: si&ecirc;ng năng, cẩn thận, năng động, nhiệt t&igrave;nh, th&ocirc;ng minh, nhạy b&eacute;n, t&aacute;c phong chuy&ecirc;n nghiệp.</li>\r\n	<li>C&oacute; nguyện vọng được l&agrave;m việc l&acirc;u d&agrave;i tại C&ocirc;ng ty.</li>\r\n	<li>C&oacute; khả năng l&agrave;m việc độc lập, theo nh&oacute;m, chịu &aacute;p lực c&ocirc;ng việc cao.</li>\r\n	<li>Hiểu biết v&agrave; đam m&ecirc; trong lĩnh vực&nbsp;thời trang, cầu tiến, năng động, độc lập trong tư duy, suy nghĩ, c&ocirc;ng việc.</li>\r\n	<li>Biết sử dụng vi t&iacute;nh th&agrave;nh thạo v&agrave; c&aacute;c loại phần mềm tin học văn ph&ograve;ng đặc biệt excel.</li>\r\n	<li>Lu&ocirc;n phấn đấu đi l&ecirc;n trong c&ocirc;ng việc</li>\r\n</ul>\r\n\r\n<p><strong>H&Igrave;NH THỨC L&Agrave;M VIỆC</strong></p>\r\n\r\n<ul>\r\n	<li>To&agrave;n thời gian/Theo ca/Xoay ca</li>\r\n	<li>Thời gian thử việc: 1 th&aacute;ng&nbsp;</li>\r\n</ul>\r\n\r\n<p><strong>PH&Uacute;C LỢI</strong></p>\r\n\r\n<ul>\r\n	<li>Mức lương cạnh tranh theo năng lực;</li>\r\n	<li>Được l&agrave;m việc trong m&ocirc;i trường chuy&ecirc;n nghiệp của C&ocirc;ng ty,&nbsp;</li>\r\n	<li>C&oacute; cơ hội thăng tiến,đ&agrave;o tạo nội bộ, c&oacute; điều kiện khẳng định bản th&acirc;n;</li>\r\n	<li>Được hưởng mức lương hấp dẫn theo thỏa thuận;</li>\r\n</ul>\r\n\r\n<p><strong>PHƯƠNG THỨC ỨNG TUYỂN</strong></p>\r\n\r\n<ul>\r\n	<li>Điền th&ocirc;ng tin ứng tuyển&nbsp;Tại đ&acirc;y</li>\r\n	<li>Gửi CV ứng tuyển về địa chỉ email: tuyendung@hungthinh1989.com</li>\r\n</ul>\r\n', 'http://work.hungthinh1989.com/upload/recruitment/avatar32.jpg', '2021-04-17 17:25:39', '2021-05-14 23:17:22'),
(7, 'Tuyển dụng NHÂN VIÊN KẾ TOÁN', 'tuyen-nhan-vien-ke-toan', 1, 2, 'Trung Cấp', '6 - 8 triệu + năng suất', '1 năm', 2, '2021-05-30', '4', '915', '', '4 đường số 8, phường Trường Thọ', 1, '<p>Thu thập v&agrave; ph&acirc;n t&iacute;ch th&ocirc;ng tin kế to&aacute;n để lập b&uacute;t to&aacute;n t&agrave;i sản, nợ phải trả, vốn chủ sở hữu.</p>\r\n\r\n<p>● Ghi ch&eacute;p c&aacute;c nghiệp vụ t&agrave;i ch&iacute;nh.</p>\r\n\r\n<p>● Thu thập th&ocirc;ng tin, lập bảng c&acirc;n đối kế to&aacute;n, b&aacute;o c&aacute;o kết quả kinh doanh v&agrave; c&aacute;c b&aacute;o c&aacute;o kh&aacute;c để tổng kết t&igrave;nh h&igrave;nh t&agrave;i ch&iacute;nh hiện tại.</p>\r\n\r\n<p>● Kiểm tra chứng từ để x&aacute;c minh c&aacute;c nghiệp vụ t&agrave;i ch&iacute;nh.</p>\r\n\r\n<p>● Kế to&aacute;n ng&acirc;n h&agrave;ng, tiền mặt, chi ph&iacute;, t&agrave;i sản cố định, thuế GTGT v&agrave; c&aacute;c nghiệp vụ ph&aacute;t sinh kh&aacute;c</p>\r\n\r\n<p>● Giao dịch với kh&aacute;ch h&agrave;ng, ng&acirc;n h&agrave;ng, cơ quan thuế</p>\r\n\r\n<p>● Lập c&aacute;c b&aacute;o c&aacute;o h&agrave;ng th&aacute;ng, qu&yacute;, năm gửi cơ quan thuế (theo đ&uacute;ng chế độ b&aacute;o c&aacute;o, thống k&ecirc; của ph&aacute;p luật về kế to&aacute;n, thuế v&agrave; quản trị)</p>\r\n\r\n<p>● Lập b&aacute;o c&aacute;o t&agrave;i ch&iacute;nh gửi Ban Gi&aacute;m Đốc</p>\r\n\r\n<p>● Thực hiện c&aacute;c c&ocirc;ng việc kh&aacute;c theo sự ph&acirc;n c&ocirc;ng của l&atilde;nh đạo c&ocirc;ng ty.</p>\r\n', 'http://work.hungthinh1989.com/upload/recruitment/avatar30.jpg', '2021-05-14 23:15:02', '2021-05-14 23:15:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_recruitment_form`
--

CREATE TABLE `tbl_recruitment_form` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_recruitment_form`
--

INSERT INTO `tbl_recruitment_form` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Làm việc theo ca', 1, '2021-04-17 10:39:24', '0000-00-00 00:00:00'),
(2, 'Làm việc giờ hành chính', 1, '2021-04-17 10:39:39', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_recruitment_rank`
--

CREATE TABLE `tbl_recruitment_rank` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_recruitment_rank`
--

INSERT INTO `tbl_recruitment_rank` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Nhân viên', 1, '2021-04-17 10:39:00', '0000-00-00 00:00:00'),
(2, 'Quản lý', 1, '2021-04-17 10:39:08', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_roles`
--

CREATE TABLE `tbl_roles` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_schools`
--

CREATE TABLE `tbl_schools` (
  `id` int(11) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_schools`
--

INSERT INTO `tbl_schools` (`id`, `code`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(2, 'ED001', 'Đại học Bách khoa- Đại học Quốc gia TPHCM', 1, '2021-02-19', '2021-02-19'),
(3, 'ED002', 'Đại học Khoa học tự nhiên- Đại học Quốc gia TPHCM', 1, '2021-02-19', '2021-02-27'),
(4, 'ED003', 'Đại học Khoa học xã hội và nhân văn- Đại học Quốc gia TPHCM', 1, '2021-02-27', '0000-00-00'),
(5, 'ED004', 'Đại học Quốc tế- Đại học Quốc gia TPHCM', 1, '2021-02-27', '0000-00-00'),
(6, 'ED005', 'Đại học Công nghệ thông tin – Đại học Quốc gia TPHCM', 1, '2021-02-27', '0000-00-00'),
(7, 'ED006', 'Đại học Kinh tế- Luật- Đại học Quốc gia TPHCM', 1, '2021-02-27', '0000-00-00'),
(8, 'ED007', 'Học viện Công nghệ Bưu chính Viễn thông TPHCM', 1, '2021-02-27', '0000-00-00'),
(9, 'ED008', 'Học viện Hàng không Việt Nam', 1, '2021-02-27', '0000-00-00'),
(10, 'ED009', 'Đại học Công nghiệp TPHCM', 1, '2021-02-27', '0000-00-00'),
(11, 'ED010', 'Đại học Công nghiệp thực phẩm TPHCM', 1, '2021-02-27', '0000-00-00'),
(12, 'ED011', 'Đại học Giao thông Vận tải cơ sở 2', 1, '2021-02-27', '0000-00-00'),
(13, 'ED012', 'Đại học Giao thông vận tải TPHCM', 1, '2021-02-27', '0000-00-00'),
(14, 'ED013', 'Đại học Kiến trúc TPHCM', 1, '2021-02-27', '0000-00-00'),
(15, 'ED014', 'Đại học Kinh tế TPHCM', 1, '2021-02-27', '0000-00-00'),
(16, 'ED015', 'Đai học Lao động xã hội TPHCM', 1, '2021-02-27', '0000-00-00'),
(17, 'ED016', 'Đại học Luật TPHCM', 1, '2021-02-27', '0000-00-00'),
(18, 'ED017', 'Đại học Mở TPHCM', 1, '2021-02-27', '0000-00-00'),
(19, 'ED018', 'Đại học Mỹ thuật TPHCM', 1, '2021-02-27', '0000-00-00'),
(20, 'ED019', 'Đại học Ngân hàng TPHCM', 1, '2021-02-27', '0000-00-00'),
(21, 'ED020', 'Đại học Ngoại thương- cơ sở phía Nam', 1, '2021-02-27', '0000-00-00'),
(22, 'ED021', 'Đại học Nội vụ Hà Nội- cơ sở phía Nam', 1, '2021-02-27', '0000-00-00'),
(23, 'ED022', 'Nhạc viện TPHCM', 1, '2021-02-27', '0000-00-00'),
(24, 'ED023', 'Đại học Nông Lâm TPHCM', 1, '2021-02-27', '0000-00-00'),
(25, 'ED024', 'Đại học Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(26, 'ED025', 'Đại học Sân khấu điện ảnh TPHCM', 1, '2021-02-27', '0000-00-00'),
(27, 'ED026', 'Đại học Sư phạm TPHCM', 1, '2021-02-27', '0000-00-00'),
(28, 'ED027', 'Đại học Sư phạm Kỹ thuật TPHCM', 1, '2021-02-27', '0000-00-00'),
(29, 'ED028', 'Đại học Tài chính-  Marketing', 1, '2021-02-27', '0000-00-00'),
(30, 'ED029', 'Đại học Tài nguyên và Môi trường TPHCM', 1, '2021-02-27', '0000-00-00'),
(31, 'ED030', 'Đại học Thể dục thể thao TPHCM', 1, '2021-02-27', '0000-00-00'),
(32, 'ED031', 'Đại học Thủy lợi TPHCM', 1, '2021-02-27', '0000-00-00'),
(33, 'ED032', 'Trường Đại học Tôn Đức Thắng', 1, '2021-02-27', '0000-00-00'),
(34, 'ED033', 'Đại học Văn hóa TPHCM', 1, '2021-02-27', '0000-00-00'),
(35, 'ED034', 'Đại học Y Dược TPHCM', 1, '2021-02-27', '0000-00-00'),
(36, 'ED035', 'Đại học Y khoa Phạm Ngọc Thạch', 1, '2021-02-27', '0000-00-00'),
(37, 'ED036', 'Đại học Công nghệ Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(38, 'ED037', 'Đại học Công nghệ TPHCM', 1, '2021-02-27', '0000-00-00'),
(39, 'ED038', 'Đại học Gia Định', 1, '2021-02-27', '0000-00-00'),
(40, 'ED039', 'Đại học Hoa Sen', 1, '2021-02-27', '0000-00-00'),
(41, 'ED040', 'Đại học Hùng Vương', 1, '2021-02-27', '0000-00-00'),
(42, 'ED041', 'Đại học Kinh tế- Tài chính TPHCM', 1, '2021-02-27', '0000-00-00'),
(43, 'ED042', 'Đại học Ngoại ngữ- Tin học TPHCM', 1, '2021-02-27', '0000-00-00'),
(44, 'ED043', 'Đại học Nguyễn Tất Thành', 1, '2021-02-27', '0000-00-00'),
(45, 'ED044', 'Đại học Quốc tế Hồng Bàng', 1, '2021-02-27', '0000-00-00'),
(46, 'ED045', 'Đại học Văn Hiến', 1, '2021-02-27', '0000-00-00'),
(47, 'ED046', 'Đại học Quốc tế Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(48, 'ED047', 'Đại học Văn Lang', 1, '2021-02-27', '0000-00-00'),
(49, 'ED048', 'Đại Học FPT', 1, '2021-02-27', '0000-00-00'),
(50, 'ED049', 'Cao đẳng An ninh mạng iSPACE', 1, '2021-02-27', '0000-00-00'),
(51, 'ED050', 'Cao đẳng Bách khoa Nam Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(52, 'ED051', 'Cao đẳng Bách Việt', 1, '2021-02-27', '0000-00-00'),
(53, 'ED052', 'Cao đẳng Bán công Công nghệ và Quản trị doanh nghiệp', 1, '2021-02-27', '0000-00-00'),
(54, 'ED053', 'Cao đẳng Công nghệ Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(55, 'ED054', 'Cao đẳng Công nghệ Thủ Đức', 1, '2021-02-27', '0000-00-00'),
(56, 'ED055', 'Cao đẳng Công thương TP.HCM', 1, '2021-02-27', '0000-00-00'),
(57, 'ED056', 'Cao đẳng Đại Việt Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(58, 'ED057', 'Cao đẳng Điện lực TP.HCM', 1, '2021-02-27', '0000-00-00'),
(59, 'ED058', 'Cao đẳng Giao thông vận tải Đường thủy II', 1, '2021-02-27', '0000-00-00'),
(60, 'ED059', 'Cao đẳng Giao thông Vận tải Trung ương VI', 1, '2021-02-27', '0000-00-00'),
(61, 'ED060', 'Cao đẳng Giao thông Vận tải TP.HCM', 1, '2021-02-27', '0000-00-00'),
(62, 'ED061', 'Cao đẳng Kinh tế – Công nghệ TP.HCM', 1, '2021-02-27', '0000-00-00'),
(63, 'ED062', 'Cao đẳng Kinh tế Đối ngoại', 1, '2021-02-27', '0000-00-00'),
(64, 'ED063', 'Cao đẳng Kinh tế Kỹ thuật TP. Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(65, 'ED064', 'Cao đẳng Kinh tế – Kỹ thuật Thủ Đức', 1, '2021-02-27', '0000-00-00'),
(66, 'ED065', 'Cao đẳng Kinh tế – Kỹ thuật Vinatex TP.HCM', 1, '2021-02-27', '0000-00-00'),
(67, 'ED066', 'Cao đẳng Kinh tế TP.HCM', 1, '2021-02-27', '0000-00-00'),
(68, 'ED067', 'Cao đẳng Kỹ nghệ II', 1, '2021-02-27', '0000-00-00'),
(69, 'ED068', 'Cao đẳng Kỹ thuật Cao Thắng', 1, '2021-02-27', '0000-00-00'),
(70, 'ED069', 'Cao đẳng Kỹ thuật Công nghệ Vạn Xuân', 1, '2021-02-27', '0000-00-00'),
(71, 'ED070', 'Cao đẳng Kỹ thuật Nguyễn Trường Tộ', 1, '2021-02-27', '0000-00-00'),
(72, 'ED071', 'Cao đẳng Lý Tự Trọng thành phố Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(73, 'ED072', 'Cao đẳng Miền Nam', 1, '2021-02-27', '0000-00-00'),
(74, 'ED073', 'Cao đẳng nghề Du lịch Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(75, 'ED074', 'Cao đẳng nghề Hàng hải Tp. Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(76, 'ED075', 'Cao đẳng nghề Thành phố Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(77, 'ED076', 'Cao đẳng nghề Thủ Thiêm – Tp. Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(78, 'ED077', 'Cao đẳng Phát thanh Truyền hình 2', 1, '2021-02-27', '0000-00-00'),
(79, 'ED078', 'Cao đẳng Quốc tế Tp. Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(80, 'ED079', 'Cao đẳng Quốc tế KENT', 1, '2021-02-27', '0000-00-00'),
(81, 'ED080', 'Cao đẳng Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(82, 'ED081', 'Cao đẳng Sài Gòn Gia Định', 1, '2021-02-27', '0000-00-00'),
(83, 'ED082', 'Cao đẳng Văn hóa Nghệ thuật TP.HCM', 1, '2021-02-27', '0000-00-00'),
(84, 'ED083', 'Cao đẳng Văn hóa Nghệ thuật và Du lịch Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(85, 'ED084', 'Cao đẳng Viễn Đông', 1, '2021-02-27', '0000-00-00'),
(86, 'ED085', 'Cao đẳng Việt Mỹ', 1, '2021-02-27', '0000-00-00'),
(87, 'ED086', 'Cao đẳng Xây dựng Tp. Hồ Chí Minh', 1, '2021-02-27', '0000-00-00'),
(88, 'ED087', 'Cao đẳng Y Dược Hồng Đức', 1, '2021-02-27', '0000-00-00'),
(89, 'ED088', 'Cao đẳng Y Dược Sài Gòn', 1, '2021-02-27', '0000-00-00'),
(90, 'ED089', 'Khác', 1, '2021-02-27', '0000-00-00'),
(91, 'ED090', 'Cao đẳng Y Dược PASTEUR', 1, '2021-05-04', '0000-00-00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `key` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `key`, `content`, `created_at`) VALUES
(1, 'general', '{\"company\":\"H\\u01afNG TH\\u1ecaNH INC.\",\"hotline\":\"089 854 89 80\",\"email\":\"info@hungthinh1989.com\"}', '2021-02-27 15:43:03');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_standards`
--

CREATE TABLE `tbl_standards` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_standards`
--

INSERT INTO `tbl_standards` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'THPT', 1, '2021-02-02 16:29:09', '2021-02-19 10:57:53'),
(2, 'Khác', 1, '2021-02-02 16:29:16', '2021-02-19 10:58:02'),
(3, 'Trung cấp', 1, '2021-02-02 16:29:25', '0000-00-00 00:00:00'),
(4, 'Cao đẳng', 1, '2021-02-02 16:29:36', '0000-00-00 00:00:00'),
(5, 'Đại học', 1, '2021-02-02 16:29:42', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_timework`
--

CREATE TABLE `tbl_timework` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` text COLLATE utf8_unicode_ci NOT NULL,
  `employeeID` int(11) NOT NULL,
  `startwork` time NOT NULL,
  `endwork` time NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `approval` int(5) NOT NULL,
  `shift` int(11) NOT NULL,
  `className` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_timework`
--

INSERT INTO `tbl_timework` (`id`, `uid`, `employeeID`, `startwork`, `endwork`, `title`, `start`, `approval`, `shift`, `className`, `type`, `created_at`, `updated_at`) VALUES
(253, '9faa9c27-3a19-40ba-b720-bbfd54ece011', 36, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-03-30', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(257, '0b4d7e21-7023-4617-b868-dea7bc8381b5', 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-07', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(258, 'c647399e-2f8a-4241-844e-46fee973f7e2', 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-06', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(259, '8e0cdb01-3ee0-47f0-ac58-2f6a378a79e9', 13, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-04-07', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(260, '60ed1243-6bf5-4acd-bca0-53cbd0b89692', 13, '07:00:00', '13:00:00', '7:00 - 13:00', '2021-04-08', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(262, '6f21fc2a-8c09-4f8f-a3fe-b5b4a19f10d6', 19, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-04-06', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(263, '61ea87ff-dcfb-4be8-b37e-40af7bcee68c', 19, '07:00:00', '13:00:00', '7:00 - 13:00', '2021-04-05', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(264, 'a181da25-1c52-4576-96fa-e743e220702f', 19, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-01', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(265, '7ee0bc8a-0a6e-445e-97dd-66cec6a9bd1f', 19, '08:00:00', '10:00:00', '8:00 - 10:00', '2021-03-28', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(266, 'ed42585f-7ce5-43a1-89a3-17faf69160e1', 19, '07:00:00', '10:00:00', '7:00 - 10:00', '2021-03-29', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(267, 'd3076ee6-0cb2-4309-a8c9-efdd53da00ac', 1, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-03-28', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(268, 'b4c53faa-e8aa-4d5a-8c2b-1536ba5bde32', 1, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-03-29', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(269, '54530e97-23eb-4e48-82ad-b17794f49019', 1, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-03-30', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(270, '4497d5da-9230-4463-959e-3ebcb7eab2f5', 1, '09:00:00', '11:00:00', '9:00 - 11:00', '2021-03-31', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(271, 'ecf3fbd1-7e6f-4404-8bb1-fce57ec106e7', 1, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-01', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(272, 'e15cf56a-62ab-4334-b98a-7e2a22a3205d', 1, '07:00:00', '10:00:00', '7:00 - 10:00', '2021-04-02', 0, 0, 'destroy_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(273, '17cbf57d-db29-48f6-9c78-7f6e0dc58cca', 17, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-03', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(274, '4279f511-8028-4050-980e-a7a00769508d', 17, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-04', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(275, '72dbf50e-484a-4540-b6ae-be48cad4bff3', 17, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-05', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(276, 'd3b16770-7801-4c92-9118-4aaab407f718', 17, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-07', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(277, '0c3b9672-b5af-425e-823e-630b8d3ddbd8', 17, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-08', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(278, '7189493d-0d78-471b-a363-2c67e3251935', 17, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-09', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(279, '363a9578-fe02-4208-8d22-8736436fff67', 10, '08:00:00', '07:00:00', '8:00 - 7:00', '2021-04-10', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(280, 'c2563375-8c42-4447-91b9-a1c8875b2f4e', 10, '14:00:00', '22:00:00', '14:00 - 22:00', '2021-05-03', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(281, '95408721-1889-4b77-bdd1-9c93ed30ca21', 10, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-04', 0, 0, 'destroy_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(282, '7a1acb20-2786-492c-9dbc-4a1ef41c1e33', 10, '14:00:00', '22:00:00', '14:00 - 22:00', '2021-05-07', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(283, '2500faae-2a08-4007-b439-8fdbad1f6afb', 10, '08:00:00', '18:00:00', '8:00 - 18:00', '2021-05-08', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(284, '05fbeaf6-91b5-4791-96c9-a7f83397e395', 10, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-09', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(285, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(286, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(287, 'c99153cd-c913-479d-92ea-c6d413960047', 11, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(288, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(289, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(290, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(291, '245d23d0-bbda-46f4-8da9-b4b94c089c3a', 11, '07:00:00', '08:00:00', '7:00 - 8:00', '2021-05-23', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(293, 'f58a842f-0f27-4720-9041-9219c143825f', 22, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(295, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(296, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(297, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(298, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(299, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(300, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, '12:00:00', '15:00:00', '12:00 - 15:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(302, '78ca26a3-dc01-4e24-acd3-0411a09f006e', 16, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(303, '925f8e54-8882-4008-9d44-32968355e9c1', 16, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(304, 'ca6a4613-2480-490c-a853-377cfdce24a7', 16, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(305, '588e1923-c36c-4423-973f-09d11f3a6e1a', 16, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(306, '7a93b5d0-291c-4afd-8371-8fb63f1a0f2f', 16, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(307, '1e15abb6-9aac-434c-9ad9-3c0f97c74ae4', 16, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(308, '2814a7de-00f7-4013-af73-f2fdecacff5d', 16, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(309, 'f7a98de0-0edb-4e9d-be0a-5ea0cff26549', 16, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(310, 'a80037d7-2771-4886-be43-fb7181a293a1', 16, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(311, '7fe4c7c1-b30f-476e-94d1-1cec79264505', 22, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(314, 'ff1e2309-2618-4173-b78a-3e649aed724f', 16, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(315, 'ea0970ca-c65a-4712-903b-f4b1f10021a0', 16, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(319, '187fab24-8fbc-4ec8-a21c-71e0b1b86ba5', 16, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(320, '3c47f72e-0e9f-4eec-b107-0c9f09faec81', 23, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(321, '5256ef94-1b9e-4194-a1a5-028f345d0502', 23, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(322, '29f3c336-1a49-4a0a-8cfe-89871be623cd', 23, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(323, '3e5b677d-b2d1-43b7-8dad-d87910e23834', 23, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(324, '9737223c-a411-457e-8a37-4af2080db066', 23, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(325, '63693cfb-e200-417b-bf5f-2e7c5517adce', 23, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(326, '2c9b5e70-878d-4241-b9af-4f50e49a2ebe', 23, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(327, 'ebf5872f-284a-462e-9379-956de3453e56', 23, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(328, '9c96df0d-4435-4c45-a678-af8a60c66683', 23, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(331, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(332, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(333, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(334, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(335, '11589977-fabf-46d4-b52e-b705963c71e1', 11, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(336, '78731f62-2193-41e9-825b-8f0176232017', 11, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(337, '5b9ddd75-0f31-44d7-84f3-402ee0812abc', 21, '17:00:00', '18:00:00', '17:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(338, '67451caa-e7fe-4212-a8c8-6f07c0f6ec1b', 21, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(339, 'b410509f-6911-4823-9974-baded37803fb', 21, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(340, 'db4192ca-342c-4f8b-b715-cac0542df724', 21, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(341, '96842007-cd5e-4b66-a627-8e0a46a67654', 21, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(342, '8de11a69-d812-45f7-9f00-6d7a5c1c5cc1', 21, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(343, '90198ca1-3e9b-452f-aef6-a2e42531576f', 21, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(344, '9bb6d163-9703-449c-88f6-181b90b7ee5e', 21, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(345, '5935f10d-5edb-4551-84ad-69baf2e372ee', 21, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(346, '8bfbcea6-257e-4ddf-afe2-8eb1fdb09723', 17, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(347, '805e1d76-afa5-4192-908a-3ec1039cecb2', 17, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(348, '65f5f804-06d2-4ef2-82f0-e9855ef87909', 17, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(349, '8492fd57-121b-408d-95aa-06c144d156f0', 17, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(350, '187ae1b8-3d80-4ac0-b111-09bafeb0497c', 17, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(351, 'deaee299-a0be-4c06-8802-bbc997396b0e', 17, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(352, 'ce5059ee-bbfe-4624-802d-d95d3d180262', 17, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(353, 'a2f00843-dc34-41f1-a544-7e8748f7e599', 17, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(354, '38f6afc1-f9fa-47d2-b85a-05949973c368', 17, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(355, '8fb6432a-d5ee-4211-86a1-e3db42397d74', 17, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(356, '93ac17ff-e879-4c3a-9065-5cee8e0f5d16', 17, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(357, '0f6c6ed7-5ddd-43ef-b83d-b8a26ce50b93', 17, '12:00:00', '15:00:00', '12:00 - 15:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(358, 'fccced1a-3731-4e40-ac86-3b5cc4570962', 20, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(359, 'ecd51c0c-aac4-472d-9b62-bc2966c70470', 20, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(360, '424231a7-769f-48cf-9dfb-03b0c3ea3bcd', 20, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(361, '466ec330-4ad9-4a06-9498-1fd2f0810732', 20, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(362, '894417c7-38e6-4232-9428-05cf2e17670a', 20, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(363, '7eae2049-9586-486c-b69f-70485ecab8b9', 20, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(364, '279695e4-6ff3-4859-a929-9511e0633d2b', 20, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(365, '2a87073b-870c-4f93-8557-2cd9c597d074', 20, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(366, '6bf095b4-fbdb-4f2e-8d85-6c7dcd264f5e', 22, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 1, 0, 'done_approval', 'admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(367, '44d9dfbf-d5a7-4f2a-97bd-51506cd82534', 22, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(368, '0b774615-f4d6-4cbb-b311-e52ee15a5707', 22, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(369, 'bf711589-14d2-47ad-a2d3-2654a5cb6419', 22, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(370, '96b4a0de-92c2-482e-a6fe-cdd0395cfcf4', 22, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(371, '0171b2f1-e51d-470e-add5-4a71a19d3721', 1, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(373, 'd5767aa5-4166-40df-8eeb-41ffd2022c9c', 1, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(374, 'ba008c93-1b63-45da-a9e7-6e4d33030fb3', 1, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(375, '4f1de21b-9c38-440a-8a97-7b21a1fe1ee7', 1, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(376, '07299808-604b-4fb0-a053-167667f486a1', 1, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(381, 'a50d8f09-5d32-4627-8fc9-c6359a6044c6', 1, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(382, '31d91215-34a2-4bd3-817d-8e8e655bed41', 7, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(383, '0dd4133b-c9be-4b9f-bf0d-62ac808742f1', 7, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(384, '8c581529-07e2-4728-b0aa-463e5ef53296', 7, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(385, '00b234bc-4415-4667-8385-6b0435ced941', 7, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(386, '38d04a22-47ba-4d5d-861d-13205176c3bc', 7, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(387, '06e39028-2cb4-4c58-8a79-054154f623bf', 7, '08:00:00', '13:00:00', '8:00 - 13:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(397, '1d951a62-08da-4a24-8df4-fffaf5510322', 1, '12:00:00', '17:00:00', '12:00 - 17:00', '2021-05-17', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(398, '311de130-202d-4839-abc3-be14c224c907', 1, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(399, 'c6a8d135-cddc-40dd-b424-6ab2cdb607be', 1, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-20', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(400, 'f4de057a-f30b-48fa-a1db-a3d090569100', 1, '08:00:00', '18:00:00', '8:00 - 18:00', '2021-05-21', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(401, '3a20ba2a-73e4-4d56-8e19-9969d896d848', 1, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(402, 'fea9aa40-d5b1-4aaa-9ee2-ae5ca78141cd', 2, '07:00:00', '14:00:00', '7:00 - 14:00', '2021-04-06', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(403, 'a2f3baaa-4682-4746-affc-ae225707dc9c', 2, '07:00:00', '16:00:00', '7:00 - 16:00', '2021-04-07', 2, 0, 'wait_approval', 'employee', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_timework_history`
--

CREATE TABLE `tbl_timework_history` (
  `id` int(10) UNSIGNED NOT NULL,
  `uid` text COLLATE utf8_unicode_ci NOT NULL,
  `employeeID` int(11) NOT NULL,
  `timeworkID` int(11) NOT NULL,
  `startwork` time NOT NULL,
  `endwork` time NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `start` date NOT NULL,
  `approval` int(5) NOT NULL,
  `className` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `shift` int(11) NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_timework_history`
--

INSERT INTO `tbl_timework_history` (`id`, `uid`, `employeeID`, `timeworkID`, `startwork`, `endwork`, `title`, `start`, `approval`, `className`, `shift`, `type`, `created_at`) VALUES
(211, '9faa9c27-3a19-40ba-b720-bbfd54ece011', 36, 0, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-03-30', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(215, '0b4d7e21-7023-4617-b868-dea7bc8381b5', 0, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-07', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(216, 'c647399e-2f8a-4241-844e-46fee973f7e2', 0, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-06', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(217, '8e0cdb01-3ee0-47f0-ac58-2f6a378a79e9', 13, 0, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-04-07', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(218, '8e0cdb01-3ee0-47f0-ac58-2f6a378a79e9', 13, 0, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-04-07', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(219, '60ed1243-6bf5-4acd-bca0-53cbd0b89692', 13, 0, '07:00:00', '13:00:00', '7:00 - 13:00', '2021-04-08', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(221, '6f21fc2a-8c09-4f8f-a3fe-b5b4a19f10d6', 19, 0, '07:00:00', '11:00:00', '7:00 - 11:00', '2021-04-06', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(222, '61ea87ff-dcfb-4be8-b37e-40af7bcee68c', 19, 0, '07:00:00', '13:00:00', '7:00 - 13:00', '2021-04-05', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(223, 'a181da25-1c52-4576-96fa-e743e220702f', 19, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-01', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(224, '7ee0bc8a-0a6e-445e-97dd-66cec6a9bd1f', 19, 0, '08:00:00', '10:00:00', '8:00 - 10:00', '2021-03-28', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(225, 'ed42585f-7ce5-43a1-89a3-17faf69160e1', 19, 0, '07:00:00', '10:00:00', '7:00 - 10:00', '2021-03-29', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(226, 'c647399e-2f8a-4241-844e-46fee973f7e2', 0, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-06', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(227, '0b4d7e21-7023-4617-b868-dea7bc8381b5', 0, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-07', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(228, 'd3076ee6-0cb2-4309-a8c9-efdd53da00ac', 1, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-03-28', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(229, 'b4c53faa-e8aa-4d5a-8c2b-1536ba5bde32', 1, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-03-29', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(230, '54530e97-23eb-4e48-82ad-b17794f49019', 1, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-03-30', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(231, '4497d5da-9230-4463-959e-3ebcb7eab2f5', 1, 0, '09:00:00', '11:00:00', '9:00 - 11:00', '2021-03-31', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(232, 'ecf3fbd1-7e6f-4404-8bb1-fce57ec106e7', 1, 0, '07:00:00', '09:00:00', '7:00 - 9:00', '2021-04-01', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(233, 'e15cf56a-62ab-4334-b98a-7e2a22a3205d', 1, 0, '07:00:00', '10:00:00', '7:00 - 10:00', '2021-04-02', 0, 'destroy_approval', 0, 'admin', '0000-00-00 00:00:00'),
(234, '17cbf57d-db29-48f6-9c78-7f6e0dc58cca', 17, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-03', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(235, '4279f511-8028-4050-980e-a7a00769508d', 17, 0, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-04', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(236, '72dbf50e-484a-4540-b6ae-be48cad4bff3', 17, 0, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-05', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(237, 'd3b16770-7801-4c92-9118-4aaab407f718', 17, 0, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-07', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(238, '0c3b9672-b5af-425e-823e-630b8d3ddbd8', 17, 0, '13:00:00', '22:00:00', '13:00 - 22:00', '2021-05-08', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(239, '7189493d-0d78-471b-a363-2c67e3251935', 17, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-09', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(240, '363a9578-fe02-4208-8d22-8736436fff67', 10, 0, '08:00:00', '07:00:00', '8:00 - 7:00', '2021-04-10', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(241, 'c2563375-8c42-4447-91b9-a1c8875b2f4e', 10, 0, '14:00:00', '22:00:00', '14:00 - 22:00', '2021-05-03', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(242, '95408721-1889-4b77-bdd1-9c93ed30ca21', 10, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-04', 0, 'destroy_approval', 0, 'admin', '0000-00-00 00:00:00'),
(243, '7a1acb20-2786-492c-9dbc-4a1ef41c1e33', 10, 0, '14:00:00', '22:00:00', '14:00 - 22:00', '2021-05-07', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(244, '2500faae-2a08-4007-b439-8fdbad1f6afb', 10, 0, '08:00:00', '18:00:00', '8:00 - 18:00', '2021-05-08', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(245, '05fbeaf6-91b5-4791-96c9-a7f83397e395', 10, 0, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-09', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(246, '363a9578-fe02-4208-8d22-8736436fff67', 10, 0, '08:00:00', '07:00:00', '8:00 - 7:00', '2021-04-10', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(247, 'c2563375-8c42-4447-91b9-a1c8875b2f4e', 10, 0, '14:00:00', '22:00:00', '14:00 - 22:00', '2021-05-03', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(248, '95408721-1889-4b77-bdd1-9c93ed30ca21', 10, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-04', 0, 'destroy_approval', 0, 'admin', '0000-00-00 00:00:00'),
(249, '7a1acb20-2786-492c-9dbc-4a1ef41c1e33', 10, 0, '14:00:00', '22:00:00', '14:00 - 22:00', '2021-05-07', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(250, '2500faae-2a08-4007-b439-8fdbad1f6afb', 10, 0, '08:00:00', '18:00:00', '8:00 - 18:00', '2021-05-08', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(251, '05fbeaf6-91b5-4791-96c9-a7f83397e395', 10, 0, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-09', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(252, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(253, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(254, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(255, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(256, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '22:00:00', '8:00 - 22:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(257, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '08:00:00', '18:00:00', '8:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(258, '245d23d0-bbda-46f4-8da9-b4b94c089c3a', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(260, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(262, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(263, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(264, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(265, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(266, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(267, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(270, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(272, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(273, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(274, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(275, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(276, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(277, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(280, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(282, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(283, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(284, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(285, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(286, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(287, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(290, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(292, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(293, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(294, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(295, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(296, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(297, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(299, '78ca26a3-dc01-4e24-acd3-0411a09f006e', 16, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(300, '925f8e54-8882-4008-9d44-32968355e9c1', 16, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(301, 'ca6a4613-2480-490c-a853-377cfdce24a7', 16, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(302, '588e1923-c36c-4423-973f-09d11f3a6e1a', 16, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(303, '7a93b5d0-291c-4afd-8371-8fb63f1a0f2f', 16, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(304, '1e15abb6-9aac-434c-9ad9-3c0f97c74ae4', 16, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(305, '2814a7de-00f7-4013-af73-f2fdecacff5d', 16, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(306, 'f7a98de0-0edb-4e9d-be0a-5ea0cff26549', 16, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(307, 'a80037d7-2771-4886-be43-fb7181a293a1', 16, 0, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(308, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(309, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(310, '7fe4c7c1-b30f-476e-94d1-1cec79264505', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(311, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(312, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(313, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(314, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(316, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(317, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(318, '7fe4c7c1-b30f-476e-94d1-1cec79264505', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(319, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(320, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(321, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(322, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(324, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(325, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(326, '7fe4c7c1-b30f-476e-94d1-1cec79264505', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(327, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(328, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(329, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(330, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(332, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(333, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(334, '7fe4c7c1-b30f-476e-94d1-1cec79264505', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(335, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(336, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(337, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(338, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(340, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(343, '1e15abb6-9aac-434c-9ad9-3c0f97c74ae4', 16, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(344, 'ff1e2309-2618-4173-b78a-3e649aed724f', 16, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(345, '1e15abb6-9aac-434c-9ad9-3c0f97c74ae4', 16, 0, '13:00:00', '18:00:00', '13:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(346, 'ea0970ca-c65a-4712-903b-f4b1f10021a0', 16, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(350, 'ff1e2309-2618-4173-b78a-3e649aed724f', 16, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(351, '187fab24-8fbc-4ec8-a21c-71e0b1b86ba5', 16, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(352, '3c47f72e-0e9f-4eec-b107-0c9f09faec81', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(353, '5256ef94-1b9e-4194-a1a5-028f345d0502', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(354, '29f3c336-1a49-4a0a-8cfe-89871be623cd', 23, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(355, '3e5b677d-b2d1-43b7-8dad-d87910e23834', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(356, '9737223c-a411-457e-8a37-4af2080db066', 23, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(357, '63693cfb-e200-417b-bf5f-2e7c5517adce', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(358, '2c9b5e70-878d-4241-b9af-4f50e49a2ebe', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(359, 'ebf5872f-284a-462e-9379-956de3453e56', 23, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(360, '9c96df0d-4435-4c45-a678-af8a60c66683', 23, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(361, '3c47f72e-0e9f-4eec-b107-0c9f09faec81', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(362, '5256ef94-1b9e-4194-a1a5-028f345d0502', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(363, '29f3c336-1a49-4a0a-8cfe-89871be623cd', 23, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(364, '3e5b677d-b2d1-43b7-8dad-d87910e23834', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(365, '9737223c-a411-457e-8a37-4af2080db066', 23, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(366, '63693cfb-e200-417b-bf5f-2e7c5517adce', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(367, '2c9b5e70-878d-4241-b9af-4f50e49a2ebe', 23, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(368, 'ebf5872f-284a-462e-9379-956de3453e56', 23, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(369, '9c96df0d-4435-4c45-a678-af8a60c66683', 23, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(372, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(373, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(374, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(375, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(376, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(377, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(378, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(379, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(380, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(381, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(382, '11589977-fabf-46d4-b52e-b705963c71e1', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(383, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(384, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(385, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(386, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(387, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(388, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(389, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(390, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(391, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(392, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(393, '11589977-fabf-46d4-b52e-b705963c71e1', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(394, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(395, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(396, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(397, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(398, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(399, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(400, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(401, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(402, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(403, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(404, '11589977-fabf-46d4-b52e-b705963c71e1', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(405, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(406, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(407, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(408, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(409, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(410, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(411, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(412, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(413, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(414, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(415, '11589977-fabf-46d4-b52e-b705963c71e1', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(416, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(417, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(418, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(419, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(420, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(421, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(422, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(423, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(424, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(425, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(426, '11589977-fabf-46d4-b52e-b705963c71e1', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(427, '245d23d0-bbda-46f4-8da9-b4b94c089c3a', 11, 0, '07:00:00', '08:00:00', '7:00 - 8:00', '2021-05-23', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(428, '07de3259-7fc7-4ad3-b7a4-fb0b886b43d5', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(429, '4402b107-867d-4d84-9165-921aae8fd2e8', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(430, '3bee8935-d4a0-4e65-96a0-b08f3ff4e4ad', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(431, '6c404520-bc92-43cd-979b-87aa12c089f3', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(432, 'e1a4b979-20d1-4302-89d0-b9e77430c21d', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(433, 'c99153cd-c913-479d-92ea-c6d413960047', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(434, 'aef8eacb-e6ab-42b8-94fb-912779619ffa', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(435, 'b6e94e3f-6b26-436b-929b-a0d2b99b910d', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(436, 'b94ea657-9ab8-4fd7-8951-0256b0897b6a', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(437, '5b4e360a-6c90-4d1c-943f-eca0fcdbe791', 11, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(438, '11589977-fabf-46d4-b52e-b705963c71e1', 11, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(439, '245d23d0-bbda-46f4-8da9-b4b94c089c3a', 11, 0, '07:00:00', '08:00:00', '7:00 - 8:00', '2021-05-23', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(440, '78731f62-2193-41e9-825b-8f0176232017', 11, 0, '07:00:00', '07:00:00', '7:00 - 7:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(441, '78731f62-2193-41e9-825b-8f0176232017', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(442, '78731f62-2193-41e9-825b-8f0176232017', 11, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(443, '5b9ddd75-0f31-44d7-84f3-402ee0812abc', 21, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(444, '67451caa-e7fe-4212-a8c8-6f07c0f6ec1b', 21, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(445, 'b410509f-6911-4823-9974-baded37803fb', 21, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(446, 'db4192ca-342c-4f8b-b715-cac0542df724', 21, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(447, '96842007-cd5e-4b66-a627-8e0a46a67654', 21, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(448, '8de11a69-d812-45f7-9f00-6d7a5c1c5cc1', 21, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(449, '90198ca1-3e9b-452f-aef6-a2e42531576f', 21, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(450, '9bb6d163-9703-449c-88f6-181b90b7ee5e', 21, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(451, '5935f10d-5edb-4551-84ad-69baf2e372ee', 21, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(452, '5b9ddd75-0f31-44d7-84f3-402ee0812abc', 21, 0, '17:00:00', '18:00:00', '17:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(453, '8bfbcea6-257e-4ddf-afe2-8eb1fdb09723', 17, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(454, '805e1d76-afa5-4192-908a-3ec1039cecb2', 17, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(455, '65f5f804-06d2-4ef2-82f0-e9855ef87909', 17, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(456, '8492fd57-121b-408d-95aa-06c144d156f0', 17, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(457, '187ae1b8-3d80-4ac0-b111-09bafeb0497c', 17, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(458, 'deaee299-a0be-4c06-8802-bbc997396b0e', 17, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(459, 'ce5059ee-bbfe-4624-802d-d95d3d180262', 17, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(460, 'a2f00843-dc34-41f1-a544-7e8748f7e599', 17, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(461, '38f6afc1-f9fa-47d2-b85a-05949973c368', 17, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(462, '8fb6432a-d5ee-4211-86a1-e3db42397d74', 17, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(463, '93ac17ff-e879-4c3a-9065-5cee8e0f5d16', 17, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(464, '0f6c6ed7-5ddd-43ef-b83d-b8a26ce50b93', 17, 0, '12:00:00', '15:00:00', '12:00 - 15:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(465, 'fccced1a-3731-4e40-ac86-3b5cc4570962', 20, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(466, 'ecd51c0c-aac4-472d-9b62-bc2966c70470', 20, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(467, '424231a7-769f-48cf-9dfb-03b0c3ea3bcd', 20, 0, '15:00:00', '18:00:00', '15:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(468, '466ec330-4ad9-4a06-9498-1fd2f0810732', 20, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(469, '894417c7-38e6-4232-9428-05cf2e17670a', 20, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(470, '7eae2049-9586-486c-b69f-70485ecab8b9', 20, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(471, '279695e4-6ff3-4859-a929-9511e0633d2b', 20, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(472, '2a87073b-870c-4f93-8557-2cd9c597d074', 20, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(473, '6bf095b4-fbdb-4f2e-8d85-6c7dcd264f5e', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(474, '86da64e3-6dd7-4cb0-bde7-d7bc3a3f6bf3', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(475, '44d9dfbf-d5a7-4f2a-97bd-51506cd82534', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(476, '9f21be3f-dc30-4b16-8f4e-34cb4396040c', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(477, 'f58a842f-0f27-4720-9041-9219c143825f', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(478, '0b774615-f4d6-4cbb-b311-e52ee15a5707', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(479, '0d4e3ce2-2004-4c5c-a99e-70bdaae2adfb', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(480, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(481, 'bf711589-14d2-47ad-a2d3-2654a5cb6419', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(482, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(483, '44d9dfbf-d5a7-4f2a-97bd-51506cd82534', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(484, '96b4a0de-92c2-482e-a6fe-cdd0395cfcf4', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(485, '44d9dfbf-d5a7-4f2a-97bd-51506cd82534', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(486, '96b4a0de-92c2-482e-a6fe-cdd0395cfcf4', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(487, '44d9dfbf-d5a7-4f2a-97bd-51506cd82534', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(488, '96b4a0de-92c2-482e-a6fe-cdd0395cfcf4', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(489, 'fea17d54-fd47-4eca-afdb-bd06712b540b', 22, 0, '12:00:00', '15:00:00', '12:00 - 15:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(490, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(491, 'd18c54e3-b98c-4d8e-8fa1-a234d32cf32a', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(492, 'f394edb2-49a1-499c-943a-114ae3e9c025', 22, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(493, '7fe4c7c1-b30f-476e-94d1-1cec79264505', 22, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(494, '96b4a0de-92c2-482e-a6fe-cdd0395cfcf4', 22, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(495, '0171b2f1-e51d-470e-add5-4a71a19d3721', 1, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(497, 'd5767aa5-4166-40df-8eeb-41ffd2022c9c', 1, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(498, 'ba008c93-1b63-45da-a9e7-6e4d33030fb3', 1, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(499, '4f1de21b-9c38-440a-8a97-7b21a1fe1ee7', 1, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(500, '07299808-604b-4fb0-a053-167667f486a1', 1, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-19', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(505, 'a50d8f09-5d32-4627-8fc9-c6359a6044c6', 1, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 1, 'done_approval', 0, 'admin', '0000-00-00 00:00:00'),
(506, '31d91215-34a2-4bd3-817d-8e8e655bed41', 7, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(507, '0dd4133b-c9be-4b9f-bf0d-62ac808742f1', 7, 0, '12:00:00', '18:00:00', '12:00 - 18:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(508, '8c581529-07e2-4728-b0aa-463e5ef53296', 7, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-18', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(509, '00b234bc-4415-4667-8385-6b0435ced941', 7, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(510, '38d04a22-47ba-4d5d-861d-13205176c3bc', 7, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(511, '06e39028-2cb4-4c58-8a79-054154f623bf', 7, 0, '08:00:00', '13:00:00', '8:00 - 13:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(521, '1d951a62-08da-4a24-8df4-fffaf5510322', 1, 0, '12:00:00', '17:00:00', '12:00 - 17:00', '2021-05-17', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(522, '311de130-202d-4839-abc3-be14c224c907', 1, 0, '18:00:00', '22:00:00', '18:00 - 22:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(523, 'c6a8d135-cddc-40dd-b424-6ab2cdb607be', 1, 0, '08:00:00', '15:00:00', '8:00 - 15:00', '2021-05-20', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(524, 'f4de057a-f30b-48fa-a1db-a3d090569100', 1, 0, '08:00:00', '18:00:00', '8:00 - 18:00', '2021-05-21', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(525, '3a20ba2a-73e4-4d56-8e19-9969d896d848', 1, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-22', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(526, 'a50d8f09-5d32-4627-8fc9-c6359a6044c6', 1, 0, '08:00:00', '12:00:00', '8:00 - 12:00', '2021-05-23', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(527, 'fea9aa40-d5b1-4aaa-9ee2-ae5ca78141cd', 2, 0, '07:00:00', '14:00:00', '7:00 - 14:00', '2021-04-06', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(528, 'fea9aa40-d5b1-4aaa-9ee2-ae5ca78141cd', 2, 0, '07:00:00', '14:00:00', '7:00 - 14:00', '2021-04-06', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00'),
(529, 'a2f3baaa-4682-4746-affc-ae225707dc9c', 2, 0, '07:00:00', '16:00:00', '7:00 - 16:00', '2021-04-07', 2, 'wait_approval', 0, 'employee', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_typefiles`
--

CREATE TABLE `tbl_typefiles` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_typefiles`
--

INSERT INTO `tbl_typefiles` (`id`, `name`, `publish`, `created_at`, `updated_at`) VALUES
(2, 'Hợp đồng', 1, '2021-02-06', '2021-02-06'),
(4, 'Biên bản', 1, '2021-02-06', '2021-02-06');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_type_question`
--

CREATE TABLE `tbl_type_question` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_type_question`
--

INSERT INTO `tbl_type_question` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Chọn đáp án A, B, C, D', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Nhập câu trả lời', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employeeID` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_account` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(12) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `verify` tinyint(4) NOT NULL,
  `ogchartID` int(11) NOT NULL,
  `employees_id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `employeeID`, `email`, `password`, `text_pass`, `type_account`, `type`, `code`, `salt`, `status`, `active`, `verify`, `ogchartID`, `employees_id`, `fullname`, `role_id`, `created_at`, `updated_at`) VALUES
(1, '', 0, 'hungthinhllc@gmail.com', '7d1aff778f9f2463590f7119e85f17bcbed7a91a', '123456', '', 'admin', 0, 'b3cWc!b!', 0, 1, 0, 0, 0, '', 0, '2020-10-19 16:45:33', '2020-11-18 13:23:17'),
(8, 'oanhm', 15, 'Oanhm@hungthinh1989.com', 'b419da5887712649dfe3ec35068400486ebe68b5', 'Oanh123456', '', '', 0, '+?EBZxBS', 0, 1, 0, 0, 0, 'Ma Thị Oanh', 1, '2021-04-11 10:46:39', '0000-00-00 00:00:00'),
(11, 'nhih', 1, 'nhi67536@gmail.com', '97660b4e29d1b0a44a79a1e3bd6ed84b952f9e9b', '12032000', '', '', 0, '?syJcpkL', 0, 1, 0, 0, 0, 'Hồ Thị Phương Nhi', 1, '2021-04-18 12:56:13', '2021-04-29 14:22:03'),
(13, 'xuant', 2, 'thanhxuann2004@gmai.com', 'f33b2e07488072b768d0431fe3950872231d922e', 'ntttqt7172', '', '', 0, 'Zn2ci4uQ', 0, 1, 0, 0, 0, 'Trần Thị Thanh Xuân', 1, '2021-04-19 16:09:59', '2021-04-27 14:48:26'),
(14, 'tienn', 3, 'nguyenthitructien167@gmail.com', 'd4fe738581b5f8127e48f82287ed4e8b18a9733f', '69452962', '', '', 0, '/wPYckaX', 0, 1, 0, 0, 0, 'Nguyễn Thị Trúc Tiên', 1, '2021-04-26 16:25:43', '0000-00-00 00:00:00'),
(15, 'hav', 4, 'vuthiha140900@gmail.com', '9a7df57994c7babe02e5b29e0475d908fe489791', '73679099', '', '', 0, '.t47Vs=3', 0, 1, 0, 0, 0, 'Vũ Thị Hà', 1, '2021-04-26 16:26:28', '0000-00-00 00:00:00'),
(16, 'binhn', 6, 'thanhbinh1592002@gmail.com', '7e807f6dc711add404b74b6893aefb904f0db3e3', '72062878', '', '', 0, 'KV!F8v3p', 0, 1, 0, 0, 0, 'Nguyễn Thị Thanh Bình', 1, '2021-04-26 16:26:46', '0000-00-00 00:00:00'),
(17, 'tramn', 9, 'nguyentram000262@gmail.com', '7ed4a677d9dc658e280a4dc507308425d7b5a38c', '34051384', '', '', 0, '4dZR5C99', 0, 1, 0, 0, 0, 'Nguyễn Thị Thùy Trâm', 1, '2021-04-26 16:27:02', '0000-00-00 00:00:00'),
(18, 'han', 10, 'thanhha20122002@gmail.com', '6b7ef49c71c370d39709a8c1e784f6a44888ccf8', '53849572', '', '', 0, '7DL4xPRV', 0, 1, 0, 0, 0, 'Nguyễn Thị Thanh Hà', 1, '2021-04-26 16:28:00', '0000-00-00 00:00:00'),
(19, 'sangl', 7, 'sangglee123@gmail.com', 'e240a12402937d5db6cb1af19f686214edec2d9a', '463391', '', '', 0, '/c2ZxSHp', 0, 1, 0, 0, 0, 'Lê Thị Ngọc Sáng', 1, '2021-04-26 16:28:34', '2021-04-30 21:04:29'),
(20, 'chaun', 11, 'nguyenthibaochau2212@gmail.com', 'e9e8ebd1527422ed4dc25b2943aa0e09e5a25f76', 'baochau2212', '', '', 0, '2nYf,?zk', 0, 1, 0, 0, 0, 'Nguyễn Thị Bảo Châu', 1, '2021-04-26 16:30:13', '2021-05-03 09:37:43'),
(21, 'tuann', 12, 'nhanhtuantkdh217@gmail.com', '0cee028eeb7e30431cf8c5bd38790166dda68e77', '65369407', '', '', 0, 'LEJV666t', 0, 1, 0, 0, 0, 'Nguyễn Hoàng Anh Tuấn', 1, '2021-04-26 16:30:31', '0000-00-00 00:00:00'),
(23, 'nhul', 17, 'nhuluu006@gmail.com', '8d44a677c3df00d13aea15b5a1f0d5f168a09050', '66127184', '', '', 0, '+cnr8GZA', 0, 1, 0, 0, 0, 'Lưu Ngọc Quỳnh Như', 1, '2021-04-26 16:31:04', '0000-00-00 00:00:00'),
(24, 'myt', 20, 'tthmy@giayhungthinh.vn', '27b0b8f10172e1c87227571d7c02b9c9ca67cd7b', '21810479', '', '', 0, 'yFEH,YGH', 0, 1, 0, 0, 0, 'Tất Thị Hồng My', 1, '2021-04-26 16:33:11', '0000-00-00 00:00:00'),
(25, 'huongn', 21, 'nhxhuong@giayhungthinh.vn', '2d91cb7e085edc55455a837826836eb0e11bf20a', '92189964', '', '', 0, 'KtJkfBhR', 0, 1, 0, 0, 0, 'Nguyễn Huỳnh Xuân Hương', 1, '2021-04-26 16:33:32', '0000-00-00 00:00:00'),
(26, 'thaont', 22, 'ntthao@giayhhungthinh.vn', '57350c693c958b00d727cefbf6490bb7bbbcfb68', '15092008', '', '', 0, 'RghxcYjh', 0, 1, 0, 0, 0, 'Nguyễn Thanh Thảo', 1, '2021-04-26 16:34:00', '2021-05-14 23:07:40'),
(30, 'thoan', 16, 'kimthonguyen130402@gmail.com', 'd7b51cca878cd211368554b5373d4cc316733538', '100596', '', '', 0, ',iGhYHJb', 0, 1, 0, 0, 0, 'Nguyễn Thị Kim Thoa', 1, '2021-05-04 10:44:10', '2021-05-13 14:42:20'),
(31, 'hungh', 14, 'Hungh@hungthinh1989.com', 'acbc50931e6e154a4436267261bd64beae4678e7', '94088250', '', '', 0, 'cix!A=K+', 0, 1, 0, 0, 0, 'Huỳnh Minh Hùng', 1, '2021-05-04 10:51:50', '0000-00-00 00:00:00'),
(32, 'thaon', 13, 'Thaon@hungthinh1989.com', 'a34af60445a12174ad3fd1b2090655e5d8d71749', '21782821', '', '', 0, '2V3n=HzH', 0, 1, 0, 0, 0, 'Nguyễn Ngọc Phương Thảo', 1, '2021-05-04 10:52:09', '0000-00-00 00:00:00'),
(33, 'dangn', 26, 'nguyenduydang2323@gmail.com', 'cd37e037c7c18e28dc6486f51ba4263985c34c90', '91238441', '', '', 0, 'vzLdkf&y', 0, 1, 0, 0, 0, 'Nguyễn Duy Đăng', 1, '2021-05-04 13:12:47', '0000-00-00 00:00:00'),
(35, 'nguyenh', 23, 'thaonguyen302802@gmail.com', 'fd504526fbf30e84d4131418ca3b38c359b316c6', '02092002', '', '', 0, 'zYE!har.', 0, 1, 0, 0, 0, 'Hồ Ngọc Thảo Nguyên', 1, '2021-05-14 08:37:42', '2021-05-14 08:48:48'),
(36, 'Trân', 28, 'tranj.giayhungthinh@gmail.com', '924113adfa54ab0acf6a31e587cf7c6dcf6d4143', '120521T', '', '', 0, 'c5yrbsWr', 0, 1, 0, 0, 0, 'Trân', 1, '2021-05-14 14:27:30', '2021-05-14 22:02:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_workrule`
--

CREATE TABLE `tbl_workrule` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_workrule`
--

INSERT INTO `tbl_workrule` (`id`, `name`, `content`, `publish`, `created_at`, `updated_at`) VALUES
(1, 'Quy định 1', '<p>Nội dungNội dungvNội dungNội dung</p>\r\n', 1, '2021-05-04 10:16:38', '0000-00-00 00:00:00'),
(2, 'Quy định 2 update', '<p>Nội dungNội dungNội dungNội dungNội dung Update</p>\r\n', 1, '2021-05-04 10:16:56', '2021-05-04 10:46:37');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_apply`
--
ALTER TABLE `tbl_apply`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_apply_workexperience`
--
ALTER TABLE `tbl_apply_workexperience`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_awardproposal`
--
ALTER TABLE `tbl_awardproposal`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_banks`
--
ALTER TABLE `tbl_banks`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_branchs`
--
ALTER TABLE `tbl_branchs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_bussinesstypes`
--
ALTER TABLE `tbl_bussinesstypes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_calendar_interview`
--
ALTER TABLE `tbl_calendar_interview`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_careerroads`
--
ALTER TABLE `tbl_careerroads`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_certificate`
--
ALTER TABLE `tbl_certificate`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_chat_private`
--
ALTER TABLE `tbl_chat_private`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_classify_employee`
--
ALTER TABLE `tbl_classify_employee`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_contracts`
--
ALTER TABLE `tbl_contracts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_create_quiz`
--
ALTER TABLE `tbl_create_quiz`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_edu_status`
--
ALTER TABLE `tbl_edu_status`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employeelevel`
--
ALTER TABLE `tbl_employeelevel`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees`
--
ALTER TABLE `tbl_employees`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees_banks`
--
ALTER TABLE `tbl_employees_banks`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees_historywork`
--
ALTER TABLE `tbl_employees_historywork`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees_relatives`
--
ALTER TABLE `tbl_employees_relatives`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees_salary`
--
ALTER TABLE `tbl_employees_salary`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees_standards`
--
ALTER TABLE `tbl_employees_standards`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_employees_works`
--
ALTER TABLE `tbl_employees_works`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_files`
--
ALTER TABLE `tbl_files`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_formofworks`
--
ALTER TABLE `tbl_formofworks`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_job_positions`
--
ALTER TABLE `tbl_job_positions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_log`
--
ALTER TABLE `tbl_log`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_majors`
--
ALTER TABLE `tbl_majors`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_maketest`
--
ALTER TABLE `tbl_maketest`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_milestones`
--
ALTER TABLE `tbl_milestones`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_modules_detail`
--
ALTER TABLE `tbl_modules_detail`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_ogcharts`
--
ALTER TABLE `tbl_ogcharts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_orgcharts`
--
ALTER TABLE `tbl_orgcharts`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_positions`
--
ALTER TABLE `tbl_positions`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_prizes`
--
ALTER TABLE `tbl_prizes`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_question`
--
ALTER TABLE `tbl_question`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_question_random`
--
ALTER TABLE `tbl_question_random`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_recruitment`
--
ALTER TABLE `tbl_recruitment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_recruitment_form`
--
ALTER TABLE `tbl_recruitment_form`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_recruitment_rank`
--
ALTER TABLE `tbl_recruitment_rank`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_roles`
--
ALTER TABLE `tbl_roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_schools`
--
ALTER TABLE `tbl_schools`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_standards`
--
ALTER TABLE `tbl_standards`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_timework`
--
ALTER TABLE `tbl_timework`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_timework_history`
--
ALTER TABLE `tbl_timework_history`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_typefiles`
--
ALTER TABLE `tbl_typefiles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_type_question`
--
ALTER TABLE `tbl_type_question`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_workrule`
--
ALTER TABLE `tbl_workrule`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_apply`
--
ALTER TABLE `tbl_apply`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_apply_workexperience`
--
ALTER TABLE `tbl_apply_workexperience`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_awardproposal`
--
ALTER TABLE `tbl_awardproposal`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `tbl_banks`
--
ALTER TABLE `tbl_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT cho bảng `tbl_branchs`
--
ALTER TABLE `tbl_branchs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `tbl_bussinesstypes`
--
ALTER TABLE `tbl_bussinesstypes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tbl_calendar_interview`
--
ALTER TABLE `tbl_calendar_interview`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT cho bảng `tbl_careerroads`
--
ALTER TABLE `tbl_careerroads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=78;

--
-- AUTO_INCREMENT cho bảng `tbl_certificate`
--
ALTER TABLE `tbl_certificate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_chat_private`
--
ALTER TABLE `tbl_chat_private`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `tbl_classify_employee`
--
ALTER TABLE `tbl_classify_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_contracts`
--
ALTER TABLE `tbl_contracts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_create_quiz`
--
ALTER TABLE `tbl_create_quiz`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT cho bảng `tbl_edu_status`
--
ALTER TABLE `tbl_edu_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tbl_employeelevel`
--
ALTER TABLE `tbl_employeelevel`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `tbl_employees`
--
ALTER TABLE `tbl_employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `tbl_employees_banks`
--
ALTER TABLE `tbl_employees_banks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=184;

--
-- AUTO_INCREMENT cho bảng `tbl_employees_historywork`
--
ALTER TABLE `tbl_employees_historywork`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT cho bảng `tbl_employees_relatives`
--
ALTER TABLE `tbl_employees_relatives`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `tbl_employees_salary`
--
ALTER TABLE `tbl_employees_salary`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT cho bảng `tbl_employees_standards`
--
ALTER TABLE `tbl_employees_standards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_employees_works`
--
ALTER TABLE `tbl_employees_works`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `tbl_files`
--
ALTER TABLE `tbl_files`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `tbl_formofworks`
--
ALTER TABLE `tbl_formofworks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `tbl_job_positions`
--
ALTER TABLE `tbl_job_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT cho bảng `tbl_log`
--
ALTER TABLE `tbl_log`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=196;

--
-- AUTO_INCREMENT cho bảng `tbl_majors`
--
ALTER TABLE `tbl_majors`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT cho bảng `tbl_maketest`
--
ALTER TABLE `tbl_maketest`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT cho bảng `tbl_milestones`
--
ALTER TABLE `tbl_milestones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT cho bảng `tbl_modules_detail`
--
ALTER TABLE `tbl_modules_detail`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=626;

--
-- AUTO_INCREMENT cho bảng `tbl_ogcharts`
--
ALTER TABLE `tbl_ogcharts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=110;

--
-- AUTO_INCREMENT cho bảng `tbl_orgcharts`
--
ALTER TABLE `tbl_orgcharts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_permissions`
--
ALTER TABLE `tbl_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=860;

--
-- AUTO_INCREMENT cho bảng `tbl_positions`
--
ALTER TABLE `tbl_positions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT cho bảng `tbl_prizes`
--
ALTER TABLE `tbl_prizes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_question`
--
ALTER TABLE `tbl_question`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT cho bảng `tbl_question_random`
--
ALTER TABLE `tbl_question_random`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT cho bảng `tbl_recruitment`
--
ALTER TABLE `tbl_recruitment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tbl_recruitment_form`
--
ALTER TABLE `tbl_recruitment_form`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_recruitment_rank`
--
ALTER TABLE `tbl_recruitment_rank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_roles`
--
ALTER TABLE `tbl_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_schools`
--
ALTER TABLE `tbl_schools`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT cho bảng `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_standards`
--
ALTER TABLE `tbl_standards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tbl_timework`
--
ALTER TABLE `tbl_timework`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=404;

--
-- AUTO_INCREMENT cho bảng `tbl_timework_history`
--
ALTER TABLE `tbl_timework_history`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=530;

--
-- AUTO_INCREMENT cho bảng `tbl_typefiles`
--
ALTER TABLE `tbl_typefiles`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `tbl_type_question`
--
ALTER TABLE `tbl_type_question`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT cho bảng `tbl_workrule`
--
ALTER TABLE `tbl_workrule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
