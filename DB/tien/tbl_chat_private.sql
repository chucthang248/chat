-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 23, 2021 lúc 03:41 PM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `2020_hungthinh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_chat_private`
--

CREATE TABLE `tbl_chat_private` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_send_id` int(11) NOT NULL,
  `user_receive_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8_unicode_ci NOT NULL,
  `view` tinyint(1) NOT NULL,
  `active_chat` tinyint(1) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_chat_private`
--

INSERT INTO `tbl_chat_private` (`id`, `user_send_id`, `user_receive_id`, `content`, `view`, `active_chat`, `created_at`) VALUES
(182, 13, 14, 'haha', 0, 0, '2021-05-23 20:09:09'),
(183, 13, 17, 'hoho', 0, 0, '2021-05-23 20:09:23'),
(184, 17, 13, 'ok ok ', 0, 0, '2021-05-23 20:09:30'),
(185, 13, 17, 'yesy', 1, 0, '2021-05-23 20:09:52'),
(186, 14, 13, 'ok', 0, 0, '2021-05-23 20:10:16'),
(187, 13, 14, 'ok', 0, 0, '2021-05-23 20:10:42'),
(188, 13, 14, 'haha', 0, 0, '2021-05-23 20:24:11'),
(189, 14, 17, 'hahahahahaha', 0, 0, '2021-05-23 20:35:52'),
(190, 17, 14, 'ok', 0, 0, '2021-05-23 20:36:04'),
(191, 14, 13, '???', 0, 0, '2021-05-23 20:36:22'),
(192, 14, 13, 'cdd', 0, 0, '2021-05-23 20:36:42'),
(193, 13, 14, 'sdfsdf', 0, 0, '2021-05-23 20:36:47'),
(194, 13, 14, 'dfgdfgdfgdfg', 0, 0, '2021-05-23 20:37:11'),
(195, 14, 13, 'dfgdfgdfg', 1, 0, '2021-05-23 20:37:18'),
(196, 13, 14, 'dfgdfgdfg', 0, 0, '2021-05-23 20:37:25'),
(197, 17, 14, 'gdfgdfgdfg', 1, 0, '2021-05-23 20:37:35');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_chat_private`
--
ALTER TABLE `tbl_chat_private`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_chat_private`
--
ALTER TABLE `tbl_chat_private`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=198;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
