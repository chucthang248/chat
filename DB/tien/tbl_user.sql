-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 23, 2021 lúc 03:41 PM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `2020_hungthinh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employeeID` int(11) NOT NULL,
  `socketID` text COLLATE utf8_unicode_ci NOT NULL,
  `online` tinyint(1) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_pass` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type_account` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` int(12) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `verify` tinyint(4) NOT NULL,
  `ogchartID` int(11) NOT NULL,
  `employees_id` int(11) NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `employeeID`, `socketID`, `online`, `email`, `password`, `text_pass`, `type_account`, `type`, `code`, `salt`, `status`, `active`, `verify`, `ogchartID`, `employees_id`, `fullname`, `role_id`, `created_at`, `updated_at`) VALUES
(1, '', 0, '', 0, 'hungthinhllc@gmail.com', '7d1aff778f9f2463590f7119e85f17bcbed7a91a', '123456', '', 'admin', 0, 'b3cWc!b!', 0, 1, 0, 0, 0, '', 0, '2020-10-19 16:45:33', '2020-11-18 13:23:17'),
(8, 'oanhm', 15, '', 0, 'Oanhm@hungthinh1989.com', 'b419da5887712649dfe3ec35068400486ebe68b5', 'Oanh123456', '', '', 0, '+?EBZxBS', 0, 1, 0, 0, 0, 'Ma Thị Oanh', 1, '2021-04-11 10:46:39', '0000-00-00 00:00:00'),
(11, 'nhih', 1, '', 0, 'nhi67536@gmail.com', '97660b4e29d1b0a44a79a1e3bd6ed84b952f9e9b', '12032000', '', '', 0, '?syJcpkL', 0, 1, 0, 0, 0, 'Hồ Thị Phương Nhi', 1, '2021-04-18 12:56:13', '2021-04-29 14:22:03'),
(13, 'xuant', 2, 'E6rBoCwf3ewntyVPAAAZ', 1, 'thanhxuann2004@gmai.com', 'f33b2e07488072b768d0431fe3950872231d922e', 'ntttqt7172', '', '', 0, 'Zn2ci4uQ', 0, 1, 0, 0, 0, 'Trần Thị Thanh Xuân', 1, '2021-04-19 16:09:59', '2021-04-27 14:48:26'),
(14, 'tienn', 3, 'LG4_zASzBM6zqvZ8AAAR', 1, 'nguyenthitructien167@gmail.com', 'd4fe738581b5f8127e48f82287ed4e8b18a9733f', '69452962', '', '', 0, '/wPYckaX', 0, 1, 0, 0, 0, 'Nguyễn Thị Trúc Tiên', 1, '2021-04-26 16:25:43', '0000-00-00 00:00:00'),
(15, 'hav', 4, '', 0, 'vuthiha140900@gmail.com', '9a7df57994c7babe02e5b29e0475d908fe489791', '73679099', '', '', 0, '.t47Vs=3', 0, 1, 0, 0, 0, 'Vũ Thị Hà', 1, '2021-04-26 16:26:28', '0000-00-00 00:00:00'),
(16, 'binhn', 6, '', 0, 'thanhbinh1592002@gmail.com', '7e807f6dc711add404b74b6893aefb904f0db3e3', '72062878', '', '', 0, 'KV!F8v3p', 0, 1, 0, 0, 0, 'Nguyễn Thị Thanh Bình', 1, '2021-04-26 16:26:46', '0000-00-00 00:00:00'),
(17, 'tramn', 9, '3Op11SOitMOE0i1JAAAP', 1, 'nguyentram000262@gmail.com', '7ed4a677d9dc658e280a4dc507308425d7b5a38c', '34051384', '', '', 0, '4dZR5C99', 0, 1, 0, 0, 0, 'Nguyễn Thị Thùy Trâm', 1, '2021-04-26 16:27:02', '0000-00-00 00:00:00'),
(18, 'han', 10, '', 0, 'thanhha20122002@gmail.com', '6b7ef49c71c370d39709a8c1e784f6a44888ccf8', '53849572', '', '', 0, '7DL4xPRV', 0, 1, 0, 0, 0, 'Nguyễn Thị Thanh Hà', 1, '2021-04-26 16:28:00', '0000-00-00 00:00:00'),
(19, 'sangl', 7, '', 0, 'sangglee123@gmail.com', 'e240a12402937d5db6cb1af19f686214edec2d9a', '463391', '', '', 0, '/c2ZxSHp', 0, 1, 0, 0, 0, 'Lê Thị Ngọc Sáng', 1, '2021-04-26 16:28:34', '2021-04-30 21:04:29'),
(20, 'chaun', 11, '', 0, 'nguyenthibaochau2212@gmail.com', 'e9e8ebd1527422ed4dc25b2943aa0e09e5a25f76', 'baochau2212', '', '', 0, '2nYf,?zk', 0, 1, 0, 0, 0, 'Nguyễn Thị Bảo Châu', 1, '2021-04-26 16:30:13', '2021-05-03 09:37:43'),
(21, 'tuann', 12, '', 0, 'nhanhtuantkdh217@gmail.com', '0cee028eeb7e30431cf8c5bd38790166dda68e77', '65369407', '', '', 0, 'LEJV666t', 0, 1, 0, 0, 0, 'Nguyễn Hoàng Anh Tuấn', 1, '2021-04-26 16:30:31', '0000-00-00 00:00:00'),
(23, 'nhul', 17, '', 0, 'nhuluu006@gmail.com', '8d44a677c3df00d13aea15b5a1f0d5f168a09050', '66127184', '', '', 0, '+cnr8GZA', 0, 1, 0, 0, 0, 'Lưu Ngọc Quỳnh Như', 1, '2021-04-26 16:31:04', '0000-00-00 00:00:00'),
(24, 'myt', 20, '', 0, 'tthmy@giayhungthinh.vn', 'c1d86162eb8a4493eedfdc408a703fda080f8b14', 'my123456.', '', '', 0, 'UR?i,s,E', 0, 1, 0, 0, 0, 'Tất Thị Hồng My', 1, '2021-04-26 16:33:11', '2021-05-17 15:13:10'),
(25, 'huongn', 21, '', 0, 'nhxhuong@giayhungthinh.vn', '2d91cb7e085edc55455a837826836eb0e11bf20a', '92189964', '', '', 0, 'KtJkfBhR', 0, 1, 0, 0, 0, 'Nguyễn Huỳnh Xuân Hương', 1, '2021-04-26 16:33:32', '0000-00-00 00:00:00'),
(26, 'thaont', 22, '', 0, 'ntthao@giayhhungthinh.vn', '57350c693c958b00d727cefbf6490bb7bbbcfb68', '15092008', '', '', 0, 'RghxcYjh', 0, 1, 0, 0, 0, 'Nguyễn Thanh Thảo', 1, '2021-04-26 16:34:00', '2021-05-14 23:07:40'),
(30, 'thoan', 16, '', 0, 'kimthonguyen130402@gmail.com', 'd7b51cca878cd211368554b5373d4cc316733538', '100596', '', '', 0, ',iGhYHJb', 0, 1, 0, 0, 0, 'Nguyễn Thị Kim Thoa', 1, '2021-05-04 10:44:10', '2021-05-13 14:42:20'),
(31, 'hungh', 14, '', 0, 'Hungh@hungthinh1989.com', 'acbc50931e6e154a4436267261bd64beae4678e7', '94088250', '', '', 0, 'cix!A=K+', 0, 1, 0, 0, 0, 'Huỳnh Minh Hùng', 1, '2021-05-04 10:51:50', '0000-00-00 00:00:00'),
(32, 'thaon', 13, '', 0, 'Thaon@hungthinh1989.com', 'a34af60445a12174ad3fd1b2090655e5d8d71749', '21782821', '', '', 0, '2V3n=HzH', 0, 1, 0, 0, 0, 'Nguyễn Ngọc Phương Thảo', 1, '2021-05-04 10:52:09', '0000-00-00 00:00:00'),
(33, 'dangn', 26, '', 0, 'nguyenduydang2323@gmail.com', 'cd37e037c7c18e28dc6486f51ba4263985c34c90', '91238441', '', '', 0, 'vzLdkf&y', 0, 1, 0, 0, 0, 'Nguyễn Duy Đăng', 1, '2021-05-04 13:12:47', '0000-00-00 00:00:00'),
(35, 'nguyenh', 23, '', 0, 'thaonguyen302802@gmail.com', 'fd504526fbf30e84d4131418ca3b38c359b316c6', '02092002', '', '', 0, 'zYE!har.', 0, 1, 0, 0, 0, 'Hồ Ngọc Thảo Nguyên', 1, '2021-05-14 08:37:42', '2021-05-14 08:48:48'),
(36, 'Trân', 28, '', 0, 'tranj.giayhungthinh@gmail.com', '924113adfa54ab0acf6a31e587cf7c6dcf6d4143', '120521T', '', '', 0, 'c5yrbsWr', 0, 1, 0, 0, 0, 'Trân', 1, '2021-05-14 14:27:30', '2021-05-14 22:02:28');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
